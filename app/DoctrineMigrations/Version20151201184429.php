<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151201184429 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cvs ADD work_experience_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cvs ADD CONSTRAINT FK_5B9529DB7D97F37 FOREIGN KEY (work_experience_type_id) REFERENCES work_experience_types (id)');
        $this->addSql('CREATE INDEX IDX_5B9529DB7D97F37 ON cvs (work_experience_type_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cvs DROP FOREIGN KEY FK_5B9529DB7D97F37');
        $this->addSql('DROP INDEX IDX_5B9529DB7D97F37 ON cvs');
        $this->addSql('ALTER TABLE cvs DROP work_experience_type_id');
    }
}
