<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170517231412 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE job_professionalarea DROP FOREIGN KEY FK_B5C94A4D482D5C3');
        $this->addSql('ALTER TABLE job_professionalarea DROP FOREIGN KEY FK_B5C94A4DBE04EA9');
        $this->addSql('ALTER TABLE job_professionalarea ADD CONSTRAINT FK_B5C94A4D482D5C3 FOREIGN KEY (professionalarea_id) REFERENCES professional_area (id)');
        $this->addSql('ALTER TABLE job_professionalarea ADD CONSTRAINT FK_B5C94A4DBE04EA9 FOREIGN KEY (job_id) REFERENCES jobs (id)');
        $this->addSql('ALTER TABLE cv_professionalarea DROP FOREIGN KEY FK_DDC7D3F5482D5C3');
        $this->addSql('ALTER TABLE cv_professionalarea DROP FOREIGN KEY FK_DDC7D3F5CFE419E2');
        $this->addSql('ALTER TABLE cv_professionalarea ADD CONSTRAINT FK_DDC7D3F5482D5C3 FOREIGN KEY (professionalarea_id) REFERENCES professional_area (id)');
        $this->addSql('ALTER TABLE cv_professionalarea ADD CONSTRAINT FK_DDC7D3F5CFE419E2 FOREIGN KEY (cv_id) REFERENCES cvs (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cv_professionalarea DROP FOREIGN KEY FK_DDC7D3F5CFE419E2');
        $this->addSql('ALTER TABLE cv_professionalarea DROP FOREIGN KEY FK_DDC7D3F5482D5C3');
        $this->addSql('ALTER TABLE cv_professionalarea ADD CONSTRAINT FK_DDC7D3F5CFE419E2 FOREIGN KEY (cv_id) REFERENCES cvs (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE cv_professionalarea ADD CONSTRAINT FK_DDC7D3F5482D5C3 FOREIGN KEY (professionalarea_id) REFERENCES professional_area (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE job_professionalarea DROP FOREIGN KEY FK_B5C94A4DBE04EA9');
        $this->addSql('ALTER TABLE job_professionalarea DROP FOREIGN KEY FK_B5C94A4D482D5C3');
        $this->addSql('ALTER TABLE job_professionalarea ADD CONSTRAINT FK_B5C94A4DBE04EA9 FOREIGN KEY (job_id) REFERENCES jobs (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE job_professionalarea ADD CONSTRAINT FK_B5C94A4D482D5C3 FOREIGN KEY (professionalarea_id) REFERENCES professional_area (id) ON DELETE CASCADE');
    }
}
