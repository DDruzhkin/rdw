<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160927210346 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('delete from cv_educations where cv_id IN (select cv.id from cvs cv inner join users user on cv.user_id = user.id and user.object_type = "anonymous")');
        $this->addSql('delete from cv_work_experiences where cv_id IN (select cvs.id from cvs inner join users on users.id = cvs.user_id where users.object_type = "anonymous")');
        $this->addSql('delete from cv_languages where cv_id IN (select cvs.id from cvs inner join users on users.id = cvs.user_id where users.object_type = "anonymous")');
        $this->addSql('delete from job_applies where cv_id IN (select cvs.id from cvs inner join users on users.id = cvs.user_id where users.object_type = "anonymous")');
        $this->addSql('delete from messages where conversation_id IN (select conversations.id from conversations inner join job_applies on conversations.apply_id = job_applies.id inner join cvs on cvs.id = job_applies.cv_id inner join users on users.id = cvs.user_id where users.object_type = "anonymous")');
        $this->addSql('delete from conversations where apply_id IN (select job_applies.id from job_applies inner join cvs on cvs.id = job_applies.cv_id inner join users on users.id = cvs.user_id where users.object_type = "anonymous")');
        $this->addSql('delete from cv_viewer where user_id in (select id from users where object_type = "anonymous")');
        $this->addSql('delete from messages where conversation_id IN (select conversations.id from conversations inner join cv_messages on conversations.cv_message_id = cv_messages.id inner join cvs on cvs.id = cv_messages.cv_id inner join users on users.id = cvs.user_id where users.object_type = "anonymous")');
        $this->addSql('delete from conversations where cv_message_id IN (select cv_messages.id from cv_messages inner join cvs on cvs.id = cv_messages.cv_id inner join users on users.id = cvs.user_id where users.object_type = "anonymous")');
        $this->addSql('delete from cv_messages where cv_id in (select cv.id from cvs cv inner join users user on cv.user_id = user.id and user.object_type = "anonymous")');
        $this->addSql('delete from history where user_id in (select id from users where object_type = "anonymous")');
        $this->addSql('delete from cvs where user_id IN (select id from users where object_type = "anonymous")');
        $this->addSql('delete from user_job_favorites where user_id in (select id from users where object_type = "anonymous")');
        $this->addSql('delete from users where object_type = "anonymous"');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
