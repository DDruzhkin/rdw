<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151202161829 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE job_contact_phones (id INT AUTO_INCREMENT NOT NULL, job_id INT NOT NULL, phone VARCHAR(255) NOT NULL, INDEX IDX_E1C349B1BE04EA9 (job_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE job_contact_phones ADD CONSTRAINT FK_E1C349B1BE04EA9 FOREIGN KEY (job_id) REFERENCES jobs (id)');
        $this->addSql('INSERT INTO job_contact_phones (job_id, phone) SELECT id, contact_person_phone FROM jobs WHERE contact_person_phone IS NOT NULL');
        $this->addSql('ALTER TABLE jobs DROP contact_person_phone');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE job_contact_phones');
        $this->addSql('ALTER TABLE jobs ADD contact_person_phone LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
