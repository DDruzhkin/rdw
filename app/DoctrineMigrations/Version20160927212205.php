<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160927212205 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E9FA93803');
        $this->addSql('DROP INDEX IDX_1483A5E9FA93803 ON users');
        $this->addSql('DROP INDEX cookie_id_idx ON users');
        $this->addSql('ALTER TABLE users DROP anonymous_id, DROP cookie_id, DROP o_auth_id, CHANGE username username VARCHAR(180) NOT NULL, CHANGE username_canonical username_canonical VARCHAR(180) NOT NULL, CHANGE email email VARCHAR(180) NOT NULL, CHANGE email_canonical email_canonical VARCHAR(180) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users ADD anonymous_id INT DEFAULT NULL, ADD cookie_id VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD o_auth_id VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE username username VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE username_canonical username_canonical VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE email email VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE email_canonical email_canonical VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E9FA93803 FOREIGN KEY (anonymous_id) REFERENCES users (id)');
        $this->addSql('CREATE INDEX IDX_1483A5E9FA93803 ON users (anonymous_id)');
        $this->addSql('CREATE INDEX cookie_id_idx ON users (cookie_id)');
    }
}
