<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170228113249 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE position_professionalarea (position_id INT NOT NULL, professionalarea_id INT NOT NULL, INDEX IDX_2EBE31A1DD842E46 (position_id), INDEX IDX_2EBE31A1482D5C3 (professionalarea_id), PRIMARY KEY(position_id, professionalarea_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE professional_area (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, type VARCHAR(256) DEFAULT NULL, is_system TINYINT(1) DEFAULT NULL, title_in_latin VARCHAR(100) DEFAULT NULL, title_where VARCHAR(100) NOT NULL, position INT NOT NULL, UNIQUE INDEX UNIQ_C5D104B637D90AD5 (title_in_latin), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE position_professionalarea ADD CONSTRAINT FK_2EBE31A1DD842E46 FOREIGN KEY (position_id) REFERENCES job_positions (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE position_professionalarea ADD CONSTRAINT FK_2EBE31A1482D5C3 FOREIGN KEY (professionalarea_id) REFERENCES professional_area (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE users CHANGE username username VARCHAR(255) NOT NULL, CHANGE username_canonical username_canonical VARCHAR(255) NOT NULL, CHANGE email email VARCHAR(255) NOT NULL, CHANGE email_canonical email_canonical VARCHAR(255) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE position_professionalarea DROP FOREIGN KEY FK_2EBE31A1482D5C3');
        $this->addSql('DROP TABLE position_professionalarea');
        $this->addSql('DROP TABLE professional_area');
        $this->addSql('ALTER TABLE users CHANGE username username VARCHAR(180) NOT NULL COLLATE utf8_unicode_ci, CHANGE username_canonical username_canonical VARCHAR(180) NOT NULL COLLATE utf8_unicode_ci, CHANGE email email VARCHAR(180) NOT NULL COLLATE utf8_unicode_ci, CHANGE email_canonical email_canonical VARCHAR(180) NOT NULL COLLATE utf8_unicode_ci');
    }
}
