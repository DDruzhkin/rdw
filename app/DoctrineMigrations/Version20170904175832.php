<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170904175832 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $socials = array(
            'social_telegram_link' => ['title' => 'Telegram url', 'url' => 'http://telegram.com'],
            'social_viber_link' => ['title' => 'Viber url', 'url' => 'http://viber.com'],
            'social_inst_link' => ['title' => 'Instagram url', 'url' => 'http://instagram.com'],
            'social_yo_link' => ['title' => 'Youtube url', 'url' => 'http://youtube.com'],
        );
        foreach ($socials as $key => $data) {
            $this->addSql(sprintf(
                'INSERT INTO settings (title, setting_key, value) VALUES (\'%s\', \'%s\', \'%s\')',
                $data['title'],
                $key,
                $data['url']
            ));
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
