<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151221205434 extends AbstractMigration implements ContainerAwareInterface
{

    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cities ADD title_where VARCHAR(100) DEFAULT NULL');
    }

    /**
     * generates latin title for all cities
     */
    public function postUp(Schema $schema)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine')->getManager();
        /** @var \RDW\Bundle\LocationBundle\Entity\City[] $cities */
        $cities = $em->getRepository('RDWLocationBundle:City')->findAll();
        foreach ($cities as $city) {
            $city->setTitleInLatin($city->generateTitleInLatin());
            $em->persist($city);
        }
        $em->flush();

        $this->connection->executeQuery("UPDATE cities SET title_where = title");
        $this->connection->executeQuery('ALTER TABLE cities CHANGE title_where title_where VARCHAR(100) NOT NULL');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cities DROP title_where');
    }
}
