<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160610121323 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE job_contact_phones RENAME contact_phones');
        $this->addSql('ALTER TABLE contact_phones DROP FOREIGN KEY FK_E1C349B1BE04EA9');
        $this->addSql('DROP INDEX IDX_E1C349B1BE04EA9 ON contact_phones');
        $this->addSql('ALTER TABLE contact_phones ADD discriminator VARCHAR(255) NOT NULL, ADD discriminator_id INT DEFAULT NULL');
        $this->addSql('UPDATE contact_phones SET discriminator = \'job\', discriminator_id = job_id');
        $this->addSql('ALTER TABLE contact_phones DROP job_id');
        $this->addSql('INSERT INTO contact_phones (discriminator, discriminator_id, phone) SELECT \'cv\', cvs.id, cvs.phone FROM cvs WHERE cvs.phone IS NOT NULL');
        $this->addSql('CREATE INDEX IDX_73BA19741CC6D998 ON contact_phones (discriminator_id)');
        $this->addSql('ALTER TABLE cvs DROP phone');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contact_phones RENAME job_contact_phones');
        $this->addSql('ALTER TABLE job_contact_phones ADD DROP discriminator, CHANGE discriminator_id job_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE job_contact_phones ADD CONSTRAINT FK_E1C349B1BE04EA9 FOREIGN KEY (job_id) REFERENCES jobs (id)');
        $this->addSql('ALTER TABLE cvs ADD phone VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
