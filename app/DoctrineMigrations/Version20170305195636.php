<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170305195636 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE job_professionalarea (job_id INT NOT NULL, professionalarea_id INT NOT NULL, INDEX IDX_B5C94A4DBE04EA9 (job_id), INDEX IDX_B5C94A4D482D5C3 (professionalarea_id), PRIMARY KEY(job_id, professionalarea_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cv_professionalarea (cv_id INT NOT NULL, professionalarea_id INT NOT NULL, INDEX IDX_DDC7D3F5CFE419E2 (cv_id), INDEX IDX_DDC7D3F5482D5C3 (professionalarea_id), PRIMARY KEY(cv_id, professionalarea_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE job_professionalarea ADD CONSTRAINT FK_B5C94A4DBE04EA9 FOREIGN KEY (job_id) REFERENCES jobs (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE job_professionalarea ADD CONSTRAINT FK_B5C94A4D482D5C3 FOREIGN KEY (professionalarea_id) REFERENCES professional_area (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE cv_professionalarea ADD CONSTRAINT FK_DDC7D3F5CFE419E2 FOREIGN KEY (cv_id) REFERENCES cvs (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE cv_professionalarea ADD CONSTRAINT FK_DDC7D3F5482D5C3 FOREIGN KEY (professionalarea_id) REFERENCES professional_area (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE job_professionalarea');
        $this->addSql('DROP TABLE cv_professionalarea');
    }
}
