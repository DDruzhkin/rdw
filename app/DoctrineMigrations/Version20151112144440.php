<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151112144440 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE jobs ADD slug VARCHAR(255) NOT NULL, CHANGE position_id position_id INT NOT NULL');
        $this->addSql('ALTER TABLE cvs ADD slug VARCHAR(255) NOT NULL');
        $this->addSql('CREATE INDEX slug_idx ON jobs (slug)');
        $this->addSql('CREATE INDEX slug_idx ON cvs (slug)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX slug_idx ON cvs');
        $this->addSql('DROP INDEX slug_idx ON jobs');
        $this->addSql('ALTER TABLE cvs DROP slug');
        $this->addSql('ALTER TABLE jobs DROP slug, CHANGE position_id position_id INT DEFAULT NULL');
    }
}
