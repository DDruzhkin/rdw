<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151126153301 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE jobs DROP FOREIGN KEY FK_A8936DC5DD842E46');
        $this->addSql('ALTER TABLE jobs ADD views_count INT DEFAULT NULL');
        $this->addSql('DROP INDEX IDX_A8936DC5DD842E46 ON jobs');
        $this->addSql('CREATE INDEX IDX_A8936DC5DD842E46 ON jobs (position_id)');
        $this->addSql('ALTER TABLE jobs ADD CONSTRAINT FK_A8936DC5DD842E46 FOREIGN KEY (position_id) REFERENCES job_positions (id)');
        $this->addSql('ALTER TABLE cvs ADD views_count INT DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cvs DROP views_count');
        $this->addSql('ALTER TABLE jobs DROP FOREIGN KEY FK_A8936DC5DD842E46');
        $this->addSql('ALTER TABLE jobs DROP views_count');
        $this->addSql('DROP INDEX idx_a8936dc5dd842e46 ON jobs');
        $this->addSql('CREATE INDEX FK_A8936DC5DD842E46 ON jobs (position_id)');
        $this->addSql('ALTER TABLE jobs ADD CONSTRAINT FK_A8936DC5DD842E46 FOREIGN KEY (position_id) REFERENCES job_positions (id)');
    }
}
