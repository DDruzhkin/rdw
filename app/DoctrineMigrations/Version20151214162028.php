<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20151214162028 extends AbstractMigration
{

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company_types ADD is_system TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE marital_statuses ADD is_system TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE scopes ADD is_system TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE cities ADD is_system TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE settings CHANGE setting_key setting_key VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE education_types ADD is_system TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE employment_types ADD is_system TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE languages ADD is_system TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE language_levels ADD is_system TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE job_positions ADD is_system TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE work_experience_types ADD is_system TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE work_schedules ADD is_system TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE business_trips ADD is_system TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE driver_license_categories ADD is_system TINYINT(1) DEFAULT NULL');

        $this->addSql('ALTER TABLE company_types ADD type VARCHAR(256) DEFAULT NULL');
        $this->addSql('ALTER TABLE marital_statuses ADD type VARCHAR(256) DEFAULT NULL');
        $this->addSql('ALTER TABLE scopes ADD type VARCHAR(256) DEFAULT NULL');
        $this->addSql('ALTER TABLE cities ADD type VARCHAR(256) DEFAULT NULL');
        $this->addSql('ALTER TABLE education_types ADD type VARCHAR(256) DEFAULT NULL');
        $this->addSql('ALTER TABLE employment_types ADD type VARCHAR(256) DEFAULT NULL');
        $this->addSql('ALTER TABLE languages ADD type VARCHAR(256) DEFAULT NULL');
        $this->addSql('ALTER TABLE language_levels ADD type VARCHAR(256) DEFAULT NULL');
        $this->addSql('ALTER TABLE job_positions ADD type VARCHAR(256) DEFAULT NULL');
        $this->addSql('ALTER TABLE work_schedules ADD type VARCHAR(256) DEFAULT NULL');
        $this->addSql('ALTER TABLE business_trips ADD type VARCHAR(256) DEFAULT NULL');
        $this->addSql('ALTER TABLE driver_license_categories ADD type VARCHAR(256) DEFAULT NULL');

        $this->addSql('ALTER TABLE cities ADD title_in_latin VARCHAR(100) DEFAULT NULL');
        $this->addSql('CREATE INDEX title_in_latin_idx ON cities (title_in_latin)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX title_in_latin_idx ON cities');
        $this->addSql('ALTER TABLE cities DROP title_in_latin');

        $this->addSql('ALTER TABLE business_trips DROP is_system');
        $this->addSql('ALTER TABLE cities DROP is_system');
        $this->addSql('ALTER TABLE company_types DROP is_system');
        $this->addSql('ALTER TABLE driver_license_categories DROP is_system');
        $this->addSql('ALTER TABLE education_types DROP is_system');
        $this->addSql('ALTER TABLE employment_types DROP is_system');
        $this->addSql('ALTER TABLE job_positions DROP is_system');
        $this->addSql('ALTER TABLE language_levels DROP is_system');
        $this->addSql('ALTER TABLE languages DROP is_system');
        $this->addSql('ALTER TABLE marital_statuses DROP is_system');
        $this->addSql('ALTER TABLE scopes DROP is_system');
        $this->addSql('ALTER TABLE settings CHANGE setting_key setting_key VARCHAR(255) DEFAULT \'\' NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE work_experience_types DROP is_system');
        $this->addSql('ALTER TABLE work_schedules DROP is_system');

        $this->addSql('ALTER TABLE business_trips DROP type');
        $this->addSql('ALTER TABLE cities DROP type');
        $this->addSql('ALTER TABLE company_types DROP type');
        $this->addSql('ALTER TABLE driver_license_categories DROP type');
        $this->addSql('ALTER TABLE education_types DROP type');
        $this->addSql('ALTER TABLE employment_types DROP type');
        $this->addSql('ALTER TABLE job_positions DROP type');
        $this->addSql('ALTER TABLE language_levels DROP type');
        $this->addSql('ALTER TABLE languages DROP type');
        $this->addSql('ALTER TABLE marital_statuses DROP type');
        $this->addSql('ALTER TABLE scopes DROP type');
        $this->addSql('ALTER TABLE work_schedules DROP type');
    }
}
