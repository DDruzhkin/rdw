<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170513155407 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE professional_area ADD parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE professional_area ADD CONSTRAINT FK_C5D104B6727ACA70 FOREIGN KEY (parent_id) REFERENCES professional_area (id)');
        $this->addSql('CREATE INDEX IDX_C5D104B6727ACA70 ON professional_area (parent_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE professional_area DROP FOREIGN KEY FK_C5D104B6727ACA70');
        $this->addSql('DROP INDEX IDX_C5D104B6727ACA70 ON professional_area');
        $this->addSql('ALTER TABLE professional_area DROP parent_id');
    }
}
