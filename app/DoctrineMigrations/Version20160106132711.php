<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160106132711 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE position_synonyms (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE position_synonym_positions (position_synonym_id INT NOT NULL, position_id INT NOT NULL, INDEX IDX_8AC63CBAA2FA5E0E (position_synonym_id), INDEX IDX_8AC63CBADD842E46 (position_id), PRIMARY KEY(position_synonym_id, position_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE position_synonym_positions ADD CONSTRAINT FK_8AC63CBAA2FA5E0E FOREIGN KEY (position_synonym_id) REFERENCES position_synonyms (id)');
        $this->addSql('ALTER TABLE position_synonym_positions ADD CONSTRAINT FK_8AC63CBADD842E46 FOREIGN KEY (position_id) REFERENCES job_positions (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE position_synonym_positions DROP FOREIGN KEY FK_8AC63CBAA2FA5E0E');
        $this->addSql('DROP TABLE position_synonyms');
        $this->addSql('DROP TABLE position_synonym_positions');
    }
}
