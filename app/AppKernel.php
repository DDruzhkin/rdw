<?php

use \Akuma\Bundle\DistributionBundle\AkumaKernel;
use Symfony\Component\Config\Loader\LoaderInterface;

/**
 * Class AppKernel
 */
class AppKernel extends AkumaKernel
{
    /**
     * {@inheritDoc}
     */
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Braincrafted\Bundle\BootstrapBundle\BraincraftedBootstrapBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new Avalanche\Bundle\ImagineBundle\AvalancheImagineBundle(),
            new Knp\Bundle\GaufretteBundle\KnpGaufretteBundle(),
            new Ivory\CKEditorBundle\IvoryCKEditorBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new Slot\MandrillBundle\SlotMandrillBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),
            new Knp\Bundle\SnappyBundle\KnpSnappyBundle(),
            new Presta\SitemapBundle\PrestaSitemapBundle(),
            new Oneup\UploaderBundle\OneupUploaderBundle(),
            new Pinano\Select2Bundle\PinanoSelect2Bundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new SunCat\MobileDetectBundle\MobileDetectBundle(),
            new JMS\DiExtraBundle\JMSDiExtraBundle($this),
            new JMS\AopBundle\JMSAopBundle(),
            new JMS\TranslationBundle\JMSTranslationBundle(),
            new FM\ElfinderBundle\FMElfinderBundle(),
            new Widop\GoogleAnalyticsBundle\WidopGoogleAnalyticsBundle(),
            new Widop\HttpAdapterBundle\WidopHttpAdapterBundle(),
            new RDW\Bundle\UserBundle\RDWUserBundle(),
            new RDW\Bundle\LocationBundle\RDWLocationBundle(),
            new RDW\Bundle\AppBundle\RDWAppBundle(),
            new RDW\Bundle\ManageBundle\RDWManageBundle(),
            new RDW\Bundle\ConversationBundle\RDWConversationBundle(),
            new RDW\Bundle\ContentBundle\RDWContentBundle(),
            new RDW\Bundle\ContactBundle\RDWContactBundle(),
            new RDW\Bundle\FaqBundle\RDWFaqBundle(),
            new RDW\Bundle\MailerBundle\RDWMailerBundle(),
            new RDW\Bundle\SlugBundle\RDWSlugBundle(),
            new RDW\Bundle\MenuBundle\RDWMenuBundle(),
            new RDW\Bundle\JobBundle\RDWJobBundle(),
            new RDW\Bundle\CvBundle\RDWCvBundle(),
            new RDW\Bundle\OrderBundle\RDWOrderBundle(),
            new RDW\Bundle\SubscriptionBundle\RDWSubscriptionBundle(),
            new RDW\Bundle\HistoryBundle\RDWHistoryBundle(),
            new RDW\Bundle\WalletBundle\RDWWalletBundle(),
            new RDW\Bundle\ServiceBundle\RDWServiceBundle(),
            new RDW\Bundle\MoneyBundle\RDWMoneyBundle(),
            new RDW\Bundle\PaymentBundle\RDWPaymentBundle(),
            new RDW\Bundle\SmsBundle\RDWSmsBundle(),
            new RDW\Bundle\SearchBundle\RDWSearchBundle(),
            new RDW\Bundle\PdfBundle\RDWPdfBundle(),
            new Staffim\RollbarBundle\StaffimRollbarBundle(),
            new HWI\Bundle\OAuthBundle\HWIOAuthBundle(),
            new RDW\Bundle\ImportExportBundle\RDWImportExportBundle(),

            // The admin requires some twig functions defined in the security
            // bundle, like is_granted. Register this bundle if it wasn't the case
            // already.
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),

            // These are the other bundles the SonataAdminBundle relies on
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            new Sonata\CoreBundle\SonataCoreBundle(),
            new Sonata\NotificationBundle\SonataNotificationBundle(),
            new Sonata\CacheBundle\SonataCacheBundle(),
            new Sonata\BlockBundle\SonataBlockBundle(),
            new Sonata\PageBundle\SonataPageBundle(),
            new Sonata\EasyExtendsBundle\SonataEasyExtendsBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Symfony\Cmf\Bundle\RoutingBundle\CmfRoutingBundle(),
            new Symfony\Cmf\Bundle\ResourceBundle\CmfResourceBundle(),
            new Symfony\Cmf\Bundle\BlockBundle\CmfBlockBundle(),

            new Sonata\SeoBundle\SonataSeoBundle(),
            new Symfony\Cmf\Bundle\SeoBundle\CmfSeoBundle(),

            // And finally, the storage and SonataAdminBundle
            new FOS\UserBundle\FOSUserBundle(),
            new Sonata\UserBundle\SonataUserBundle('FOSUserBundle'),
            new Sonata\AdminBundle\SonataAdminBundle(),

            new Symfony\Cmf\Bundle\TreeBrowserBundle\CmfTreeBrowserBundle(),
            new Sonata\DoctrineORMAdminBundle\SonataDoctrineORMAdminBundle(),


            /////
            new Application\Sonata\NotificationBundle\ApplicationSonataNotificationBundle(),
            new Application\Sonata\PageBundle\ApplicationSonataPageBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
        }

        return parent::registerBundles($bundles);
    }

    /**
     * {@inheritDoc}
     */
    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
