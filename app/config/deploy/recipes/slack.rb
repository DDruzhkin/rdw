namespace :slack do
    task :starting, :roles => :app do
        slack_connect("New release is comming... :sweat_smile:")
    end

    task :finished, :roles => :app do
        slack_connect("Enjoy :clap:")
    end

    namespace :migration do
        task :start do
            @migration_start_time = Time.now
            slack_connect("Running Migrations")
        end
        task :end do
            elapsed_time = Time.now.to_i - @migration_start_time.to_i   if @migration_start_time
            slack_connect("Migrations finished in #{elapsed_time} seconds")
        end
    end
end

def slack_connect(msg)
    webhook_url = "https://hooks.slack.com/services/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"
    username = "deployer"
    channel = "#rdw"

    run "curl -X POST --data-urlencode 'payload={\"channel\": \"#{channel}\", \"username\": \"#{username}\", \"text\": \"#{msg}\", \"icon_emoji\": \":ghost:\"}' #{webhook_url}"
end
