namespace :fpm do
    desc <<-DESC
        Restarts PHP5-FPM
    DESC
    task :restart, :roles => :app do
        run "sudo /usr/sbin/service php5-fpm restart"
    end
end