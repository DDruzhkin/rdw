namespace :translation do
    desc <<-DESC
    Extract translations
    DESC
    task :extract do
      run "cd #{latest_release} && php app/console translation:extract -c app --env=prod"
    end
end