namespace :cache do
    desc <<-DESC
        Create a temporary PHP file to clear cache, call it (using curl) and removes it
        This task must be triggered AFTER the deployment to clear cache
    DESC
    task :clear, :roles => :app do
        cache_file = "#{current_release}/web/cache_clear.php"
        curl_options = "-s"

        put "<?php opcache_reset(); apc_clear_cache('opcode'); apc_clear_cache(); apc_clear_cache('user'); ", cache_file, :mode => 0644
        run "curl #{curl_options} #{url_base}/cache_clear.php && rm -f #{cache_file}"
    end
end