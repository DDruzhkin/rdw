load 'app/config/deploy/recipes/translations'
load 'app/config/deploy/recipes/cache'
load 'app/config/deploy/recipes/slack'

# encoding: UTF-8
set :application, "rdw"
set :user, "deploy"
set :app_path,    "app"

set :stages,        %w(staging production)
set :default_stage, "staging"
set :stage_dir,     "app/config/deploy"
require 'capistrano/ext/multistage'

set :repository,  "git@bitbucket.org:Nikita7000/rdw-by.git"
set :scm,         :git
set :deploy_via,  :remote_cache

default_run_options[:pty] = true
ssh_options[:forward_agent] = true

set :model_manager, "doctrine"

set :use_sudo,      false

set  :keep_releases,  3
# cleanup releases after deploy
after "deploy", "deploy:cleanup"

# do not use interactive mode
set :interactive_mode, false

# Symfony2 configuration
set :shared_files,      ["app/config/parameters.yml", "app/Resources/bin/rdwby-cc3615c417d5.p12"]
set :shared_children,   ["app/logs", "app/Resources/translations", "vendor", "app/sessions", "web/images", "web/media", "web/uploads", "web/exports"]
set :writable_dirs,     ["app/cache", "app/Resources/translations", "app/logs", "web/bundles", "web/media", "web/uploads", "web/exports"]
set :webserver_user,     "www-data"
set :permission_method,   :acl
set :use_set_permissions, true

# Composer configuration
set :use_composer, true
set :copy_vendors, true
set :composer_options,  "--no-dev --verbose --prefer-dist --optimize-autoloader"

# Symfony2 specific config
set :dump_assetic_assets, true

# Be more verbose by uncommenting the following  line
logger.level = Logger::MAX_LEVEL
