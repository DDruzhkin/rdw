<?php

/**
 * solution for PDF generation
 *
 * @author aur1mas <ab@nebijokit.lt>
 */

$slug = array_key_exists('slug', $_GET) ? (string)$_GET['slug'] : null;

if ($slug === null || mb_strlen($slug, 'utf-8') === 0) {
    die('wrong parameters provided');
}

require_once __DIR__.'/../../vendor/autoload.php';

class RDWPdf extends \Knp\Snappy\Pdf
{
    public function getStream($input, array $options = [])
    {
        $command = $this->getCommand($input, '-', $options);
        return $this->executeCommand($command);
    }
}

$pdf = new RDWPdf('/usr/local/bin/wkhtmltopdf-amd64');
$pdf->setOption('print-media-type', true);

$options = [];

$cookieName = 'rdw_ssid';
if (isset($_COOKIE[$cookieName])) {
    $options['cookie'] = [$cookieName => $_COOKIE[$cookieName]];
}

$slug = urldecode($slug);

$id = explode('/', $slug);
$id = array_pop($id);

$uri = sprintf('http://rdw.by%s', $slug);

$result = $pdf->getStream($uri, $options);
$result = $result[1];

header("Content-Description: File Transfer");
header("Cache-Control: public; must-revalidate, max-age=0");
header("Pragma: public");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate('D, d m Y H:i:s') . " GMT");
header("Content-Type: application/force-download");
header("Content-Type: application/octec-stream", false);
header("Content-Type: application/download", false);
header("Content-Type: application/pdf", false);
header('Content-Disposition: attachment; filename="' . $id .'.pdf";');
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . strlen($result));
echo $result;
exit;
