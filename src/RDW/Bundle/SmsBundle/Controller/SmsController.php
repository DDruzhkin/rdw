<?php

namespace RDW\Bundle\SmsBundle\Controller;

use RDW\Bundle\OrderBundle\Entity\Order;
use RDW\Bundle\OrderBundle\RDWOrderStatus;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class DefaultController
 *
 * @package RDW\Bundle\SmsBundle\Controller
 *
 * @Route("/sms")
 */
class SmsController extends Controller
{
    /**
     * @param Order $order
     *
     * @Route("/pay/{id}", name="rdw_sms.sms.pay", requirements={"id" = "\d+"})
     *
     * @return RedirectResponse
     */
    public function payAction(Order $order)
    {
        $this->isGrantedLocal($order);

        $period = $this->container->getParameter('rdw_order.default_period');

        if (! $order->isItemsPeriodValidForSms($period)) {
            $this->container->get('braincrafted_bootstrap.flash')
                ->alert($this->get('translator')->trans('Pay via SMS it\'s possible only for 7 days period for each service'));

            return $this->redirect($this->generateUrl('rdw_order.order.submit', ['id' => $order->getId()]));
        }

        $orderManager = $this->container->get('rdw_order.service.order_manager');
        $orderManager->generateSmsCodes($order);

        return $this->redirect($this->generateUrl('rdw_sms.sms.finish_payment', ['id' => $order->getId()]));
    }

    /**
     * @param Order $order
     *
     * @Route("/finish-payment/{id}", name="rdw_sms.sms.finish_payment")
     * @Template()
     *
     * @return RedirectResponse|array
     */
    public function finishAction(Order $order)
    {
        $this->isGrantedLocal($order);

        if (Order::PAYMENT_SMS != $order->getPaymentType()) {
            return $this->redirect($this->generateUrl('rdw_order.order.list', ['id' => $this->getUser()->getId()]));
        }

        if ($order->hasItemsWithoutSmsCode()) {
            $this->container->get('rdw_order.service.order_manager')->generateSmsCodes($order);
        }

        $items = $this->getDoctrine()->getRepository('RDWOrderBundle:OrderItemCv')->findBy(['order' => $order]);

        return [
            'order' => $order,
            'items' => $items,
        ];
    }

    /**
     * @param Request $request
     *
     * @Route("/callback", name="rdw_sms.sms.callback")
     *
     * @return Response
     */
    public function callbackAction(Request $request)
    {
        $smsCodeManager = $this->container->get('rdw_sms.service.sms_code_manager');
        $smsCodeManager->logAllInfo();
        $smsCodeManager->checkGateway($request->getClientIp());

        $code = trim(urldecode($request->query->get('utf8_text')));
        $number = $request->query->get('to');

        $smsCode = $smsCodeManager->getByCodeAndNumber($code, $number);

        $orderItem = $this->getDoctrine()
            ->getRepository('RDWOrderBundle:OrderItemCv')
            ->findOneBy(['smsCode' => $smsCode, 'status' => RDWOrderStatus::STATUS_PENDING]);

        if (! $orderItem) {
            return new Response('Ne oplacheno');
        }

        $this->container->get('rdw_order.service.order_item_manager')->setAsPaid($orderItem);

        $order = $orderItem->getOrder();
        $order->setPaidAt(new \DateTime());
        $this->container->get('rdw_order.service.order_manager')->setAsFullyPaid($order);

        return new Response('Oplacheno');
    }

    /**
     * @param Order $order
     *
     * @throws AccessDeniedException
     */
    private function isGrantedLocal(Order $order)
    {
        if ( ! $this->isOwner($order)) {
            throw new AccessDeniedException();
        }
    }

    /**
     * @param Order $order
     *
     * @return bool
     */
    private function isOwner(Order $order)
    {
        $user = $this->getUser();

        if (
            (
                $this->container->get('security.context')->isGranted('ROLE_EMPLOYER')
                || $this->container->get('security.context')->isGranted('ROLE_EMPLOYEE')
            )
            && $user instanceof RegisteredUser
            && (
                $user->getId() == $order->getUser()->getId() // if owner
                || $order->getUser()->isUserManager($user) // is manager of job owner
            )
        ) {
            return true;
        } else {
            return false;
        }
    }
}
