<?php

namespace RDW\Bundle\SmsBundle\Repository;

use Doctrine\ORM\EntityRepository;
use RDW\Bundle\UserBundle\Util\StringGenerator;

/**
 * Class SmsCodeRepository
 *
 * @package RDW\Bundle\SmsBundle\Repository
 */
class SmsCodeRepository extends EntityRepository
{
    /**
     * @param int $length
     *
     * @return string
     */
    public function generateCode($length)
    {
        do {
            $code = $this->generateSmsCodeText($length);
        } while ($this->findOneBy(['code' => $code]));

        return $code;
    }

    public function generateSmsCodeText($length)
    {
        $generator = new StringGenerator();

        return sprintf('RDW %s', strtoupper($generator->generateRandomString($length)));
    }
}
