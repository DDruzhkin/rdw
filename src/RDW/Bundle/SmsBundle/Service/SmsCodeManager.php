<?php

namespace RDW\Bundle\SmsBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\ServiceBundle\Entity\Service;
use RDW\Bundle\SmsBundle\Entity\SmsCode;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class SmsCodeManager
 *
 * @package RDW\Bundle\SmsBundle\Service
 */
class SmsCodeManager
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $entityManager;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var int
     */
    private $codeLength;

    /**
     * @var string
     */
    private $smsGateway;

    /**
     * @param ObjectManager $objectManager
     * @param Logger        $logger
     * @param int           $codeLength
     * @param string        $smsGateway
     */
    public function __construct(ObjectManager $objectManager, Logger $logger, $codeLength, $smsGateway = null)
    {
        $this->entityManager = $objectManager;
        $this->logger = $logger;
        $this->codeLength = $codeLength;
        $this->smsGateway = $smsGateway;
    }

    /**
     * @return SmsCode
     */
    public function create(Service $service)
    {
        $repository = $this->entityManager->getRepository('RDWSmsBundle:SmsCode');

        $code = new SmsCode();
        $code->setCode($repository->generateCode($this->codeLength));
        $code->setNumber($service->getSmsNumber());

        $this->entityManager->persist($code);
        $this->entityManager->flush();

        return $code;
    }

    /**
     * Log all info from globals
     */
    public function logAllInfo()
    {
        $this->logger->info('GET: ' . print_r($_GET, true));
        $this->logger->info('POST: ' . print_r($_POST, true));
        $this->logger->info('REQUEST: ' . print_r($_REQUEST, true));
        $this->logger->info('SERVER: ' . print_r($_SERVER, true));
    }

    /**
     * Check if gateway is valid
     *
     * @throws NotFoundHttpException
     */
    public function checkGateway($clientIp)
    {
        if ($this->smsGateway && $this->smsGateway != $clientIp) {
            $this->logger->error(sprintf('Wrong SMS gateway: %s', $clientIp));

            throw new NotFoundHttpException();
        }
    }

    /**
     * @param string $code
     * @param int    $number
     *
     * @return SmsCode|null
     */
    public function getByCodeAndNumber($code, $number)
    {
        $smsCode = $this->entityManager
            ->getRepository('RDWSmsBundle:SmsCode')
            ->findOneBy(['code' => $code, 'number' => $number]);

        if ( ! $smsCode) {
            $this->logger->info(sprintf('SmsCode was not found (by code = %s)', $code));
        }

        return $smsCode;
    }
}
