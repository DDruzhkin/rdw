<?php

namespace RDW\Bundle\SmsBundle\Tests\Repository;

use RDW\Bundle\SmsBundle\Repository\SmsCodeRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SmsCodeRepositoryTest extends WebTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var SmsCodeRepository
     */
    private $repository;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $client = static::createClient();
        $this->container = $client->getContainer();
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->repository = $this->em->getRepository('RDWSmsBundle:SmsCode');
    }

    /**
     * @test
     */
    public function it_should_add_valid_prefix_to_sms_code_text()
    {
        $prefix = 'RDW';
        $length = 6;

        $smsCodeText = $this->repository->generateSmsCodeText($length);

        $this->assertEquals(strlen($prefix) + 1 + $length, strlen($smsCodeText));
        $this->assertEquals($prefix, substr($smsCodeText, 0, 3));
    }
}
