<?php

namespace RDW\Bundle\SmsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class RDWSmsBundle
 *
 * @package RDW\Bundle\SmsBundle
 */
class RDWSmsBundle extends Bundle
{
}
