<?php

namespace RDW\Bundle\SmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Main order entity class
 *
 * @ORM\Entity(repositoryClass="RDW\Bundle\SmsBundle\Repository\SmsCodeRepository")
 * @ORM\Table(name="sms_codes", uniqueConstraints={@ORM\UniqueConstraint(name="sms_code_unique", columns={"code"})})
 */
class SmsCode
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string")
     */
    private $code;

    /**
     * @var int
     *
     * @ORM\Column(name="number", type="integer")
     */
    private $number;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param int $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }
}
