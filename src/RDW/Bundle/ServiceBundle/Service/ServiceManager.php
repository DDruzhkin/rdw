<?php

namespace RDW\Bundle\ServiceBundle\Service;

use RDW\Bundle\ServiceBundle\Entity\Service;
use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\ServiceBundle\Repository\ServiceRepository;

/**
 * Class ServiceManager
 *
 * @package RDW\Bundle\ServiceBundle\Service
 */
class ServiceManager
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $entityManager;

    /**
     * @var ServiceRepository
     */
    private $serviceRepository;

    /**
     * @param ObjectManager     $objectManager
     * @param ServiceRepository $serviceRepository
     */
    public function __construct(
        ObjectManager $objectManager,
        ServiceRepository $serviceRepository
    )
    {
        $this->entityManager     = $objectManager;
        $this->serviceRepository = $serviceRepository;

    }

    /**
     * @param Service $service
     *
     * @return bool
     */
    public function update(Service $service)
    {
        $this->entityManager->persist($service);

        $this->entityManager->flush();

        return true;
    }

    /**
     * @param array $services
     *
     * @return bool
     */
    public function updateServices(array $services)
    {
        foreach ($services as $id => $service) {
            $existingService = $this->serviceRepository->find($id);

            if ($existingService instanceof Service) {
                $existingService->setTitle($service['title']);
                $existingService->setPrice($service['price']);

                $this->entityManager->persist($existingService);
            }
        }

        $this->entityManager->flush();

        return true;
    }

    /**
     * @param Service $service
     * @param int     $position
     *
     * @return bool
     */
    public function changePosition(Service $service, $position)
    {
        $lastPosition = $this->serviceRepository->getLastPosition();
        $position = ($lastPosition >= $position) ? ($position >= 0) ? $position : 0 : $lastPosition;

        $service->setPosition($position);
        $this->update($service);

        return true;
    }

    /**
     * @param string $isFor
     *
     * @return array|\RDW\Bundle\ServiceBundle\Entity\Service[]
     */
    public function getServicesAllowedToOrderFromList($isFor)
    {
        return $this
            ->entityManager
            ->getRepository('RDWServiceBundle:Service')
            ->findBy(['isFor' => $isFor, 'allowToOrderFormList' => true]);
    }
}
