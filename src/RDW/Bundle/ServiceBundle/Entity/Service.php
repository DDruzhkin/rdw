<?php

namespace RDW\Bundle\ServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\ManageBundle\Entity\SortableInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class Wallet
 *
 * @package RDW\Bundle\ServiceBundle\Entity
 *
 * @ORM\Entity(repositoryClass="RDW\Bundle\ServiceBundle\Repository\ServiceRepository")
 * @ORM\Table(name="services")
 */
class Service implements SortableInterface
{
    const PERIOD = 7;

    const FOR_EMPLOYER = 1;
    const FOR_EMPLOYEE = 2;

    const TYPE_CAROUSEL         = 1;
    const TYPE_VIP              = 2;
    const TYPE_SEND_EMAIL       = 3;
    const TYPE_HIGHLIGHT        = 4;
    const TYPE_SHOW_IN_TOP      = 5;
    const TYPE_VIP_JOB          = 7;
    const TYPE_JOB_PUBLISHING   = 8;
    const TYPE_MONEY_TO_BALANCE = 9;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Title cannot be empty")
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", precision=16, scale=2)
     *
     * @Assert\NotBlank(message="Price cannot be empty")
     * @Assert\GreaterThan(
     *     value = -1,
     *     message="Please enter valid price"
     * )
     */
    protected $price;

    /**
     * @var int
     *
     * @Gedmo\SortableGroup
     * @ORM\Column(type="integer", name="is_for", nullable=false)
     * @Assert\NotBlank(message="Choose service for")
     */
    protected $isFor;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", name="is_personal", nullable=false)
     */
    protected $isPersonal;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", name="is_single", nullable=false)
     */
    protected $isSingle;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", name="allow_to_order_form_list", nullable=false)
     */
    protected $allowToOrderFormList;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", name="is_sale", nullable=false)
     */
    protected $isSale;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true, name="sale_text")
     */
    protected $saleText;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="type", nullable=true)
     */
    protected $type;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="sms_number", nullable=true)
     */
    protected $smsNumber;

    /**
     * @var integer
     *
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    protected $position;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_visible", type="boolean")
     */
    protected $visible;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->isFor = self::FOR_EMPLOYER;
        $this->isSingle = false;
        $this->isSale = false;
        $this->isPersonal = false;
        $this->allowToOrderFormList = false;
        $this->position = 0;
        $this->visible = false;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param int $price
     *
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $duration
     *
     * @return int
     */
    public function calcPrice($duration = 7)
    {
        return $this->getPrice() * ceil($duration / self::PERIOD);
    }

    /**
     * @return int
     */
    public function getIsFor()
    {
        return $this->isFor;
    }

    /**
     * @param int $isFor
     *
     * @return $this
     */
    public function setIsFor($isFor)
    {
        $this->isFor = $isFor;

        return $this;
    }

    /**
     * @return array
     */
    public static function getIsForChoicesList()
    {
        return [
            self::FOR_EMPLOYER => 'For employer',
            self::FOR_EMPLOYEE => 'For employee',
        ];
    }

    /**
     * @return bool
     */
    public function getIsPersonal()
    {
        return $this->isPersonal;
    }

    /**
     * @param bool $isPersonal
     *
     * @return $this
     */
    public function setIsPersonal($isPersonal)
    {
        $this->isPersonal = $isPersonal;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsSingle()
    {
        return $this->isSingle;
    }

    /**
     * @param boolean $isSingle
     *
     * @return $this
     */
    public function setIsSingle($isSingle)
    {
        $this->isSingle = $isSingle;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsSale()
    {
        return $this->isSale;
    }

    /**
     * @param boolean $isSale
     *
     * @return $this
     */
    public function setIsSale($isSale)
    {
        $this->isSale = $isSale;

        return $this;
    }

    /**
     * @return string
     */
    public function getSaleText()
    {
        return $this->saleText;
    }

    /**
     * @param string $saleText
     *
     * @return $this
     */
    public function setSaleText($saleText)
    {
        $this->saleText = $saleText;

        return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getAllowToOrderFormList()
    {
        return $this->allowToOrderFormList;
    }

    /**
     * @param boolean $allowToOrderFormList
     *
     * @return $this
     */
    public function setAllowToOrderFormList($allowToOrderFormList)
    {
        $this->allowToOrderFormList = $allowToOrderFormList;

        return $this;
    }

    /**
     * @return int
     */
    public function getSmsNumber()
    {
        return $this->smsNumber;
    }

    /**
     * @param int $smsNumber
     */
    public function setSmsNumber($smsNumber)
    {
        $this->smsNumber = $smsNumber;
    }

    /**
     * @return bool
     */
    public function isForEmployee()
    {
        return self::FOR_EMPLOYEE == $this->getIsFor();
    }

    /**
     * @return bool
     */
    public function isForEmployer()
    {
        return self::FOR_EMPLOYER == $this->getIsFor();
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     *
     * @return $this
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isVisible()
    {
        return $this->visible;
    }

    /**
     * @param boolean $visible
     *
     * @return $this
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * @return bool
     */
    public function isJobPublishing()
    {
        return (self::TYPE_JOB_PUBLISHING == $this->getType());
    }

    /**
     * @return bool
     */
    public function isMoneyToBalance()
    {
        return (self::TYPE_MONEY_TO_BALANCE == $this->getType());
    }

    /**
     * Get visible
     *
     * @return boolean 
     */
    public function getVisible()
    {
        return $this->visible;
    }
}
