<?php

namespace RDW\Bundle\ServiceBundle\Entity;

/**
 * Interface TopItemInterface
 *
 * @package RDW\Bundle\ServiceBundle\Entity
 */
interface TopItemInterface
{
    /**
     * @return boolean
     */
    public function isTop();

    /**
     * @param boolean $top
     *
     * @return $this
     */
    public function setTop($top);
}
