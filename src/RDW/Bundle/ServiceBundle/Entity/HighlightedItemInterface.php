<?php

namespace RDW\Bundle\ServiceBundle\Entity;

/**
 * Interface HighlightedItemInterface
 *
 * @package RDW\Bundle\ServiceBundle\Entity
 */
interface HighlightedItemInterface
{
    /**
     * @return boolean
     */
    public function isHighlighted();

    /**
     * @param boolean $highlighted
     *
     * @return $this
     */
    public function setHighlighted($highlighted);
}
