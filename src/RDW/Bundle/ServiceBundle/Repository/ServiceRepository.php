<?php

namespace RDW\Bundle\ServiceBundle\Repository;

use Doctrine\ORM\EntityRepository;
use RDW\Bundle\ServiceBundle\Entity\Service;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;

/**
 * Class ServiceRepository
 *
 * @package RDW\Bundle\ServiceBundle\Repository
 */
class ServiceRepository extends EntityRepository
{
    /**
     * @return int
     */
    public function getLastPosition()
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        return $qb
            ->select('s.position')
            ->from('RDWServiceBundle:Service', 's')
            ->orderBy('s.position', 'DESC')
            ->setMaxResults(1)
            ->getQuery()->getSingleScalarResult();
    }

    /**
     * @param RegisteredUser $user
     *
     * @return null|Service
     */
    public function getBalanceAdditionService(RegisteredUser $user)
    {
        $isFor = ($user->isTypeEmployee()) ? Service::FOR_EMPLOYEE : Service::FOR_EMPLOYER;

        return $this->findOneBy(['isFor' => $isFor, 'type' => Service::TYPE_MONEY_TO_BALANCE]);
    }
}
