<?php

namespace RDW\Bundle\ServiceBundle\Twig;

use RDW\Bundle\OrderBundle\Entity\OrderItem;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ServiceExtension
 *
 * @package RDW\Bundle\ServiceBundle\Twig
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class ServiceExtension extends \Twig_Extension implements ContainerAwareInterface
{
    /**
     * Container
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('find_active_service_for_item', [$this, 'findActiveServiceForItem']),
            new \Twig_SimpleFunction('find_pending_service_for_item', [$this, 'findPendingServiceForItem']),
            new \Twig_SimpleFunction('find_pending_personal_service', [$this, 'findPendingPersonalService']),
            new \Twig_SimpleFunction('find_active_personal_service', [$this, 'findActivePersonalService']),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('validTill', [$this, 'getValidTillDate']),
        ];
    }

    /**
     * @param OrderItem $orderItem
     *
     * @return null|string
     */
    public function getValidTillDate(OrderItem $orderItem)
    {
        $service = $orderItem->getService();

        if ($service->getIsSingle()) {
            return $this->container->get('translator')->trans('Activated');
        } else {
            return ($orderItem->getValidTill() ? $orderItem->getValidTill()->format('d.m.Y') : null)    ;
        }
    }

    /**
     * @param int            $serviceId
     * @param int            $object
     * @param RegisteredUser $user
     *
     * @return bool
     */
    public function findActiveServiceForItem($serviceId, $object, RegisteredUser $user)
    {
        if (! $user) {
            return false;
        }

        $activeService = $this
            ->container
            ->get('rdw_order.order.repository')
            ->findActiveServiceForObjectByUser($serviceId, $object, $user);

        if ($activeService) {
            return $activeService;
        }

        return false;
    }

    /**
     * @param int            $serviceId
     * @param RegisteredUser $user
     *
     * @return bool
     */
    public function findActivePersonalService($serviceId, RegisteredUser $user)
    {
        if ($user) {
            $activeService = $this
                ->container
                ->get('rdw_order.order.repository')
                ->findActivePersonalService($serviceId, $user);

            if ($activeService) {
                return $activeService;
            }
        }

        return false;
    }

    /**
     * @param int            $serviceId
     * @param int            $objectId
     * @param RegisteredUser $user
     *
     * @return bool
     */
    public function findPendingServiceForItem($serviceId, $objectId, RegisteredUser $user)
    {
        if ($user) {
            $pendingService = $this
                ->container
                ->get('rdw_order.order.repository')
                ->findPendingServiceForObjectByUser($serviceId, $objectId, $user);

            if ($pendingService) {
                return $pendingService;
            }
        }

        return false;
    }

    /**
     * @param int            $serviceId
     * @param RegisteredUser $user
     *
     * @return bool
     */
    public function findPendingPersonalService($serviceId, RegisteredUser $user)
    {
        if ($user) {
            $pendingService = $this
                ->container
                ->get('rdw_order.order.repository')
                ->findPendingPersonalService($serviceId, $user);

            if ($pendingService) {
                return $pendingService;
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_service_extension';
    }
}
