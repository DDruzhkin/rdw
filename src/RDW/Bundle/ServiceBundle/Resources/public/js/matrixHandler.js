$(document).ready(function() {
    // then user checks to order service for all items
    $('[data-handler="selectAll"]').on('ifChanged', function() {
        var $this = $(this),
            serviceId = $this.data('id');

        if ($this.prop('checked')) {
            $('[ data-period-all-service-id="' + serviceId + '"]').removeAttr('disabled');
            var period = $this.parents('td').find('select').val();

            // set as checked all checkboxes and remove disabled from all period selects
            $('[data-service-id="' + serviceId + '"]').not('[data-active=""]').each(function(){
                var $itemCheckbox = $(this);
                $itemCheckbox.iCheck('check');

                if (period) {
                    $('[data-period-service-id="' + serviceId + '"]').each(function(){
                        var $periodDropdown = $(this);
                        $periodDropdown.removeAttr('disabled').val(period).trigger('change');
                    });
                }
            });
        } else {
            // set as checked all checkboxes and remove disabled from all period selects
            $('[data-service-id="' + serviceId + '"]').not('[data-active=""]').each(function(){
                $(this).iCheck('uncheck');
            });

            $('[data-period-service-id="' + serviceId + '"]').attr('disabled', 'disabled');
            $('[ data-period-all-service-id="' + serviceId + '"]').attr('disabled', 'disabled');
        }

        calcMatrixPrice();
    });

    $('[data-handler="extendService"]').on('click', function() {
        var itemId = $(this).data('item-id'),
            serviceId = $(this).data('service-id'),
            $checkbox = $('#service-select-' + serviceId + '-' + itemId);

        $('[data-type-active-' + serviceId + '-' + itemId + ']').remove();

        $checkbox.show();
        $checkbox.find("input:checkbox").iCheck('check');

        return false;
    });

    $('[data-handler="extendPersonalService"]').on('click', function() {
        var serviceId = $(this).data('service-id'),
            $select = $('#service-personal-select-' + serviceId);

        $select.show();
        $select.find("input:checkbox").removeAttr('data-active');
    });

    // when personal service is checked
    $('[data-handler="personalService"]').on('ifChanged', function() {
        var serviceId = $(this).data('service-id');
        if ($(this).prop('checked')) {
            $('[data-personal-id="'+ serviceId+'"]').removeAttr('disabled');
        } else {
            $('[data-personal-id="'+ serviceId+'"]').attr('disabled', 'disabled');
        }

        calcMatrixPrice();
    });

    calcMatrixPrice();

    // then user changes period for all items
    $('[data-handler="periodAll"]').on('change', function() {
        var serviceId = $(this).data('id');
        var selectedPeriod = $(this).val();
        $('[data-period-service-id="' + serviceId + '"]').val(selectedPeriod).trigger('change');

        calcMatrixPrice();
    });

    // then user changes one item period
    $('[data-handler="periodForService"]').on('change', function() {
        calcMatrixPrice();
    });

    // then user changes vip service period
    $('[data-handler="selectPersonalService"]').on('change', function() {
        calcMatrixPrice();
    });

    // then user checks one item
    $('[data-handler="selectService"]').on('ifChanged', function() {
        var serviceId = $(this).data('service-id');

        if ($(this).prop('checked')) {
            $(this).parents('td').find('select').removeAttr('disabled');
            $(this).parents('td').addClass('selected');
        } else {
            $(this).parents('td').find('select').attr('disabled', 'disabled');
            $(this).parents('td').removeClass('selected');
        }

        calcMatrixPrice();
    });

    // calc matrix table prices
    function calcMatrixPrice()
    {
        $('[data-total-price]').html(0);
        var totalPrice = 0;
        var prices = [];
        var periods = [];

        // job publishing service promo
        var hasJobPublishingServicePromo = $('[data-first-job-publishing-promo]').data('first-job-publishing-promo'),
            jobPublishingServiceId = $('[data-job-publishing="1"]').parent().find('input').data('id'),
            usedDiscount = false;

        $('input[data-handler="selectService"]' ).each(function(index) {
            var serviceId = $(this).data('service-id');
            var servicePrice = $('[data-service-price="' + serviceId + '"]').data('value');

            // first time set array key
            if( prices[serviceId] === undefined ) {
                prices[serviceId] = 0;
            }

            // first time set array key
            if( periods[serviceId] === undefined ) {
                periods[serviceId] = 0;
            }

            if ($(this).prop('checked')) {
                //count prices
                var period = parseFloat($(this).parents('td').find('select').val());

                if (isNaN(period)) {
                    var itemPrice =  parseFloat(servicePrice);
                    periods[serviceId] = parseFloat(parseFloat(periods[serviceId]) + 1);
                } else {
                    var itemPrice = parseFloat(servicePrice * (period / 7));
                    periods[serviceId] = parseFloat(parseFloat(periods[serviceId]) + period);
                }

                if (! usedDiscount && serviceId === jobPublishingServiceId && hasJobPublishingServicePromo) {
                    itemPrice = 0;
                    usedDiscount = true;
                }

                totalPrice += itemPrice;

                prices[serviceId] = parseFloat(prices[serviceId] + itemPrice).toFixed(2);
            }
        });

        // if Personal service selected
        $( '[data-handler="personalService"]').each(function(index) {
            var serviceId = $(this).data('service-id'),
                servicePrice = $( '[data-service-price="' + serviceId + '"]').data('value');

            // first time set array key
            if( prices[serviceId] === undefined ) {
                prices[serviceId] = 0;
            }

            // first time set array key
            if( periods[serviceId] === undefined ) {
                periods[serviceId] = 0;
            }

            if ($(this).prop('checked')) {
                //count prices
                var period = parseFloat($('[data-personal-id="' + serviceId + '"]').val());

                if (isNaN(period)) {
                    var itemPrice =  parseFloat(servicePrice);
                    periods[serviceId] = parseFloat(parseFloat(periods[serviceId]) + 1);
                } else {
                    var itemPrice = parseFloat(servicePrice * (period / 7));
                    periods[serviceId] = parseFloat(parseFloat(periods[serviceId]) + period);
                }

                totalPrice += itemPrice;

                prices[serviceId] = parseFloat(prices[serviceId] + itemPrice).toFixed(2);

            }
        });

        // update total price for each service
        $.each(prices, function(index, value){
            $( '[data-service-total-sum="' + index + '"]').find('.money').html(value);
        });

        // update total periods for each service
        $.each(periods, function(index, value){
            $( '[data-service-period-sum="' + index + '"]').html(value);
        });

        // update total order price
        $('[data-total-price]').html(totalPrice.toFixed(2));
        $.applyDataMask('.money');

    }

    $("[data-handler='submitMatrixForm']").on('click', function(event) {
        var $this = $(this),
            $form = $('#' + $this.data('form'))
            modal_content = '#modal_content',
            modal_container = '#modal_container';

        $.ajax($this.data('validate-url'), {
            data: $form.serialize(),
            dataType: "json"
        }).done(function(response) {
            if (response.success) {
                $form.submit();
            } else {
                $(modal_content).find('.modal-body-content').html(response.error);
                $(modal_content).find('.modal-body-content').show();
                $(modal_content).find('.modal-footer').hide();

                $(modal_container).html($(modal_content).show()).modal();
            }
        });

        return false;
    });
});
