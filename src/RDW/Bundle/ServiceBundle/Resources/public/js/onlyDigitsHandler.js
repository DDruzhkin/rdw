$(document).ready(function() {
    $('[data-handler="onlyDigits"]').on('keypress', function(event) {
        var charCode = (event.which) ? event.which : event.keyCode;

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }

        return true;
    });
});
