<?php

namespace RDW\Bundle\ServiceBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\OrderBundle\Entity\Order;
use RDW\Bundle\OrderBundle\Entity\OrderItem;
use RDW\Bundle\OrderBundle\Service\OrderManager;
use RDW\Bundle\ServiceBundle\Entity\Service;
use RDW\Bundle\ServiceBundle\Repository\ServiceRepository;
use RDW\Bundle\AppBundle\Service\JobPublishingService;

/**
 * Class MatrixServicesHandler
 *
 * @package RDW\Bundle\ServiceBundle\Handler
 */
class MatrixServicesHandler
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $objectManager;

    /**
     * @var OrderManager
     */
    private $orderManager;

    /**
     * @var \RDW\Bundle\ServiceBundle\Repository\ServiceRepository
     */
    protected $serviceRepository;

    /**
     * @var \RDW\Bundle\AppBundle\Service\JobPublishingService
     */
    protected $jobPublishingService;

    /**
     * @param ObjectManager        $objectManager
     * @param OrderManager         $orderManager
     * @param ServiceRepository    $serviceRepository
     * @param JobPublishingService $jobPublishingService
     */
    public function __construct(
        ObjectManager $objectManager,
        OrderManager $orderManager,
        ServiceRepository $serviceRepository,
        JobPublishingService $jobPublishingService
    )
    {
        $this->objectManager     = $objectManager;
        $this->orderManager      = $orderManager;
        $this->serviceRepository = $serviceRepository;
        $this->jobPublishingService = $jobPublishingService;
    }

    /**
     * @param array  $selectedServices
     * @param Order  $order
     * @param string $type
     *
     * @return Order
     */
    public function handleSelectedServices($selectedServices, Order $order, $type)
    {
        $user = $order->getUser();
        $hasUsedFirstJobPromo = $user->hasUsedFirstJobPromo();

        foreach ($selectedServices as $itemId => $services) {
            if ($type == OrderItem::TYPE_CV) {
                $item = $this->objectManager->getRepository('RDWCvBundle:Cv')->find($itemId);
            } else {
                $item = $this->objectManager->getRepository('RDWJobBundle:Job')->find($itemId);
            }

            foreach ($services as $serviceId => $options) {
                if (empty($options['is_checked'])) {
                    continue;
                }
                $period = isset($options['period']) ? $options['period'] : 1;

                // find service object
                $service = $this->serviceRepository->find($serviceId);

                if ($service instanceof Service && $period > 0) {
                    $orderItem = $this->orderManager->addNewOrderItem($order, $type);
                    $orderItem->setItem($item);
                    $orderItem->setService($service);
                    $orderItem->setTitle($service->getTitle());
                    $orderItem->setPeriod($period);
                    $orderItem->setPrice($service->calcPrice($period));

                    // set first job publishing service price 0 (free)
                    if ($item instanceof Job && ! $hasUsedFirstJobPromo && $service->isJobPublishing()) {
                        $orderItem->setPrice(0);
                        $hasUsedFirstJobPromo = true;
                        $user->setUsedFirstJobPromo(true);

                        $this->jobPublishingService->publish($item);
                    }
                }
            }
        }

        return $order;
    }

    /**
     * @param array  $services
     * @param Order  $order
     * @param string $type
     *
     * @return Order
     */
    public function handlePersonalServices($services, Order $order, $type)
    {
        foreach ($services as $serviceId => $options) {
            if (empty($options['is_checked'])) {
                continue;
            }

            $period = isset($options['period']) ? $options['period'] : 1;

            // find service object
            $service = $this->serviceRepository->find($serviceId);

            //if VIP service
            if ($service instanceof Service && $service->getIsPersonal()) {
                $orderItem = $this->orderManager->addNewOrderItem($order, $type);

                $orderItem->setService($service);
                $orderItem->setTitle($service->getTitle());
                $orderItem->setPeriod($period);
                $orderItem->setPrice($service->calcPrice($period));
            }
        }

        return $order;
    }

    /**
     * @param array $selectedServices
     *
     * @return bool
     */
    public function isJobServicesValid(array $selectedServices)
    {
        foreach ($selectedServices as $itemId => $services) {
            /** @var Job $item */
            $item = $this->objectManager->getRepository('RDWJobBundle:Job')->find($itemId);

            // something wrong, job not found
            if (! $item instanceof Job) {
                return false;
            }

            // job already have active publishing service, check next one
            if ($item->hasActivePublishingService()) {
                continue;
            }

            // one of jobs have no publishing service, return false
            if (! $this->hasPublishingService($services)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param array $services
     *
     * @return bool
     */
    public function hasPublishingService(array $services)
    {
        foreach ($services as $serviceId => $options) {
            $service = $this->serviceRepository->find($serviceId);

            // something wrong, service not found
            if (! $service instanceof Service) {
                return false;
            }

            if (Service::TYPE_JOB_PUBLISHING == $service->getType()
                && ! empty($options['is_checked'])
                && ! empty($options['period'])
            ) {
                return true;
            }
        }

        return false;
    }
}
