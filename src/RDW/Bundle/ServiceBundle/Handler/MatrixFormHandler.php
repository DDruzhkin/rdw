<?php

namespace RDW\Bundle\ServiceBundle\Handler;

use Braincrafted\Bundle\BootstrapBundle\Session\FlashMessage;
use RDW\Bundle\OrderBundle\Entity\Order;
use RDW\Bundle\OrderBundle\Entity\OrderItem;
use RDW\Bundle\OrderBundle\Service\OrderManager;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;

/**
 * Class MatrixFormHandler
 *
 * @package RDW\Bundle\ServiceBundle\Handler
 */
class MatrixFormHandler
{
    /**
     * @var OrderManager
     */
    private $orderManager;

    /**
     * @var MatrixServicesHandler
     */
    private $matrixServicesHandler;

    /**
     * @var \Braincrafted\Bundle\BootstrapBundle\Session\FlashMessage
     */
    private $flashMessage;

    /**
     * @param OrderManager          $orderManager
     * @param MatrixServicesHandler $matrixServicesHandler
     * @param FlashMessage          $flashMessage
     */
    public function __construct(
        OrderManager $orderManager,
        MatrixServicesHandler $matrixServicesHandler,
        FlashMessage $flashMessage
    )
    {
        $this->orderManager          = $orderManager;
        $this->matrixServicesHandler = $matrixServicesHandler;
        $this->flashMessage          = $flashMessage;
    }

    /**
     * @param array          $selectedServices
     * @param array          $personalServices
     * @param RegisteredUser $user
     * @param string         $type
     *
     * @return null|Order
     */
    public function handleForm($selectedServices, $personalServices, RegisteredUser $user, $type)
    {
        if (empty($selectedServices) && empty($personalServices)) {
            $this->flashMessage->error('Please select at least one service');

            return null;
        }

        if (OrderItem::TYPE_JOB == $type
            && ! empty($selectedServices)
            && ! $this->matrixServicesHandler->isJobServicesValid($selectedServices)
        ) {
            $this->flashMessage->error('One of selected jobs do not have publishing service');

            return null;
        }

        $order = $this->orderManager->createNewOrderForUser($user, Order::TYPE_CALCULATOR);

        // add vip services
        if (! empty($personalServices)) {
            $this->matrixServicesHandler->handlePersonalServices($personalServices, $order, $type);
        }

        // add services
        if (! empty($selectedServices)) {
            $this->matrixServicesHandler->handleSelectedServices($selectedServices, $order, $type);
        }

        // update total
        $this->orderManager->updateTotal($order);

        return $order;
    }
}
