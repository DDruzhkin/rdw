<?php
/**
 * Created by PhpStorm.
 * User: vb
 * Date: 1/25/15
 * Time: 3:47 PM
 */

namespace RDW\Bundle\ServiceBundle\Tests\Handler;


use RDW\Bundle\OrderBundle\Entity\OrderItem;
use RDW\Bundle\ServiceBundle\Handler\MatrixServicesHandler;

class MatrixServicesHandlerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $repo;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $em;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $orderManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $orderItemCvRepo;

    /**
     * @var MatrixServicesHandler
     */
    private $matrixServicesHandler;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $order;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $jobPublishingService;

    public function setUp()
    {
        $this->em = $this->getMockBuilder('Doctrine\Common\Persistence\ObjectManager')->getMock();
        $this->order = $this->getMock('RDW\Bundle\OrderBundle\Entity\Order');
        $this->orderManager = $this
            ->getMockBuilder('RDW\Bundle\OrderBundle\Service\OrderManager')
            ->disableOriginalConstructor()
            ->getMock();

        $this->repo = $this
            ->getMockBuilder('RDW\Bundle\ServiceBundle\Repository\ServiceRepository')
            ->disableOriginalConstructor()
            ->getMock();

        $this->orderItemCvRepo = $this->getMockBuilder('Doctrine\Common\Persistence\ObjectRepository')->disableOriginalConstructor()->getMock();

        $this->em->expects($this->any())
            ->method('getRepository')
            ->with('RDWCvBundle:Cv')
            ->will($this->returnValue($this->orderItemCvRepo));

        $this->jobPublishingService = $this->getMockBuilder('RDW\Bundle\AppBundle\Service\JobPublishingService')->disableOriginalConstructor()->getMock();

        $this->matrixServicesHandler = new MatrixServicesHandler($this->em, $this->orderManager, $this->repo, $this->jobPublishingService);
    }

    /**
     * @test
     */
    public function its_should_handle_personal_services()
    {
        $selectedServices = [
            93 => [
                1 => ['period' => 7, 'is_checked' => true]
            ]
        ];
        $this->assertEquals($this->order, $this->matrixServicesHandler->handlePersonalServices($selectedServices, $this->order, OrderItem::TYPE_JOB));
    }
}
