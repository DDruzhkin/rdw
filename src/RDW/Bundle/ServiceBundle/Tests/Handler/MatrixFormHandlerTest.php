<?php

namespace RDW\Bundle\ServiceBundle\Tests\Handler;

use RDW\Bundle\OrderBundle\Entity\Order;
use RDW\Bundle\OrderBundle\Entity\OrderItem;
use RDW\Bundle\ServiceBundle\Entity\Service;
use RDW\Bundle\ServiceBundle\Handler\MatrixFormHandler;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class MatrixFormHandlerTest extends WebTestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $orderManager;

    /**
     * @var MatrixFormHandler
     */
    private $handler;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $user;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $order;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $event;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $matrixServiceHandler;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $flashMessage;

    /**
     * set up
     */
    public function setUp()
    {
        parent::setUp();

        $this->orderManager = $this
            ->getMockBuilder('RDW\Bundle\OrderBundle\Service\OrderManager')
            ->disableOriginalConstructor()
            ->getMock();

        $this->matrixServiceHandler = $this
            ->getMockBuilder('RDW\Bundle\ServiceBundle\Handler\MatrixServicesHandler')
            ->disableOriginalConstructor()
            ->getMock();

        $this->flashMessage = $this
            ->getMockBuilder('Braincrafted\Bundle\BootstrapBundle\Session\FlashMessage')
            ->disableOriginalConstructor()
            ->getMock();

        $this->request = new Request();
        $this->request->headers->set('referer', 'http://rdw.by');

        $this->user = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');
        $this->order = $this->getMock('RDW\Bundle\OrderBundle\Entity\Order');
        $this->event = $this
            ->getMockBuilder('RDW\Bundle\OrderBundle\Event\OrderEvent')
            ->disableOriginalConstructor()
            ->getMock();

        $this->user->expects($this->any())
            ->method('getId')
            ->willReturn(5);

        $this->handler = new MatrixFormHandler(
            $this->orderManager,
            $this->matrixServiceHandler,
            $this->flashMessage
        );
    }

    /**
     * @test
     */
    function it_should_set_error_message_and_return_null_if_no_services_selected()
    {
        $selectedServices = [];
        $personalServices = [];

        $this->flashMessage
            ->expects($this->once())
            ->method('error')
            ->with($this->equalTo('Please select at least one service'));

        $result = $this->handler->handleForm($selectedServices, $personalServices, $this->user, OrderItem::TYPE_CV);

        $this->assertNull($result);
    }

    /**
     * @test
     */
    function it_should_create_new_order_with_items()
    {
        $selectedServices = [
            93 => [
                2 => ['period' => 7, 'is_checked' => true],
            ],
        ];

        $personalServices = [];

        $this->orderManager
            ->expects($this->once())
            ->method('createNewOrderForUser')
            ->with($this->user, Order::TYPE_CALCULATOR)
            ->willReturn($this->order);

        $result = $this->handler->handleForm($selectedServices, $personalServices, $this->user, OrderItem::TYPE_CV);

        $this->assertInstanceOf('RDW\Bundle\OrderBundle\Entity\Order', $result);
    }

    /**
     * @test
     */
    public function it_should_not_allow_to_order_services_for_vacancy_without_publishing_service()
    {
        // @todo refactor test
        $client = static::createClient();
        $em = $client->getContainer()->get('doctrine.orm.entity_manager');

        $orderManager = $client->getContainer()->get('rdw_order.service.order_manager');
        $servicesHandler = $client->getContainer()->get('rdw_service.handler.matrix_services');
        $flashMessage = $client->getContainer()->get('braincrafted_bootstrap.flash');

        $sut = new MatrixFormHandler($orderManager, $servicesHandler, $flashMessage);

        $user = $em->getRepository('RDWUserBundle:RegisteredUser')->findOneBy(['type' => RegisteredUser::USER_TYPE_EMPLOYER]);
        $job = $user->getJobs()->first();

        $services = $em->getRepository('RDWServiceBundle:Service')->findBy(['isFor' => Service::FOR_EMPLOYER]);

        $itemServicesData = [];

        foreach ($services as $service) {
            if (Service::TYPE_JOB_PUBLISHING != $service->getType()) {
                $itemServicesData[$service->getId()] = ['period' => 7, 'is_checked' => true];
            }
        }

        $selectedServices = [$job->getId() => $itemServicesData];
        $personalServices = [];

        $result = $sut->handleForm($selectedServices, $personalServices, $user, OrderItem::TYPE_JOB);

        $this->assertNull($result);
    }
}
