<?php
/**
 * Created by PhpStorm.
 * User: vb
 * Date: 12/15/14
 * Time: 10:05 PM
 */

namespace RDW\Bundle\ServiceBundle\Tests\Service;


use RDW\Bundle\ServiceBundle\Service\ServiceManager;

class ServiceManagerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $em;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $repo;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $service;

    /**
     * @var ServiceManager
     */
    private $serviceManager;

    /**
     * set up
     */
    public function setUp()
    {
        $this->em = $this->getMockBuilder('Doctrine\Common\Persistence\ObjectManager')->getMock();
        $this->repo = $this->getMockBuilder('RDW\Bundle\ServiceBundle\Repository\ServiceRepository')->disableOriginalConstructor()->getMock();
        $this->service = $this->getMock('RDW\Bundle\ServiceBundle\Entity\Service');

        $this->serviceManager = new ServiceManager($this->em, $this->repo);
    }

    /**
     * @test
     */
    public function it_should_save_new_service()
    {
        $this->em
            ->expects($this->once())
            ->method('persist')
            ->with($this->equalTo($this->service));

        $this->em
            ->expects($this->once())
            ->method('flush');

        $this->assertTrue($this->serviceManager->update($this->service));
    }

    /**
     * @test
     */
    public function it_should_save_services()
    {
        $servicesToSave = [
            1 => ['title' => 'Title1', 'price' => 4000]
        ];

        $this->repo
            ->expects($this->exactly(1))
            ->method('find')
            ->willReturn($this->service);

        $this->service
            ->expects($this->once())
            ->method('setTitle')
            ->with($this->equalTo('Title1'));

        $this->service
            ->expects($this->once())
            ->method('setPrice')
            ->with($this->equalTo(4000));

        $this->em
            ->expects($this->once())
            ->method('persist')
            ->with($this->equalTo($this->service));

        $this->em
            ->expects($this->once())
            ->method('flush');

        $this->assertTrue($this->serviceManager->updateServices($servicesToSave));
    }
}
