<?php
/**
 * Created by PhpStorm.
 * User: vb
 * Date: 12/15/14
 * Time: 3:36 PM
 */

namespace RDW\Bundle\ServiceBundle\Tests\Entity;


use RDW\Bundle\ServiceBundle\Entity\Service;

class ServiceTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var Service
     */
    protected $service;

    public function setUp()
    {
        $this->service = new Service();
    }

    /**
     * @test
     */
    public function it_should_has_no_id_by_default()
    {
        $this->assertNull($this->service->getId());
    }

    /**
     * @test
     */
    public function it_should_set_is_for_properly()
    {
        $this->service->setIsFor(Service::FOR_EMPLOYER);
        $this->assertEquals(Service::FOR_EMPLOYER, $this->service->getIsFor());
    }

    /**
     * @test
     */
    public function it_should_set_price_properly()
    {
        $this->service->setPrice(1000);
        $this->assertEquals(1000, $this->service->getPrice());
    }

    /**
     * @test
     */
    public function it_should_set_title_properly()
    {
        $this->service->setTitle('Title');
        $this->assertEquals('Title', $this->service->getTitle());
    }

    /**
     * @test
     */
    public function it_should_not_allow_to_order_from_list()
    {
        $this->assertFalse($this->service->getAllowToOrderFormList());
    }

    /**
     * @test
     */
    public function it_should_set_allow_to_order_from_list_properly()
    {
        $this->service->setAllowToOrderFormList(true);
        $this->assertTrue($this->service->getAllowToOrderFormList());
    }

    /**
     * @test
     */
    function it_should_calc_price_by_default_period()
    {
        $this->service->setPrice(5);
        $this->assertEquals(5, $this->service->calcPrice());
    }

    /**
     * @test
     */
    function it_should_calc_price_by_period()
    {
        $this->service->setPrice(5);
        $this->assertEquals(10, $this->service->calcPrice(14));
    }

    /**
     * @test
     */
    function it_should_calc_price_by_rounded_up_period()
    {
        $this->service->setPrice(5);
        $this->assertEquals(10, $this->service->calcPrice(10));
    }

    /**
     * @test
     */
    function it_should_set_action_text_properly()
    {
        $this->service->setSaleText('Text');
        $this->assertEquals('Text', $this->service->getSaleText());
    }

    /**
     * @test
     */
    public function it_should_return_is_service_add_money_or_not()
    {
        $this->service->setType(Service::TYPE_MONEY_TO_BALANCE);
        $this->assertTrue($this->service->isMoneyToBalance());

        $this->service->setType(Service::TYPE_VIP);
        $this->assertFalse($this->service->isMoneyToBalance());
    }
}
