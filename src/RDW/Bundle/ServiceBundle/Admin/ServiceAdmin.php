<?php

namespace RDW\Bundle\ServiceBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ServiceAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('smsNumber', 'text', [
                'label' => 'SMS number',
                'required' => false,
                'attr' => ['placeholder' => 'SMS number placeholder'],
            ])
            ->add('isSale', 'checkbox', [
                'label' => 'Is sale',
                'required' => false,
            ])
            ->add('title', 'text', [
                'label' => 'Title',
                'required' => false,
                'attr' => ['placeholder' => 'Title placeholder'],
            ])
            ->add('price', 'money', [
                'required' => false,
                'label' => 'Price',
                'currency' => 'BYR',
            ])
            ->add('description', 'textarea', [
                'required' => false,
                'label' => 'Description',
                'attr' => ['placeholder' => 'Service description placeholder'],
            ])
            ->add('saleText', 'textarea', [
                'required' => false,
                'label' => 'Sale text',
                'attr' => ['placeholder' => 'Sale text placeholder'],
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        //$datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title')
            ->add('price')
        ;

        $listMapper->add('_action', 'actions', array('actions' => array(
            'edit'       => array(),
            'delete'     => array('label' => 'delete')
        )));
    }
}