<?php

namespace RDW\Bundle\ServiceBundle\Form\Type;

use RDW\Bundle\ServiceBundle\Entity\Service;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class ServiceFormType
 *
 * @package RDW\Bundle\UserBundle\Form\Type
 */
class ServiceFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('smsNumber', 'text', [
                'label' => 'SMS number',
                'required' => false,
                'attr' => ['placeholder' => 'SMS number placeholder'],
            ])
            ->add('isSale', 'checkbox', [
                'label' => 'Is sale',
                'required' => false,
            ])
            ->add('title', 'text', [
                'label' => 'Title',
                'required' => false,
                'attr' => ['placeholder' => 'Title placeholder'],
            ])
            ->add('price', 'money', [
                'required' => false,
                'label' => 'Price',
                'currency' => 'BYR',
            ])
            ->add('description', 'textarea', [
                'required' => false,
                'label' => 'Description',
                'attr' => ['placeholder' => 'Service description placeholder'],
            ])
            ->add('saleText', 'textarea', [
                'required' => false,
                'label' => 'Sale text',
                'attr' => ['placeholder' => 'Sale text placeholder'],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\ServiceBundle\Entity\Service',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_service';
    }
}
