<?php

namespace RDW\Bundle\ServiceBundle\Controller;

use RDW\Bundle\ServiceBundle\Entity\Service;
use RDW\Bundle\ServiceBundle\Form\Type\ServiceFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class ServiceAdminController
 *
 * @package RDW\Bundle\ServiceBundle\Controller
 *
 * @Route("/manage/service")
 */
class ServiceAdminController extends Controller
{
    /**
     * @Route("/list", name="rdw_service.service_admin.list")
     * @Method({"GET"})
     * @Template()
     *
     * @return Response|array
     */
    public function listAction()
    {
        $this->isGrantedLocal();

        $employeeServices = $this->getDoctrine()->getRepository('RDWServiceBundle:Service')->findBy(['isFor' => Service::FOR_EMPLOYEE, 'visible' => true], ['isFor' => 'ASC', 'position' => 'ASC']);
        $employerServices = $this->getDoctrine()->getRepository('RDWServiceBundle:Service')->findBy(['isFor' => Service::FOR_EMPLOYER, 'visible' => true], ['isFor' => 'ASC', 'position' => 'ASC']);

        return [
            'employeeServices' => $employeeServices,
            'employerServices' => $employerServices,
        ];
    }

    /**
     * @param Request $request
     * @param Service $service
     *
     * @Route("/edit/{id}", name="rdw_service.service_admin.edit", requirements={"id" = "\d+"})
     * @Template()
     * @Method({"GET", "POST"})
     *
     * @return Response|array
     */
    public function editAction(Request $request, Service $service)
    {
        $this->isGrantedLocal();

        $form = $this->createForm(new ServiceFormType(), $service);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->get('rdw_service.service.service_manager')->update($service);

            $this->get('braincrafted_bootstrap.flash')->success('You successfully updated service');

            return $this->redirect($this->get('router')->generate('rdw_service.service_admin.list'));
        }

        return [
            'form' => $form->createView(),
            'service' => $service,
        ];
    }

    /**
     * @param Request $request
     *
     * @Route("/confirm-save", name="rdw_service.service_admin.confirm_save")
     * @Method({"POST"})
     * @Template()
     *
     * @return Response|array
     */
    public function confirmSaveAction(Request $request)
    {
        $this->isGrantedLocal();

        $services = $request->request->get('rdw_services');

        //if confirm button pressed
        if ($request->request->get('confirm')) {
            $this->get('braincrafted_bootstrap.flash')->success('Services updated');
            $this->get('rdw_service.service.service_manager')->updateServices($services);

            return $this->redirect($this->get('router')->generate('rdw_service.service_admin.list'));
        }

        return [
            'services' => $services,
        ];
    }

    /**
     * @param Service $service
     * @param int     $position
     *
     * @Route("/position/{id}/{position}", name="rdw_service.service_admin.position")
     * @Method({"GET"})
     *
     * @return array|Response
     */
    public function positionAction(Service $service, $position)
    {
        $this->isGrantedLocal();

        if ($this->get('rdw_service.service.service_manager')->changePosition($service, $position)) {
            $this->get('braincrafted_bootstrap.flash')->success('Service position changed');
        }

        return $this->redirect($this->generateUrl('rdw_service.service_admin.list'));
    }

    /**
     * @throws AccessDeniedException
     */
    private function isGrantedLocal()
    {
        if (! $this->container->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }
    }
}
