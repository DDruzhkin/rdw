<?php

namespace RDW\Bundle\ServiceBundle\Controller;

use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\CvBundle\Form\Type\CvCollectionType;
use RDW\Bundle\CvBundle\Model\CvFilter;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Form\Type\JobCollectionType;
use RDW\Bundle\OrderBundle\Entity\Order;
use RDW\Bundle\OrderBundle\Entity\OrderItem;
use RDW\Bundle\OrderBundle\Event\OrderEvent;
use RDW\Bundle\OrderBundle\RDWOrderEvents;
use RDW\Bundle\ServiceBundle\Entity\Service;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class ServiceController
 *
 * @package RDW\Bundle\ServiceBundle\Controller
 *
 * @Route("/service")
 */
class ServiceController extends Controller
{
    /**
     * @param Request        $request
     * @param RegisteredUser $user
     *
     * @Route("/order/employee/{id}", name="rdw_service.service.matrix_employee", requirements={"id" = "\d+"})
     * @Method({"GET"})
     *
     * @return Response
     */
    public function matrixForEmployeeAction(Request $request, RegisteredUser $user)
    {
        if (! $this->isAllowed($user)) {
            throw new AccessDeniedException();
        }

        $services = $this->getDoctrine()->getRepository('RDWServiceBundle:Service')->findBy(['isFor' => Service::FOR_EMPLOYEE, 'visible' => true], ['position' => 'ASC']);

        $filter = new CvFilter();
        $filter->setStatuses([Cv::STATUS_ACTIVE]);
        $filter->setUser($user);
        $selectedService = $request->query->get('service');

        $ids = $request->query->get('cv_collection', ['id' => []])['id'];
        $form = $this->createForm(new CvCollectionType($ids), null, ['method' => 'get']);
        $form->handleRequest($request);
        $selectedCvs = ($form->isValid()) ? $form->get('id')->getData() : null;
        $filter->setOnlyItems($selectedCvs);

        $items = $this->get('rdw_cv.cv.repository')->getListQueryBuilder($filter)->getQuery()->getResult();

        return $this->render('RDWServiceBundle:Service:matrix_narrow.html.twig', [
            'items' => $items,
            'user' => $user,
            'services' => $services,
            'type' => OrderItem::TYPE_CV,
            'selectedService' => $selectedService,
        ]);
    }

    /**
     * @param Request        $request
     * @param RegisteredUser $user
     *
     * @Route("/order/employer/{id}", name="rdw_service.service.matrix_employer", requirements={"id" = "\d+"})
     * @Method({"GET"})
     *
     * @return Response
     */
    public function matrixForEmployerAction(Request $request, RegisteredUser $user)
    {
        if (! $this->isAllowed($user)) {
            throw new AccessDeniedException();
        }

        $selectedService = $request->query->get('service');

        $services = $this->getDoctrine()->getRepository('RDWServiceBundle:Service')->findBy(['isFor' => Service::FOR_EMPLOYER, 'isPersonal' => false, 'visible' => true], ['position' => 'ASC']);
        $personal = $this->getDoctrine()->getRepository('RDWServiceBundle:Service')->findBy(['isFor' => Service::FOR_EMPLOYER, 'isPersonal' => true, 'visible' => true], ['position' => 'ASC']);

        $jobPublishingServiceParams = ['isFor' => Service::FOR_EMPLOYER, 'isPersonal' => false, 'isSingle' => false, 'allowToOrderFormList' => true, 'type' => Service::TYPE_JOB_PUBLISHING];
        $jobPublishingService = $this->getDoctrine()->getRepository('RDWServiceBundle:Service')->findOneBy($jobPublishingServiceParams);

        $ids = $request->query->get('job_collection', ['id' => []])['id'];
        $form = $this->createForm(new JobCollectionType($ids), null, ['method' => 'get']);

        $form->handleRequest($request);

        $selectedJobs = ($form->isValid()) ? $form->get('id')->getData() : null;

        $criteria = ['statuses' => [Job::STATUS_ACTIVE, Job::STATUS_WAITING, Job::STATUS_PUBLISHED], 'only_items' => $selectedJobs];
        $items = $this->get('rdw_job.job.repository')->getUserJobsListQuery($user, $criteria)->getResult();

        return $this->render('RDWServiceBundle:Service:matrix.html.twig', [
            'items' => $items,
            'user' => $user,
            'services' => $services,
            'type' => OrderItem::TYPE_JOB,
            'personal' => $personal,
            'selectedService' => $selectedService,
            'jobPublishingService' => $jobPublishingService,
        ]);
    }

    /**
     * @param Request        $request
     * @param string         $type
     * @param RegisteredUser $user
     *
     * @Route(
     *      "/handle/{type}/{id}",
     *      name="rdw_service.service.handle",
     *      requirements={"type" = "cv|job", "id" = "\d+"}
     * )
     * @Method({"POST"})
     *
     * @return Response
     */
    public function handleAction(Request $request, $type, RegisteredUser $user)
    {
        if (! $this->isAllowed($user)) {
            throw new AccessDeniedException();
        }

        $selectedServices = $request->request->get('services');
        $personalServices = $request->request->get('personal_services');
        $order = $this
            ->get('rdw_service.handler.matrix_form')
            ->handleForm($selectedServices, $personalServices, $user, $type);

        if (! $order instanceof Order) {
            return new RedirectResponse($request->headers->get('referer'));
        }

        // dispatch events
        $event = new OrderEvent($order);
        $dispatcher = $this->get('event_dispatcher');
        $dispatcher->dispatch(RDWOrderEvents::CREATE_SUCCESS, $event);
        $dispatcher->dispatch(RDWOrderEvents::MATRIX_SUBMITTED_SUCCESS, $event);

        if ($event->getResponse() instanceof RedirectResponse) {
            return $event->getResponse();
        }

        return new RedirectResponse($this->get('router')->generate('rdw_order.order.submit', ['id' => $order->getId()]));
    }

    /**
     * @param Request        $request
     * @param RegisteredUser $user
     *
     * @Route("/validate-matrix/{id}", name="rdw_service.validate_matrix", requirements={"type" = "cv|job", "id" = "\d+"})
     * @Method({"GET"})
     * @Template()
     *
     * @return Response
     */
    public function validateMatrixAction(Request $request, RegisteredUser $user)
    {
        $data['success'] = false;
        $data['error'] = '';
        $response = new JsonResponse();

        if (! $this->isAllowed($user)) {
            $data['error'] = $this->get('translator')->trans('User is not allowed');
            $response->setData($data);

            return $response;
        }

        $selectedServices = $request->query->get('services');
        $selectedPersonalServices = $request->query->get('personal_services');

        if (empty($selectedServices) && empty($selectedPersonalServices)) {
            $data['error'] = $this->get('translator')->trans('Please select at least one service');
            $response->setData($data);

            return $response;
        }

        if (! empty($selectedServices)) {
            $isValid = $this->get('rdw_service.handler.matrix_services')->isJobServicesValid($selectedServices);

            if (!$isValid) {
                $data['error'] = $this->get('translator')->trans('One of selected jobs do not have publishing service');
                $response->setData($data);

                return $response;
            }
        }

        $data['success'] = true;
        $response->setData($data);

        return $response;
    }

    /**
     * @param OrderItem $orderItem
     *
     * @Route("/suspend/confirm/{id}", name="rdw_service.service.confirm_suspend", requirements={"id" = "\d+"})
     * @Method({"GET"})
     * @Template()
     *
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     *
     * @return Response
     */
    public function confirmSuspendAction(OrderItem $orderItem)
    {
        /** @var $user RegisteredUser */
        $user = $this->getUser();
        $order = $orderItem->getOrder();

        if (! $order->isOwner($user) && ! $order->getUser()->isUserManager($user)) {
            throw new AccessDeniedException();
        }

        return [
            'orderItem' => $orderItem,
        ];
    }

    /**
     * @param OrderItem $orderItem
     *
     * @Route("/suspend/{id}", name="rdw_service.service.suspend", requirements={"id" = "\d+"})
     * @Method({"GET"})
     *
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     *
     * @return RedirectResponse
     */
    public function suspendAction(OrderItem $orderItem)
    {
        /** @var $user RegisteredUser */
        $user = $this->getUser();
        $order = $orderItem->getOrder();

        if (! $order->isOwner($user) && ! $order->getUser()->isUserManager($user)) {
            throw new AccessDeniedException();
        }

        $this->get('rdw_order.service.order_item_manager')->suspend($orderItem);

        if ($user->isTypeEmployee()) {
            $url = $this->generateUrl('rdw_service.service.matrix_employee', ['id' => $user->getId()]);
        } else {
            $url = $this->generateUrl('rdw_service.service.matrix_employer', ['id' => $user->getId()]);
        }

        return $this->redirect($url);
    }

    /**
     * @param RegisteredUser $user
     *
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    private function isAllowed(RegisteredUser $user)
    {
        return ($user instanceof RegisteredUser || (
            $user->getId() != $this->getUser()->getId()
            && ! $user->isUserManager($this->getUser())
        ));
    }
}
