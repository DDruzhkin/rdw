<?php

namespace RDW\Bundle\ServiceBundle\Command;

use RDW\Bundle\ServiceBundle\Entity\HighlightedItemInterface;
use RDW\Bundle\ServiceBundle\Entity\TopItemInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\Output;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Stopwatch\Stopwatch;
use RDW\Bundle\ServiceBundle\Entity\Service;

/**
 * Class RemoveAttributeHighlightedCommand
 *
 * @package RDW\Bundle\ServiceBundle\Command
 */
class RemoveAdAttributesCommand extends ContainerAwareCommand
{
    private $em;
    private $output;

    /**
     * Command configuration
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('rdw:service:remove-expired')
            ->setDescription('Remove highlighted attribute from CVs/Jobs which have no valid services');
    }

    /**
     * Execute
     *
     * @param InputInterface  $input  Input interface
     * @param OutputInterface $output Output interface
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('default_socket_timeout', 600);
        ini_set('max_execution_time', 0);

        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->output = $output;

        $stopwatch = new Stopwatch();
        $stopwatch->start('remove_attributes');
        $this->output->writeln('Started...');
        $this->output->writeln('');

        $this->output->writeln('Removing highlighted ads...');
        $this->remove(Service::TYPE_HIGHLIGHT);

        $this->output->writeln('Removing top ads...');
        $this->remove(Service::TYPE_SHOW_IN_TOP);

        $event = $stopwatch->stop('remove_attributes');
        $this->output->writeln(sprintf('Executed in %s milliseconds', $event->getDuration()));
        $this->output->writeln('Finished!');
    }

    protected function remove($serviceType)
    {
        $items = array_merge(
            $this->em->getRepository('RDWCvBundle:Cv')->findAllExpiredWithAttributesByServiceType($serviceType),
            $this->em->getRepository('RDWJobBundle:Job')->findAllExpiredWithAttributesByServiceType($serviceType)
        );

        if (count($items) < 1) {
            $this->output->writeln('No expired items with highlight attribute was found');
            return;
        }

        $this->output->writeln(sprintf('Total will be removed: %d', count($items)));
        $this->output->writeln('');

        foreach ($items as $item) {
            if (Service::TYPE_HIGHLIGHT === $serviceType && $item instanceof HighlightedItemInterface) {
                $item->setHighlighted(false);
                $this->em->persist($item);
            }

            if (Service::TYPE_SHOW_IN_TOP === $serviceType && $item instanceof TopItemInterface) {
                $item->setTop(false);
                $this->em->persist($item);
            }
        }

        $this->em->flush();
    }
}
