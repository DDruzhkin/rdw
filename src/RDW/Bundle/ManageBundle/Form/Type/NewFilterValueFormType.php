<?php

namespace RDW\Bundle\ManageBundle\Form\Type;

use RDW\Bundle\AppBundle\Form\Type\ProfessionalAreaCollectionPopupType;
use RDW\Bundle\JobBundle\Entity\Position;
use RDW\Bundle\LocationBundle\Entity\City;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class NewFilterValueFormType
 *
 * @package RDW\Bundle\ManageBundle\Form\Type
 */
class NewFilterValueFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('save', SubmitType::class);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $entity = $event->getData();
            $form = $event->getForm();

            if ($entity instanceof City) {
                $form->add('titleWhere', TextType::class);
                $form->add('file', FileType::class, ['label' => 'Image', 'attr' => ['class' => 'image_city']]);
            }

            if ($entity instanceof Position) {
                $form->add('professionalArea', ProfessionalAreaCollectionPopupType::class);
            }
        });
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_manage_filter';
    }
}
