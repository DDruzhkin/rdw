<?php

namespace RDW\Bundle\ManageBundle\Form\Type;

use RDW\Bundle\ManageBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class UserFilterType
 *
 * @package RDW\Bundle\ManageBundle\Form\Type
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class UserFilterType extends AbstractType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('GET');

        $builder
            ->add('name', 'text', array(
                'label' => 'Name',
                'required' => false,
            ))
            ->add('surname', 'text', array(
                'label' => 'Surname',
                'required' => false,
            ))
            ->add('registeredAtFrom', 'date', array(
                'label' => 'Registered from',
                'input'  => 'datetime',
                'widget' => 'single_text',
                'required' => false,
                'format' => 'yyyy-MM-dd',
                'empty_value' => ''
            ))
            ->add('registeredAtTo', 'date', array(
                'label' => 'Registered to',
                'input'  => 'datetime',
                'widget' => 'single_text',
                'required' => false,
                'format' => 'yyyy-MM-dd',
                'empty_value' => ''
            ))
            ->add('type', 'choice', array(
                'label' => 'Type',
                'choices'   => User::getTypes(),
                'empty_value' => 'Type',
                'required' => false,
            ))
            ->add('status', 'choice', array(
                'label' => 'Status',
                'choices'   => User::getStatuses(),
                'empty_value' => 'Status',
                'required' => false,
            ))
            ->add('search', 'submit', array(
                'label' => 'Search'
            ));
    }

    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RDW\Bundle\ManageBundle\Model\UserFilter',
        ));
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'filter';
    }
}
