<?php

namespace RDW\Bundle\ManageBundle\Form\Type;

use RDW\Bundle\ManageBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class UserType
 *
 * @package RDW\Bundle\ManageBundle\Form\Type
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var User $user */
        $user = $builder->getForm()->getData();

        $builder
            ->add('username', 'email', array(
                'required' => true,
                'label' => 'Username',
            ))
            ->add('type', 'choice', array(
                'choices'   => User::getTypes(),
                'empty_value' => 'Choose a type',
                'required' => true,
                'label' => 'User type',
            ));

        $builder
            ->add('status', 'choice', array(
                'choices'   => User::getStatuses(),
                'required' => true,
                'label' => 'Status',
            ))
            ->add('name', 'text', array(
                'required' => true,
                'label' => 'Name',
            ))
            ->add('surname', 'text', array(
                'required' => false,
                'label' => 'Surname',
            ))
            ->add('phone', 'text', array(
                'required' => true,
                'label' => 'Phone',
            ))
            ->add('plainPassword', 'repeated', array(
                'type' => 'password',
                'invalid_message' => 'The password fields must match.',
                'required' => false,
                'first_options' => array('label' => 'Password'),
                'second_options' => array('label' => 'Repeat password'),
            ))
            ->add('search', 'submit', array(
                'label' => 'Save'
            ));
    }

    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RDW\Bundle\ManageBundle\Entity\User',
            'validation_groups' =>
                function(FormInterface $form) {

                    $groups = [];

                    if (null != $form->getData()->getId()) {
                        $groups[] = 'edit';

                    } else {
                        $groups[] = 'create';
                    }

                    return $groups;
                }
        ));
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'rdw_manage_user';
    }
}
