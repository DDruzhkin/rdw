<?php

namespace RDW\Bundle\ManageBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\ManageBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadUserData
 *
 * @package RDW\Bundle\ManageBundle\DataFixtures\ORM
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class LoadUserData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * Container
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $entityManager)
    {
        /** @var \Symfony\Component\Security\Core\Encoder\EncoderFactory $encoderFactory */
        $encoderFactory = $this->container->get('security.encoder_factory');

        for ($i = 1; $i <= 5; $i++) {
            $name = 'censor' . $i;

            $user = new User();
            $user->setUsername($name . '@example.com');
            $user->setName('Name' . $i);
            $user->setSurname('Surname' . $i);
            $user->setEmail($name . '@example.com');
            $user->addRole(User::ROLE_CENSOR);
            $user->setType(User::TYPE_CENSOR);
            $user->setStatus($this->getStatusByCounter($i));
            $user->setPhone('+37033310' . $i);

            $encoder = $encoderFactory->getEncoder($user);
            $user->setPassword($encoder->encodePassword('censorPass', $user->getSalt()));

            if (User::STATUS_BLOCKED == $user->getStatus()) {
                $user->setRoles(array(User::ROLE_BLOCKED));
            }

            $entityManager->persist($user);
        }

        for ($i = 1; $i <= 5; $i++) {
            $name = 'admin' . $i;

            $user = new User();
            $user->setUsername($name . '@example.com');
            $user->setName('Name' . $i);
            $user->setSurname('Surname' . $i);
            $user->setEmail($name . '@example.com');
            $user->addRole(User::ROLE_ADMIN);
            $user->setType(User::TYPE_ADMIN);
            $user->setStatus($this->getStatusByCounter($i));
            $user->setPhone('+370 333 30' . $i);

            $encoder = $encoderFactory->getEncoder($user);
            $user->setPassword($encoder->encodePassword('adminPass', $user->getSalt()));

            if ($user->isBlocked()) {
                $user->setRoles(array(User::ROLE_BLOCKED));
            }

            $entityManager->persist($user);
        }

        $entityManager->flush();
    }

    /**
     * @param int $counter
     *
     * @return string
     */
    private function getStatusByCounter($counter)
    {
        switch ($counter) {
            case 1:
                $status = User::STATUS_ACTIVE;
                break;
            case 2:
                $status = User::STATUS_INACTIVE;
                break;
            case 3:
                $status = User::STATUS_BLOCKED;
                break;
            default:
                $status = User::STATUS_DELETED;
        }

        return $status;
    }
}
