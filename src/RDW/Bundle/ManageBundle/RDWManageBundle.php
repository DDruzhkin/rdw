<?php

namespace RDW\Bundle\ManageBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class RDWManageBundle
 *
 * @package RDW\Bundle\ManageBundle
 */
class RDWManageBundle extends Bundle
{

}
