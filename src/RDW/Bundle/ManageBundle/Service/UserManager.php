<?php

namespace RDW\Bundle\ManageBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use Knp\Component\Pager\Paginator;
use RDW\Bundle\ManageBundle\Entity\User;
use RDW\Bundle\ManageBundle\Model\UserFilter;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;

/**
 * Class UserManager
 *
 * @package RDW\Bundle\ManageBundle\Service
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class UserManager
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $entityManager;

    /**
     * @var \Knp\Component\Pager\Paginator
     */
    private $paginator;

    /**
     * @var \Symfony\Component\Security\Core\Encoder\EncoderFactory
     */
    private $encoderFactory;

    /**
     * @param ObjectManager  $entityManager
     * @param Paginator      $paginator
     * @param EncoderFactory $encoderFactory
     */
    public function __construct(ObjectManager $entityManager, Paginator $paginator, EncoderFactory $encoderFactory)
    {
        $this->entityManager = $entityManager;
        $this->paginator = $paginator;
        $this->encoderFactory = $encoderFactory;
    }

    /**
     * @return User
     */
    public function create()
    {
        return new User();
    }

    /**
     * @param User $user
     */
    public function addNewUser(User $user)
    {
        $this->update($user);
    }

    /**
     * @param string $username
     *
     * @return \RDW\Bundle\ManageBundle\Entity\User
     */
    public function findByUsername($username)
    {
        return $this->getRepository()->findOneBy(array('username' => $username));
    }

    /**
     * @param UserFilter $filter
     * @param int        $page
     * @param int        $limit
     *
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function getPaginatedUserList(UserFilter $filter, $page, $limit)
    {
        $query = $this->getRepository()->getUserListQuery($filter);

        $pagination = $this->paginator->paginate($query, $page, $limit);

        return $pagination;
    }

    /**
     * @param int $id
     *
     * @return \RDW\Bundle\ManageBundle\Entity\User
     */
    public function findById($id)
    {
        return $this->getRepository()->find($id);
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function delete(User $user)
    {
        $user->setStatus(User::STATUS_DELETED);
        $this->update($user);

        return true;
    }

    /**
     * @param User $user
     * @param bool $andFlush
     *
     * @return bool
     */
    public function update(User $user, $andFlush = true)
    {
        $user->setEmail($user->getUsername());

        $this->updateRoles($user);
        $this->updatePassword($user);

        $this->entityManager->persist($user);

        if ($andFlush) {
            $this->entityManager->flush();
        }

        return true;
    }

    /**
     * @param User $user
     */
    private function updateRoles(User $user)
    {
        if (User::STATUS_BLOCKED == $user->getStatus()) {
            $user->setRoles([User::ROLE_BLOCKED]);
        } else {
            $user->removeRole(User::ROLE_BLOCKED);

            if (User::TYPE_ADMIN == $user->getType()) {
                $user->setRoles([User::ROLE_ADMIN]);
            } elseif (User::TYPE_CENSOR == $user->getType()) {
                $user->setRoles([User::ROLE_CENSOR]);
            }
        }
    }

    /**
     * @param User $user
     */
    private function updatePassword(User $user)
    {
        if (0 !== strlen($user->getPlainPassword())) {
            $user->setPassword($this->getEncodedPassword($user));

            $user->eraseCredentials();
        }
    }

    /**
     * @return array
     */
    public function getAllCensors()
    {
        $criteria = [
            'status' => User::STATUS_ACTIVE,
            'type' => User::TYPE_CENSOR,
        ];

        return $this->getRepository()->findBy($criteria);
    }

    /**
     * @param User   $user
     *
     * @return string
     */
    public function getEncodedPassword(User $user)
    {
        return $this->encoderFactory->getEncoder($user)->encodePassword($user->getPlainPassword(), $user->getSalt());
    }

    /**
     * @return \RDW\Bundle\ManageBundle\Repository\UserRepository
     */
    private function getRepository()
    {
        return $this->entityManager->getRepository('RDWManageBundle:User');
    }
}
