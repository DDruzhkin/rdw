<?php

namespace RDW\Bundle\ManageBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\AppBundle\Collection\FiltersCollection;
use RDW\Bundle\LocationBundle\Entity\City;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class FilterManager
 *
 * @package RDW\Bundle\ManageBundle\Service
 */
class FilterManager
{
    /**
     * @var array
     */
    private $repositoryClasses;

    /**
     * @var array
     */
    private $objectClasses;

    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $entityManager;

    /**
     * @param ObjectManager $entityManager
     */
    public function __construct(ObjectManager $entityManager)
    {
        $this->entityManager = $entityManager;

        $this->repositoryClasses = [
            'city' => 'RDWLocationBundle:City',
            'position' => 'RDWJobBundle:Position',
            'scope' => 'RDWUserBundle:Scope',
            'schedule' => 'RDWJobBundle:WorkScheduleType',
            'education_type' => 'RDWJobBundle:EducationType',
            'language' => 'RDWJobBundle:Language',
            'language_level' => 'RDWJobBundle:LanguageLevel',
            'marital' => 'RDWUserBundle:MaritalStatus',
            'driving_category' => 'RDWCvBundle:DriverLicenseCategory',
            'business_trip' => 'RDWCvBundle:BusinessTrip',
            'work_experience' => 'RDWJobBundle:WorkExperienceType',
            'company_type' => 'RDWUserBundle:CompanyType',
            'employment_type' => 'RDWJobBundle:EmploymentType',
        ];

        $this->objectClasses = [
            'city' => 'RDW\Bundle\LocationBundle\Entity\City',
            'position' => 'RDW\Bundle\JobBundle\Entity\Position',
            'scope' => 'RDW\Bundle\UserBundle\Entity\Scope',
            'schedule' => 'RDW\Bundle\JobBundle\Entity\WorkScheduleType',
            'education_type' => 'RDW\Bundle\JobBundle\Entity\EducationType',
            'language' => 'RDW\Bundle\JobBundle\Entity\Language',
            'language_level' => 'RDW\Bundle\JobBundle\Entity\LanguageLevel',
            'marital' => 'RDW\Bundle\UserBundle\Entity\MaritalStatus',
            'driving_category' => 'RDW\Bundle\CvBundle\Entity\DriverLicenseCategory',
            'business_trip' => 'RDW\Bundle\CvBundle\Entity\BusinessTrip',
            'company_type' => 'RDW\Bundle\UserBundle\Entity\CompanyType',
            'work_experience' => 'RDW\Bundle\JobBundle\Entity\WorkExperienceType',
            'employment_type' => 'RDW\Bundle\JobBundle\Entity\EmploymentType',
        ];
    }

    /**
     * @param string $type
     *
     * @return array
     * @throws \Exception
     */
    public function getFilterValues($type)
    {
        $className = $this->getDataClass($type);

        /** @var \RDW\Bundle\ManageBundle\Entity\Filter $class */
        $class = new $className();

        $orderBy = ($class->isSortable()) ? ['position' => 'ASC'] : [];
        $items = $this->entityManager->getRepository($this->getRepositoryClass($type))->findBy([], $orderBy);

        return new FiltersCollection($items, $class->isSortable());
    }

    /**
     * @param string $type
     * @param int    $id
     *
     * @return bool
     */
    public function deleteValue($type, $id)
    {
        $repositoryClass = $this->getRepositoryClass($type);
        $object = $this->entityManager->getRepository($repositoryClass)->find($id);

        if ($object && $object->isDeletable()) {
            $this->entityManager->remove($object);
            $this->entityManager->flush();

            return true;
        }

        return false;
    }

    /**
     * @param string  $type
     * @param integer $id
     *
     * @return FilterInterface
     */
    public function getFilterValue($type, $id)
    {
        $repositoryClass = $this->getRepositoryClass($type);
        $entity = $this->entityManager->getRepository($repositoryClass)->find($id);

        return $entity;
    }

    /**
     * @param string $type
     * @param int    $id
     * @param int    $position
     *
     * @return bool
     */
    public function changePosition($type, $id, $position)
    {
        $repositoryClass = $this->getRepositoryClass($type);
        $object = $this->entityManager->getRepository($repositoryClass)->find($id);

        if ($object) {
            $object->setPosition($position);

            $this->entityManager->persist($object);
            $this->entityManager->flush();

            return true;
        }

        return false;
    }

    /**
     * @param object $object
     *
     * @return true
     */
    public function saveValue($object)
    {
        if ($object instanceof City) {
            $object->preUpload();
            $object->upload();
        }
        $this->entityManager->persist($object);
        $this->entityManager->flush();

        return true;
    }

    /**
     * @param string $objectType
     *
     * @return string
     *
     * @throws \InvalidArgumentException
     */
    public function getDataClass($objectType)
    {
        if (!array_key_exists($objectType, $this->objectClasses)) {
            throw new \InvalidArgumentException(sprintf('The object type %s not exist', $objectType));
        }

        return $this->objectClasses[$objectType];
    }

    /**
     * @param string $objectType
     *
     * @return string
     *
     * @throws \InvalidArgumentException
     */
    private function getRepositoryClass($objectType)
    {
        if (! array_key_exists($objectType, $this->repositoryClasses)) {
            throw new \InvalidArgumentException(sprintf('The object type %s not exist', $objectType));
        }

        return $this->repositoryClasses[$objectType];
    }
}
