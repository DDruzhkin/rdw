<?php

namespace RDW\Bundle\ManageBundle\Tests\Service;

use RDW\Bundle\ManageBundle\Service\FilterManager;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Class FilterManagerTest
 *
 * @package RDW\Bundle\ManageBundle\Tests\Service
 */
class FilterManagerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $em;

    /**
     * @var array
     */
    private $repositoryClasses;

    /**
     * @var array
     */
    private $objectClasses;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $repo;

    /**
     * @var FilterManager
     */
    private $filterManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $object;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $request;

    /**
     * set up
     */
    public function setUp()
    {
        $this->em = $this->getMockBuilder('Doctrine\Common\Persistence\ObjectManager')->getMock();
        $this->object = $this->getMock('RDW\Bundle\LocationBundle\Entity\City');
        $this->repo = $this->getMock('\Doctrine\Common\Persistence\ObjectRepository');

        $this->repositoryClasses = [
            'city' => 'RDWLocationBundle:City',
            'position' => 'RDWJobBundle:Position',
        ];

        $this->objectClasses = [
            'city' => 'RDW\Bundle\LocationBundle\Entity\City',
        ];

        $this->filterManager = new FilterManager($this->em);
    }

    /**
     * @test
     */
    public function it_should_return_proper_repository_class()
    {
        $this->em->expects($this->once())
            ->method('getRepository')
            ->with('RDWLocationBundle:City')
            ->willReturn($this->repo);

        $this->repo->expects($this->once())
            ->method('findBy')
            ->willReturn([]);

        $this->assertInstanceOf('RDW\Bundle\AppBundle\Collection\FiltersCollection', $this->filterManager->getFilterValues('city'));
    }

    /**
     * @test
     */
    public function it_should_save_value()
    {
        $this->em
            ->expects($this->once())
            ->method('persist')
            ->with($this->object)
            ->will($this->returnValue(true));

        $this->em
            ->expects($this->once())
            ->method('flush')
            ->will($this->returnValue(true));

        $this->assertTrue($this->filterManager->saveValue($this->object));
    }

    /**
     * @test
     */
    public function it_should_not_delete_object_when_not_allowed()
    {
        $this->em->expects($this->once())
            ->method('getRepository')
            ->with('RDWLocationBundle:City')
            ->willReturn($this->repo);

        $this->repo->expects($this->once())
            ->method('find')
            ->with($this->equalTo(5))
            ->willReturn($this->object);

        $this->object->expects($this->once())
            ->method('isDeletable')
            ->willReturn(false);

        $this->assertFalse($this->filterManager->deleteValue('city', 5));
    }

    /**
     * @test
     */
    public function it_should_delete_object_when_allowed()
    {
        $this->em->expects($this->once())
            ->method('getRepository')
            ->with('RDWLocationBundle:City')
            ->willReturn($this->repo);

        $this->repo->expects($this->once())
            ->method('find')
            ->with($this->equalTo(5))
            ->willReturn($this->object);

        $this->object->expects($this->once())
            ->method('isDeletable')
            ->willReturn(true);

        $this->em
            ->expects($this->once())
            ->method('remove')
            ->with($this->object)
            ->will($this->returnValue(true));

        $this->em
            ->expects($this->once())
            ->method('flush')
            ->will($this->returnValue(true));

        $this->assertTrue($this->filterManager->deleteValue('city', 5));
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function it_should_throw_exception_with_undefined_filter_type()
    {
        $this->filterManager->deleteValue('non_existing', 6);
    }

    /**
     * @test
     * @expectedException \InvalidArgumentException
     */
    public function it_should_throw_exception_when_data_class_not_exist()
    {
        $this->filterManager->getDataClass('non_existing');
    }

    /**
     * @test
     */
    public function it_should_return_proper_data_class()
    {
        $this->assertEquals('RDW\Bundle\LocationBundle\Entity\City', $this->filterManager->getDataClass('city'));
    }

    public function it_should_return_filter_entity()
    {
        $this->em->expects($this->once())
            ->method('getRepository')
            ->with('RDWLocationBundle:City')
            ->willReturn($this->repo);

        $this->repo->expects($this->once())
            ->method('find')
            ->with($this->equalTo(3))
            ->willReturn($this->object);

        $this->assertEquals($this->object, $this->filterManager->getFilterValue('city', 3));
    }
}
