<?php

namespace RDW\Bundle\ManageBundle\Tests\Service;

use RDW\Bundle\ManageBundle\Entity\User;
use RDW\Bundle\ManageBundle\Service\UserManager;

/**
 * Class UserManagerTest
 * @package RDW\Bundle\ManageBundle\Tests\Service
 */
class UserManagerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $em;

    private $paginator;

    private $encoderFactory;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * set up
     */
    public function setUp()
    {
        $this->em = $this->getMockBuilder('Doctrine\Common\Persistence\ObjectManager')->getMock();
        $this->paginator = $this->getMock('Knp\Component\Pager\Paginator');
        $this->encoderFactory = $this
                ->getMockBuilder('\Symfony\Component\Security\Core\Encoder\EncoderFactory')
                ->disableOriginalConstructor()
                ->getMock();

        $this->userManager = new UserManager($this->em, $this->paginator, $this->encoderFactory);
    }

    /**
     * @test
     */
    public function it_should_properly_set_role()
    {
        $user = new User();
        $user->setType(User::TYPE_ADMIN);

        $this->invokeMethod($this->userManager, 'updateRoles', [$user]);

        $this->assertTrue($user->hasRole(User::ROLE_ADMIN));
        $this->assertFalse($user->hasRole(User::ROLE_CENSOR));

        $user->setType(User::TYPE_CENSOR);

        $this->invokeMethod($this->userManager, 'updateRoles', [$user]);

        $this->assertTrue($user->hasRole(User::ROLE_CENSOR));
        $this->assertFalse($user->hasRole(User::ROLE_ADMIN));
    }

    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
