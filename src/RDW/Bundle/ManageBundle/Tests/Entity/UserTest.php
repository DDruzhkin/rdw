<?php

namespace RDW\Bundle\ManageBundle\Tests\Entity;

use RDW\Bundle\ManageBundle\Entity\User;

class UserTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @test
     */
    public function it_should_know_when_user_is_blocked()
    {
        $user = new User();

        $this->assertFalse($user->isBlocked());

        $user->setStatus(User::STATUS_BLOCKED);

        $this->assertTrue($user->isBlocked());
    }
}