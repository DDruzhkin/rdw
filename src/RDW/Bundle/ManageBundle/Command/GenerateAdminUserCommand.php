<?php

namespace RDW\Bundle\ManageBundle\Command;

use RDW\Bundle\ManageBundle\Entity\User;
use RDW\Bundle\ManageBundle\Service\UserManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateAdminUserCommand extends ContainerAwareCommand
{
    const NAME = 'rdw:manage:add-user';

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName(self::NAME)
            ->addOption('email', null, InputOption::VALUE_REQUIRED)
            ->addOption('password', null, InputOption::VALUE_REQUIRED);
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $email = $input->getOption('email');
        $password = $input->getOption('password');
        /** @var User $user */
        $user = $this->getUserManager()->create();
        $user->setName($email)
            ->setUsername($email)
            ->setPhone($email)
            ->setEmail($email)
            ->setPlainPassword($password)
            ->setStatus('active')
            ->setType('admin');
        $this->getUserManager()->update($user);
    }

    /**
     * @return UserManager
     */
    private function getUserManager()
    {
        return $this->getContainer()->get('rdw_manage.service.user_manager');
    }
}
