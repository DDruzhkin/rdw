<?php

namespace RDW\Bundle\ManageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Filter
 *
 * @package RDW\Bundle\ManageBundle\Entity
 *
 * @ORM\MappedSuperclass()
 */
abstract class Filter implements FilterInterface, SortableInterface
{
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\Column(type="string", nullable=true, length=256, name="type")
     */
    private $type;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_system", type="boolean", nullable=true)
     */
    private $system;

    /**
     * @var integer
     *
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    protected $position;

    /**
     * {@inheritdoc}
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * {@inheritdoc}
     */
    public function setTitle($title)
    {
        $this->title = mb_strcut($title, 0, 255);

        return $this;
    }

    /**
     * @return bool
     */
    public function isSortable()
    {
        return ($this instanceof SortableInterface);
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     *
     * @return $this
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTitle();
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function setSystem($value)
    {
        $this->system = $value;

        return $this;
    }

    public function isSystem()
    {
        return $this->system;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }
}
