<?php

namespace RDW\Bundle\ManageBundle\Entity;

/**
 * Interface SortableInterface
 *
 * @package RDW\Bundle\ManageBundle\Entity
 */
interface SortableInterface
{
    /**
     * @param int $position
     *
     * @return self
     */
    public function setPosition($position);

    /**
     * @return int
     */
    public function getPosition();
}
