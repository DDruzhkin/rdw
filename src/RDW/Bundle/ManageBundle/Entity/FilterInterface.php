<?php

namespace RDW\Bundle\ManageBundle\Entity;

/**
 * Interface FilterInterface
 *
 * @package RDW\Bundle\ManageBundle\Entity
 */
interface FilterInterface
{
    /**
     * @return bool
     */
    public function isDeletable();

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param string $title
     *
     * @return self
     */
    public function setTitle($title);

    /**
     * @return int
     */
    public function getId();
}
