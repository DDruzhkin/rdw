<?php

namespace RDW\Bundle\ManageBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\HistoryBundle\Entity\History;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class User
 *
 * @package RDW\Bundle\ManageBundle\Entity
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 *
 * @ORM\Table(name="administrators")
 * @ORM\Entity(repositoryClass="RDW\Bundle\ManageBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 *
 * @UniqueEntity(fields="username", message="This email is already used.", groups={"create", "edit"})
 */
class User implements UserInterface, \Serializable
{
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_CENSOR = 'ROLE_CENSOR';
    const ROLE_BLOCKED = 'ROLE_BLOCKED';

    const TYPE_ADMIN = 'admin';
    const TYPE_CENSOR = 'censor';

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';
    const STATUS_BLOCKED = 'blocked';
    const STATUS_DELETED = 'deleted';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, unique=true)
     *
     * @Assert\NotBlank(message="Username cannot be empty", groups={"create", "edit"})
     * @Assert\Email(message="Username must be a valid email address", groups={"create", "edit"})
     *
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=88)
     */
    private $password;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Password cannot be empty", groups={"create"})
     * @Assert\Length(
     *      min=5,
     *      max=20,
     *      minMessage="Password must be at least {{ limit }} characters length.",
     *      maxMessage="Password cannot be at longer than {{ limit }} characters length.",
     *      groups={"create", "edit"}
     * )
     */
    private $plainPassword;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     *
     * @Assert\NotBlank(message="Name cannot be empty", groups={"create", "edit"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=255, nullable=true)
     */
    private $surname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true))
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=20)
     *
     * @Assert\NotBlank(message="Type cannot be empty", groups={"create", "edit"})
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=20)
     *
     * @Assert\NotBlank(message="Status cannot be empty", groups={"create", "edit"})
     */
    private $status;

    /**
     * @var array
     *
     * @ORM\Column(name="roles", type="array")
     */
    private $roles;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string")
     *
     * @Assert\NotBlank(message="Phone cannot be empty", groups={"create", "edit"})
     * @Assert\Regex(
     *      pattern="/^([\+0-9\s\-]+)$/",
     *      message="Field must be a valid phone number and can have only numbers, +, - and spaces",
     *      groups={"create", "edit"}
     * )
     */
    private $phone;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\HistoryBundle\Entity\History", mappedBy="user")
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    protected $history;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roles = array();
        $this->status = self::STATUS_ACTIVE;
        $this->history = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName() . ' ' . $this->getSurname();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return bool
     */
    public function isLoggedInAsCensor()
    {
        return ($this->hasRole(self::ROLE_CENSOR)) ? true : false;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->getName() . ' ' . $this->getSurname();
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * {@inheritDoc}
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * {@inheritDoc}
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->createdAt,
            $this->updatedAt,
        ));
    }

    /**
     * {@inheritDoc}
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->createdAt,
            $this->updatedAt,
        ) = unserialize($serialized);
    }

    /**
     * Returns the user roles
     *
     * @return array The roles
     */
    public function getRoles()
    {
        $roles = $this->roles;

        if (empty($roles)) {
            $roles[] = self::ROLE_CENSOR;
        }

        return array_unique($roles);
    }

    /**
     * Set user roles
     *
     * @param array $roles
     *
     * @return $this
     */
    public function setRoles(array $roles)
    {
        $this->roles = array();

        foreach (array_unique($roles) as $role) {
            $this->addRole($role);
        }

        return $this;
    }

    /**
     * @param string $role
     *
     * @return bool
     */
    public function hasRole($role)
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    /**
     * Add role to user
     *
     * @param string $role
     *
     * @return $this
     */
    public function addRole($role)
    {
        $role = strtoupper($role);

        if ( ! in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * @param string $role
     *
     * @return $this
     */
    public function removeRole($role)
    {
        if (false !== $key = array_search(strtoupper($role), $this->roles, true)) {
            unset($this->roles[$key]);
            $this->roles = array_values($this->roles);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     *
     * @return $this
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return array(
            self::TYPE_ADMIN => 'Administrator',
            self::TYPE_CENSOR => 'Censor',
        );
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return array(
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_INACTIVE => 'Inactive',
            self::STATUS_BLOCKED => 'Blocked',
        );
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param string $plainPassword
     *
     * @return $this
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * checks if user is blocked
     * @return boolean
     */
    public function isBlocked()
    {
        return $this->getStatus() === self::STATUS_BLOCKED;
    }

    /**
     * @return ArrayCollection
     */
    public function getHistory()
    {
        return $this->history;
    }

    /**
     * @param History $history
     *
     * @return $this
     */
    public function addHistory(History $history)
    {
        $this->history->add($history);

        return $this;
    }

    /**
     * @param History $history
     *
     * @return $this
     */
    public function removeHistory(History $history)
    {
        $this->history->removeElement($history);

        return $this;
    }
}
