$(function() {
    $("[data-handler='modalLink']").modalLink({
        'modalContentHolder' : 'modal_container'
    });

    $('.select2-hidden-multi').select2Field({'type': 'multi'});
    $('.select2-hidden-single').select2Field({'type': 'single'});

    if ($.fn.iCheck) {
        $("input[type=checkbox]").iCheck({
            checkboxClass: "icheckbox_minimal checkbox",
            radioClass: "iradio_minimal radio"
        });
    }

    var $positionAutocomlete = $('.position-autocomplete');
    var ajaxUrl = $positionAutocomlete.attr('data-ajax-url');
    $positionAutocomlete.autocomplete({
        source: function(request, response) {
            $.ajax( {
                url: ajaxUrl,
                dataType: "json",
                type : 'Get',
                data: {
                    q: request.term
                },
                success: function(data) {
                    var updatedData = {};
                    $.each(data, function(i, position) {
                        updatedData[position.value] = position.title;
                    });
                    response(updatedData);
                }
            });
        },
        minLength: 2
    });
});


