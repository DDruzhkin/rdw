(function( $ ){
    $.fn.confirmFormSubmitHandler= function(options) {

        var settings = $.extend({
            messageAttribute: "message"
        }, options);

        this.on('click', function(event){

            var element = $(event.target);

            if (confirm(element.data(settings.messageAttribute))) {
                var form = $("#"+element.data('form'));
                form.submit();
            }

            return false;
        });
    };
})(jQuery);

$(document).ready(function() {
    $("[data-handler='confirmFormSubmit']").confirmFormSubmitHandler();
});
