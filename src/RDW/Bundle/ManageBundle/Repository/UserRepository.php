<?php

namespace RDW\Bundle\ManageBundle\Repository;

use Doctrine\ORM\EntityRepository;
use RDW\Bundle\ManageBundle\Entity\User;
use RDW\Bundle\ManageBundle\Model\UserFilter;

/**
 * Class UserRepository
 *
 * @package RDW\Bundle\ManageBundle\Repository
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class UserRepository extends EntityRepository
{
    /**
     * @param UserFilter $filter
     *
     * @return \Doctrine\ORM\Query
     */
    public function getUserListQuery(UserFilter $filter)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('u')
            ->from('RDWManageBundle:User', 'u');

        // name, surname
        if (null != $filter->getName() && null != $filter->getSurname()) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->andX(
                    $queryBuilder->expr()->like('u.name', ':name'),
                    $queryBuilder->expr()->like('u.surname', ':surname')
                ))
                ->setParameter('name', '%' . $filter->getName() . '%')
                ->setParameter('surname', '%' . $filter->getSurname() . '%');
        } elseif (null != $filter->getName()) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->like('u.name', ':name'))
                ->setParameter('name', '%' . $filter->getName() . '%');
        } elseif (null != $filter->getSurname()) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->like('u.surname', ':surname'))
                ->setParameter('surname', '%' . $filter->getSurname() . '%');
        }

        // registration date from, to
        if (null != $filter->getRegisteredAtFrom() && null != $filter->getRegisteredAtTo()) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->andX(
                    $queryBuilder->expr()->gte('DATE(u.createdAt)', ':registeredAtFrom'),
                    $queryBuilder->expr()->lte('DATE(u.createdAt)', ':registeredAtTo')
                ))
                ->setParameter('registeredAtFrom', $filter->getRegisteredAtFrom())
                ->setParameter('registeredAtTo', $filter->getRegisteredAtTo());
        } elseif (null != $filter->getRegisteredAtFrom()) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->gte('DATE(u.createdAt)', ':registeredAtFrom'))
                ->setParameter('registeredAtFrom', $filter->getRegisteredAtFrom());
        } elseif (null != $filter->getRegisteredAtTo()) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->lte('DATE(u.createdAt)', ':registeredAtTo'))
                ->setParameter('registeredAtTo', $filter->getRegisteredAtTo());
        }

        // user type
        if (null != $filter->getType()) {
            $queryBuilder
                ->andWhere('u.type = :type')
                ->setParameter('type', $filter->getType());
        }

        // user status, by default show != deleted
        if (null != $filter->getStatus()) {
            $queryBuilder
                ->andWhere('u.status = :status')
                ->setParameter('status', $filter->getStatus());
        } else {
            $queryBuilder
                ->andWhere('u.status != :status')
                ->setParameter('status', User::STATUS_DELETED);
        }

        $queryBuilder
            ->orderBy('u.id', 'desc');

        return $queryBuilder->getQuery();
    }
}
