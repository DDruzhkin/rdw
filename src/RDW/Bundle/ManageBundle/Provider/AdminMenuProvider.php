<?php

namespace RDW\Bundle\ManageBundle\Provider;

use RDW\Bundle\ManageBundle\Model\AdminMenuItem;
use Symfony\Component\HttpKernel\Config\FileLocator;
use Symfony\Component\VarDumper\VarDumper;
use Symfony\Component\Yaml\Yaml;

class AdminMenuProvider
{
    /**
     * @var FileLocator
     */
    protected $fileLocator;

    /**
     * AdminMenuProvider constructor.
     *
     * @param FileLocator $fileLocator
     */
    public function __construct(FileLocator $fileLocator)
    {
        $this->fileLocator = $fileLocator;
    }

    /**
     * @return AdminMenuItem[]
     */
    public function getMenuTree()
    {
        $menuTree = Yaml::parse(
            file_get_contents($this->fileLocator->locate('@RDWManageBundle/Resources/config/admin_menu.yml'))
        );

        if (isset($menuTree['imports'])) {
            $imports = $menuTree['imports'];
            unset($menuTree['imports']);
            foreach ($imports as $k => $v) {
                $data = Yaml::parse(file_get_contents($this->fileLocator->locate($v['resource'])));
                $menuTree['admin_menu'] = array_merge_recursive(
                    array_values($data['admin_menu']),
                    array_values($menuTree['admin_menu'])
                );
            }
        }

        $menuTree = $menuTree['admin_menu'];
        foreach ($menuTree as $k => $v) {
            $menuTree[$k] = new AdminMenuItem($v);
        }

        return $menuTree;
    }
}
