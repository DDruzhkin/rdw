<?php

namespace RDW\Bundle\ManageBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;

class AdminMenuItem
{
    /** @var string */
    protected $title;

    /** @var string */
    protected $route;

    /** @var array */
    protected $routeParams = [];

    /** @var ArrayCollection|AdminMenuItem[] */
    protected $children = [];

    public function __construct($arrayData = [])
    {
        $this->children = new ArrayCollection();
        if ($arrayData && isset($arrayData['title']) && (isset($arrayData['route']) || isset($arrayData['children']))) {
            $this->setTitle($arrayData['title']);

            if (isset($arrayData['route'])) {
                $this->setRoute($arrayData['route']);
                $this->setRouteParams(isset($arrayData['routeParams']) ? $arrayData['routeParams'] : []);
            }

            if (isset($arrayData['children'])) {
                foreach ($arrayData['children'] as $child) {
                    $this->addChild(new self($child));
                }
            }
        }
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @param string $route
     *
     * @return $this
     */
    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getRouteParams()
    {
        return $this->routeParams;
    }

    /**
     * @param array|null $routeParams
     *
     * @return $this
     */
    public function setRouteParams($routeParams)
    {
        $this->routeParams = $routeParams;

        return $this;
    }

    /**
     * @return AdminMenuItem[]
     */
    public function getChildren()
    {
        return $this->children->toArray();
    }

    /**
     * @param AdminMenuItem[] $children
     *
     * @return $this
     */
    public function setChildren(array $children = [])
    {
        $this->children->clear();

        foreach ($children as $child) {
            $this->addChild($child);
        }

        return $this;
    }

    /**
     * @param AdminMenuItem $item
     *
     * @return $this
     */
    public function addChild(AdminMenuItem $item)
    {
        if (!$this->children->contains($item)) {
            $this->children->add($item);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function hasChildren()
    {
        return $this->children->count() ? true : false;
    }
}
