<?php

namespace RDW\Bundle\ManageBundle\Model;

/**
 * Class UserFilter
 *
 * @package RDW\Bundle\ManageBundle\Model
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class UserFilter
{
    protected $name;
    protected $surname;
    protected $registeredAtFrom;
    protected $registeredAtTo;
    protected $type;
    protected $status;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return mixed
     */
    public function getRegisteredAtFrom()
    {
        return $this->registeredAtFrom;
    }

    /**
     * @param mixed $registeredAtFrom
     */
    public function setRegisteredAtFrom($registeredAtFrom)
    {
        $this->registeredAtFrom = $registeredAtFrom;
    }

    /**
     * @return mixed
     */
    public function getRegisteredAtTo()
    {
        return $this->registeredAtTo;
    }

    /**
     * @param mixed $registeredAtTo
     */
    public function setRegisteredAtTo($registeredAtTo)
    {
        $this->registeredAtTo = $registeredAtTo;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
}