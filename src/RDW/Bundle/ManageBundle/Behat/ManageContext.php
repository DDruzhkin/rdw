<?php

namespace RDW\Bundle\ManageBundle\Behat;

use Behat\Gherkin\Node\TableNode;
use RDW\Bundle\AppBundle\Behat\DefaultContext;
use RDW\Bundle\ManageBundle\Entity\User;

/**
 * Class AdminsContext
 *
 * @package RDW\Bundle\UserBundle\Behat
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class ManageContext extends DefaultContext
{
    /**
     * @Given /^I am logged in as administrator$/
     */
    public function iAmLoggedInAsAdministrator()
    {
        $this->iAmLoggedInAsRole(User::ROLE_ADMIN, 'admin@example.com');
    }

    /**
     * @Given /^I am on the manage login page$/
     */
    public function iAmOnTheManageLoginPage()
    {
        $this->getSession()->visit($this->generatePageUrl('rdw.manage.security.login'));
    }

    /**
     * @Given /^I should be on the manage login page$/
     */
    public function iShouldBeTheOnManageLoginPage()
    {
        $this->assertSession()->addressEquals($this->generateUrl('rdw.manage.security.login'));
    }

    /**
     * @Given /^I am on the user list page$/
     */
    public function iAmOnTheUserListPage()
    {
        $this->getSession()->visit($this->generatePageUrl('rdw.manage.user.list'));
    }

    /**
     * @Given /^I should be on the user list page$/
     */
    public function iShouldBeTheOnUserListPage()
    {
        $this->assertSession()->addressEquals($this->generateUrl('rdw.manage.user.list'));
    }

    /**
     * @Given /^I should be on the user blocked page$/
     */
    public function iShouldBeOnTheUserBlockedPage()
    {
        $this->assertSession()->addressEquals($this->generateUrl('rdw.manage.user.blocked'));
    }

    /**
     * @Given /^I am on the backend home page$/
     */
    public function iAmOnTheBackendHomePage()
    {
        $this->assertSession()->addressEquals($this->generateUrl('rdw.manage'));
    }

    /**
     * @Given /^I should be on the backend home page$/
     */
    public function iShouldBeOnTheBackendHomePage()
    {
        $this->assertSession()->addressEquals($this->generateUrl('rdw.manage'));
    }

    /**
     * @param string $username
     *
     * @Given /^I am on the user "([^""]*)" editing page$/
     */
    public function iAmOnTheUserEditPage($username)
    {
        $user = $this->getUserManager()->findByUsername($username);

        $this->getSession()->visit($this->generateUrl('rdw.manage.user.edit', array('userId' => $user->getId(), true)));
    }

    /**
     * @Given /^I am on the user create page$/
     */
    public function iAmOnTheUserCreatePage()
    {
        $this->getSession()->visit($this->generateUrl('rdw.manage.user.create'));
    }

    /**
     * @Given /^I should be on the user create page$/
     */
    public function iShouldBeOnTheUserCreatePage()
    {
        $this->assertSession()->addressEquals($this->generateUrl('rdw.manage.user.create'));
    }

    /**
     * @param TableNode $table
     *
     * @Given /^there are following backend users:$/
     */
    public function thereAreFollowingBackendUsers(TableNode $table)
    {
        foreach ($table->getHash() as $data) {
            $this->thereIsUser(
                $data['username'],
                isset($data['password']) ? $data['password'] : $this->faker->word(),
                isset($data['role']) ? $data['role'] : User::ROLE_ADMIN,
                isset($data['type']) ? $data['type'] : User::TYPE_ADMIN,
                isset($data['status']) ? $data['status'] : User::STATUS_ACTIVE,
                false
            );
        }

        $this->getEntityManager()->flush();
    }

    /**
     * @param string $username
     * @param string $password
     * @param null   $role
     * @param string $type
     * @param string $status
     * @param bool   $flush
     *
     * @return User
     */
    public function thereIsUser($username, $password, $role, $type, $status, $flush = true)
    {
        if (null === $user = $this->getUserManager()->findByUsername($username)) {

            /* @var $user User */
            $user = $this->getUserManager()->create();
            $user->setUsername($username);
            $user->setEmail($username);
            $user->setPlainPassword($password);
            $user->setType($type);
            $user->setStatus($status);
            $user->setName($this->faker->firstName());
            $user->setSurname($this->faker->lastName());
            $user->setPhone($this->faker->phoneNumber());
            $user->addRole($role);
            $user->setPassword($this->getUserManager()->getEncodedPassword($user));

            $this->getEntityManager()->persist($user);

            if ($flush) {
                $this->getEntityManager()->flush();
            }
        }

        return $user;
    }

    /**
     * @param string $role
     * @param string $username
     */
    private function iAmLoggedInAsRole($role, $username)
    {
        $this->getSession()->visit($this->generatePageUrl('rdw.manage.security.login'));

        $this->fillField('_username', $username);
        $this->fillField('_password', 'rdwrdw');
        $this->pressButton('Sign in');
    }

    /**
     * @return \RDW\Bundle\ManageBundle\Service\UserManager
     */
    protected function getUserManager()
    {
        return $this->getService('rdw_manage.service.user_manager');
    }
}
