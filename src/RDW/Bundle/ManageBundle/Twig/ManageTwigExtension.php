<?php

namespace RDW\Bundle\ManageBundle\Twig;

use RDW\Bundle\ManageBundle\Provider\AdminMenuProvider;

class ManageTwigExtension extends \Twig_Extension
{

    /** @var AdminMenuProvider */
    protected $adminMenuProvider;

    public function __construct(AdminMenuProvider $adminMenuProvider)
    {
        $this->adminMenuProvider = $adminMenuProvider;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('admin_menu_tree', [$this->adminMenuProvider, 'getMenuTree']),
        ];
    }
}
