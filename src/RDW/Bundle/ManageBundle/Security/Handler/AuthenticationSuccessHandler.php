<?php

namespace RDW\Bundle\ManageBundle\Security\Handler;

use RDW\Bundle\HistoryBundle\Entity\History;
use RDW\Bundle\ManageBundle\Entity\User;
use RDW\Bundle\HistoryBundle\Event\HistoryEvent;
use RDW\Bundle\HistoryBundle\RDWHistoryEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

/**
 * Class AuthenticationSuccessHandler
 *
 * @package RDW\Bundle\ManageBundle\Security\Handler
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class AuthenticationSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    /**
     * @var \Symfony\Component\Routing\Generator\UrlGeneratorInterface
     */
    private $router;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param UrlGeneratorInterface    $router
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(UrlGeneratorInterface $router, EventDispatcherInterface $dispatcher)
    {
        $this->router = $router;
        $this->eventDispatcher = $dispatcher;
    }

    /**
     * @param Request        $request
     * @param TokenInterface $token
     *
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        if ($token->getUser()->hasRole(User::ROLE_BLOCKED)) {
            $route = 'rdw.manage.user.blocked';
        } else {
            $route = 'rdw.manage';
        }

        return new RedirectResponse($this->router->generate($route, [], true));
    }
}
