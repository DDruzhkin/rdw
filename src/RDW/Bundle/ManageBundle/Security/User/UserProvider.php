<?php

namespace RDW\Bundle\ManageBundle\Security\User;

use RDW\Bundle\ManageBundle\Entity\User;
use RDW\Bundle\ManageBundle\Service\UserManager;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

/**
 * Class UserProvider
 *
 * @package RDW\Bundle\ManageBundle\Security\User
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class UserProvider implements UserProviderInterface
{
    /**
     * @var \RDW\Bundle\ManageBundle\Service\UserManager
     */
    private $userManager;

    /**
     * @param UserManager $userManager
     */
    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @param string $username
     *
     * @return object|UserInterface
     * @throws \Symfony\Component\Security\Core\Exception\UsernameNotFoundException
     */
    public function loadUserByUsername($username)
    {
        $user = $this->userManager->findByUsername($username);

        if ( ! $user) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }

        if ( ! in_array($user->getStatus(), [User::STATUS_ACTIVE, User::STATUS_BLOCKED])) {
            return new User();
        }

        return $user;
    }

    /**
     * @param UserInterface $user
     *
     * @return object|UserInterface
     * @throws \Symfony\Component\Security\Core\Exception\UnsupportedUserException
     */
    public function refreshUser(UserInterface $user)
    {
        if ( ! $user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return $class === 'RDW\Bundle\ManageBundle\Entity\User';
    }
}