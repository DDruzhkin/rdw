<?php

namespace RDW\Bundle\ManageBundle\Controller;

use RDW\Bundle\JobBundle\Entity\Position;
use RDW\Bundle\ManageBundle\Form\Type\NewFilterValueFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class UserController
 *
 * @package RDW\Bundle\ManageBundle\Controller
 *
 * @Route("/filters")
 */
class FilterController extends Controller
{
    /**
     * @Route("/list", name="rdw_manage.filter.list")
     * @Template()
     *
     * @return array
     */
    public function listAction()
    {
        $this->isGrantedLocal();

        return [];
    }

    /**
     * @param string $type
     *
     * @Route("/view/{type}", name="rdw_manage.filter.view")
     * @Template()
     *
     * @return array
     */
    public function viewAction($type)
    {
        $this->isGrantedLocal();

        $filterValues = $this->get('rdw_manage.service.filter_manager')->getFilterValues($type);

        return [
            'filterValues' => $filterValues,
            'type' => $type,
        ];
    }

    /**
     * @param string $type
     * @param int    $id
     *
     * @Route("/delete/{type}/{id}", name="rdw_manage.filter.delete")
     *
     * @return array|RedirectResponse
     */
    public function deleteAction($type, $id)
    {
        $this->isGrantedLocal();

        if ($this->get('rdw_manage.service.filter_manager')->deleteValue($type, $id)) {
            $flash = $this->get('braincrafted_bootstrap.flash');
            $flash->success('Filter value deleted successfully');
        }

        return $this->redirect($this->generateUrl('rdw_manage.filter.view', ['type' => $type]));
    }

    /**
     * @param Request $request
     * @param string  $type
     *
     * @Route("/add/{type}", name="rdw_manage.filter.add")
     * @Template()
     *
     * @return array|RedirectResponse
     */
    public function addAction(Request $request, $type)
    {
        $this->isGrantedLocal();

        $dataClass = $this->get('rdw_manage.service.filter_manager')->getDataClass($type);
        $object = new $dataClass;

        $form = $this->createForm(new NewFilterValueFormType(), $object, array('data_class' => $dataClass));
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->get('rdw_manage.service.filter_manager')->saveValue($object);

            $flash = $this->get('braincrafted_bootstrap.flash');
            $flash->success('New filter value created successfully');

            return $this->redirect($this->generateUrl('rdw_manage.filter.view', ['type' => $type]));
        }

        return [
            'type' => $type,
            'form' => $form->createView(),
        ];
    }

    /**
     * @param Request $request
     * @param string  $type
     * @param integer $id
     *
     * @Route("/edit/{type}/{id}", name="rdw_manage.filter.edit")
     * @Template()
     *
     * @return array|RedirectResponse
     */
    public function editAction(Request $request, $type, $id)
    {
        $this->isGrantedLocal();

        $dataClass = $this->get('rdw_manage.service.filter_manager')->getDataClass($type);
        $entity = $this->get('rdw_manage.service.filter_manager')->getFilterValue($type, $id);

        $form = $this->createForm(new NewFilterValueFormType(), $entity, array('data_class' => $dataClass));
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->get('rdw_manage.service.filter_manager')->saveValue($entity);
            $this->get('braincrafted_bootstrap.flash')->success('Filter values updated successfully');

            return $this->redirect($this->generateUrl('rdw_manage.filter.view', ['type' => $type]));
        }

        $unapprovedPa = [];
        if ($entity instanceof Position && $entity->getId()) {
            $unapprovedPa = $this->get('rdw_job.provider.professional_area')->findUnapproved($entity);
        }


        return [
            'type' => $type,
            'filterValue' => $entity,
            'form' => $form->createView(),
            'entity' => $entity,
            'unapproved_pa' => $unapprovedPa,
        ];
    }

    /**
     * @param string $type
     * @param int    $id
     * @param int    $position
     *
     * @Route("/position/{type}/{id}/{position}", name="rdw_manage.filter.position")
     *
     * @return array|Response
     */
    public function positionAction($type, $id, $position)
    {
        $this->isGrantedLocal();

        if ($this->get('rdw_manage.service.filter_manager')->changePosition($type, $id, $position)) {
            $flash = $this->get('braincrafted_bootstrap.flash');
            $flash->success('Filter value deleted successfully');
        }

        return $this->redirect($this->generateUrl('rdw_manage.filter.view', ['type' => $type]));
    }

    /**
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    private function isGrantedLocal()
    {
        if (! $this->container->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }
    }
}
