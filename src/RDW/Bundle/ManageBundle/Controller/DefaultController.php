<?php

namespace RDW\Bundle\ManageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class DefaultController
 *
 * @package RDW\Bundle\ManageBundle\Controller
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="rdw.manage")
     * @Template()
     *
     * @return array
     */
    public function indexAction()
    {
        $this->get('rdw_manage.menu.provide.admin')->getMenuTree();
        return [];
    }
}
