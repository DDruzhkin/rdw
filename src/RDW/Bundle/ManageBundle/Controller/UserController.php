<?php

namespace RDW\Bundle\ManageBundle\Controller;

use RDW\Bundle\ManageBundle\Form\Type\UserFilterType;
use RDW\Bundle\ManageBundle\Form\Type\UserType;
use RDW\Bundle\ManageBundle\Model\UserFilter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class UserController
 *
 * @package RDW\Bundle\ManageBundle\Controller
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 *
 * @Route("/user")
 *
 */
class UserController extends Controller
{
    /**
     * @param Request $request
     *
     * @Route("/list", name="rdw.manage.user.list")
     * @Template()
     *
     * @return array
     */
    public function listAction(Request $request)
    {
        $this->isGrantedLocal();

        $filter = new UserFilter();
        $form = $this->createForm(new UserFilterType(), $filter);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $filter = $form->getData();
        }

        $pagination = $this->getUserManager()->getPaginatedUserList(
            $filter,
            $this->get('request')->query->get('page', 1),
            $this->container->getParameter('rdw_manage.user.paginator.per_page')
        );

        return [
            'pagination' => $pagination,
            'filter_form' => $form->createView()
        ];
    }

    /**
     * @param Request $request
     *
     * @Route("/create", name="rdw.manage.user.create")
     * @Template()
     *
     * @return array|RedirectResponse
     */
    public function createAction(Request $request)
    {
        $this->isGrantedLocal();

        $user = $this->getUserManager()->create();

        $form = $this->createForm(new UserType(), $user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getUserManager()->addNewUser($form->getData());

            $this->get('braincrafted_bootstrap.flash')->success('User successfully created');

            return $this->redirect($this->get('router')->generate('rdw.manage.user.list'));
        }

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @param Request $request
     * @param int     $userId
     *
     * @Route("/edit/{userId}", requirements={"userId" = "\d+"}, name="rdw.manage.user.edit")
     * @Template()
     *
     * @return array|RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function editAction(Request $request, $userId)
    {
        $this->isGrantedLocal();

        $user = $this->getUserManager()->findById($userId);

        if ( ! $user) {
            throw new NotFoundHttpException(printf("User with id %d was not found.", $userId));
        }

        $form = $this->createForm(new UserType(), $user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getUserManager()->update($form->getData());

            $this->get('braincrafted_bootstrap.flash')->success('User successfully updated');

            return $this->redirect($this->get('router')->generate('rdw.manage.user.list'));
        }

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @param int $userId
     *
     * @Route("/delete/{userId}", requirements={"userId" = "\d+"}, name="rdw.manage.user.delete")
     *
     * @return RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteAction($userId)
    {
        $this->isGrantedLocal();

        $user = $this->getUserManager()->findById($userId);

        if ( ! $user) {
            throw new NotFoundHttpException(sprintf('The user with id "%d" does not exist.', $userId));
        }

        $this->getUserManager()->delete($user);

        return new RedirectResponse($this->get('router')->generate('rdw.manage.user.list'));
    }

    /**
     * @return \RDW\Bundle\ManageBundle\Service\UserManager
     */
    private function getUserManager()
    {
        return $this->container->get('rdw_manage.service.user_manager');
    }

    /**
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    private function isGrantedLocal()
    {
        if ( ! $this->container->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }
    }

    /**
     * @Route("/blocked", name="rdw.manage.user.blocked")
     * @Template()
     *
     * @return array
     */
    public function blockedAction()
    {
        return [];
    }
}
