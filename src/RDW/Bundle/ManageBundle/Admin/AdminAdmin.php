<?php

namespace RDW\Bundle\ManageBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use RDW\Bundle\ManageBundle\Entity\User;

class AdminAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('username', 'email', array(
                'required' => true,
                'label' => 'Username',
            ))
            ->add('type', 'choice', array(
                'choices'   => User::getTypes(),
                'empty_value' => 'Choose a type',
                'required' => true,
                'label' => 'User type',
            ))
            ->add('status', 'choice', array(
                'choices'   => User::getStatuses(),
                'required' => true,
                'label' => 'Status',
            ))
            ->add('name', 'text', array(
                'required' => true,
                'label' => 'Name',
            ))
            ->add('surname', 'text', array(
                'required' => false,
                'label' => 'Surname',
            ))
            ->add('phone', 'text', array(
                'required' => true,
                'label' => 'Phone',
            ))
            ->add('plainPassword', 'repeated', array(
                'type' => 'password',
                'invalid_message' => 'The password fields must match.',
                'required' => false,
                'first_options' => array('label' => 'Password'),
                'second_options' => array('label' => 'Repeat password'),
            ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('username');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('name')
            ->add('surname')
            ->add("email")
            ->add("status", 'choice', array(
                'choices' => array('inactive' => 'Не размещенa', 'active' => 'Размещена', 'blocked' => 'Заблокированные'),
            ));

        $listMapper->add('_action', 'actions', array('actions' => array(
            'edit'       => array(),
            'delete'     => array('label' => 'delete')
        )));
    }
}