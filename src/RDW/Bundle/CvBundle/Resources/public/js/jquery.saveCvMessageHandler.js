(function( $ ){
    $.fn.saveCvMessageHandler = function(options) {
        var settings = $.extend({
            contentHolder: "#modal_container"
        }, options);

        $(this).on('click', function() {

            var form = $(this).parents('form');
            var $this = $(this);

            var options = {
                success:    showResponse,
                type:       'post',
                dataType:   'json',
                resetForm:  false
            };

            form.ajaxForm(options);
            form.submit();

            $(document).loaderStart();

            // post-submit callback
            function showResponse(data)  {
                if (data.success) {
                    if ((typeof(data.needRegister) != 'undefined') && (typeof(data.needRegisterUrl) != 'undefined') && (data.needRegister == true)) {
                        $(document).registrationPopupHandler({url: data.needRegisterUrl});
                    } else if (typeof(data.popup) != 'undefined') {
                        //$(settings.contentHolder).modal('hide').html(data.popup).modal('show');
                        $(settings.contentHolder).removeData('bs.modal').html(data.popup).modal('show');
                    } else {
                        window.location.reload();
                    }
                } else {
                    $('#' + $this.attr('data-content-holder')).html(data.content);
                    $("[data-handler='saveCvMessage']").saveCvMessageHandler();
                }

                $(document).loaderStop();

                return false;
            }

            return false;
        });
    };
})(jQuery);

$(function() {
    $("[data-handler='saveCvMessage']").saveCvMessageHandler();
});
