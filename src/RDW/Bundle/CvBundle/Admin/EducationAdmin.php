<?php

namespace RDW\Bundle\CvBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use RDW\Bundle\CvBundle\Form\Transformer\IncompleteDateTransformer;
use RDW\Bundle\AppBundle\Form\Type\YearMonthFormType;

class EducationAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add($formMapper->create('dateTo', YearMonthFormType::class, [
                    'input' => 'datetime',
                    'format' => 'yyyy-MMMMd',
                    'years' => range(date('Y'), date('Y') - 30, -1),
                    'required' => false,
                    'label' => 'Education date to',
                    //'empty_value' => 'years',
                ])
                ->addViewTransformer(new IncompleteDateTransformer())
            )
            ->add('educationType', 'entity', [
                'class' => 'RDWJobBundle:EducationType',
                'empty_value' => 'Choose option',
                'property' => 'title',
                'required' => false,
            ])
            ->add('institution', 'text', [
                'required' => false,
                'label' => 'Institution',
                'attr' => [
                    'placeholder' => 'Institution placeholder',
                ],
            ])
            ->add('faculty', 'text', [
                'required' => false,
                'label' => 'Faculty',
                'attr' => [
                    'placeholder' => 'Faculty placeholder',
                ],
            ])
            ->add('additionalInfo', 'textarea', [
                'required' => false,
                'label' => 'Additional education info',
                'attr' => [
                    'placeholder' => 'Additional education info placeholder',
                ],
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        //$datagridMapper->add('id');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
        ;

        $listMapper->add('_action', 'actions', array('actions' => array(
            'edit'       => array(),
            'delete'     => array('label' => 'delete')
        )));
    }
}