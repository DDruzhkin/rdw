<?php

namespace RDW\Bundle\CvBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use RDW\Bundle\CvBundle\Form\Transformer\IncompleteDateTransformer;

class TrainingAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add($formMapper->create('dateTo', 'rdw_year_month', [
                'input'  => 'datetime',
                'format' => 'yyyy-MMMMd',
                'years'  => range(date('Y'), date('Y') - 30, -1),
                'required' => false,
                'label' => 'Course date to',
                'empty_value' => 'Choose option',
            ])
                ->addViewTransformer(new IncompleteDateTransformer())
            )
            ->add('institution', 'text', [
                'required' => false,
                'label' => 'Course institution',
                'attr' => [
                    'placeholder' => 'Course institution',
                ],
            ])
            ->add('additionalInfo', 'textarea', [
                'required' => false,
                'label' => 'Course information',
                'attr' => [
                    'placeholder' => 'Course information placeholder',
                ],
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        //$datagridMapper->add('id');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
        ;

        $listMapper->add('_action', 'actions', array('actions' => array(
            'edit'       => array(),
            'delete'     => array('label' => 'delete')
        )));
    }
}