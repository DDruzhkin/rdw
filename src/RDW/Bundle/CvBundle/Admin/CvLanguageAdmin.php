<?php

namespace RDW\Bundle\CvBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class CvLanguageAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('language', 'entity', [
                'class' => 'RDWJobBundle:Language',
                'empty_value' => 'Choose option',
                'property' => 'title',
                'required' => false,
                'label' => 'Language',
            ])
            ->add('level', 'entity', [
                'class' => 'RDWJobBundle:LanguageLevel',
                'empty_value' => 'Choose option',
                'property' => 'title',
                'required' => false,
                'label' => 'Level',
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        //$datagridMapper->add('id');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
        ;

        $listMapper->add('_action', 'actions', array('actions' => array(
            'edit'       => array(),
            'delete'     => array('label' => 'delete')
        )));
    }
}