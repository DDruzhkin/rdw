<?php
namespace RDW\Bundle\CvBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Braincrafted\Bundle\BootstrapBundle\Form\Type\BootstrapCollectionType;
use RDW\Bundle\AppBundle\Form\Type\ProfessionalAreaCollectionPopupType;
use RDW\Bundle\AppBundle\Form\Type\PositionAutocompleteType;
use RDW\Bundle\AppBundle\Form\Type\SalaryType;
use RDW\Bundle\AppBundle\Form\Type\Select2\CityCollectionType;
use RDW\Bundle\AppBundle\Form\Type\Select2\CityType;
use RDW\Bundle\AppBundle\Form\Type\Select2\ScopeType;
use RDW\Bundle\CvBundle\Form\Type\ContactPersonPhoneType;

class CvAdmin extends AbstractAdmin
{

    protected $datagridValues = array(

        // display the first page (default = 1)
        '_page' => 1,

        // reverse order (default = 'ASC')
        '_sort_order' => 'DESC',

        // name of the ordered field (default = the model's id field, if any)
        '_sort_by' => 'createdAt',
    );

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text')
            ->add('patronymic', 'text', [
                'required' => false,
            ])
            ->add('surname', 'text')
            ->add('gender','choice', array(
                'choices' => array(0 => 'Женский', 1 => 'Мужской'),
            ))
            ->add('birthday', 'birthday', array('required' => true,
                'format'   => 'ddMMyyyy'))
            ->add('cities', 'entity', array(
                'required' => true,
                'class' => 'RDW\Bundle\LocationBundle\Entity\City',
                'multiple' => true
            ))
            ->add('contactPersonPhones', 'sonata_type_collection', [
                'by_reference' => false,
                'required' => false,
                'label' => 'Phone',
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
                'admin_code' => 'admin.contact_person_phone_cv'
            ])
            ->add('email', 'text', [
                'required' => true,
            ])
            ->add('fbContact', 'text', [
                'required' => false,
            ])
            ->add('vkContact', 'text', [
                'required' => false,
            ])
            ->add('okContact', 'text', [
                'required' => false,
            ])
            ->add('gPlusContact', 'text', [
                'required' => false,
            ])
            ->add('twitterContact', 'text', [
                'required' => false,
            ])
            ->add('linkedInContact', 'text', [
                'required' => false,
            ])
            ->add('skypeContact', 'text', [
                'required' => false,
            ])
            ->add('position', 'sonata_type_model_autocomplete', array(
                'property' => 'title',
                'class' => 'RDW\Bundle\JobBundle\Entity\Position',
                'required' => true
            ))
            ->add('professionalArea', 'sonata_type_model_autocomplete', [
                'multiple' => true,
                'required' => true,
                'property' => 'title',
                'class' => 'RDW\Bundle\JobBundle\Entity\ProfessionalArea',
                'label' => 'Professional Area'
            ])
            ->add('scope', 'entity', array(
                'property' => 'title',
                'class' => 'RDW\Bundle\UserBundle\Entity\Scope',
                'required' => false
            ))
            ->add('workingPlaceCity', 'entity', array(
                'required' => true,
                'class' => 'RDW\Bundle\LocationBundle\Entity\City',
                'label' => 'Working place city'
            ))
            ->add('salaryFrom', 'text', [
                'required' => false,
                "label" => 'Зарплата в BYR Oт'
            ])
            ->add('salaryTo', 'text', [
                'required' => false,
                "label" => 'Зарплата в BYR До'
            ])
            ->add('workScheduleType', 'entity', [
                'class' => \RDW\Bundle\JobBundle\Entity\WorkScheduleType::class,
                'empty_value' => 'Not important',
                'property' => 'title',
                'required' => false,
                'label' => 'Work schedule type',
            ])
            ->add('educations', 'sonata_type_collection', [
                'required' => false,
                'by_reference' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
                'admin_code' => 'admin.education'
            ])
            ->add('trainings', 'sonata_type_collection', [
                'required' => false,
                'by_reference' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
                'admin_code' => 'admin.training'
            ])
            ->add('workExperience', 'sonata_type_collection', [
                'required' => false,
                'by_reference' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
                'admin_code' => 'admin.work_experience'
            ])
            ->add('languages', 'sonata_type_collection', [
                'required' => false,
                'by_reference' => false,
                'label' => 'Languages',
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
                'admin_code' => 'admin.cv_language'
            ])
            ->add('otherInformation', 'textarea', array(
                'label' => 'Other information',
                'required' => false,
                'help' => 'Не более 1000 символов'
            ))
            ->add('professionalSkill', 'textarea', array(
                'label' => 'Professional skill',
                'required' => false,
                'help' => 'Не более 1000 символов'
            ))
            ->add('driverLicenseCategories', 'entity', [
                'label' => 'Driver license category',
                'class' => 'RDWCvBundle:DriverLicenseCategory',
                'property' => 'title',
                'expanded' => true,
                'multiple' => true,
                'required' => false,
            ])
            ->add('haveCar', 'choice', [
                'label' => 'Have car',
                'choices' => [
                    true => 'Yes',
                ],
                'expanded' => true,
                'multiple' => false,
                'required' => false,
            ])
            ->add('validTill', 'date', [
                'label' => 'Срок действия до',
                'input' => 'datetime',
                'required' => false,
            ])
            ->add('businessTrip', 'entity', [
                'label' => 'Business trip possibilities',
                'class' => 'RDWCvBundle:BusinessTrip',
                'required' => false,
                'property' => 'title',
                'empty_value' => 'Choose option',
            ])
            ->add('computerSkill', 'textarea', [
                'label' => 'Computer skill',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Compupter skill placeholder',
                ],
            ])
            ->add('children', 'number', [
                'label' => 'Children',
                'required' => false,
                'attr' => ['placeholder' => 'Children placeholder'],
            ])
            ->add('youtubeVideo', 'text', [
                'label' => 'Video',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Video placeholder',
                ],
            ])
            ->add('maritalStatus', 'entity', [
                'class' => 'RDWUserBundle:MaritalStatus',
                'empty_value' => 'Choose option',
                'property' => 'title',
                'required' => false,
                'label' => 'Marital status',
            ])
            ->add('employmentType', 'entity', [
                'class' => 'RDWJobBundle:EmploymentType',
                'empty_value' => 'Not important',
                'property' => 'title',
                'required' => false,
                'label' => 'Employment type',
            ])
            ->add('confidential', 'checkbox', [
                'label' => 'Make CV confidential',
                'required' => false,
            ])
            ->add('mainInfoHidden', 'checkbox', [
                'label' => 'Hide my contact info',
                'required' => false,
            ])
            ->add('socialInfoHidden', 'checkbox', [
                'label' => 'Hide my social info',
                'required' => false,
            ])
            ->add('status', 'choice', array(
                'choices' => array('inactive' => 'Не размещенa', 'active' => 'Размещена', 'blocked' => 'Заблокированные'),
            ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        //$datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('name')
            ->add("surname")
            ->add("email")
            ->add('position', 'entity', array(
                'property' => 'title',
                'class' => 'RDW\Bundle\JobBundle\Entity\Position',
                'required' => true
            ))
            ->add("status", 'choice', array(
                'choices' => array('inactive' => 'Не размещенa', 'active' => 'Размещена', 'blocked' => 'Заблокированные'),
            ));

        $listMapper->add('_action', 'actions', array('actions' => array(
            'edit'       => array(),
            'delete'     => array('label' => 'delete')
        )));
    }
}