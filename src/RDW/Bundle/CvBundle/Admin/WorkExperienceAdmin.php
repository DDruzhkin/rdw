<?php

namespace RDW\Bundle\CvBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use RDW\Bundle\CvBundle\Form\Transformer\IncompleteDateTransformer;

class WorkExperienceAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add($formMapper->create('dateFrom', 'rdw_year_month', [
                'input'  => 'datetime',
                'format' => 'yyyy-MMMMdd',
                'years'  => range(date('Y'), date('Y') - 30, -1),
                'required' => false,
                'label' => 'Start date',
                'empty_value' => ['year' => 'Year', 'month' => 'Month'],
            ])->addViewTransformer(new IncompleteDateTransformer())
            )
            ->add($formMapper->create('dateTo', 'rdw_year_month', [
                    'input'  => 'datetime',
                    'format' => 'yyyy-MMMMdd',
                    'years'  => range(date('Y'), date('Y') - 30, -1),
                    'required' => false,
                    'label' => 'End date',
                    'empty_value' => ['year' => 'Year', 'month' => 'Month'],
                ])->addViewTransformer(new IncompleteDateTransformer())
            )
            ->add('companyTitle', 'text', [
                'label' => 'Company title',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Company title placeholder',
                ],
            ])
            ->add('scope', 'entity', [
                'class' => 'RDWUserBundle:Scope',
                'empty_value' => 'Choose option',
                'property' => 'title',
                'required' => false,
                'label' => 'Scope',
            ])
            ->add('position', 'text', [
                'label' => 'Work experience position',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Work experience position placeholder',
                ],
            ])
            ->add('additionalInfo', 'textarea', [
                'required' => false,
                'label' => 'Work experience info',
                'attr' => [
                    'placeholder' => 'Work experience info placeholder',
                ],
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        //$datagridMapper->add('id');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
        ;

        $listMapper->add('_action', 'actions', array('actions' => array(
            'edit'       => array(),
            'delete'     => array('label' => 'delete')
        )));
    }
}