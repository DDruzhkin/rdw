<?php

namespace RDW\Bundle\CvBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\ManageBundle\Entity\Filter;

/**
 * Class DriverLicenseCategory
 *
 * @package RDW\Bundle\CvBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="driver_license_categories")
 */
class DriverLicenseCategory extends Filter
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="RDW\Bundle\CvBundle\Entity\Cv", mappedBy="driverLicenseCategories", fetch="EXTRA_LAZY")
     */
    protected $cvs;

    /**
     * constructor
     */
    public function __construct()
    {
        $this->cvs = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function isDeletable()
    {
        return ! ($this->cvs->count() > 0 || $this->isSystem());
    }
}
