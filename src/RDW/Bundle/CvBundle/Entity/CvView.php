<?php

namespace RDW\Bundle\CvBundle\Entity;

use RDW\Bundle\JobBundle\Entity\View;
use RDW\Bundle\UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class Cv View
 *
 * @package RDW\Bundle\JobBundle\Entity
 * @author  Paulius Aleliūnas <paulius@eface.lt>
 *
 * @ORM\Table(name="cv_viewer")
 * @ORM\Entity(repositoryClass="RDW\Bundle\CvBundle\Repository\CvViewRepository")
 */
class CvView extends View
{
    /**
     * @var Cv
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\CvBundle\Entity\Cv", inversedBy="usersWhoViewed")
     * @ORM\JoinColumn(name="cv_id", referencedColumnName="id")
     */
    protected $cv;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\RegisteredUser", inversedBy="cvsViewed")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * Constructor
     *
     * @param Cv   $cv
     * @param User $user
     */
    public function __construct(Cv $cv, User $user)
    {
        $this->cv = $cv;
        $this->user = $user;
    }

    /**
     * @return Cv
     */
    public function getCv()
    {
        return $this->cv;
    }

    /**
     * @param Cv $cv
     *
     * @return $this
     */
    public function setCv($cv)
    {
        $this->cv = $cv;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }
}
