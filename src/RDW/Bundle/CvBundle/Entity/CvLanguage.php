<?php

namespace RDW\Bundle\CvBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\JobBundle\Entity\Language;
use RDW\Bundle\JobBundle\Entity\LanguageLevel;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class CvLanguage
 *
 * @package RDW\Bundle\CvBundle\Entity
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 *
 * @ORM\Table(name="cv_languages")
 * @ORM\Entity()
 */
class CvLanguage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Cv
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\CvBundle\Entity\Cv", inversedBy="languages")
     * @ORM\JoinColumn(name="cv_id", referencedColumnName="id")
     */
    private $cv;

    /**
     * @var Language
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\JobBundle\Entity\Language", inversedBy="cvLanguages", cascade={"persist", "merge"})
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     *
     * @Assert\NotBlank(message="Choose an option", groups={"create"})
     */
    private $language;

    /**
     * @var LanguageLevel
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\JobBundle\Entity\LanguageLevel", inversedBy="cvLevels", cascade={"persist", "merge"})
     * @ORM\JoinColumn(name="level_id", referencedColumnName="id")
     *
     * @Assert\NotBlank(message="Choose an option", groups={"create"})
     */
    private $level;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Cv $cv
     *
     * @return $this
     */
    public function setCv(Cv $cv)
    {
        $this->cv = $cv;

        return $this;
    }

    /**
     * @return Cv
     */
    public function getCv()
    {
        return $this->cv;
    }

    /**
     * @param Language $language
     *
     * @return $this
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @return Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param LanguageLevel $level
     *
     * @return $this
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * @return LanguageLevel
     */
    public function getLevel()
    {
        return $this->level;
    }
}
