<?php

namespace RDW\Bundle\CvBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\AppBundle\Entity\CreatedAtAwareInterface;
use RDW\Bundle\AppBundle\Entity\Traits\ContactPhonesTrait;
use RDW\Bundle\JobBundle\Entity\Position;
use RDW\Bundle\JobBundle\Entity\ProfessionalArea;
use RDW\Bundle\JobBundle\Entity\WorkScheduleType;
use RDW\Bundle\LocationBundle\Entity\City;
use RDW\Bundle\OrderBundle\Entity\OrderItemCv;
use RDW\Bundle\ServiceBundle\Entity\HighlightedItemInterface;
use RDW\Bundle\ServiceBundle\Entity\TopItemInterface;
use RDW\Bundle\UserBundle\Entity\MaritalStatus;
use RDW\Bundle\UserBundle\Entity\Scope;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Gedmo\Mapping\Annotation as Gedmo;
use RDW\Bundle\JobBundle\Entity\EmploymentType;
use RDW\Bundle\ManageBundle\Entity\User as ManageUser;
use RDW\Bundle\AppBundle\Entity\HighlightedTrait;
use RDW\Bundle\AppBundle\Entity\TopTrait;
use RDW\Bundle\AppBundle\Entity\TotalViewsCountTrait;
use RDW\Bundle\AppBundle\Entity\SluggifyTrait;
use RDW\Bundle\AppBundle\Entity\AbstractAd;
use RDW\Bundle\JobBundle\Entity\WorkExperienceType;
use RDW\Bundle\AppBundle\Entity\AdInterface;

/**
 * Class Cv
 *
 * @package RDW\Bundle\CvBundle\Entity
 *
 * @ORM\Table(
 *      name="cvs",
 *      indexes={
 *          @ORM\Index(name="slug_idx", columns={"slug"})
 *      }
 * )
 * @ORM\Entity(repositoryClass = "RDW\Bundle\CvBundle\Repository\CvRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\HasLifecycleCallbacks()
 */
class Cv extends AbstractAd implements HighlightedItemInterface, TopItemInterface, AdInterface, CreatedAtAwareInterface
{
    use HighlightedTrait;
    use TopTrait;
    use SluggifyTrait;
    use TotalViewsCountTrait;
    use ContactPhonesTrait;

    const SECRET_FIELD_STRING = '*****';

    const STATUS_ACTIVE       = 'active';
    const STATUS_INACTIVE     = 'inactive';
    const STATUS_DELETED      = 'deleted';
    const STATUS_BLOCKED      = 'blocked';

    const DRIVER_LICENSE_A = 'a';
    const DRIVER_LICENSE_B = 'b';
    const DRIVER_LICENSE_C = 'c';

    const BUSINESS_TRIP_YES     = 1;
    const BUSINESS_TRIP_NO      = 2;
    const BUSINESS_TRIP_DISCUSS = 3;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=20, nullable=true, name="status")
     *
     * @Assert\NotBlank(message="Status cannot be empty", groups={"create"})
     */
    private $status;

    /**
     * @var RegisteredUser
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\RegisteredUser", inversedBy="cvs", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     *
     * @Assert\NotBlank(message="Name cannot be empty", groups={"create"})
     * @Assert\Regex(pattern="/^[a-zA-Z\p{Cyrillic}\s]+$/u", message="Cannot contain a number", groups={"create"})
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $surname;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $patronymic;

    /**
     * @var string
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $gender;

    /**
     * @var \DateTime
     * @ORM\Column(type="date", nullable=true)
     */
    protected $birthday;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=false, name="valid_till")
     *
     * @Assert\NotNull(message="Field cannot be empty", groups={"create"})
     * @Assert\DateTime(groups={"create"})
     */
    private $validTill;

    /**
     * @ORM\ManyToMany(targetEntity="RDW\Bundle\LocationBundle\Entity\City", cascade={"persist", "merge"})
     */
    protected $cities;

    /**
     * @var City
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\LocationBundle\Entity\City", inversedBy="cvs", cascade={"persist", "merge"})
     * @ORM\JoinColumn(name="working_place_city_id", referencedColumnName="id")
     */
    protected $workingPlaceCity;

    /**
     * @var Position
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\JobBundle\Entity\Position", inversedBy="cvs", cascade={"persist", "merge"})
     * @ORM\JoinColumn(name="position_id", referencedColumnName="id", nullable=false)
     *
     * @Assert\NotBlank(message="Position cannot be empty", groups={"create"})
     */
    protected $position;

    /**
     * @var ProfessionalArea
     *
     * @ORM\ManyToMany(
     *     targetEntity="RDW\Bundle\JobBundle\Entity\ProfessionalArea",
     *     cascade={"persist"},
     *     fetch="EXTRA_LAZY",
     *     inversedBy="cvs"
     * )
     * @ORM\JoinTable(name="cv_professionalarea",
     *      joinColumns={@ORM\JoinColumn(name="cv_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="professionalarea_id", referencedColumnName="id")}
     *      )
     * @Assert\NotBlank(message="Professional Area cannot be empty", groups={"create", "publish", "censor", "import"})
     */
    private $professionalArea;


    /**
     * @var Scope
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\Scope", inversedBy="cvs", cascade={"persist", "merge"})
     * @ORM\JoinColumn(name="scope_id", referencedColumnName="id")
     */
    private $scope;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="photo")
     */
    protected $photo;

    /**
     * @var string
     *
     * @ORM\Column(name="block_reason", type="text", nullable=true)
     * @Assert\NotBlank(message="Please enter block reason", groups={"block"})
     */
    private $blockReason;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="document")
     */
    protected $document;

    /**
     * @var string
     */
    private $oldStatus;

    /**
     * @var \DateTime $updatedAt
     *
     * @ORM\Column(type="datetime", nullable=true, name="updated_at")
     */
    protected $updatedAt;

    /**
     * @var \DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\CvBundle\Entity\Education", mappedBy="cv", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $educations;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\CvBundle\Entity\CvMessage", mappedBy="cv", cascade={"persist", "merge"})
     */
    private $cvMessages;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\CvBundle\Entity\WorkExperience", mappedBy="cv", cascade={"persist", "remove", "merge"}, orphanRemoval=true)
     * @ORM\OrderBy({"dateFrom" = "DESC", "dateTo" = "DESC"})
     */
    private $workExperience;

    /**
     * @var WorkExperienceType
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\JobBundle\Entity\WorkExperienceType", inversedBy="cvs", cascade={"persist", "merge"})
     * @ORM\JoinColumn(name="work_experience_type_id", referencedColumnName="id")
     */
    private $workExperienceType;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\CvBundle\Entity\CvLanguage", mappedBy="cv", cascade={"persist", "remove", "merge"}, orphanRemoval=true)
     */
    private $languages;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="youtube_video")
     */
    protected $youtubeVideo;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\CvBundle\Entity\ContactPersonPhone", mappedBy="entity", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $contactPersonPhones;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="fb_contact")
     */
    protected $fbContact;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="vk_contact")
     */
    protected $vkContact;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="ok_contact")
     */
    protected $okContact;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="g_plus_contact")
     */
    protected $gPlusContact;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="twitter_contact")
     */
    protected $twitterContact;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="linked_in_contact")
     */
    protected $linkedInContact;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="skype_contact")
     */
    protected $skypeContact;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false, name="email")
     *
     * @Assert\Email(message="Enter valid email address", groups={"create"})
     * @Assert\NotBlank(message="Email is blank", groups={"create"})
     */
    protected $email;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true, name="salary_from")
     * @Assert\NotBlank(message="Salary cannot be empty", groups={"create"})
     * @Assert\Regex(pattern="/\d+/", message="The value {{ value }} is not a valid {{ type }}.", groups={"create"})
     * @Assert\GreaterThan(value = 0, groups={"create"})
     * @Assert\Length(
     *      max = "8",
     *      maxMessage = "Salary cannot be longer than {{ limit }} characters length",
     *      groups={"create"}
     * )
     */
    protected $salaryFrom;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true, name="salary_to")
     * @Assert\Regex(pattern="/\d+/", message="The value {{ value }} is not a valid {{ type }}.", groups={"create"})
     * @Assert\GreaterThanOrEqual(value = 0, groups={"create"})
     * @Assert\Length(
     *      max = "8",
     *      maxMessage = "Salary cannot be longer than {{ limit }} characters length",
     *      groups={"create"}
     * )
     */
    protected $salaryTo;

    /**
     * @var WorkScheduleType
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\JobBundle\Entity\WorkScheduleType", inversedBy="cvs", cascade={"persist", "merge"})
     * @ORM\JoinColumn(name="work_schedule_type_id", referencedColumnName="id")
     */
    private $workScheduleType;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true, name="computer_skill")
     * @Assert\Length(
     *      max = 1000,
     *      maxMessage = "Computer skills cannot be longer than {{ limit }} characters",
     *      groups={"create"}
     * )
     */
    private $computerSkill;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="RDW\Bundle\CvBundle\Entity\DriverLicenseCategory", inversedBy="cvs", cascade={"persist", "merge"}, indexBy="id")
     * @ORM\JoinTable(name="cv_driver_licences",
     *      joinColumns={@ORM\JoinColumn(name="cv_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="driver_licence_category_id", referencedColumnName="id")}
     * )
     */
    private $driverLicenseCategories;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true, name="have_car")
     */
    private $haveCar;

    /**
     * @var \RDW\Bundle\CvBundle\Entity\BusinessTrip
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\CvBundle\Entity\BusinessTrip", inversedBy="cvs", cascade={"persist", "merge"})
     * @ORM\JoinColumn(name="business_trip_id", referencedColumnName="id", nullable=true)
     */
    private $businessTrip;

    /**
     * @var string
     * @ORM\Column(type="smallint", nullable=true, name="driver_license_year")
     *
     * @Assert\Type(type="integer", message="Only digits", groups={"create"})
     * @Assert\Range(
     *      min = 0,
     *      minMessage = "Minimum allowed value {{ limit }}",
     *      groups={"create"}
     * )
     */
    private $driverLicenseYear;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true, name="other_information")
     *
     * @Assert\Length(
     *      max = "1000",
     *      maxMessage = "Other information cannot be longer than {{ limit }} characters length",
     *      groups={"create"}
     * )
     */
    private $otherInformation;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true, name="professional_skill")
     *
     * @Assert\Length(
     *      max = "1000",
     *      maxMessage = "Professional skills cannot be longer than {{ limit }} characters length",
     *      groups={"create"}
     * )
     */
    private $professionalSkill;

    /**
     * @var MaritalStatus
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\MaritalStatus", inversedBy="cvs", cascade={"persist", "merge"})
     * @ORM\JoinColumn(name="marital_status_id", referencedColumnName="id")
     */
    private $maritalStatus;

    /**
     * @var string
     * @ORM\Column(type="integer", nullable=true, name="children")
     *
     * @Assert\Range(
     *      min = 0,
     *      minMessage = "Minimum allowed value {{ limit }}",
     *      groups={"create"}
     * )
     */
    private $children;

    /**
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\CvBundle\Entity\CvView", mappedBy="cv", cascade={"persist"})
     */
    protected $usersWhoViewed;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\OrderBundle\Entity\OrderItemCv", mappedBy="item", cascade={"persist"})
     */
    protected $services;

    /**
     * @var int
     * @ORM\Column(type="integer", name="views_count", nullable=true, options={"default" = 0})
     */
    protected $viewsCount;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", name="views_count_updated_at", nullable=true)
     */
    protected $viewsCountUpdatedAt;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\CvBundle\Entity\Training", mappedBy="cv", cascade={"persist", "remove", "merge"}, orphanRemoval=true)
     */
    private $trainings;

    /**
     * @var EmploymentType
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\JobBundle\Entity\EmploymentType", inversedBy="cvs", cascade={"persist", "merge"})
     * @ORM\JoinColumn(name="employment_type_id", referencedColumnName="id")
     */
    private $employmentType;

    /**
     * @var int
     *
     * @ORM\Column(name="foreign_id", type="integer", nullable=true)
     */
    private $foreignId;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_confidential", type="boolean", nullable=true)
     */
    private $confidential;

    /**
     * @var bool
     *
     * @ORM\Column(name="main_info_hidden", type="boolean", nullable=true)
     */
    private $mainInfoHidden;

    /**
     * @var bool
     *
     * @ORM\Column(name="social_info_hidden", type="boolean", nullable=true)
     */
    private $socialInfoHidden;

    /**
     * @var null|UserInterface
     */
    private $viewer;

    /**
     * Constructor
     *
     * @param UserInterface|null $viewer
     */
    public function __construct(UserInterface $viewer = null)
    {
        parent::__construct();

        $this->viewer = $viewer;

        $this->contactPersonPhones = new ArrayCollection();
        $this->cities = new ArrayCollection();
        $this->professionalArea = new ArrayCollection();
        $this->cvMessages = new ArrayCollection();
        $this->educations = new ArrayCollection();
        $this->workExperience = new ArrayCollection();
        $this->languages = new ArrayCollection();
        $this->driverLicenseCategories = new ArrayCollection();
        $this->trainings = new ArrayCollection();

        $this->validTill = new \DateTime();
        $this->validTill->add(new \DateInterval('P1Y'));
        $this->viewsCount = 0;

        $this->status = self::STATUS_INACTIVE;

        $this->children = null;
    }

    /**
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @return string
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * @param string $photo
     *
     * @return $this
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function setUserPhoto()
    {
        $user = $this->getUser();

        if (null === $this->getPhoto() && $user instanceof RegisteredUser && null !== $user->getPhoto()) {
            $this->setPhoto($user->getPhoto());
        }
    }

    /**
     * @param string $document
     *
     * @return $this
     */
    public function setDocument($document)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPublic()
    {
        return ($this->getStatus() == self::STATUS_ACTIVE && ! $this->isConfidential()) ? true : false;
    }

    /**
     * @return ArrayCollection
     */
    public function getEducations()
    {
        return $this->educations;
    }

    /**
     * @return ArrayCollection
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @return ArrayCollection
     */
    public function getWorkExperience()
    {
        return $this->workExperience;
    }

    /**
     * @return bool
     */
    public function hasWorkExperience()
    {
        return ! $this->workExperience->isEmpty();
    }

    /**
     * @param Education|null $education
     *
     * @return $this
     */
    public function addEducation($education)
    {
        if ($education && ! $this->educations->contains($education)) {
            $education->setCv($this);
            $this->educations->add($education);
        }

        return $this;
    }

    /**
     * @param CvLanguage $language
     *
     * @return $this
     */
    public function addLanguage($language)
    {
        if ($language && ! $this->languages->contains($language)) {
            $language->setCv($this);
            $this->languages->add($language);
        }

        return $this;
    }

    /**
     * @param ArrayCollection $workExperiences
     *
     * @return $this
     */
    public function setWorkExperience($workExperiences)
    {
        $this->workExperience = $workExperiences;

        return $this;
    }

    /**
     * @param WorkExperience|null $workExperience
     *
     * @return $this
     */
    public function addWorkExperience($workExperience)
    {
        if ($workExperience && ! $this->workExperience->contains($workExperience)) {
            $workExperience->setCv($this);
            $this->workExperience->add($workExperience);
        }

        return $this;
    }

    /**
     * @param Education $education
     *
     * @return $this
     */
    public function removeEducation(Education $education)
    {
        $this->educations->removeElement($education);

        return $this;
    }

    /**
     * @param CvLanguage $language
     *
     * @return $this
     */
    public function removeLanguage(CvLanguage $language)
    {
        $this->languages->removeElement($language);

        return $this;
    }

    /**
     * @param WorkExperience $workExperience
     *
     * @return $this
     */
    public function removeWorkExperience(WorkExperience $workExperience)
    {
        $this->workExperience->removeElement($workExperience);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @param City $city
     *
     * @return self
     */
    public function addCity($city)
    {
        if ($city && ! $this->cities->contains($city)) {
            $this->cities->add($city);
        }

        return $this;
    }

    /**
     * @param City $city
     *
     * @return $this
     */
    public function removeCity(City $city)
    {
        $this->cities->removeElement($city);

        return $this;
    }

    /**
     * @param ArrayCollection $cities
     *
     * @return self
     */
    public function setCities($cities)
    {
        $this->cities = $cities;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->showMainInfo() ? $this->surname : self::SECRET_FIELD_STRING;
    }

    /**
     * @param string $surname
     *
     * @return $this
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     *
     * @return $this
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param \DateTime $birthday
     *
     * @return $this
     */
    public function setBirthday(\DateTime $birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * @return string
     */
    public function getYoutubeVideo()
    {
        return $this->showMainInfo() ? $this->youtubeVideo : self::SECRET_FIELD_STRING;
    }

    /**
     * @param string $youtubeVideo
     *
     * @return $this
     */
    public function setYoutubeVideo($youtubeVideo)
    {
        $this->youtubeVideo = $youtubeVideo;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhonesAsString()
    {
        if (! $this->showMainInfo()) {
            return self::SECRET_FIELD_STRING;
        }

        return $this->getContactPersonPhonesAsString();
    }

    /**
     * @return string
     */
    public function getSkypeContact()
    {
        return $this->showSocialInfo() ? $this->skypeContact : self::SECRET_FIELD_STRING;
    }

    /**
     * @return string
     */
    public function getFbContact()
    {
        return $this->showSocialInfo() ? $this->fbContact : self::SECRET_FIELD_STRING;
    }

    /**
     * @return string
     */
    public function getVkContact()
    {
        return $this->showSocialInfo() ? $this->vkContact : self::SECRET_FIELD_STRING;
    }

    /**
     * @return string
     */
    public function getOkContact()
    {
        return $this->showSocialInfo() ? $this->okContact : self::SECRET_FIELD_STRING;
    }

    /**
     * @return string
     */
    public function getGPlusContact()
    {
        return $this->showSocialInfo() ? $this->gPlusContact : self::SECRET_FIELD_STRING;
    }

    /**
     * @return string
     */
    public function getTwitterContact()
    {
        return $this->showSocialInfo() ? $this->twitterContact : self::SECRET_FIELD_STRING;
    }

    /**
     * @return string
     */
    public function getLinkedInContact()
    {
        return $this->showSocialInfo() ? $this->linkedInContact : self::SECRET_FIELD_STRING;
    }

    /**
     * @param string $skypeContact
     *
     * @return $this
     */
    public function setSkypeContact($skypeContact)
    {
        $this->skypeContact = $skypeContact;

        return $this;
    }

    /**
     * @param string $fbContact
     *
     * @return $this
     */
    public function setFbContact($fbContact)
    {
        $this->fbContact = $fbContact;

        return $this;
    }

    /**
     * @param string $vkContact
     *
     * @return $this
     */
    public function setVkContact($vkContact)
    {
        $this->vkContact = $vkContact;

        return $this;
    }

    /**
     * @param string $okContact
     *
     * @return $this
     */
    public function setOkContact($okContact)
    {
        $this->okContact = $okContact;

        return $this;
    }

    /**
     * @param string $gPlusContact
     *
     * @return $this
     */
    public function setGPlusContact($gPlusContact)
    {
        $this->gPlusContact = $gPlusContact;

        return $this;
    }

    /**
     * @param string $twitterContact
     *
     * @return $this
     */
    public function setTwitterContact($twitterContact)
    {
        $this->twitterContact = $twitterContact;

        return $this;
    }

    /**
     * @param string $linkedIdContact
     *
     * @return $this
     */
    public function setLinkedInContact($linkedIdContact)
    {
        $this->linkedInContact = $linkedIdContact;

        return $this;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->showMainInfo() ? $this->email : self::SECRET_FIELD_STRING;
    }

    /**
     * @param Scope $scope
     *
     * @return $this
     */
    public function setScope($scope)
    {
        $this->scope = $scope;

        return $this;
    }

    /**
     * @return Scope
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * @return ProfessionalArea
     */
    public function getProfessionalArea()
    {
        return $this->professionalArea;
    }

    /**
     * @param ProfessionalArea $professionalArea
     *
     * @return $this
     */
    public function setProfessionalArea($professionalArea)
    {
        $this->professionalArea = $professionalArea;

        return $this;
    }

    /**
     * @return WorkScheduleType
     */
    public function getWorkScheduleType()
    {
        return $this->workScheduleType;
    }

    /**
     * @param WorkScheduleType $workScheduleType
     *
     * @return $this
     */
    public function setWorkScheduleType(WorkScheduleType $workScheduleType = null)
    {
        $this->workScheduleType = $workScheduleType;

        return $this;
    }

    /**
     * @return MaritalStatus
     */
    public function getMaritalStatus()
    {
        return $this->maritalStatus;
    }

    /**
     * @param MaritalStatus $maritalStatus
     *
     * @return $this
     */
    public function setMaritalStatus(MaritalStatus $maritalStatus = null)
    {
        $this->maritalStatus = $maritalStatus;

        return $this;
    }

    /**
     * @param string $otherInformation
     *
     * @return $this
     */
    public function setOtherInformation($otherInformation)
    {
        $this->otherInformation = $otherInformation;

        return $this;
    }

    /**
     * @return string
     */
    public function getOtherInformation()
    {
        return $this->showMainInfo() ? $this->otherInformation : self::SECRET_FIELD_STRING;
    }

    /**
     * @return ArrayCollection
     */
    public function getDriverLicenseCategories()
    {
        return $this->driverLicenseCategories;
    }

    /**
     * @param ArrayCollection $categories
     *
     * @return $this
     */
    public function setDriverLicenseCategories(ArrayCollection $categories)
    {
        $this->driverLicenseCategories = $categories;

        return $this;
    }

    /**
     * @param DriverLicenseCategory $category
     *
     * @return $this
     */
    public function addDriverLicenseCategories(DriverLicenseCategory $category)
    {
        $this->driverLicenseCategories->add($category);

        return $this;
    }

    /**
     * @param DriverLicenseCategory $category
     *
     * @return $this
     */
    public function removeDriverLicenseCategories(DriverLicenseCategory $category)
    {
        $this->driverLicenseCategories->remove($category);

        return $this;
    }

    /**
     * @param int $year
     *
     * @return $this
     */
    public function setDriverLicenseYear($year)
    {
        $this->driverLicenseYear = $year;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasVideo()
    {
        return ($this->getYoutubeVideo() != '') ? true : false;
    }

    /**
     * @return string
     */
    public function getDriverLicenseYear()
    {
        return $this->driverLicenseYear;
    }

    /**
     * @return string
     */
    public function getComputerSkill()
    {
        return $this->computerSkill;
    }

    /**
     * @param string $computerSkill
     *
     * @return $this
     */
    public function setComputerSkill($computerSkill)
    {
        $this->computerSkill = $computerSkill;

        return $this;
    }

    /**
     * @return array
     */
    public static function getStatusesChoiceListForUser()
    {
        return [
            self::STATUS_INACTIVE => 'Inactive',
            self::STATUS_ACTIVE => 'Active',
        ];
    }

    /**
     * @return array
     */
    public static function getStatusesChoiceListForAdmin()
    {
        return [
            self::STATUS_INACTIVE => 'Inactive',
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_BLOCKED => 'Blocked',
        ];
    }

    /**
     * @param string $children
     *
     * @return $this
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * @return string
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param string $skill
     *
     * @return $this
     */
    public function setProfessionalSkill($skill)
    {
        $this->professionalSkill = $skill;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfessionalSkill()
    {
        return $this->professionalSkill;
    }

    /**
     * @return \RDW\Bundle\CvBundle\Entity\BusinessTrip
     */
    public function getBusinessTrip()
    {
        return $this->businessTrip;
    }

    /**
     * @param \RDW\Bundle\CvBundle\Entity\BusinessTrip $businessTrip
     *
     * @return $this
     */
    public function setBusinessTrip(BusinessTrip $businessTrip = null)
    {
        $this->businessTrip = $businessTrip;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return $this
     */
    public function setOldStatus($status)
    {
        $this->oldStatus = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getOldStatus()
    {
        return $this->oldStatus;
    }

    /**
     * @param string $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getValidTill()
    {
        return $this->validTill;
    }

    /**
     * @param \DateTime $validTill
     *
     * @return $this
     */
    public function setValidTill(\DateTime $validTill = null)
    {
        $this->validTill = $validTill;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPublished()
    {
        return (self::STATUS_ACTIVE === $this->getStatus() && ! $this->confidential);
    }

    /**
     * @return string
     */
    public function getBlockReason()
    {
        return $this->blockReason;
    }

    /**
     * @param string $blockReason
     *
     * @return $this
     */
    public function setBlockReason($blockReason)
    {
        $this->blockReason = $blockReason;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     *
     * @return $this
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return !is_null($this->getDeletedAt());
    }

    /**
     * @return ArrayCollection|CvMessage[]
     */
    public function getCvMessages()
    {
        return $this->cvMessages;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->getPosition()->getTitle();
    }

    /**
     * Returns last employer company title
     *
     * @return null|WorkExperience
     */
    public function getLastEmployer()
    {
        if ($this->getWorkExperience()->count() > 0) {
            return $this->getWorkExperience()->first();
        }

        return null;
    }

    /**
     * @return bool
     */
    public function isStatusActive()
    {
        return ($this->getStatus() == self::STATUS_ACTIVE) ? true : false;
    }

    /**
     * @return int|null
     */
    public function getAge()
    {
        if (null === $this->getBirthday()) {
            return null;
        }

        $oDateNow = new \DateTime();
        $oDateInterval = $oDateNow->diff($this->getBirthday());

        return (int) $oDateInterval->y;
    }

    /**
     * @return boolean
     */
    public function isHaveCar()
    {
        return $this->haveCar;
    }

    /**
     * @param boolean $haveCar
     *
     * @return $this
     */
    public function setHaveCar($haveCar)
    {
        $this->haveCar = $haveCar;

        return $this;
    }

    /**
     * @return string
     */
    public function getPatronymic()
    {
        return $this->showMainInfo() ? $this->patronymic : self::SECRET_FIELD_STRING;
    }

    /**
     * @param string $patronymic
     *
     * @return $this
     */
    public function setPatronymic($patronymic)
    {
        $this->patronymic = $patronymic;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getTrainings()
    {
        return $this->trainings;
    }

    /**
     * @param Training|null $training
     *
     * @return $this
     */
    public function addTraining($training)
    {
        if ($training && ! $this->trainings->contains($training)) {
            $training->setCv($this);
            $this->trainings->add($training);
        }

        return $this;
    }

    /**
     * @param Training $training
     *
     * @return $this
     */
    public function removeTraining(Training $training)
    {
        $this->trainings->removeElement($training);

        return $this;
    }

    /**
     * @return EmploymentType
     */
    public function getEmploymentType()
    {
        return $this->employmentType;
    }

    /**
     * @param EmploymentType $employmentType
     *
     * @return $this
     */
    public function setEmploymentType($employmentType)
    {
        $this->employmentType = $employmentType;

        return $this;
    }

    /**
     * @return int
     */
    public function getCvMessagesCount()
    {
        $total = 0;

        foreach ($this->getCvMessages() as $message) {
            if ($message->getUser() instanceof RegisteredUser) {
                $total++;
            }
        }

        return $total;
    }

    /**
     * @return int
     */
    public function getForeignId()
    {
        return $this->foreignId;
    }

    /**
     * @param int $foreignId
     */
    public function setForeignId($foreignId)
    {
        $this->foreignId = $foreignId;
    }

    /**
     * @return boolean
     */
    public function isConfidential()
    {
        return $this->confidential;
    }

    /**
     * @param boolean $confidential
     *
     * @return $this
     */
    public function setConfidential($confidential)
    {
        $this->confidential = $confidential;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isMainInfoHidden()
    {
        return $this->mainInfoHidden;
    }

    /**
     * @param boolean $mainInfoHidden
     *
     * @return $this
     */
    public function setMainInfoHidden($mainInfoHidden)
    {
        $this->mainInfoHidden = $mainInfoHidden;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isSocialInfoHidden()
    {
        return $this->socialInfoHidden;
    }

    /**
     * @param boolean $socialInfoHidden
     *
     * @return $this
     */
    public function setSocialInfoHidden($socialInfoHidden)
    {
        $this->socialInfoHidden = $socialInfoHidden;

        return $this;
    }

    /**
     * @return null|UserInterface
     */
    public function getViewer()
    {
        return $this->viewer;
    }

    /**
     * @param null|UserInterface $viewer
     *
     * @return $this
     */
    public function setViewer(UserInterface $viewer = null)
    {
        $this->viewer = $viewer;

        return $this;
    }

    /**
     * @return bool
     */
    public function isStatusChangeableForUser()
    {
        return (in_array($this->status, array_keys(self::getStatusesChoiceListForUser())));
    }

    /**
     * @return bool
     */
    private function showMainInfo()
    {
        return (! $this->isMainInfoHidden() || $this->hasViewerAccessToHiddenInfo());
    }

    private function hasViewerAccessToHiddenInfo()
    {
        $return = false;
        $viewer = $this->getViewer();

        if ($viewer instanceof ManageUser
            || $viewer instanceof RegisteredUser && $viewer->getId() == $this->getUser()->getId()
        ) {
            $return = true;
        }

        return $return;
    }

    /**
     * @return bool
     */
    private function showSocialInfo()
    {
        return (! $this->isSocialInfoHidden() || $this->hasViewerAccessToHiddenInfo());
    }

    /**
     * @return array
     */
    public function getStatusLabels()
    {
        return [
            self::STATUS_INACTIVE => 'Cv inactive',
            self::STATUS_ACTIVE => 'Cv active',
            self::STATUS_BLOCKED => 'Blocked',
            self::STATUS_DELETED => 'Deleted',
        ];
    }

    /**
     * @return string
     */
    public function getStatusLabel()
    {
        $labels = $this->getStatusLabels();

        if (array_key_exists($this->status, $labels)) {
            return $labels[$this->status];
        }

        return 'status_undefined';
    }

    /**
     * @return WorkExperienceType
     */
    public function getWorkExperienceType()
    {
        return $this->workExperienceType;
    }

    /**
     * @param WorkExperienceType $workExperienceType
     */
    public function setWorkExperienceType($workExperienceType)
    {
        $this->workExperienceType = $workExperienceType;
    }

    public function isAllowedToDeleteByUser(RegisteredUser $user)
    {
        return $this->isOwner($user);
    }

    /**
     * @return ContactPersonPhone
     */
    public static function createContactPersonPhone()
    {
        return new ContactPersonPhone();
    }


    /**
     * Increment view cv
     */
    public function incView()
    {
        $this->viewsCount++;

        return $this;
    }

    public function __toString() {

        return $this->getName()." ".$this->getSurname();
    }
}
