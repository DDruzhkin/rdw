<?php

namespace RDW\Bundle\CvBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ContactPersonPhone
 *
 * @package RDW\Bundle\JobBundle\Entity
 *
 * @ORM\Entity
 */
class ContactPersonPhone extends \RDW\Bundle\AppBundle\Entity\ContactPersonPhone
{
    /**
     * @var Cv
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\CvBundle\Entity\Cv", inversedBy="contactPersonPhones")
     * @ORM\JoinColumn(name="discriminator_id", referencedColumnName="id")
     */
    protected $entity;
}
