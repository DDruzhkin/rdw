<?php

namespace RDW\Bundle\CvBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\ManageBundle\Entity\Filter;

/**
 * Class MaritalStatus
 *
 * @package RDW\Bundle\CvBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="business_trips")
 */
class BusinessTrip extends Filter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\CvBundle\Entity\Cv", mappedBy="businessTrip", fetch="EXTRA_LAZY")
     */
    protected $cvs;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function isDeletable()
    {
        return ! ($this->cvs->count() > 0 || $this->isSystem());
    }
}
