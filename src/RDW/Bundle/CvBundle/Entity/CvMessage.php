<?php

namespace RDW\Bundle\CvBundle\Entity;

use RDW\Bundle\CvBundle\Model\MultipleCvMessage;
use Symfony\Component\Validator\Constraints as Assert;
use RDW\Bundle\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class CvMessage
 *
 * @ORM\Entity
 * @ORM\Table(name="cv_messages")
 *
 * @package RDW\Bundle\CvBundle\Model
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class CvMessage extends MultipleCvMessage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="company_title", type="text")
     * @Assert\NotBlank(message="Company title cannot be empty")
     */
    private $companyTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_name", type="text")
     * @Assert\NotBlank(message="Name cannot be empty")
     */
    private $contactName;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_email", type="text")
     *
     * @Assert\NotBlank(message="Email cannot be empty")
     * @Assert\Email(message="Email must be a valid email address")
     */
    private $contactEmail;

    /**
     * @var Cv
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\CvBundle\Entity\Cv", inversedBy="cvMessages")
     * @ORM\JoinColumn(name="cv_id", referencedColumnName="id", nullable=true)
     *
     * @Assert\NotBlank(groups={"apply_my"})
     */
    protected $cv;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\User", inversedBy="cvMessages")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="text", nullable=true)
     * @Assert\Regex(
     *      pattern="/^([\+0-9\s\-]+)$/",
     *      message="Field must be a valid phone number and can have only numbers, +, - and spaces"
     * )
     */
    private $contactPhone;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Message cannot be empty")
     */
    private $message;

    /**
     * @return string
     */
    public function getCompanyTitle()
    {
        return $this->companyTitle;
    }

    /**
     * @param string $companyTitle
     *
     * @return $this
     */
    public function setCompanyTitle($companyTitle)
    {
        $this->companyTitle = $companyTitle;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactName()
    {
        return $this->contactName;
    }

    /**
     * @param string $contactName
     *
     * @return $this
     */
    public function setContactName($contactName)
    {
        $this->contactName = $contactName;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * @param string $contactEmail
     *
     * @return $this
     */
    public function setContactEmail($contactEmail)
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactPhone()
    {
        return $this->contactPhone;
    }

    /**
     * @param string $contactPhone
     *
     * @return $this
     */
    public function setContactPhone($contactPhone)
    {
        $this->contactPhone = $contactPhone;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \RDW\Bundle\CvBundle\Entity\Cv
     */
    public function getCv()
    {
        return $this->cv;
    }

    /**
     * @param \RDW\Bundle\CvBundle\Entity\Cv $cv
     *
     * @return $this
     */
    public function setCv(Cv $cv)
    {
        $this->cv = $cv;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }
}
