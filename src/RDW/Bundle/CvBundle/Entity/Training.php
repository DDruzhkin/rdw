<?php

namespace RDW\Bundle\CvBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Education
 *
 * @package RDW\Bundle\CvBundle\Entity
 * @author  Paulius Aleliūnas <paulius@eface.lt>
 *
 * @ORM\Entity
 */
class Training extends BaseEducation
{
    /**
     * @var Cv
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\CvBundle\Entity\Cv", inversedBy="trainings", cascade={"persist"})
     * @ORM\JoinColumn(name="cv_id", referencedColumnName="id")
     */
    private $cv;

    /**
     * @param Cv $cv
     *
     * @return $this
     */
    public function setCv(Cv $cv)
    {
        $this->cv = $cv;

        return $this;
    }

    /**
     * @return Cv
     */
    public function getCv()
    {
        return $this->cv;
    }
}
