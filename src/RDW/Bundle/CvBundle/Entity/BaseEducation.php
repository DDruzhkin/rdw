<?php

namespace RDW\Bundle\CvBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\JobBundle\Entity\EducationType;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class BaseEducation
 *
 * @package RDW\Bundle\CvBundle\Entity
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 *
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"education" = "Education", "training" = "Training"})
 *
 * @ORM\Table(name="cv_educations")
 * @ORM\HasLifecycleCallbacks()
 */
class BaseEducation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="date", nullable=true, name="date_from")
     */
    private $dateFrom;

    /**
     * @var \DateTime
     * @ORM\Column(type="date", nullable=true, name="date_to")
     */
    private $dateTo;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true, name="additional_info")
     * @Assert\Length(
     *      max = "1000",
     *      maxMessage = "Field cannot be longer than {{ limit }} characters length",
     *      groups={"create", "create_training"}
     * )
     */
    private $additionalInfo;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="institution", nullable=true)
     */
    private $institution;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="faculty", nullable=true)
     */
    private $faculty;

    /**
     * @var EducationType
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\JobBundle\Entity\EducationType", inversedBy="educations", cascade={"persist"})
     * @ORM\JoinColumn(name="education_type_id", referencedColumnName="id")
     * @Assert\NotBlank(message="Choose an option", groups={"create"})
     */
    private $educationType;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * @param string $institution
     *
     * @return $this
     */
    public function setInstitution($institution)
    {
        $this->institution = $institution;

        return $this;
    }

    /**
     * @return string
     */
    public function getFaculty()
    {
        return $this->faculty;
    }

    /**
     * @param string $faculty
     *
     * @return $this
     */
    public function setFaculty($faculty)
    {
        $this->faculty = $faculty;

        return $this;
    }

    /**
     * @param EducationType $type
     *
     * @return $this
     */
    public function setEducationType(EducationType $type)
    {
        $this->educationType = $type;

        return $this;
    }

    /**
     * @return EducationType
     */
    public function getEducationType()
    {
        return $this->educationType;
    }

    /**
     * @param \DateTime $dateFrom
     *
     * @return $this
     */
    public function setDateFrom(\DateTime $dateFrom = null)
    {
        $this->dateFrom = $dateFrom;

        return $this;
    }

    /**
     * @param \DateTime $to
     *
     * @return $this
     */
    public function setDateTo(\DateTime $to = null)
    {
        $this->dateTo = $to;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * @return string
     */
    public function getAdditionalInfo()
    {
        return $this->additionalInfo;
    }

    /**
     * @param string $additionalInfo
     *
     * @return $this
     */
    public function setAdditionalInfo($additionalInfo)
    {
        $this->additionalInfo = $additionalInfo;

        return $this;
    }
}
