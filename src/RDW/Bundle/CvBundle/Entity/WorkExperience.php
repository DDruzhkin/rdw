<?php

namespace RDW\Bundle\CvBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\UserBundle\Entity\Scope;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class WorkExperience
 *
 * @package RDW\Bundle\CvBundle\Entity
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 *
 * @ORM\Table(name="cv_work_experiences")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class WorkExperience
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="date", nullable=true, name="date_from")
     */
    private $dateFrom;

    /**
     * @var \DateTime
     * @ORM\Column(type="date", nullable=true, name="date_to")
     */
    private $dateTo;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true, name="additional_info")
     * @Assert\Length(
     *      max = "1000",
     *      maxMessage = "Field cannot be longer than {{ limit }} characters length",
     *      groups={"create"}
     * )
     */
    private $additionalInfo;

    /**
     * @var Cv
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\CvBundle\Entity\Cv", inversedBy="workExperience")
     * @ORM\JoinColumn(name="cv_id", referencedColumnName="id")
     */
    private $cv;

    /**
     * @var Scope
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\Scope", cascade={"persist"})
     * @ORM\JoinColumn(name="scope_id", referencedColumnName="id")
     */
    private $scope;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="company_title")
     *
     * @Assert\NotBlank(message="Company title cannot be empty", groups={"create"})
     */
    private $companyTitle;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="position")
     *
     * @Assert\NotBlank(message="Position cannot be empty", groups={"create"})
     */
    private $position;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="work_type")
     */
    private $workType;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Cv $cv
     *
     * @return $this
     */
    public function setCv(Cv $cv)
    {
        $this->cv = $cv;

        return $this;
    }

    /**
     * @return Cv
     */
    public function getCv()
    {
        return $this->cv;
    }

    /**
     * @param \DateTime $dateFrom
     *
     * @return $this
     */
    public function setDateFrom(\DateTime $dateFrom = null)
    {
        $this->dateFrom = $dateFrom;

        return $this;
    }

    /**
     * @param \DateTime $to
     *
     * @return $this
     */
    public function setDateTo(\DateTime $to = null)
    {
        $this->dateTo = $to;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * @param Scope $scope
     *
     * @return $this
     */
    public function setScope(Scope $scope = null)
    {
        $this->scope = $scope;

        return $this;
    }

    /**
     * @return Scope
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * @return string
     */
    public function getCompanyTitle()
    {
        return $this->companyTitle;
    }

    /**
     * @param string $companyTitle
     *
     * @return self
     */
    public function setCompanyTitle($companyTitle)
    {
        $this->companyTitle = $companyTitle;

        return $this;
    }

    /**
     * @return string
     */
    public function getWorkType()
    {
        return $this->workType;
    }

    /**
     * @param string $workType
     *
     * @return self
     */
    public function setWorkType($workType)
    {
        $this->workType = $workType;

        return $this;
    }

    /**
    * @return string
    */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param string $position
     *
     * @return self
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return string
     */
    public function getAdditionalInfo()
    {
        return $this->additionalInfo;
    }

    /**
     * @param string $additionalInfo
     *
     * @return $this
     */
    public function setAdditionalInfo($additionalInfo)
    {
        $this->additionalInfo = $additionalInfo;

        return $this;
    }

}
