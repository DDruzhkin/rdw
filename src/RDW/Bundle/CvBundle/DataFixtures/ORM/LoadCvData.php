<?php

namespace RDW\Bundle\CvBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\CvBundle\Entity\Education;
use RDW\Bundle\CvBundle\Entity\WorkExperience;
use RDW\Bundle\JobBundle\DataFixtures\ORM\LoadPositionData;
use RDW\Bundle\JobBundle\DataFixtures\ORM\LoadWorkScheduleTypeData;
use RDW\Bundle\JobBundle\Entity\Position;
use RDW\Bundle\JobBundle\Entity\WorkScheduleType;
use RDW\Bundle\LocationBundle\DataFixtures\ORM\LoadCityData;
use RDW\Bundle\UserBundle\DataFixtures\ORM\LoadMaritalStatusData;
use RDW\Bundle\UserBundle\DataFixtures\ORM\LoadScopeData;
use RDW\Bundle\UserBundle\DataFixtures\ORM\LoadUserData;
use RDW\Bundle\UserBundle\Entity\MaritalStatus;
use RDW\Bundle\UserBundle\Entity\Scope;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Faker\Factory as FakerFactory;
use Faker\Generator;

/**
 * Class LoadCvData
 *
 * @package RDW\Bundle\CvBundle\DataFixtures\ORM
 */
class LoadCvData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * Faker.
     *
     * @var Generator
     */
    protected $faker;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->faker = FakerFactory::create();
    }

    function getDependencies()
    {
        return [
            LoadUserData::class,
            LoadPositionData::class,
            LoadScopeData::class,
            LoadEducationData::class,
            LoadWorkExperienceData::class,
            LoadPositionData::class,
            LoadWorkScheduleTypeData::class,
            LoadBusinessTripData::class,
            LoadMaritalStatusData::class,
            LoadDriverLicenseCategoryData::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $entityManager)
    {
        $numberOfCvs = 10;
        $genders = ['m', 'f'];

        $positions = $entityManager->getRepository(Position::class)->findAll();

        foreach ($positions as $position) {

            for ($i = 1; $i <= $numberOfCvs; $i++) {
                /** @var $user RegisteredUser */
                $user = $this->getReference(LoadUserData::REFERENCE_STRING . rand(1, LoadUserData::NUMBER_OF_EMPLOYEES));

                $cv = new Cv();
                $cv->setUser($user);
                $cv->setStatus(array_rand(Cv::getStatusesChoiceListForUser()));
                $cv->setName($user->getName());
                $cv->setSurname($user->getSurname());
                $cv->setGender($genders[rand(0, 1)]);
                $cv->setBirthday($user->getBirthday());
                $cv->setCities([
                    $this->getReference(LoadCityData::REFERENCE_STRING . rand(1, LoadCityData::COUNT))
                ]);
                $cv->setWorkingPlaceCity(
                    $this->getReference(LoadCityData::REFERENCE_STRING . rand(1, LoadCityData::COUNT))
                );
                $cv->setCreatedAt(new \DateTime());
                $cv->addContactPersonPhoneByString($this->faker->phoneNumber);
                $cv->setYoutubeVideo('http://youtu.be/ArpKAtnlIk0?t=13s');
                $cv->setFbContact($this->faker->url);
                $cv->setVkContact($this->faker->url);
                $cv->setOkContact($this->faker->url);
                $cv->setGPlusContact($this->faker->url);
                $cv->setTwitterContact($this->faker->url);
                $cv->setLinkedInContact($this->faker->url);
                $cv->setSkypeContact($this->faker->url);
                $cv->setEmail($user->getEmail());
                $cv->setSalaryFrom(rand(2000, 4000));
                $cv->setSalaryTo(rand(6000, 8000));
                $cv->setDriverLicenseYear(rand(1998, 2008));
                $cv->setOtherInformation($this->faker->text(300));
                $cv->setProfessionalSkill($this->faker->text(50));
                $cv->setChildren(rand(0, 4));

                $validTillValues = ['+1year', '+3months', '+1month'];
                $cv->setValidTill(new \DateTime($validTillValues[array_rand($validTillValues)]));

                $cv->setPosition($position);

                /** @var $scope Scope */
                $scope = $this->getReference(LoadScopeData::REFERENCE_STRING . rand(1, LoadScopeData::COUNT));
                $cv->setScope($scope);

                /** @var $education Education */
                $education = $this->getReference(
                    LoadEducationData::REFERENCE_STRING . rand(1, LoadEducationData::COUNT)
                );
                $cv->addEducation($education);

                /** @var $workExperience WorkExperience */
                $workExperience = $this->getReference(
                    LoadWorkExperienceData::REFERENCE_STRING . rand(1, LoadWorkExperienceData::COUNT)
                );
                $cv->addWorkExperience($workExperience);

                /** @var $workScheduleType WorkScheduleType */
                $workScheduleType = $this->getReference(
                    LoadWorkScheduleTypeData::REFERENCE_STRING . rand(1, LoadWorkScheduleTypeData::COUNT)
                );
                $cv->setWorkScheduleType($workScheduleType);

                $computerSkills = ['good', 'excelent', 'poor'];
                $cv->setComputerSkill($computerSkills[array_rand($computerSkills)]);

                $businessTrip = $this->getReference(
                    LoadBusinessTripData::REFERENCE_STRING . rand(1, LoadBusinessTripData::COUNT)
                );
                $cv->setBusinessTrip($businessTrip);

                /** @var $maritalStatus MaritalStatus */
                $maritalStatus = $this->getReference(
                    LoadMaritalStatusData::REFERENCE_STRING . rand(1, LoadMaritalStatusData::COUNT)
                );
                $cv->setMaritalStatus($maritalStatus);

                $driverLicenseCategory = $this->getReference(
                    LoadDriverLicenseCategoryData::REFERENCE_STRING . rand(
                        1,
                        LoadDriverLicenseCategoryData::CATEGORIES_COUNT - 1
                    )
                );
                $cv->addDriverLicenseCategories($driverLicenseCategory);

                $entityManager->persist($cv);
            }

            $entityManager->flush();
        }
    }
}
