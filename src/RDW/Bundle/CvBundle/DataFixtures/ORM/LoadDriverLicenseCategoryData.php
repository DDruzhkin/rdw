<?php

namespace RDW\Bundle\CvBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\CvBundle\Entity\DriverLicenseCategory;

/**
 * Class LoadDriverLicenseCategoryData
 *
 * @package RDW\Bundle\CvBundle\DataFixtures\ORM
 */
class LoadDriverLicenseCategoryData extends AbstractFixture
{
    const CATEGORIES_COUNT = 6;
    const REFERENCE_STRING = 'driver_license_category_';
    const COUNT = 5;

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $entityManager)
    {
        $categories = ['A', 'B1', 'B2', 'C1', 'E', 'F'];

        for ($i = 0; $i < self::CATEGORIES_COUNT; $i++) {
            $category = new DriverLicenseCategory();
            $category->setTitle($categories[$i]);

            $this->addReference(self::REFERENCE_STRING . $i, $category);
            $entityManager->persist($category);
        }

        $entityManager->flush();
    }
}
