<?php

namespace RDW\Bundle\CvBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use RDW\Bundle\CvBundle\Entity\Education;
use Faker\Factory as FakerFactory;
use Faker\Generator;
use RDW\Bundle\JobBundle\DataFixtures\ORM\LoadEducationTypeData;
use RDW\Bundle\JobBundle\Entity\EducationType;

/**
 * Class LoadEducationData
 *
 * @package RDW\Bundle\CvBundle\DataFixtures\ORM
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class LoadEducationData extends AbstractFixture implements DependentFixtureInterface
{
    const REFERENCE_STRING = 'rdw.education.';
    const COUNT = 5;

    /**
     * Faker.
     *
     * @var Generator
     */
    protected $faker;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->faker = FakerFactory::create();
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $entityManager)
    {
        $educationsNumber = self::COUNT;

        for ($i = 1; $i <= $educationsNumber; $i++) {
            $education = new Education();
            $education->setAdditionalInfo($this->faker->text(100));

            $dateFromValues = ['-10year', '-2years'];
            $dateFromIndex = array_rand($dateFromValues);
            $education->setDateFrom(new \DateTime($dateFromValues[$dateFromIndex]));

            $dateToValues = ['-3years', 'now'];
            $education->setDateTo(new \DateTime($dateToValues[$dateFromIndex]));

            /** @var $educationType EducationType */
            $educationType = $this->getReference(
                LoadEducationTypeData::REFERENCE_STRING . rand(1, LoadEducationTypeData::COUNT)
            );
            $education->setEducationType($educationType);

            $education->setInstitution($this->faker->text(100));
            $education->setFaculty($this->faker->text(100));

            $this->addReference(self::REFERENCE_STRING . $i, $education);

            $entityManager->persist($education);
        }

        $entityManager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    function getDependencies()
    {
        return [
            LoadEducationTypeData::class,
        ];
    }
}
