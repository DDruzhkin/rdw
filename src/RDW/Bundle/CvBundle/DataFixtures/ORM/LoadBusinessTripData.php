<?php

namespace RDW\Bundle\CvBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use RDW\Bundle\CvBundle\Entity\BusinessTrip;

/**
 * Class LoadWorkExperienceData
 *
 * @package RDW\Bundle\CvBundle\DataFixtures\ORM
 */
class LoadBusinessTripData extends AbstractFixture
{
    const REFERENCE_STRING = 'rdw.business_trip.';
    const COUNT = 3;

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $entityManager)
    {
        $titles = [
            1 => 'Possible',
            2 => 'Once per year',
            3 => 'No',
        ];

        foreach ($titles as $key => $title) {
            $businessTrip = new BusinessTrip();
            $businessTrip->setTitle($title);

            $this->addReference(self::REFERENCE_STRING . $key, $businessTrip);

            $entityManager->persist($businessTrip);
        }

        $entityManager->flush();
    }
}
