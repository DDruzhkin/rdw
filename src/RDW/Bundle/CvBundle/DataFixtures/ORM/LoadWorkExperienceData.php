<?php

namespace RDW\Bundle\CvBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use RDW\Bundle\CvBundle\Entity\WorkExperience;
use Faker\Factory as FakerFactory;
use Faker\Generator;
use RDW\Bundle\JobBundle\Entity\Position;
use RDW\Bundle\UserBundle\DataFixtures\ORM\LoadScopeData;
use RDW\Bundle\UserBundle\Entity\Scope;

/**
 * Class LoadWorkExperienceData
 *
 * @package RDW\Bundle\CvBundle\DataFixtures\ORM
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class LoadWorkExperienceData extends AbstractFixture implements DependentFixtureInterface
{
    const REFERENCE_STRING = 'rdw.work_experience.';
    const COUNT = 5;

    /**
     * Faker.
     *
     * @var Generator
     */
    protected $faker;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->faker = FakerFactory::create();
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $entityManager)
    {
        $workExperienceNumber = 5;

        for ($i = 1; $i <= $workExperienceNumber; $i++) {
            $workExperience = new WorkExperience();
            $workExperience->setAdditionalInfo($this->faker->text(100));
            $workExperience->setCompanyTitle(implode(' ', $this->faker->words(rand(3, 5))));
            $workExperience->setWorkType(implode(' ', $this->faker->words(2)));

            $dateFromValues = ['-1year', '-2years', '-3years', '-4years'];
            $dateFromIndex = array_rand($dateFromValues);
            $workExperience->setDateFrom(new \DateTime($dateFromValues[$dateFromIndex]));

            $dateToValues = ['now', '-1years', '-2years', '-3years'];
            $workExperience->setDateTo(new \DateTime($dateToValues[$dateFromIndex]));

            /** @var $scope Scope */
            $scope = $this->getReference(LoadScopeData::REFERENCE_STRING . rand(1, LoadScopeData::COUNT));
            $workExperience->setScope($scope);

            /** @var $position Position */
//            $position = $this->getReference('rdw.position.' . rand(0, 4));
//            $workExperience->setPosition($position);
            $workExperience->setPosition(implode(' ', $this->faker->words(rand(1, 3))));

            $this->addReference(self::REFERENCE_STRING . $i, $workExperience);

            $entityManager->persist($workExperience);
        }

        $entityManager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    function getDependencies()
    {
        return [
            LoadScopeData::class,
        ];
    }
}
