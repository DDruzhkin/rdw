<?php

namespace RDW\Bundle\CvBundle\EventListener;

use Braincrafted\Bundle\BootstrapBundle\Session\FlashMessage;
use RDW\Bundle\ConversationBundle\Entity\Conversation;
use RDW\Bundle\ConversationBundle\Entity\Message;
use RDW\Bundle\ConversationBundle\Service\ConversationManager;
use RDW\Bundle\CvBundle\Entity\CvMessage;
use RDW\Bundle\CvBundle\Event\CvMessageEvent;
use RDW\Bundle\HistoryBundle\Entity\History;
use RDW\Bundle\HistoryBundle\Event\HistoryEvent;
use RDW\Bundle\HistoryBundle\RDWHistoryEvents;
use RDW\Bundle\HistoryBundle\Service\HistoryManager;
use RDW\Bundle\MailerBundle\Service\MailerManager;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class SaveCvMessageListener
 *
 * @package RDW\Bundle\CvBundle\EventListener
 */
class SaveCvMessageListener
{
    /**
     * @var \RDW\Bundle\ConversationBundle\Service\ConversationManager;
     */
    private $conversationManager;

    /**
     * @var \Braincrafted\Bundle\BootstrapBundle\Session\FlashMessage
     */
    private $flashMessage;

    /**
     * @var \RDW\Bundle\MailerBundle\Service\MailerManager
     */
    private $mailerManager;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var HistoryManager
     */
    private $historyManager;

    /**
     * @param ConversationManager      $conversationManager
     * @param FlashMessage             $flashMessage
     * @param MailerManager            $mailerManager
     * @param EventDispatcherInterface $eventDispatcher
     * @param HistoryManager           $historyManager
     */
    public function __construct(
        ConversationManager $conversationManager,
        FlashMessage $flashMessage,
        MailerManager $mailerManager,
        EventDispatcherInterface $eventDispatcher,
        HistoryManager $historyManager
    )
    {
        $this->conversationManager = $conversationManager;
        $this->flashMessage        = $flashMessage;
        $this->mailerManager       = $mailerManager;
        $this->eventDispatcher     = $eventDispatcher;
        $this->historyManager      = $historyManager;
    }

    /**
     * @param CvMessageEvent $event
     *
     * @throws \Symfony\Component\Validator\Exception\UnexpectedTypeException
     */
    public function onCvMessageSave(CvMessageEvent $event)
    {
        $this->checkEventObjects($event);

        $cvMessage = $event->getMessage();
        $cv = $cvMessage->getCv();
        $user = $event->getUser();
        $receiver = $cv->getUser();

        // create new conversation
        $conversation = new Conversation();
        $conversation->setTitle($cv->getPosition()->getTitle());
        $conversation->setCvMessage($cvMessage);
        $conversation->setReceiver($receiver);
        $conversation->setSender($user);
        $conversation->setCreatedAt(new \DateTime());

        // create message for conversation
        $message = new Message();
        $message->setConversation($conversation);
        $message->setReceiver($receiver);
        $message->setSender($user);
        $message->setCreatedAt(new \DateTime());
        $message->setContent($cvMessage->getMessage());
        $conversation->addMessage($message);
        $conversation->setUpdatedAt(new \DateTime());

        $this->conversationManager->save($conversation);

        if ($user instanceof RegisteredUser) {
            $this->flashMessage->success('Your message sent. Thank You.');

            $this->historyManager->logEvent($user, History::ACTION_REPLY, $cv, RDWHistoryEvents::REPLY);
        }
    }

    /**
     * @param CvMessageEvent $event
     *
     * @return bool
     * @throws \Symfony\Component\Validator\Exception\UnexpectedTypeException
     */
    private function checkEventObjects(CvMessageEvent $event)
    {
        $message = $event->getMessage();

        if (! $message instanceof CvMessage) {
            throw new UnexpectedTypeException($message, 'RDW\Bundle\CvBundle\Model\CvMessage');
        }

        $user = $event->getUser();

        if (! $user instanceof User) {
            throw new UnexpectedTypeException($user, 'RDW\Bundle\UserBundle\Entity\User');
        }

        return true;
    }
}
