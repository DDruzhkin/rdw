<?php

namespace RDW\Bundle\CvBundle\EventListener;

use Braincrafted\Bundle\BootstrapBundle\Session\FlashMessage;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\CvBundle\Event\BlockEvent;
use RDW\Bundle\CvBundle\Service\CvManager;
use RDW\Bundle\MailerBundle\Service\MailerManager;
use RDW\Bundle\ManageBundle\Entity\User;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class CvBlockListener
 *
 * @package RDW\Bundle\CvBundle\EventListener
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class CvBlockListener
{
    /**
     * @var
     */
    private $cvManager;

    /**
     * @var \Braincrafted\Bundle\BootstrapBundle\Session\FlashMessage
     */
    private $flashMessage;

    /**
     * @var \RDW\Bundle\MailerBundle\Service\MailerManager
     */
    private $mailerManager;

    /**
     * @param CvManager     $cvManager
     * @param FlashMessage  $flashMessage
     * @param MailerManager $mailerManager
     */
    public function __construct(
        CvManager $cvManager,
        FlashMessage $flashMessage,
        MailerManager $mailerManager
    )
    {
        $this->cvManager = $cvManager;
        $this->flashMessage = $flashMessage;
        $this->mailerManager = $mailerManager;
    }

    /**
     * @param BlockEvent $event
     *
     * @throws \Symfony\Component\Validator\Exception\UnexpectedTypeException
     */
    public function onCvBlock(BlockEvent $event)
    {
        $cv = $event->getCv();

        if ( ! $cv instanceof Cv) {
            throw new UnexpectedTypeException($cv, 'RDW\Bundle\CvBundle\Entity\Cv');
        }

        $user = $event->getUser();

        if ( ! $user instanceof User) {
            throw new UnexpectedTypeException($cv, 'RDW\Bundle\ManageBundle\Entity\User');
        }

        // always set status as blocked
        $cv->setStatus(Cv::STATUS_BLOCKED);

        $this->cvManager->update($cv);
        $this->flashMessage->success('Cv was blocked');

        $this->mailerManager->sendEmailForUserCvBlocked($cv, $user);
    }
}
