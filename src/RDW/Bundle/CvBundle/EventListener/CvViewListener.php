<?php

namespace RDW\Bundle\CvBundle\EventListener;

use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\CvBundle\Entity\CvView;
use RDW\Bundle\CvBundle\Event\UserEvent;
use RDW\Bundle\CvBundle\Service\CvManager;
use RDW\Bundle\HistoryBundle\Entity\History;
use RDW\Bundle\HistoryBundle\Event\HistoryEvent;
use RDW\Bundle\HistoryBundle\RDWHistoryEvents;
use RDW\Bundle\HistoryBundle\Service\HistoryManager;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;

/**
 * Class JobViewListener
 *
 * @package RDW\Bundle\CvBundle\EventListener
 */
class CvViewListener
{
    /**
     * @var \RDW\Bundle\CvBundle\Service\CvManager
     */
    private $cvManager;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var HistoryManager
     */
    private $historyManager;

    /**
     * @param CvManager                $cvManager
     * @param EventDispatcherInterface $eventDispatcher
     * @param HistoryManager           $historyManager
     */
    public function __construct(CvManager $cvManager, EventDispatcherInterface $eventDispatcher, HistoryManager $historyManager)
    {
        $this->cvManager       = $cvManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->historyManager  = $historyManager;
    }

    /**
     * @param UserEvent $event
     *
     * @return null|void
     * @throws UnexpectedTypeException
     */
    public function onCvView(UserEvent $event)
    {
        $user = $event->getUser();

        $cv = $event->getCv();

        if (! $cv instanceof Cv) {
            throw new UnexpectedTypeException($cv, 'RDW\Bundle\CvBundle\Entity\Cv');
        }

        $cvId = $cv->getId();

        $isViewedToday = isset($_COOKIE["show_c_id_$cvId"]) ? 1 : 0;

        if (!$isViewedToday) {
            if ($user) {
                if (! $user->isManager() && $cv->getUser() != $user && ! $this->cvManager->existsView($cv, $user)) {
                    $cv->addUserWhoViewed(new CvView($cv, $user));
                    $this->historyManager->logEvent($user, History::ACTION_VIEW, $cv, RDWHistoryEvents::VIEW_OBJECT);
                }
            } elseif ($user == null) {
                $cv->incView();
            }
            setcookie("show_c_id_" . $cv->getId(), 1 ,time()+86400); // 24 hours
        }

        $this->cvManager->update($cv);
    }
}
