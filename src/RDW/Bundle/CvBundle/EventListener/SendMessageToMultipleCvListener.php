<?php

namespace RDW\Bundle\CvBundle\EventListener;

use Braincrafted\Bundle\BootstrapBundle\Session\FlashMessage;
use Doctrine\Common\Collections\ArrayCollection;
use RDW\Bundle\ConversationBundle\Entity\Conversation;
use RDW\Bundle\ConversationBundle\Entity\Message;
use RDW\Bundle\ConversationBundle\Service\ConversationManager;
use RDW\Bundle\CvBundle\Event\MultipleCvMessageEvent;
use RDW\Bundle\CvBundle\Model\MultipleCvMessage;
use RDW\Bundle\HistoryBundle\Entity\History;
use RDW\Bundle\HistoryBundle\Event\HistoryEvent;
use RDW\Bundle\HistoryBundle\RDWHistoryEvents;
use RDW\Bundle\HistoryBundle\Service\HistoryManager;
use RDW\Bundle\MailerBundle\Service\MailerManager;
use RDW\Bundle\UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class SendMessageToMultipleCvListener
 *
 * @package RDW\Bundle\CvBundle\EventListener
 */
class SendMessageToMultipleCvListener
{
    /**
     * @var \RDW\Bundle\ConversationBundle\Service\ConversationManager;
     */
    private $conversationManager;

    /**
     * @var \Braincrafted\Bundle\BootstrapBundle\Session\FlashMessage
     */
    private $flashMessage;

    /**
     * @var \RDW\Bundle\MailerBundle\Service\MailerManager
     */
    private $mailerManager;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var HistoryManager
     */
    private $historyManager;

    /**
     * @param ConversationManager      $conversationManager
     * @param FlashMessage             $flashMessage
     * @param MailerManager            $mailerManager
     * @param EventDispatcherInterface $eventDispatcher
     * @param HistoryManager           $historyManager
     */
    public function __construct(
        ConversationManager $conversationManager,
        FlashMessage $flashMessage,
        MailerManager $mailerManager,
        EventDispatcherInterface $eventDispatcher,
        HistoryManager $historyManager
    )
    {
        $this->conversationManager = $conversationManager;
        $this->flashMessage        = $flashMessage;
        $this->mailerManager       = $mailerManager;
        $this->eventDispatcher     = $eventDispatcher;
        $this->historyManager      = $historyManager;
    }

    /**
     * @param MultipleCvMessageEvent $event
     *
     * @throws \Symfony\Component\Validator\Exception\UnexpectedTypeException
     */
    public function onMessageSave(MultipleCvMessageEvent $event)
    {
        $this->checkEventObjects($event);

        $cvMessage = $event->getMessage();
        $cvs = $event->getCvs();
        $sender = $event->getUser();

        foreach ($cvs as $cv) {
            $receiver = $cv->getUser();

            // set cv for CvMessage
            $cvMessage->setCv($cv);
            $cvMessage->setUser($sender);

            // create new conversation
            $conversation = new Conversation();
            $conversation->setCvMessage($cvMessage);
            $conversation->setSender($sender);
            $conversation->setCreatedAt(new \DateTime());
            $conversation->setTitle($cv->getPosition()->getTitle());
            $conversation->setReceiver($receiver);

            // create message for conversation
            $message = new Message();
            $message->setConversation($conversation);
            $message->setReceiver($receiver);
            $message->setSender($sender);
            $message->setCreatedAt(new \DateTime());
            $message->setContent($cvMessage->getMessage());

            $conversation->addMessage($message);
            $conversation->setUpdatedAt(new \DateTime());

            $flush = true;//($cv == $cvs->last());

            $this->conversationManager->update($conversation, $flush);

            $this->historyManager->logEvent($sender, History::ACTION_REPLY, $cv, RDWHistoryEvents::REPLY);
        }

        $this->flashMessage->success('Your message sent. Thank You.');
    }

    /**
     * @param MultipleCvMessageEvent $event
     *
     * @return bool
     * @throws \Symfony\Component\Validator\Exception\UnexpectedTypeException
     */
    private function checkEventObjects(MultipleCvMessageEvent $event)
    {
        $message = $event->getMessage();

        if (! $message instanceof MultipleCvMessage) {
            throw new UnexpectedTypeException($message, 'RDW\Bundle\CvBundle\Model\MultipleCvMessage');
        }

        $cvs = $event->getCvs();

        if (! $cvs instanceof ArrayCollection) {
            throw new UnexpectedTypeException($cvs, 'Doctrine\Common\Collections\ArrayCollection');
        }

        $user = $event->getUser();

        if (! $user instanceof User) {
            throw new UnexpectedTypeException($user, 'RDW\Bundle\UserBundle\Entity\User');
        }

        return true;
    }
}
