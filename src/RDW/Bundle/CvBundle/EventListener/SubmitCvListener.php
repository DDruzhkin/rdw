<?php

namespace RDW\Bundle\CvBundle\EventListener;

use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\CvBundle\Event\CvEvent;
use RDW\Bundle\CvBundle\RDWCvEvents;
use RDW\Bundle\CvBundle\Service\CvManager;
use RDW\Bundle\UserBundle\Event\SingleUserEvent;
use RDW\Bundle\UserBundle\RDWUserEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class SubmitCvListener
 *
 * @package RDW\Bundle\JobBundle\EventListener
 */
class SubmitCvListener implements EventSubscriberInterface
{
    /**
     * @var CvManager
     */
    protected $cvManager;

    /**
     * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @param CvManager                $cvManager
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(CvManager $cvManager, EventDispatcherInterface $dispatcher)
    {
        $this->cvManager = $cvManager;
        $this->dispatcher = $dispatcher;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            RDWCvEvents::CV_SAVE => 'onSave',
            RDWCvEvents::CV_PUBLISH => 'onPublish',
        ];
    }

    /**
     * @param CvEvent $event
     */
    public function onSave(CvEvent $event)
    {
        $cv = $event->getCv();
        $cv->setStatus(Cv::STATUS_INACTIVE);

        $this->updateAndDispatch($cv);
    }

    /**
     * @param CvEvent $event
     */
    public function onPublish(CvEvent $event)
    {
        $cv = $event->getCv();
        $cv->setStatus(Cv::STATUS_ACTIVE);

        $this->updateAndDispatch($cv);
    }

    /**
     * @param Cv $cv
     */
    private function updateAndDispatch(Cv $cv)
    {
        $this->cvManager->update($cv);
        $this->dispatcher->dispatch(RDWUserEvents::UPDATE_USER_PROFILE_PERCENT, new SingleUserEvent($cv->getUser()));
    }
}
