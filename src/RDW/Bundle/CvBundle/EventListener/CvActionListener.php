<?php

namespace RDW\Bundle\CvBundle\EventListener;

use RDW\Bundle\CvBundle\Entity\Cv;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class CvActionListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::SUBMIT   => 'onSubmit',
        );
    }

    /**
     * @param FormEvent $event
     */
    public function onSubmit(FormEvent $event)
    {
        $entity = $event->getForm()->getData();

        if ($entity instanceof Cv) {
            $entity->setUpdatedAt(new \DateTime());
        }
    }
}