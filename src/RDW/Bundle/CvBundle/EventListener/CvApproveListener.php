<?php

namespace RDW\Bundle\CvBundle\EventListener;

use Braincrafted\Bundle\BootstrapBundle\Session\FlashMessage;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\CvBundle\Event\CvApproveEvent;
use RDW\Bundle\CvBundle\Service\CvManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class CvApproveListener
 *
 * @package RDW\Bundle\CvBundle\EventListener
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class CvApproveListener
{
    /**
     * @var \RDW\Bundle\CvBundle\Service\CvManager
     */
    private $cvManager;

    /**
     * @var \Braincrafted\Bundle\BootstrapBundle\Session\FlashMessage
     */
    private $flashMessage;

    /**
     * @var Router
     */
    private $router;

    /**
     * @param CvManager    $cvManager
     * @param FlashMessage $flashMessage
     * @param Router       $router
     */
    public function __construct(CvManager $cvManager, FlashMessage $flashMessage, Router $router)
    {
        $this->cvManager = $cvManager;
        $this->flashMessage = $flashMessage;
        $this->router = $router;
    }

    /**
     * @param CvApproveEvent $event
     *
     * @throws \Symfony\Component\Validator\Exception\UnexpectedTypeException
     */
    public function onCvApprove(CvApproveEvent $event)
    {
        $cv = $event->getCv();

        if ( ! $cv instanceof Cv) {
            throw new UnexpectedTypeException($cv, 'RDW\Bundle\CvBundle\Entity\Cv');
        }

        if ($cv->getStatus() == Cv::STATUS_BLOCKED) {
            // still not change status, we will updated it when submitting block cv form
            $cv->setStatus($cv->getOldStatus());
            $url = $this->router->generate('rdw_cv.cv_admin.block', ['id' => $cv->getId()]);
        } else {
            $this->flashMessage->success('Cv successfully updated');
            $url = $this->router->generate('rdw_cv.cv_admin.list');
        }

        $this->cvManager->update($cv);
        $event->setResponse(new RedirectResponse($url));
    }
}
