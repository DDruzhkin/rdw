<?php

namespace RDW\Bundle\CvBundle\Tests\Model;

use RDW\Bundle\CvBundle\Model\CvFilter;

class CvFilterTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function it_should_allow_set_nullable_working_place_city()
    {
        $filter = new CvFilter();
        $filter->setWorkingPlaceCity(null);

        $this->assertNull($filter->getWorkingPlaceCity());
    }
}
