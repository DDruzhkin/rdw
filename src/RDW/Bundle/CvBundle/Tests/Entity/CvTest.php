<?php

namespace RDW\Bundle\CvBundle\Tests\Entity;

use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\CvBundle\Entity\CvLanguage;
use RDW\Bundle\CvBundle\Entity\WorkExperience;
use RDW\Bundle\JobBundle\Entity\Position;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\Scope;
use RDW\Bundle\LocationBundle\Entity\City;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\ValidatorBuilder;

/**
 * Class CvTest
 *
 * @package RDW\Bundle\CvBundle\Tests\Entity
 */
class CvTest extends \PHPUnit_Framework_TestCase
{
    const CLASS_NAME = 'RDW\Bundle\CvBundle\Entity\Cv';

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $user;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $owner;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $admin;

    /**
     * set up
     */
    public function setUp()
    {
        $this->user = $this->getMock('\RDW\Bundle\UserBundle\Entity\RegisteredUser');
        $this->owner = $this->getMock('\RDW\Bundle\UserBundle\Entity\RegisteredUser');
        $this->admin = $this->getMock('\RDW\Bundle\ManageBundle\Entity\User');
    }

    /**
     * @test
     */
    public function it_should_return_last_employer_title_and_position()
    {
        $workExperience = new WorkExperience();
        $workExperience->setDateFrom(new \DateTime('2013-01-01'));
        $workExperience->setCompanyTitle('Company');
        $workExperience->setPosition('Position');
        $cv = $this->getCv();
        $cv->addWorkExperience($workExperience);

        $this->assertEquals('Company', $cv->getLastEmployer()->getCompanyTitle());
        $this->assertEquals('Position', $cv->getLastEmployer()->getPosition());
    }

    /**
     * @test
     */
    public function it_should_return_title()
    {
        $position = new Position();
        $position->setTitle('position');

        $cv = $this->getCv();
        $cv->setPosition($position);
        $this->assertEquals('position', $cv->getTitle());
    }

    /**
     * @test
     */
    public function it_should_return_null_if_cv_has_not_work_experience()
    {
        $cv = $this->getCv();
        $this->assertNull($cv->getLastEmployer());
    }

    /**
     * @test
     */
    public function it_should_implement_highlighted_item_interface()
    {
        $this->assertInstanceOf(self::CLASS_NAME, new Cv());
    }

    /**
     * Test languages
     */
    public function test_languages_add()
    {
        $cv = $this->getCv();

        $this->assertTrue($cv->getLanguages()->isEmpty());

        $language = new CvLanguage();

        $this->assertInstanceOf(self::CLASS_NAME, $cv->addLanguage($language));
        $this->assertNotTrue($cv->getLanguages()->isEmpty());
        $this->assertEquals($language, $cv->getLanguages()->last());
    }

    /**
     * @test
     */
    public function it_should_have_default_status_inactive()
    {
        $cv = $this->getCv();
        $this->assertEquals(Cv::STATUS_INACTIVE, $cv->getStatus());
    }

    /**
     * @test
     */
    public function main_info_should_be_visible_for_admins()
    {
        $cv = $this->getCv();
        $cv->setViewer($this->admin);
        $cv->setMainInfoHidden(true);

        $this->assertMainInfoVisible($cv);
    }

    /**
     * @test
     */
    public function main_info_should_not_be_visible_for_user_when_it_is_hidden()
    {
        $cv = $this->getCv();
        $cv->setMainInfoHidden(true);

        $this->assertMainInfoNotVisible($cv);
    }

    /**
     * @test
     */
    public function hidden_info_should_be_visible_for_owner_when_it_is_hidden()
    {
        $this->owner->expects($this->any())->method('getId')->willReturn(5);
        $this->user->expects($this->any())->method('getId')->willReturn(5);

        $cv = $this->getCv();
        $cv->setMainInfoHidden(true);
        $cv->setUser($this->user);
        $cv->setViewer($this->owner);

        $this->assertMainInfoVisible($cv);
        $this->assertSocialInfoVisible($cv);
    }

    /**
     * @test
     */
    public function social_info_should_be_visible_for_admins()
    {
        $cv = $this->getCv();
        $cv->setViewer($this->admin);
        $cv->setSocialInfoHidden(true);

        $this->assertSocialInfoVisible($cv);
    }

    /**
     * @test
     */
    public function social_info_should_not_be_visible_for_users_when_it_is_hidden()
    {
        $cv = $this->getCv();
        $cv->setSocialInfoHidden(true);

        $this->assertSocialInfoNotVisible($cv);
    }

    /**
     * @return Cv
     */
    private function getCv()
    {
        return new Cv();
    }

    private function assertSocialInfoNotVisible($cv)
    {
        $expectedValue = Cv::SECRET_FIELD_STRING;
        $value = 'Field value';

        $this->assertSocialInfo($cv, $expectedValue, $value);
    }

    private function assertSocialInfoVisible($cv)
    {
        $expectedValue = 'Field value';
        $value = 'Field value';

        $this->assertSocialInfo($cv, $expectedValue, $value);
    }

    private function assertSocialInfo($cv, $expectedValue, $value)
    {
        $cv->setSkypeContact($value);
        $this->assertEquals($expectedValue, $cv->getSkypeContact());

        $cv->setFbContact($value);
        $this->assertEquals($expectedValue, $cv->getFbContact());

        $cv->setVkContact($value);
        $this->assertEquals($expectedValue, $cv->getVkContact());

        $cv->setOkContact($value);
        $this->assertEquals($expectedValue, $cv->getOkContact());

        $cv->setGPlusContact($value);
        $this->assertEquals($expectedValue, $cv->getGPlusContact());

        $cv->setTwitterContact($value);
        $this->assertEquals($expectedValue, $cv->getTwitterContact());

        $cv->setLinkedInContact($value);
        $this->assertEquals($expectedValue, $cv->getLinkedInContact());
    }

    private function assertMainInfoNotVisible($cv)
    {
        $expectedValue = Cv::SECRET_FIELD_STRING;
        $value = 'Field value';

        $this->assertMainInfo($cv, $expectedValue, $value);
    }

    private function assertMainInfoVisible($cv)
    {
        $expectedValue = 'Field value';
        $value = 'Field value';

        $this->assertMainInfo($cv, $expectedValue, $value);
    }

    private function assertMainInfo($cv, $expectedValue, $value)
    {
        $cv->setSurname($value);
        $this->assertEquals($expectedValue, $cv->getSurname());

        $cv->setYoutubeVideo($value);
        $this->assertEquals($expectedValue, $cv->getYoutubeVideo());

        $cv->addContactPersonPhoneByString($value);
        $this->assertEquals($expectedValue, $cv->getPhonesAsString());

        $cv->setEmail($value);
        $this->assertEquals($expectedValue, $cv->getEmail());

        $cv->setOtherInformation($value);
        $this->assertEquals($expectedValue, $cv->getOtherInformation());

        $cv->setPatronymic($value);
        $this->assertEquals($expectedValue, $cv->getPatronymic());
    }

    /**
     * data provider
     *
     * @return array
     */
    public static function slugs()
    {
        $position = new Position();
        $position->setTitle('Position');
        $city = new City();
        $city->setTitle('city');
        $scope = new Scope();
        $scope->setTitle('Scope');

        return [
            ['position', 'Position', 'City', 'Scope'],
            ['position', 'Position', 'City'],
            ['position', 'Position', null, 'Scope'],
            ['position', 'Position', null, null],
            ['position-with-spaces', 'Position with spaces', 'city', 'scope'],
            ['position-one-position-two', 'Position one,position two', 'city', 'scope'],
            ['kurer', 'Курьер', 'Минск', null],
            ['zhurnalist', 'Журналист', 'Минск', 'IT, телекоммуникации, связь']
        ];
    }

    /**
     * @test
     * @dataProvider slugs
     */
    public function it_should_generate_slug(
        $slug,
        $positionTitle,
        $cityTitle = null,
        $scopeTitle = null
    ) {
        $cv = $this->getCv();

        $position = new Position();
        $position->setTitle($positionTitle);
        $cv->setPosition($position);

        if ($cityTitle) {
            $city = new City();
            $city->setTitle($cityTitle);
            $cv->setWorkingPlaceCity($city);
        }

        if ($scopeTitle) {
            $scope = new Scope();
            $scope->setTitle($scopeTitle);
            $cv->setScope($scope);
        }

        $cv->generateSlug();

        $this->assertSame($slug, $cv->getSlug());
    }
}
