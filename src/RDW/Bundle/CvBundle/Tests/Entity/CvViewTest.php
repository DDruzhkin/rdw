<?php

namespace RDW\Bundle\CvBundle\Tests\Entity;

use RDW\Bundle\CvBundle\Entity\CvView;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;

/**
 * Class CvViewTest
 *
 * @package RDW\Bundle\CvBundle\Tests\Entity
 */
class CvViewTest extends \PHPUnit_Framework_TestCase
{
    const CLASS_NAME = 'RDW\Bundle\CvBundle\Entity\CvView';

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $user;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $cv;

    /**
     * @var CvView
     */
    protected $cvView;

    /**
     * set up
     */
    public function setUp()
    {
        $this->user = $this->getMock('\RDW\Bundle\UserBundle\Entity\RegisteredUser');
        $this->cv = $this->getMock('\RDW\Bundle\CvBundle\Entity\Cv');
        $this->cvView = new CvView($this->cv, $this->user);
    }

    /**
     * @test
     */
    public function it_should_return_user_cv_after_instance()
    {
        $this->assertInstanceOf(self::CLASS_NAME, $this->cvView);
        $this->assertEquals($this->user, $this->cvView->getUser());
        $this->assertEquals($this->cv, $this->cvView->getCv());
    }

    /**
     * @test
     */
    public function it_should_return_created_at()
    {
        $this->assertNull($this->cvView->getCreatedAt());

        $date = new \DateTime();

        $this->assertInstanceOf(self::CLASS_NAME, $this->cvView->setCreatedAt($date));
        $this->assertEquals($date, $this->cvView->getCreatedAt());
        $this->assertInstanceOf('DateTime', $this->cvView->getCreatedAt());
    }

    /**
     * @test
     */
    public function it_should_return_different_user()
    {
        $this->assertEquals($this->user, $this->cvView->getUser());

        $user = new RegisteredUser();

        $this->assertInstanceOf(self::CLASS_NAME, $this->cvView->setUser($user));
        $this->assertEquals($user, $this->cvView->getUser());
    }

    /**
     * @test
     */
    public function it_should_return_different_cv()
    {
        $this->assertEquals($this->cv, $this->cvView->getCv());

        $cv = new Cv();

        $this->assertInstanceOf(self::CLASS_NAME, $this->cvView->setCv($cv));
        $this->assertEquals($cv, $this->cvView->getCv());
    }
}
