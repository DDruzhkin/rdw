<?php

namespace RDW\Bundle\CvBundle\Tests\Entity;

use RDW\Bundle\CvBundle\Entity\CvLanguage;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\JobBundle\Entity\Language;
use RDW\Bundle\JobBundle\Entity\LanguageLevel;

/**
 * Class CvLanguageTest
 *
 * @package RDW\Bundle\CvBundle\Tests\Entity
 * @author  Paulius Aleliūnas <paulius@eface.lt>
 */
class CvLanguageTest extends \PHPUnit_Framework_TestCase
{
    const CLASS_NAME = 'RDW\Bundle\CvBundle\Entity\CvLanguage';

    /**
     * Test set cv
     */
    public function test_set_cv()
    {
        $cvLanguage = $this->getCvLanguage();

        $this->assertNull($cvLanguage->getCv());

        $cv = new Cv();

        $this->assertInstanceOf(self::CLASS_NAME, $cvLanguage->setCv($cv));
        $this->assertEquals($cv, $cvLanguage->getCv());
    }

    /**
     * Test set language
     */
    public function test_set_language()
    {
        $cvLanguage = $this->getCvLanguage();

        $this->assertNull($cvLanguage->getLanguage());

        $language = new Language();

        $this->assertInstanceOf(self::CLASS_NAME, $cvLanguage->setLanguage($language));
        $this->assertEquals($language, $cvLanguage->getLanguage());
    }

    /**
     * Test set language level
     */
    public function test_set_language_level()
    {
        $cvLanguage = $this->getCvLanguage();

        $this->assertNull($cvLanguage->getLevel());

        $level = new LanguageLevel();

        $this->assertInstanceOf(self::CLASS_NAME, $cvLanguage->setLevel($level));
        $this->assertEquals($level, $cvLanguage->getLevel());
    }

    /**
     * @return CvLanguage
     */
    private function getCvLanguage()
    {
        return new CvLanguage();
    }
}
