<?php

namespace RDW\Bundle\CvBundle\Tests\EventListener;

use RDW\Bundle\CvBundle\Event\UserEvent;
use RDW\Bundle\CvBundle\EventListener\CvViewListener;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\CvBundle\Entity\CvView;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class CvViewListenerTest
 * @package RDW\Bundle\CvBundle\Tests\EventListener
 */
class CvViewListenerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $cvManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $dispatcher;

    /**
     * @var CvViewListener
     */
    private $listener;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $user;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $owner;

    /**
     * @var Cv
     */
    private $cv;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $cvView;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $userEvent;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $historyManager;

    /**
     * Set up
     */
    public function setUp()
    {
        $this->cvManager = $this->getMockBuilder('RDW\Bundle\CvBundle\Service\CvManager')
            ->disableOriginalConstructor()->getMock();
        $this->dispatcher = $this->getMock('Symfony\Component\EventDispatcher\EventDispatcher');

        $this->historyManager = $this->getMockBuilder('\RDW\Bundle\HistoryBundle\Service\HistoryManager')
            ->disableOriginalConstructor()->getMock();

        $this->listener = new CvViewListener($this->cvManager, $this->dispatcher, $this->historyManager);

        $this->user = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');
        $this->owner = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');

        $this->cv = new Cv();
        $this->cv->setUser($this->owner);

        $this->cvView = $this->getMock('RDW\Bundle\CvBundle\Entity\CvView', [], [$this->cv, $this->user]);

        $this->userEvent = $this->getMockBuilder('RDW\Bundle\CvBundle\Event\UserEvent')->disableOriginalConstructor()->getMock();
    }

    /**
     * @test
     */
    public function it_should_add_cv_view_to_cv()
    {
        $this->userEvent
            ->expects($this->once())
            ->method('getUser')
            ->willReturn($this->user);

        $this->userEvent
            ->expects($this->once())
            ->method('getCv')
            ->willReturn($this->cv);

        $this->cvManager
            ->expects($this->once())
            ->method('existsView')
            ->with($this->cv, $this->user)
            ->willReturn(false);

        $this->assertEmpty($this->cv->getUsersWhoViewed());
        $this->listener->onCvView($this->userEvent);
        $this->assertNotEmpty($this->cv->getUsersWhoViewed());
    }

    /**
     * @test
     */
    public function it_should_not_add_cv_view_to_cv_if_exists()
    {
        $this->userEvent
            ->expects($this->once())
            ->method('getUser')
            ->willReturn($this->user);

        $this->userEvent
            ->expects($this->once())
            ->method('getCv')
            ->willReturn($this->cv);

        $this->cvManager
            ->expects($this->once())
            ->method('existsView')
            ->with($this->cv, $this->user)
            ->willReturn(true);

        $this->assertEmpty($this->cv->getUsersWhoViewed());
        $this->listener->onCvView($this->userEvent);
        $this->assertEmpty($this->cv->getUsersWhoViewed());
    }

    /**
     * @test
     */
    public function it_should_not_add_cv_view_to_cv_if_user_is_owner()
    {
        $this->cv->setUser($this->user);

        $this->userEvent
            ->expects($this->once())
            ->method('getUser')
            ->willReturn($this->user);

        $this->userEvent
            ->expects($this->once())
            ->method('getCv')
            ->willReturn($this->cv);

        $this->cvManager
            ->expects($this->any())
            ->method('existsView')
            ->with($this->cv, $this->user)
            ->willReturn(true);

        $this->assertEmpty($this->cv->getUsersWhoViewed());
        $this->listener->onCvView($this->userEvent);
        $this->assertEmpty($this->cv->getUsersWhoViewed());
    }

    /**
     * @test
     */
    public function it_should_not_add_cv_view_to_cv_if_user_is_manager()
    {
        $this->userEvent
            ->expects($this->once())
            ->method('getUser')
            ->willReturn($this->user);

        $this->userEvent
            ->expects($this->once())
            ->method('getCv')
            ->willReturn($this->cv);

        $this->cvManager
            ->expects($this->any())
            ->method('existsView')
            ->with($this->cv, $this->user)
            ->willReturn(false);

        $this->user
            ->expects($this->once())
            ->method('isManager')
            ->willReturn(true);

        $this->assertEmpty($this->cv->getUsersWhoViewed());
        $this->listener->onCvView($this->userEvent);
        $this->assertEmpty($this->cv->getUsersWhoViewed());
    }

    /**
     * @test
     */
    public function it_should_return_null_if_user_not_set()
    {
        $this->userEvent
            ->expects($this->once())
            ->method('getUser')
            ->willReturn(null);

        $this->assertNull($this->listener->onCvView($this->userEvent));
    }

    /**
     * @test
     *
     * @expectedException  \Symfony\Component\Form\Exception\UnexpectedTypeException
     */
    public function it_should_throw_unexpected_type_exception_with_wrong_cv()
    {
        $this->userEvent
            ->expects($this->once())
            ->method('getUser')
            ->willReturn($this->user);

        $this->userEvent
            ->expects($this->once())
            ->method('getCv')
            ->willReturn(null);

        $this->listener->onCvView($this->userEvent);
    }
}
