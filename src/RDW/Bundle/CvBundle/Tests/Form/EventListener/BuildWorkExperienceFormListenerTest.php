<?php

namespace RDW\Bundle\CvBundle\Tests\Form\EventListener;

use RDW\Bundle\CvBundle\Form\EventListener\BuildWorkExperienceFormListener;
use Symfony\Component\Form\Test\TypeTestCase;

/**
 * Class BuildWorkExperienceFormListenerTest
 * @package RDW\Bundle\CvBundle\Tests\Form\EventListener
 */
class BuildWorkExperienceFormListenerTest extends TypeTestCase
{
    public function formValues()
    {
        return [
            [false, null, 'once'],
            [true, null, 'never'],
            [true, true, 'never'],
            [false, true, 'never'],
        ];
    }

    /**
     * @dataProvider formValues
     * @test
     */
    public function it_should_add_error_to_form_if_is_no_work_experience($withoutWorkExperience, $workExperienceType, $times)
    {
        $formEventMock = $this->getFormEventMock($withoutWorkExperience, $workExperienceType, $times);

        $listener = new BuildWorkExperienceFormListener();
        $listener->postSubmit($formEventMock);
    }

    private function getFormEventMock($withoutWorkExperience, $workExperienceType, $times)
    {
        $formEventMock = $this->getMockBuilder('\Symfony\Component\Form\FormEvent')->disableOriginalConstructor()
            ->getMock();

        $formMock = $this->getMockBuilder('\Symfony\Component\Form\Form')
            ->disableOriginalConstructor()
            ->getMock();

        $formEventMock
            ->expects($this->any())->method('getForm')
            ->withAnyParameters()
            ->will($this->returnValue($formMock));

        $formItemMock = $this->getMockBuilder('\Symfony\Component\Form\Form')
            ->disableOriginalConstructor()
            ->getMock();

        $formItemMock
            ->method('getData')
            ->will($this->onConsecutiveCalls($withoutWorkExperience, $workExperienceType));

        $formItemMock
            ->expects(('once' == $times) ? $this->once() : $this->never())
            ->method('addError');

        $formMock
            ->expects($this->any())->method('get')
            ->withAnyParameters()
            ->will($this->returnValue($formItemMock));

        return $formEventMock;
    }
}
