<?php

namespace RDW\Bundle\CvBundle;

/**
 * Class RDWCvEvents
 * Contains all events thrown in the RDWCvBundle
 *
 * @package RDW\Bundle\CvBundle
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
final class RDWCvEvents
{
    /**
     * This event occurs then user reply to cv
     */
    const CV_MESSAGE_SAVE = 'rdw_cv.event.cv_message_save';

    /**
     * This event occurs then user reply to multiple cvs
     */
    const MULTIPLE_CV_MESSAGE_SAVE = 'rdw_cv.event.multiple_cv_message_save';

    /**
     * This event occurs then admin,censor or manager updated a cv
     */
    const CV_APPROVE = 'rdw_cv.event.cv_approve';

    /**
     * This event occurs then censor successfully submits block form
     */
    const CV_BLOCK = 'rdw_cv.event.cv_block';

    /**
     * This event occurs then user views cv
     */
    const CV_VIEW = 'rdw_cv.event.cv_view';

    /**
     * On CV save
     */
    const CV_SAVE = 'rdw_cv.event.cv_save';

    /**
     * On CV publish
     */
    const CV_PUBLISH = 'rdw_cv.event.cv_publish';
}
