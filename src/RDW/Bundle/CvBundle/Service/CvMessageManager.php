<?php

namespace RDW\Bundle\CvBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\CvBundle\Entity\CvMessage;
use RDW\Bundle\CvBundle\Model\MultipleCvMessage;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\User;
use RDW\Bundle\UserBundle\Entity\UserInterface;

/**
 * Class CvMessageManager
 *
 * @package RDW\Bundle\CvBundle\Service
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class CvMessageManager
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    protected $objectManager;

    /**
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function create(Cv $cv, UserInterface $user = null)
    {
        if (!$user) {
            return (new CvMessage())->setCv($cv);
        } else {
            return $this->createCvMessageFromUserAndCv($user, $cv);
        }
    }

    /**
     * @param User $user
     * @param Cv   $cv
     *
     * @return CvMessage
     */
    public function createCvMessageFromUserAndCv(UserInterface $user, Cv $cv)
    {
        $message = new CvMessage();

        if ($user instanceof RegisteredUser) {
            $message->setCompanyTitle($user->getCompanyTitle());
            $message->setContactName($user->getContactPerson());
            $message->setContactEmail($user->getEmail());
            $message->setContactPhone($user->getPhone());
        }

        $message->setCv($cv);
        $message->setUser($user);

        return $message;
    }

    /**
     * @param User            $user
     * @param ArrayCollection $cvs
     *
     * @return MultipleCvMessage
     */
    public function createMultipleCvMessage(User $user, ArrayCollection $cvs = null)
    {
        $message = new CvMessage();
        $message->setCompanyTitle($user->getCompanyTitle());
        $message->setContactName($user->getContactPerson());
        $message->setContactEmail($user->getEmail());
        $message->setContactPhone($user->getPhone());
        $message->setUser($user);

        if ($cvs) {
            $message->setCvs($cvs);
        }

        return $message;
    }

    /**
     * @param CvMessage $message
     * @param bool      $andFlush
     *
     * @return bool
     */
    public function update(CvMessage $message, $andFlush = true)
    {
        $this->objectManager->persist($message);

        if ($andFlush) {
            $this->objectManager->flush();
        }

        return true;
    }

    /**
     * @param CvMessage $cvMessage
     * @return object
     */
    public function merge($cvMessage)
    {
        return $this->objectManager->merge($cvMessage);
    }
}
