<?php

namespace RDW\Bundle\CvBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use Knp\Component\Pager\Paginator;
use RDW\Bundle\AppBundle\Entity\CvAbuse;

/**
 * Class CvAbuseManager
 *
 * @package RDW\Bundle\CvBundle\Service
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class CvAbuseManager
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $entityManager;

    /**
     * @var \Knp\Component\Pager\Paginator
     */
    private $paginator;

    /**
     * @param ObjectManager $entityManager
     * @param Paginator     $paginator
     */
    public function __construct(ObjectManager $entityManager, Paginator $paginator)
    {
        $this->entityManager = $entityManager;
        $this->paginator = $paginator;
    }

    /**
     * @return CvAbuse
     */
    public function create()
    {
        $abuse = new CvAbuse();

        return $abuse;
    }

    /**
     * @param CvAbuse $abuse
     * @param bool  $andFlush
     *
     * @return bool
     */
    public function update(CvAbuse $abuse, $andFlush = true)
    {
        $this->entityManager->persist($abuse);

        if ($andFlush) {
            $this->entityManager->flush();
        }

        return true;
    }

    /**
     * @param int $page
     * @param int $limit
     *
     * @return mixed
     */
    public function getPaginatedList($page, $limit)
    {
        $query = $this->getRepository()->getListQuery();

        $pagination = $this->paginator->paginate($query, $page, $limit);

        return $pagination;
    }

    /**
     * @return \RDW\Bundle\CvBundle\Repository\AbuseRepository
     */
    private function getRepository()
    {
        return $this->entityManager->getRepository('RDWAppBundle:CvAbuse');
    }
}
