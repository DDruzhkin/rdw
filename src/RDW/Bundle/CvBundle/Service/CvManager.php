<?php
namespace RDW\Bundle\CvBundle\Service;

use RDW\Bundle\AppBundle\Service\AdManager;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\CvBundle\Model\CvFilter;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\User;
use RDW\Bundle\UserBundle\Entity\UserInterface;

/**
 * Class CvManager
 *
 * @package RDW\Bundle\CvBundle\Service
 */
class CvManager extends AdManager
{
    public function create(RegisteredUser $user = null)
    {
        if ($user) {
            return $this->createCvFromUser($user);
        } else {
            return new Cv();
        }
    }

    /**
     * @param RegisteredUser $user
     *
     * @return Cv
     */
    public function createCvFromUser(RegisteredUser $user)
    {
        $cv = new Cv();

        // pre fill cv person info fields
        $cv->setName($user->getName());
        $cv->setSurname($user->getSurname());
        $cv->setPatronymic($user->getPatronymic());
        if ($user->getBirthday()) {
            $cv->setBirthday($user->getBirthday());
        }
        $cv->setGender($user->getGender());

        // pre fill cv contact info fields
        $cv->setCities($user->getCities());
        $cv->addContactPersonPhoneByString($user->getPhone());
        $cv->setEmail($user->getEmail());
        $cv->setFbContact($user->getFbContact());
        $cv->setVkContact($user->getVkContact());
        $cv->setOkContact($user->getOkContact());
        $cv->setGPlusContact($user->getGPlusContact());
        $cv->setTwitterContact($user->getTwitterContact());
        $cv->setLinkedInContact($user->getLinkedInContact());
        $cv->setSkypeContact($user->getSkypeContact());
        $cv->setYoutubeVideo($user->getYoutubeVideo());

        $cv->setMaritalStatus($user->getFamilyStatus());
        $cv->setChildren($user->getChildren());
        $cv->setPhoto($user->getPhoto());

        $cv->setUser($user);

        return $cv;
    }

    /**
     * @param int  $id
     * @param User $user
     *
     * @return Cv|null
     */
    public function findCvByIdAndUser($id, User $user)
    {
        return $this->getRepository()->findOneBy([
            'user' => $user,
            'id' => $id,
        ]);
    }

    /**
     * @param int $id
     *
     * @return Cv
     */
    public function findCvById($id)
    {
        return $this->getRepository()->find($id);
    }

    /**
     * @param int $id
     *
     * @return Cv
     */
    public function getById($id)
    {
        return $this->getRepository()->getByid($id);
    }

    /**
     * @param int $id
     *
     * @return Cv
     */
    public function getByIdIncludingDeleted($id)
    {
        $this->objectManager->getFilters()->disable('softdeleteable');
        $cv = $this->getById($id);
        $this->objectManager->getFilters()->enable('softdeleteable');

        return $cv;
    }

    /**
     * @param CvFilter $filter
     * @param int      $page
     * @param int      $limit
     *
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function getPaginatedList(CvFilter $filter, $page, $limit)
    {
        $query = $this->getRepository()->getListQueryBuilder($filter);

        $pagination = $this->paginator->paginate($query, $page, $limit);

        return $pagination;
    }

    /**
     * @param CvFilter $filter
     * @param int      $page
     * @param int      $limit
     *
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function getPaginatedMyList(CvFilter $filter, $page, $limit)
    {
        $query = $this->getRepository()->getMyListQueryBuilder($filter);

        $pagination = $this->paginator->paginate($query, $page, $limit);

        return $pagination;
    }

    /**
     * @return \RDW\Bundle\CvBundle\Repository\CvRepository
     */
    protected function getRepository()
    {
        return $this->objectManager->getRepository('RDWCvBundle:Cv');
    }

    /**
     * @param Cv            $cv
     * @param int           $limit
     * @param UserInterface $user
     *
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function findRelated(Cv $cv, $limit, UserInterface $user = null)
    {
        $relatedCvsFilter = new CvFilter();

        $relatedCvsFilter->addPosition($cv->getPosition());
        $relatedCvsFilter->setNotId($cv->getId());
        $relatedCvsFilter->setStatuses([Cv::STATUS_ACTIVE]);

        if ($user instanceof RegisteredUser) {
            $relatedCvsFilter->setNotShowViewedCvs(true);
            $relatedCvsFilter->setUser($user);
        }

        $relatedCvsFilter->setWorkingPlaceCity($cv->getWorkingPlaceCity());

        $query = $this->getRepository()->getListQueryBuilder($relatedCvsFilter);
        $pagination = $this->paginator->paginate($query, 1, $limit);

        if ($pagination->getTotalItemCount() < $limit) {
            $relatedCvsFilter->setNotShowViewedCvs(false);
            $relatedCvsFilter->setWorkingPlaceCity(null);

            $query = $this->getRepository()->getListQueryBuilder($relatedCvsFilter);
            $pagination = $this->paginator->paginate($query, 1, $limit);
        }

        return $pagination;
    }

    /**
     * @param Cv   $cv
     * @param User $user
     *
     * @return boolean
     */
    public function existsView(Cv $cv, User $user)
    {
        $view = $this->objectManager->getRepository('RDWCvBundle:CvView')->findOneBy(['cv' => $cv, 'user' => $user]);

        return $view;
    }
}
