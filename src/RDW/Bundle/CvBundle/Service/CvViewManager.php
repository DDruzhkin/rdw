<?php

namespace RDW\Bundle\CvBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use Knp\Component\Pager\Paginator;
use RDW\Bundle\CvBundle\Model\UsersViewFilter;

/**
 * Class CvViewManager
 *
 * @package RDW\Bundle\CvBundle\Service
 */
class CvViewManager
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $entityManager;

    /**
     * @var \Knp\Component\Pager\Paginator
     */
    private $paginator;

    /**
     * @param ObjectManager $entityManager
     * @param Paginator     $paginator
     */
    public function __construct(ObjectManager $entityManager, Paginator $paginator)
    {
        $this->entityManager = $entityManager;
        $this->paginator = $paginator;
    }

    /**
     * @param UsersViewFilter $filters
     * @param int             $page
     * @param int             $limit
     *
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function getPaginatedList(UsersViewFilter $filters, $page, $limit)
    {
        $query = $this->getRepository()->getListQuery($filters);

        $pagination = $this->paginator->paginate($query, $page, $limit);

        return $pagination;
    }

    /**
     * @return \RDW\Bundle\CvBundle\Repository\CvViewRepository
     */
    protected function getRepository()
    {
        return $this->entityManager->getRepository('RDWCvBundle:CvView');
    }

}
