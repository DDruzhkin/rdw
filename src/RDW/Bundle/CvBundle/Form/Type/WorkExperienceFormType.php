<?php

namespace RDW\Bundle\CvBundle\Form\Type;

use RDW\Bundle\CvBundle\Form\Transformer\IncompleteDateTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use RDW\Bundle\AppBundle\Validator\Constraints\DateRangeValidator;
use Symfony\Component\Form\FormEvents;

/**
 * Class WorkExperienceFormType
 *
 * @author Vytautas Beliunas <vytautas@eface.lt>
 */
class WorkExperienceFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            $builder->create('dateFrom', 'rdw_year_month', [
                'input'  => 'datetime',
                'format' => 'yyyy-MMMMdd',
                'years'  => range(date('Y'), date('Y') - 30, -1),
                'required' => false,
                'label' => 'Start date',
                'empty_value' => ['year' => 'Year', 'month' => 'Month'],
            ])->addViewTransformer(new IncompleteDateTransformer())
        );

        $builder->add(
            $builder->create('dateTo', 'rdw_year_month', [
                'input'  => 'datetime',
                'format' => 'yyyy-MMMMdd',
                'years'  => range(date('Y'), date('Y') - 30, -1),
                'required' => false,
                'label' => 'End date',
                'empty_value' => ['year' => 'Year', 'month' => 'Month'],
            ])->addViewTransformer(new IncompleteDateTransformer())
        );

        $builder
            ->add('companyTitle', 'text', [
                'label' => 'Company title',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Company title placeholder',
                ],
            ])
            ->add('scope', 'entity', [
                'class' => 'RDWUserBundle:Scope',
                'empty_value' => 'Choose option',
                'property' => 'title',
                'required' => false,
                'label' => 'Scope',
            ])
            ->add('position', 'text', [
                'label' => 'Work experience position',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Work experience position placeholder',
                ],
            ])
            ->add('additionalInfo', 'textarea', [
                'required' => false,
                'label' => 'Work experience info',
                'attr' => [
                    'placeholder' => 'Work experience info placeholder',
                ],
            ]);

        $validator = new DateRangeValidator();
        $builder->addEventListener(FormEvents::POST_SUBMIT, [$validator, 'validate']);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\CvBundle\Entity\WorkExperience',
            'cascade_validation' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_cv_work_experience';
    }
}