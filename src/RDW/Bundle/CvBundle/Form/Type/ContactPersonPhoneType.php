<?php

namespace RDW\Bundle\CvBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class ContactPersonPhoneType
 */
class ContactPersonPhoneType extends \RDW\Bundle\AppBundle\Form\Type\ContactPersonPhoneType
{
    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\CvBundle\Entity\ContactPersonPhone',
            'validation_groups' => ['publish', 'create', 'censor'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_cv_contact_person_phone';
    }
}