<?php

namespace RDW\Bundle\CvBundle\Form\Type;

use RDW\Bundle\AppBundle\Form\EventListener\BuildDocumentUploadFormListener;
use RDW\Bundle\AppBundle\Form\EventListener\BuildPhotoUploadFormListener;
use RDW\Bundle\AppBundle\Form\EventListener\BuildAgeFormListener;
use RDW\Bundle\CvBundle\Form\EventListener\BuildWorkExperienceFormListener;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\CvBundle\EventListener\CvActionListener;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CvFormType
 *
 * @package RDW\Bundle\CvBundle\Form\Type
 */
class CvFormType extends BaseCvType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        if (!$options['disabled']) {
            $builder->addEventSubscriber(new BuildPhotoUploadFormListener());
            $builder->addEventSubscriber(new BuildDocumentUploadFormListener());
        }

        /** @var Cv $cv */
        $cv = $builder->getData();

        if (($cv->getId() > 0 && $cv->isStatusActive()) || $options['disabled']) {
            $builder
                ->add('status', 'choice', [
                    'label' => 'Status',
                    'choices' => Cv::getStatusesChoiceListForUser(),
                    'required' => true,
                ]);
        }

        if (!$options['disabled']) {
            $builder
                ->add('submit', 'submit', [
                    'label' => 'Save CV',
                ]);

            if ($cv->getUser() instanceof RegisteredUser) {
                $builder
                    ->add('submitAndPublish', 'submit', [
                        'label' => 'Publish CV',
                    ]);
            }
        }

        $builder->addEventSubscriber(new BuildAgeFormListener());
        $builder->addEventSubscriber(new BuildWorkExperienceFormListener());
        $builder->addEventSubscriber(new CvActionListener());
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\CvBundle\Entity\Cv',
            'validation_groups' => ['create', 'update'],
            'cascade_validation' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_cv';
    }
}
