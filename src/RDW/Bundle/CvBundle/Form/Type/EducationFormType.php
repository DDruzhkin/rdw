<?php

namespace RDW\Bundle\CvBundle\Form\Type;

use RDW\Bundle\AppBundle\Form\Type\YearMonthFormType;
use RDW\Bundle\CvBundle\Entity\Education;
use RDW\Bundle\CvBundle\Form\Transformer\IncompleteDateTransformer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class EducationFormType
 *
 * @author Vytautas Beliunas <vytautas@eface.lt>
 */
class EducationFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            $builder
                ->create('dateTo', YearMonthFormType::class, [
                    'input' => 'datetime',
                    'format' => 'yyyy-MMMMd',
                    'years' => range(date('Y'), date('Y') - 30, -1),
                    'required' => false,
                    'label' => 'Education date to',
                    //'empty_value' => 'years',
                ])
                ->addViewTransformer(new IncompleteDateTransformer())
        );

        $builder
            ->add('educationType', EntityType::class, [
                'class' => 'RDWJobBundle:EducationType',
                'empty_value' => 'Choose option',
                'property' => 'title',
                'required' => false,
            ])
            ->add('institution', TextType::class, [
                'required' => false,
                'label' => 'Institution',
                'attr' => [
                    'placeholder' => 'Institution placeholder',
                ],
            ])
            ->add('faculty', TextType::class, [
                'required' => false,
                'label' => 'Faculty',
                'attr' => [
                    'placeholder' => 'Faculty placeholder',
                ],
            ])
            ->add('additionalInfo', TextareaType::class, [
                'required' => false,
                'label' => 'Additional education info',
                'attr' => [
                    'placeholder' => 'Additional education info placeholder',
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Education::class,
            'validation_groups' => ['create'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_cv_education';
    }
}
