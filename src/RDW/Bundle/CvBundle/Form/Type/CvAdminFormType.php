<?php

namespace RDW\Bundle\CvBundle\Form\Type;

use RDW\Bundle\AppBundle\Form\EventListener\BuildDocumentUploadFormListener;
use RDW\Bundle\AppBundle\Form\EventListener\BuildPhotoUploadFormListener;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\CvBundle\EventListener\CvActionListener;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * Class CvAdminFormType
 *
 * @author Vytautas Beliunas <vytautas@eface.lt>
 */
class CvAdminFormType extends BaseCvType
{
    /**
     * @var SecurityContext
     */
    private $securityContext;

    /**
     * @param SecurityContext $securityContext
     */
    public function __construct(SecurityContext $securityContext)
    {
        $this->securityContext = $securityContext;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('status', 'choice', [
            'choices' => Cv::getStatusesChoiceListForAdmin(),
            'empty_value' => 'Choose option'
        ]);

        $builder->addEventSubscriber(new BuildPhotoUploadFormListener());
        $builder->addEventSubscriber(new BuildDocumentUploadFormListener());
        $builder->addEventSubscriber(new CvActionListener());
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'viewMode' => false,
            'data_class' => 'RDW\Bundle\CvBundle\Entity\Cv',
            'validation_groups' => ['create'],
            'cascade_validation' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_cv_admin';
    }
}
