<?php

namespace RDW\Bundle\CvBundle\Form\Type;

use RDW\Bundle\CvBundle\Repository\CvRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class CvCollectionType
 *
 * @package RDW\Bundle\CvBundle\Form\Type
 */
class CvCollectionType extends AbstractType
{
    /**
     * @var array
     */
    private $items;

    /**
     * @param array $items
     */
    public function __construct(array $items)
    {
        $this->items = $items;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', 'entity', [
            'required' => false,
            'class' => 'RDWCvBundle:Cv',
            'property' => 'id',
            'property_path' => '[id]',
            'multiple' => true,
            'expanded' => true,
            'query_builder' => function (CvRepository $repository) {
                return $repository
                    ->createQueryBuilder('cv')
                    ->where('cv.id IN (:ids)')
                    ->setParameter('ids', $this->items);
            },
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
            'csrf_protection' => false,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'cv_collection';
    }
}
