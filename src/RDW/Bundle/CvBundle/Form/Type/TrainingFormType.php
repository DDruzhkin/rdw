<?php

namespace RDW\Bundle\CvBundle\Form\Type;

use RDW\Bundle\CvBundle\Form\Transformer\IncompleteDateTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class TrainingFormType
 *
 * @author Paulius Aleliūnas <paulius@eface.lt>
 */
class TrainingFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            $builder
                ->create('dateTo', 'rdw_year_month', [
                    'input'  => 'datetime',
                    'format' => 'yyyy-MMMMd',
                    'years'  => range(date('Y'), date('Y') - 30, -1),
                    'required' => false,
                    'label' => 'Course date to',
                    'empty_value' => 'Choose option',
                ])
                ->addViewTransformer(new IncompleteDateTransformer())
        );

        $builder
            ->add('institution', 'text', [
                'required' => false,
                'label' => 'Course institution',
                'attr' => [
                    'placeholder' => 'Course institution',
                ],
            ])
            ->add('additionalInfo', 'textarea', [
                'required' => false,
                'label' => 'Course information',
                'attr' => [
                    'placeholder' => 'Course information placeholder',
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\CvBundle\Entity\Training',
            'validation_groups' => ['create_training'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_cv_training';
    }
}
