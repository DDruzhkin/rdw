<?php

namespace RDW\Bundle\CvBundle\Form\Type;

use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\CvBundle\Model\CvFilter;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use RDW\Bundle\CvBundle\Repository\CvRepository;

class CvUsersViewFilterType extends AbstractType
{
    /**
     * @var RegisteredUser
     */
    protected $user;

    /**
     * @param RegisteredUser $user
     */
    public function __construct(RegisteredUser $user)
    {
        $this->user = $user;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('GET');

        $builder
            ->add('cv', 'entity', [
                'class' => 'RDWCvBundle:Cv',
                'query_builder' => function (CvRepository $er) {
                    $filter = new CvFilter();
                    $filter->setUser($this->user);

                    return $er->getMyListQueryBuilder($filter);
                },
                'property' => 'position.title',
                'empty_value' => 'All cvs',
                'required' => false,
                'label' => 'Filter by cv',
            ])
            ->add('search', 'submit', [
                'label' => 'Search',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RDW\Bundle\CvBundle\Model\UsersViewFilter',
            'csrf_protection' => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return '';
    }
}
