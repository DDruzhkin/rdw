<?php

namespace RDW\Bundle\CvBundle\Form\Type;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityRepository;
use RDW\Bundle\AppBundle\Form\DataTransformer\EntityCollectionTransformer;
use RDW\Bundle\AppBundle\Service\SalaryRangeService;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\CvBundle\Model\CvFilter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use RDW\Bundle\AppBundle\Form\DataTransformer\SingleEntityTransformer;

/**
 * Class CvFilterType
 *
 * @package RDW\Bundle\CvBundle\Form\Type
 */
class CvFilterType extends AbstractType
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @var SalaryRangeService
     */
    private $salaryRangeService;

    /**
     * @param ObjectManager      $objectManager
     * @param SalaryRangeService $salaryRangeService
     */
    public function __construct(ObjectManager $objectManager, SalaryRangeService $salaryRangeService)
    {
        $this->om = $objectManager;
        $this->salaryRangeService = $salaryRangeService;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $citiesTransformer = new EntityCollectionTransformer($this->om, 'RDWLocationBundle:City');
        $positionsTransformer = new EntityCollectionTransformer($this->om, 'RDWJobBundle:Position');
        $scopesTransformer = new EntityCollectionTransformer($this->om, 'RDWUserBundle:Scope');
        $professionalAreaTransformer = new EntityCollectionTransformer($this->om, 'RDWJobBundle:ProfessionalArea');
        $cityTransformer = new SingleEntityTransformer($this->om, 'RDWLocationBundle:City');

        $builder->setMethod('get');
        $builder->add(
            $builder
                ->create('cities', 'hidden', [
                    'required' => false,
                    'label' => 'City',
                    'attr' => [
                        'data-handler' => 'filterTag',
                        'data-filter' => 'city',
                        'data-multiple' => 'true',
                    ],
                ])
                ->addModelTransformer($citiesTransformer)
        );
        $builder
            ->add(
                $builder
                    ->create('positions', 'hidden', [
                        'required' => false,
                        'label' => 'Position',
                        'attr' => [
                            'data-handler' => 'filterTag',
                            'data-filter' => 'position',
                            'data-multiple' => 'true',
                        ],
                    ])
                    ->addModelTransformer($positionsTransformer)
            )

            ->add(
                $builder
                    ->create('professionalArea', 'hidden', [
                        'required' => false,
                        'label' => 'Professional Area',
                        'attr' => [
                            'data-handler' => 'filterTag',
                            'data-filter' => 'professional_area',
                            'data-multiple' => 'true',
                        ],
                    ])
                    ->addModelTransformer($professionalAreaTransformer)
            )

            ->add(
                $builder
                    ->create('scopes', 'hidden', [
                        'required' => false,
                        'label' => 'Scope',
                        'attr' => [
                            'data-handler' => 'filterTag',
                            'data-filter' => 'scope',
                            'data-multiple' => 'false',
                        ],
                    ])
                    ->addModelTransformer($scopesTransformer)
            );

        $builder
            ->add('query', 'hidden', [
                'required' => false,
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'query',
                    'data-multiple' => 'false',
                    'data-load-from-hidden' => true,
                ],
            ])
            ->add(
                $builder
                    ->create('workingPlaceCity', 'hidden', [
                        'required' => false,
                        'label' => 'Working place city',
                        'attr' => [
                            'data-handler' => 'filterTag',
                            'data-filter' => 'city',
                            'data-multiple' => 'false',
                        ],
                    ])
                    ->addModelTransformer($cityTransformer)
            )
            ->add('updatedAt', 'choice', [
                'choices'   => CvFilter::getUpdatedAtChoices(),
                'empty_value' => 'All period',
                'required'  => false,
                'label' => 'Date',
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'updatedAt',
                    'data-multiple' => 'false',
                ],
            ])
            ->add('gender', 'choice', [
                'choices'   => RegisteredUser::getGenderChoices(),
                'label' => 'Gender',
                'required' => false,
                'empty_value' => 'No matter',
                'expanded' => true,
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'gender',
                    'data-multiple' => 'false',
                ],
            ])
            ->add('educationType', 'entity', [
                'class' => 'RDWJobBundle:EducationType',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('i')
                        ->orderBy('i.position', 'ASC');
                },
                'empty_value' => 'No mattter',
                'property' => 'title',
                'required' => false,
                'label' => 'Education type',
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'educationType',
                    'data-multiple' => 'false',
                ],
            ])
            ->add('workExperienceType', 'entity', [
                'class' => 'RDWJobBundle:WorkExperienceType',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('i')
                        ->orderBy('i.position', 'ASC');
                },
                'property' => 'title',
                'multiple' => false,
                'required' => false,
                'empty_value' => 'No matter',
                'label' => 'Work experience',
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'workExperienceScope',
                    'data-multiple' => 'false',
                ],
            ])
            ->add('businessTrip', 'entity', [
                'class' => 'RDWCvBundle:BusinessTrip',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('i')
                        ->orderBy('i.position', 'ASC');
                },
                'empty_value' => 'No matter',
                'property' => 'title',
                'required' => false,
                'label' => 'Business trip',
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'businessTrip',
                    'data-multiple' => 'false',
                ],
            ])
            ->add('salaryFrom', 'choice', [
                'choices' => $this->salaryRangeService->getCvSalaryIntervals(),
                /** @Ignore */
                'label' => false,
                'empty_value' => 'Choose from',
                'required' => false,
                'mapped' => false,
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'salaryFrom',
                    'data-multiple' => 'false',
                ],
            ])
            ->add('salaryTo', 'choice', [
                'choices' => $this->salaryRangeService->getCvSalaryIntervals(),
                /** @Ignore */
                'label' => false,
                'empty_value' => 'Choose to',
                'required' => false,
                'mapped' => false,
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'salaryTo',
                    'data-multiple' => 'false',
                ],
            ])
            ->add('workScheduleType', 'entity', [
                'class' => 'RDWJobBundle:WorkScheduleType',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('i')
                        ->orderBy('i.position', 'ASC');
                },
                'empty_value' => 'No matter',
                'property' => 'title',
                'required' => false,
                'label' => 'Work schedule',
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'workScheduleType',
                    'data-multiple' => 'false',
                ],
            ])
            ->add('ageFrom', 'choice', [
                'choices' => CvFilter::getAgeChoices(),
                /** @Ignore */
                'label' => false,
                'empty_value' => 'Choose from',
                'required' => false,
                'mapped' => false,
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'ageFrom',
                    'data-multiple' => 'false',
                ],
            ])
            ->add('ageTo', 'choice', [
                'choices' => CvFilter::getAgeChoices(),
                /** @Ignore */
                'label' => false,
                'empty_value' => 'Choose to',
                'required' => false,
                'mapped' => false,
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'ageTo',
                    'data-multiple' => 'false',
                ],
            ])
            ->add('language', 'hidden', [
                'required' => false,
                'label' => 'Languages',
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'language',
                    'data-multiple' => 'true',
                    'placeholder' => 'No matter',
                ],
            ])
            ->add('driverLicenseCategory', 'entity', [
                'class' => 'RDWCvBundle:DriverLicenseCategory',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('i')
                        ->orderBy('i.position', 'ASC');
                },
                'empty_value' => 'No matter',
                'property' => 'title',
                'required' => false,
                'label' => 'Driver license category',
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'driverLicenseCategory',
                    'data-multiple' => 'false',
                ],
            ])
            ->add('hasCar', 'choice', [
                'choices'   => [true => 'Yes', false => 'No'],
                'label' => 'Has a car',
                'required' => false,
                'empty_value' => 'No mater',
                'expanded' => true,
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'hasCar',
                    'data-multiple' => 'false',
                ],
            ])
            ->add('employmentType', 'entity', [
                'class' => 'RDWJobBundle:EmploymentType',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('i')
                        ->orderBy('i.position', 'ASC');
                },
                'empty_value' => 'No matter',
                'property' => 'title',
                'required' => false,
                'label' => 'Employment type',
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'employmentType',
                    'data-multiple' => 'false',
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\CvBundle\Model\CvFilter',
            'csrf_protection'   => false,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'cvs';
    }
}
