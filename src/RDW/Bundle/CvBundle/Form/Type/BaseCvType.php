<?php

namespace RDW\Bundle\CvBundle\Form\Type;

use Braincrafted\Bundle\BootstrapBundle\Form\Type\BootstrapCollectionType;
use RDW\Bundle\AppBundle\Form\Type\ProfessionalAreaCollectionPopupType;
use RDW\Bundle\AppBundle\Form\Type\PositionAutocompleteType;
use RDW\Bundle\AppBundle\Form\Type\SalaryType;
use RDW\Bundle\AppBundle\Form\Type\Select2\CityCollectionType;
use RDW\Bundle\AppBundle\Form\Type\Select2\CityType;
use RDW\Bundle\AppBundle\Form\Type\Select2\ScopeType;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\AppBundle\Form\DataTransformer\FormattedNumberTransformer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class BaseCvType
 *
 * @package RDW\Bundle\CvBundle\Form\Type
 */
class BaseCvType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Name',
                'required' => true,
                'attr' => ['placeholder' => 'Name placeholder'],
            ])
            ->add('patronymic', TextType::class, [
                'label' => 'Patronymic',
                'required' => false,
                'attr' => ['placeholder' => 'Patronymic placeholder'],
            ])
            ->add('surname', TextType::class, [
                'label' => 'Surname',
                'required' => false,
                'attr' => ['placeholder' => 'Surname placeholder'],
            ])
            ->add('gender', ChoiceType::class, [
                'label' => 'Gender',
                'required' => true,
                'expanded' => true,
                'choices' => RegisteredUser::getGenderChoices(),
            ])
            ->add('birthday', BirthdayType::class, [
                'label' => 'Birthday',
                'years' => range(1925, date('Y')),
            ])
            ->add('cities', CityCollectionType::class, [
                'label' => 'Cities',
                'required' => true,
            ])
            ->add('contactPersonPhones', BootstrapCollectionType::class, [
                'type' => new ContactPersonPhoneType(),
                'allow_add' => (!$options['disabled']) ? true : false,
                'allow_delete' => (!$options['disabled']) ? true : false,
                'by_reference' => false,
                'required' => false,
                'label' => 'Phone',
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Email placeholder',
                ],
            ])
            ->add('fbContact', TextType::class, [
                'label' => 'Facebook contact',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Facebook contact placeholder',
                ],
            ])
            ->add('vkContact', TextType::class, [
                'label' => 'VK contact',
                'required' => false,
                'attr' => [
                    'placeholder' => 'VK contact placeholder',
                ],
            ])
            ->add('okContact', TextType::class, [
                'label' => 'Odnoklassniki contact',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Odnoklassniki contact placeholder',
                ],
            ])
            ->add('gPlusContact', TextType::class, [
                'label' => 'Google plus contact',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Google pluse contact placeholder',
                ],
            ])
            ->add('twitterContact', TextType::class, [
                'label' => 'Twitter contact',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Twitter contact placeholder',
                ],
            ])
            ->add('linkedInContact', TextType::class, [
                'label' => 'LinkedId contact',
                'required' => false,
                'attr' => [
                    'placeholder' => 'LinkedIn contact placeholder',
                ],
            ])
            ->add('skypeContact', TextType::class, [
                'label' => 'Skype contact',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Skype contact placeholder',
                ],
            ])
            ->add('position', PositionAutocompleteType::class, ['required' => true])
            ->add('professionalArea', ProfessionalAreaCollectionPopupType::class, [
                'required' => true,
                'label' => 'Professional Area',
            ])
            ->add('scope', ScopeType::class)
            ->add('workingPlaceCity', CityType::class, [
                'label' => 'Working place city',
            ])
            ->add('salaryFrom', SalaryType::class, [
                'required' => false,
                'label' => 'Salary from',
                'attr' => [
                    'placeholder' => 'Salary from placeholder',
                ],
            ])
            ->add('salaryTo', SalaryType::class, [
                'required' => false,
                'label' => 'Salary to',
                'attr' => [
                    'placeholder' => 'Salary to placeholder',
                ],
            ])
            ->add('workScheduleType', EntityType::class, [
                //'class' => 'RDWJobBundle:WorkScheduleType',
                'class' => \RDW\Bundle\JobBundle\Entity\WorkScheduleType::class,
                'empty_value' => 'Not important',
                'property' => 'title',
                'required' => false,
                'label' => 'Work schedule type',
            ])
            ->add('educations', BootstrapCollectionType::class, [
                'type' => EducationFormType::class,
                'allow_add' => (!$options['disabled']),
                'allow_delete' => (!$options['disabled']),
                'by_reference' => false,
                /** @Ignore */
                'label' => false,
                'required' => false,
                'cascade_validation' => true,
            ])
            ->add('trainings', BootstrapCollectionType::class, [
                'type' => TrainingFormType::class,
                'allow_add' => (!$options['disabled']),
                'allow_delete' => (!$options['disabled']),
                'by_reference' => false,
                /** @Ignore */
                'label' => false,
                'required' => false,
                'validation_groups' => 'create_training',
                'cascade_validation' => true,
            ])
            ->add('workExperience', BootstrapCollectionType::class, [
                'type' => WorkExperienceFormType::class,
                'allow_add' => (!$options['disabled']),
                'allow_delete' => (!$options['disabled']),
                'by_reference' => false,
                /** @Ignore */
                'label' => false,
                'required' => false,
                'cascade_validation' => true,
            ])
            ->add('languages', BootstrapCollectionType::class, [
                'type' => LanguageFormType::class,
                'allow_add' => (!$options['disabled']),
                'allow_delete' => (!$options['disabled']),
                'by_reference' => false,
                'required' => false,
            ])
            ->add('otherInformation', TextareaType::class, [
                'label' => 'Other information',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Other information placeholder',
                ],
            ])
            ->add('professionalSkill', TextareaType::class, [
                'label' => 'Professional skill',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Professional skill placeholder',
                ],
            ])
            ->add('driverLicenseCategories', EntityType::class, [
                'label' => 'Driver license category',
                'class' => 'RDWCvBundle:DriverLicenseCategory',
                'property' => 'title',
                'expanded' => true,
                'multiple' => true,
                'required' => false,
            ])
            ->add('haveCar', ChoiceType::class, [
                'label' => 'Have car',
                'choices' => [
                    true => 'Yes',
                ],
                'expanded' => true,
                'multiple' => false,
                'required' => false,
            ])
            ->add('validTill', DateType::class, [
                'input' => 'datetime',
                'required' => false,
            ])
            ->add('businessTrip', EntityType::class, [
                'label' => 'Business trip possibilities',
                'class' => 'RDWCvBundle:BusinessTrip',
                'required' => false,
                'property' => 'title',
                'empty_value' => 'Choose option',
            ])
            ->add('computerSkill', TextareaType::class, [
                'label' => 'Computer skill',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Compupter skill placeholder',
                ],
            ])
            ->add('children', NumberType::class, [
                'label' => 'Children',
                'required' => false,
                'attr' => ['placeholder' => 'Children placeholder'],
            ])
            ->add('youtubeVideo', TextType::class, [
                'label' => 'Video',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Video placeholder',
                ],
            ])
            ->add('maritalStatus', EntityType::class, [
                'class' => 'RDWUserBundle:MaritalStatus',
                'empty_value' => 'Choose option',
                'property' => 'title',
                'required' => false,
                'label' => 'Marital status',
            ])
            ->add('employmentType', EntityType::class, [
                'class' => 'RDWJobBundle:EmploymentType',
                'empty_value' => 'Not important',
                'property' => 'title',
                'required' => false,
                'label' => 'Employment type',
            ])
            ->add('confidential', CheckboxType::class, [
                'label' => 'Make CV confidential',
                'required' => false,
            ])
            ->add('mainInfoHidden', CheckboxType::class, [
                'label' => 'Hide my contact info',
                'required' => false,
            ])
            ->add('socialInfoHidden', CheckboxType::class, [
                'label' => 'Hide my social info',
                'required' => false,
            ]);

        $builder
            ->get('salaryFrom')
            ->addModelTransformer(new FormattedNumberTransformer());
        $builder
            ->get('salaryTo')
            ->addModelTransformer(new FormattedNumberTransformer());
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return '';
    }
}
