<?php

namespace RDW\Bundle\CvBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class LanguageFormType
 *
 * @author Vytautas Beliunas <vytautas@eface.lt>
 */
class LanguageFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('language', 'entity', [
                'class' => 'RDWJobBundle:Language',
                'empty_value' => 'Choose option',
                'property' => 'title',
                'required' => false,
                'label' => 'Language',
            ])
            ->add('level', 'entity', [
                'class' => 'RDWJobBundle:LanguageLevel',
                'empty_value' => 'Choose option',
                'property' => 'title',
                'required' => false,
                'label' => 'Level',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\CvBundle\Entity\CvLanguage',
            'cascade_validation' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_cv_language';
    }
}
