<?php

namespace RDW\Bundle\CvBundle\Form\Type;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\CvBundle\Form\Transformer\MultipleCvTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class MultipleCvMessageType
 *
 * @package RDW\Bundle\CvBundle\Form\Type
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class MultipleCvMessageType extends AbstractType
{
    /**
     * @var ObjectManager
     */
    private $entityManager;

    /**
     * @param ObjectManager $entityManager
     */
    public function __construct(ObjectManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $multipleCvTransformer = new MultipleCvTransformer($this->entityManager);

        $builder->add(
            $builder->create('cvs', 'hidden', [
                'required' => false,
            ])
            ->addModelTransformer($multipleCvTransformer)
        );

        $builder
            ->add('companyTitle', 'text', [
                'label' => 'Company title',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Company title placeholder'
                ]
            ])
            ->add('contactName', 'text', [
                'label' => 'Name',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Contact name placeholder'
                ]
            ])
            ->add('contactEmail', 'email', [
                'label' => 'Email',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Contact email placeholder'
                ]
            ])
            ->add('contactPhone', 'text', [
                'label' => 'Phone',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Contact phone placeholder'
                ]
            ])
            ->add('message', 'textarea', [
                'label' => 'Message',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Message placeholder'
                ]
            ])
            ->add('save', 'submit', [
                'label' => 'Send',
                'attr' => [
                    'data-handler' => 'saveCvMessage',
                    'data-content-holder' => 'cv-message-form-holder',
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\CvBundle\Model\MultipleCvMessage',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_cv_multiple_message';
    }
}
