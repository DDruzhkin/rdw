<?php

namespace RDW\Bundle\CvBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class CvBlockType
 *
 * @package RDW\Bundle\CvBundle\Form\Type
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class CvBlockType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('blockReason', 'textarea', [
                'label' => 'Block reason',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Block placeholder'
                ]
            ])
            ->add('submit', 'submit', [
                'label' => 'Save',
                'validation_groups' => ['block']
            ])
            ->add('cancel', 'submit', [
                'label' => 'Cancel',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\CvBundle\Entity\Cv'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_cv_block';
    }
}
