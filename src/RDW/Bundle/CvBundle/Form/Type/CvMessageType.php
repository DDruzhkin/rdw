<?php

namespace RDW\Bundle\CvBundle\Form\Type;

use RDW\Bundle\CvBundle\Entity\CvMessage;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class CvMessageType
 *
 * @package RDW\Bundle\CvBundle\Form\Type
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class CvMessageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var CvMessage $subscription */
        $message = $builder->getForm()->getData();

        $builder
            ->add('companyTitle', 'text', [
                'label' => 'Company title',
                'required' => true,
            ])
            ->add('contactName', 'text', [
                'label' => 'Name',
                'required' => true,
            ])
            ->add('contactEmail', 'email', [
                'label' => 'Email',
                'required' => true,
            ])
            ->add('contactPhone', 'text', [
                'label' => 'Phone',
                'required' => false,
            ])
            ->add('message', 'textarea', [
                'label' => 'Message',
                'required' => true,
            ]);

        if ($message->getUser() instanceof RegisteredUser) {
            $builder
                ->add('save', 'submit', [
                    'label' => 'Send',
                ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\CvBundle\Entity\CvMessage',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_cv_message';
    }
}
