<?php

namespace RDW\Bundle\CvBundle\Form\Transformer;

use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class IncompleteDateTransformer
 *
 * @package RDW\Bundle\CvBundle\Form\Transformer
 */
class IncompleteDateTransformer implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transform($object)
    {
        return $object;
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($date)
    {
        if (!is_array($date)) {
            return $date;
        }

        if (empty($date['year'])) {
            return $date;
        }

        if (empty($date['day'])) {
            $date['day']=1;
        }

        if (empty($date['month'])) {
            $date['month']=1;
        }

        return $date;
    }
}