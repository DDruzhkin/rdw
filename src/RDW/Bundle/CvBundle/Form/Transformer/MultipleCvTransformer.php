<?php

namespace RDW\Bundle\CvBundle\Form\Transformer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\PersistentCollection;
use RDW\Bundle\CvBundle\Entity\Cv;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class MultipleCvTransformer
 *
 * @package RDW\Bundle\CvBundle\Form\Transformer
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class MultipleCvTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $entityManager;

    /**
     * @param ObjectManager $entityManager
     */
    public function __construct(ObjectManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Transform to objects to string of objects' ids
     *
     * @param PersistentCollection $entity
     *
     * @return string
     */
    public function transform($entity)
    {
        $ids = [];

        if (null !== $entity && ($totalItems = $entity->count())) {
            foreach ($entity as $key => $value) {
                $ids[] = $value->getId();
            }
        }

        return implode(',', $ids);
    }

    /**
     * @param string $propertyValueIds
     *
     * @return mixed|object
     */
    public function reverseTransform($propertyValueIds)
    {
        $collection = new ArrayCollection();
        $ids = explode(',', $propertyValueIds);

        $repository = $this->entityManager->getRepository('RDWCvBundle:Cv');

        foreach ($ids as $id) {
            $cv = $repository->find(trim($id));

            if ($cv instanceof Cv) {
                $collection->add($cv);
            }
        }

        return $collection;
    }
}
