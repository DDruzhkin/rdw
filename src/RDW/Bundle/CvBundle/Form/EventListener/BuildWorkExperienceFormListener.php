<?php

namespace RDW\Bundle\CvBundle\Form\EventListener;

use Doctrine\ORM\EntityRepository;
use RDW\Bundle\CvBundle\Entity\Cv;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class BuildWorkExperienceFormListener
 * @package RDW\Bundle\CvBundle\Form\EventListener
 */
class BuildWorkExperienceFormListener implements EventSubscriberInterface
{
    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::POST_SUBMIT => 'postSubmit',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function preSetData(FormEvent $event)
    {
        $form = $event->getForm();
        /** @var Cv $data */
        $data = $event->getData();

        $form
            ->add('withoutWorkExperience', 'checkbox', [
                'required'  => false,
                'mapped'    => false,
                'data'      => ($data->getId() > 0 && ! $data->getWorkExperienceType()),
                'attr'      => [
                    'class' => 'form_toggle',
                ],
            ]);

        $form
            ->add('workExperienceType', 'entity', [
                'class' => 'RDWJobBundle:WorkExperienceType',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('i')
                        ->orderBy('i.position', 'ASC');
                },
                'property' => 'title',
                'multiple' => false,
                'required' => false,
                'empty_value' => 'No matter',
                'label' => 'Work experience in years',
            ]);
    }

    /**
     * {@inheritDoc}
     */
    public function postSubmit(FormEvent $event)
    {
        $form = $event->getForm();

        $withoutWorkExperience = $form->get('withoutWorkExperience')->getData();
        $workExperienceType = $form->get('workExperienceType')->getData();

        if (! $withoutWorkExperience && is_null($workExperienceType)) {
            $form->get('workExperienceType')->addError(new FormError('Work experience cannot be empty', 'validators', []));
        }
    }
}
