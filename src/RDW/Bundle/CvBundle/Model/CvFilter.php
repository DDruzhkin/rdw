<?php

namespace RDW\Bundle\CvBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\JobBundle\Entity\ProfessionalArea;
use RDW\Bundle\JobBundle\Entity\Position;
use RDW\Bundle\LocationBundle\Entity\City;
use RDW\Bundle\SubscriptionBundle\Entity\Subscription;
use RDW\Bundle\SubscriptionBundle\Model\FilterInterface as SubscriptionFilterInterface;
use RDW\Bundle\UserBundle\Entity\User;

/**
 * Class CvFilter
 *
 * @package RDW\Bundle\CvBundle\Model
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class CvFilter implements SubscriptionFilterInterface
{
    const UPDATED_AT_1_MONTH = '-1 month';
    const UPDATED_AT_10_DAYS = '-10 days';
    const UPDATED_AT_3_DAYS = '-3 days';
    const UPDATED_AT_1_DAY = '-1 day';

    /**
     * @var ArrayCollection
     */
    protected $cities;

    protected $query;

    /**
     * @var ArrayCollection
     */
    protected $professionalArea;

    /**
     * @var ArrayCollection
     */
    protected $scopes;

    /**
     * @var ArrayCollection
     */
    protected $positions;

    /**
     * @var string
     */
    protected $updatedAt;

    /**
     * @var string
     */
    protected $gender;

    /**
     * @var boolean
     */
    protected $hasCar;

    /**
     * @var int
     */
    protected $salaryFrom;

    /**
     * @var int
     */
    protected $salaryTo;

    /**
     * @var int
     */
    protected $ageFrom;

    /**
     * @var int
     */
    protected $ageTo;

    /**
     * @var \RDW\Bundle\JobBundle\Entity\WorkScheduleType
     */
    protected $workScheduleType;

    /**
     * @var \RDW\Bundle\CvBundle\Entity\DriverLicenseCategory
     */
    protected $driverLicenseCategory;

    /**
     * @var \RDW\Bundle\CvBundle\Entity\BusinessTrip
     */
    protected $businessTrip;

    /**
     * @var \RDW\Bundle\JobBundle\Entity\EducationType
     */
    protected $educationType;

    /**
     * @var \RDW\Bundle\JobBundle\Entity\WorkExperienceType
     */
    protected $workExperienceType;

    /**
    * @var \RDW\Bundle\JobBundle\Entity\EmploymentType
    */
    protected $employmentType;

    /**
     * @var \RDW\Bundle\JobBundle\Entity\Language
     */
    protected $language;

    /**
     * @var array
     */
    private $statuses;

    /**
     * @var User
     */
    private $user;

    /**
     * @var int
     */
    private $notId;

    /**
     * @var bool
     */
    private $notShowViewedCvs;

    /**
     * @var City
     */
    private $workingPlaceCity;

    /**
     * @var ArrayCollection
     */
    protected $onlyItems;

    /**
     * constructor
     */
    public function __construct()
    {
        $this->cities = new ArrayCollection();
        $this->scopes = new ArrayCollection();
        $this->professionalArea = new ArrayCollection();
        $this->positions = new ArrayCollection();
        $this->onlyItems = new ArrayCollection();
    }

    /**
     * @return array
     */
    public function getStatuses()
    {
        return $this->statuses;
    }

    /**
     * @param array $statuses
     *
     * @return $this
     */
    public function setStatuses(array $statuses)
    {
        $this->statuses = $statuses;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @param ArrayCollection $cities
     *
     * @return $this
     */
    public function setCities($cities)
    {
        $this->cities = $cities;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getScopes()
    {
        return $this->scopes;
    }

    /**
     * @param ArrayCollection $scopes
     *
     * @return $this
     */
    public function setScopes(ArrayCollection $scopes)
    {
        $this->scopes = $scopes;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPositions()
    {
        return $this->positions;
    }

    /**
     * @param ArrayCollection $positions
     *
     * @return $this
     */
    public function setPositions(ArrayCollection $positions)
    {
        $this->positions = $positions;

        return $this;
    }

    /**
     * @param Position $position
     */
    public function addPosition(Position $position)
    {
        if (! $this->positions->contains($position)) {
            $this->positions->add($position);
        }
    }

    /**
     * @param Position $position
     *
     * @return $this
     */
    public function removePosition(Position $position)
    {
        if ($this->positions->contains($position)) {
            $this->positions->removeElement($position);
        }

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getProfessionalArea()
    {
        return $this->professionalArea;
    }

    /**
     * @param ArrayCollection $professionalArea
     *
     * @return $this
     */
    public function setProfessionalArea($professionalArea)
    {
        $this->professionalArea = $professionalArea;

        return $this;
    }

    /**
     * @param ProfessionalArea $professionalArea
     * @return $this
     *
     */
    public function addProfessionalArea(ProfessionalArea $professionalArea)
    {
        $this->professionalArea->add($professionalArea);

        return $this;
    }

    /**
     * @param ProfessionalArea $professionalArea
     *
     * @return $this
     */
    public function removeProfessionalArea(ProfessionalArea $professionalArea)
    {
        if ($this->professionalArea->contains($professionalArea)) {
            $this->professionalArea->removeElement($professionalArea);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param string $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return array
     */
    public static function getUpdatedAtChoices()
    {
        return [
            self::UPDATED_AT_1_DAY => '1 day ago',
            self::UPDATED_AT_3_DAYS => '3 days ago',
            self::UPDATED_AT_10_DAYS => '10 days ago',
            self::UPDATED_AT_1_MONTH => '1 month ago',
        ];
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     *
     * @return $this
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return int
     */
    public function getSalaryFrom()
    {
        return $this->salaryFrom;
    }

    /**
     * @param int $salaryFrom
     *
     * @return $this
     */
    public function setSalaryFrom($salaryFrom)
    {
        $this->salaryFrom = $salaryFrom;

        return $this;
    }

    /**
     * @return int
     */
    public function getSalaryTo()
    {
        return $this->salaryTo;
    }

    /**
     * @param int $salaryTo
     *
     * @return $this
     */
    public function setSalaryTo($salaryTo)
    {
        $this->salaryTo = $salaryTo;

        return $this;
    }

    /**
     * @return \RDW\Bundle\JobBundle\Entity\WorkScheduleType
     */
    public function getWorkScheduleType()
    {
        return $this->workScheduleType;
    }

    /**
     * @param \RDW\Bundle\JobBundle\Entity\WorkScheduleType $workScheduleType
     *
     * @return $this
     */
    public function setWorkScheduleType($workScheduleType)
    {
        $this->workScheduleType = $workScheduleType;

        return $this;
    }

    /**
     * @return \RDW\Bundle\JobBundle\Entity\EducationType
     */
    public function getEducationType()
    {
        return $this->educationType;
    }

    /**
     * @param \RDW\Bundle\JobBundle\Entity\EducationType $educationType
     *
     * @return $this
     */
    public function setEducationType($educationType)
    {
        $this->educationType = $educationType;

        return $this;
    }

    /**
     * @return \RDW\Bundle\JobBundle\Entity\EmploymentType
     */
    public function getEmploymentType()
    {
        return $this->employmentType;
    }

    /**
     * @param \RDW\Bundle\JobBundle\Entity\EmploymentType $employmentType
     */
    public function setEmploymentType($employmentType)
    {
        $this->employmentType = $employmentType;
    }

    /**
     * @return \RDW\Bundle\JobBundle\Entity\Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param \RDW\Bundle\JobBundle\Entity\Language $language
     *
     * @return $this
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @return int
     */
    public function getAgeFrom()
    {
        return $this->ageFrom;
    }

    /**
     * @param int $ageFrom
     *
     * @return $this
     */
    public function setAgeFrom($ageFrom)
    {
        $this->ageFrom = $ageFrom;

        return $this;
    }

    /**
     * @return int
     */
    public function getAgeTo()
    {
        return $this->ageTo;
    }

    /**
     * @param int $ageTo
     *
     * @return $this
     */
    public function setAgeTo($ageTo)
    {
        $this->ageTo = $ageTo;

        return $this;
    }

    /**
     * @return \RDW\Bundle\UserBundle\Entity\RegisteredUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param \RDW\Bundle\UserBundle\Entity\RegisteredUser $user
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return int
     */
    public function getNotId()
    {
        return $this->notId;
    }

    /**
     * @param int $notId
     */
    public function setNotId($notId)
    {
        $this->notId = $notId;
    }

    /**
     * @return boolean
     */
    public function getNotShowViewedCvs()
    {
        return $this->notShowViewedCvs;
    }

    /**
     * @param boolean $notShowViewedCvs
     */
    public function setNotShowViewedCvs($notShowViewedCvs)
    {
        $this->notShowViewedCvs = $notShowViewedCvs;
    }

    /**
     * {@inheritdoc}
     */
    public function update(Subscription $subscription)
    {
        $workScheduleType = ($subscription->getWorkSchedules()) ? $subscription->getWorkSchedules()->first() : null;

        $this->setCities($subscription->getCities());
        $this->setWorkScheduleType($workScheduleType);
        $this->setScopes($subscription->getScopes());
        $this->setPositions($subscription->getPositions());

        return $this;
    }

    /**
     * @return array
     */
    public static function getAgeChoices()
    {
        $result = [];

        for ($i = 15; $i <= 70; $i++) {
            $result[$i] = $i;
        }

        return $result;
    }

    /**
     * @return \RDW\Bundle\CvBundle\Entity\DriverLicenseCategory
     */
    public function getDriverLicenseCategory()
    {
        return $this->driverLicenseCategory;
    }

    /**
     * @param \RDW\Bundle\CvBundle\Entity\DriverLicenseCategory $driverLicenseCategory
     */
    public function setDriverLicenseCategory($driverLicenseCategory)
    {
        $this->driverLicenseCategory = $driverLicenseCategory;
    }

    /**
     * @return boolean
     */
    public function getHasCar()
    {
        return $this->hasCar;
    }

    /**
     * @param boolean $hasCar
     */
    public function setHasCar($hasCar)
    {
        $this->hasCar = $hasCar;
    }

    /**
     * @return \RDW\Bundle\CvBundle\Entity\BusinessTrip
     */
    public function getBusinessTrip()
    {
        return $this->businessTrip;
    }

    /**
     * @param \RDW\Bundle\CvBundle\Entity\BusinessTrip $businessTrip
     */
    public function setBusinessTrip($businessTrip)
    {
        $this->businessTrip = $businessTrip;
    }

    /**
     * @return \RDW\Bundle\JobBundle\Entity\WorkExperienceType
     */
    public function getWorkExperienceType()
    {
        return $this->workExperienceType;
    }

    /**
     * @param \RDW\Bundle\JobBundle\Entity\WorkExperienceType $workExperienceType
     */
    public function setWorkExperienceType($workExperienceType)
    {
        $this->workExperienceType = $workExperienceType;
    }

    public function getWorkingPlaceCity()
    {
        return $this->workingPlaceCity;
    }

    public function setWorkingPlaceCity($workingPlaceCity)
    {
        $this->workingPlaceCity = $workingPlaceCity;
    }

    /**
     * @return ArrayCollection
     */
    public function getOnlyItems()
    {
        return $this->onlyItems;
    }

    /**
     * @param ArrayCollection $onlyItems
     */
    public function setOnlyItems($onlyItems)
    {
        $this->onlyItems = $onlyItems;
    }

    /**
     * @param Cv $cv
     */
    public function addOnlyItem(Cv $cv)
    {
        if (! $this->onlyItems->contains($cv)) {
            $this->onlyItems->add($cv);
        }
    }

    /**
     * @param Cv $cv
     *
     * @return $this
     */
    public function removeOnlyItem(Cv $cv)
    {
        if ($this->onlyItems->contains($cv)) {
            $this->onlyItems->removeElement($cv);
        }

        return $this;
    }

    public function getOrderByFromRequestQuery(array $query = [])
    {
        $orderBy = [];
        $allowedOrderByFields = $this->getAllowedOrderByFields();
        $direction = (isset($query['direction']) && 'asc' === $query['direction']) ? 'ASC' : 'DESC';

        if (isset($query['sort']) && array_key_exists($query['sort'], $allowedOrderByFields)) {
            $orderBy[$allowedOrderByFields[$query['sort']]] = $direction;
        }

        return $orderBy;
    }

    public function getQuery()
    {
        return $this->query;
    }

    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }


    private function getAllowedOrderByFields()
    {
        return [
            'scope' => 'scope_title',
            'salaryFrom' => 'salary_from',
            'updatedAt' => 'updated_at',
        ];
    }
}
