<?php

namespace RDW\Bundle\CvBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class MultipleCvMessage
 *
 * @package RDW\Bundle\CvBundle\Model
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class MultipleCvMessage implements CvMessageInterface
{
    /**
     * @var string
     *
     * Assert\NotBlank(message="Company title cannot be empty")
     */
    private $companyTitle;

    /**
     * @var string
     *
     * Assert\NotBlank(message="Name cannot be empty")
     */
    private $contactName;

    /**
     * @var string
     *
     * Assert\NotBlank(message="Email cannot be empty")
     * Assert\Email(message="Email must be a valid email address")
     */
    private $contactEmail;

    /**
     * @var ArrayCollection
     *
     * Assert\NotBlank()
     */
    protected $cvs;

    /**
     * @var string
     *
     * Assert\Regex(
     *      pattern="/^([\+0-9\s\-]+)$/",
     *      message="Field must be a valid phone number and can have only numbers, +, - and spaces"
     * )
     */
    private $contactPhone;

    /**
     * @var string
     *
     * Assert\NotBlank(message="Message cannot be empty")
     */
    private $message;

    /**
     * @return string
     */
    public function getCompanyTitle()
    {
        return $this->companyTitle;
    }

    /**
     * @param string $companyTitle
     *
     * @return $this
     */
    public function setCompanyTitle($companyTitle)
    {
        $this->companyTitle = $companyTitle;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactName()
    {
        return $this->contactName;
    }

    /**
     * @param string $contactName
     *
     * @return $this
     */
    public function setContactName($contactName)
    {
        $this->contactName = $contactName;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * @param string $contactEmail
     *
     * @return $this
     */
    public function setContactEmail($contactEmail)
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactPhone()
    {
        return $this->contactPhone;
    }

    /**
     * @param string $contactPhone
     *
     * @return $this
     */
    public function setContactPhone($contactPhone)
    {
        $this->contactPhone = $contactPhone;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getCvs()
    {
        return $this->cvs;
    }

    /**
     * @param ArrayCollection $cvs
     *
     * @return $this
     */
    public function setCvs(ArrayCollection $cvs)
    {
        $this->cvs = $cvs;

        return $this;
    }
}
