<?php

namespace RDW\Bundle\CvBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;


/**
 * Class MultipleCvMessage
 *
 * @package RDW\Bundle\CvBundle\Model
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
interface CvMessageInterface
{
    /**
     * @return string
     */
    public function getCompanyTitle();

    /**
     * @param string $companyTitle
     *
     * @return $this
     */
    public function setCompanyTitle($companyTitle);

    /**
     * @return string
     */
    public function getContactName();

    /**
     * @param string $contactName
     *
     * @return $this
     */
    public function setContactName($contactName);

    /**
     * @return string
     */
    public function getContactEmail();

    /**
     * @param string $contactEmail
     *
     * @return $this
     */
    public function setContactEmail($contactEmail);

    /**
     * @return string
     */
    public function getContactPhone();

    /**
     * @param string $contactPhone
     *
     * @return $this
     */
    public function setContactPhone($contactPhone);

    /**
     * @return string
     */
    public function getMessage();

    /**
     * @param string $message
     *
     * @return $this
     */
    public function setMessage($message);
}
