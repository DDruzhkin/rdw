<?php

namespace RDW\Bundle\CvBundle\Model;

use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\UserBundle\Entity\User;

/**
 * Class UsersViewFilter
 * @package RDW\Bundle\JobBundle\Model
 *
 * @author Paulius Aleliūnas <paulius@eface.lt>
 */
class UsersViewFilter
{
    /**
     * @var Cv
     */
    protected $cv;

    /**
     * @var User
     */
    protected $user;

    /**
     * @return Cv
     */
    public function getCv()
    {
        return $this->cv;
    }

    /**
     * @param Cv $cv
     *
     * @return $this
     */
    public function setCv(Cv $cv)
    {
        $this->cv = $cv;

        return $this;
    }

    /**
     * @return \RDW\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param \RDW\Bundle\UserBundle\Entity\User $user
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }
}
