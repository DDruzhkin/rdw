<?php

namespace RDW\Bundle\CvBundle\Provider;

use Doctrine\Common\Persistence\ManagerRegistry;
use RDW\Bundle\AnalyticBundle\Model\AnalyticDataModel;
use RDW\Bundle\AnalyticBundle\Model\AnalyticDataProviderInterface;
use RDW\Bundle\CvBundle\Entity\Cv;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

class CvAnalyticDataProvider implements AnalyticDataProviderInterface
{
    /** @var ManagerRegistry */
    protected $doctrine;

    /** @var RequestStack */
    protected $requestStack;

    /** @var RouterInterface */
    protected $router;

    /**
     * @param ManagerRegistry $doctrine
     * @param RouterInterface $router
     */
    public function __construct(ManagerRegistry $doctrine, RouterInterface $router)
    {
        $this->doctrine = $doctrine;
        $this->router = $router;
    }

    /**
     * {@inheritDoc}
     */
    public function isUriSupported($uri)
    {
        $route = $this->router->match($uri);
        if (!isset($route['_route']) || !($route['_route'] === 'rdw_cv.cv.view') || !isset($route['id'])) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function fillData($uri, AnalyticDataModel $dataModel)
    {
        $route = $this->router->match($uri);
        $id = $route['id'];
        $cv = $this->doctrine->getManagerForClass(Cv::class)->find(Cv::class, $id);
        if ($cv) {
            $dataModel
                ->setUri($uri)
                ->setViewsUniqueCount($cv->getViewsCount())
                ->setViewsCount($cv->getTotalViewsCount());
        }
    }
}
