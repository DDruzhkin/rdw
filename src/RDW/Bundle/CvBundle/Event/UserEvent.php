<?php

namespace RDW\Bundle\CvBundle\Event;

use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class UserEvent
 *
 * @package RDW\Bundle\CvBundle\Event
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class UserEvent extends CvEvent
{
    /**
     * @var \RDW\Bundle\CvBundle\Entity\Cv
     */
    private $cv;

    /**
     * @var \RDW\Bundle\UserBundle\Entity\User
     */
    private $user;

    /**
     * Constructor
     *
     * @param Cv   $cv
     * @param User $user
     */
    public function __construct(Cv $cv, User $user = null)
    {
        parent::__construct($cv);

        $this->user = $user;
    }

    /**
     * @return \RDW\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
