<?php

namespace RDW\Bundle\CvBundle\Event;

use Doctrine\Common\Collections\ArrayCollection;
use RDW\Bundle\CvBundle\Model\CvMessageInterface;
use RDW\Bundle\CvBundle\Model\MultipleCvMessage;
use RDW\Bundle\UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class MultipleCvMessageEvent
 *
 * @package RDW\Bundle\CvBundle\Event
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class MultipleCvMessageEvent extends Event
{
    /**
     * @var MultipleCvMessage
     */
    private $message;

    /**
     * @var ArrayCollection
     */
    private $cvs;

    /**
     * @var \RDW\Bundle\UserBundle\Entity\User
     */
    private $user;

    /**
     * @param ArrayCollection    $cvs
     * @param CvMessageInterface $message
     * @param User               $user
     */
    public function __construct(ArrayCollection $cvs, CvMessageInterface $message, User $user)
    {
        $this->cvs = $cvs;
        $this->message = $message;
        $this->user = $user;
    }

    /**
     * @return CvMessageInterface
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return ArrayCollection
     */
    public function getCvs()
    {
        return $this->cvs;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}
