<?php

namespace RDW\Bundle\CvBundle\Event;

use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\ManageBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class CvEvent
 *
 * @package RDW\Bundle\CvBundle\Event
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class BlockEvent extends CvEvent
{
    /**
     * @var User
     */
    private $user;

    /**
     * Constructor
     *
     * @param Cv   $cv
     * @param User $user
     */
    public function __construct(Cv $cv, User $user)
    {
        parent::__construct($cv);

        $this->user = $user;
    }

    /**
     * @return \RDW\Bundle\ManageBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
