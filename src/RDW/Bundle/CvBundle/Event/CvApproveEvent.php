<?php

namespace RDW\Bundle\CvBundle\Event;

use RDW\Bundle\CvBundle\Entity\Cv;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CvApproveEvent
 *
 * @package RDW\Bundle\CvBundle\Event
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class CvApproveEvent extends Event
{
    /**
     * @var \RDW\Bundle\CvBundle\Entity\Cv
     */
    private $cv;

    /**
     * @var Response
     */
    private $response;

    /**
     * @param Cv $cv
     */
    public function __construct(Cv $cv)
    {
        $this->cv = $cv;
    }

    /**
     * @return Cv
     */
    public function getCv()
    {
        return $this->cv;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Response $response
     *
     * @return $this
     */
    public function setResponse($response)
    {
        $this->response = $response;

        return $this;
    }
}
