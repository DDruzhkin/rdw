<?php

namespace RDW\Bundle\CvBundle\Event;

use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\CvBundle\Entity\CvMessage;
use RDW\Bundle\UserBundle\Entity\User;
use RDW\Bundle\UserBundle\Entity\UserInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class CvMessageEvent
 *
 * @package RDW\Bundle\CvBundle\Event
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class CvMessageEvent extends Event
{
    /**
     * @var \RDW\Bundle\CvBundle\Entity\CvMessage
     */
    private $message;

    /**
     * @var \RDW\Bundle\UserBundle\Entity\UserInterface
     */
    private $user;

    /**
     * @param CvMessage $message
     * @param UserInterface $user
     */
    public function __construct(CvMessage $message, UserInterface $user)
    {
        $this->message = $message;
        $this->user = $user;
    }

    /**
     * @return \RDW\Bundle\CvBundle\Entity\CvMessage
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return UserInterface
     */
    public function getUser()
    {
        return $this->user;
    }
}
