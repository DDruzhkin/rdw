<?php

namespace RDW\Bundle\CvBundle\Event;

use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\ManageBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class CvEvent
 *
 * @package RDW\Bundle\CvBundle\Event
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class CvEvent extends Event
{
    /**
     * @var \RDW\Bundle\CvBundle\Entity\Cv
     */
    private $cv;

    /**
     * Constructor
     *
     * @param Cv $cv
     */
    public function __construct(Cv $cv)
    {
        $this->cv = $cv;
    }

    /**
     * @return Cv
     */
    public function getCv()
    {
        return $this->cv;
    }
}
