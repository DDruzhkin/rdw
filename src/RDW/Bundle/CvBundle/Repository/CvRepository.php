<?php

namespace RDW\Bundle\CvBundle\Repository;

use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\CvBundle\Model\CvFilter;
use Doctrine\ORM\QueryBuilder;
use RDW\Bundle\JobBundle\Entity\ProfessionalArea;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\AppBundle\Repository\AdRepository;

/**
 * Class CvRepository
 *
 * @package RDW\Bundle\CvBundle\Repository
 */
class CvRepository extends AdRepository
{
    /**
     * @param CvFilter $filter
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQueryBuilder(CvFilter $filter)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('c', 'user')
            ->from('RDWCvBundle:Cv', 'c')
            ->join('c.user', 'user')
            ->orderBy('c.updatedAt', 'DESC');

        $this->applyFiltersForQueryBuilder($queryBuilder, $filter);

        return $queryBuilder;
    }

    /**
     * @param RegisteredUser $user
     * @param int            $limit
     *
     * @return array
     */
    public function getHavingViews(RegisteredUser $user, $limit)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('c', 'user', 'COUNT(v) as viewers')
            ->from('RDWCvBundle:Cv', 'c')
            ->join('c.user', 'user')
            ->leftJoin('c.usersWhoViewed', 'v')
            ->join('v.user', 'userWhoViewed')
            ->andWhere('c.user = :user')
            ->andWhere('v.user <> :viewUser')
            ->having('viewers > 0')
            ->setParameter('user', $user)
            ->setParameter('viewUser', $user)
            ->groupBy('c.id')
            ->orderBy('c.updatedAt', 'DESC')
            ->setMaxResults($limit);

        $results = [];

        foreach ($queryBuilder->getQuery()->getResult() as $item) {
            $cv = $item[0];
            $views = $this->getViewers($cv);
            $cv->setUsersWhoViewed($views);

            $results[] = $cv;
        }

        return $results;
    }

    /**
     * @param CvFilter $filter
     *
     * @return QueryBuilder
     */
    public function getMyListQueryBuilder(CvFilter $filter)
    {
        $queryBuilder = $this->getListQueryBuilder($filter);

        if ($filter->getUser() instanceof RegisteredUser) {
            $queryBuilder
                ->andWhere('c.user = :user')
                ->setParameter('user', $filter->getUser());
        }

        $queryBuilder->leftjoin('c.cvMessages', 'cm');
        $queryBuilder->leftjoin('c.usersWhoViewed', 'uv');

        return $queryBuilder;
    }

    /**
     * @param CvFilter $filter
     *
     * @return int
     */
    public function countActiveCvs(CvFilter $filter)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('COUNT(c.id)')
            ->from('RDWCvBundle:Cv', 'c')
            ->join('c.user', 'user');

        $this->applyFiltersForQueryBuilder($queryBuilder, $filter);

        $total = $queryBuilder->getQuery()->getSingleScalarResult();

        return $total;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param CvFilter     $filter
     *
     * @return QueryBuilder
     */
    private function applyFiltersForQueryBuilder(QueryBuilder $queryBuilder, CvFilter $filter)
    {
        if ($filter->getUser() instanceof RegisteredUser) {
            $queryBuilder
                ->andWhere('c.user = :user')
                ->setParameter('user', $filter->getUser());
        }

        if ($filter->getNotId()) {
            $queryBuilder
                ->andWhere('c.id != :notId')
                ->setParameter('notId', $filter->getNotId());
        }

        if (is_array($filter->getStatuses())) {
            $statuses = $filter->getStatuses();
            if (!empty($statuses)) {
                $queryBuilder->andWhere('c.status IN (:statuses)')->setParameter('statuses', $filter->getStatuses());
            }
        }

        // not show user viewed cvs
        if ($filter->getNotShowViewedCvs() && $filter->getUser() instanceof RegisteredUser) {
            if ($filter->getUser()->getCvsViewed()) {
                $queryBuilder
                    ->andWhere('c.id NOT IN (:ids)')
                    ->setParameter('ids', $filter->getUser()->getCvsViewedIds());
            }
        }

        if ($filter->getProfessionalArea() && $filter->getProfessionalArea()->count() > 0) {
            $queryBuilder
                ->andWhere('cv.professionalArea IN (:professionalArea)')
                ->setParameter('professionalArea', $filter->getProfessionalArea()->getValues());
        }

        if ($filter->getPositions() && $filter->getPositions()->count()) {
            $queryBuilder
                ->andWhere('c.position IN (:positions)')
                ->setParameter('positions', $filter->getPositions()->getValues());
        }

        if ($filter->getOnlyItems() && $filter->getOnlyItems()->count()) {
            $queryBuilder
                ->andWhere('c IN (:cvs)')
                ->setParameter('cvs', $filter->getOnlyItems()->getValues());
        }

        return $queryBuilder;
    }

    public function findAllExpiredWithAttributesByServiceType($serviceType)
    {
        return $this->findAllExpiredWithAttributesByServiceTypeAndRepository('RDWCvBundle:Cv', $serviceType);
    }

    /**
     * @param array $ids
     *
     * @return array
     */
    public function getByIds(array $ids = [])
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('cv', 'scope', 'professionalArea', 'workingPlaceCity')
            ->from('RDWCvBundle:Cv', 'cv')
            ->leftJoin('cv.scope', 'scope')
            ->leftJoin('cv.professionalArea', 'professionalArea')
            ->leftJoin('cv.workingPlaceCity', 'workingPlaceCity')
            ->andWhere('cv.id IN (:ids)')
            ->setParameter('ids', $ids);

        if (! empty($ids)) {
            $queryBuilder
                ->select('cv, FIELD(cv.id, ' . implode(',', $ids) . ') AS HIDDEN ordering')
                ->orderBy('ordering');
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param int $id
     *
     * @return Cv|null
     */
    public function getById($id)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('cv', 'scope', 'cities', 'user', 'workingPlaceCity')
            ->from('RDWCvBundle:Cv', 'cv')
            ->leftJoin('cv.scope', 'scope')
            ->leftJoin('cv.cities', 'cities')
            ->leftJoin('cv.user', 'user')
            ->leftJoin('cv.workingPlaceCity', 'workingPlaceCity')
            ->andWhere('cv.id = :id')
            ->setParameter('id', $id);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function getPopularScopes($limit)
    {
        return $this->getPopular('RDWUserBundle:Scope', $limit);
    }

    public function getPopularPositions($limit)
    {
        return $this->getPopular('RDWJobBundle:Position', $limit);
    }

    public function getPopularCities($limit)
    {
        return $this->getPopular('RDWLocationBundle:City', $limit);
    }

    private function getViewers(Cv $cv)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('view', 'user')
            ->from('RDWCvBundle:CvView', 'view')
            ->innerJoin('view.user', 'user')
            ->where('view.cv = :cv')
            ->setParameter('cv', $cv);

        return $qb->getQuery()->getResult();
    }

    private function getPopular($repositoryName, $limit)
    {
        $joinItems = 'i.cvs';
        $status = Cv::STATUS_ACTIVE;

        return $this->getPopularByParams($repositoryName, $limit, $joinItems, $status);
    }

    /**
     * @param ProfessionalArea $professionalArea
     * @param int $limit
     *
     * @return array|Cv[]
     */
    public function getByProfessionalArea(ProfessionalArea $professionalArea, $limit = 10)
    {
        $qb = $this->createQueryBuilder('c');
        $qb
            ->join('c.professionalArea','pa')
            ->setMaxResults($limit)
            ->andWhere($qb->expr()->eq('pa.id', $professionalArea->getId()));

        return $qb->getQuery()->getResult();
    }
}
