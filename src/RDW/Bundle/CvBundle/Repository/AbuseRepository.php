<?php

namespace RDW\Bundle\CvBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class AbuseRepository
 *
 * @package RDW\Bundle\CvBundle\Repository
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class AbuseRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\Query
     */
    public function getListQuery()
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('a')
            ->from('RDWAppBundle:CvAbuse', 'a')
            ->orderBy('a.createdAt', 'DESC');

        return $queryBuilder->getQuery();
    }
}
