<?php

namespace RDW\Bundle\CvBundle\Repository;

use Doctrine\ORM\EntityRepository;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\CvBundle\Model\UsersViewFilter;
use RDW\Bundle\UserBundle\Entity\User;

/**
 * Class CvViewRepository
 *
 * @package RDW\Bundle\CvBundle\Repository
 * @author  Paulius Aleliūnas <paulius@eface.lt>
 */
class CvViewRepository extends EntityRepository
{
    /**
     * @param UsersViewFilter $filters
     *
     * @return \Doctrine\ORM\Query
     */
    public function getListQuery(UsersViewFilter $filters)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('v')
            ->from('RDWCvBundle:CvView', 'v')
            ->join('v.cv', 'c')
            ->join('v.user', 'user');

        if ($filters->getCv() instanceof Cv) {
            $queryBuilder
                ->andWhere('v.cv = :cv')
                ->setParameter('cv', $filters->getCv());
        }

        if ($filters->getUser() instanceof User) {
            $queryBuilder
                ->andWhere('c.user = :cvUser')
                ->andWhere('v.user <> :user')
                ->setParameter('cvUser', $filters->getUser())
                ->setParameter('user', $filters->getUser());
        }

        return $queryBuilder->getQuery();
    }
}
