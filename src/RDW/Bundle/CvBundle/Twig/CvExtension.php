<?php

namespace RDW\Bundle\CvBundle\Twig;

use RDW\Bundle\CvBundle\Entity\Cv;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CvExtension
 *
 * @package RDW\Bundle\CvBundle\Twig
 */
class CvExtension extends \Twig_Extension implements ContainerAwareInterface
{
    /**
     * Container
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('downloadable_cv_url', [$this, 'getDownloadableCvUrl']),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('countActiveCvs', [$this, 'getCountActiveCvs']),
            new \Twig_SimpleFunction('cv_uri', [$this, 'getCvUri']),
        ];
    }

    /**
     * @param Cv $cv
     *
     * @return string
     */
    public function getDownloadableCvUrl(Cv $cv)
    {
        $storage = 'document';

        if ($filename = $cv->getDocument()) {
            return $this->container->get('router')->generate('rdw_app.get_file', [
                'filename' => $filename,
                'storage' => $storage,
            ]);
        } else {
            $params['id'] = $cv->getId();
            $params['slug'] = $cv->getSlug();

            return sprintf('/pdf/export.php?slug=%s', urlencode($this->container->get('router')->generate('rdw_cv.cv.view', $params)));
        }
    }

    /**
     * @return int
     */
    public function getCountActiveCvs()
    {
        return $this->container->get('sphinx_search_service')->countActiveCvs([Cv::STATUS_ACTIVE]);
    }

    /**
     * @param Cv $cv
     *
     * @return string
     */
    public function getCvUri(Cv $cv)
    {
        return $this->container->get('router')->generate(
            'rdw_cv.cv.view',
            [
                'id' => $cv->getId(),
                'slug' => $cv->getSlug(),
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_cv_extension';
    }
}
