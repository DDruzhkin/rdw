<?php

namespace RDW\Bundle\CvBundle\Controller;

use RDW\Bundle\AppBundle\Event\AdAbuseEvent;
use RDW\Bundle\AppBundle\Form\Type\AbuseType;
use RDW\Bundle\AppBundle\RDWAppEvents;
use RDW\Bundle\CvBundle\Service\CvAbuseManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class CvAbuseController
 *
 * @package RDW\Bundle\CvBundle\Controller
 *
 * @Route("/cv/abuse")
 */
class CvAbuseController extends Controller
{
    /**
     * @param int $id
     *
     * @Route("/new/{id}", name="rdw_cv.abuse.new", requirements={"id" = "\d+"})
     * @Method({"GET"})
     * @Template()
     *
     * @return Response|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function abuseAction($id)
    {
        $repository = $this->getDoctrine()->getRepository('RDWCvBundle:Cv');

        $cv = $repository->find($id);

        if (! $cv) {
            throw new NotFoundHttpException(sprintf('CV with id %d was not found.', $id));
        }

        $abuse = $this->getCvAbuseManager()->create();

        $form = $this->createForm(new AbuseType(), $abuse, ['data_class' => 'RDW\Bundle\AppBundle\Entity\CvAbuse']);

        return [
            'form' => $form->createView(),
            'abuse' => $abuse,
            'cv' => $cv,
        ];
    }

    /**
     * @param Request $request
     * @param int     $id
     *
     * @Route("/save/{id}",name="rdw_cv.abuse.save", requirements={"id" = "\d+"})
     * @Method({"POST"})
     *
     * @return JsonResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function saveAbuseAction(Request $request, $id)
    {
        $data['success'] = false;
        $response = new JsonResponse();

        $user = $this->getUser();

        if (! $user) {
            $response->setData($data);

            return $response;
        }

        $repository = $this->getDoctrine()->getRepository('RDWCvBundle:Cv');

        $cv = $repository->find($id);

        if (! $cv) {
            throw new NotFoundHttpException(sprintf('CV with id %d was not found.', $id));
        }

        $abuse = $this->getCvAbuseManager()->create();

        $form = $this->createForm(new AbuseType(), $abuse, ['data_class' => 'RDW\Bundle\AppBundle\Entity\CvAbuse']);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new AdAbuseEvent($form->getData(), $cv, $user);
            $this->container->get('event_dispatcher')->dispatch(RDWAppEvents::AD_ABUSE, $event);

            $data['success'] = true;
        }

        $data['content'] = $this->renderView('RDWCvBundle:CvAbuse:_form.html.twig', [
            'form' => $form->createView(),
            'abuse' => $abuse,
            'cv' => $cv,
        ]);

        $response->setData($data);

        return $response;
    }

    /**
     * @return CvAbuseManager
     */
    private function getCvAbuseManager()
    {
        return $this->get('rdw_cv.service.cv_abuse_manager');
    }
}
