<?php

namespace RDW\Bundle\CvBundle\Controller;

use RDW\Bundle\AppBundle\Event\GenericGuestEvent;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\CvBundle\Entity\CvMessage;
use RDW\Bundle\CvBundle\Event\CvMessageEvent;
use RDW\Bundle\CvBundle\Event\MultipleCvMessageEvent;
use RDW\Bundle\CvBundle\Model\MultipleCvMessage;
use RDW\Bundle\CvBundle\RDWCvEvents;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Form\Type\FavoriteCollectionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class CvMessageController
 *
 * @package RDW\Bundle\CvBundle\Controller
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 *
 * @Route("/cv/message")
 */
class CvMessageController extends Controller
{
    /**
     * @param Cv $cv
     *
     * @Route("/create/{id}", name="rdw_cv.cv_message.create", requirements={"id" = "\d+"})
     * @Method({"GET"})
     * @Template()
     *
     * @return Response|RedirectResponse
     */
    public function createAction(Cv $cv)
    {
        $user = $this->getUser();

        /** @var CvMessage $message */
        $message = $this->container->get('rdw_cv.service.cv_message_manager')->create($cv, $user);

        $form = $this->createForm('rdw_cv_message', $message);

        return [
            'form' => $form->createView(),
            'cv' => $cv,
            'message' => $message,
            'action' => $this->generateUrl('rdw_cv.cv_message.save', ['id' => $cv->getId()]),
            'handler' => 'saveCvMessage',
            'formHandler' => 'cvMessage',
        ];
    }

    /**
     * @param Request $request
     *
     * @Route("/multiple-create", name="rdw_cv.cv_message.multiple_create")
     * @Method({"POST"})
     * @Template()
     *
     * @return Response
     */
    public function multiCreateAction(Request $request)
    {
        $data['success'] = false;
        $data['errors'] = [];
        $response = new JsonResponse();

        $user = $this->getUser();

        if (! $user) {
            $data['errors'][] = 'User not found';
            $response->setData($data);

            return $response;
        }

        $ids = $request->request->get('favorite_collection', ['id' => []])['id'];
        $favoritesForm = $this->createForm(new FavoriteCollectionType($ids, 'cv'));
        $favoritesForm->handleRequest($request);

        if ($favoritesForm->isValid()) {
            $cvList = $this->container
                ->get('rdw_user.service.favorite_manager')
                ->getCvListFromFavorites($favoritesForm->get('id')->getData());

            /** @var MultipleCvMessage $message */
            $message = $this->container
                ->get('rdw_cv.service.cv_message_manager')
                ->createMultipleCvMessage($user, $cvList);

            $form = $this->createForm('rdw_cv_multiple_message', $message);

            $data['success'] = true;
            $data['content'] = $this->renderView('RDWCvBundle:CvMessage:multiCreate.html.twig', [
                'form' => $form->createView(),
                'message' => $message,
            ]);
        } else {
            $data['errors'] = $favoritesForm->getErrors();
        }

        $response->setData($data);

        return $response;
    }

    /**
     * @param Request $request
     * @param Cv      $cv
     *
     * @Route("/save/{id}",name="rdw_cv.cv_message.save", requirements={"id" = "\d+"})
     * @Method({"POST"})
     *
     * @return JsonResponse
     */
    public function saveAction(Request $request, Cv $cv)
    {
        $data = [];
        $data['success'] = false;

        $user = $this->getUser();

        /** @var CvMessage $message */
        $message = $this->container->get('rdw_cv.service.cv_message_manager')->create($cv, $user);

        $form = $this->createForm('rdw_cv_message', $message);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new GenericGuestEvent($request, $message);
            $this->container->get('event_dispatcher')->dispatch('rdw_app.cv_message.pre_create', $event);

            if ($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
                $event = new CvMessageEvent($form->getData(), $user);
                $this->container->get('event_dispatcher')->dispatch(RDWCvEvents::CV_MESSAGE_SAVE, $event);

                $data['url'] = $this->generateUrl('rdw_cv.cv.view', ['id' => $cv->getId(), 'slug' => $cv->getSlug()]);
            }

            $data['success'] = true;
        } else {
            $data['content'] = $this->renderView('RDWCvBundle:CvMessage:_form.html.twig', [
                'form' => $form->createView(),
                'cv' => $cv,
                'message' => $message,
                'action' => $this->generateUrl('rdw_cv.cv_message.save', ['id' => $cv->getId()]),
                'handler' => 'saveCvMessage',
                'formHandler' => 'cvMessage',
            ]);
        }

        $response = new JsonResponse();
        $response->setData($data);

        return $response;
    }

    /**
     * @param Request $request
     *
     * @Route("/multiple-save",name="rdw_cv.cv_message.multiple_save")
     * @Method({"POST"})
     *
     * @return JsonResponse
     */
    public function multipleSaveAction(Request $request)
    {
        $data['success'] = false;
        $data['errors'] = [];
        $response = new JsonResponse();

        $sender = $this->getUser();

        if (! $sender) {
            $data['errors'][] = 'Sender not found';
            $response->setData($data);

            return $response;
        }

        /** @var MultipleCvMessage $message */
        $message = $this->container->get('rdw_cv.service.cv_message_manager')->createMultipleCvMessage($sender);

        $form = $this->createForm('rdw_cv_multiple_message', $message);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $message = $form->getData();
            $cvs = $message->getCvs();

            $event = new MultipleCvMessageEvent($cvs, $message, $sender);
            $this->container->get('event_dispatcher')->dispatch(RDWCvEvents::MULTIPLE_CV_MESSAGE_SAVE, $event);

            $data['success'] = true;
        }

        $data['content'] = $this->renderView('RDWCvBundle:CvMessage:_form.html.twig', [
            'form' => $form->createView(),
            'message' => $message,
            'action' => $this->generateUrl('rdw_cv.cv_message.multiple_save'),
            'handler' => 'saveCvMessage',
            'formHandler' => 'sendMultipleCvMessage',
        ]);

        $response->setData($data);

        return $response;
    }

    /**
     * @return string
     */
    private function getRegistrationPopup()
    {
        return $this->renderView('RDWCvBundle:CvMessage:registration_popup.html.twig', [
            'days' => $this->container->getParameter('rdw_user.save_cookie_days_count'),
            'url' => $this->generateUrl('rdw_user.registration.register', [
                'type' => RegisteredUser::USER_TYPE_EMPLOYER,
            ]),
        ]);
    }
}
