<?php

namespace RDW\Bundle\CvBundle\Controller;

use RDW\Bundle\AppBundle\Entity\CvAbuse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CvAbuseAdminController
 *
 * @package RDW\Bundle\CvBundle\Controller
 *
 * @Route("/manage/cv/abuse")
 */
class CvAbuseAdminController extends Controller
{
    /**
     * @param Request $request
     *
     * @Route("/list", name="rdw_cv.abuse_admin.list")
     * @Template()
     * @Method({"GET"})
     *
     * @return array
     */
    public function listAction(Request $request)
    {
        $abuses = $this->container->get('rdw_cv.service.cv_abuse_manager')->getPaginatedList(
            $request->query->get('page', 1),
            $this->container->getParameter('rdw_job.job_abuse_admin.paginator.per_page')
        );

        $user = $this->getUser();
        $items = [];

        /** @var CvAbuse $item */
        foreach ($abuses as $item) {
            $item->getAd()->setViewer($user);
            $items[] = $item;
        }

        $abuses->setItems($items);

        return [
            'pagination' => $abuses,
        ];
    }
}
