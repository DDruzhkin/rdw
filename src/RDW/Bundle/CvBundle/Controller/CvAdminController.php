<?php

namespace RDW\Bundle\CvBundle\Controller;

use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\CvBundle\Event\BlockEvent;
use RDW\Bundle\CvBundle\Event\CvApproveEvent;
use RDW\Bundle\CvBundle\RDWCvEvents;
use RDW\Bundle\CvBundle\Model\CvFilter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use RDW\Bundle\UserBundle\Event\SingleUserEvent;
use RDW\Bundle\UserBundle\RDWUserEvents;

/**
 * Class CvAdminController
 *
 * @package RDW\Bundle\CvBundle\Controller
 *
 * @Route("/manage/cv")
 */
class CvAdminController extends Controller
{
    /**
     * @Route("/list", name="rdw_cv.cv_admin.list")
     * @Template()
     * @Method({"GET"})
     *
     * @return array
     */
    public function listAction()
    {
        $this->isGrantedAsCensor();

        $pagination = $this->get('rdw_cv.service.cv_manager')->getPaginatedList(
            new CvFilter(),
            $this->get('request')->query->get('page', 1),
            $this->container->getParameter('rdw_cv.cv_admin.paginator.per_page')
        );

        $user = $this->getUser();
        $items = [];

        /** @var Cv $item */
        foreach ($pagination as $item) {
            $item->setViewer($user);
            $items[] = $item;
        }

        $pagination->setItems($items);

        return [
            'pagination' => $pagination,
        ];
    }

    /**
     * @param Request $request
     * @param Cv      $cv
     *
     * @Route("/edit/{id}", name="rdw_cv.cv_admin.edit", requirements={"id" = "\d+"})
     * @Method({"GET", "POST"})
     * @Template()
     *
     * @return array|Response
     * @throws NotFoundHttpException
     */
    public function editAction(Request $request, Cv $cv)
    {
        $this->isGrantedAsCensor();

        $cv->setViewer($this->getUser());
        $cv->setOldStatus($cv->getStatus());
        $form = $this->createForm('rdw_cv_admin', $cv);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new CvApproveEvent($form->getData());
            $this->getEventDispatcher()->dispatch(RDWCvEvents::CV_APPROVE, $event);

            $userEvent = new SingleUserEvent($form->getData()->getUser());
            $this->getEventDispatcher()->dispatch(RDWUserEvents::UPDATE_USER_PROFILE_PERCENT, $userEvent);

            if ($event->getResponse() instanceof Response) {
                return $event->getResponse();
            }

            return new RedirectResponse($this->generateUrl('rdw_cv.cv_admin.edit', ['id' => $cv->getId()]));
        }

        return [
            'form' => $form->createView(),
            'cv' => $cv,
        ];
    }

    /**
     * @param Request $request
     * @param Cv      $cv
     *
     * @Route("/block/{id}", name="rdw_cv.cv_admin.block", requirements={"id" = "\d+"})
     * @Method({"GET", "POST"})
     * @Template()
     *
     * @return array|RedirectResponse
     * @throws NotFoundHttpException
     */
    public function blockAction(Request $request, Cv $cv)
    {
        $this->isGrantedAsCensor();

        $form = $this->createForm('rdw_cv_block', $cv);
        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var SubmitButton $cancelButton */
            $cancelButton = $form->get('cancel');

            if ($cancelButton->isClicked()) {
                return new RedirectResponse($this->generateUrl('rdw_cv.cv_admin.edit', ['id' => $cv->getId()]));
            } else {
                $dispatcher = $this->getEventDispatcher();

                $event = new BlockEvent($form->getData(), $this->getUser());
                $dispatcher->dispatch(RDWCvEvents::CV_BLOCK, $event);

                return new RedirectResponse($this->generateUrl('rdw_cv.abuse_admin.list'));
            }
        }

        return [
            'form' => $form->createView(),
            'cv' => $cv,
        ];
    }

    /**
     * @param int $id
     *
     * @Route("/delete/{id}", requirements={"id" = "\d+"}, name="rdw_cv.cv_admin.delete")
     * @Method({"GET"})
     *
     * @return RedirectResponse
     * @throws NotFoundHttpException
     */
    public function deleteAction($id)
    {
        $this->isGrantedLocal();

        $cv = $this->get('rdw_cv.service.cv_manager')->findCvById($id);

        if (! $cv) {
            throw new NotFoundHttpException(sprintf('The cv with id "%d" does not exist.', $id));
        }

        $this->get('rdw_cv.service.cv_manager')->delete($cv);

        return new RedirectResponse($this->generateUrl('rdw_cv.cv_admin.list'));
    }

    /**
     * @return \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    private function getEventDispatcher()
    {
        return $this->container->get('event_dispatcher');
    }

    /**
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    private function isGrantedLocal()
    {
        if (! $this->container->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }
    }

    /**
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    private function isGrantedAsCensor()
    {
        if (! $this->container->get('security.context')->isGranted('ROLE_CENSOR')) {
            throw new AccessDeniedException();
        }
    }
}
