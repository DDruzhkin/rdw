<?php

namespace RDW\Bundle\CvBundle\Controller;

use RDW\Bundle\AppBundle\Event\GenericGuestEvent;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\CvBundle\Event\CvEvent;
use RDW\Bundle\CvBundle\Event\UserEvent;
use RDW\Bundle\CvBundle\Form\Type\CvCollectionType;
use RDW\Bundle\CvBundle\Form\Type\CvFormType;
use RDW\Bundle\CvBundle\Form\Type\CvUsersViewFilterType;
use RDW\Bundle\CvBundle\Model\CvFilter;
use RDW\Bundle\CvBundle\Model\UsersViewFilter;
use RDW\Bundle\HistoryBundle\Entity\History;
use RDW\Bundle\HistoryBundle\Event\HistoryEvent;
use RDW\Bundle\HistoryBundle\RDWHistoryEvents;
use RDW\Bundle\CvBundle\RDWCvEvents;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Entity\Position;
use RDW\Bundle\JobBundle\Entity\ProfessionalArea;
use RDW\Bundle\ServiceBundle\Entity\Service;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use RDW\Bundle\ContentBundle\Entity\Text;
use RDW\Bundle\ContentBundle\Service\TextManager;
use Symfony\Component\Form\SubmitButton;
use RDW\Bundle\LocationBundle\Entity\City;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class CvController
 *
 * @package RDW\Bundle\CvBundle\Controller
 */
class CvController extends Controller
{
    /**
     * @Route("/cvs", name="rdw_cv.cv.index")
     * @Template()
     *
     * @return array
     */
    public function indexAction()
    {
        $text = $this->getTextManager()->getActiveTextBy(['type' => Text::TYPE_CV_BLOCK]);

        return [
            'text' => $text,
        ];
    }
    /**
     *
     * @return Response
     * @throws NotFoundHttpException
     * @throws \InvalidArgumentException
     */
    public function sidebarAction()
    {

        $form = $this->createForm("cvs");
        $master_request = Request::createFromGlobals();

        $form->handleRequest($master_request);
        $formdata = $form->all();
        $fields = [];
        foreach ($formdata as $field) {
            $fields[] = $field->getName();
        }
        $excluded_params = [
            'query',
            //'professionalArea',
            //'positions',
            //'salaryFrom',
            'salaryTo',
            //'driverLicenseCategory',
            //'updatedAt',
            //'cities',
            //'scopes',
            'workingPlaceCity',
            //'gender',
            //'educationType',
            //'workExperienceType',
            //'businessTrip',
            //'workScheduleType',
            'ageFrom',
            'ageTo',
            //'language',
            //'hasCar',
            //'employmentType'
        ];
        $query_all = $master_request->query->all();
        if (isset($query_all['cvs']))
            $params = array_keys(array_filter($query_all['cvs']));
        $params = !empty($params) ? array_merge($params, $excluded_params) : $excluded_params;
        $fields=array_diff($fields, $params);
        $sphinx = $this->container->get('sphinx_search_service');
        echo "<pre>";
        //var_dump($sphinx->getCvSidebar($form, $fields));
        echo "</pre>";

        $data = [
            'form' => $form->createView(),
            'data' => $sphinx->getCvSidebar($form, $fields)
        ];

        return $this->render('RDWCvBundle:Cv:_sidebar_filter.html.twig', $data);
    }

    /**
     * @param Request $request
     *
     * @Route("/resumes", name="rdw_cv.cv.list")
     * @Template()
     *
     * @return array
     */
    public function listAction(Request $request)
    {
        $filter = new CvFilter();
        $filter->setUser($this->getUser());

        $filterForm = $this->createForm('cvs', $filter);
        $filterForm->handleRequest($request);

        $request->getSession()->set('_subscription_cv_filter', $filter);

        $limit = $this->getPerPageNumber();
        $page = $request->query->get('page', 1);
        $offset = abs($page - 1) * $limit;
        $sphinx = $this->container->get('sphinx_search_service');
        $orderBy = $filter->getOrderByFromRequestQuery($request->query->all());
        $sphinx->filterCvs($filterForm, $orderBy, $limit, $offset);

        $items = [];
        if ($sphinx->getTotalFound()) {
            $items = $this->container
                ->get('doctrine')
                ->getRepository('RDWCvBundle:Cv')
                ->getByIds($sphinx->getResults());
        }

        $pagination = $this->get('knp_paginator')->paginate($items, 1, $limit);
        $pagination->setTotalItemCount($sphinx->getTotalFound());
        $pagination->setCurrentPageNumber($page);

        return [
            'pagination' => $pagination,
            'form' => $filterForm
        ];
    }

    /**
     * @Route("/cvs/advanced", name="rdw_cv.cv.advanced")
     * @param Request $request
     *
     * @return Response
     * @throws NotFoundHttpException
     * @throws \InvalidArgumentException
     */
    public function advancedAction(Request $request) {

        $form = $this->createForm("cvs");
        $form->handleRequest($request);
        if($request->query->has('count')){
            $sphinx = $this->container->get('sphinx_search_service');
            return new Response($sphinx->getCvCount($form));
        }
        $data = [
            'form' => $form->createView(),
        ];

        return $this->render('RDWCvBundle:Cv:_filter.html.twig', $data);
    }

    /**
     * @Route("/cv/my-list", name="rdw_cv.cv.my_list")
     * @Method({"GET"})
     * @Template()
     *
     * @return array
     */

    public function myListAction()
    {
        $filter = new CvFilter();
        $filter->setStatuses(array_keys(Cv::getStatusesChoiceListForUser()));
        $filter->setUser($this->getUser());

        $pagination = $this->getCvManager()->getPaginatedMyList(
            $filter,
            $this->get('request')->query->get('page', 1),
            $this->container->getParameter('rdw_cv.cv.paginator.per_page')
        );

        $user = $this->getUser();
        $items = [];

        /** @var Cv $item */
        foreach ($pagination as $item) {
            $item->setViewer($user);
            $items[] = $item;
        }

        $pagination->setItems($items);

        $form = $this->createForm(new CvCollectionType($pagination->getItems()));
        $services = $this->get('rdw_service.service.service_manager')->getServicesAllowedToOrderFromList(Service::FOR_EMPLOYEE);

        return [
            'cvs' => $pagination,
            'services' => $services,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/cv/new", name="rdw_cv.cv.new")
     * @Method({"GET"})
     * @Security("has_role('ROLE_EMPLOYEE') or is_anonymous()")
     * @Template()
     *
     * @return array
     */
    public function newAction()
    {
        $cv = $this->getCvManager()->create($this->getUser());

        $form = $this->createForm('rdw_cv', $cv);

        return [
            'form' => $form->createView(),
            'cv' => $cv,
        ];
    }

    /**
     * @param RegisteredUser $user
     * @param Request $request
     *
     * @Route("/cv/users-who-viewed/{id}", requirements={"id" = "\d+"}, name="rdw_cv.cv.users_who_viewed_list")
     * @Template()
     * @Method({"GET"})
     *
     * @throws AccessDeniedException
     *
     * @return array|RedirectResponse
     */
    public function usersWhoViewedAction(RegisteredUser $user, Request $request)
    {
        $cvId = $request->query->get('cv', 0);
        $cv = $this->container->get('rdw_cv.service.cv_manager')->getById($cvId);

        if ($cv && !$cv->isOwner($user)) {
            throw new AccessDeniedException();
        }

        $viewFilter = new UsersViewFilter();
        $viewFilter->setUser($user);

        $filterForm = $this->createForm(new CvUsersViewFilterType($user), $viewFilter);
        $filterForm->handleRequest($request);

        $pagination = $this->getCvViewManager()->getPaginatedList(
            $viewFilter,
            $this->get('request')->query->get('page', 1),
            $this->getPerPageNumber()
        );

        return [
            'form' => $filterForm->createView(),
            'viewers' => $pagination,
            'cv' => $cv,
            'user' => $this->getUser(),
        ];
    }

    /**
     * @Template()
     *
     * @return array
     */
    public function lastViewedBlockAction()
    {
        $limit = $this->container->getParameter('rdw_cv.last_viewed_cvs_block_limit');
        $cvs = $this->container
            ->get('doctrine')
            ->getRepository('RDWCvBundle:Cv')
            ->getHavingViews($this->getUser(), $limit);

        return [
            'cvs' => $cvs,
        ];
    }

    /**
     * @param Request $request
     *
     * @Route("/cv/create", name="rdw_cv.cv.create")
     * @Route("/cv/guest-create", name="rdw_cv.cv.guest_create")
     * @Method({"GET", "POST"})
     * @Template("RDWCvBundle:Cv:new.html.twig")
     *
     * @return array
     */
    public function createAction(Request $request)
    {
        $cv = $this->container->get('session')->get('ad');
        if ($cv) {
            $cv = $this->get('rdw_cv.service.cv_manager')->map($this->container->get('session'));
        } else {
            $cv = $this->getCvManager()->create($this->getUser());
        }

        $form = $this->createForm('rdw_cv', $cv);
        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
                $this->handleSubmit($form, $cv, History::ACTION_CREATE, RDWHistoryEvents::CREATE_OBJECT);

                return $this->redirect($this->generateUrl('rdw_cv.cv.my_list'));
            } else {
                $event = new GenericGuestEvent($request, $cv);
                $this->container->get('event_dispatcher')->dispatch('rdw_app.ad.pre_create', $event);

                return $this->redirect($this->generateUrl('rdw_cv.cv.guest_create'));
            }
        }

        return [
            'form' => $form->createView(),
            'cv' => $cv,
            'showRegistrationPopup' => ('rdw_cv.cv.guest_create' == $request->get('_route')),
        ];
    }

    /**
     * @param int $id
     *
     * @Route("/cv/edit/{id}", requirements={"id" = "\d+"}, name="rdw_cv.cv.edit")
     * @Method({"GET"})
     * @Template()
     *
     * @return array
     * @throws NotFoundHttpException
     */
    public function editAction($id)
    {
        $user = $this->getUser();
        $cv = $this->getCvManager()->findCvByIdAndUser($id, $user);

        if (!$cv) {
            throw new NotFoundHttpException(sprintf('The cv with id "%d" does not exist.', $id));
        }

        $cv->setViewer($user);
        $form = $this->createForm(CvFormType::class, $cv);

        return [
            'form' => $form->createView(),
            'cv' => $cv,
        ];
    }

    /**
     * @param Request $request
     * @param int $id
     *
     * @Route("/cv/update/{id}", requirements={"id" = "\d+"}, name="rdw_cv.cv.update")
     * @Method({"POST"})
     * @Template("RDWCvBundle:Cv:edit.html.twig")
     *
     * @return array|RedirectResponse
     * @throws NotFoundHttpException
     */
    public function updateAction(Request $request, $id)
    {
        $cv = $this->getCvManager()->findCvByIdAndUser($id, $this->getUser());

        if (!$cv) {
            throw new NotFoundHttpException(sprintf('The cv with id "%d" does not exist.', $id));
        }

        $form = $this->createForm('rdw_cv', $cv);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->handleSubmit($form, $cv, History::ACTION_EDIT, RDWHistoryEvents::EDIT_OBJECT);

            return $this->redirect($this->generateUrl('rdw_cv.cv.my_list'));
        }

        return [
            'form' => $form->createView(),
            'cv' => $cv,
        ];
    }

    /**
     * @param Cv $cv
     *
     * @Route("/cv/delete/{id}", requirements={"id" = "\d+"}, name="rdw_cv.cv.delete")
     * @Method({"GET"})
     *
     * @return RedirectResponse
     * @throws NotFoundHttpException
     */
    public function deleteAction(Cv $cv)
    {
        if ($cv->getUser()->getId() == $this->getUser()->getId()) {
            $this->getCvManager()->delete($cv);

            $this->get('rdw_history.service.history_manager')->logEvent($this->getUser(), History::ACTION_DELETE, $cv, RDWHistoryEvents::DELETE_OBJECT);
        }

        return $this->redirect($this->generateUrl('rdw_cv.cv.my_list'));
    }

    /**
     * View cv
     *
     * @param integer $id
     *
     * @Route("/resumes/{slug}/{id}", name="rdw_cv.cv.view", requirements={"id" = "\d+"})
     * @Method({"GET"})
     *
     * Cache(smaxage="300")
     *
     * @throws NotFoundHttpException
     * @return array
     */
    public function viewAction($id)
    {
        $cv = $this->getCvManager()->getByIdIncludingDeleted($id);

        if (!$cv) {
            throw new NotFoundHttpException(sprintf('The cv with id "%d" does not exist.', $id));
        }

        $user = $this->getUser();
        $template = 'view';
        $returnData = [
            'cv' => $cv,
        ];

        $returnData['relatedCvs'] = $this->getCvManager()->findRelated(
            $cv,
            $this->container->getParameter('rdw_cv.cv_view.related.per_page'),
            $user
        );

        $event = new UserEvent($cv, $user);
        $this->getEventDispatcher()->dispatch(RDWCvEvents::CV_VIEW, $event);

        if ($user instanceof RegisteredUser) {
            if ($user->getCvs()->contains($cv)) {
                $form = $this->createForm('rdw_cv', $cv, ['disabled' => true]);
                $template = 'my_view';
                $returnData['services'] = $this->get('rdw_service.service.service_manager')->getServicesAllowedToOrderFromList(Service::FOR_EMPLOYEE);
                $returnData['form'] = $form->createView();
            }
        }

        return $this->render('RDWCvBundle:Cv:' . $template . '.html.twig', $returnData);
    }

    /**
     * @Cache(smaxage="1800")
     *
     * @return Response
     */
    public function carouselAction()
    {
        $limit = $this->container->getParameter('rdw_cv.cv.carousel_limit');
        $orderItems = $this->getDoctrine()->getRepository('RDWOrderBundle:OrderItemCv')->findAllForCarousel($limit);

        return $this->render('RDWCvBundle:Cv:carousel.html.twig', ['orderItems' => $orderItems]);
    }

    /**
     * @param Request $request
     * @param RegisteredUser $user
     *
     * @Route("/cv/multiple-delete/{id}", name="rdw_cv.cv.multiple_delete", requirements={"id" = "\d+"})
     *
     * @return RedirectResponse
     * @throws AccessDeniedException
     */
    public function multipleDeleteAction(Request $request, RegisteredUser $user)
    {
        $ids = $request->request->get('cv_collection', ['id' => []])['id'];
        $form = $this->createForm(new CvCollectionType($ids));
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getCvManager()->multipleDelete($form->get('id')->getData(), $user);
            $this->container->get('braincrafted_bootstrap.flash')->success('Successfully deleted');
        }

        return $this->redirect($this->generateUrl('rdw_cv.cv.my_list'));
    }

    /**
     * @param Cv $cv
     * @param string $newStatus
     *
     * @Route("/cv/change-status/{id}/{newStatus}", name="rdw_cv_change_status")
     * @Method({"GET"})
     *
     * @return RedirectResponse
     */
    public function changeStatus(Cv $cv, $newStatus)
    {
        $user = $this->getUser();

        if ($user->getId() != $cv->getUser()->getId()) {
            throw new AccessDeniedException();
        }

        if ($cv->isStatusChangeableForUser() && in_array($newStatus, array_keys(Cv::getStatusesChoiceListForUser()))) {
            $cv->setStatus($newStatus);
            $this->getCvManager()->update($cv);

            if (CV::STATUS_ACTIVE == $cv->getStatus()) {
                $this->get('braincrafted_bootstrap.flash')->success(
                    $this->get('translator')->trans('Cv successfully published')
                );
            } else {
                $this->get('braincrafted_bootstrap.flash')->success(
                    $this->get('translator')->trans('Cv status inactive')
                );
            }
        }

        return $this->redirect($this->generateUrl('rdw_cv.cv.my_list'));
    }

    /**
     * @Template("RDWCvBundle:Cv:list.html.twig")
     * @ParamConverter("city", class="RDWLocationBundle:City", options={"mapping": {"city": "titleInLatin"}})
     */
    public function byCityAction(City $city, Request $request)
    {
        $parameters = $request->query->has('cvs') ? $request->query->get('cvs') : [];
        if (isset($parameters['workingPlaceCity']) && ($city->getId() != $parameters['workingPlaceCity'])) {
            /** @var City $rCity */
            $rCity = $this->getDoctrine()
                ->getManagerForClass(City::class)->find(City::class, $parameters['workingPlaceCity']);
            if ($rCity && $rCity->getTitleInLatin()) {
                return new RedirectResponse(
                    $this->generateUrl(
                        'rdw_subdomain_cvs',
                        array_merge(
                            ['city' => $rCity->getTitleInLatin()],
                            $request->query->all()
                        )
                    )
                );
            }
        }

        $parameters['workingPlaceCity'] = $city->getId();
        $request->query->set('cvs', $parameters);

        return $this->listAction($request);
    }

    /**
     * @Template("RDWCvBundle:Cv:list.html.twig")
     * @Route("/resumes/{slug}", name="rdw_cv.cv.by_filter_value")
     * @ParamConverter("slug", class="RDWSlugBundle:Slug", options={"mapping": {"slug": "name"}})
     */
    public function byFilterValueAction(\RDW\Bundle\SlugBundle\Entity\Slug $slug, Request $request)
    {
        $parameters = $request->query->has('cvs') ? $request->query->get('cvs') : [];
        switch ($slug->getType()) {
            case 'RDW\Bundle\UserBundle\Entity\Scope':
                $key = 'scopes';
                $parameters[$key] = isset($parameters[$key]) ? $parameters[$key] . ',' : '';
                $parameters[$key] .= $slug->getItemId();
                break;
            case 'RDW\Bundle\JobBundle\Entity\EducationType':
                $key = 'educationType';
                $parameters[$key] = $slug->getItemId();
                break;
            case 'RDW\Bundle\JobBundle\Entity\WorkExperienceType':
                $key = 'workExperienceType';
                $parameters[$key] = $slug->getItemId();
                break;
            case 'RDW\Bundle\JobBundle\Entity\WorkScheduleType':
                $key = 'workScheduleType';
                $parameters[$key] = $slug->getItemId();
                break;
            case 'RDW\Bundle\JobBundle\Entity\EmploymentType':
                $key = 'employmentType';
                $parameters[$key] = $slug->getItemId();
                break;
            case 'RDW\Bundle\JobBundle\Entity\Position':
                $key = 'positions';
                $parameters[$key] = isset($parameters[$key]) ? $parameters[$key] . ',' : '';
                $parameters[$key] .= $slug->getItemId();
                break;
            default:
                throw $this->createNotFoundException();
        }

        $request->query->set('cvs', $parameters);

        return $this->listAction($request);
    }

    /**
     * @return \RDW\Bundle\CvBundle\Service\CvManager
     */
    private function getCvManager()
    {
        return $this->container->get('rdw_cv.service.cv_manager');
    }

    /**
     * Extract per page param from query string
     *
     * @return int
     */
    private function getPerPageNumber()
    {
        $current = $this->get('request')->query->get('per_page');
        $options = $this->container->getParameter('rdw_cv.list.per_page.options');

        $perPage = ($current && in_array($current, $options)) ? $current : $options[0];

        return $perPage;
    }

    /**
     * @return \Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher
     */
    private function getEventDispatcher()
    {
        return $this->container->get('event_dispatcher');
    }

    /**
     * @return TextManager
     */
    private function getTextManager()
    {
        return $this->get('rdw_content.service.text_manager');
    }

    /**
     * @return \RDW\Bundle\CvBundle\Service\CvViewManager
     */
    private function getCvViewManager()
    {
        return $this->get('rdw_cv.service.cv_view_manager');
    }

    /**
     * @param Form $form
     * @param Cv $cv
     * @param string $historyEventAction
     * @param string $historyEventType
     */
    private function handleSubmit(Form $form, Cv $cv, $historyEventAction, $historyEventType)
    {
        $dispatcher = $this->get('event_dispatcher');

        /** @var SubmitButton $submitAndPublish */
        $submitAndPublish = $form->get('submitAndPublish');
        $event = new CvEvent($cv);

        if ($submitAndPublish->isClicked()) {
            $dispatcher->dispatch(RDWCvEvents::CV_PUBLISH, $event);
            $this->get('braincrafted_bootstrap.flash')->success(
                $this->get('translator')->trans('Cv successfully published')
            );
        } else {
            $dispatcher->dispatch(RDWCvEvents::CV_SAVE, $event);
            $this->get('braincrafted_bootstrap.flash')->success(
                $this->get('translator')->trans('Cv successfully saved')
            );
        }

        $this->get('rdw_history.service.history_manager')->logEvent($this->getUser(), $historyEventAction, $cv, $historyEventType);
    }
}
