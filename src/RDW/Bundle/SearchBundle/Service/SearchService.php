<?php

namespace RDW\Bundle\SearchBundle\Service;

use RDW\Bundle\ManageBundle\Entity\Filter;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class SearchService
 *
 * @package RDW\Bundle\CvBundle\Service
 */
class SearchService implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    const FILTER_TYPE_CITY              = 'city';
    const FILTER_TYPE_SCOPE             = 'scope';
    const FILTER_TYPE_LANGUAGE          = 'language';
    const FILTER_TYPE_EDUCATION         = 'education';
    const FILTER_TYPE_POSITION          = 'position';
    const FILTER_TYPE_PROFESSIONAL_AREA = 'professional_area';

    /**
     * @var array
     */
    private $indexes = [];

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var array
     */
    private $repositories = [
        self::FILTER_TYPE_CITY               => 'RDWLocationBundle:City',
        self::FILTER_TYPE_SCOPE              => 'RDWUserBundle:Scope',
        self::FILTER_TYPE_LANGUAGE           => 'RDWJobBundle:Language',
        self::FILTER_TYPE_EDUCATION          => 'RDWJobBundle:EducationType',
        self::FILTER_TYPE_POSITION           => 'RDWJobBundle:Position',
        self::FILTER_TYPE_PROFESSIONAL_AREA  => 'RDWJobBundle:ProfessionalArea',
    ];

    /**
     * @param string $index
     */
    public function addIndex($index)
    {
        $this->indexes[] = $index;
    }

    /**
     * Init translator
     */
    public function initTranslator()
    {
        $this->translator = $this->container->get('translator');
    }

    /**
     * @param Filter $item
     * @param string $filter
     *
     * @return array
     */
    public function formatResult($item, $filter)
    {
        $filterTitle = ('companyTitle' != $filter) ? ucfirst($filter) : 'Company';
        $title = method_exists($item, 'getParent') && $item->getParent() != null ? $item->getParent()->getTitle() . " - " . $item->getTitle() : $item->getTitle();

        return [
            'value' => $item->getId(),
            'title' => $title,
            'filter' => $filter,
            /** @Ignore */
            'filter_title' => $this->translator->trans($filterTitle),
        ];
    }

    /**
     * @param string $queryValues
     *
     * @return JsonResponse
     */
    public function searchInitValues($queryValues)
    {
        $result = [];

        foreach ($this->getFilters($queryValues) as $filter => $filterValues) {
            $items = $this->getResults($this->repositories[$filter], $filter, $filterValues);
            $result = array_merge($result, $items);
        }

        return new JsonResponse($result);
    }

    /**
     * @param $queryValues
     *
     * @return array
     */
    private function getFilters($queryValues)
    {
        $filters = [];
        $values = explode(',', $queryValues);

        foreach ($values as $value) {
            if (empty($value)) {
                continue;
            }

            $fieldItem = explode(':', $value);

            if (sizeof($fieldItem) < 2) {
                continue;
            }

            $filter = $fieldItem[0];
            $id = $fieldItem[1];

            if (! array_key_exists($filter, $this->repositories)) {
                continue;
            }

            if (! array_key_exists($filter, $filters)) {
                $filters[$filter] = [];
            }

            $filters[$filter][] = $id;
        }

        return $filters;
    }

    /**
     * @param string $repository
     * @param array  $filterValues
     *
     * @return array
     */
    private function getResults($repository, $filter, $filterValues)
    {
        $result = [];

        $queryBuilder = $this->container->get('doctrine.orm.entity_manager')->createQueryBuilder();
        $queryBuilder
            ->select('i')
            ->from($repository, 'i')
            ->where('i.id IN (:ids)')
            ->setParameter('ids', $filterValues);

        $items = $queryBuilder->getQuery()->getResult();

        foreach ($items as $item) {
            $result[] = $this->formatResult($item, $filter);
        }

        return $result;
    }
}
