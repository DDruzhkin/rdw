function formatSelection(object, container) {
	//RDWBY-31
	// $('input[data-filter='+object.filter+']').parent().find('a span.select2-chosen').text(object.title);
	$('input[data-filter='+object.filter+']').parent().find('a span.select2-chosen').text(object.filter_title);
	return '<span data-filter="' + object.filter + '" data-title="' + object.title + '" data-value="' + object.value + '">' + object.filter_title + ': ' + object.title + '</span>';
}

function formatResult(item) {
    return item.title;
}

function formatId(object) {
    return object.filter + ':' + object.value;
}

function selectElement(event) {
    var element = event.added;
    var $formElement = $('form .filters [data-filter="' + element.filter + '"]'),
        value = element.value.toString();

    if ('query' == element.filter) {
        $formElement.val(value);
        singleElementSet(event.val, element);
    } else if ('hidden' == $formElement.attr('type')) {
        if ($formElement.data('multiple')) {
            var values = ($formElement.val() != "") ? $formElement.val().split(',') : [];
            values.push(value);
            $formElement.val(values.join(','));
        } else {
            var values = [];
            values.push(value);
            $formElement.val(values);
        }
        //console.log('debug (' + element.filter + '):', $formElement.val());
    } else if ($formElement.data('multiple')) {
        var values = $formElement.val() || [];
        values.push(value);
        $formElement.val(values);
    } else {
        $formElement.val(value);
        singleElementSet(event.val, element);
    }
}

function removeElement(event) {
    var element = event.removed;
    var $formElement = $('form .filters [data-filter="' + element.filter + '"]'),
        value = element.value.toString();
	var values = [];

    if ('hidden' == $formElement.attr('type')) {
        values = ($formElement.val() != "") ? $formElement.val().split(',') : [];
        values.splice(values.indexOf(value), 1);
        $formElement.val(values.join(','));
        //console.log('debug (' + element.filter + '):', $formElement.val());
    } else if ($formElement.data('multiple')) {
        values = $formElement.val() || [];
        values.splice(values.indexOf(value), 1);
        $formElement.val(values);
    } else if ($formElement.is('div')) {
        // radio iCheck handling
        $formElement.find(':radio[value=""]').iCheck('check');
    } else {
        // if exists select2
        if ($('#s2id_' + $formElement.attr('id')).length) {
            $formElement.select2('val', '');
        } else {
            // selecter plugin trigger change
            $formElement.val('').trigger('change');
        }

        singleElementUnset(event.val, element);
    }
}

function singleElementUnset(select2Values, object) {
    var leaveValues = [];

    $.each(select2Values, function(index, value){
        if (value.split(':')[0] != object.filter) {
            leaveValues.push(formatId(object));
        }
    });

    $("#tags").select2('val', leaveValues);
}

function singleElementSet(select2Values, object) {
    var leaveValues = [],
        newValue = formatId(object);

    $.each(select2Values, function(index, value){
        if (value.split(':')[0] != object.filter) {
            leaveValues.push(value);
        }
    });

    leaveValues.push(newValue);

    $("#tags").select2('val', leaveValues);
}

function handleFilter(element) {
    var $searchTagSelect = $('#tags'),
        $element = $(element),
        select2Values = $searchTagSelect.select2('val'),
        values = [];

    // create values array of selected element
    if ($element.val()) {
        if ($element.data('multiple')) {
            if ('hidden' == $element.attr('type')) {
                $.each($element.val().split(','), function (index, value) {
                    values.push($element.data('filter') + ':' + value);
                });
            } else {
                $.each($element.val(), function (index, value) {
                    values.push($element.data('filter') + ':' + value);
                });
            }
        } else {
            var filter = '';

            // if element is styled iCheck radio
            if ($element.is(':radio')) {
                filter = $element.parents('[data-handler="filterTag"]').data('filter');
            } else {
                filter = $element.data('filter');
            }

            values.push(filter + ':' + $element.val());
        }

        // add select2 values except selected element
        $.each(select2Values, function(index, value){
            if ((value.split(':').length > 1)) {
                if (value.split(':')[0] != $element.data('filter')) {
                    values.push(value);
                }
            }
        });
    }

    $searchTagSelect.select2('val', values);
}

$(document).ready(function() {
    $("#tags")
        .select2({
            tags: [],
            multiple: true,
            minimumInputLength: 2,
            openOnEnter: false,
            formatSelection: formatSelection,
            formatResult: formatResult,
	        formatResultCssClass: function (){ return 'XXX964'},
            id: formatId,
            createSearchChoice: function (term, data) {
                return {value: term, filter: 'query', filter_title: $('#jobs_position').data('placeholder'), title: term, status: 'new'};
            },
            ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                dataType: 'json',
                quietMillis: 500,
                data: function (term, page) {
                    return {
                        q: term // search term
                    };
                },
                results: function (data, page) { // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to alter the remote JSON data
                    return { results: data };
                },
                cache: true
            },
            initSelection: function(element, callback) {
                // the input tag has a value attribute preloaded that points to a preselected repository's id
                // this function resolves that id attribute to an object that select2 can render
                // using its formatResult renderer - that way the repository name is shown preselected

                var data = [],
                    hiddenInputValues = [];

                $('[data-handler="filterTag"]').each(function(index, element) {
                    var $element = $(element);

                    // create values array of selected element
                    if ($element.val()) {
                        // if element is hidden input
                        if ('hidden' == $element.attr('type')) {
                            if ($element.data('multiple')) {
                                $.each($element.val().split(','), function(index, value){
                                    hiddenInputValues.push($element.data('filter') + ':' + value);
                                });
                            } else {
                                if ($element.data('load-from-hidden')) {
                                    var filterTitle = $element.data('filter-title') ? $element.data('filter-title') : $element.parents('li').find('label').html();

                                    data.push({
                                        'filter': $element.data('filter'),
                                        'filter_title': filterTitle,
                                        'value': $element.val(),
                                        'title': $element.val()
                                    });
                                } else {
                                    hiddenInputValues.push($element.data('filter') + ':' + $element.val());
                                }
                            }
                        // element is normal select
                        } else {
                            if ($element.data('multiple')) {
                                $.each($element.val(), function(index, value){
                                    data.push({
                                        'filter': $element.data('filter'),
                                        'filter_title': $element.parents('li').find('label').html(),
                                        'value': value,
                                        'title': $element.find('option[value="' + value + '"]').text()
                                    });
                                });
                            } else {
                                data.push({
                                    'filter': $element.data('filter'),
                                    'filter_title': $element.parents('li').find('label').html(),
                                    'value': $element.val(),
                                    'title': $element.find('option[value="' + $element.val() + '"]').text()
                                });
                            }
                        }
                    // if element is styled iCheck radio select
                    } else if ($element.is('div') && ($element.find('label div.radio input:checked').val() !== undefined) && ($element.find('label div.radio input:checked').val().length > 0)) {
                        data.push({
                            'filter': $element.data('filter'),
                            'filter_title': $element.parents('li').find('label').first().html(),
                            'value': $element.find('label div.radio input:checked').val(),
                            'title': $element.find('label div.radio input:checked').parent().next().html()
                        });
                    }
                });

                if (hiddenInputValues.length) {
                    $.ajax($(element).data('init-url'), {
                        data: { val: hiddenInputValues.join(',') },
                        dataType: "json"
                    }).done(function(response) {
                        var unsetQuery = false;

                        // check if position is set
                        $.each(response, function(index, object){
                            if (object.filter == 'position') {
                                unsetQuery = true;
                            }
                        });

                        // if position set, remove query
	                    // TO Allow QUERY AND POSITION remove following
                        if (unsetQuery) {
                            $.each(data, function(index, object){
                                if (object.filter == 'query') {
                                    data.splice(index, 1);
                                }
                            });
                        }

                        data = data.concat(response);
                        callback(data);
                    });
                } else {
                    callback(data);
                }
            }
        })
        .on("change", function(e) {
            if (e.added) {
                selectElement(e);

                if (e.added.filter == 'query' || e.added.filter == 'position') {
                    $('form.filter_block').submit();
                }
            }

            if (e.removed) {
                removeElement(e);
            }
        })
        // .on("select2-focus", function() {
        //     $(".content .filter_block .expand_form").addClass("open");
        // });

    $('[data-handler="filterTag"]').on('change', function(event) {
        handleFilter(event.target);
    });

    // for icheck radio selects
    $('[data-handler="filterTag"] input:radio').on('ifChecked', function(event) {
        handleFilter(event.target);
    });

    $(".search-main-styled-select2").select2({
        minimumResultsForSearch: Infinity
    }).on("select2:open", function() {
        $(".select2-results__options").mCustomScrollbar({
            mouseWheelPixels: 160,
            scrollInertia: 300,
            advanced: {
                updateOnContentResize: true,
                autoScrollOnFocus: false
            }
        });
    });

    $(".search-main-multi-select2").select2({
        multiple: true,
        formatResult: function (item) { return item.title; },
        formatSelection: function (item) { return item.title; },
        id: function (item) { return item.value; },
        closeOnSelect: false,
        ajax: {
            dataType: 'json',
            quietMillis: 500,
            data: function (term, page) {
                return {
                    q: term
                };
            },
            results: function (data, page) {
                return { results: data };
            },
            cache: true
        }
    });

    $('.search-main-single-select2').select2({
        formatResult: function (item) { return item.title; },
        formatSelection: function (item) { return item.title; },
        id: function (item) { return item.value; },
        ajax: {
            dataType: 'json',
            quietMillis: 500,
            data: function (term, page) {
                return {
                    q: term
                };
            },
            results: function (data, page) {
                return { results: data };
            },
            cache: true
        }
    });

    $('.search-advanced-multi-select2').select2({
        multiple: true,
        formatResult: function (item) { return item.title; },
        formatSelection: function (item) { return item.title; },
        id: function (item) { return item.value; },
        closeOnSelect: false,
        ajax: {
            dataType: 'json',
            quietMillis: 500,
            data: function (term, page) {
                return {
                    q: term
                };
            },
            results: function (data, page) {
                return { results: data };
            },
            cache: true
        }
    });

    $('.search-advanced-single-select2').select2({
        formatResult: function (item) { return item.title; },
        formatSelection: function (item) { return item.title; },
        id: function (item) { return item.value; },
        ajax: {
            dataType: 'json',
            quietMillis: 500,
            data: function (term, page) {
                return {
                    q: term
                };
            },
            results: function (data, page) {
                return { results: data };
            },
            cache: true
        }
    });

    /* TD#964 */
    $('form[data-type="search-form"] input.submit').on('click', function() {
	    var drop = $('.XXX964').parent().parent();
	    if(!$(drop).hasClass('select2-drop-active')){
	    	return true;
	    }

        var val = $(drop).find('li:first div').html();

        if (val) {
            var element = $('form .filters [data-filter="query"]'),
                object = {value: val, filter: "query", filter_title: "Position", title: val, status: "new"};

            element.val(val);
            singleElementSet($('#tags').val(), object);
        }

        return true;
    });
});/*!
 *
 *   typeit - The most versatile animated typing utility on the planet.
 *   Author: Alex MacArthur <alex@macarthur.me> (https://macarthur.me)
 *   Version: v5.6.0
 *   URL: https://typeitjs.com
 *   License: GPL-2.0
 *
 */
!function(t,e){"object"==typeof exports&&"undefined"!=typeof module?module.exports=e():"function"==typeof define&&define.amd?define(e):t.TypeIt=e()}(this,function(){"use strict";function n(t){var e=t.getBoundingClientRect();return!(e.right>window.innerWidth||e.bottom>window.innerHeight)&&!(e.top<0||e.left<0)}function o(t,e){return Math.abs(Math.random()*(t+e-(t-e))+(t-e))}function r(t,e){return 0===t.indexOf(e)}function u(t){return Array.isArray(t)?t.slice(0):t.split("<br>")}window.TypeItDefaults={strings:[],speed:100,deleteSpeed:null,lifeLike:!0,cursor:!0,cursorChar:"|",cursorSpeed:1e3,breakLines:!0,startDelay:250,startDelete:!1,nextStringDelay:750,loop:!1,loopDelay:750,html:!0,autoStart:!0,callback:!1,beforeString:!1,afterString:!1,beforeStep:!1,afterStep:!1,afterComplete:!1};var s="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(t){return typeof t}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t},h=function(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")},t=function(){function n(t,e){for(var i=0;i<e.length;i++){var n=e[i];n.enumerable=n.enumerable||!1,n.configurable=!0,"value"in n&&(n.writable=!0),Object.defineProperty(t,n.key,n)}}return function(t,e,i){return e&&n(t.prototype,e),i&&n(t,i),t}}(),a=function(){function s(t,e,i,n){h(this,s),this.typeit=n,this.timeouts=[],this.id=e,this.queue=[],this.hasStarted=!1,this.isFrozen=!1,this.isComplete=!1,this.isInTag=!1,this.stringsToDelete="",this.style="display:inline;position:relative;font:inherit;color:inherit;",this.element=t,this.setOptions(i,window.TypeItDefaults,!1),this.setNextStringDelay(),this.init()}return t(s,[{key:"setNextStringDelay",value:function(){var t=Array.isArray(this.options.nextStringDelay),e=t?null:this.options.nextStringDelay/2;this.options.nextStringDelay={before:t?this.options.nextStringDelay[0]:e,after:t?this.options.nextStringDelay[1]:e,total:t?this.options.nextStringDelay.reduce(function(t,e){return t+e}):this.options.nextStringDelay}}},{key:"init",value:function(){this.checkElement(),this.options.strings=u(this.options.strings),this.options.strings=this.options.strings.map(function(t){return t.replace(/<\!--.*?-->/g,"")}),1<=this.options.strings.length&&""===this.options.strings[0]||(this.element.innerHTML='\n        <span style="'+this.style+'" class="ti-container"></span>\n      ',this.element.setAttribute("data-typeitid",this.id),this.elementContainer=this.element.querySelector("span"),this.options.startDelete&&(this.insert(this.stringsToDelete),this.queue.push([this.delete]),this.insertSplitPause(1)),this.cursor(),this.generateQueue(),this.kickoff())}},{key:"generateQueue",value:function(){var i=this,t=0<arguments.length&&void 0!==arguments[0]?arguments[0]:null;t=null===t?[this.pause,this.options.startDelay]:t,this.queue.push(t),this.options.strings.forEach(function(t,e){if(i.queueString(t),e+1!==i.options.strings.length){if(i.options.breakLines)return i.queue.push([i.break]),void i.insertSplitPause(i.queue.length);i.queueDeletions(t),i.insertSplitPause(i.queue.length,t.length)}})}},{key:"queueDeletions",value:function(){for(var t=0<arguments.length&&void 0!==arguments[0]?arguments[0]:null,e="string"==typeof t?t.length:t,i=0;i<e;i++)this.queue.push([this.delete,1])}},{key:"queueString",value:function(t){var e=!(1<arguments.length&&void 0!==arguments[1])||arguments[1];if(t){if(t=u(t),document.implementation.createHTMLDocument("").body.innerHTML=t,e&&(t=(t=this.rake(t))[0]),this.options.html&&r(t[0],"<")&&!r(t[0],"</")){var i=t[0].match(/\<(.*?)\>/),n=document.implementation.createHTMLDocument("");n.body.innerHTML="<"+i[1]+"></"+i[1]+">",this.queue.push([this.type,n.body.children[0]])}else this.queue.push([this.type,t[0]]);t.splice(0,1),e&&this.queue[this.queue.length-1].push("first-of-string"),t.length?this.queueString(t,!1):this.queue[this.queue.length-1].push("last-of-string")}}},{key:"insertSplitPause",value:function(t){var e=1<arguments.length&&void 0!==arguments[1]?arguments[1]:1;this.queue.splice(t,0,[this.pause,this.options.nextStringDelay.before]),this.queue.splice(t-e,0,[this.pause,this.options.nextStringDelay.after])}},{key:"kickoff",value:function(){if(this.options.autoStart)return this.hasStarted=!0,void this.next();if(n(this.element))return this.hasStarted=!0,void this.next();var i=this;window.addEventListener("scroll",function t(e){n(i.element)&&!i.hasStarted&&(i.hasStarted=!0,i.next(),e.currentTarget.removeEventListener(e.type,t))})}},{key:"cursor",value:function(){var t="visibility: hidden;";if(this.options.cursor){var e=document.createElement("style");e.id=this.id;var i="\n            @keyframes blink-"+this.id+" {\n              0% {opacity: 0}\n              49% {opacity: 0}\n              50% {opacity: 1}\n            }\n\n            [data-typeitid='"+this.id+"'] .ti-cursor {\n              animation: blink-"+this.id+" "+this.options.cursorSpeed/1e3+"s infinite;\n            }\n          ";e.appendChild(document.createTextNode(i)),document.head.appendChild(e),t=""}this.element.insertAdjacentHTML("beforeend",'<span style="'+this.style+t+'" class="ti-cursor">'+this.options.cursorChar+"</span>")}},{key:"insert",value:function(t){1<arguments.length&&void 0!==arguments[1]&&arguments[1]?this.elementContainer.lastChild.insertAdjacentHTML("beforeend",t):this.elementContainer.insertAdjacentHTML("beforeend",t),this.elementContainer.innerHTML=this.elementContainer.innerHTML.split("").join("")}},{key:"checkElement",value:function(){var e=this;[].slice.call(this.element.childNodes).forEach(function(t){void 0!==t.classList&&t.classList.contains("ti-container")&&(e.element.innerHTML="")}),!this.options.startDelete&&0<this.element.innerHTML.length?this.options.strings=this.element.innerHTML.trim():this.stringsToDelete=this.element.innerHTML}},{key:"break",value:function(){this.insert("<br>"),this.next()}},{key:"pause",value:function(){var t=this,e=0<arguments.length&&void 0!==arguments[0]&&arguments[0];setTimeout(function(){t.next()},e||this.options.nextStringDelay.total)}},{key:"rake",value:function(t){var e=this;return t.map(function(t){return t=t.split(""),e.options.html?function(t){for(var e=[],i=void 0,n=!1,s=0;s<t.length;s++)"<"!==t[s]&&"&"!==t[s]||(n="&"===t[e[0]=s]),(">"===t[s]||";"===t[s]&&n)&&(e[1]=s,s=0,i=t.slice(e[0],e[1]+1).join(""),t.splice(e[0],e[1]-e[0]+1,i),n=!1);return t}(t):t})}},{key:"type",value:function(t){var e=this;this.setPace(),this.timeouts[0]=setTimeout(function(){return"string"!=typeof t?(t.innerHTML="",e.elementContainer.appendChild(t),e.isInTag=!0):r(t,"</")?e.isInTag=!1:e.insert(t,e.isInTag),void e.next()},this.typePace)}},{key:"setOptions",value:function(t){var e=1<arguments.length&&void 0!==arguments[1]?arguments[1]:null,i=!(2<arguments.length&&void 0!==arguments[2])||arguments[2],n={};for(var s in null===e&&(e=this.options),e)n[s]=e[s];for(var o in t)n[o]=t[o];this.options=n,i&&this.next()}},{key:"setPace",value:function(){var t=this.options.speed,e=null!==this.options.deleteSpeed?this.options.deleteSpeed:this.options.speed/3,i=t/2,n=e/2;this.typePace=this.options.lifeLike?o(t,i):t,this.deletePace=this.options.lifeLike?o(e,n):e}},{key:"delete",value:function(){var o=this,r=0<arguments.length&&void 0!==arguments[0]?arguments[0]:null;this.timeouts[1]=setTimeout(function(){o.setPace();for(var t=o.elementContainer.innerHTML.split(""),e=t.length-1;-1<e;e--){if(">"!==t[e]&&";"!==t[e]||!o.options.html){t.pop();break}for(var i=e;-1<i;i--){if("<br>"===t.slice(i-3,i+1).join("")){t.splice(i-3,4);break}if("&"===t[i]){t.splice(i,e-i+1);break}if("<"===t[i]&&">"!==t[i-1]){if(";"===t[i-1])for(var n=i-1;-1<n;n--)if("&"===t[n]){t.splice(n,i-n);break}t.splice(i-1,1);break}}break}if(-1<o.elementContainer.innerHTML.indexOf("></"))for(var s=o.elementContainer.innerHTML.indexOf("></")-2;0<=s;s--)if("<"===t[s]){t.splice(s,t.length-s);break}o.elementContainer.innerHTML=t.join("").replace(/<[^\/>][^>]*><\/[^>]+>/,""),null===r&&o.queue.unshift([o.delete,t.length]),1<r&&o.queue.unshift([o.delete,r-1]),o.next()},this.deletePace)}},{key:"empty",value:function(){this.elementContainer.innerHTML="",this.next()}},{key:"next",value:function(){var t=this;if(!this.isFrozen){if(0<this.queue.length)return this.step=this.queue.shift(),"first-of-string"===this.step[2]&&this.options.beforeString&&this.options.beforeString(this.step,this.queue,this.typeit),this.options.beforeStep&&this.options.beforeStep(this.step,this.queue,this.typeit),this.step[0].call(this,this.step[1],this.step[2]),"last-of-string"===this.step[2]&&this.options.afterString&&this.options.afterString(this.step,this.queue,this.typeit),void(this.options.afterStep&&this.options.afterStep(this.step,this.queue,this.typeit));if(this.options.callback&&this.options.callback(),this.options.afterComplete&&this.options.afterComplete(this.step,this.typeit),this.options.loop)return this.queueDeletions(this.elementContainer.innerHTML),this.generateQueue([this.pause,this.options.loopDelay/2]),void setTimeout(function(){t.next()},this.options.loopDelay/2);this.isComplete=!0}}}]),s}();return function(){function i(t,e){h(this,i),this.id=this.generateHash(),this.instances=[],this.elements=[],this.args=e,"object"===(void 0===t?"undefined":s(t))&&(void 0===t.length?this.elements.push(t):this.elements=t),"string"==typeof t&&(this.elements=document.querySelectorAll(t)),this.createInstances()}return t(i,[{key:"generateHash",value:function(){return Math.random().toString(36).substring(2,15)+Math.random().toString(36).substring(2,15)}},{key:"createInstances",value:function(){var e=this;[].slice.call(this.elements).forEach(function(t){e.instances.push(new a(t,e.id,e.args,e))})}},{key:"pushAction",value:function(e){var i=1<arguments.length&&void 0!==arguments[1]?arguments[1]:null;this.instances.forEach(function(t){t.queue.push([t[e],i]),!0===t.isComplete&&t.next()})}},{key:"type",value:function(){var e=0<arguments.length&&void 0!==arguments[0]?arguments[0]:"";return this.instances.forEach(function(t){t.queueString(e),!0===t.isComplete&&t.next()}),this}},{key:"delete",value:function(){var t=0<arguments.length&&void 0!==arguments[0]?arguments[0]:null;return this.pushAction("delete",t),this}},{key:"freeze",value:function(){this.instances.forEach(function(t){t.isFrozen=!0})}},{key:"unfreeze",value:function(){this.instances.forEach(function(t){t.isFrozen&&(t.isFrozen=!1,t.next())})}},{key:"pause",value:function(){var t=0<arguments.length&&void 0!==arguments[0]?arguments[0]:null;return this.pushAction("pause",t),this}},{key:"destroy",value:function(){var e=!(0<arguments.length&&void 0!==arguments[0])||arguments[0];this.instances.forEach(function(t){t.timeouts=t.timeouts.map(function(t){return clearTimeout(t),null}),e&&t.element.removeChild(t.element.querySelector(".ti-cursor"))}),this.instances=[]}},{key:"empty",value:function(){return this.pushAction("empty"),this}},{key:"break",value:function(){return this.pushAction("break"),this}},{key:"options",value:function(t){return this.pushAction("setOptions",t),this}},{key:"isComplete",get:function(){return!!this.instances.length&&this.instances[0].isComplete}}]),i}()});