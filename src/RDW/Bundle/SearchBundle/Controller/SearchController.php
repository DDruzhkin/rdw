<?php

namespace RDW\Bundle\SearchBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\VarDumper\VarDumper;

/**
 * Class SearchController
 *
 * @package RDW\Bundle\SearchBundle\Controller
 */
class SearchController extends Controller
{

    /**
     * @param Request $request
     *
     * @Route("/test1", name="rdw_search.search.city")
     *
     * @return array|Response
     */
    public function searchCityAction(Request $request)
    {

        try {
            $cities = $this->getDoctrine()
                ->getRepository('RDWLocationBundle:City')
                ->createQueryBuilder('c')
                ->select('c.id,c.title,c.position')
                ->orderBy('c.title', 'ASC')
                ->getQuery()
                ->getResult();
            $key = array_search('Минск', array_column($cities, 'title'));
            $cities = array($cities[$key]) + $cities;
            unset($cities[$key]);
        } catch (\Exception $e) {
            $cities = $e->getMessage();
        }

        $response = $this->render('RDWSearchBundle::city.html.twig', [
            //'form' => $form->createView(),
            'cities' => $cities
        ]);

        return $response;
    }

    /**
     * @param Request $request
     * @param string $type
     *
     * @Route("/form/{type}", name="rdw_search.search.form", requirements={"type" = "job|cv"})
     *
     * @return array|Response
     */
    public function searchFormAction(Request $request, $type)
    {
        $formName = $type . 's';
        $form = $this->createForm($formName);
        $form->handleRequest($request);
        $city_name = 'Минск';
        if(!empty($request->query->get('jobs')['cities'])) {
            $cities = explode(',', $request->query->get('jobs')['cities']);
            $cities = array_filter($cities);
            switch (count($cities)) {
                case 0:
                    $city_name = 'Минск';
                    break;
                case 1:
                    $city = $this->getDoctrine()
                        ->getRepository('RDWLocationBundle:City')
                        ->createQueryBuilder('c')
                        ->select('c.title')
                        ->where('c.id=' . array_shift($cities))
                        ->getQuery()
                        ->getResult();
                    $city_name = $city[0]['title'];
                    break;
                case 114:
                    $city_name = 'Вся Беларусь';
                    break;
                default:
                    $city_name = 'Несколько';
            }
        }

        $response = $this->render('RDWSearchBundle:' . ucfirst($type) . ':searchForm.html.twig', [
            'form' => $form->createView(),
            'hasValues' => ($request->query->count() > 0),
            'city_name' => $city_name
        ]);

        return $response;
    }

    /**
     * @param Request $request
     * @param string $filter
     *
     * Route("/filter-values/{filter}", name="rdw_search.search.filter_values", requirements = {"filter" = "job|cv"})
     * @Route("/filter-values/{filter}", name="rdw_search.search.filter_values")
     *
     * @return JsonResponse
     */
    public function searchFilterValuesAction(Request $request, $filter)
    {
        if ('professional_area' === mb_strtolower($filter)) {
            $result = $this->container->get('rdw_job.provider.professional_area')->findPaByRequest($request);

            return new JsonResponse($result);
        } elseif ('all_positions' === mb_strtolower($filter)) {
            $limit = $this->container->getParameter('rdw_job_positions_limit');
            $result = $this->container->get('rdw_job.provider.position')->findApprovedPositionByRequest($request, $limit);
            $query = $request->query->get('q');
            if (!count($result)) {
                $result = [
                    [
                        'id' => \time(),
                        'value' => $query,
                        'filter' => 'position',
                        'filter_title' => 'Position',
                        'title' => $query,
                    ],
                ];
            }
            return new JsonResponse($result);
        }

        $query = $request->query->get('q');
        $sphinx = $this->container->get('sphinx_search_service');
        /** RDWBY-92 */
        if (('job' == $filter) || ('cv' == $filter)) {
            //TODO there are should be found proper way to find job||cv by relevance
            $sphinx->search($filter, $query, 20);
            $result = $sphinx->getResults();
            //TODO: We should remove ID and replace "value" in result to support more effective search
            //TODO: But before should be properly fixed JS (src/RDW/Bundle/SearchBundle/Resources/public/js/search.js)
            //$result = array_map(function ($element) {
            //    $element['value'] = $element['title'];
            //
            //    return $element;
            //}, $result);
        } else {
            $sphinx->search($filter, $query);
            $result = $sphinx->getResults();
        }

        /**
         * RDWBY-31
         */
        $uniques = [];

        foreach ($result as $k => $item) {
            $title = strtolower($item['title']);
            if (in_array($title, $uniques)) {
                unset($result[$k]);
            } else {
                $uniques[] = $title;
            }
        }

        return new JsonResponse(array_values($result));
    }

    /**
     * @param Request $request
     *
     * @Route("/init-values", name="rdw_search.search.init_values")
     *
     * @return JsonResponse
     */
    public function initValuesAction(Request $request)
    {
        return $this->container->get('rdw_search_service')->searchInitValues($request->query->get('val'));
    }

    /**
     * @param Request $request
     * @param string $filter
     *
     * @Route("/find-pa-by/{filter}", name="rdw_search.search.find_pa")
     *
     * @return JsonResponse
     */
    public function findProfessionalArea(Request $request, $filter)
    {
        $result = [];

        if ('all_positions' === mb_strtolower($filter)) {
            $result = $this->container->get('rdw_job.provider.professional_area')->findPaByPosition($request);
        }

        return new JsonResponse(array_values($result));
    }
}
