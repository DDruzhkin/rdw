<?php

namespace RDW\Bundle\SearchBundle\Tests\Service;

use RDW\Bundle\LocationBundle\Entity\City;
use RDW\Bundle\SearchBundle\Service\SearchService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class SearchServiceTest
 *
 * @package RDW\Bundle\SearchBundle\Tests\Service
 */
class SearchServiceTest extends WebTestCase
{
    const CLASS_JSON_RESPONSE = 'Symfony\Component\HttpFoundation\JsonResponse';

    /**
     * @var SearchService
     */
    private $sut;

    /**
     * @var int
     */
    private $itemId;

    /**
     * @var string
     */
    private $itemTitle;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $item;

    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $em;

    public function setUp()
    {
        $client = static::createClient();
        $container = $client->getContainer();

        $this->em = $container->get('doctrine')->getManager();

        $this->sut = $container->get('rdw_search_service');
        $this->sut->initTranslator();

        $this->itemId = 5;
        $this->itemTitle = 'scope';
        $this->item = $this->getMock('RDW\Bundle\ManageBundle\Entity\Filter');

        $this->translator = $container->get('translator');
    }

    /**
     * @test
     */
    public function it_should_format_correct_result_for_other_filter_types()
    {
        $this->item->expects($this->once())->method('getId')->willReturn($this->itemId);
        $this->item->expects($this->once())->method('getTitle')->willReturn($this->itemTitle);

        $filter = SearchService::FILTER_TYPE_CITY;

        $expected = ['value' => $this->itemId, 'title' => $this->itemTitle, 'filter' => $filter, 'filter_title' => $this->translator->trans(ucfirst($filter))];
        $result = $this->sut->formatResult($this->item, $filter);

        $this->assertEquals($expected, $result);
    }

    /**
     * @test
     */
    public function it_should_return_json_response_when_searching_init_values()
    {
        $result = $this->sut->searchInitValues('');

        $this->assertInstanceOf(self::CLASS_JSON_RESPONSE, $result);
    }

    /**
     * @test
     */
    public function it_should_not_search_in_non_existing_filters()
    {
        $this->item->expects($this->exactly(0))->method('getId');
        $this->item->expects($this->exactly(0))->method('getTitle');

        $queryValues = 'fake:44';

        /** @var JsonResponse $result */
        $result = $this->sut->searchInitValues($queryValues)->getContent();
        $resultDecoded = json_decode($result);

        $this->assertTrue(empty($resultDecoded));
    }

    /**
     * @test
     */
    public function it_should_add_new_filter_key_when_it_not_exists()
    {
        $city = $this->createCity();

        $queryValues = sprintf('%s:%d', SearchService::FILTER_TYPE_CITY, $city->getId());

        /** @var JsonResponse $result */
        $result = $this->sut->searchInitValues($queryValues)->getContent();
        $resultDecoded = json_decode($result);

        $expectedSearchResult = new \stdClass();
        $expectedSearchResult->value = $city->getId();
        $expectedSearchResult->title = $city->getTitle();
        $expectedSearchResult->filter = SearchService::FILTER_TYPE_CITY;
        $expectedSearchResult->filter_title = $this->translator->trans(ucfirst(SearchService::FILTER_TYPE_CITY));

        $expected = [];
        $expected[] = $expectedSearchResult;

        $this->assertEquals($expected, $resultDecoded);
    }

    /**
     * @return City
     */
    private function createCity()
    {
        $city = new City();
        $city->setTitle('Vilnius');
        $city->setTitleWhere('Vilnius');

        $this->em->persist($city);
        $this->em->flush();

        return $city;
    }
}
