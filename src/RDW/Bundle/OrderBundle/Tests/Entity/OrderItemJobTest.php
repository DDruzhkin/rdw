<?php
/**
 * Created by PhpStorm.
 * User: vb
 * Date: 12/22/14
 * Time: 1:08 PM
 */

namespace RDW\Bundle\OrderBundle\Tests\Entity;

use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\OrderBundle\Entity\OrderItemJob;

class OrderItemJobTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    function it_should_get_set_item_properly()
    {
        $item = new OrderItemJob();

        $job = new Job();
        $item->setItem($job);

        $this->assertEquals($job, $item->getItem());
    }
}
