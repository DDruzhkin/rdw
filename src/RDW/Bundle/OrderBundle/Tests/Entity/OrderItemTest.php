<?php

namespace RDW\Bundle\OrderBundle\Tests\Entity;

use RDW\Bundle\OrderBundle\Entity\Order;
use RDW\Bundle\OrderBundle\Entity\OrderItem;

/**
 * Class OrderItemTest
 *
 * @package RDW\Bundle\OrderBundle\Tests\Entity
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class OrderItemTest extends \PHPUnit_Framework_TestCase
{
    const CLASS_NAME = 'RDW\Bundle\OrderBundle\Entity\OrderItem';
    const ORDER_CLASS_NAME = 'RDW\Bundle\OrderBundle\Entity\Order';

    /**
     * @var OrderItem
     */
    private $orderItem;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $service;

    /**
     * set up
     */
    public function setUp()
    {
        $this->service = $this->getMock('RDW\Bundle\ServiceBundle\Entity\Service');
        $this->orderItem = new OrderItem();
    }

    /**
     * @test
     */
    public function it_has_no_id_by_default()
    {
        $this->assertNull($this->orderItem->getId());
    }

    /**
     * @test
     */
    public function it_should_set_price_properly()
    {
        $result = $this->orderItem->setPrice(55.25);

        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals(55.25, $this->orderItem->getPrice());
    }

    /**
     * @test
     */
    public function it_should_set_service_properly()
    {
        $result = $this->orderItem->setService($this->service);

        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals($this->service, $this->orderItem->getService());
    }

    /**
     * @test
     */
    public function it_should_set_valid_till_properly()
    {
        $validTill = new \DateTime();
        $result = $this->orderItem->setValidTill($validTill);

        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals($validTill, $this->orderItem->getValidTill());
    }

    /**
     * @test
     */
    public function it_should_set_order_properly()
    {
        $result = $this->orderItem->setOrder(new Order());

        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertInstanceOf(self::ORDER_CLASS_NAME, $this->orderItem->getOrder());
    }

    /**
     * @test
     */
    public function it_should_return_get_period_properly()
    {
        $orderItem = new OrderItem();
        $orderItem->setPeriod(9);

        $this->assertEquals(9, $orderItem->getPeriod());
    }

    /**
     * @test
     */
    public function it_should_return_get_valid_days_properly()
    {
        $orderItem = new OrderItem();
        $orderItem->setValidTill(new \DateTime('+2days'));

        $this->assertEquals(2, $orderItem->getValidDaysLeft());
    }

    /**
     * @test
     */
    public function it_should_return_get_price_for_one_period_properly()
    {
        $orderItem = new OrderItem();
        $orderItem->setPeriod(14);
        $orderItem->setPrice(28);

        $this->assertEquals(14, $orderItem->getPriceForOnePeriod(7));
    }

    /**
     * @test
     */
    public function it_should_set_title_properly()
    {
        $orderItem = new OrderItem();

        $this->assertNull($orderItem->getTitle());

        $orderItem->setTitle('Bla bla BLAAA');

        $this->assertEquals('Bla bla BLAAA', $orderItem->getTitle());
    }
}
