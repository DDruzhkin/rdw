<?php
/**
 * Created by PhpStorm.
 * User: vb
 * Date: 12/22/14
 * Time: 1:07 PM
 */

namespace RDW\Bundle\OrderBundle\Tests\Entity;

use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\OrderBundle\Entity\OrderItemCv;

class OrderItemCvTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    function it_should_get_set_item_properly()
    {
        $item = new OrderItemCv();

        $cv = new Cv();
        $item->setItem($cv);

        $this->assertEquals($cv, $item->getItem());
    }
}
