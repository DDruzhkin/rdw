<?php

namespace RDW\Bundle\OrderBundle\Tests\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use RDW\Bundle\OrderBundle\Entity\Order;
use RDW\Bundle\OrderBundle\Entity\OrderItem;
use RDW\Bundle\OrderBundle\RDWOrderStatus;
use RDW\Bundle\PaymentBundle\Entity\Payment;
use RDW\Bundle\ServiceBundle\Entity\Service;
use RDW\Bundle\SmsBundle\Entity\SmsCode;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;

/**
 * Class OrderTest
 *
 * @package RDW\Bundle\OrderBundle\Tests\Entity
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class OrderTest extends \PHPUnit_Framework_TestCase
{
    const CLASS_NAME = 'RDW\Bundle\OrderBundle\Entity\Order';

    protected $user;

    /**
     * @var Order
     */
    protected $order;

    /**
     * set up
     */
    public function setUp()
    {
        $this->user = $this->getMock('RDW\Bundle\UserBundle\Entity\User');
        $this->order = new Order();
    }

    /**
     * @test
     */
    public function it_has_no_id_by_default()
    {
        $this->assertNull($this->getOrder()->getId());
    }

    /**
     * @test
     */
    public function it_should_set_user_properly()
    {
        $order = $this->getOrder()->setUser($this->user);

        $this->assertInstanceOf('RDW\Bundle\UserBundle\Entity\User', $order->getUser());
    }

    /**
     * @test
     */
    public function it_should_has_no_products_by_default()
    {
        $this->assertCount(0, $this->getOrder()->getItems());
        $this->assertInstanceOf('Doctrine\Common\Collections\ArrayCollection', $this->getOrder()->getItems());
    }

    /**
     * @test
     */
    public function it_should_add_remove_product_properly()
    {
        $order = $this->getOrder();
        $orderItem = new OrderItem();

        $order->addItem($orderItem);
        $this->assertCount(1, $order->getItems());

        $order->removeItem($orderItem);
        $this->assertCount(0, $order->getItems());
    }

    /**
     * @test
     */
    public function it_should_set_status_properly()
    {
        $order = $this->getOrder();
        $result = $order->setStatus(RDWOrderStatus::STATUS_PAID);

        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals(RDWOrderStatus::STATUS_PAID, $order->getStatus());
    }

    /**
     * @test
     */
    public function it_should_set_total_properly()
    {
        $order = $this->getOrder();
        $result = $order->setTotal(10000);

        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals(10000, $order->getTotal());
    }

    /**
     * @test
     */
    public function it_should_count_total()
    {
        $item1 = new OrderItem();
        $item1->setPrice(5000);
        $item2 = new OrderItem();
        $item2->setPrice(7000);

        $order = $this->getOrder();
        $order->addItem($item1);
        $order->addItem($item2);

        $this->assertEquals(12000, $order->countTotal());
    }

    /**
     * @test
     */
    public function it_should_set_created_at_properly()
    {
        $createdAt = new \DateTime('-1hour');
        $order = $this->getOrder();
        $result = $order->setCreatedAt($createdAt);

        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals($createdAt, $order->getCreatedAt());
    }

    /**
     * @test
     */
    public function it_should_set_updated_at_properly()
    {
        $createdAt = new \DateTime('-1hour');
        $order = $this->getOrder();
        $result = $order->setUpdatedAt($createdAt);

        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals($createdAt, $order->getUpdatedAt());
    }

    /**
     * @test
     */
    public function it_should_set_paid_at_properly()
    {
        $paidAt = new \DateTime();
        $order = $this->getOrder();
        $order->setStatus(RDWOrderStatus::STATUS_PAID);
        $result = $order->setPaidAt($paidAt);

        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals($paidAt, $order->getPaidAt());
        $this->assertTrue($order->isPaid());
    }

    /**
     * @test
     */
    public function it_should_return_is_guarantee_true()
    {
        $order = $this->getOrder();
        $order->setPaymentType(Order::PAYMENT_GUARANTEE);

        $this->assertTrue($order->isGuarantee());
    }

    /**
     * @test
     */
    public function it_should_return_is_transfer_true()
    {
        $order = $this->getOrder();
        $order->setPaymentType(Order::PAYMENT_TRANSFER);

        $this->assertTrue($order->isTransfer());
    }

    /**
     * @test
     */
    public function it_should_return_show_mark_as_paid_button_true()
    {
        $order = $this->getOrder();
        $order->setStatus(RDWOrderStatus::STATUS_SUBMITTED);

        $order->setPaymentType(Order::PAYMENT_TRANSFER);
        $this->assertTrue($order->showMarkAsPaidButton());

        $order->setPaymentType(Order::PAYMENT_GUARANTEE);
        $this->assertTrue($order->showMarkAsPaidButton());
    }

    /**
     * @test
     */
    public function it_should_not_allow_to_change_status_into_same()
    {
        $order = $this->getOrder();

        $order->setStatus(RDWOrderStatus::STATUS_CANCELLED);
        $this->assertFalse($order->isStatusChangeable(RDWOrderStatus::STATUS_CANCELLED));

        $order->setStatus(RDWOrderStatus::STATUS_PAID);
        $this->assertFalse($order->isStatusChangeable(RDWOrderStatus::STATUS_PAID));

        $order->setStatus(RDWOrderStatus::STATUS_PENDING);
        $this->assertFalse($order->isStatusChangeable(RDWOrderStatus::STATUS_PENDING));

        $order->setStatus(RDWOrderStatus::STATUS_SUBMITTED);
        $this->assertFalse($order->isStatusChangeable(RDWOrderStatus::STATUS_SUBMITTED));
    }

    /**
     * @test
     */
    public function it_should_allow_to_change_status_from_pending_to_cancelled()
    {
        $order = $this->getOrder();
        $order->setStatus(RDWOrderStatus::STATUS_PENDING);

        $this->assertTrue($order->isStatusChangeable(RDWOrderStatus::STATUS_CANCELLED));
    }

    /**
     * @test
     */
    public function it_should_allow_to_change_status_from_submitted_to_cancelled()
    {
        $order = $this->getOrder();
        $order->setStatus(RDWOrderStatus::STATUS_SUBMITTED);

        $this->assertTrue($order->isStatusChangeable(RDWOrderStatus::STATUS_CANCELLED));
    }

    /**
     * @test
     */
    public function it_should_not_allow_to_change_status_from_paid_to_cancelled()
    {
        $order = $this->getOrder();
        $order->setStatus(RDWOrderStatus::STATUS_PAID);

        $this->assertFalse($order->isStatusChangeable(RDWOrderStatus::STATUS_CANCELLED));
    }

    /**
     * @test
     */
    public function it_should_allow_to_change_status_from_submitted_to_pending()
    {
        $order = $this->getOrder();
        $order->setStatus(RDWOrderStatus::STATUS_SUBMITTED);

        $this->assertTrue($order->isStatusChangeable(RDWOrderStatus::STATUS_PENDING));
    }

    /**
     * @test
     */
    public function it_should_allow_to_change_status_from_submitted_to_paid()
    {
        $order = $this->getOrder();
        $order->setStatus(RDWOrderStatus::STATUS_SUBMITTED);

        $this->assertTrue($order->isStatusChangeable(RDWOrderStatus::STATUS_PAID));
    }

    /**
     * @test
     */
    public function it_should_allow_to_change_status_from_pending_to_paid()
    {
        $order = new Order();
        $order->setStatus(RDWOrderStatus::STATUS_PENDING);

        $this->assertTrue($order->isStatusChangeable(RDWOrderStatus::STATUS_PAID));
    }

    /**
     * @test
     */
    public function it_should_return_is_type_calculator_true()
    {
        $order = $this->getOrder();
        $order->setType(Order::TYPE_CALCULATOR);

        $this->assertTrue($order->isTypeCalculator());
    }

    /**
     * @test
     */
    public function it_should_return_payment_types_for_employee_properly()
    {
        $types = [Order::PAYMENT_SMS];
        $user = new RegisteredUser();
        $user->setType(RegisteredUser::USER_TYPE_EMPLOYEE);

        $paymentTypes = $this->getOrder()->getPaymentTypes($user);
        $this->assertEmpty(array_diff($types, array_keys($paymentTypes)));
    }

    /**
     * @test
     */
    public function it_should_return_payment_types_for_manager_properly()
    {
        $types = [
            Order::PAYMENT_GUARANTEE,
            Order::PAYMENT_TRANSFER,
        ];

        $this->assertEmpty(array_diff($types, array_keys(Order::getPaymentTypesForManager())));
    }

    /**
     * @test
     */
    public function it_should_return_payment_types_properly_when_user_is_employer()
    {
        $user = new RegisteredUser();
        $user->setType(RegisteredUser::USER_TYPE_EMPLOYER);

        $types = [
            Order::PAYMENT_GUARANTEE,
            Order::PAYMENT_TRANSFER,
        ];

        $this->assertEmpty(array_diff($types, array_keys(Order::getPaymentTypes($user))));
    }

    /**
     * @test
     */
    public function it_should_return_payment_types_to_add_money_to_wallet_properly()
    {
        $types = [
            Order::PAYMENT_TRANSFER,
            Order::PAYMENT_CARD,
        ];

        $this->assertEmpty(array_diff($types, Order::getPaymentTypesToAddMoneyToWallet()));
    }

    /**
     * @test
     */
    public function it_should_return_successfull_payment_properly()
    {
        $order = $this->getOrder();
        $payment = new Payment();
        $order->setSuccessfulPayment($payment);

        $this->assertEquals($payment, $order->getSuccessfulPayment());

        $class = 'RDW\Bundle\PaymentBundle\Entity\Payment';
        $this->assertInstanceOf($class, $order->getSuccessfulPayment());
    }

    /**
     * @test
     */
    public function it_should_return_invoice_number_as_integer()
    {
        $number = 1000;
        $order = $this->getOrder();
        $order->setInvoiceNumber($number);

        $this->assertEquals($number, $order->getInvoiceNumber());
        $this->assertInternalType('int', $order->getInvoiceNumber());
    }

    /**
     * @test
     */
    public function it_should_return_currency_as_string()
    {
        $currency = 'BYR';
        $order = $this->getOrder();
        $order->setCurrency($currency);

        $this->assertEquals($currency, $order->getCurrency());
        $this->assertInternalType('string', $order->getCurrency());
    }

    /**
     * @test
     */
    public function it_should_return_email_of_user()
    {
        $email = 'gs@nebijokit.lt';
        $user = new RegisteredUser();
        $user->setEmail($email);

        $order = $this->getOrder();
        $order->setUser($user);

        $this->assertEquals($email, $order->getEmail());
    }

    /**
     * @test
     */
    public function it_should_return_phone_of_user()
    {
        $phone = '+370 600 11 000';
        $user = new RegisteredUser();
        $user->setPhone($phone);

        $order = $this->getOrder();
        $order->setUser($user);

        $this->assertEquals($phone, $order->getPhone());
    }

    /**
     * @test
     */
    public function it_should_return_payments_as_array_collection()
    {
        $payments = new ArrayCollection();
        $order = $this->getOrder();
        $order->setPayments($payments);

        $this->assertInstanceOf('Doctrine\Common\Collections\ArrayCollection', $order->getPayments());
    }

    /**
     * @test
     */
    public function it_should_return_true_when_has_items_without_sms_code()
    {
        $item1 = new OrderItem();
        $item2 = new OrderItem();

        $order = $this->getOrder();
        $order->addItem($item1);
        $order->addItem($item2);

        $this->assertTrue($order->hasItemsWithoutSmsCode());
    }

    /**
     * @test
     */
    public function it_should_return_false_when_has_no_items_without_sms_code()
    {
        $item1 = new OrderItem();
        $item1->setSmsCode(new SmsCode());
        $item2 = new OrderItem();
        $item2->setSmsCode(new SmsCode());

        $order = $this->getOrder();
        $order->addItem($item1);
        $order->addItem($item2);

        $this->assertFalse($order->hasItemsWithoutSmsCode());
    }

    /**
     * @test
     */
    public function it_should_return_true_when_all_items_has_valid_period_for_sms()
    {
        $period = 7;
        $item1 = new OrderItem();
        $item1->setPeriod(7);
        $item2 = new OrderItem();
        $item2->setPeriod(4);

        $order = $this->getOrder();
        $order->addItem($item1);
        $order->addItem($item2);

        $this->assertTrue($order->isItemsPeriodValidForSms($period));
    }

    /**
     * @test
     */
    public function it_should_return_false_when_at_least_one_of_items_has_not_valid_period_for_sms()
    {
        $period = 7;
        $item1 = new OrderItem();
        $item1->setPeriod(14);
        $item2 = new OrderItem();
        $item2->setPeriod(4);

        $order = $this->getOrder();
        $order->addItem($item1);
        $order->addItem($item2);

        $this->assertFalse($order->isItemsPeriodValidForSms($period));
    }

    /**
     * @test
     */
    public function it_should_return_valid_boolean_when_checking_if_order_is_paid_by_sms()
    {
        $order = $this->getOrder();
        $order->setPaymentType(Order::PAYMENT_SMS);

        $this->assertTrue($order->isPaidBySms());

        $order->setPaymentType(Order::PAYMENT_CARD);
        $this->assertFalse($order->isPaidBySms());
    }

    /**
     * @test
     */
    public function it_should_set_order_as_paid_only_when_all_items_are_paid()
    {
        $item1 = new OrderItem();
        $item1->setStatus(RDWOrderStatus::STATUS_PAID);
        $item2 = new OrderItem();
        $item2->setStatus(RDWOrderStatus::STATUS_PAID);

        $order = $this->getOrder();
        $order->setStatus(RDWOrderStatus::STATUS_PENDING);

        $order->addItem($item1);
        $order->addItem($item2);

        $order->setFullyPaid();
        $this->assertEquals(RDWOrderStatus::STATUS_PAID, $order->getStatus());
    }

    /**
     * @test
     */
    public function it_should_not_set_order_as_paid_when_not_all_items_are_paid()
    {
        $item1 = new OrderItem();
        $item1->setStatus(RDWOrderStatus::STATUS_PAID);
        $item2 = new OrderItem();
        $item2->setStatus(RDWOrderStatus::STATUS_PENDING);

        $order = $this->getOrder();
        $order->setStatus(RDWOrderStatus::STATUS_PENDING);

        $order->addItem($item1);
        $order->addItem($item2);

        $this->assertNull($order->setFullyPaid());
        $this->assertEquals(RDWOrderStatus::STATUS_PENDING, $order->getStatus());
    }

    /**
     * @test
     */
    public function it_should_allow_to_cancel_order_when_its_status_is_submitted_or_pending()
    {
        $order = $this->getOrder();
        $order->setStatus(RDWOrderStatus::STATUS_SUBMITTED);

        $this->assertTrue($order->showCancelButton());

        $order->setStatus(RDWOrderStatus::STATUS_PENDING);
        $this->assertTrue($order->showCancelButton());
    }

    /**
     * @test
     */
    public function it_should_return_true_when_user_is_owner_of_order()
    {
        $id = 5;
        $owner = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');
        $owner->expects($this->once())->method('getId')->willReturn($id);
        $user = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');
        $user->expects($this->once())->method('getId')->willReturn($id);

        $order = $this->getOrder();
        $order->setUser($owner);

        $this->assertTrue($order->isOwner($user));
    }

    /**
     * @test
     */
    public function it_should_return_is_order_only_add_money_to_balance()
    {
        $order = $this->getOrder();

        $service = new Service();
        $service->setType(Service::TYPE_MONEY_TO_BALANCE);

        $orderItem = new OrderItem();
        $orderItem->setService($service);

        $order->addItem($orderItem);

        $this->assertFalse($order->isPeriodVisible());

        $service2 = new Service();
        $service2->setType(Service::TYPE_VIP);

        $orderItem2 = new OrderItem();
        $orderItem2->setService($service2);

        $order->addItem($orderItem2);

        $this->assertTrue($order->isPeriodVisible());
    }


    /**
     * @return Order
     */
    private function getOrder()
    {
        return new Order();
    }
}
