<?php

namespace RDW\Bundle\OrderBundle\Tests\Service;

use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\OrderBundle\Entity\Order;
use RDW\Bundle\OrderBundle\Entity\OrderItem;
use RDW\Bundle\OrderBundle\Entity\OrderItemCv;
use RDW\Bundle\OrderBundle\RDWOrderStatus;
use RDW\Bundle\OrderBundle\Service\OrderManager;
use RDW\Bundle\ServiceBundle\Entity\Service;

class OrderManagerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $em;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $repo;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $translator;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $smsCodeManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $order;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $user;

    /**
     * @var OrderManager
     */
    private $orderManager;

    /**
     * @var int
     */
    private $firstinvoiceNumber;

    /**
     * @var int
     */
    private $invoiceNumberLength;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var int
     */
    private $vat;

    /**
     * set up
     */
    public function setUp()
    {
        $this->order = $this->getMock('RDW\Bundle\OrderBundle\Entity\Order');
        $this->user = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');
        $this->translator = $this->getMockBuilder('Symfony\Component\Translation\Translator')->disableOriginalConstructor()->getMock();
        $this->smsCodeManager = $this->getMockBuilder('RDW\Bundle\SmsBundle\Service\SmsCodeManager')->disableOriginalConstructor()->getMock();

        $this->repo = $this->getMockBuilder('RDW\Bundle\OrderBundle\Repository\OrderRepository')->disableOriginalConstructor()->getMock();
        $this->em = $this->getMockBuilder('Doctrine\Common\Persistence\ObjectManager')
            ->disableOriginalConstructor()->getMock();
        $this->em->expects($this->any())
            ->method('getRepository')
            ->with('RDWOrderBundle:Order')
            ->willReturn($this->repo);

        $this->firstinvoiceNumber = 1;
        $this->invoiceNumberLength = 6;
        $this->currency = 'BYR';
        $this->vat = 20;

        $this->orderManager = new OrderManager($this->em, $this->translator, $this->smsCodeManager, $this->firstinvoiceNumber, $this->invoiceNumberLength, $this->currency, $this->vat);
    }

    /**
     * @test
     */
    public function it_should_update_order()
    {
        $this->em
            ->expects($this->once())
            ->method('persist')
            ->with($this->equalTo($this->order));

        $this->em
            ->expects($this->once())
            ->method('flush');

        $this->assertTrue($this->orderManager->update($this->order));
    }

    /**
     * @test
     */
    function it_should_create_new_order_instance_for_user()
    {
        $order = new Order();
        $order->setUser($this->user);
        $order->setCurrency($this->currency);
        $order->setVat($this->vat);
        $order->setType(Order::TYPE_CALCULATOR);
        $order->setInvoiceNumber(000001);

        $this->assertEquals($order, $this->orderManager->createNewOrderForUser($this->user, Order::TYPE_CALCULATOR));
    }

    /**
     * @test
     */
    function it_should_add_new_item_for_order_with_type_job()
    {
        $result = $this->orderManager->addNewOrderItem($this->order, OrderItem::TYPE_JOB, new Job());

        $this->assertInstanceOf('RDW\Bundle\OrderBundle\Entity\OrderItemJob', $result);
    }

    /**
     * @test
     */
    function it_should_not_generate_invoice_number_when_order_already_has_it()
    {
        $order = new Order();
        $order->setInvoiceNumber('5555');

        $this->assertFalse($this->orderManager->generateInvoiceNumber($order));
    }

    /**
     * @test
     */
    function it_should_set_next_invoice_number_properly()
    {
        $order = new Order();

        $lastNumber = 1234;
        $nextNumber = str_pad($lastNumber + 1, $this->invoiceNumberLength, 0, STR_PAD_LEFT);

        $this->repo
            ->expects($this->once())
            ->method('getLastInvoiceNumber')
            ->willReturn($lastNumber);

        $this->orderManager->generateInvoiceNumber($order);

        $this->assertEquals($nextNumber, $order->getInvoiceNumber());
    }

    /**
     * @test
     */
    function it_should_set_next_invoice_number_properly_first_time()
    {
        $order = new Order();

        $nextNumber = str_pad($this->firstinvoiceNumber, $this->invoiceNumberLength, 0, STR_PAD_LEFT);

        $this->repo
            ->expects($this->once())
            ->method('getLastInvoiceNumber')
            ->willReturn(null);

        $this->orderManager->generateInvoiceNumber($order);

        $this->assertEquals($nextNumber, $order->getInvoiceNumber());
    }

    /**
     * @test
     */
    function it_should_create_order_with_balance_addition_properly()
    {
        $this->user
            ->expects($this->once())
            ->method('isTypeEmployee')
            ->willReturn(true);

        $order = new Order();
        $order->setUser($this->user);
        $order->setCurrency($this->currency);
        $order->setInvoiceNumber(000001);
        $order->setVat($this->vat);
        $order->setType(Order::TYPE_CREDIT);
        $order->setStatus(RDWOrderStatus::STATUS_SUBMITTED);

        $service = new Service();
        $service->setTitle('Add to balance');

        $price = 1000;

        $orderItem = new OrderItemCv();
        $orderItem->setPrice($price);
        $orderItem->setOrder($order);
        $orderItem->setStatus(RDWOrderStatus::STATUS_SUBMITTED);
        $orderItem->setPeriod(1);
        $orderItem->setService($service);
        $orderItem->setTitle($service->getTitle());

        $order->addItem($orderItem);

        $order->setTotal($price);

        $this->em
            ->expects($this->once())
            ->method('persist')
            ->with($this->equalTo($order));

        $this->em
            ->expects($this->once())
            ->method('flush');

        $this->assertEquals($order, $this->orderManager->createOrderWithBalanceAddition($this->user, $service, $price));
    }

    /**
     * @test
     */
    function it_should_add_new_item_for_order_with_type_cv()
    {
        $result = $this->orderManager->addNewOrderItem($this->order, OrderItem::TYPE_CV, new Cv());

        $this->assertInstanceOf('RDW\Bundle\OrderBundle\Entity\OrderItemCv', $result);
    }
}
