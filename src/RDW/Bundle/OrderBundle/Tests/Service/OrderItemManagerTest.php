<?php

namespace RDW\Bundle\OrderBundle\Tests\Service;

use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Event\JobEvent;
use RDW\Bundle\JobBundle\RDWJobEvents;
use RDW\Bundle\OrderBundle\RDWOrderStatus;
use RDW\Bundle\ServiceBundle\Entity\Service;
use RDW\Bundle\OrderBundle\Service\OrderItemManager;
use RDW\Bundle\OrderBundle\Entity\Order;

/**
 * Class OrderItemManagerTest
 *
 * @package RDW\Bundle\OrderBundle\Tests\Service
 */
class OrderItemManagerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $em;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $repo;

    /**
     * @var OrderItemManager
     */
    private $orderItemManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $orderManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $walletManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $translator;

    /**
     * @var int
     */
    private $period = 7;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $item;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $orderItem;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $user;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $service;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $activeOrderItem;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $order;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $dispatcher;

    /**
     * Setup
     */
    public function setUp()
    {
        $this->em = $this->getMockBuilder('Doctrine\Common\Persistence\ObjectManager')
            ->disableOriginalConstructor()->getMock();
        $this->repo = $this->getMockBuilder('RDW\Bundle\OrderBundle\Repository\OrderRepository')->disableOriginalConstructor()->getMock();
        $this->orderManager = $this->getMockBuilder('RDW\Bundle\OrderBundle\Service\OrderManager')->disableOriginalConstructor()->getMock();
        $this->walletManager = $this->getMockBuilder('RDW\Bundle\WalletBundle\Service\WalletManager')->disableOriginalConstructor()->getMock();
        $this->translator = $this->getMockBuilder('Symfony\Component\Translation\Translator')->disableOriginalConstructor()->getMock();
        $this->dispatcher = $this->getMockBuilder('Symfony\Component\EventDispatcher\EventDispatcher')
            ->disableOriginalConstructor()->getMock();

        $this->orderItemManager = new OrderItemManager($this->em, $this->repo, $this->orderManager, $this->walletManager, $this->translator, $this->period, $this->dispatcher);

        $this->item = $this->getMock('RDW\Bundle\JobBundle\Entity\Job');
        $this->user = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');
        $this->service = $this->getMock('RDW\Bundle\ServiceBundle\Entity\Service');
        $this->order = $this->getMock('RDW\Bundle\OrderBundle\Entity\Order');

        // @todo do refactoring, change OrderItemJob to OrderItem
        $this->orderItem = $this->getMock('RDW\Bundle\OrderBundle\Entity\OrderItemJob');
        $this->activeOrderItem = $this->getMock('RDW\Bundle\OrderBundle\Entity\OrderItemJob');
    }

    /**
     * @test
     */
    public function it_should_not_set_valid_till_if_it_has_no_service()
    {
        $this->orderItem->expects($this->once())->method('getService')->willReturn(null);

        $result = $this->orderItemManager->activateService($this->orderItem, $this->user);
        $this->assertNull($result);
    }

    /**
     * @test
     */
    public function it_should_set_valid_till_for_single_service_properly()
    {
        $this->service->expects($this->any())->method('getIsSingle')->willReturn(true);

        $validTill = new \DateTime('+1000years');
        $this->orderItem->expects($this->any())->method('getService')->willReturn($this->service);
        $this->orderItem->expects($this->once())->method('getUnlimitedValidTill')->willReturn($validTill);
        $this->orderItem->expects($this->once())->method('setValidTill')->with($validTill);
        $this->orderItem->expects($this->once())->method('getItem')->willReturn($this->item);

        $this->orderItemManager->activateService($this->orderItem, $this->user);
    }

    /**
     * @test
     */
    public function it_should_set_new_valid_till_when_it_has_no_active_service()
    {
        $serviceId = 5;
        $period = 7;

        $this->service->expects($this->any())->method('getIsSingle')->willReturn(false);
        $this->service->expects($this->once())->method('getId')->willReturn($serviceId);

        $this->repo
            ->expects($this->once())
            ->method('findActiveServiceForObjectByUser')
            ->with($serviceId, $this->item, $this->user)
            ->willReturn(null);

        $validTill =  new \DateTime('+'.$period.'days');
        $this->orderItem->expects($this->any())->method('getService')->willReturn($this->service);
        $this->orderItem->expects($this->once())->method('getItem')->willReturn($this->item);
        $this->orderItem->expects($this->once())->method('calculateValidTill')->willReturn($validTill);
        $this->orderItem->expects($this->once())->method('setValidTill')->with($validTill);

        $this->orderItemManager->activateService($this->orderItem, $this->user);
    }

    /**
     * @test
     */
    public function it_should_extend_valid_till_when_it_has_active_service()
    {
        $serviceId = 5;
        $period = 7;

        $activeTill = new \DateTime('+14days');
        $this->activeOrderItem->expects($this->once())->method('getValidTill')->willReturn($activeTill);

        $this->service->expects($this->any())->method('getIsSingle')->willReturn(false);
        $this->service->expects($this->once())->method('getId')->willReturn($serviceId);

        $this->repo
            ->expects($this->once())
            ->method('findActiveServiceForObjectByUser')
            ->with($serviceId, $this->item, $this->user)
            ->willReturn($this->activeOrderItem);

        $this->activeOrderItem->expects($this->once())->method('setExpiredValidTill');

        $dateInterval = new \DateInterval('P'.$period.'D');
        $validTill = $activeTill->add($dateInterval);

        $this->orderItem->expects($this->any())->method('getService')->willReturn($this->service);
        $this->orderItem->expects($this->once())->method('getItem')->willReturn($this->item);
        $this->orderItem->expects($this->once())->method('getPeriod')->willReturn($period);
        $this->orderItem->expects($this->once())->method('setValidTill')->with($validTill);

        $this->em->expects($this->any())->method('persist')->with($this->orderItem);
        $this->em->expects($this->any())->method('flush');

        $this->orderItemManager->activateService($this->orderItem, $this->user);
    }

    /**
     * @test
     */
    public function it_should_not_set_period_and_refund_money_for_single_service()
    {
        $this->service->expects($this->once())->method('getIsSingle')->willReturn(true);
        $this->orderItem->expects($this->exactly(2))->method('getService')->willReturn($this->service);
        $this->orderItem->expects($this->exactly(0))->method('setPeriod');
        $this->walletManager->expects($this->exactly(0))->method('add');

        $this->orderItemManager->suspend($this->orderItem);
    }

    /**
     * @test
     * @dataProvider notRefundAmountData
     */
    public function it_should_not_refund_amount_when_order_is_system_or_amount_is_zero($orderType, $getValidDaysLeftCalls, $getPriceForOnePeriodCalls)
    {
        $this->service->expects($this->once())->method('getIsSingle')->willReturn(false);
        $this->order->expects($this->once())->method('getType')->willReturn($orderType);

        $this->orderItem->expects($this->once())->method('getOrder')->willReturn($this->order);
        $this->orderItem->expects($this->exactly(2))->method('getService')->willReturn($this->service);
        $this->orderItem->expects($this->exactly($getValidDaysLeftCalls))->method('getValidDaysLeft')->willReturn(3);
        $this->orderItem->expects($this->exactly($getPriceForOnePeriodCalls))->method('getPriceForOnePeriod')->willReturn(2000);
        $this->walletManager->expects($this->exactly(0))->method('add');

        $this->orderItemManager->suspend($this->orderItem);
    }

    public function notRefundAmountData()
    {
        return [
            [Order::TYPE_CALCULATOR, 1, 1],
            [Order::TYPE_SYSTEM, 0, 0],
        ];
    }

    /**
     * @test
     */
    public function it_should_refund_amount_when_service_was_ordered_longer_than_for_week()
    {
        $daysLeft = 12;
        $amountForOnePeriod = 2000;
        $periodsToPay = floor($daysLeft / $this->period);
        $amountToPay = $periodsToPay * $amountForOnePeriod;
        $serviceTitle = 'Service title';
        $walletPurpose = sprintf('Service suspend for %s', $serviceTitle);

        $this->service->expects($this->once())->method('getIsSingle')->willReturn(false);
        $this->service->expects($this->once())->method('getTitle')->willReturn($serviceTitle);
        $this->order->expects($this->once())->method('getUser')->willReturn($this->user);
        $this->orderItem->expects($this->exactly(2))->method('getService')->willReturn($this->service);
        $this->orderItem->expects($this->once())->method('getValidDaysLeft')->willReturn($daysLeft);
        $this->orderItem->expects($this->once())->method('getPriceForOnePeriod')->willReturn($amountForOnePeriod);
        $this->orderItem->expects($this->any())->method('getOrder')->willReturn($this->order);
        $this->walletManager->expects($this->once())->method('add')->with($this->user, $amountToPay, $walletPurpose);

        $this->translator
            ->expects($this->once())
            ->method('trans')
            ->with('Service suspend for %serviceTitle%', ['%serviceTitle%' => $serviceTitle])
            ->willReturn($walletPurpose);

        $this->orderItemManager->suspend($this->orderItem);
    }

    /**
     * @test
     */
    public function it_should_not_manage_attributes_when_order_item_has_no_service()
    {
        $this->orderItem->expects($this->once())->method('getService')->willReturn(null);

        $result = $this->orderItemManager->manageAttributes($this->orderItem, true);
        $this->assertNull($result);
    }

    /**
     * @test
     */
    public function it_should_set_highlight_attribute_when_item_implements_highlighted_item_interface()
    {
        $this->service->expects($this->any())->method('getType')->willReturn(Service::TYPE_HIGHLIGHT);
        $this->orderItem->expects($this->any())->method('getService')->willReturn($this->service);
        $this->orderItem->expects($this->any())->method('getItem')->willReturn($this->item);
        $this->item->expects($this->once())->method('setHighlighted')->with(true);

        $this->orderItemManager->manageAttributes($this->orderItem, true);
    }

    /**
     * @test
     */
    public function it_should_return_datetime_object_when_service_is_single()
    {
        $this->service->expects($this->once())->method('getIsSingle')->willReturn(true);
        $this->orderItem->expects($this->once())->method('getService')->willReturn($this->service);
        $this->orderItem->expects($this->once())->method('getUnlimitedValidTill')->willReturn(new \DateTime('+1000years'));

        $result = $this->orderItemManager->calculateValidTill($this->orderItem);
        $this->assertInstanceOf('\DateTime', $result);
    }

    /**
     * @test
     */
    public function it_should_return_datetime_object_when_service_is_not_single_and_has_no_activated_service()
    {
        $validTill = new \DateTime();
        $this->service->expects($this->once())->method('getIsSingle')->willReturn(false);
        $this->orderItem->expects($this->once())->method('getService')->willReturn($this->service);
        $this->orderItem->expects($this->any())->method('getPeriod')->willReturn(7);
        $this->orderItem->expects($this->any())->method('calculateValidTill')->willReturn($validTill);

        $result = $this->orderItemManager->calculateValidTill($this->orderItem);
        $this->assertInstanceOf('\DateTime', $result);
    }

    /**
     * @test
     */
    public function it_should_return_datetime_object_when_service_is_not_single_and_has_activated_service()
    {
        $validTill = new \DateTime('+3days');
        $this->service->expects($this->once())->method('getIsSingle')->willReturn(false);
        $this->orderItem->expects($this->once())->method('getService')->willReturn($this->service);
        $this->orderItem->expects($this->once())->method('getPeriod')->willReturn(7);
        $this->activeOrderItem->expects($this->once())->method('getValidTill')->willReturn($validTill);

        $result = $this->orderItemManager->calculateValidTill($this->orderItem, $this->activeOrderItem);
        $this->assertInstanceOf('\DateTime', $result);
    }

    /**
     * @test
     */
    public function it_should_set_as_paid_and_activate_services()
    {
        $this->order->expects($this->once())->method('getUser')->willReturn($this->user);
        $this->orderItem->expects($this->once())->method('setStatus')->with(RDWOrderStatus::STATUS_PAID);
        $this->orderItem->expects($this->once())->method('getOrder')->willReturn($this->order);

        $this->orderItemManager->setAsPaid($this->orderItem);
    }

    public function jobStatuses()
    {
        return [
            [Job::STATUS_INACTIVE, 0],
            [Job::STATUS_WAITING, 0],
            [Job::STATUS_PUBLISHED, 1],
        ];
    }

    /**
     * @test
     * @dataProvider jobStatuses
     * @group dispatcher
     */
    public function it_should_dispatch_job_publish_event_depending_on_job_status($status, $times)
    {
        $this->service->expects($this->any())->method('getIsSingle')->willReturn(true);

        $validTill = new \DateTime('+1000years');
        $this->orderItem->expects($this->any())->method('getService')->willReturn($this->service);
        $this->orderItem->expects($this->once())->method('getUnlimitedValidTill')->willReturn($validTill);
        $this->orderItem->expects($this->once())->method('setValidTill')->with($validTill);

        $this->em->expects($this->once())->method('persist')->with($this->orderItem);
        $this->em->expects($this->once())->method('flush');

        $this->orderItem->expects($this->once())->method('hasItem')->willReturn(true);
        $this->service->expects($this->once())->method('isJobPublishing')->willReturn(true);
        $this->orderItem->expects($this->any())->method('getItem')->willReturn($this->item);
        $this->item->expects($this->once())->method('getStatus')->willReturn($status);

        $this->dispatcher
            ->expects($this->exactly($times))
            ->method('dispatch')
            ->with(
                $this->equalTo(RDWJobEvents::JOB_PUBLISH),
                $this->equalTo(new JobEvent($this->item))
            );

        $this->orderItemManager->activateService($this->orderItem, $this->user);
    }
}
