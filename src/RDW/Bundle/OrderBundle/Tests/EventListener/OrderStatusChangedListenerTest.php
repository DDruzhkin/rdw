<?php

namespace RDW\Bundle\OrderBundle\Tests\EventListener;

use RDW\Bundle\OrderBundle\Entity\Order;
use RDW\Bundle\OrderBundle\Event\OrderStatusEvent;
use RDW\Bundle\OrderBundle\EventListener\OrderStatusChangedListener;
use RDW\Bundle\OrderBundle\RDWOrderEvents;
use RDW\Bundle\OrderBundle\RDWOrderStatus;
use RDW\Bundle\OrderBundle\Service\OrderManager;
use RDW\Bundle\MailerBundle\Service\MailerManager;
use RDW\Bundle\PaymentBundle\Service\PaymentManager;
use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * Class OrderStatusChangedListenerTest
 *
 * @package RDW\Bundle\OrderBundle\Tests\EventListener
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class OrderStatusChangedListenerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var OrderManager
     */
    private $orderManager;

    /**
     * @var MailerManager
     */
    private $mailerManager;

    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    private $order;
    private $orderStatusEvent;

    /**
     * @var OrderStatusChangedListener
     */
    private $listener;

    /**
     * set up
     */
    public function setUp()
    {
        $this->orderManager = $this->getMockBuilder('RDW\Bundle\OrderBundle\Service\OrderManager')->disableOriginalConstructor()->getMock();
        $this->mailerManager = $this->getMockBuilder('RDW\Bundle\MailerBundle\Service\MailerManager')->disableOriginalConstructor()->getMock();
        $this->paymentManager = $this->getMockBuilder('RDW\Bundle\PaymentBundle\Service\PaymentManager')->disableOriginalConstructor()->getMock();
        $this->eventDispatcher = $this->getMockBuilder('Symfony\Component\EventDispatcher\EventDispatcher')->disableOriginalConstructor()->getMock();

        $this->order = $this->getMock('RDW\Bundle\OrderBundle\Entity\Order');
        $this->orderStatusEvent = $this->getMockBuilder('RDW\Bundle\OrderBundle\Event\OrderStatusEvent')->disableOriginalConstructor()->getMock();

        $this->listener = new OrderStatusChangedListener($this->orderManager, $this->mailerManager, $this->paymentManager, $this->eventDispatcher);
    }

    /**
     * @test
     */
    public function it_should_return_subscribed_events_properly()
    {
        $events = [
            RDWOrderEvents::ORDER_STATUS_PAID,
            RDWOrderEvents::ORDER_STATUS_CANCELLED,
        ];

        $this->assertEmpty(array_diff($events, array_keys(OrderStatusChangedListener::getSubscribedEvents())));
    }

    /**
     * @test
     */
    public function it_should_not_cancel_order_when_status_is_not_changeable()
    {
        $order = new Order();
        $event = new OrderStatusEvent($order, 'bad bad status');

        $this->assertFalse($this->listener->onOrderCancel($event));
    }

    /**
     * @test
     */
    public function it_should_cancel_order_properly()
    {
        $order = new Order();
        $order->setStatus(RDWOrderStatus::STATUS_PENDING);

        $event = new OrderStatusEvent($order, RDWOrderStatus::STATUS_CANCELLED);

        $this->listener->onOrderCancel($event);

        $this->assertEquals(RDWOrderStatus::STATUS_CANCELLED, $event->getOrder()->getStatus());
    }

    /**
     * @test
     */
    public function it_should_not_mark_as_paid_order_when_status_is_already_paid()
    {
        $order = new Order();
        $order->setPaidAt(new \DateTime());
        $order->setStatus(RDWOrderStatus::STATUS_PAID);

        $event = new OrderStatusEvent($order, RDWOrderStatus::STATUS_PAID);

        $this->assertFalse($this->listener->onOrderPay($event));
    }

    /**
     * @test
     */
    public function it_should_not_mark_as_paid_order_when_status_is_not_changeable()
    {
        $order = new Order();
        $order->setStatus(RDWOrderStatus::STATUS_CANCELLED);

        $event = new OrderStatusEvent($order, RDWOrderStatus::STATUS_PAID);

        $this->assertFalse($this->listener->onOrderPay($event));
    }
}
