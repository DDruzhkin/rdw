<?php

namespace RDW\Bundle\OrderBundle\Tests\Event;

use RDW\Bundle\OrderBundle\Entity\Order;
use RDW\Bundle\OrderBundle\Event\OrderStatusEvent;
use RDW\Bundle\OrderBundle\RDWOrderStatus;

/**
 * Class OrderStatusEventTest
 *
 * @package RDW\Bundle\OrderBundle\Tests\Event
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class OrderStatusEventTest extends OrderEventTest
{
    /**
     * @var OrderStatusEvent
     */
    private $orderStatusEvent;

    /**
     * @var string
     */
    private $status;

    /**
     * set up
     */
    public function setUp()
    {
        parent::setUp();

        $this->status = RDWOrderStatus::STATUS_PAID;
        $this->orderStatusEvent = new OrderStatusEvent($this->order, $this->status);
    }

    /**
     * @test
     */
    public function it_should_return_status_properly()
    {
        $this->assertEquals($this->status, $this->orderStatusEvent->getStatus());
    }

    /**
     * @test
     */
    public function it_should_be_instance_of_order_event()
    {
        $this->assertInstanceOf(get_class($this->orderEvent), $this->orderStatusEvent);
    }
}
