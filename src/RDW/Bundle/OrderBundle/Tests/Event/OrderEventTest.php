<?php

namespace RDW\Bundle\OrderBundle\Tests\Event;


use RDW\Bundle\OrderBundle\Event\OrderEvent;
use RDW\Bundle\OrderBundle\Entity\Order;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class OrderEventTest
 *
 * @package RDW\Bundle\OrderBundle\Tests\Event
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class OrderEventTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Order
     */
    protected $order;

    /**
     * @var OrderEvent
     */
    protected $orderEvent;

    /**
     * set up
     */
    public function setUp()
    {
        $this->order = $this->getMock('RDW\Bundle\OrderBundle\Entity\Order');
        $this->orderEvent = new OrderEvent($this->order);
    }

    /**
     * @test
     */
    public function it_should_return_order_properly()
    {
        $this->assertEquals($this->order, $this->orderEvent->getOrder());
    }

    /**
     * @test
     */
    public function it_should_return_redirect_reponse_properly()
    {
        $url = 'http://www.nebijokit.lt';
        $response = new RedirectResponse($url);

        $event = new OrderEvent($this->order);
        $event->setResponse($response);

        $this->orderEvent->setResponse($response);

        $this->assertEquals($event->getResponse(), $this->orderEvent->getResponse());
    }

    /**
     * @test
     */
    public function it_should_return_reponse_properly()
    {
        $response = new Response('What?');

        $event = new OrderEvent($this->order);
        $event->setResponse($response);

        $this->orderEvent->setResponse($response);

        $this->assertEquals($event->getResponse(), $this->orderEvent->getResponse());
    }
}
