<?php


namespace RDW\Bundle\OrderBundle\Model;
use RDW\Bundle\OrderBundle\Entity\Order;

/**
 * Class SplitOrder
 *
 * @package RDW\Bundle\OrderBundle\Model
 */
class SplitOrder
{
    /**
     * @var Order
     */
    protected $orderToPay;

    /**
     * @var Order
     */
    protected $payedOrder;

    /**
     * @param Order $orderToPay
     * @param Order $payedOrder
     */
    public function __construct(Order $orderToPay, Order $payedOrder = null)
    {
        $this->orderToPay = $orderToPay;
        $this->payedOrder = $payedOrder;
    }

    /**
     * @return \RDW\Bundle\OrderBundle\Entity\Order
     */
    public function getPayedOrder()
    {
        return $this->payedOrder;
    }

    /**
     * @param \RDW\Bundle\OrderBundle\Entity\Order $payedOrder
     *
     * @return $this
     */
    public function setPayedOrder($payedOrder = null)
    {
        $this->payedOrder = $payedOrder;

        return $this;
    }

    /**
     * @return \RDW\Bundle\OrderBundle\Entity\Order
     */
    public function getOrderToPay()
    {
        return $this->orderToPay;
    }

    /**
     * @param \RDW\Bundle\OrderBundle\Entity\Order $orderToPay
     *
     * @return $this
     */
    public function setOrderToPay($orderToPay)
    {
        $this->orderToPay = $orderToPay;

        return $this;
    }
}
