<?php

namespace RDW\Bundle\OrderBundle;

/**
 * Class RDWOrderEvents
 * Contains all events thrown in the RDWOrderBundle
 *
 * @package RDW\Bundle\OrderBundle
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
final class RDWOrderEvents
{
    /**
     * This event occurs then order submitted successfully and ready for payment
     */
    const SUBMIT_SUCCESS = 'rdw_order.event.submit_success';

    /**
     * This event occurs then order created successfully and ready for payment
     */
    const CREATE_SUCCESS = 'rdw_order.event.create_success';

    /**
     * This event occurs then order submitted successfully and ready for payment
     */
    const MATRIX_SUBMITTED_SUCCESS = 'rdw_order.event.matrix_submitted_success';

    /**
     * This event occurs then order  status has been changed to cancelled
     */
    const ORDER_STATUS_CANCELLED = 'rdw_order.event.order_status_cancelled';

    /**
     * This event occurs then order  status has been changed to paid
     */
    const ORDER_STATUS_PAID = 'rdw_order.event.order_status_paid';
}
