<?php

namespace RDW\Bundle\OrderBundle\Controller;

use RDW\Bundle\OrderBundle\Entity\Order;
use RDW\Bundle\OrderBundle\Event\OrderEvent;
use RDW\Bundle\OrderBundle\Event\OrderStatusEvent;
use RDW\Bundle\OrderBundle\RDWOrderEvents;
use RDW\Bundle\OrderBundle\RDWOrderStatus;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class OrderController
 *
 * @package RDW\Bundle\OrderBundle\Controller
 *
 * @Route("/order")
 */
class OrderController extends Controller
{
    /**
     * @param Request $request
     * @param Order   $order
     *
     * @Route("/submit/{id}", name="rdw_order.order.submit", requirements={"id" = "\d+"})
     * @Method({"GET", "POST"})
     * @Template()
     *
     * @throws NotFoundHttpException
     *
     * @return Response
     */
    public function submitAction(Request $request, Order $order)
    {
        $this->isGrantedLocal($order);

        $form = $this->createForm('rdw_order_form', $order);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new OrderEvent($order);
            $this->get('event_dispatcher')->dispatch(RDWOrderEvents::SUBMIT_SUCCESS, $event);

            switch ($order->getPaymentType()) {
                case Order::PAYMENT_TRANSFER:
                    $url = 'rdw_payment.transfer.pay';
                    break;

                case Order::PAYMENT_GUARANTEE:
                    $url = 'rdw_payment.transfer.pay';
                    break;

                case Order::PAYMENT_SMS:
                    $url = 'rdw_sms.sms.pay';
                    break;

                default:
                    $url = 'rdw_payment.payment.pay';
            }

            return $this->redirect($this->generateUrl($url, ['id' => $order->getId()]));
        }

        return [
            'order' => $order,
            'form'  => $form->createView(),
            'defaultPeriod' => $this->container->getParameter('rdw_order.default_period'),
        ];
    }

    /**
     * @param RegisteredUser $user
     *
     * @Route("/list/{id}", name="rdw_order.order.list", requirements={"id" = "\d+"})
     * @Method({"GET"})
     * @Template()
     *
     * @return Response
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function listAction(RegisteredUser $user)
    {
        if (! $user
            || (
                $user->getId() != $this->getUser()->getId()
                && ! $user->isUserManager($this->getUser())
            )
        ) {
            throw new AccessDeniedException();
        }

        $userServices = $this->get('rdw_order.order.repository')->getUserActiveServices($user);
        $orders = $this->container
            ->get('rdw_order.order.repository')
            ->createQueryBuilder('o')
            ->andWhere('o.user = :user')
            ->andWhere('o.type != :type_system')
            ->andWhere('o.status != :status_cancelled')
            ->setParameter('user', $user)
            ->setParameter('type_system', Order::TYPE_SYSTEM)
            ->setParameter('status_cancelled', RDWOrderStatus::STATUS_CANCELLED)
            ->orderBy('o.createdAt', 'DESC')
            ->getQuery()
            ->getResult();

        return [
            'orders' => $orders,
            'userServices' => $userServices,
            'user' => $user,
        ];
    }

    /**
     * @param Order $order
     *
     * @Route("/view/{id}", name="rdw_order.order.view", requirements={"id" = "\d+"})
     * @Method({"GET"})
     * @Template()
     *
     * @return array
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function viewAction(Order $order)
    {
        $this->isGrantedLocal($order);

        return [
            'order' => $order,
        ];
    }

    /**
     * @param Order $order
     *
     * @Route("/reset-period/{id}", name="rdw_order.order.reset_period", requirements={"id" = "\d+"})
     * @Method({"GET"})
     *
     * @return RedirectResponse
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function resetPeriodAction(Order $order)
    {
        $this->isGrantedLocal($order);

        $orderManager = $this->container->get('rdw_order.service.order_manager');
        $orderManager->recalculatePeriod($order, $this->container->getParameter('rdw_order.default_period'));

        return $this->redirect($this->generateUrl('rdw_order.order.submit', ['id' => $order->getId()]));
    }

    /**
     * @param Order  $order
     * @param string $status
     *
     * @Route(
     *      "/change-status/{id}/{status}",
     *      name="rdw_order.order.change_status",
     *      requirements={"id" = "\d+", "status" = "paid|cancelled"}
     * )
     * @Method({"GET"})
     *
     * @return RedirectResponse
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function changeStatusAction(Order $order, $status)
    {
        if (! ($this->isOwner($order) && $this->getUser()->hasRole('ROLE_MANAGER'))) {
            throw new AccessDeniedException();
        }

        $event = new OrderStatusEvent($order, $status);
        $this->container->get('event_dispatcher')->dispatch('rdw_order.event.order_status_'.$status, $event);

        $this->container->get('braincrafted_bootstrap.flash')->success('Order status has been changed');

        return $this->redirect($this->generateUrl('rdw_order.order.view', ['id' => $order->getId()]));
    }

    /**
     * @Template()
     * @Method({"GET"})
     *
     * @return array
     */
    public function activeServicesBlockAction()
    {
        $limit = $this->container->getParameter('rdw_order.active_services_block_limit');
        $services = $this->get('rdw_order.order.repository')->getUserActiveServices($this->getUser(), $limit);

        return [
            'services' => $services,
        ];
    }

    /**
     * @param Order $order
     *
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    private function isGrantedLocal(Order $order)
    {
        if (! $this->isOwner($order)) {
            throw new AccessDeniedException();
        }
    }

    /**
     * @param Order $order
     *
     * @return bool
     */
    private function isOwner(Order $order)
    {
        /** @var RegisteredUser $user */
        $user = $this->getUser();

        if ($user instanceof RegisteredUser
            && (
                $this->container->get('security.context')->isGranted('ROLE_EMPLOYER')
                || $this->container->get('security.context')->isGranted('ROLE_EMPLOYEE')
            )
            && (
                $user->getId() == $order->getUser()->getId() // if owner
                || $order->getUser()->isUserManager($user) // is manager of job owner
            )
        ) {
            return true;
        } else {
            return false;
        }
    }
}
