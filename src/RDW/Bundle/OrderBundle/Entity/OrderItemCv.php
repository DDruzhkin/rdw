<?php

namespace RDW\Bundle\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\CvBundle\Entity\Cv;
use Doctrine\ORM\EntityNotFoundException;

/**
 * Class OrderItemCv
 *
 * @package RDW\Bundle\OrderBundle\Entity
 *
 * @ORM\Entity(repositoryClass="RDW\Bundle\OrderBundle\Repository\OrderItemCvRepository")
 */
class OrderItemCv extends OrderItem
{
    /**
     * @var Cv
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\CvBundle\Entity\Cv", inversedBy="services")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     **/
    private $item;

    /**
     * @return Cv
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param Cv $item
     *
     * @return $this
     */
    public function setItem(Cv $item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasItem()
    {
        try {
            $cv = $this->item;
            if (isset($cv) && ! $cv->isDeleted()) {
                return true;
            }

            return false;
        } catch (EntityNotFoundException $e) {
            return false;
        }
    }
}
