<?php

namespace RDW\Bundle\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\JobBundle\Entity\Job;
use Doctrine\ORM\EntityNotFoundException;

/**
 * Class OrderItemJob
 *
 * @package RDW\Bundle\OrderBundle\Entity
 *
 * @ORM\Entity(repositoryClass="RDW\Bundle\OrderBundle\Repository\OrderItemJobRepository")
 */
class OrderItemJob extends OrderItem
{
    /**
     * @var Job
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\JobBundle\Entity\Job", inversedBy="services")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     **/
    private $item;

    /**
     * @return Job
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param Job $item
     *
     * @return $this
     */
    public function setItem(Job $item)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasItem()
    {
        try {
            $job = $this->item;
            if (isset($job) && ! $job->isDeleted()) {
                return true;
            }

            return false;
        } catch (EntityNotFoundException $e) {
            return false;
        }
    }
}
