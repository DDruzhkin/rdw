<?php

namespace RDW\Bundle\OrderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\AppBundle\Entity\ActItem;
use RDW\Bundle\ServiceBundle\Entity\Service;
use RDW\Bundle\SmsBundle\Entity\SmsCode;
use RDW\Bundle\OrderBundle\RDWOrderStatus;

/**
 * Order Item
 *
 * @ORM\Table(name="order_items")
 * @ORM\Entity
 *
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"cv" = "OrderItemCv", "job" = "OrderItemJob"})
 */
class OrderItem
{
    const DAYS_PER_PERIOD = 7;

    const TYPE_CV = 'cv';
    const TYPE_JOB = 'job';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\OrderBundle\Entity\Order", inversedBy="items")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     **/
    private $order;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", name="paid_at", nullable=true)
     */
    private $paidAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", name="valid_till", nullable=true)
     */
    private $validTill;

    /**
     * @var Service
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\ServiceBundle\Entity\Service")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id")
     **/
    private $service;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float")
     */
    private $price;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $period;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $title;

    /**
     * @var SmsCode
     *
     * @ORM\OneToOne(targetEntity="\RDW\Bundle\SmsBundle\Entity\SmsCode")
     * @ORM\JoinColumn(name="sms_code_id", referencedColumnName="id")
     */
    private $smsCode;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $status;

    /**
     * @var ArrayCollection|ActItem[]
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\AppBundle\Entity\ActItem", mappedBy="orderItem")
     */
    private $actItems;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->actItems = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \RDW\Bundle\OrderBundle\Entity\Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param \RDW\Bundle\OrderBundle\Entity\Order $order
     *
     * @return $this
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return float
     */
    public function getPriceWithoutVat()
    {
        return $this->getPrice() - $this->getPriceVat();
    }

    /**
     * @return float
     */
    public function getPricePerUnitWithoutVat()
    {
        return $this->getPriceWithoutVat() / $this->getUnitSize();
    }

    /**
     * @return float
     */
    public function getPriceVat()
    {
        return $this->price * ($this->order->getVat()) / (100 + $this->order->getVat());
    }

    /**
     * @return float
     */
    public function getPricePerUnit()
    {
        return $this->price / $this->getUnitSize();
    }

    /**
     * @param float $price
     *
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return \RDW\Bundle\ServiceBundle\Entity\Service
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @param \RDW\Bundle\ServiceBundle\Entity\Service $service
     *
     * @return $this
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getValidTill()
    {
        return $this->validTill;
    }

    /**
     * @param \DateTime $validTill
     *
     * @return $this
     */
    public function setValidTill($validTill)
    {
        $this->validTill = $validTill;

        return $this;
    }

    /**
     * @return $this
     */
    public function setExpiredValidTill()
    {
        $this->validTill = new \DateTime('-1day');

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUnlimitedValidTill()
    {
        return new \DateTime('+1000years');
    }

    /**
     * @return \DateTime
     */
    public function calculateValidTill()
    {
        $when = ($this->period) ? sprintf('+%ddays', $this->period) : 'now';

        return new \DateTime($when);
    }

    public function calculatePeriod()
    {
        $interval = $this->getValidTill()->diff(new \DateTime());

        return ceil($interval->format('%d') / 7);
    }

    /**
     * @return int
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * @param int $period
     *
     * @return $this
     */
    public function setPeriod($period)
    {
        $this->period = $period;

        return $this;
    }

    /**
     * @return int
     */
    public function getUnitSize()
    {
        return ($this->isSingle()) ? 1 : $this->period;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return int
     */
    public function getValidDaysLeft()
    {
        $currentDate = new \DateTime();

        return ($currentDate < $this->getValidTill()) ? $this->getValidTill()->diff($currentDate)->days : -1;
    }

    /**
     * @param int $period
     *
     * @return float
     */
    public function getPriceForOnePeriod($period = self::DAYS_PER_PERIOD)
    {
        $payedPrice = $this->getPrice();
        $priceForPeriod = $payedPrice / ($this->getPeriod() / $period);

        return $priceForPeriod;
    }

    /**
     * @return SmsCode
     */
    public function getSmsCode()
    {
        return $this->smsCode;
    }

    /**
     * @param SmsCode $smsCode
     */
    public function setSmsCode($smsCode = null)
    {
        $this->smsCode = $smsCode;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPaid()
    {
        return RDWOrderStatus::STATUS_PAID == $this->getStatus();
    }

    /**
     * @return \DateTime
     */
    public function getPaidAt()
    {
        return $this->paidAt;
    }

    /**
     * @param \DateTime $paidAt
     *
     * @return $this
     */
    public function setPaidAt(\DateTime $paidAt)
    {
        $this->paidAt = $paidAt;

        return $this;
    }

    /**
     * @return ArrayCollection|\RDW\Bundle\AppBundle\Entity\ActItem[]
     */
    public function getActItems()
    {
        return $this->actItems;
    }

    /**
     * @param ArrayCollection|\RDW\Bundle\AppBundle\Entity\ActItem[] $actItems
     *
     * @return OrderItem
     */
    public function setActItems($actItems)
    {
        $this->actItems = $actItems;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSingle()
    {
        return $this->service->getIsSingle();
    }
}
