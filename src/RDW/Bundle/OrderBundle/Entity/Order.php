<?php

namespace RDW\Bundle\OrderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use RDW\Bundle\OrderBundle\RDWOrderStatus;
use RDW\Bundle\PaymentBundle\Model\OrderInterface;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\Validator\Constraints as Assert;
use RDW\Bundle\PaymentBundle\Entity\Payment;

/**
 * Main order entity class
 *
 * @ORM\Entity(repositoryClass="RDW\Bundle\OrderBundle\Repository\OrderRepository")
 * @ORM\Table(name="orders")
 */
class Order implements OrderInterface
{
    const PAYMENT_CARD = 'card';
    const PAYMENT_TRANSFER = 'transfer';
    const PAYMENT_GUARANTEE = 'guarantee';
    const PAYMENT_WALLET = 'wallet';
    const PAYMENT_SMS = 'sms';

    const TYPE_CALCULATOR = 'calculator';
    const TYPE_CREDIT = 'credit'; // rdw.by account credit addition
    const TYPE_SYSTEM = 'system';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     *
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="paid_at", type="datetime", nullable=true)
     */
    private $paidAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     *
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=100)
     */
    private $currency;

    /**
     * @var RegisteredUser
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\RegisteredUser")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     **/
    private $user;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\OrderBundle\Entity\OrderItem", mappedBy="order", cascade={"persist"})
     */
    private $items;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $total;

    /**
     * @var string
     * @ORM\Column(type="string", name="payment_type", nullable=true)
     *
     * @Assert\NotBlank(message="Select payment type")
     */
    private $paymentType;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\PaymentBundle\Entity\Payment", mappedBy="order")
     */
    private $payments;

    /**
     * @var Payment
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\PaymentBundle\Entity\Payment")
     * @ORM\JoinColumn(name="payment_id", referencedColumnName="id")
     */
    private $successfulPayment;

    /**
     * @var int
     * @ORM\Column(type="integer", name="invoice_number", nullable=true)
     */
    private $invoiceNumber;

    /**
     * @var int
     *
     * @ORM\Column(name="vat", type="integer")
     */
    private $vat;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new ArrayCollection();
        $this->payments = new ArrayCollection();
        $this->status = RDWOrderStatus::STATUS_SUBMITTED;
        $this->paymentType = null;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \RDW\Bundle\UserBundle\Entity\RegisteredUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param \RDW\Bundle\UserBundle\Entity\RegisteredUser $user
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        $this->setStatusForItems($status);

        return $this;
    }

    /**
     * @return int
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param int $vat
     *
     * @return Order
     */
    public function setVat($vat)
    {
        $this->vat = $vat;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return ArrayCollection|OrderItem[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param OrderItem $item
     *
     * @return $this
     */
    public function addItem(OrderItem $item)
    {
        $this->items->add($item);

        return $this;
    }

    /**
     * @param OrderItem $item
     *
     * @return $this
     */
    public function removeItem(OrderItem $item)
    {
        $this->items->removeElement($item);

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPaidAt()
    {
        return $this->paidAt;
    }

    /**
     * @param \DateTime $paidAt
     *
     * @return $this
     */
    public function setPaidAt($paidAt)
    {
        $this->paidAt = $paidAt;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPaid()
    {
        return ($this->paidAt && RDWOrderStatus::STATUS_PAID == $this->getStatus());
    }

    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     *
     * @return $this
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalWithoutVat()
    {
        return $this->total - $this->getTotalVat();
    }

    /**
     * @return float
     */
    public function getTotalVat()
    {
        return $this->total * ($this->vat) / (100 + $this->vat);
    }

    /**
     * @return int
     */
    public function countTotal()
    {
        $total  = 0;

        foreach ($this->getItems() as $item) {
            $total += $item->getPrice();
        }

        return $total;
    }

    /**
     * @return string
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * @param string $paymentType
     *
     * @return $this
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    /**
     * @return bool
     */
    public function isTypeCalculator()
    {
        return (self::TYPE_CALCULATOR == $this->getType());
    }

    public static function getPaymentTypes(RegisteredUser $user, $type = null)
    {
        $paymentTypes = [];
        $paymentTypes[self::PAYMENT_CARD] = 'Credit card';

        if ($user->isTypeEmployer()) {
            $paymentTypes[self::PAYMENT_TRANSFER] = 'Bank transfer';
            $paymentTypes[self::PAYMENT_GUARANTEE] = 'By guarantee';
        } elseif (self::TYPE_CREDIT != $type) {
            $paymentTypes[self::PAYMENT_SMS] = 'Via SMS';
        }

        return $paymentTypes;
    }

    /**
     * @return array
     */
    public static function getPaymentTypesForManager()
    {
        $paymentTypes[self::PAYMENT_TRANSFER] = 'Bank transfer';
        $paymentTypes[self::PAYMENT_GUARANTEE] = 'By guarantee';

        return $paymentTypes;
    }

    /**
     * @return array
     */
    public static function getPaymentTypesToAddMoneyToWallet()
    {
        return [
            self::PAYMENT_TRANSFER,
            self::PAYMENT_CARD,
        ];
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Payment
     */
    public function getSuccessfulPayment()
    {
        return $this->successfulPayment;
    }

    /**
     * @param Payment $successfulPayment
     *
     * @return $this
     */
    public function setSuccessfulPayment($successfulPayment)
    {
        $this->successfulPayment = $successfulPayment;

        return $this;
    }

    /**
     * @return int
     */
    public function getInvoiceNumber()
    {
        return $this->invoiceNumber;
    }

    /**
     * @param int $invoiceNumber
     *
     * @return $this
     */
    public function setInvoiceNumber($invoiceNumber)
    {
        $this->invoiceNumber = $invoiceNumber;

        return $this;
    }

    /**
     * @return bool
     */
    public function isGuarantee()
    {
        return (self::PAYMENT_GUARANTEE == $this->getPaymentType());
    }

    /**
     * @return bool
     */
    public function isTransfer()
    {
        return (self::PAYMENT_TRANSFER == $this->getPaymentType());
    }

    /**
     * @return bool
     */
    public function showMarkAsPaidButton()
    {
        return ( ! $this->isPaid() && ($this->isGuarantee() || $this->isTransfer()) && $this->isPayable());
    }

    /**
     * @return bool
     */
    public function showCancelButton()
    {
        return ($this->isStatusChangeable(RDWOrderStatus::STATUS_CANCELLED));
    }

    /**
     * @param string $status
     *
     * @return bool
     */
    public function isStatusChangeable($status)
    {
        // new status => [current statuses array, from whom order can be changed to new status]
        $rules = [
            RDWOrderStatus::STATUS_SUBMITTED => [],
            RDWOrderStatus::STATUS_PAID =>      [RDWOrderStatus::STATUS_SUBMITTED, RDWOrderStatus::STATUS_PENDING],
            RDWOrderStatus::STATUS_CANCELLED => [RDWOrderStatus::STATUS_SUBMITTED, RDWOrderStatus::STATUS_PENDING],
            RDWOrderStatus::STATUS_PENDING =>   [RDWOrderStatus::STATUS_SUBMITTED],
        ];

        // rules for new status exists and current status is between allowed statuses to change from
        return (array_key_exists($status, $rules) && in_array($this->getStatus(), $rules[$status]));
    }

    /**
     * @return bool
     */
    public function isPayable()
    {
        return $this->isStatusChangeable(RDWOrderStatus::STATUS_PAID);
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     *
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Is user owner of this order?
     *
     * @param RegisteredUser $user
     *
     * @return bool
     */
    public function isOwner(RegisteredUser $user)
    {
        $owner = $this->getUser();

        return ($owner->getId() == $user->getId());
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->getUser()->getEmail();
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->getUser()->getPhone();
    }

    /**
     * @return ArrayCollection|Payment[]
     */
    public function getPayments()
    {
        return $this->payments;
    }

    /**
     * @param ArrayCollection $payments
     *
     * @return $this
     */
    public function setPayments($payments)
    {
        $this->payments = $payments;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasItemsWithoutSmsCode()
    {
        foreach ($this->getItems() as $item) {
            if (!$item->getSmsCode()) {
                return true;
            }
        }

        return false;
    }

    public function hasFreeItems()
    {
        foreach ($this->getItems() as $item) {
            if ($item->getPrice() === 0) {
                return true;
            }
        }

        return false;
    }

    public function shouldBeSplitted($balance)
    {
        return (($balance > 0 && $balance < $this->getTotal()) || $this->hasFreeItems());
    }

    /**
     * Check if all order items are valid period for payments via sms
     *
     * @param int $period
     *
     * @return bool
     */
    public function isItemsPeriodValidForSms($period)
    {
        foreach ($this->getItems() as $item) {
            if ($item->getPeriod() > $period) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isPaidBySms()
    {
        return self::PAYMENT_SMS == $this->getPaymentType();
    }

    /**
     * Set order status to paid if all of its items are paid
     *
     * @return bool|null
     */
    public function setFullyPaid()
    {
        foreach ($this->getItems() as $item) {
            if (RDWOrderStatus::STATUS_PAID != $item->getStatus()) {
                return null;
            }
        }

        $this->setStatus(RDWOrderStatus::STATUS_PAID);
        $this->setPaidAt(new \DateTime());
    }

    /**
     * @return bool
     */
    public function isPeriodVisible()
    {
        if (1 == $this->getItems()->count() && $this->getItems()->first()->getService()->isMoneyToBalance()) {
            return false;
        }

        return true;
    }

    public function hasActs()
    {
        foreach ($this->items as $orderItem) {
            if (count($orderItem->getActItems()) > 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $status
     */
    private function setStatusForItems($status)
    {
        foreach ($this->getItems() as $item) {
            $item->setStatus($status);
        }
    }
}
