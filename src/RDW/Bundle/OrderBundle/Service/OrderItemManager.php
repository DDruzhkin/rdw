<?php

namespace RDW\Bundle\OrderBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Event\JobEvent;
use RDW\Bundle\JobBundle\RDWJobEvents;
use RDW\Bundle\OrderBundle\Entity\OrderItemJob;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\ServiceBundle\Entity\HighlightedItemInterface;
use RDW\Bundle\ServiceBundle\Entity\TopItemInterface;
use RDW\Bundle\OrderBundle\Entity\Order;
use RDW\Bundle\OrderBundle\Entity\OrderItem;
use RDW\Bundle\OrderBundle\Model\SplitOrder;
use RDW\Bundle\OrderBundle\RDWOrderStatus;
use RDW\Bundle\OrderBundle\Repository\OrderRepository;
use RDW\Bundle\ServiceBundle\Entity\Service;
use RDW\Bundle\UserBundle\Entity\User;
use RDW\Bundle\WalletBundle\Service\WalletManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class OrderItemManager
 *
 * @package RDW\Bundle\OrderBundle\Service
 */
class OrderItemManager
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $objectManager;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var OrderManager
     */
    private $orderManager;

    /**
     * @var WalletManager
     */
    private $walletManager;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var int
     */
    private $period;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @param ObjectManager $objectManager
     * @param OrderRepository $orderRepository
     * @param OrderManager $orderManager
     * @param WalletManager $walletManager
     * @param TranslatorInterface $translator
     * @param int $period
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(
        ObjectManager $objectManager,
        OrderRepository $orderRepository,
        OrderManager $orderManager,
        WalletManager $walletManager,
        TranslatorInterface $translator,
        $period,
        EventDispatcherInterface $dispatcher
    ) {
        $this->objectManager = $objectManager;
        $this->orderRepository = $orderRepository;
        $this->orderManager = $orderManager;
        $this->walletManager = $walletManager;
        $this->translator = $translator;
        $this->period = $period;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param array $orderItems
     * @param User $user
     */
    public function activateServicesForUser($orderItems, User $user)
    {
        foreach ($orderItems as $item) {
            $this->activateService($item, $user);
            $this->manageAttributes($item, true);
        }
    }

    /**
     * @param Order $order
     */
    public function setPaidAtForAll(Order $order)
    {
        foreach ($order->getItems() as $item) {
            $item->setPaidAt($order->getPaidAt());
            $this->update($item);
        }
    }

    /**
     * @param OrderItem $orderItem
     * @param RegisteredUser $user
     *
     * @todo Refactor params. User can be get by $orderItem->getOrder()->getUser()
     *
     * @return OrderItem|null
     */
    public function activateService(OrderItem $orderItem, RegisteredUser $user)
    {
        $service = $orderItem->getService();

        if (!$service instanceof Service) {
            return null;
        }

        /** @var OrderItem $activeServiceForItem */
        $activeServiceForItem = $this
            ->orderRepository
            ->findActiveServiceForObjectByUser($service->getId(), $orderItem->getItem(), $user);

        if (!$service->getIsSingle() && $activeServiceForItem) {
            $activeServiceForItem->setExpiredValidTill();
            $this->update($activeServiceForItem);
        }

        $validTill = $this->calculateValidTill($orderItem, $activeServiceForItem);
        $orderItem->setValidTill($validTill);
        $this->update($orderItem);

        if ($orderItem instanceof OrderItemJob && $orderItem->hasItem() && $orderItem->getService()->isJobPublishing()) {
            $item = $orderItem->getItem();
            if (Job::STATUS_PUBLISHED === $item->getStatus()) {
                $this->dispatcher->dispatch(RDWJobEvents::JOB_PUBLISH, new JobEvent($item));
            }
        }

        return $orderItem;
    }

    /**
     * @param OrderItem $orderItem
     * @param OrderItem $activeOrderItem
     *
     * @return \DateTime
     */
    public function calculateValidTill(OrderItem $orderItem, OrderItem $activeOrderItem = null)
    {
        $service = $orderItem->getService();

        if ($service->getIsSingle()) {
            $validTill = $orderItem->getUnlimitedValidTill();
        } else {
            if ($activeOrderItem) {
                $dateInterval = new \DateInterval('P' . $orderItem->getPeriod() . 'D');
                $validTill = $activeOrderItem->getValidTill()->add($dateInterval);
            } else {
                $validTill = $orderItem->calculateValidTill();
            }
        }

        return $validTill;
    }

    /**
     * @param OrderItem $orderItem
     * @param boolean $value
     *
     * @return bool
     */
    public function manageAttributes(OrderItem $orderItem, $value)
    {
        $service = $orderItem->getService();

        if (!$service instanceof Service) {
            return null;
        }

        if (Service::TYPE_HIGHLIGHT == $service->getType() && $orderItem->getItem() instanceof HighlightedItemInterface) {
            $orderItem->getItem()->setHighlighted($value);
        } elseif (Service::TYPE_SHOW_IN_TOP == $service->getType() && $orderItem->getItem() instanceof TopItemInterface) {
            $orderItem->getItem()->setTop($value);
        }
    }

    /**
     * @param OrderItem $orderItem
     *
     * @return OrderItem
     */
    public function setAsPaid(OrderItem $orderItem)
    {
        $orderItem->setStatus(RDWOrderStatus::STATUS_PAID);
        $orderItem->setPaidAt(new \DateTime());

        $this->activateService($orderItem, $orderItem->getOrder()->getUser());
        $this->manageAttributes($orderItem, true);
        $this->update($orderItem);

        return $orderItem;
    }

    /**
     * @param OrderItem $orderItem
     */
    public function suspend(OrderItem $orderItem)
    {
        $service = $orderItem->getService();
        $order = $orderItem->getOrder();

        // return money only for periodical services
        if (!$service->getIsSingle()
            && Order::TYPE_SYSTEM != $order->getType()
        ) {
            $daysLeft = $orderItem->getValidDaysLeft();

            // how many times to pay
            $periodsToPay = floor($daysLeft / $this->period); // exm 1 or 2 or 3
            $priceForOnePeriod = $orderItem->getPriceForOnePeriod();

            $amountToPay = $periodsToPay * $priceForOnePeriod;

            if ($amountToPay > 0) {
                $operationTitle = $this->translator->trans('Service suspend for %serviceTitle%', ['%serviceTitle%' => $service->getTitle()]);

                $this->walletManager->add($order->getUser(), $amountToPay, $operationTitle);
            }

            $orderItem->setPeriod($this->period);
        }

        // remove static attributes
        $this->manageAttributes($orderItem, false);

        // change service valid till date. service must be suspended instantly
        $validTill = new \DateTime();
        $orderItem->setValidTill($validTill);

        $this->update($orderItem);
    }

    /**
     * @param Order $order
     *
     * @return SplitOrder
     */
    public function splitOrder(Order $order)
    {
        $user = $order->getUser();
        $balance = $this->walletManager->getUserBalance($user);
        $sumToPay = 0;

        $payedItems = new ArrayCollection();

        foreach ($order->getItems() as $orderItem) {
            $sumToPay += $orderItem->getPrice();

            if ($balance >= $sumToPay) {
                $payedItems->add($orderItem);
                $order->removeItem($orderItem);

                // delete item from db
                $this->removeOrderItem($orderItem);
            }
        }
        // update order total
        $order->setTotal($order->countTotal());
        $this->orderManager->update($order);

        $splitOrder = new SplitOrder($order);

        if ($payedItems->count() > 0) {
            // create new order
            $payedOrder = $this->orderManager->createNewOrderForUser($user, Order::TYPE_CALCULATOR);

            foreach ($payedItems as $newOrderItem) {
                $newOrderItem->setOrder($payedOrder);
                $payedOrder->addItem($newOrderItem);
            }

            $payedOrder->setTotal($payedOrder->countTotal());

            $this->orderManager->update($payedOrder);
            $splitOrder->setPayedOrder($payedOrder);
        }

        return $splitOrder;
    }

    /**
     * @param OrderItem $orderItem
     * @param bool $andFlush
     *
     * @return bool
     */
    public function update(OrderItem $orderItem, $andFlush = true)
    {
        $this->objectManager->persist($orderItem);

        if ($andFlush) {
            $this->objectManager->flush();
        }

        return true;
    }

    /**
     * @param OrderItem $orderItem
     */
    public function removeOrderItem(OrderItem $orderItem)
    {
        $this->objectManager->remove($orderItem);
        $this->objectManager->flush();
    }
}
