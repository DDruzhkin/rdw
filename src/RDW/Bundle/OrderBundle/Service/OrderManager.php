<?php

namespace RDW\Bundle\OrderBundle\Service;

use RDW\Bundle\OrderBundle\Entity\Order;
use RDW\Bundle\OrderBundle\Entity\OrderItem;
use RDW\Bundle\OrderBundle\Entity\OrderItemCv;
use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\OrderBundle\Entity\OrderItemJob;
use RDW\Bundle\OrderBundle\RDWOrderStatus;
use RDW\Bundle\ServiceBundle\Entity\Service;
use RDW\Bundle\SmsBundle\Service\SmsCodeManager;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\Config\Definition\Exception\InvalidTypeException;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class OrderManager
 *
 * @package RDW\Bundle\OrderBundle\Service
 */
class OrderManager
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $entityManager;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var SmsCodeManager
     */
    private $smsCodeManager;

    /**
     * @var int
     */
    private $firstInvoiceNumber;

    /**
     * @var int
     */
    private $invoiceNumberLength;

    /**
     * @var string
     */
    private $currency;

    /**
     * @var int
     */
    private $vat;

    /**
     * @param ObjectManager $objectManager
     * @param TranslatorInterface $translator
     * @param SmsCodeManager $smsCodeManager
     * @param int $firstInvoiceNumber
     * @param int $invoiceNumberLength
     * @param string $currency
     * @param int $vat
     */
    public function __construct(
        ObjectManager $objectManager,
        TranslatorInterface $translator,
        SmsCodeManager $smsCodeManager,
        $firstInvoiceNumber,
        $invoiceNumberLength,
        $currency,
        $vat
    ) {
        $this->entityManager = $objectManager;
        $this->translator = $translator;
        $this->smsCodeManager = $smsCodeManager;
        $this->firstInvoiceNumber = $firstInvoiceNumber;
        $this->invoiceNumberLength = $invoiceNumberLength;
        $this->currency = $currency;
        $this->vat = $vat;
    }

    /**
     * @param RegisteredUser $user
     * @param string $type
     *
     * @return Order
     */
    public function createNewOrderForUser(RegisteredUser $user, $type)
    {
        $order = new Order();
        $order->setUser($user);
        $order->setCurrency($this->currency);
        $order->setVat($this->vat);
        $order->setType($type);

        // generate invoice number
        if (Order::TYPE_SYSTEM != $order->getType()) {
            $this->generateInvoiceNumber($order);
        }

        return $order;
    }

    /**
     * @param Order $order
     * @param string $type
     *
     * @return OrderItemCv|OrderItemJob
     */
    public function addNewOrderItem(Order $order, $type)
    {
        $orderItem = ($type == OrderItem::TYPE_CV) ? new OrderItemCv() : new OrderItemJob();
        $orderItem->setOrder($order);

        $order->addItem($orderItem);

        return $orderItem;
    }

    /**
     * @param Order $order
     */
    public function updateTotal(Order $order)
    {
        $order->setTotal($order->countTotal());

        $this->update($order);
    }

    /**
     * @param Order $order
     * @param bool $andFlush
     *
     * @return bool
     */
    public function update(Order $order, $andFlush = true)
    {
        $this->entityManager->persist($order);

        if ($andFlush) {
            $this->entityManager->flush();
        }

        return true;
    }

    /**
     * If all order items are paid, then set order status as paid
     *
     * @param Order $order
     */
    public function setAsFullyPaid(Order $order)
    {
        $order->setFullyPaid();
        $this->update($order);
    }

    /**
     * @param RegisteredUser $user
     * @param Service $service
     * @param int $price
     *
     * @return Order
     */
    public function createOrderWithBalanceAddition(RegisteredUser $user, Service $service, $price)
    {
        $order = $this->createNewOrderForUser($user, Order::TYPE_CREDIT);

        if ($user->isTypeEmployee()) {
            $orderItem = new OrderItemCv();
        } elseif ($user->isTypeEmployer()) {
            $orderItem = new OrderItemJob();
        } else {
            throw new InvalidTypeException();
        }

        $orderItem->setPrice($price);
        $orderItem->setOrder($order);
        $orderItem->setPeriod(1);
        $orderItem->setService($service);
        $orderItem->setStatus(RDWOrderStatus::STATUS_SUBMITTED);
        $orderItem->setTitle($service->getTitle());

        $order->addItem($orderItem);
        $order->setTotal($price);

        $this->update($order);

        return $order;
    }

    /**
     * @param Order $order
     *
     * @return bool|void
     */
    public function generateInvoiceNumber(Order $order)
    {
        if (null !== $order->getInvoiceNumber()) {
            return false;
        }

        $order->setInvoiceNumber(str_pad($this->getNextInvoiceNumber(), $this->invoiceNumberLength, 0, STR_PAD_LEFT));

        return true;
    }

    /**
     * @param Order $order
     * @param int $period
     */
    public function recalculatePeriod(Order $order, $period)
    {
        foreach ($order->getItems() as $orderItem) {
            $orderItem->setPeriod($period);
            $orderItem->setPrice($orderItem->getService()->calcPrice($period));
        }

        $order->setTotal($order->countTotal());

        // recalculating is needed only for employee when paying by sms,
        // so set payment type to sms
        if ($order->getUser()->isTypeEmployee()) {
            $order->setPaymentType(Order::PAYMENT_SMS);
        }

        $this->update($order);
    }

    /**
     * @param Order $order
     */
    public function generateSmsCodes(Order $order)
    {
        foreach ($order->getItems() as $orderItem) {
            if ($orderItem->getSmsCode()) {
                continue;
            }

            $smsCode = $this->smsCodeManager->create($orderItem->getService());

            $orderItem->setStatus(RDWOrderStatus::STATUS_PENDING);
            $orderItem->setSmsCode($smsCode);

            $this->entityManager->persist($orderItem);
        }

        $this->entityManager->flush();
    }

    /**
     * @return int
     */
    private function getNextInvoiceNumber()
    {
        $repository = $this->entityManager->getRepository('RDWOrderBundle:Order');

        if (null === $number = $repository->getLastInvoiceNumber()) {
            return $this->firstInvoiceNumber;
        }

        return (int)$number + 1;
    }
}
