(function( $ ){
    $.fn.submitFormHandler = function(options) {
        var settings = $.extend({}, options);

        $(this).on('click', function(event) {
            var $this = $(this),
                $form = $('#' + $this.data('form'));

            $form.submit();

            return false;
        });
    };
})(jQuery);

$(document).ready(function() {
    $("[data-handler='submitForm']").submitFormHandler();
});
