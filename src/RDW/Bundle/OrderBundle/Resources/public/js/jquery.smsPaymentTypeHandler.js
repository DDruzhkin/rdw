(function( $ ){
    $.fn.smsPaymentTypeHandler= function(options) {
        var settings = $.extend({}, options);

        $(this).on('ifChanged', function(event) {

            var $this = $(this),
                paymentType = $this.val(),
                $infoBlock = $('.info-block-' + paymentType);

            $('.info-block').hide();

            if ($infoBlock) {
                $infoBlock.show();
            }

            return false;
        });
    };
})(jQuery);

$(document).ready(function() {
    $("[data-handler='paymentType']").smsPaymentTypeHandler();
});
