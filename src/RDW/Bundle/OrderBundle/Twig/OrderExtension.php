<?php

namespace RDW\Bundle\OrderBundle\Twig;

use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\OrderBundle\Entity\OrderItemCv;
use RDW\Bundle\OrderBundle\RDWOrderStatus;
use RDW\Bundle\UserBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class OrderExtension
 *
 * @package RDW\Bundle\OrderBundle\Twig
 */
class OrderExtension extends \Twig_Extension implements ContainerAwareInterface
{
    /**
     * Container
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('user_order_balance', [$this, 'getUserOrdersBalance']),
            new \Twig_SimpleFunction('item_url', [$this, 'getItemUrl']),
            new \Twig_SimpleFunction('sms_code_text', [$this, 'getSmsCodeText']),
        ];
    }

    /**
     * @param User $user
     *
     * @return null|int
     */
    public function getUserOrdersBalance(User $user)
    {
        $repo = $this->container->get('rdw_order.order.repository');

        return $repo->countUserSumToPay($user);
    }

    /**
     * @param object $item
     *
     * @return null|string
     */
    public function getItemUrl($item)
    {
        $router = $this->container->get('router');

        $url = null;

        if ($item instanceof Job) {
            $url = $router->generate('rdw_job.job.view', ['id' => $item->getId(), 'slug' => $item->getSlug()]);
        }

        if ($item instanceof Cv) {
            $url = $router->generate('rdw_cv.cv.view', ['id' => $item->getId(), 'slug' => $item->getSlug()]);
        }

        return $url;
    }

    /**
     * @param OrderItemCv $item
     * @param string      $action
     *
     * @return string
     */
    public function getSmsCodeText(OrderItemCv $item, $action)
    {
        $translator = $this->container->get('translator');

        if ( ! $item->getSmsCode()) {
            return $translator->trans('No SMS code available');
        }

        if ($item->isPaid()) {
            /** @Ignore */
            return $translator->trans(ucfirst(RDWOrderStatus::STATUS_PAID));
        }

        switch ($action) {
            case 'order_finish':
                return $translator->trans('To activate service "%service_title%" for CV ID-%cv_id%, please sent SMS with code: %sms_code% to number %number%', [
                        '%service_title%' => $item->getTitle(),
                        '%cv_id%' => $item->getItem()->getId(),
                        '%sms_code%' => $item->getSmsCode()->getCode(),
                        '%number%' => $item->getSmsCode()->getNumber(),
                    ]);
            case 'order_view':
                return $translator->trans('To activate this service for CV ID-%cv_id%, please sent SMS with code: %sms_code% to number %number%', [
                        '%cv_id%' => $item->getItem()->getId(),
                        '%sms_code%' => $item->getSmsCode()->getCode(),
                        '%number%' => $item->getSmsCode()->getNumber(),
                    ]);
            default:
                $message = sprintf('Message for action "%s" is not defined', $action);
                /** @Ignore */
                return $translator->trans($message);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_order_extension';
    }
}
