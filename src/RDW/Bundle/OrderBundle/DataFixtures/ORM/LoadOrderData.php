<?php

namespace RDW\Bundle\OrderBundle\DataFixtures\ORM;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\AppBundle\DataFixtures\ORM\LoadServiceData;
use RDW\Bundle\OrderBundle\Entity\Order;
use RDW\Bundle\OrderBundle\Entity\OrderItemCv;
use RDW\Bundle\OrderBundle\Entity\OrderItemJob;
use RDW\Bundle\OrderBundle\RDWOrderStatus;
use RDW\Bundle\ServiceBundle\Entity\Service;
use RDW\Bundle\UserBundle\DataFixtures\ORM\LoadUserData;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class LoadOrderData
 *
 * @package RDW\Bundle\OrderBundle\DataFixtures\ORM
 */
class LoadOrderData extends AbstractFixture implements DependentFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    const USER_EMPLOYEE = 'employee';
    const USER_EMPLOYER = 'employer';

    function getDependencies()
    {
        return [
            LoadUserData::class,
            LoadServiceData::class
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $entityManager)
    {
        $numberOfEmployees = LoadUserData::NUMBER_OF_EMPLOYEES + LoadUserData::NUMBER_OF_EMPLOYERS;

        for ($i = 1; $i <= $numberOfEmployees; $i++) {
            $this->createOrder($i, $entityManager);
        }

        $entityManager->flush();
    }

    /**
     * @param int $userNumber
     * @param ObjectManager $entityManager
     *
     * @return null
     */
    private function createOrder($userNumber, ObjectManager $entityManager)
    {
        /** @var \RDW\Bundle\UserBundle\Entity\RegisteredUser $user */
        $user = $this->getReference('rdw.user.' . $userNumber);

        $userType = $user->hasRole(RegisteredUser::ROLE_EMPLOYEE) ? self::USER_EMPLOYEE : self::USER_EMPLOYER;

        // user cvs or jobs
        $items = (self::USER_EMPLOYEE == $userType) ? $user->getCvs() : $user->getJobs();

        // user has no cvs or jobs, so can't has order.
        if (0 == $items->count()) {
            return null;
        }

        $numberOfOrders = rand(3, 10);

        for ($x = 1; $x <= $numberOfOrders; $x++) {
            $order = new Order();
            $order->setCurrency('BYR');
            $order->setVat($this->container->getParameter('vat'));
            $order->setStatus(RDWOrderStatus::STATUS_SUBMITTED);
            $order->setUser($user);
            $order->setType(Order::TYPE_CALCULATOR);

            $this->addItems($order, $items, $userType, $entityManager);
        }
    }

    /**
     * @param Order $order
     * @param ArrayCollection $items
     * @param string $userType
     * @param ObjectManager $entityManager
     */
    private function addItems(Order $order, $items, $userType, ObjectManager $entityManager)
    {
        // add products
        $numberOfProductsPerOrder = rand(1, 6);

        for ($y = 1; $y <= $numberOfProductsPerOrder; $y++) {
            $this->createOrderItem($order, $userType, $items);
        }

        $entityManager->persist($order);
    }

    /**
     * @param Order $order
     * @param string $userType
     * @param ArrayCollection $items
     */
    private function createOrderItem(Order $order, $userType, $items)
    {
        $orderProduct = (self::USER_EMPLOYEE == $userType) ? new OrderItemCv() : new OrderItemJob();

        $periods = [7, 14, 21];
        $service = $this->getService($userType);

        $orderProduct->setPeriod($periods[rand(0, 2)]);
        $orderProduct->setItem($items->get(rand(0, $items->count() - 1)));
        $orderProduct->setService($service);
        $orderProduct->setStatus($order->getStatus()); // status the same as order
        $orderProduct->setPrice($orderProduct->getService()->calcPrice($orderProduct->getPeriod()));
        $orderProduct->setTitle($service->getTitle());
        $orderProduct->setOrder($order);

        if (RDWOrderStatus::STATUS_PAID == $orderProduct->getStatus()) {
            $orderProduct->setPaidAt(new \DateTime());
        }

        $order->addItem($orderProduct);
        $order->setTotal($order->getTotal() + $orderProduct->getPrice());
    }

    /**
     * @param string $userType
     *
     * @return Service
     */
    private function getService($userType)
    {
        // get service
        $services = [
            Service::FOR_EMPLOYEE => [
                Service::TYPE_HIGHLIGHT,
                Service::TYPE_SHOW_IN_TOP,
                Service::TYPE_CAROUSEL,
                Service::TYPE_MONEY_TO_BALANCE,
            ],
            Service::FOR_EMPLOYER => [
                Service::TYPE_CAROUSEL,
                Service::TYPE_VIP,
                Service::TYPE_SEND_EMAIL,
                Service::TYPE_HIGHLIGHT,
                Service::TYPE_JOB_PUBLISHING,
                Service::TYPE_SHOW_IN_TOP,
                Service::TYPE_MONEY_TO_BALANCE,
            ],
        ];

        $serviceType = (self::USER_EMPLOYEE == $userType)
            ? $services[Service::FOR_EMPLOYEE][rand(0, count($services[Service::FOR_EMPLOYEE]) - 1)]
            : $services[Service::FOR_EMPLOYER][rand(0, count($services[Service::FOR_EMPLOYER]) - 1)];

        $isFor = (self::USER_EMPLOYEE == $userType) ? Service::FOR_EMPLOYEE : Service::FOR_EMPLOYER;

        return $this->getReference(sprintf('rdw.service.%d_%d', $isFor, $serviceType));
    }
}
