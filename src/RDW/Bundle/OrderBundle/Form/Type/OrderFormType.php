<?php

namespace RDW\Bundle\OrderBundle\Form\Type;

use RDW\Bundle\OrderBundle\Entity\Order;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * Class OrderFormType
 *
 * @package RDW\Bundle\OrderBundle\Form\Type
 */
class OrderFormType extends AbstractType
{
    /**
     * @var SecurityContext
     */
    private $security;

    /**
     * @param SecurityContext $context
     */
    public function __construct(SecurityContext $context)
    {
        $this->security = $context;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $order = $builder->getForm()->getData();
        $user = $order->getUser();

        // if manager is not owner of order
        if (
            $this->security->isGranted(RegisteredUser::ROLE_MANAGER)
            && ($token = $this->security->getToken())
            && ($loggedInUser = $token->getUser())
            && ! $order->isOwner($loggedInUser)
        ) {
            $paymentTypes = Order::getPaymentTypesForManager();
            $dataHandler = '';
        } else {
            $paymentTypes = Order::getPaymentTypes($user, $order->getType());
            $dataHandler = 'paymentType';
        }

        $builder
            ->add('paymentType', 'choice', [
                'required' => true,
                'expanded' => true,
                'choices'   => $paymentTypes,
                'attr' => [
                    'data-handler' => $dataHandler,
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\OrderBundle\Entity\Order'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_order_form';
    }
}
