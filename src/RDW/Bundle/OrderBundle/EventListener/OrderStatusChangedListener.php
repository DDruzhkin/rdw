<?php

namespace RDW\Bundle\OrderBundle\EventListener;

use RDW\Bundle\MailerBundle\Service\MailerManager;
use RDW\Bundle\OrderBundle\Event\OrderStatusEvent;
use RDW\Bundle\OrderBundle\RDWOrderEvents;
use RDW\Bundle\OrderBundle\Service\OrderManager;
use RDW\Bundle\PaymentBundle\Event\PaymentEvent;
use RDW\Bundle\PaymentBundle\RDWPaymentEvents;
use RDW\Bundle\PaymentBundle\Service\PaymentManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class OrderStatusChangedListener
 *
 * @package RDW\Bundle\OrderBundle\EventListener
 */
class OrderStatusChangedListener implements EventSubscriberInterface
{
    /**
     * @var \RDW\Bundle\OrderBundle\Service\OrderManager
     */
    private $orderManager;

    /**
     * @var MailerManager
     */
    private $mailerManager;

    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param OrderManager             $orderManager
     * @param MailerManager            $mailerManager
     * @param PaymentManager           $paymentManager
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        OrderManager $orderManager,
        MailerManager $mailerManager,
        PaymentManager $paymentManager,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->orderManager  = $orderManager;
        $this->mailerManager = $mailerManager;
        $this->paymentManager = $paymentManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            RDWOrderEvents::ORDER_STATUS_PAID => 'onOrderPay',
            RDWOrderEvents::ORDER_STATUS_CANCELLED => 'onOrderCancel',
        ];
    }

    /**
     * @param OrderStatusEvent $event
     *
     * @return bool
     */
    public function onOrderCancel(OrderStatusEvent $event)
    {
        $order = $event->getOrder();
        $status = $event->getStatus();

        if (! $order->isStatusChangeable($status)) {
            return false;
        }

        $order->setStatus($status);

        $this->orderManager->update($order);
        $this->mailerManager->sendOrderCancelledEmailForUser($order);
    }

    /**
     * @param OrderStatusEvent $event
     *
     * @return bool
     */
    public function onOrderPay(OrderStatusEvent $event)
    {
        $order = $event->getOrder();
        $status = $event->getStatus();

        if ($order->isPaid() || ! $order->isStatusChangeable($status)) {
            return false;
        }

        $payment = $this->paymentManager->getOneBy(['order' => $order, 'provider' => $order->getPaymentType()]);

        if (! $payment) {
            $payment = $this->paymentManager->create(
                $order,
                $order->getPaymentType(),
                $order->getCurrency(),
                $order->getTotal()
            );
        }

        $order->setStatus($status);
        $this->orderManager->update($order);

        $paymentEvent = new PaymentEvent($payment);
        $this->eventDispatcher->dispatch(RDWPaymentEvents::PAYMENT_MARK_AS_PAID, $paymentEvent);
    }
}
