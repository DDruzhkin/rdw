<?php

namespace RDW\Bundle\OrderBundle\EventListener;

use RDW\Bundle\OrderBundle\Entity\Order;
use RDW\Bundle\OrderBundle\Event\OrderEvent;
use RDW\Bundle\OrderBundle\Event\OrderStatusEvent;
use RDW\Bundle\OrderBundle\RDWOrderEvents;
use RDW\Bundle\OrderBundle\RDWOrderStatus;
use RDW\Bundle\OrderBundle\Service\OrderItemManager;
use RDW\Bundle\WalletBundle\Service\WalletManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class ActivateServicesFromListener
 *
 * @package RDW\Bundle\OrderBundle\EventListener
 */
class ActivateServicesFromWalletListener implements EventSubscriberInterface
{
    /**
     * @var OrderItemManager
     */
    protected $orderItemManager;

    /**
     * @var WalletManager
     */
    protected $walletManager;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @param OrderItemManager         $orderItemManager
     * @param WalletManager            $walletManager
     * @param Router                   $router
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(
        OrderItemManager $orderItemManager,
        WalletManager $walletManager,
        \Symfony\Cmf\Component\Routing\ChainRouter $router,
        EventDispatcherInterface $dispatcher
    )
    {
        $this->orderItemManager = $orderItemManager;
        $this->walletManager = $walletManager;
        $this->router = $router;
        $this->dispatcher = $dispatcher;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            RDWOrderEvents::MATRIX_SUBMITTED_SUCCESS => 'onMatrixSubmit',
        ];
    }

    /**
     * @param OrderEvent $event
     *
     * @return OrderEvent
     */
    public function onMatrixSubmit(OrderEvent $event)
    {
        $order = $event->getOrder();
        $user = $order->getUser();
        $balance = $this->walletManager->getUserBalance($user);

        // if user has more money than order total, activate all services
        if ($balance >= $order->getTotal()) {
            $order->setPaymentType(Order::PAYMENT_WALLET);
            $orderStatusEvent = new OrderStatusEvent($order, RDWOrderStatus::STATUS_PAID);
            $this->dispatcher->dispatch(RDWOrderEvents::ORDER_STATUS_PAID, $orderStatusEvent);

            $response = new RedirectResponse($this->router->generate('rdw_order.order.view', ['id' => $order->getId()]));

            $event->setResponse($response);

            return $event;
        }

        // if balance more then zero, but less then order total, when split current order into two
        if ($order->shouldBeSplitted($balance)) {
            $orders = $this->orderItemManager->splitOrder($order);

            // activate services from payed order
            if ($orders->getPayedOrder() instanceof Order) {
                $payedOrder = $orders->getPayedOrder();
                $payedOrder->setPaymentType(Order::PAYMENT_WALLET);
                $orderStatusEvent = new OrderStatusEvent($payedOrder, RDWOrderStatus::STATUS_PAID);
                $this->dispatcher->dispatch(RDWOrderEvents::ORDER_STATUS_PAID, $orderStatusEvent);
            }

            // redirect to order submit page to newly created order
            if ($orders->getOrderToPay()) {
                $response = new RedirectResponse($this->router->generate('rdw_order.order.submit', ['id' => $orders->getOrderToPay()->getId()]));

                $event->setResponse($response);

                return $event;
            }
        }

        return $event;
    }
}
