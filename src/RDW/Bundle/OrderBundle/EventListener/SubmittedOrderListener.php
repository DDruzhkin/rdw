<?php

namespace RDW\Bundle\OrderBundle\EventListener;

use RDW\Bundle\MailerBundle\Service\MailerManager;
use RDW\Bundle\OrderBundle\Entity\Order;
use RDW\Bundle\OrderBundle\Event\OrderEvent;
use RDW\Bundle\OrderBundle\RDWOrderEvents;
use RDW\Bundle\OrderBundle\RDWOrderStatus;
use RDW\Bundle\OrderBundle\Service\OrderManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class SubmittedOrderListener
 *
 * @package Order
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class SubmittedOrderListener implements EventSubscriberInterface
{
    /**
     * @var \RDW\Bundle\OrderBundle\Service\OrderManager
     */
    private $orderManager;

    /**
     * @var MailerManager
     */
    private $mailerManager;

    /**
     * @param OrderManager  $orderManager
     * @param MailerManager $mailerManager
     */
    public function __construct(OrderManager $orderManager, MailerManager $mailerManager)
    {
        $this->orderManager  = $orderManager;
        $this->mailerManager = $mailerManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            RDWOrderEvents::SUBMIT_SUCCESS => 'onSubmitSuccess',
            RDWOrderEvents::CREATE_SUCCESS => 'onOrderCreate',
        ];
    }

    /**
     * @param OrderEvent $event
     */
    public function onSubmitSuccess(OrderEvent $event)
    {
        $order = $event->getOrder();
        $order->setStatus(RDWOrderStatus::STATUS_PENDING);

        // send emails
        switch ($order->getPaymentType()) {
            case Order::PAYMENT_TRANSFER:
                $this->mailerManager->sendPaymentViaBankTransferEmail($order);
                break;

            case Order::PAYMENT_GUARANTEE:
                $this->mailerManager->sendPaymentViaGuaranteeEmail($order);
                break;

            default:
                break;
        }

        $this->orderManager->update($order);
    }

    /**
     * When order created
     *
     * @param OrderEvent $event
     */
    public function onOrderCreate(OrderEvent $event)
    {
        $order = $event->getOrder();

        $this->mailerManager->sendOrderSubmittedEmailForUser($order);
    }
}
