<?php

namespace RDW\Bundle\OrderBundle\Event;

use RDW\Bundle\OrderBundle\Entity\Order;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class OrderStatusEvent
 *
 * @package RDW\Bundle\OrderBundle\Event
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class OrderStatusEvent extends OrderEvent
{
    /**
     * @var string
     */
    private $status;

    /**
     * @param Order  $order
     * @param string $status
     */
    public function __construct(Order $order, $status)
    {
        parent::__construct($order);

        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
}
