<?php

namespace RDW\Bundle\OrderBundle\Event;

use RDW\Bundle\OrderBundle\Entity\Order;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class OrderEvent
 *
 * @package RDW\Bundle\OrderBundle\Event
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class OrderEvent extends Event
{
    /**
     * @var Order
     */
    private $order;

    private $response;

    /**
     * @param Order  $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return \RDW\Bundle\OrderBundle\Entity\Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param mixed $response
     *
     * @return $this
     */
    public function setResponse($response)
    {
        $this->response = $response;

        return $this;
    }
}
