<?php

namespace RDW\Bundle\OrderBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use RDW\Bundle\OrderBundle\Entity\Order;
use RDW\Bundle\OrderBundle\Entity\OrderItem;
use RDW\Bundle\OrderBundle\RDWOrderStatus;
use RDW\Bundle\ServiceBundle\Entity\Service;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;

/**
 * Class OrderRepository
 *
 * @package RDW\Bundle\OrderBundle\Repository
 */
class OrderRepository extends EntityRepository
{
    /**
     * @param RegisteredUser $user
     * @param null           $limit
     *
     * @return array
     */
    public function getUserActiveServices(RegisteredUser $user, $limit = null)
    {
        $qb = $this->_em->createQueryBuilder();

        $from = ($user->isTypeEmployee()) ? 'RDWOrderBundle:OrderItemCv' : 'RDWOrderBundle:OrderItemJob';

        $qb
            ->select('oi', 's', 'item')
            ->from($from, 'oi')
            ->join('oi.order', 'o')
            ->join('oi.service', 's')
            ->innerJoin('oi.item', 'item')
            ->andWhere('o.user = :user')
            ->andWhere('oi.validTill > :date')
            ->andWhere('oi.status = :status')
            ->andWhere('s.visible = :service_is_visible')
            ->setParameter('status', RDWOrderStatus::STATUS_PAID)
            ->setParameter('user', $user)
            ->setParameter('service_is_visible', true)
            ->setParameter('date', new \DateTime());

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param int            $id
     * @param RegisteredUser $user
     *
     * @return mixed
     */
    public function findActivePersonalService($id, RegisteredUser $user)
    {
        $qb = $this->getActiveServiceQueryBuilderForUser($id, $user);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param int            $id
     * @param object         $object
     * @param RegisteredUser $user
     *
     * @return mixed
     */
    public function findActiveServiceForObjectByUser($id, $object, RegisteredUser $user)
    {
        $qb = $this->getActiveServiceQueryBuilderForUser($id, $user);

        $qb
            ->andWhere('oi.item = :item')
            ->setParameter('item', $object);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param int            $id
     * @param object         $object
     * @param RegisteredUser $user
     *
     * @return mixed
     */
    public function findPendingServiceForObjectByUser($id, $object, RegisteredUser $user)
    {
        $qb = $this->getPendingServiceQueryBuilderForUser($id, $user);

        $qb
            ->andWhere('oi.item = :item')
            ->setParameter('item', $object);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param int            $id
     * @param RegisteredUser $user
     *
     * @return mixed
     */
    public function findPendingPersonalService($id, RegisteredUser $user)
    {
        $qb = $this->getPendingServiceQueryBuilderForUser($id, $user);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @return int|null
     */
    public function getLastInvoiceNumber()
    {
        try {
            return $this->_em->createQueryBuilder()
                ->select('o.invoiceNumber')
                ->from('RDWOrderBundle:Order', 'o')
                ->orderBy('o.invoiceNumber', 'desc')
                ->setMaxResults(1)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
     * @param RegisteredUser $user
     *
     * @return mixed|null
     */
    public function countUserSumToPay(RegisteredUser $user)
    {
        try {
            return $this->_em->createQueryBuilder()
                ->select('SUM(o.total)')
                ->from('RDWOrderBundle:Order', 'o')
                ->where('o.user = :user')
                ->andWhere('o.status IN (:statuses)')
                ->setParameter('user', $user)
                ->setParameter('statuses', [RDWOrderStatus::STATUS_PENDING, RDWOrderStatus::STATUS_SUBMITTED])
                ->setMaxResults(1)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
     * @param RegisteredUser $user
     *
     * @return QueryBuilder
     */
    protected function getQueryBuilderForObjectByUser(RegisteredUser $user)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('oi', 's');

        if ($user->isTypeEmployee()) {
            $qb->from('RDWOrderBundle:OrderItemCv', 'oi');
        } else {
            $qb->from('RDWOrderBundle:OrderItemJob', 'oi');
        }

        return $qb;
    }

    /**
     * @param int            $id
     * @param RegisteredUser $user
     *
     * @return QueryBuilder
     */
    protected function getActiveServiceQueryBuilderForUser($id, RegisteredUser $user)
    {
        $qb = $this->getQueryBuilderForObjectByUser($user);

        $qb
            ->join('oi.order', 'o')
            ->join('oi.service', 's')
            ->andWhere('o.user = :user')
            ->andWhere('oi.validTill > :date')
            ->andWhere('oi.status = :status')
            ->andWhere('s.id = :serviceId')
            ->setParameter('status', RDWOrderStatus::STATUS_PAID)
            ->setParameter('serviceId', $id)
            ->setParameter('user', $user)
            ->setParameter('date', new \DateTime())
            ->setMaxResults(1);

        return $qb;
    }

    /**
     * @param int            $id
     * @param RegisteredUser $user
     *
     * @return QueryBuilder
     */
    protected function getPendingServiceQueryBuilderForUser($id, RegisteredUser $user)
    {
        $qb = $this->getQueryBuilderForObjectByUser($user);

        $qb
            ->join('oi.order', 'o')
            ->join('oi.service', 's')
            ->andWhere('o.user = :user')
            ->andWhere('oi.status IN (:statuses)')
            ->andWhere('s.id = :serviceId')
            ->setParameter('statuses', [RDWOrderStatus::STATUS_SUBMITTED, RDWOrderStatus::STATUS_PENDING])
            ->setParameter('serviceId', $id)
            ->setParameter('user', $user)
            ->setMaxResults(1);

        return $qb;
    }
}
