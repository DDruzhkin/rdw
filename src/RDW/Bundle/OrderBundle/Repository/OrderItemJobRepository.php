<?php

namespace RDW\Bundle\OrderBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr as Expr;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\OrderBundle\Entity\OrderItemJob;
use RDW\Bundle\OrderBundle\RDWOrderStatus;
use RDW\Bundle\ServiceBundle\Entity\Service;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;

/**
 * Class OrderItemJobRepository
 *
 * @package RDW\Bundle\OrderBundle\Repository
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class OrderItemJobRepository extends EntityRepository
{
    /**
     * @param string $type
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getJobsQueryBuilderByServiceType($type)
    {

        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder
            ->select('oi, j')
            ->from('RDWOrderBundle:OrderItemJob', 'oi')
            ->innerJoin('oi.order', 'o')
            ->innerJoin('oi.item', 'j')
            ->innerJoin('oi.service', 'service')
            ->andWhere('oi.status = :orderItemStatus')
            ->andWhere('j.status = :jobStatus')
            ->andWhere('service.type = :serviceType')
            ->andWhere('oi.validTill >= :validTill')
            ->setParameter('validTill', new \DateTime())
            ->setParameter('orderItemStatus', RDWOrderStatus::STATUS_PAID)
            ->setParameter('jobStatus', Job::STATUS_ACTIVE)
            ->setParameter('serviceType', $type)
            ->groupBy('j.id');

        return $queryBuilder;
    }

    /**
     * @param int $limit
     *
     * @return array|OrderItemJob[]
     */
    public function findAllForCarousel($limit)
    {

        $queryBuilder = $this->getJobsQueryBuilderByServiceType(Service::TYPE_CAROUSEL);

        $queryBuilder
            ->addSelect('RAND() as HIDDEN rand')
            ->addOrderBy('rand')
            ->setMaxResults($limit);

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param int $limit
     *
     * @return array
     */
    public function findAllForVipJobs($limit)
    {
        $queryBuilder = $this->getJobsQueryBuilderByServiceType(Service::TYPE_VIP_JOB);

        $queryBuilder
            ->groupBy('o.user')
            ->setMaxResults($limit);

        $query = $queryBuilder->getQuery();
        $result = $query->getResult();

        return $result;
    }

    /**
     * @param int $limit
     *
     * @return array
     */
    public function findAllTopEmployers($limit)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder
            ->select('oi', 'o', 'user')
            ->addSelect('RAND() as HIDDEN rand')
            ->from('RDWOrderBundle:OrderItemJob', 'oi')
            ->leftJoin('oi.order', 'o')
            ->leftJoin('oi.service', 'service')
            ->leftJoin('o.user', 'user')
            ->where('oi.validTill >= :validTill')
            ->andWhere('oi.status = :orderStatus')
            ->andWhere('service.type = :type')
            ->andWhere('user.photo is NOT NULL')
            ->setParameter('validTill', new \DateTime())
            ->setParameter('orderStatus', RDWOrderStatus::STATUS_PAID)
            ->setParameter('type', Service::TYPE_VIP)
            ->setMaxResults($limit)
            ->addOrderBy('rand')
            ->groupBy('o.user');

        $query = $queryBuilder->getQuery();
        $result = $query->getResult();

        return $result;
    }

    /**
     * @param RegisteredUser $user
     * @param string         $serviceType
     *
     * @return mixed
     */
    public function findActivePersonalServiceForUser(RegisteredUser $user, $serviceType)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder
            ->select('oi')
            ->from('RDWOrderBundle:OrderItemJob', 'oi')
            ->innerJoin('oi.order', 'o')
            ->innerJoin('oi.service', 'service')
            ->where('oi.validTill >= :validTill')
            ->andWhere('o.status = :orderStatus')
            ->andWhere('o.user = :user')
            ->andWhere('service.type = :serviceType')
            ->setParameter('validTill', new \DateTime())
            ->setParameter('user', $user)
            ->setParameter('orderStatus', RDWOrderStatus::STATUS_PAID)
            ->setParameter('serviceType', $serviceType)
            ->setMaxResults(1);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    /**
     * @param Job $job
     *
     * @return bool
     */
    public function hasActiveJobPublishingService(Job $job)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder
            ->select('COUNT(oi.id)')
            ->from('RDWOrderBundle:OrderItemJob', 'oi')
            ->innerJoin('oi.service', 'service')
            ->innerJoin('oi.order', 'orders')
            ->where('service.type = :serviceTypeJobPublishing')
            ->andWhere('oi.status = :itemStatus')
            ->andWhere('oi.validTill >= :validTill')
            ->andWhere('oi.item = :job')
            ->setParameter(':serviceTypeJobPublishing', Service::TYPE_JOB_PUBLISHING)
            ->setParameter(':itemStatus', RDWOrderStatus::STATUS_PAID)
            ->setParameter('validTill', new \DateTime())
            ->setParameter('job', $job);

        $count = $queryBuilder->getQuery()->getSingleScalarResult();

        return ($count > 0) ? true : false;
    }
}
