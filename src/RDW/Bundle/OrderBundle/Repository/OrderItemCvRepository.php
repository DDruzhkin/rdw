<?php

namespace RDW\Bundle\OrderBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr as Expr;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\OrderBundle\Entity\OrderItemCv;
use RDW\Bundle\OrderBundle\RDWOrderStatus;
use RDW\Bundle\ServiceBundle\Entity\Service;
use RDW\Bundle\UserBundle\Entity\UserInterface;

/**
 * Class OrderItemCvRepository
 *
 * @package RDW\Bundle\OrderBundle\Repository
 */
class OrderItemCvRepository extends EntityRepository
{
    /**
     * @param int $limit
     *
     * @return array|OrderItemCv[]
     */
    public function findAllForCarousel($limit)
    {
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder
            ->select('oi, cv')
            ->addSelect('RAND() as HIDDEN rand')
            ->from('RDWOrderBundle:OrderItemCv', 'oi')
            ->innerJoin('oi.order', 'o')
            ->innerJoin('oi.item', 'cv')
            ->innerJoin('oi.service', 'service')
            ->andWhere('oi.validTill >= :validTill')
            ->andWhere('oi.status = :orderItemStatus')
            ->andWhere('cv.status IN (:statuses)')
            ->andWhere('service.type = :serviceType')
            ->setParameter('validTill', new \DateTime())
            ->setParameter('orderItemStatus', RDWOrderStatus::STATUS_PAID)
            ->setParameter('statuses', [Cv::STATUS_ACTIVE])
            ->setParameter('serviceType', Service::TYPE_CAROUSEL)
            ->groupBy('cv.id')
            ->addOrderBy('rand')
            ->setMaxResults($limit);

        return $queryBuilder->getQuery()->getResult();
    }
}
