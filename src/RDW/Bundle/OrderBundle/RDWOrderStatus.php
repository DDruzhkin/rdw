<?php

namespace RDW\Bundle\OrderBundle;

/**
 * Class RDWOrderStatus
 *
 * @package RDW\Bundle\OrderBundle
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
final class RDWOrderStatus
{
    const STATUS_SUBMITTED = 'submitted';
    const STATUS_PAID      = 'paid';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_PENDING   = 'pending';
}
