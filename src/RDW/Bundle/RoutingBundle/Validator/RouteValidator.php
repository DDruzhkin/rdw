<?php

namespace RDW\Bundle\RoutingBundle\Validator;

use RDW\Bundle\RoutingBundle\Provider\RouteRedirectRegistry;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class RouteValidator
{
    /** @var RouteRedirectRegistry */
    protected $registry;

    /**
     * @param RouteRedirectRegistry $registry
     */
    public function __construct(RouteRedirectRegistry $registry)
    {
        $this->registry = $registry;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $response = $this->registry->handleRequest($event->getRequest());
        if ($response) {
            $event->setResponse($response);
            $event->stopPropagation();
        }
    }
}
