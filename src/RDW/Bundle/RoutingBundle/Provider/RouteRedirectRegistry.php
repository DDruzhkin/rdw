<?php

namespace RDW\Bundle\RoutingBundle\Provider;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class RouteRedirectRegistry
{
    /** @var array|RouteRedirectProviderInterface[] */
    protected $providers = [];

    /**
     * @param RouteRedirectProviderInterface $provider
     *
     * @return $this
     */
    public function addProvider(RouteRedirectProviderInterface $provider)
    {
        $this->providers[] = $provider;

        return $this;
    }

    /**
     * @param Request $request
     *
     * @return null|RedirectResponse
     */
    public function handleRequest(Request $request)
    {
        foreach ($this->providers as $provider) {
            $redirect = $provider->handleRequest($request);
            if ($redirect) {
                return $redirect;
            }
        }
    }
}
