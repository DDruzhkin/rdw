<?php

namespace RDW\Bundle\RoutingBundle\Provider;

use Doctrine\Bundle\DoctrineBundle\Registry;
use RDW\Bundle\LocationBundle\Entity\City;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

class CityRedirectProvider implements RouteRedirectProviderInterface
{
    /** @var Registry */
    protected $doctrine;

    /** @var RouterInterface */
    protected $router;

    /** @var string */
    protected $baseDomain;

    /**
     * RouteValidator constructor.
     *
     * @param Registry $doctrine
     * @param RouterInterface $router
     * @param string $baseDomain
     */
    public function __construct(Registry $doctrine, RouterInterface $router, $baseDomain)
    {
        $this->doctrine = $doctrine;
        $this->router = $router;
        $this->baseDomain = $baseDomain;
    }

    /**
     * {@inheritDoc}
     */
    public function handleRequest(Request $request)
    {
        $baseDomain = $this->baseDomain;

        if (!in_array($request->getPort(), array(80, 443))) {
            $baseDomain = $this->baseDomain . ':' . $request->getPort();
        }

        $subDomain = trim(str_replace($baseDomain, '', $request->getHttpHost()), '.');

        if (!strlen($subDomain) || in_array($subDomain, array('www'))) {
            return null;
        }

        $city = $this->doctrine->getRepository(City::class)->findOneBy(array('titleInLatin' => ucfirst($subDomain)));

        if ($city) {
            return null;
        }

        return new RedirectResponse(
        //trim($this->router->generate('rdw_app.default.index', array(), RouterInterface::ABSOLUTE_URL), '/'),
            'http://' . $baseDomain,
            Response::HTTP_MOVED_PERMANENTLY
        );
    }
}
