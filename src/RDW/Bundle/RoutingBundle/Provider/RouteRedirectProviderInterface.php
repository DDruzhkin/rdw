<?php

namespace RDW\Bundle\RoutingBundle\Provider;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

interface RouteRedirectProviderInterface
{
    /**
     * @param Request $request
     *
     * @return null|RedirectResponse
     */
    public function handleRequest(Request $request);
}
