<?php

namespace RDW\Bundle\MailerBundle\Helper;

use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\JobBundle\Entity\Job;
use Symfony\Component\Routing\Router;

/**
 * Class SubscriptionTableHelper
 *
 * @package RDW\Bundle\MailerBundle\Helper
 */
class SubscriptionTableHelper
{
    const TYPE_JOB = 'job';
    const TYPE_CV = 'cv';

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * @param Router $router
     */
    public function __construct(\Symfony\Cmf\Component\Routing\ChainRouter $router, \Twig_Environment $twig)
    {
        $this->router = $router;
        $this->twig = $twig;
    }

    /**
     * @param array $objects
     *
     * @return string
     */
    public function constructObjectsTableForEmail(array $objects, $type)
    {
        if ( ! in_array($type, [self::TYPE_JOB, self::TYPE_CV])) {
            throw new \InvalidArgumentException(sprintf(
                'Wrong type. Expected "%s", "%s", but got "%s"',
                self::TYPE_JOB, self::TYPE_CV, $type
            ));
        }

        return $this->twig->render('RDWMailerBundle::subscriptionTable.html.twig', [
            'items' => $objects,
            'type' => $type,
        ]);
    }
}
