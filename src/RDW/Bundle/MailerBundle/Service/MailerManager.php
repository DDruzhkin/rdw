<?php

namespace RDW\Bundle\MailerBundle\Service;

use Slot\MandrillBundle\Dispatcher;
use Slot\MandrillBundle\Message;
use RDW\Bundle\AppBundle\Entity\Abuse;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\FaqBundle\Entity\Faq;
use RDW\Bundle\AppBundle\Entity\JobAbuse;
use RDW\Bundle\AppBundle\Entity\CvAbuse;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Model\EmailForEmployee;
use RDW\Bundle\MailerBundle\Helper\SubscriptionTableHelper;
use RDW\Bundle\OrderBundle\Entity\Order;
use RDW\Bundle\SubscriptionBundle\Entity\Subscription;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\User;
use RDW\Bundle\ManageBundle\Entity\User as ManageUser;
use RDW\Bundle\ContactBundle\Model\Message as RequestMessage;
use Symfony\Component\Routing\Router;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class MailerManager
 *
 * @package RDW\Bundle\MailerBundle\Service
 */
class MailerManager
{
    /**
     * @var Dispatcher
     */
    private $dispatcher;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var SubscriptionTableHelper
     */
    private $subscriptionTableFormatter;

    /**
     * @var string
     */
    private $administratorEmail;

    /**
     * @param Dispatcher $dispatcher
     * @param TranslatorInterface $translator
     * @param Router $router
     * @param SubscriptionTableHelper $tableHelper
     * @param string $administratorEmail
     */
    public function __construct(
        Dispatcher $dispatcher,
        TranslatorInterface $translator,
        \Symfony\Cmf\Component\Routing\ChainRouter $router,
        SubscriptionTableHelper $tableHelper,
        $administratorEmail
    ) {
        $this->dispatcher = $dispatcher;
        $this->translator = $translator;
        $this->router = $router;
        $this->subscriptionTableFormatter = $tableHelper;
        $this->administratorEmail = $administratorEmail;
    }

    /**
     * @param User $user
     */
    public function sendRegistrationConfirmEmail(User $user)
    {
        $templateName = 'registration-confirm';

        $url = $this->router->generate('fos_user_registration_confirm', ['token' => $user->getConfirmationToken()], true);

        $mergeVars = [
            'url' => $url,
        ];

        $message = $this->createMessage()
            ->addTo($user->getEmail())
            ->setSubject($this->translator->trans('Registration confirm at rdw.by'))
            ->setMerge(true)
            ->addMergeVars($user->getEmail(), $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param User $user
     */
    public function sendSocialRegistrationConfirmEmailMessage(User $user)
    {
        $templateName = 'social-registration-confirm';

        $url = $this->router->generate('rdw_user.registration.confirm_social', ['token' => $user->getConfirmationToken()], true);
        $mergeVars = [
            'url' => $url,
        ];

        $message = $this->createMessage()
            ->addTo($user->getSocialEmail())
            ->setSubject($this->translator->trans('Registration confirm at rdw.by'))
            ->setMerge(true)
            ->addMergeVars($user->getSocialEmail(), $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param RegisteredUser $user
     */
    public function sendChangeEmailConfirmationEmail(RegisteredUser $user)
    {
        $templateName = 'change-email-confirm';

        $url = $this->router->generate('rdw_user.change_email.confirm', ['token' => $user->getConfirmationToken()], true);
        $mergeVars = [
            'url' => $url,
        ];

        $message = $this->createMessage()
            ->addTo($user->getNewEmail())
            ->setSubject($this->translator->trans('Email change function at rdw.by'))
            ->setMerge(true)
            ->addMergeVars($user->getNewEmail(), $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param Subscription $subscription
     * @param array $jobs
     */
    public function sendNewSubscribedJobsEmail(Subscription $subscription, array $jobs)
    {
        $templateName = 'send-subscription';

        /** @var RegisteredUser $user */
        $user = $subscription->getUser();

        $url = $this->router->generate('rdw_subscription.subscription.unsubscribe', ['token' => $subscription->getToken()], true);

        $mergeVars = [
            'unsubscribe_url' => $url,
            'username' => $user->getTitle(),
            'table_content' => $this->subscriptionTableFormatter->constructObjectsTableForEmail($jobs, SubscriptionTableHelper::TYPE_JOB),
        ];

        $message = $this->createMessage()
            ->addTo($subscription->getEmail())
            ->setSubject($this->translator->trans('New jobs at rdw.by'))
            ->setMerge(true)
            ->addMergeVars($subscription->getEmail(), $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param RegisteredUser $employee
     * @param array $jobs
     */
    public function sendJobOffer(RegisteredUser $employee, array $jobs)
    {
        $templateName = 'job-offer';

        $mergeVars = [
            'username' => $employee->getTitle(),
            'table_content' => $this->subscriptionTableFormatter->constructObjectsTableForEmail($jobs, SubscriptionTableHelper::TYPE_JOB),
        ];

        $message = $this->createMessage()
            ->addTo($employee->getEmail())
            ->setSubject($this->translator->trans('Jobs matching your profile'))
            ->setMerge(true)
            ->addMergeVars($employee->getEmail(), $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param Subscription $subscription
     * @param array $cvs
     */
    public function sendNewSubscribedCvsEmail(Subscription $subscription, array $cvs)
    {
        $templateName = 'send-subscription';

        /** @var RegisteredUser $user */
        $user = $subscription->getUser();

        $url = $this->router->generate('rdw_subscription.subscription.unsubscribe', ['token' => $subscription->getToken()], true);

        $mergeVars = [
            'unsubscribe_url' => $url,
            'username' => $user->getTitle(),
            'table_content' => $this->subscriptionTableFormatter->constructObjectsTableForEmail($cvs, SubscriptionTableHelper::TYPE_CV),
        ];

        $message = $this->createMessage()
            ->addTo($subscription->getEmail())
            ->setSubject($this->translator->trans('New cvs at rdw.by'))
            ->setMerge(true)
            ->addMergeVars($subscription->getEmail(), $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param User $user
     */
    public function sendPasswordResetEmail(User $user)
    {
        $templateName = 'password-reset';

        $url = $this->router->generate('fos_user_resetting_reset', ['token' => $user->getConfirmationToken()], true);
        $mergeVars = [
            'url' => $url,
        ];

        $message = $this->createMessage()
            ->addTo($user->getEmail())
            ->setSubject($this->translator->trans('Password reset at rdw.by'))
            ->setMerge(true)
            ->addMergeVars($user->getEmail(), $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param Order $order
     */
    public function sendPaymentViaBankTransferEmail(Order $order)
    {
        $templateName = 'payment-via-bank-transfer';

        $client = $order->getUser();
        $manager = $client->getManager();

        if (!$manager) {
            return;
        }

        $mergeVars = [
            'manager' => $manager->getContactPersonFullName(),
            'user' => $client->getCompanyTitle(),
            'phone' => $client->getPhone(),
            'email' => $client->getEmail(),
            'order_id' => $order->getId(),
        ];

        $message = $this->createMessage()
            ->addTo($manager->getEmail())
            ->setSubject($this->translator->trans('New order at www.rdw.by, payment - bank transfer'))
            ->setMerge(true)
            ->addMergeVars($manager->getEmail(), $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param Order $order
     */
    public function sendPaymentViaGuaranteeEmail(Order $order)
    {
        $templateName = 'payment-via-guarantee-transfer';

        $client = $order->getUser();
        $manager = $client->getManager();

        if (!$manager) {
            return;
        }

        $mergeVars = [
            'manager' => $manager->getContactPersonFullName(),
            'user' => $client->getCompanyTitle(),
            'phone' => $client->getPhone(),
            'email' => $client->getEmail(),
            'order_id' => $order->getId(),
        ];

        $message = $this->createMessage()
            ->addTo($manager->getEmail())
            ->setSubject($this->translator->trans('New order at www.rdw.by, payment - guarantee'))
            ->setMerge(true)
            ->addMergeVars($manager->getEmail(), $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param Order $order
     */
    public function sendOrderCancelledEmailForUser(Order $order)
    {
        $templateName = 'order-cancelled-by-manager';

        $user = $order->getUser();
        $manager = $user->getManager();

        if (!$manager) {
            return;
        }

        $mergeVars = [
            'user' => $user->getTitle(),
            'manager' => $manager->getContactPersonFullName(),
            'manager_email' => $manager->getEmail(),
            'manger_phone' => $manager->getPhone(),
            'order_id' => $order->getId(),
            'order_url' => $this->router->generate('rdw_order.order.view', ['id' => $order->getId()], true),
        ];

        $message = $this->createMessage()
            ->addTo($user->getEmail())
            ->setSubject($this->translator->trans('Order at www.rdw.by has been cancelled'))
            ->setMerge(true)
            ->addMergeVars($user->getEmail(), $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param Order $order
     */
    public function sendOrderSubmittedEmailForUser(Order $order)
    {
        $templateName = 'order-submitted';

        $user = $order->getUser();

        $mergeVars = [
            'user' => $user->getTitle(),
        ];

        $message = $this->createMessage()
            ->addTo($user->getEmail())
            ->setSubject($this->translator->trans('Order at www.rdw.by submitted'))
            ->setMerge(true)
            ->addMergeVars($user->getEmail(), $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param Order $order
     */
    public function sendOrderPaidEmailForUser(Order $order)
    {
        $templateName = 'order-paid';

        $user = $order->getUser();

        $mergeVars = [
            'user' => $user->getTitle(),
        ];

        $message = $this->createMessage()
            ->addTo($user->getEmail())
            ->setSubject($this->translator->trans('Payment for order at www.rdw.by received'))
            ->setMerge(true)
            ->addMergeVars($user->getEmail(), $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param Faq $faq
     */
    public function sendAskNewFaqForAdminEmail(Faq $faq)
    {
        $templateName = 'faq-request';

        $mergeVars = [
            'name' => $faq->getName(),
            'email' => $faq->getEmail(),
            'question' => $faq->getQuestion(),
        ];

        $message = $this->createMessage()
            ->addTo($this->administratorEmail)
            ->setSubject($this->translator->trans('FAQ at rdw.by'))
            ->setMerge(true)
            ->addMergeVars($this->administratorEmail, $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param Faq $faq
     */
    public function sendFaqReplyEmail(Faq $faq)
    {
        $templateName = 'faq-reply';

        $mergeVars = [
            'name' => $faq->getName(),
            'answer' => $faq->getAnswer(),
            'question' => $faq->getQuestion(),
        ];

        $message = $this->createMessage()
            ->addTo($faq->getEmail())
            ->setSubject($this->translator->trans('FAQ question at rdw.by replied'))
            ->setMerge(true)
            ->addMergeVars($faq->getEmail(), $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param RequestMessage $requestMessage
     */
    public function sendContactFormEmail(RequestMessage $requestMessage)
    {
        $templateName = 'contact-form-email';

        $mergeVars = [
            'email' => $requestMessage->getEmail(),
            'name' => $requestMessage->getName(),
            'body' => $requestMessage->getBody(),
        ];

        $message = $this->createMessage()
            ->addTo($this->administratorEmail)
            ->setSubject($this->translator->trans('New contact form request'))
            ->setMerge(true)
            ->addMergeVars($this->administratorEmail, $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param RegisteredUser $user
     */
    public function sendDeletedUserEmail(RegisteredUser $user)
    {
        $templateName = 'account-delete';

        $mergeVars = [
            'email' => $user->getEmail(),
            'name' => $user->getName(),
        ];

        $message = $this->createMessage()
            ->addTo($this->administratorEmail)
            ->setSubject($this->translator->trans('User deleted'))
            ->setMerge(true)
            ->addMergeVars($this->administratorEmail, $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param Abuse $abuse
     */
    public function sendAbuseEmailForUser(Abuse $abuse)
    {
        if ($abuse instanceof CvAbuse) {
            $type = 'cv';
        } elseif ($abuse instanceof JobAbuse) {
            $type = 'job';
        } else {
            throw new UnexpectedTypeException($abuse, 'RDW\Bundle\AppBundle\Entity\Abuse');
        }

        $templateName = $type . '-abuse';

        $mergeVars = [
            'email' => $abuse->getUser()->getEmail(),
            'name' => $abuse->getUser()->getName(),
            /** @Ignore */
            'reason' => $this->translator->trans(sprintf('rdw_%s.%s_abuse.reason.%s', $type, $type, $abuse->getReason())),
        ];

        if (Abuse::REASON_SPAM != $abuse->getReason() && $description = $abuse->getDescription()) {
            $mergeVars['description'] = $description;
        }

        $subject = ($type === 'job') ? $this->translator->trans('Job abuse') : $this->translator->trans('Cv abuse');

        $message = $this->createMessage()
            ->addTo($abuse->getUser()->getEmail())
            ->setSubject($subject)
            ->setMerge(true)
            ->addMergeVars($abuse->getUser()->getEmail(), $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param Abuse $abuse
     * @param array|ManageUser[] $censors
     */
    public function sendAbuseEmailForCensor(Abuse $abuse, array $censors)
    {
        if ($abuse instanceof CvAbuse) {
            $type = 'cv';
        } elseif ($abuse instanceof JobAbuse) {
            $type = 'job';
        } else {
            throw new UnexpectedTypeException($abuse, 'RDW\Bundle\AppBundle\Entity\Abuse');
        }

        $templateName = 'new-' . $type . '-abuse';

        $mergeVars = [
            'user' => $abuse->getUser()->getUsername(),
            'email' => $abuse->getUser()->getEmail(),
            'company' => $abuse->getUser()->getCompanyTitle(),
            /** @Ignore */
            'reason' => $this->translator->trans(sprintf('rdw_%s.%s_abuse.reason.%s', $type, $type, $abuse->getReason())),
            'url' => ($type === 'job')
                ? $this->router->generate('rdw_job.job_admin.edit', ['id' => $abuse->getAd()->getId()], true)
                : $this->router->generate('rdw_cv.cv_admin.edit', ['id' => $abuse->getAd()->getId()], true),
        ];

        if (JobAbuse::REASON_SPAM != $abuse->getReason() && $description = $abuse->getDescription()) {
            $mergeVars['description'] = $description;
        }

        $subject = ($type === 'job')
            ? $this->translator->trans('New job abuse at rdw.by')
            : $this->translator->trans('New cv abuse at rdw.by');

        $this->sendForCensors($censors, $mergeVars, $subject, $templateName);
    }

    /**
     * @return Message
     */
    private function createMessage()
    {
        $message = new Message();
        $message->addGlobalMergeVar('date', (new \DateTime())->format('Y-m-d'));

        return $message;
    }

    /**
     * @param User $user
     */
    public function sendNewPasswordEmail(User $user)
    {
        $templateName = 'new-password';

        $mergeVars = [
            'email' => $user->getEmail(),
            'password' => $user->getPlainPassword(),
        ];

        $message = $this->createMessage()
            ->addTo($user->getEmail())
            ->setSubject($this->translator->trans('New password created'))
            ->setMerge(true)
            ->addMergeVars($user->getEmail(), $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param Job $job
     * @param array|ManageUser[] $censors
     */
    public function sendEmailForCensorAboutPublishedNewJob(Job $job, array $censors)
    {
        $templateName = 'new-job-published';

        $mergeVars = [
            'company' => $job->getCompanyTitle(),
            'email' => $job->getContactPersonEmail(),
            'url' => $this->router->generate('rdw_job.job_admin.edit', ['id' => $job->getId()], true),
        ];

        $subject = sprintf('%s (ID: %d)', $this->translator->trans('New job at rdw.by'), $job->getId());

        $this->sendForCensors($censors, $mergeVars, $subject, $templateName);
    }

    /**
     * @param Job $job
     */
    public function sendEmailForManagerWhenClientPublishedJob(Job $job)
    {
        $manager = $job->getUser()->getManager();

        if (!$manager instanceof RegisteredUser) {
            return null;
        }

        $templateName = 'new-job-published';

        $mergeVars = [
            'company' => $job->getCompanyTitle(),
            'email' => $job->getContactPersonEmail(),
            'url' => $this->router->generate('rdw_job.job.edit', ['id' => $job->getId()], true),
        ];

        $subject = sprintf('%s (ID: %d)', $this->translator->trans('New job at rdw.by'), $job->getId());

        $message = $this->createMessage()
            ->addTo($manager->getEmail())
            ->setSubject($subject)
            ->setMerge(true)
            ->addMergeVars($manager->getEmail(), $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param Job $job
     */
    public function sendEmailForUserJobApproved(Job $job)
    {
        $templateName = 'job-approved-by-censor';

        $mergeVars = [
            'username' => $job->getUser()->getName(),
            'position' => $job->getPosition()->getTitle(),
            'url' => $this->router->generate('rdw_job.job.view', ['id' => $job->getId(), 'slug' => $job->getSlug()], true),
        ];

        $email = $job->getUser()->getEmail();

        $message = $this->createMessage()
            ->addTo($email)
            ->setSubject($this->translator->trans('Your job is published at rdw.by'))
            ->setMerge(true)
            ->addMergeVars($email, $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param Job $job
     * @param ManageUser $censor
     */
    public function sendEmailForUserJobBlocked(Job $job, ManageUser $censor)
    {
        $templateName = 'job-blocked-by-censor';

        $mergeVars = [
            'username' => $job->getUser()->getName(),
            'position' => $job->getPosition()->getTitle(),
            'censor_full_name' => $censor->getFullName(),
            'censor_phone' => $censor->getPhone(),
            'censor_email' => $censor->getEmail(),
            'reason' => $job->getBlockReason(),
        ];

        $email = $job->getUser()->getEmail();

        $message = $this->createMessage()
            ->addTo($email)
            ->setSubject($this->translator->trans('Your job is blocked at rdw.by'))
            ->setMerge(true)
            ->addMergeVars($email, $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param Cv $cv
     * @param ManageUser $censor
     */
    public function sendEmailForUserCvBlocked(Cv $cv, ManageUser $censor)
    {
        $templateName = 'cv-blocked-by-censor';

        $mergeVars = [
            'username' => $cv->getUser()->getName(),
            'position' => $cv->getPosition()->getTitle(),
            'censor_full_name' => $censor->getFullName(),
            'censor_phone' => $censor->getPhone(),
            'censor_email' => $censor->getEmail(),
            'reason' => $cv->getBlockReason(),
        ];

        $email = $cv->getUser()->getEmail();

        $message = $this->createMessage()
            ->addTo($email)
            ->setSubject($this->translator->trans('Your cv is blocked at rdw.by'))
            ->setMerge(true)
            ->addMergeVars($email, $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param RegisteredUser $user
     */
    public function sendEmailForBlockedUser(RegisteredUser $user)
    {
        $templateName = 'block-user';

        $mergeVars = [
            'email' => $user->getEmail(),
            'reason' => $user->getBlockReason(),
            'company' => $user->getCompanyTitle(),
        ];

        if ($user->getManager()) {
            $mergeVars['manager'] = $user->getManager()->getContactPerson() . ' ' . $user->getManager()->getContactPersonSurname();
            $mergeVars['manager_phone'] = $user->getManager()->getPhone();
            $mergeVars['manager_email'] = $user->getManager()->getEmail();
        }

        $message = $this->createMessage()
            ->addTo($user->getEmail())
            ->setSubject($this->translator->trans('User is blocked'))
            ->setMerge(true)
            ->addMergeVars($user->getEmail(), $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param RegisteredUser $user
     */
    public function sendEmailForUnblockedUser(RegisteredUser $user)
    {
        $templateName = 'unblock-user';

        $mergeVars = [
            'email' => $user->getEmail(),
            'company' => $user->getCompanyTitle(),
        ];

        $message = $this->createMessage()
            ->addTo($user->getEmail())
            ->setSubject($this->translator->trans('User was unblocked'))
            ->setMerge(true)
            ->addMergeVars($user->getEmail(), $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param RegisteredUser $user
     */
    public function sendEmailForAdminAboutCreatedNewUser(RegisteredUser $user)
    {
        $templateName = 'new-employer-created';

        $mergeVars = [
            'email' => $user->getEmail(),
            'company' => $user->getCompanyTitle(),
        ];

        if ($user->getManager()) {
            $mergeVars['manager'] = $user->getManager()->getContactPerson() . ' ' . $user->getManager()->getContactPersonSurname();
        }

        $message = $this->createMessage()
            ->addTo($this->administratorEmail)
            ->setSubject($this->translator->trans('New employer created'))
            ->setMerge(true)
            ->addMergeVars($this->administratorEmail, $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param RegisteredUser $user
     */
    public function sendEmailForManagerAboutNewUser(RegisteredUser $user)
    {
        $templateName = 'new-employer-assigned';

        $mergeVars = [
            'email' => $user->getManager()->getEmail(),
            'manager_name' => $user->getManager()->getContactPerson(),
            'user_company_title' => $user->getCompanyTitle(),
            'user_phone' => $user->getPhone(),
            'user_email' => $user->getEmail(),
            'url' => $this->router->generate('rdw_user.user.employer_list', [], true) . '#user-' . $user->getId(),
        ];

        $message = $this->createMessage()
            ->addTo($user->getManager()->getEmail())
            ->setSubject($this->translator->trans('Your new client at site rdw.by'))
            ->setMerge(true)
            ->addMergeVars($user->getManager()->getEmail(), $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param RegisteredUser $user
     */
    public function sendEmailForUserAboutAssignedManager(RegisteredUser $user)
    {
        $templateName = 'new-manager-assigned';

        $mergeVars = [
            'email' => $user->getEmail(),
            'user_name' => $user->getName() ? $user->getName() : $user->getContactPerson(),
            'manager_name' => $user->getManager()->getContactPerson(),
            'manager_surname' => $user->getManager()->getContactPersonSurname(),
            'manager_phone' => $user->getManager()->getPhone(),
            'manager_email' => $user->getManager()->getEmail(),
        ];

        $message = $this->createMessage()
            ->addTo($user->getEmail())
            ->setSubject($this->translator->trans('Your manager at site rdw.by'))
            ->setMerge(true)
            ->addMergeVars($user->getEmail(), $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param \RDW\Bundle\ConversationBundle\Entity\Message $rdwMessage
     */
    public function sendNewMessageEmail(\RDW\Bundle\ConversationBundle\Entity\Message $rdwMessage)
    {
        $templateName = 'new-message';

        $mergeVars = [
            'email' => $rdwMessage->getReceiver()->getEmail(),
            'receiver' => $rdwMessage->getReceiver()->getName(),
            'title' => $rdwMessage->getConversation()->getTitle(),
            'message' => $rdwMessage->getContent(),
        ];

        $message = $this->createMessage()
            ->addTo($rdwMessage->getReceiver()->getEmail())
            ->setSubject($this->translator->trans('Message from %username% at www.rdw.by', ['%username%' => $rdwMessage->getSender()->getName()]))
            ->setMerge(true)
            ->addMergeVars($rdwMessage->getReceiver()->getEmail(), $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param EmailForEmployee $email
     * @param Job $job
     * @param array|RegisteredUser[] $employees
     */
    public function sendEmailForEmployees(EmailForEmployee $email, Job $job, array $employees)
    {
        $templateName = 'job-email-employees';

        $mergeVars = [
            'company_title' => $email->getCompanyTitle(),
            'text' => $email->getText(),
            'job_position' => $job->getPosition(),
            'job_url' => $this->router->generate('rdw_job.job.view', ['id' => $job->getId(), 'slug' => $job->getSlug()], true),
        ];

        $message = $this->createMessage()
            ->setSubject($this->translator->trans('New job at rdw.by'))
            ->setMerge(true);

        foreach ($employees as $user) {
            $message
                ->addTo($user->getEmail(), $user->getTitle())
                ->addMergeVars($user->getEmail(), $mergeVars);
        }

        $this->dispatcher->send($message, $templateName);
    }

    /**
     * @param RegisteredUser $user
     * @param int $price
     */
    public function sendEmailAboutBalanceAddition(RegisteredUser $user, $price)
    {
        $templateName = 'money-added-to-wallet';

        $mergeVars = [
            'username' => $user->getTitle(),
            'amount' => $price,
            'manager' => $user->getManager()->getContactPersonFullName(),
            'manager_email' => $user->getManager()->getEmail(),
        ];

        $message = $this->createMessage()
            ->addTo($user->getEmail())
            ->setSubject($this->translator->trans('Your account has been replenished at www.rdw.by'))
            ->setMerge(true)
            ->addMergeVars($user->getEmail(), $mergeVars);

        $this->dispatcher->send($message, $templateName);
    }

    private function sendForCensors(array $censors, array $mergeVars, $subject, $templateName)
    {
        $message = $this->createMessage()
            ->setSubject($subject)
            ->setMerge(true);

        foreach ($censors as $censor) {
            $mergeVars['censor_name'] = $censor->getName();

            $message
                ->addTo($censor->getEmail(), $censor->getFullName())
                ->addMergeVars($censor->getEmail(), $mergeVars);
        }

        $this->dispatcher->send($message, $templateName);
    }
}
