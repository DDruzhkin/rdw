<?php

namespace RDW\Bundle\AppBundle\Templating\Helper;

use Symfony\Component\Templating\Helper\Helper;

/**
 * Class NumberToWordHelper
 *
 * @package RDW\Bundle\AppBundle\Templating\Helper
 */
class NumberToWordHelper extends Helper
{
    /**
     * @var \Numbers_Words
     */
    private $numberToWordService;

    /**
     * @var string
     */
    private $locale;

    /**
     * @param string $locale
     */
    public function __construct($locale)
    {
        $this->numberToWordService = new \Numbers_Words();
        $this->locale = $locale;
    }

    /**
     * @param integer $number
     *
     * @return string
     */
    public function numberToWord($number)
    {
        return $this->numberToWordService->toWords($number, $this->locale);
    }

    /**
     * @param float $number
     *
     * @return string
     */
    public function numberToCurrency($number)
    {
        return $this->numberToWordService->toCurrency($number, $this->locale);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'number_to_word_helper';
    }
}
