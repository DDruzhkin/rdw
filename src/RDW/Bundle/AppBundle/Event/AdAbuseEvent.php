<?php

namespace RDW\Bundle\AppBundle\Event;

use RDW\Bundle\AppBundle\Entity\AbstractAd;
use RDW\Bundle\AppBundle\Entity\Abuse;
use RDW\Bundle\UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class AbuseEvent
 * @package RDW\Bundle\AppBundle\Event
 */
class AdAbuseEvent extends Event
{
    /**
     * @var \RDW\Bundle\AppBundle\Entity\Abuse
     */
    private $abuse;

    /**
     * @var \RDW\Bundle\AppBundle\Entity\AbstractAd
     */
    private $ad;

    /**
     * @var \RDW\Bundle\UserBundle\Entity\User
     */
    private $user;

    /**
     * @param Abuse      $abuse
     * @param AbstractAd $ad
     * @param User       $user
     */
    public function __construct(Abuse $abuse, AbstractAd $ad, User $user)
    {
        $this->abuse = $abuse;
        $this->ad = $ad;
        $this->user = $user;
    }

    /**
     * @return AbstractAd
     */
    public function getAd()
    {
        return $this->ad;
    }

    /**
     * @return \RDW\Bundle\AppBundle\Entity\Abuse
     */
    public function getAbuse()
    {
        return $this->abuse;
    }

    /**
     * @return \RDW\Bundle\UserBundle\Entity\RegisteredUser
     */
    public function getUser()
    {
        return $this->user;
    }
}
