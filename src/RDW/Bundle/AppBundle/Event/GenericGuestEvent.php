<?php

namespace RDW\Bundle\AppBundle\Event;

use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;

class GenericGuestEvent extends GenericEvent
{
    private $request;

    public function __construct(Request $request, $subject = null, $arguments = [])
    {
        parent::__construct($subject, $arguments);

        $this->request = $request;
    }

    public function getRequest()
    {
        return $this->request;
    }
}
