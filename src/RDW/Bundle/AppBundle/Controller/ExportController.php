<?php

namespace RDW\Bundle\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Class ExportController
 *
 * @package RDW\Bundle\AppBundle\Controller
 * @Route("/export")
 */
class ExportController extends Controller
{

    /**
     * @Route("/{name}.xml", name="rdw_export.export.index", requirements={"name" = "belmeta|jooble"})
     * @Method({"GET"})
     *
     * @return Response
     */
    public function exportAction($name)
    {
        $filename = $this->container->getParameter('jobs_xml_export_path') . '/' . $name .'.xml';

        if (!file_exists($filename)) {
            throw new NotFoundHttpException("Service not found");
        }

        $response = new Response();
        $response->setContent(file_get_contents($filename));
        $response->headers->set('Content-Type', 'application/xml');

        return $response;
    }
}
