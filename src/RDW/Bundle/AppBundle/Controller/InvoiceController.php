<?php

namespace RDW\Bundle\AppBundle\Controller;

use RDW\Bundle\OrderBundle\Entity\Order;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


/**
 * Class InvoiceController
 *
 * @package RDW\Bundle\AppBundle\Controller
 *
 * @Route("/invoice")
 */
class InvoiceController extends Controller
{
    /**
     * @param Order $order
     *
     * @Route("/view/{id}", name="invoice_view", requirements={"id" = "\d+"})
     * @Template()
     * @Method({"GET"})
     *
     * @return Response|RedirectResponse
     */
    public function viewAction(Order $order)
    {
        if ($order->getUser()->getId() != $this->getUser()->getId()
            && ! $order->getUser()->isUserManager($this->getUser())
        ) {
            throw new AccessDeniedException();
        }

        return [
            'order' => $order,
        ];
    }
}
