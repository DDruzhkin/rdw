<?php

namespace RDW\Bundle\AppBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;
use RDW\Bundle\AppBundle\Entity\Setting;

class SettingCRUDController extends CRUDController
{
    public function listAction()
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $repository = $this->getDoctrine()->getRepository('RDWAppBundle:Setting');

        if(!empty($_REQUEST['setting'])) {
            if ($settings = $_REQUEST['setting']) {
                foreach ($settings as $key => $item) {
                    $setting = $repository->findOneBy(['key' => $key]);

                    if ($setting instanceof Setting) {
                        $setting->setValue($item['value']);

                        $em->persist($setting);
                    }
                }

                $em->flush();
            }
        }

        return $this->render('RDWAppBundle::admin_setting.html.twig', ["settings" => $repository->findAll()]);
    }
}