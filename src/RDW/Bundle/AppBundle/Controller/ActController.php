<?php

namespace RDW\Bundle\AppBundle\Controller;

use RDW\Bundle\AppBundle\Entity\Act;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class ActController
 *
 * @package RDW\Bundle\AppBundle\Controller
 * @Route("/act")
 */
class ActController extends Controller
{
    /**
     * @param Act $act
     *
     * @Route("/view/{id}", name="act_view")
     * @Template()
     *
     * @return array
     */
    public function viewAction(Act $act)
    {
        $user = $this->getUser();

        if (!$user instanceof RegisteredUser
            || ($user != $act->getUser() && ! $act->getUser()->isUserManager($user))
        ) {
            throw new AccessDeniedHttpException();
        }

        return [
            'act' => $act,
        ];
    }
}
