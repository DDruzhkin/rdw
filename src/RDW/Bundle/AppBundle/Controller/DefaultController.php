<?php

namespace RDW\Bundle\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Default action for home page
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="rdw_app.default.index")
     * @Template()
     *
     * @return array
     */
    public function indexAction()
    {
        return [];
    }

    /**
     * @param string $storage
     * @param string $filename
     *
     * @Route("/file/{storage}/{filename}", name="rdw_app.get_file")
     *
     * @return Response
     */
    public function getFileAction($storage, $filename)
    {
        /** @var \Gaufrette\Filesystem $filesystem */
        $filesystem = $this->container->get('knp_gaufrette.filesystem_map')->get($storage.'_storage');

        /** @var \Gaufrette\File $file */
        $file = $filesystem->get($filename);

        $response = new Response();
        $response->headers->set('Content-Type', $filesystem->mimeType($file->getName()));
        $contentDisposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $file->getName());
        $response->headers->set('Content-Disposition', $contentDisposition);
        $response->setContent($file->getContent());

        return $response;
    }
}
