<?php

namespace RDW\Bundle\AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use RDW\Bundle\AppBundle\Entity\Setting;

/**
 * @Route("/manage/settings")
 */
class SettingController extends Controller
{
    /**
     * @param Request $request
     *
     * @Route("/", name="rdw_manage_settings")
     * Method({"GET", "POST"})
     * @Template()
     *
     * @return array|RedirectResponse
     */
    public function indexAction(Request $request)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $repository = $this->getDoctrine()->getRepository('RDWAppBundle:Setting');

        if ($settings = $request->request->get('setting')) {
            foreach ($settings as $key => $item) {
                $setting = $repository->findOneBy(['key' => $key]);

                if ($setting instanceof Setting) {
                    $setting->setValue($item['value']);

                    $em->persist($setting);
                }
            }

            $em->flush();
        }

        return [
            'settings' => $repository->findAll(),
        ];
    }
}
