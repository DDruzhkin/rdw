<?php

namespace RDW\Bundle\AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use RDW\Bundle\AppBundle\Entity\PositionSynonym;

/**
 * @Route("/manage/position-synonyms")
 */
class PositionSynonymController extends Controller
{
    /**
     * @Route("/list", name="rdw_manage_position_synonyms_list")
     * Method({"GET", "POST"})
     * @Template()
     *
     * @return array|RedirectResponse
     */
    public function listAction(Request $request)
    {
        $items = $this->getDoctrine()->getRepository('RDWAppBundle:PositionSynonym')->findAll();

        return [
            'items' => $items,
        ];
    }

    /**
     * @Route("/add", name="rdw_manage_position_synonyms_add")
     * @Route("/edit/{id}", name="rdw_manage_position_synonyms_edit", requirements={"id" = "\d+"})
     * @Template()
     *
     * @return array
     */
    public function formAction(Request $request, PositionSynonym $synonym = null)
    {
        if (null === $synonym) {
            $synonym = new PositionSynonym();
            $pageTitle = $this->get('translator')->trans('Add position synonym');
        } else {
            $pageTitle = $this->get('translator')->trans('Edit position synonym');
        }

        $form = $this->createForm('rdw_position_synonym_type', $synonym);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($synonym);
            $em->flush();

            $this->get('braincrafted_bootstrap.flash')->success('Synonym updated successfully');

            return $this->redirect($this->generateUrl('rdw_manage_position_synonyms_list'));
        }

        return [
            'pageTitle' => $pageTitle,
            'synonym' => $synonym,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/delete/{id}", name="rdw_manage_position_synonyms_delete", requirements={"id" = "\d+"})
     */
    public function deleteAction(PositionSynonym $synonym)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $em->remove($synonym);
        $em->flush();

        $this->get('braincrafted_bootstrap.flash')->success('Synonym deleted successfully');

        return $this->redirect($this->generateUrl('rdw_manage_position_synonyms_list'));
    }
}
