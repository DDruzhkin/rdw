<?php

namespace RDW\Bundle\AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Ddeboer\DataImport\Reader\CsvReader;
use Symfony\Component\Form\FormError;
use Symfony\Component\Routing\Exception\InvalidParameterException;

/**
 * @Route("/manage/import")
 */
class ImportController extends Controller
{
    /**
     * @param Request $request
     * @param string  $type
     *
     * @Route("/{type}", name="rdw_manage_import", requirements={"type" = "users|jobs"})
     * Method({"GET", "POST"})
     * @Template()
     *
     * @return array|RedirectResponse
     */
    public function importAction(Request $request, $type)
    {
        switch ($type) {
            case 'users':
                $formName = 'rdw_user_import';
                $serviceName = 'rdw_user_import_service';
                break;
            case 'jobs':
                $formName = 'rdw_job_import';
                $serviceName = 'rdw_job_import_service';
                break;
            default:
                throw new InvalidParameterException();
        }

        $form = $this->createForm($formName);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $filePath = $this->container->getParameter('gaufrette_path_import') . '/' . $form->get('document')->getData();
            $file = new \SplFileObject($filePath);
    		$reader = new CsvReader($file, '#');

            $importService = $this->get($serviceName);
            $importService->import($reader);

            if ($errors = $importService->getErrors()) {
                foreach ($errors as $error) {
                    $form->addError(new FormError($error));
                }

                return [
                    'form' => $form->createView(),
                    'type' => $type,
                    'importedCount' => $importService->getImportedCount(),
                    'totalRows' => $reader->count(),
                ];
            }

            $this->get('braincrafted_bootstrap.flash')->success('Succesfully imported');

            return $this->redirect($this->generateUrl('rdw_manage_import', ['type' => $type]));
        }

        return [
            'form' => $form->createView(),
            'type' => $type,
            'totalRows' => 0,
        ];
    }
}
