<?php

namespace RDW\Bundle\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class ThumbController extends Controller
{
    /**
     * @Route("/thumb/delete", name="thumb_delete")
     * @Method({"GET"})
     */
    public function deleteAction(Request $request)
    {
        $user = $this->getUser();
        $field = $request->query->get('field');
        $filename = $request->query->get('filename');

        $success = $this->get('rdw_thumb_service')->deleteThumb($field, $filename, $user);

        return new JsonResponse(['success' => $success]);
    }
}
