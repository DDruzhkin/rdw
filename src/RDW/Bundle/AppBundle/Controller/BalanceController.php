<?php

namespace RDW\Bundle\AppBundle\Controller;

use RDW\Bundle\OrderBundle\Event\OrderEvent;
use RDW\Bundle\OrderBundle\RDWOrderEvents;
use RDW\Bundle\ServiceBundle\Entity\Service;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class BalanceController
 *
 * @package RDW\Bundle\AppBundle\Controller
 *
 * @Route("/balance")
 */
class BalanceController extends Controller
{
    /**
     * @param RegisteredUser $user
     *
     * @Method({"GET"})
     * @Template
     *
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     *
     * @return array
     */
    public function addMoneyBlockAction(RegisteredUser $user)
    {
        $service = $this->getDoctrine()->getRepository('RDWServiceBundle:Service')->getBalanceAdditionService($user);

        $form = $this->createForm('rdw_balance', $service);

        return [
            'form' => $form->createView(),
            'user' => $user,
        ];
    }

    /**
     * @param Request        $request
     * @param RegisteredUser $user
     *
     * @Route("/add/{id}", name="balance_add", requirements={"id" = "\d+"})
     * @Method({"GET", "POST"})
     * @Template()
     *
     * @return Response
     */
    public function addAction(Request $request, RegisteredUser $user)
    {
        /** @var RegisteredUser $loggedInUser */
        $loggedInUser = $this->getUser();

        if ($loggedInUser->getId() != $user->getId() && ! $user->isUserManager($loggedInUser)) {
            throw new AccessDeniedException();
        }

        $service = $this->getDoctrine()->getRepository('RDWServiceBundle:Service')->getBalanceAdditionService($user);

        $form = $this->createForm('rdw_balance', $service);
        $form->handleRequest($request);

        if (! $form->isValid()) {
            return [
                'form' => $form->createView(),
                'user' => $user,
            ];
        }

        $price = $form->get('price')->getData();

        if ($loggedInUser->getId() == $user->getId()) {
            $order = $this->get('rdw_order.service.order_manager')
                ->createOrderWithBalanceAddition($user, $service, $price);

            $this->get('event_dispatcher')->dispatch(RDWOrderEvents::CREATE_SUCCESS, new OrderEvent($order));

            $response = $this->redirect($this->generateUrl('rdw_order.order.submit', ['id' => $order->getId()]));
        } elseif ($user->isUserManager($loggedInUser)) {
            $moneyFor = $this->get('translator')->trans('Account addition');
            $this->get('rdw_wallet.service.wallet_manager')->add($user, $price, $moneyFor);
            $this->get('rdw_mailer.service.mailer_manager')->sendEmailAboutBalanceAddition($user, $price);
            $this->get('braincrafted_bootstrap.flash')->success(
                $this->get('translator')->trans('You successfully added money for user')
            );

            $response = $this->redirect($this->generateUrl('rdw_order.order.list', ['id' => $user->getId()]));
        } else {
            throw new AccessDeniedException();
        }

        return $response;
    }
}
