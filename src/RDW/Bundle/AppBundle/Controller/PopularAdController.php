<?php

namespace RDW\Bundle\AppBundle\Controller;

use RDW\Bundle\CvBundle\Repository\CvRepository;
use RDW\Bundle\JobBundle\Repository\JobRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Component\HttpFoundation\Response;

class PopularAdController extends Controller
{
    /**
     * @Cache(smaxage="600")
     *
     * @return Response
     */
    public function footerBlockAction($type)
    {
        switch ($type) {
            case 'job':
                $limit = $this->container->getParameter('rdw_job.popular_items_limit');
                $repositoryName = 'RDWJobBundle:Job';
                $method = 'getJobCategoriesByFilterValues';
                $template = 'RDWAppBundle:PopularAd:jobs.html.twig';
                break;
            case 'cv':
                $limit = $this->container->getParameter('rdw_cv.popular_items_limit');
                $repositoryName = 'RDWCvBundle:Cv';
                $method = 'getCvCategoriesByFilterValues';
                $template = 'RDWAppBundle:PopularAd:cvs.html.twig';
                break;
            default:
                throw new \InvalidArgumentException("Wrong type");
        }

        $repository = $this->getDoctrine()->getRepository($repositoryName);
        $scopes = $repository->getPopularScopes($limit);
        $positionProvider = $this->container->get('rdw_job.provider.position');
        $positions = $positionProvider->getPopular(1, $limit);
        $cities = $repository->getPopularCities($limit);
        $professionalAreaProvider = $this->container->get('rdw_job.provider.professional_area');
        $professionalAreas = $professionalAreaProvider->getPopular(1, $limit);
        $customPopularItems = $this->container->get('popular_ad_service')->{$method}();

        return $this->render($template, [
            'scopes' => $scopes,
            'professionalAreas' => $professionalAreas,
            'positions' => $positions,
            'cities' => $cities,
            'customPopularItems' => $customPopularItems,
        ]);
    }

    /**
     * @Cache(smaxage="600")
     *
     * @return Response
     */
    public function jobByCitiesAction($type)
    {
        switch ($type) {
            case 'job':
                $limit = $this->container->getParameter('rdw_job.popular_items_limit');
                $repositoryName = 'RDWJobBundle:Job';
                $method = 'getJobCategoriesByFilterValues';
                $template = 'RDWAppBundle:PopularAd:jobsByCities.html.twig';
                break;
            case 'cv':
                $limit = $this->container->getParameter('rdw_cv.popular_items_limit');
                $repositoryName = 'RDWCvBundle:Cv';
                $method = 'getCvCategoriesByFilterValues';
                $template = 'RDWAppBundle:PopularAd:cvsByCities.html.twig';
                break;
            default:
                throw new \InvalidArgumentException("Wrong type");
        }

        /** @var JobRepository|CvRepository $repository*/
        $repository = $this->getDoctrine()->getRepository($repositoryName);
        $cities = $repository->getPopularCities($limit);

        return $this->render($template, [
            'cities' => $cities
        ]);
    }
}
