<?php

namespace RDW\Bundle\AppBundle\Behat;

/**
 * Class WebContext. Command features for app
 */
class WebContext extends DefaultContext
{
    /**
     * @Given /^I should be on home page$/
     */
    public function iShouldBeOnHomePage()
    {
        $this->assertSession()->addressEquals($this->generateUrl('rdw_app.default.index'));
    }

    /**
     * @Given /^I should be on dashboard$/
     */
     public function isShouldBeOnDashboard()
     {
         $this->assertSession()->addressEquals($this->generateUrl('rdw_user.user.dashboard'));
     }

    /**
     * @Given /^I am on the job create page$/
     */
    public function iAmOnTheJobCreatePage()
    {
        $this->getSession()->visit($this->generatePageUrl('rdw_job.job.create'));
    }

    /**
     * @Given /^I am on the cv create page$/
     */
    public function iAmOnTheCvCreatePage()
    {
        $this->getSession()->visit($this->generatePageUrl('rdw_cv.cv.new'));
    }

    /**
     * For example: I should see 10 products in that list.
     *
     * @param int    $amount
     * @param string $type
     *
     * @Then /^I should see (\d+) ([^""]*) in (that|the) list$/
     */
    public function iShouldSeeThatMuchResourcesInTheList($amount, $type)
    {
        if (1 === count($this->getSession()->getPage()->findAll('css', 'table'))) {
            $this->assertSession()->elementsCount('css', 'table tbody > tr', $amount);
        } else {
            $this->assertSession()->elementsCount('css', sprintf('ul.%s > li', str_replace(' ', '-', $type)), $amount);
        }
    }

    /**
     * For example: I should see 10 in the "element_id".
     *
     * @param int    $amount
     * @param string $id
     *
     * @Then /^I should see (\d+) in the "([^"]*)"$/
     */
    public function iShouldSeeTextInTheElement($amount, $id)
    {
        $this->assertSession()->elementTextContains('css', sprintf('#%s', $id), $amount);
    }

    /**
     * @param string $formName
     *
     * @Given /^I should see "([^""]*)" form$/
     */
    public function iShouldSeeSomeForm($formName)
    {
        $this->assertSession()->elementExists('css', 'form[name="' . $formName . '"]');
    }

    /**
     * @param string $param
     *
     * @Given /^I fill captcha validator "([^"]*)"$/
     */
    public function iFillCaptchaValidator($param)
    {
        $session = $this->getService('session');

        if ($param == 'correctly') {

            $digit1 = $session->get('_digit1');
            $digit2 = $session->get('_digit2');
            $this->getSession()->getPage()->fillField('rdw_sum_captcha', $digit1 + $digit2);
        } else {
            //fill incorrect
            $this->getSession()->getPage()->fillField('rdw_sum_captcha', 500);
        }
    }

	/**
     * For test reasons
     *
     * @param string $arg1
     *
     * @Given /^I should seee "([^"]*)"$/
     */
    public function iShouldSeee($arg1)
    {
        $page = $this->getSession()->getPage();
        echo '<pre>';
        print_r($page->getContent());
        echo '</pre>';
        die();
    }
}
