<?php

namespace RDW\Bundle\AppBundle\Collection;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class FiltersCollection
 *
 * @package RDW\Bundle\AppBundle\Collection
 */
class FiltersCollection extends ArrayCollection
{
    /**
     * @var bool
     */
    private $sortable;

    /**
     * @param array $elements
     * @param bool  $sortable
     */
    public function __construct(array $elements = array(), $sortable = false)
    {
        parent::__construct($elements);

        $this->sortable = $sortable;
    }

    /**
     * @return bool
     */
    public function isSortable()
    {
        return $this->sortable;
    }
}
