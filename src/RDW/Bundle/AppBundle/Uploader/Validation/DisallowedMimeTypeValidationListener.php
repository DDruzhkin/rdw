<?php

namespace RDW\Bundle\AppBundle\Uploader\Validation;

use Oneup\UploaderBundle\Event\ValidationEvent;
use Oneup\UploaderBundle\Uploader\Exception\ValidationException;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class DisallowedMimeTypeValidationListener
 *
 * @package Oneup\UploaderBundle\EventListener
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class DisallowedMimeTypeValidationListener
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param ValidationEvent $event
     */
    public function onValidate(ValidationEvent $event)
    {
        $config = $event->getConfig();
        $file   = $event->getFile();

        if (count($config['disallowed_mimetypes']) == 0) {
            return;
        }

        $mimeType = $file->getExtension();

        if (in_array($mimeType, $config['disallowed_mimetypes'])) {
            throw new ValidationException($this->translator->trans('Type of attached file is disallowed'));
        }
    }
}
