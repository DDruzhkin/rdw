<?php

namespace RDW\Bundle\AppBundle\Uploader\Validation;

use Oneup\UploaderBundle\Event\ValidationEvent;
use Oneup\UploaderBundle\Uploader\Exception\ValidationException;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class AllowedMimeTypeValidationListener
 *
 * @package RDW\Bundle\AppBundle\Uploader\Validation
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class AllowedMimeTypeValidationListener
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param ValidationEvent $event
     */
    public function onValidate(ValidationEvent $event)
    {
        $config = $event->getConfig();
        $file   = $event->getFile();

        if (count($config['allowed_mimetypes']) == 0) {
            return;
        }

        $mimeType = $file->getMimeType();

        if ( ! in_array($mimeType, $config['allowed_mimetypes'])) {
            throw new ValidationException($this->translator->trans('Type of attached file is not allowed'));
        }
    }
}
