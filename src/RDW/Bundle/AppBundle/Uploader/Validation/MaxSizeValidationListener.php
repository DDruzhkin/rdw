<?php

namespace RDW\Bundle\AppBundle\Uploader\Validation;

use Oneup\UploaderBundle\Event\ValidationEvent;
use Oneup\UploaderBundle\Uploader\Exception\ValidationException;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class MaxSizeValidationListener
 *
 * @package RDW\Bundle\AppBundle\Uploader\Validation
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class MaxSizeValidationListener
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param ValidationEvent $event
     */
    public function onValidate(ValidationEvent $event)
    {
        $config = $event->getConfig();
        $file   = $event->getFile();

        if ($file->getSize() > $config['max_size']) {
            $limit = round($config['max_size'] / 1024 / 1024, 2);
            $message = 'Attached file size is too big. Max allowed file size is %s megabytes';

            /** @Ignore */
            $translatedMessage = $this->translator->trans($message, ['%s' => $limit]);

            throw new ValidationException($translatedMessage);
        }
    }
}
