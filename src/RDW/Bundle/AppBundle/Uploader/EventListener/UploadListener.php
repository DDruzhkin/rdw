<?php

namespace RDW\Bundle\AppBundle\Uploader\EventListener;

use Avalanche\Bundle\ImagineBundle\Imagine\CachePathResolver;
use Oneup\UploaderBundle\Event\PostPersistEvent;

/**
 * Class UploadListener
 *
 * @package RDW\Bundle\AppBundle\Uploader\EventListener
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class UploadListener
{
    /**
     * @var CachePathResolver
     */
    private $cacheResolver;

    /**
     * @param CachePathResolver $cacheResolver
     */
    public function __construct(CachePathResolver $cacheResolver)
    {
        $this->cacheResolver = $cacheResolver;
    }

    /**
     * @param PostPersistEvent $event
     */
    public function onUpload(PostPersistEvent $event)
    {
        $file = $event->getFile();

        $filter = $event->getRequest()->request->get('filter');

        $response = $event->getResponse();
        $response['success'] = true;

        $response['file_name'] = $file->getName();

        // uploaded file is image, get path for it
        if ($filter) {
            $response['file_path'] = $this->cacheResolver->getBrowserPath($file->getName(), $filter, true);
        }
    }
}
