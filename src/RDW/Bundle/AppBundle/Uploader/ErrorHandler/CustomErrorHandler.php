<?php

namespace RDW\Bundle\AppBundle\Uploader\ErrorHandler;

use Exception;
use Oneup\UploaderBundle\Uploader\ErrorHandler\ErrorHandlerInterface;
use Oneup\UploaderBundle\Uploader\Response\AbstractResponse;

/**
 * Class CustomErrorHandler
 *
 * @package RDW\Bundle\AppBundle\Uploader\ErrorHandler
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class CustomErrorHandler implements ErrorHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public function addException(AbstractResponse $response, Exception $exception)
    {
        $message = $exception->getMessage();

        $response['success'] = false;
        $response['error'] = $message;
    }
}
