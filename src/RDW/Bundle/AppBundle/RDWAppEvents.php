<?php

namespace RDW\Bundle\AppBundle;

/**
 * Class RDWAppEvents
 * @package RDW\Bundle\AppBundle
 */
final class RDWAppEvents
{
    /**
     * This event occurs then user abuses a ad
     */
    const AD_ABUSE = 'rdw_app.event.abuse';
}
