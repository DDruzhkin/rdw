<?php

namespace RDW\Bundle\AppBundle\Service;

use Foolz\SphinxQL\Drivers\SimpleConnection;
use Foolz\SphinxQL\SphinxQL;

class AutoCorrectService
{
    const LENGTH_THRESHOLD = 2;
    const LEVENSHTEIN_THRESHOLD = 2;
    const TOP_COUNT = 5;

    private $connection;

    public function __construct($host, $port)
    {
        $this->connection = new SimpleConnection();
        $this->connection->setParams(array('host' => $host, 'port' => $port));
    }

    public function buildTrigrams($keyword)
    {
    	$t = "__" . $keyword . "__";
        $t = trim($t);

    	$trigrams = "";
    	for ($i = 0; $i < mb_strlen($t) - 2; $i++)
    		$trigrams .= mb_substr($t, $i, 3) . " ";

    	return $trigrams;
    }

    public function makePhraseSuggestion($words, $query)
    {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            return false;
        }

        //we don't to search in stemmed words
        $words = $this->removeStemmerForms($words);

    	$suggested = array();
    	$llimf = 0;
    	$i = 0;
        $mis = [];

    	foreach ($words  as $key => $word) {
    		if ($word['docs'] != 0) {
                $llimf += $word['docs'];
            }

            $i++;
    	}

        if ($i > 0) {
            $llimf = $llimf / ($i * $i);
        }

    	foreach ($words  as $key => $word) {
    		if ((0 == $word['docs']) | ($word['docs'] < $llimf)) {
    			$mis[] = $word['keyword'];
    		}
    	}

    	if (count($mis) > 0) {
    		foreach ($mis as $m) {
    			$re = $this->makeSuggestion($m);

    			if ($re && $m != $re) {
					$suggested[$m] = $re;
    			}
    		}

    		if(1 === count($words) && empty($suggested)) {
    			return false;
    		}

    		$phrase = explode(' ', $query);

    		foreach ($phrase as $key => $word) {
    			if (isset($suggested[mb_strtolower($word)])) {
                    $phrase[$key] = $suggested[mb_strtolower($word)];
                }
    		}

    		return implode(' ', $phrase);
    	} else{
    		return false;
    	}
    }

    private function makeSuggestion($keyword)
    {
    	$trigrams = $this->buildTrigrams($keyword);

        $delta = self::LENGTH_THRESHOLD;
        $limit = self::TOP_COUNT;
        $query = "\"$trigrams\"/1";
        $length = mb_strlen($keyword);
        $minLength = $length - $delta;
        $maxLength = $length + $delta;


        $q = sprintf(
            "SELECT *, weight() as w, w+%d-ABS(len-%d) as myrank FROM suggest WHERE MATCH('%s') AND len BETWEEN %d AND %d ORDER BY myrank DESC, freq DESC LIMIT 0,%d OPTION ranker=wordcount",
            $delta, $length, $query, $minLength, $maxLength, $limit
        );

        $lnq = SphinxQL::create($this->connection);

        $rows = $lnq->query($q)->execute();

    	if (empty($rows)) {
            return false;
        }

    	// further restrict trigram matches with a sane Levenshtein distance limit
    	foreach ($rows as $match) {
    		$suggested = $match["keyword"];

    		if (levenshtein($keyword, $suggested) <= self::LEVENSHTEIN_THRESHOLD) {
                return $suggested;
            }
    	}

    	return $keyword;
    }

    private function removeStemmerForms($words)
    {
        $result = [];

        foreach ($words as $key => $word) {
    		if (mb_strpos($word['keyword'], '*') !== false) {
                $word['keyword'] = str_replace('*', '', $word['keyword']);
                $result[] = $word;
            }
    	}

        return $result;
    }
}
