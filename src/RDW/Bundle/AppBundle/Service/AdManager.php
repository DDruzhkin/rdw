<?php

namespace RDW\Bundle\AppBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use RDW\Bundle\AppBundle\Entity\AdInterface;
use RDW\Bundle\HistoryBundle\Entity\History;
use RDW\Bundle\HistoryBundle\RDWHistoryEvents;
use RDW\Bundle\HistoryBundle\Service\HistoryManager;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Knp\Component\Pager\Paginator;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class AdManager
 * @package RDW\Bundle\AppBundle\Service
 */
class AdManager
{
    /**
     * @var ObjectManager|EntityManager
     */
    protected $objectManager;

    /**
     * @var Paginator
     */
    protected $paginator;

    /**
     * @var HistoryManager
     */
    protected $historyManager;

    /**
     * @param ObjectManager  $objectManager
     * @param Paginator      $paginator
     * @param HistoryManager $historyManager
     */
    public function __construct(ObjectManager $objectManager, Paginator $paginator, HistoryManager $historyManager)
    {
        $this->objectManager = $objectManager;
        $this->paginator = $paginator;
        $this->historyManager = $historyManager;
    }

    /**
     * @return ObjectManager|EntityManager
     */
    public function getEntityManager()
    {
        return $this->objectManager;
    }

    /**
     * @param AdInterface $ad
     * @param bool        $andFlush
     *
     * @return bool
     */
    public function update(AdInterface $ad, $andFlush = true)
    {
        $this->objectManager->persist($ad);

        if ($andFlush) {
            $this->objectManager->flush();
        }

        return true;
    }

    /**
     * @param $object
     *
     * @return object
     */
    public function merge($object)
    {
        return $this->objectManager->merge($object);
    }

    public function detach($object)
    {
        $this->objectManager->detach($object);
    }

    /**
     * @param AdInterface $ad
     */
    public function delete(AdInterface $ad)
    {
        $this->objectManager->remove($ad);
        $this->objectManager->flush();
    }

    /**
     * @param ArrayCollection|AdInterface[] $ads
     * @param RegisteredUser                $user
     */
    public function multipleDelete(ArrayCollection $ads, RegisteredUser $user)
    {
        foreach ($ads as $ad) {
            if ($ad->isAllowedToDeleteByUser($user)) {
                $this->objectManager->remove($ad);

                $this->historyManager->logEvent($user, History::ACTION_DELETE, $ad, RDWHistoryEvents::DELETE_OBJECT);
            }
        }

        $this->objectManager->flush();
    }

    /**
     * map entity from Session
     *
     * @param Session $session
     *
     * @return object
     */
    public function map(Session $session)
    {
        return $this->objectManager->merge($session->get('ad'));
    }
}
