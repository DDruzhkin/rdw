<?php

namespace RDW\Bundle\AppBundle\Service;

/**
 * Class SalaryRangeService
 *
 * @package RDW\Bundle\AppBundle\Service
 */
class SalaryRangeService
{
    /**
     * @var SphinxSearchService
     */
    private $sphinxSearchService;

    /**
     * @var array
     */
    private $jobIntervals;

    /**
     * @var array
     */
    private $cvIntervals;

    /**
     * @param SphinxSearchService $sphinxSearchService
     */
    public function __construct(SphinxSearchService $sphinxSearchService)
    {
        $this->sphinxSearchService = $sphinxSearchService;

        $this->loadIntervals();
    }

    /**
     * @return array
     */
    public function getJobSalaryIntervals()
    {
        return $this->formatChoiceList($this->jobIntervals);
    }

    /**
     * @return array
     */
    public function getCvSalaryIntervals()
    {
        return $this->formatChoiceList($this->cvIntervals);
    }

    private function formatChoiceList($intervals)
    {
        $results = [];

        foreach ($intervals as $interval) {
            $results[$interval] = number_format($interval, 0, '.', ' ');
        }

        return $results;
    }

    /**
     * @return void
     */
    private function loadIntervals()
    {
        if (empty($this->jobIntervals)) {
            $this->jobIntervals = $this->sphinxSearchService->getJobSalaryIntervals();
        }

        if (empty($this->cvIntervals)) {
            $this->cvIntervals = $this->sphinxSearchService->getCvSalaryIntervals();
        }
    }

}
