<?php

namespace RDW\Bundle\AppBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\AppBundle\Entity\AbstractAd;
use RDW\Bundle\ManageBundle\Entity\User as Admin;

class ThumbService
{
    const FIELD_CV = 'cv';
    const FIELD_JOB = 'job';
    const FIELD_JOB_CONTACT_PERSON = 'job_contact';
    const FIELD_PROFILE = 'profile';
    const FIELD_PROFILE_CONTACT_PERSON = 'profile_contact';

    private $em;

    private $fieldsMap = [
        self::FIELD_CV => 'photo',
        self::FIELD_JOB => 'photo',
        self::FIELD_JOB_CONTACT_PERSON => 'contactPersonPhoto',
        self::FIELD_PROFILE => 'photo',
        self::FIELD_PROFILE_CONTACT_PERSON => 'contactPersonPhoto',
    ];

    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

    public function deleteThumb($fieldType, $filename, $loggedInUser)
    {
        // check field
        if (! array_key_exists($fieldType, $this->fieldsMap)) {
            throw new \RuntimeException("Wrong field map!");
        }

        $repository = $this->getRepository($fieldType);
        $property = $this->fieldsMap[$fieldType];

        $object = $repository->findOneBy([$property => $filename]);

        if (! $this->isAllowed($object, $loggedInUser)) {
            return false;
        }

        $object->{'set' . ucfirst($property)}(null);

        $this->em->persist($object);
        $this->em->flush();

        return true;
    }

    private function isAllowed($object, $loggedInUser)
    {
        if ($loggedInUser instanceof Admin) {
            return true;
        }

        if ($object instanceof RegisteredUser) {
            return ($loggedInUser->getId() == $object->getId() || $object->isUserManager($loggedInUser));
        } elseif ($object instanceof AbstractAd) {
            return $object->isAllowedToDeleteByUser($loggedInUser);
        }

        return false;
    }

    private function getRepository($field)
    {
        switch ($field) {
            case self::FIELD_CV:
                return $this->em->getRepository('RDWCvBundle:Cv');
            case self::FIELD_JOB:
            case self::FIELD_JOB_CONTACT_PERSON:
                return $this->em->getRepository('RDWJobBundle:Job');
            case self::FIELD_PROFILE:
            case self::FIELD_PROFILE_CONTACT_PERSON:
                return $this->em->getRepository('RDWUserBundle:RegisteredUser');
            default:
                throw new \RuntimeException("Repository by field not found!");
        }
    }
}
