<?php

namespace RDW\Bundle\AppBundle\Service;

use Ddeboer\DataImport\Reader\CsvReader;
use Symfony\Component\Validator\Validator;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\Scope;
use RDW\Bundle\UserBundle\Entity\CompanyType;
use RDW\Bundle\LocationBundle\Entity\City;

class UserImport extends AbstractImportService
{
	const COLUMNS_COUNT = 35;

	public function import(CsvReader $reader)
	{
		$this->loadKeys($reader);

		$this->om->getConnection()->getConfiguration()->setSQLLogger(null);

		$userRepository = $this->om->getRepository('RDWUserBundle:RegisteredUser');
		$scopeRepository = $this->om->getRepository('RDWUserBundle:Scope');
		$cityRepository = $this->om->getRepository('RDWLocationBundle:City');
		$companyTypeRepository = $this->om->getRepository('RDWUserBundle:CompanyType');

		$rowNumber = 0;
		foreach ($reader as $row) {
			$rowNumber++;

			if (! $this->checkRowsCount(self::COLUMNS_COUNT, $row, $rowNumber)) {
				continue;
			}

			$item = $this->buildItem($row);

            $user = $userRepository->findOneBy(['foreignId' => $item->crm_id]);
			if ($user) {
				$this->addError(sprintf('Row %d. User %d already imported.', $rowNumber, $item->crm_id));
				$this->updateManager($userRepository, $user, $item->manager_id, $rowNumber);
				continue;
			}

            if ($user = $userRepository->findOneBy(['email' => $item->email])) {
                $this->addError(sprintf('Row %d. User %s already imported.', $rowNumber, $item->email));
				$this->updateManager($userRepository, $user, $item->manager_id, $rowNumber);
				$this->updateForeignId($userRepository, $user, $item->crm_id, $rowNumber);
				continue;
			}

			if (! $this->validate($row, $rowNumber)) {
				continue;
			}

			$insertValues = [];
			$insertValues['foreign_id'] = $item->crm_id;
			$insertValues['email'] = $item->email;
			$insertValues['employer_type'] = $item->employer_type;
			$insertValues['company_title'] = $item->company_title;
			$insertValues['url_address'] = $item->company_url;
			$insertValues['company_info'] = $item->company_info;
			$insertValues['address'] = $item->address;
			$insertValues['phone'] = $item->phone;
			$insertValues['fax'] = $item->fax;
			$insertValues['name'] = $item->name;
			$insertValues['surname'] = $item->surname;
			$insertValues['gender'] = $item->gender;
			$insertValues['birthday'] = ($item->birthday) ? $item->birthday : null;
			$insertValues['bank_account'] = $item->bank_account;
			$insertValues['bank_name'] = $item->bank_name;
			$insertValues['bank_code'] = $item->bank_code;
			$insertValues['patronymic'] = $item->patronymic;
			$insertValues['contact_person'] = $item->contact_person_name;
			$insertValues['contact_person_surname'] = $item->contact_person_surname;
			$insertValues['contact_person_phone'] = $item->contact_person_phone;
			$insertValues['contact_person_email'] = $item->contact_person_email;
			$insertValues['fb_contact'] = $item->fb_contact;
			$insertValues['vk_contact'] = $item->vk_contact;
			$insertValues['ok_contact'] = $item->ok_contact;
			$insertValues['g_plus_contact'] = $item->g_plus_contact;
			$insertValues['linked_in_contact'] = $item->linkedin_contact;
			$insertValues['twitter_contact'] = $item->twitter_contact;
			$insertValues['skype_contact'] = $item->skype_contact;
			$insertValues['password'] = sha1(time() . mt_rand() . $insertValues['email']);
			$insertValues['salt'] = sha1(time() . mt_rand() . $insertValues['password']);
			$insertValues['username'] = $item->email;
			$insertValues['username_canonical'] = $item->email;
			$insertValues['unp_code'] = $item->unp_code;
			$insertValues['email_canonical'] = $item->email;
			$insertValues['object_type'] = RegisteredUser::OBJECT_TYPE_REGISTERED;
			$insertValues['type'] = RegisteredUser::USER_TYPE_EMPLOYER;
			$insertValues['roles'] = 'a:1:{i:0;s:13:"ROLE_EMPLOYER";}';
			$insertValues['deleted'] = 0;
			$insertValues['expired'] = 0;
			$insertValues['is_unlimited'] = 0;
			$insertValues['credentials_expired'] = 0;
			$insertValues['locked'] = 0;
			$insertValues['enabled'] = 1;

			if (! empty($item->manager_id) && $manager = $userRepository->find($item->manager_id)) {
				$insertValues['manager_id'] = $item->manager_id;
			}

			$companyType = $companyTypeRepository->findOneBy(['title' => $item->company_type]);
			if ($companyType instanceof CompanyType) {
				$insertValues['company_type_id'] = $companyType->getId();
			} else {
				$this->addError(sprintf('Row %d. Company type %s was not found.', $rowNumber, $item->company_type));
				continue;
			}

			$scope = $scopeRepository->findOneBy(['title' => $item->scope]);
			if ($scope instanceof Scope) {
				$insertValues['scope_id'] = $scope->getId();
			}

			$workingPlaceCity = $cityRepository->findOneBy(['title' => $item->working_place_city]);
			if ($workingPlaceCity instanceof City) {
				$insertValues['working_place_city_id'] = $workingPlaceCity->getId();
			}

			$fields = [];
			$params = [];
			foreach ($insertValues as $field => $value) {
				$fields[] = sprintf('`%s`', $field);
				$params[] = sprintf(':%s', $field);
			}

			$sql = 'INSERT INTO `users` ('.implode(', ', $fields).') VALUES ('.implode(', ', $params).')';
			$statement = $this->om->getConnection()->prepare($sql);
			$statement->execute($insertValues);

			$this->increaseImportedCount();
		}

	}

	private function validate($row, $rowNumber)
	{
		$valid = true;
		$missing = [];
		$required = ['email', 'crm_id', 'employer_type', 'company_title', 'company_type'];

		foreach ($required as $field) {
			if (!array_key_exists($field, $row)) {
				$missing[] = $field;
				$valid = false;
			}
		}

		if (! $valid) {
			$this->addError(sprintf('Row %d. Missing required fields (%s)', $rowNumber, implode(', ', $missing)));
		}

		return $valid;
	}

	private function updateManager($userRepository, RegisteredUser $user, $managerId, $rowNumber)
	{
		if ($managerId > 0 && (
			(null == $user->getManager())
			|| $managerId != $user->getManager()->getId()
		)) {
			$manager = $userRepository->find($managerId);

			if ($manager instanceof RegisteredUser && $manager->hasRole('ROLE_MANAGER')) {
				$user->setManager($manager);
				$user->setHasManagerFrom(new \DateTime());

				$this->om->persist($user);
				$this->om->flush();

				$this->addError(sprintf('Row %d. Manager updated for user (%s). Assigned manager: %s', $rowNumber, $user->getEmail(), $manager->getEmail()));
			}
		}
	}

	private function updateForeignId($userRepository, RegisteredUser $user, $crmId, $rowNumber)
	{
		if ((int) $crmId === 0) {
			return;
		}

		$user->setForeignId($crmId);

		$this->om->persist($user);
		$this->om->flush();

		$this->addError(sprintf('Row %d. CRM id updated for user (%s). New crm id: %d', $rowNumber, $user->getEmail(), $user->getForeignId()));
	}
}
