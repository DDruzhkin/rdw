<?php

namespace RDW\Bundle\AppBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\ManageBundle\Service\UserManager;
use RDW\Bundle\MailerBundle\Service\MailerManager;
use RDW\Bundle\OrderBundle\Service\OrderManager;
use RDW\Bundle\OrderBundle\RDWOrderStatus;
use RDW\Bundle\OrderBundle\Entity\Order;
use RDW\Bundle\OrderBundle\Entity\OrderItem;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\ServiceBundle\Entity\Service;
use Doctrine\ORM\EntityNotFoundException;

/**
 * Class JobPublishingService
 *
 * @package RDW\Bundle\AppBundle\Service
 */
class JobPublishingService
{
    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var ObjectManager
     */
    private $entityManager;

    /**
     * @var MailerManager
     */
    private $mailerManager;

    /**
     * @var OrderManager
     */
    private $orderManager;

    /**
     * @param UserManager   $userManager
     * @param ObjectManager $entityManager
     * @param MailerManager $mailerManager
     * @param OrderManager  $orderManager
     */
    public function __construct(
        UserManager $userManager,
        ObjectManager $entityManager,
        MailerManager $mailerManager,
        OrderManager $orderManager
    )
    {
        $this->userManager = $userManager;
        $this->entityManager = $entityManager;
        $this->mailerManager = $mailerManager;
        $this->orderManager = $orderManager;
    }

    /**
     * @param Job $job
     *
     * @return boolean
     */
    public function publish(Job $job)
    {
        $job->setStatus(Job::STATUS_WAITING);

        $censors = $this->userManager->getAllCensors();
        $this->mailerManager->sendEmailForCensorAboutPublishedNewJob($job, $censors);
        $this->mailerManager->sendEmailForManagerWhenClientPublishedJob($job);

        $this->entityManager->persist($job);
        $this->entityManager->flush();

        return true;
    }
}
