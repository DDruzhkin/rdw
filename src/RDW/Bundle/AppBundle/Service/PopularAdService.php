<?php

namespace RDW\Bundle\AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Routing\RouterInterface;
use RDW\Bundle\ManageBundle\Entity\Filter;

class PopularAdService
{
    const TYPE_JOB = 'job';
    const TYPE_CV = 'cv';

    private $sphinx;
    private $em;
    private $translator;
    private $router;

    public function __construct(
        SphinxSearchService $sphinx,
        EntityManager $em,
        TranslatorInterface $translator,
        RouterInterface $router
    )
    {
        $this->sphinx = $sphinx;
        $this->em = $em;
        $this->translator = $translator;
        $this->router = $router;
    }

    public function getJobCategoriesByFilterValues()
    {
        $categoriesConfig = [
            'without_education' => [
                'repository' => 'RDWJobBundle:EducationType',
                'type' => \RDW\Bundle\JobBundle\Entity\EducationType::TYPE_WITHOUT_EDUCATION,
                'title' => 'Jobs without education',
                'field' => 'education_type_id'
            ],
            'without_work_experience' => [
                'repository' => 'RDWJobBundle:WorkExperienceType',
                'type' => \RDW\Bundle\JobBundle\Entity\WorkExperienceType::TYPE_NO_EXPERIENCE,
                'title' => 'Jobs without work experience',
                'field' => 'work_experience_type_id'
            ],
            'work_schedule_type_alternative' => [
                'repository' => 'RDWJobBundle:WorkScheduleType',
                'type' => \RDW\Bundle\JobBundle\Entity\WorkScheduleType::TYPE_ALTERNATIVE,
                'title' => 'Jobs with work schedule type alternative',
                'field' => 'work_schedule_type_id'
            ],
            'employment_type_partial' => [
                'repository' => 'RDWJobBundle:EmploymentType',
                'type' => \RDW\Bundle\JobBundle\Entity\EmploymentType::TYPE_PARTIAL,
                'title' => 'Jobs with employment type partial',
                'field' => 'employment_type_id'
            ],
            'work_schedule_type_remote' => [
                'repository' => 'RDWJobBundle:WorkScheduleType',
                'type' => \RDW\Bundle\JobBundle\Entity\WorkScheduleType::TYPE_REMOTE,
                'title' => 'Jobs with work schedule remote',
                'field' => 'work_schedule_type_id'
            ],
            'work_schedule_type_flexible' => [
                'repository' => 'RDWJobBundle:WorkScheduleType',
                'type' => \RDW\Bundle\JobBundle\Entity\WorkScheduleType::TYPE_FLEXIBLE,
                'title' => 'Jobs with work schedule flexible',
                'field' => 'work_schedule_type_id'
            ],
        ];

        return $this->getCategoriesByFilterValues($categoriesConfig, self::TYPE_JOB);
    }

    public function getCvCategoriesByFilterValues()
    {
        $categoriesConfig = [
            'without_education' => [
                'repository' => 'RDWJobBundle:EducationType',
                'type' => \RDW\Bundle\JobBundle\Entity\EducationType::TYPE_WITHOUT_EDUCATION,
                'title' => 'Cvs without education',
                'field' => 'education_ids'
            ],
            'without_work_experience' => [
                'repository' => 'RDWJobBundle:WorkExperienceType',
                'type' => \RDW\Bundle\JobBundle\Entity\WorkExperienceType::TYPE_NO_EXPERIENCE,
                'title' => 'Cvs without work experience',
                'field' => 'work_experience_type_id'
            ],
            'work_schedule_type_alternative' => [
                'repository' => 'RDWJobBundle:WorkScheduleType',
                'type' => \RDW\Bundle\JobBundle\Entity\WorkScheduleType::TYPE_ALTERNATIVE,
                'title' => 'Cvs with work schedule type alternative',
                'field' => 'work_schedule_type_id'
            ],
            'employment_type_partial' => [
                'repository' => 'RDWJobBundle:EmploymentType',
                'type' => \RDW\Bundle\JobBundle\Entity\EmploymentType::TYPE_PARTIAL,
                'title' => 'Cvs with employment type partial',
                'field' => 'employment_type_id'
            ],
            'work_schedule_type_remote' => [
                'repository' => 'RDWJobBundle:WorkScheduleType',
                'type' => \RDW\Bundle\JobBundle\Entity\WorkScheduleType::TYPE_REMOTE,
                'title' => 'Cvs with work schedule remote',
                'field' => 'work_schedule_type_id'
            ],
            'work_schedule_type_flexible' => [
                'repository' => 'RDWJobBundle:WorkScheduleType',
                'type' => \RDW\Bundle\JobBundle\Entity\WorkScheduleType::TYPE_FLEXIBLE,
                'title' => 'Cvs with work schedule flexible',
                'field' => 'work_schedule_type_id'
            ],
        ];

        return $this->getCategoriesByFilterValues($categoriesConfig, self::TYPE_CV);
    }

    private function getCategoriesByFilterValues(array $categoriesConfig, $type)
    {
        $categories = [];
        $sphinxMethod = (self::TYPE_JOB === $type) ? 'getJobItemsCountByFilterValue' : 'getCvItemsCountByFilterValue';

        foreach ($categoriesConfig as $config) {
            $filterItem = $this->em->getRepository($config['repository'])->findOneBy(['type' => $config['type']]);

            if ($filterItem instanceof Filter) {
                $categories[] = [
                    /** @Ignore */
                    'title' => $this->translator->trans($config['title']),
                    'total' => $this->sphinx->{$sphinxMethod}($filterItem, $config['field']),
                    'item' => $filterItem
                ];
            }
        }

        return $categories;
    }
}
