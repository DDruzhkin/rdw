<?php

namespace RDW\Bundle\AppBundle\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Connection;
use Foolz\SphinxQL\Drivers\MultiResultSetInterface;
use Foolz\SphinxQL\Drivers\ResultSetInterface;
use Foolz\SphinxQL\Drivers\SimpleConnection;
use Foolz\SphinxQL\Exception\DatabaseException;
use Foolz\SphinxQL\Exception\SphinxQLException;
use Foolz\SphinxQL\SphinxQL;

use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\JobBundle\Entity\Position;
use RDW\Bundle\JobBundle\Entity\ProfessionalArea;
use RDW\Bundle\JobBundle\Entity\DriverLicenseCategory;
use RDW\Bundle\JobBundle\Entity\WorkExperienceType;
use RDW\Bundle\JobBundle\Entity\EducationType;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\ManageBundle\Entity\Filter;
use RDW\Bundle\JobBundle\Entity\Language;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;
use Symfony\Component\Form\Exception\RuntimeException;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Translation\TranslatorInterface;

class SphinxSearchService implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    const CONFIG_PARAM_ID = 'id';
    const CONFIG_PARAM_FILTER = 'filter';
    const CONFIG_PARAM_FILTER_TITLE = 'filter_title';
    const CONFIG_PARAM_TITLE = 'title';

    /** @var SimpleConnection */
    private $connection;

    /** @var mixed */
    private $results;

    /** @var array */
    private $config;

    /** @var int */
    private $totalFound;

    /** @var TranslatorInterface */
    private $translator;

    /** @var AutoCorrectService */
    private $autoCorrectService;

    /**
     * SphinxSearchService constructor.
     *
     * @param array $config
     * @param string $host
     * @param int $port
     * @param TranslatorInterface $translator
     * @param AutoCorrectService $autoCorrectService
     */
    public function __construct(
        array $config,
        $host,
        $port,
        TranslatorInterface $translator,
        AutoCorrectService $autoCorrectService
    )
    {
        $this->connection = new SimpleConnection();
        $this->connection->setParams(array('host' => $host, 'port' => $port));

        $this->config = $config;
        $this->translator = $translator;
        $this->autoCorrectService = $autoCorrectService;
    }

    /**
     * @param string $filter
     * @param string $query
     * @param int $limit
     *
     * @throws ParameterNotFoundException
     * @throws \InvalidArgumentException
     */
    public function search($filter, $query, $limit = 10000)
    {
        if (!array_key_exists($filter, $this->config)) {
            throw new ParameterNotFoundException($filter);
        }

        $config = $this->config[$filter];
        $query = (mb_strlen(trim($query))) ? trim($query) : '';

        $ql = $this->getSearchQuery($config, $query);
        $this->setLimits($ql, $limit, 0);
        $ql->option('ranker', 'sph04');

        $this->results = $ql->execute();

        if ($config['format_result']) {
            $this->formatResult($config);
        }

        if ($config['translate_filter_title']) {
            $this->translateResult($config);
        }
    }

    /**
     * @param Form $form
     * @param array $orderBy
     * @param null|int $limit
     * @param null|int $offset
     *
     * @throws DatabaseException
     * @throws SphinxQLException
     * @throws \OutOfBoundsException
     */
    public function filterJobs(Form $form, $orderBy = [], $limit = null, $offset = null)
    {
        $ids = $this->getPositionIdsByQuery($form->get('query')->getData());

        if ($form->get('query')->getData() && empty($ids)) {
            $this->results = [];
            $this->totalFound = 0;

            return;
        }

        if ($form->get('professionalArea')->getData()->count()) {

            $paIds = array_map(
                function (ProfessionalArea $pa) {
                    return $pa->getId();
                },
                $form->get('professionalArea')->getData()->toArray()
            );
            $ids = $this->getJobsByPa($paIds);

            if ($form->get('professionalArea')->getData()->count() && empty($ids)) {
                $this->results = [];
                $this->totalFound = 0;

                return;
            }
        }

        $ql = $this->getJobBaseQuery();
        $this->buildJobQuery($form, $ql);

        $this->addSalaryQuery($ql, $form);
        $this->setOrderBy($ql, $orderBy);
        $this->setLimits($ql, $limit, $offset);

        $results = $this->runQuery($ql);

        $this->results = isset($results[0]) ? $this->extractIdsFromResult($results[0], 'job_id') : [];
        $this->totalFound = isset($results[1][1]['Value']) ? $results[1][1]['Value'] : 0;
    }

    function getCvSidebar(Form $form, array $fields)
    {
        $conn = $this->getDoctrine()->getConnection();
        $data = [];
        $ids = $this->getCvSidebarPivotIds($form);
        foreach ($fields as $field) {
            $extra = [];
            $dist = true;
            $table = null;
            $key = $field;
            $use_salary = false;
            $use_created_at = false;
            switch ($field) {
                case 'cities':
                    $field = 'working_place_city_id';
                    break;
                case 'positions':
                    $field = 'position_id';
                    break;
                case 'language':
                    $field = 'language_ids';
                    break;
                case 'scopes':
                    $field = 'scope_id';
                    break;
                case 'updatedAt':
                    $field = 'updated_at';
                    $c_intervals = $this->getSidebarCreationIntervals();
                    $use_created_at = true;
                    break;
                case 'employmentType':
                    $field = 'employment_type_id';
                    break;
                case 'educationType':
                    $field = 'education_type_id';
                    $table = 'cv_educations';
                    $extra = ['cv_id', 'IN', $ids];
                    break;
                case 'workScheduleType':
                    $field = 'work_schedule_type_id';
                    break;
                case 'workExperienceType':
                    $field = 'work_experience_type_id';
                    break;
                case 'professionalArea':
                    $field = 'professionalarea_id';
                    $table = 'cv_professionalarea';
                    $extra = ['cv_id', 'IN', $ids];
                    break;
                case 'driverLicenseCategory':
                    $field = 'driver_licence_category_id';
                    $table = 'cv_driver_licences';
                    $extra = ['cv_id', 'IN', $ids];
                    break;
                case 'salaryFrom':
                    $field = 'salary_from';
                    $s_intervals = $this->getSidebarCvSalaries($form);
                    $use_salary = true;
                    break;
                case 'gender':
                    $field = 'gender';
                    break;
                case 'hasCar':
                    $field = 'have_car';
                    $extra = [$field, '=', 1];
                    break;
                case 'businessTrip':
                    $field = 'business_trip_id';
                    break;
                case 'ageFrom':
                    $field = 'salary_from';
                    $s_intervals = $this->getSidebarCvSalaries($form);
                    $use_salary = true;
                    break;
            }
            if ($use_salary) {
                foreach ($s_intervals as $interval) {
                    $ql = $this->getCvSidebarBaseQuery($field);
                    $this->buildCvQuery($form, $ql);
                    $ql->where('salary_from', '>=', $interval[0]);
                    try {
                        $results = $this->runQuery($ql);
                        $data[$key][] = [
                            'title' => ($interval[0] > 0) ? 'От ' . $interval[0] . ' руб.' : 'Указана',
                            'value' => $interval[0],
                            'count' => $results[0][0]['count']
                        ];
                    } catch (\Exception $e) {
                        return $e->getMessage();
                    }
                }
            } elseif ($use_created_at) {

                $value = 1;
                foreach ($c_intervals as $ikey => $interval) {
                    $ql = $this->getCvSidebarBaseQuery($field);
                    $this->buildCvQuery($form, $ql);
                    $ql->where('updated_at', '>=', $interval);
                    try {
                        $results = $this->runQuery($ql);
                        $data[$key][] = [
                            'title' => 'за ' . $ikey,
                            'value' => $value,
                            'count' => $results[0][0]['count']
                        ];
                    } catch (\Exception $e) {
                        return $e->getMessage();
                    }
                    $value++;
                }
            } elseif (!is_null($table)) {
                $qb = $conn->createQueryBuilder();
                $qb->select("count(*) as count, $field as value")
                    ->from($table)
                    ->where($qb->expr()->in(
                        $extra[0],
                        array_map(
                            function ($item) {
                                return sprintf("'%s'", $item);
                            },
                            $extra[2]
                        )))
                    ->groupBy($field);
                $results = $qb->execute()->fetchAll();
                $data[$key] = $results;
            } else {
                $ql = $this->getCvSidebarBaseQuery($field);
                $this->buildCvQuery($form, $ql);
                $this->setLimits($ql, 10000, 0);
                if (is_null($table) && !empty($extra)) {
                    $ql->where($extra[0], $extra[1], $extra[2]);
                }
                if ($dist) {
                    $ql->groupBy($field);
                }
                try {
                    $results = $this->runQuery($ql);
                    $data[$key] = $results[0];
                } catch (\Exception $e) {
                    return $e->getMessage();
                }
            }
        }


        if (!empty($data['cities'])) {
            $qb = $conn->createQueryBuilder();
            $qb->select("id, title")
                ->from('cities');
            $cities = $qb->execute()->fetchAll();
            foreach ($data['cities'] as $k=>&$city) {
                unset($key);
                $key = array_search($city['value'], array_column($cities, 'id'));
                if (!is_int($key)) {
                    unset($data['cities'][$k]);
                }else
                $city['title'] = $cities[$key]['title'];
            }
            array_multisort(array_column($data['cities'], "count"), SORT_DESC, $data['cities']);
        }
        if (!empty($data['positions'])) {
            $qb = $conn->createQueryBuilder();
            $qb->select("id, title")
                ->from('job_positions');
            $poss = $qb->execute()->fetchAll();
            foreach ($data['positions'] as $k=>&$pos) {
                unset($key);
                $key = array_search($pos['value'], array_column($poss, 'id'));
                if (!is_int($key)) {
                    unset($data['positions'][$k]);
                }else
                $pos['title'] = is_int($key) ? $poss[$key]['title'] : "";
            }
            array_multisort(array_column($data['positions'], "count"), SORT_DESC, $data['positions']);
        }
        if (!empty($data['language'])) {
            $qb = $conn->createQueryBuilder();
            $qb->select("id, title")
                ->from('languages');
            $languages = $qb->execute()->fetchAll();
            foreach ($data['language'] as &$language) {
                unset($key);
                $key = array_search($language['value'], array_column($languages, 'id'));
                $language['title'] = is_int($key) ? $languages[$key]['title'] : "";
            }
            array_multisort(array_column($data['language'], "count"), SORT_DESC, $data['language']);
        }
        if (!empty($data['scopes'])) {
            $qb = $conn->createQueryBuilder();
            $qb->select("id, title")
                ->from('scopes');
            $scopes = $qb->execute()->fetchAll();
            foreach ($data['scopes'] as &$scope) {
                unset($key);
                $key = array_search($scope['value'], array_column($scopes, 'id'));
                $scope['title'] = is_int($key) ? $scopes[$key]['title'] : "";
            }
            array_multisort(array_column($data['scopes'], "count"), SORT_DESC, $data['scopes']);
        }
        if (!empty($data['employmentType'])) {
            $qb = $conn->createQueryBuilder();
            $qb->select("id, title")
                ->from('employment_types');
            $ets = $qb->execute()->fetchAll();
            foreach ($data['employmentType'] as &$et) {
                unset($key);
                $key = array_search($et['value'], array_column($ets, 'id'));
                $et['title'] = is_int($key) ? $ets[$key]['title'] : '';
            }
            array_multisort(array_column($data['employmentType'], "count"), SORT_DESC, $data['employmentType']);
        }
        if (!empty($data['educationType'])) {
            $qb = $conn->createQueryBuilder();
            $qb->select("id, title")
                ->from('education_types');
            $ets = $qb->execute()->fetchAll();
            foreach ($data['educationType'] as &$et) {
                unset($key);
                $key = array_search($et['value'], array_column($ets, 'id'));
                $et['title'] = is_int($key) ? $ets[$key]['title'] : "";
            }
            array_multisort(array_column($data['educationType'], "count"), SORT_DESC, $data['educationType']);
        }
        if (!empty($data['workScheduleType'])) {
            $qb = $conn->createQueryBuilder();
            $qb->select("id, title")
                ->from('work_schedules');
            $scts = $qb->execute()->fetchAll();
            foreach ($data['workScheduleType'] as &$sct) {
                unset($key);
                $key = array_search($sct['value'], array_column($scts, 'id'));
                $sct['title'] = is_int($key) ? $scts[$key]['title'] : "";
            }
            array_multisort(array_column($data['workScheduleType'], "count"), SORT_DESC, $data['workScheduleType']);
        }
        if (!empty($data['workExperienceType'])) {
            $qb = $conn->createQueryBuilder();
            $qb->select("id, title")
                ->from('work_experience_types');
            $wets = $qb->execute()->fetchAll();
            foreach ($data['workExperienceType'] as &$wet) {
                unset($key);
                $key = array_search($wet['value'], array_column($wets, 'id'));
                $wet['title'] = is_int($key) ? $wets[$key]['title'] : "";
            }
            array_multisort(array_column($data['workExperienceType'], "count"), SORT_DESC, $data['workExperienceType']);
        }

        if (!empty($data['onlyWithSalary'])) {
            $data['onlyWithSalary']['value'] = 1;
            $data['onlyWithSalary']['count'] = $data['onlyWithSalary'][0]['count'];
            $data['onlyWithSalary']['title'] = 'Только с зарплатой';
            unset($data['onlyWithSalary'][0]);
        }
        if (!empty($data['withoutHigh'])) {
            $data['withoutHigh']['value'] = 1;
            $data['withoutHigh']['count'] = $data['withoutHigh'][0]['count'];
            $data['withoutHigh']['title'] = 'Без высшего образования';
            unset($data['withoutHigh'][0]);
        }
        if (!empty($data['withoutExp'])) {
            $data['withoutExp']['value'] = 1;
            $data['withoutExp']['count'] = $data['withoutExp'][0]['count'];
            $data['withoutExp']['title'] = 'Без опыта работы';
            unset($data['withoutExp'][0]);
        }
        if (!empty($data['gender'])) {
            foreach ($data['gender'] as &$gender){
                switch($gender['value']){
                    case 'f':
                        $gender['title']='Женский';
                        break;
                    case 'm':
                        $gender['title']='Мужской';
                        break;
                    case '':
                        $gender['title']='Не указан';
                        break;
                }
            }
        }
        if (!empty($data['professionalArea'])) {
            $qb = $conn->createQueryBuilder();
            $qb->select("id, title")
                ->from('professional_area');
            $pas = $qb->execute()->fetchAll();
            foreach ($data['professionalArea'] as &$pa) {
                unset($key);
                $key = array_search($pa['value'], array_column($pas, 'id'));
                $pa['title'] = is_int($key) ? $pas[$key]['title'] : "";
            }
            array_multisort(array_column($data['professionalArea'], "count"), SORT_DESC, $data['professionalArea']);
        }
        if (!empty($data['driverLicenseCategory'])) {
            $qb = $conn->createQueryBuilder();
            $qb->select("id, title")
                ->from('driver_license_categories');
            $dlcs = $qb->execute()->fetchAll();
            foreach ($data['driverLicenseCategory'] as &$dlc) {
                unset($key);
                $key = array_search($dlc['value'], array_column($dlcs, 'id'));
                $dlc['title'] = is_int($key) ? $dlcs[$key]['title'] : "";
            }
        }
        if (!empty($data['hasCar'])) {
            $data['hasCar'][0]['title'] = 'Есть машина';
        }
        if (!empty($data['businessTrip'])) {
            $qb = $conn->createQueryBuilder();
            $qb->select("id, title")
                ->from('business_trips');
            $dlcs = $qb->execute()->fetchAll();
            foreach ($data['businessTrip'] as &$dlc) {
                unset($key);
                $key = array_search($dlc['value'], array_column($dlcs, 'id'));
                $dlc['title'] = is_int($key) ? $dlcs[$key]['title'] : "";
            }
            array_multisort(array_column($data['businessTrip'], "count"), SORT_DESC, $data['businessTrip']);
        }

        return $data;
    }

    function getVacancySidebar(Form $form, array $fields)
    {
        $conn = $this->getDoctrine()->getConnection();
        $data = [];
        $ids = $this->getJobSidebarPivotIds($form);
        foreach ($fields as $field) {
            $extra = [];
            $dist = true;
            $table = null;
            $key = $field;
            $use_salary = false;
            $use_created_at = false;
            switch ($field) {
                case 'cities':
                    $field = 'working_place_city_id';
                    break;
                case 'position':
                    $field = 'position_id';
                    break;
                case 'languages':
                    $field = 'language_ids';
                    break;
                case 'scope':
                    $field = 'scope_id';
                    break;
                case 'createdAt':
                    $field = 'updated_at';
                    $c_intervals = $this->getSidebarCreationIntervals();
                    $use_created_at = true;
                    break;
                case 'employmentType':
                    $field = 'employment_type_id';
                    break;
                case 'companyTitle':
                    $field = 'company_title';
                    break;
                case 'workScheduleType':
                    $field = 'work_schedule_type_id';
                    break;
                case 'employerType':
                    $field = 'employer_type';
                    break;
                case 'education':
                    $field = 'education_type_id';
                    break;
                case 'withoutHigh':
                    $field = 'education_type_id';
                    $extra = [$field, '!=', 2];
                    $dist = false;
                    break;
                case 'workExperience':
                    $field = 'work_experience_type_id';
                    break;
                case 'withoutExp':
                    $field = 'work_experience_type_id';
                    $dist = false;
                    break;
                case 'onlyWithSalary':
                    $field = 'salary_from';
                    $dist = false;
                    break;
                case 'hideAgencies':
                    $field = 'employer_type';
                    $extra = [$field, '!=', 2];
                    $dist = false;
                    break;
                case 'professionalArea':
                    $field = 'professionalarea_id';
                    $table = 'job_professionalarea';
                    $extra = ['job_id', 'IN', $ids];
                    break;
                case 'driverLicenseCategories':
                    $field = 'job_id';
                    $table = 'job_driver_licences';
                    $extra = ['driver_licence_category_id', 'IN', $ids];
                    break;
                case 'salary':
                    $field = 'salary_from';
                    $s_intervals = $this->getSidebarSalaries($form);
                    $use_salary = true;
                    break;
            }

            if ($use_salary) {
                foreach ($s_intervals as $interval) {
                    $ql = $this->getJobSidebarBaseQuery($field);
                    $this->buildJobQuery($form, $ql);
                    $ql->where('salary_from', '>=', $interval[0]);
                    try {
                        $results = $this->runQuery($ql);
                        $data[$key][] = [
                            'title' => ($interval[0] > 0) ? 'От ' . $interval[0] . ' руб.' : 'Указана',
                            'value' => $interval[0],
                            'count' => $results[0][0]['count']
                        ];
                    } catch (\Exception $e) {
                        return $e->getMessage();
                    }
                }
            } elseif ($use_created_at) {
                $value = 1;
                foreach ($c_intervals as $ikey => $interval) {
                    $ql = $this->getJobSidebarBaseQuery($field);
                    $this->buildJobQuery($form, $ql);
                    $ql->where('updated_at', '>=', $interval);

                    try {
                        $results = $this->runQuery($ql);
                        $data[$key][] = [
                            'title' => 'за ' . $ikey,
                            'value' => $value,
                            'count' => $results[0][0]['count']
                        ];
                    } catch (\Exception $e) {
                        return $e->getMessage();
                    }
                    $value++;
                }
            } elseif (!is_null($table)) {
                $qb = $conn->createQueryBuilder();
                $qb->select("count(*) as count, $field as value")
                    ->from($table)
                    ->where($qb->expr()->in(
                        $extra[0],
                        array_map(
                            function ($item) {
                                return sprintf("'%s'", $item);
                            },
                            $extra[2]
                        )))
                    ->groupBy($field);
                $results = $qb->execute()->fetchAll();
                $data[$key] = $results;
            } else {
                $ql = $this->getJobSidebarBaseQuery($field);
                $this->buildJobQuery($form, $ql);
                $this->setLimits($ql, 10000, 0);
                if (is_null($table) && !empty($extra)) {
                    $ql->where($extra[0], $extra[1], $extra[2]);
                }
                if ($dist) {
                    $ql->groupBy($field);
                }
                try {
                    $results = $this->runQuery($ql);
                    $data[$key] = $results[0];
                } catch (\Exception $e) {
                    return $e->getMessage();
                }
            }
        }
        if (!empty($data['cities'])) {
            $qb = $conn->createQueryBuilder();
            $qb->select("id, title")
                ->from('cities');
            $cities = $qb->execute()->fetchAll();
            foreach ($data['cities'] as &$city) {
                unset($key);
                $key = array_search($city['value'], array_column($cities, 'id'));

                    $city['title'] = $cities[$key]['title'];

            }
            array_multisort(array_column($data['cities'], "count"), SORT_DESC, $data['cities']);
        }
        if (!empty($data['languages'])) {
            $qb = $conn->createQueryBuilder();
            $qb->select("id, title")
                ->from('languages');
            $languages = $qb->execute()->fetchAll();
            foreach ($data['languages'] as &$language) {
                unset($key);
                $key = array_search($language['value'], array_column($languages, 'id'));
                $language['title'] = is_int($key) ? $languages[$key]['title'] : "";
            }
            array_multisort(array_column($data['languages'], "count"), SORT_DESC, $data['languages']);

        }
        if (!empty($data['companyTitle'])) {
            foreach ($data['companyTitle'] as &$ct) {
                $ct['title'] = $ct['value'];
            }
            array_multisort(array_column($data['companyTitle'], "count"), SORT_DESC, $data['companyTitle']);

        }
        if (!empty($data['scope'])) {
            $qb = $conn->createQueryBuilder();
            $qb->select("id, title")
                ->from('scopes');
            $scopes = $qb->execute()->fetchAll();
            foreach ($data['scope'] as &$scope) {
                unset($key);
                $key = array_search($scope['value'], array_column($scopes, 'id'));
                $scope['title'] = is_int($key) ? $scopes[$key]['title'] : "";
            }
            array_multisort(array_column($data['scope'], "count"), SORT_DESC, $data['scope']);
        }
        if (!empty($data['professionalArea'])) {
            $qb = $conn->createQueryBuilder();
            $qb->select("id, title")
                ->from('professional_area');
            $pas = $qb->execute()->fetchAll();
            foreach ($data['professionalArea'] as &$pa) {
                unset($key);
                $key = array_search($pa['value'], array_column($pas, 'id'));
                $pa['title'] = is_int($key) ? $pas[$key]['title'] : "";
            }
            array_multisort(array_column($data['professionalArea'], "count"), SORT_DESC, $data['professionalArea']);

        }
        if (!empty($data['position'])) {
            $qb = $conn->createQueryBuilder();
            $qb->select("id, title")
                ->from('job_positions');
            $poss = $qb->execute()->fetchAll();
            foreach ($data['position'] as &$pos) {
                unset($key);
                $key = array_search($pos['value'], array_column($poss, 'id'));
                $pos['title'] = is_int($key) ? $poss[$key]['title'] : "";
            }
            array_multisort(array_column($data['position'], "count"), SORT_DESC, $data['position']);
        }
        if (!empty($data['onlyWithSalary'])) {
            $data['onlyWithSalary']['value'] = 1;
            $data['onlyWithSalary']['count'] = $data['onlyWithSalary'][0]['count'];
            $data['onlyWithSalary']['title'] = 'Только с зарплатой';
            unset($data['onlyWithSalary'][0]);
        }
        if (!empty($data['withoutHigh'])) {
            $data['withoutHigh']['value'] = 1;
            $data['withoutHigh']['count'] = $data['withoutHigh'][0]['count'];
            $data['withoutHigh']['title'] = 'Без высшего образования';
            unset($data['withoutHigh'][0]);
        }
        if (!empty($data['withoutExp'])) {
            $data['withoutExp']['value'] = 1;
            $data['withoutExp']['count'] = $data['withoutExp'][0]['count'];
            $data['withoutExp']['title'] = 'Без опыта работы';
            unset($data['withoutExp'][0]);
        }
        if (!empty($data['employmentType'])) {
            $qb = $conn->createQueryBuilder();
            $qb->select("id, title")
                ->from('employment_types');
            $ets = $qb->execute()->fetchAll();
            foreach ($data['employmentType'] as &$et) {
                unset($key);
                $key = array_search($et['value'], array_column($ets, 'id'));
                $et['title'] = is_int($key) ? $ets[$key]['title'] : "";
            }
            array_multisort(array_column($data['employmentType'], "count"), SORT_DESC, $data['employmentType']);

        }
        if (!empty($data['workScheduleType'])) {
            $qb = $conn->createQueryBuilder();
            $qb->select("id, title")
                ->from('work_schedules');
            $scts = $qb->execute()->fetchAll();
            foreach ($data['workScheduleType'] as &$sct) {
                unset($key);
                $key = array_search($sct['value'], array_column($scts, 'id'));
                $sct['title'] = is_int($key) ? $scts[$key]['title'] : "";
            }
            array_multisort(array_column($data['workScheduleType'], "count"), SORT_DESC, $data['workScheduleType']);
        }
        if (!empty($data['driverLicenseCategories'])) {
            $qb = $conn->createQueryBuilder();
            $qb->select("id, title")
                ->from('driver_license_categories');
            $dlcs = $qb->execute()->fetchAll();
            foreach ($data['driverLicenseCategories'] as &$dlc) {
                unset($key);
                $key = array_search($dlc['value'], array_column($dlcs, 'id'));
                $dlc['title'] = is_int($key) ? $dlcs[$key]['title'] : "";
            }
        }
        if (!empty($data['education'])) {
            $qb = $conn->createQueryBuilder();
            $qb->select("id, title")
                ->from('education_types');
            $ets = $qb->execute()->fetchAll();
            foreach ($data['education'] as &$et) {
                unset($key);
                $key = array_search($et['value'], array_column($ets, 'id'));
                $et['title'] = is_int($key) ? $ets[$key]['title'] : "";
            }
            array_multisort(array_column($data['education'], "count"), SORT_DESC, $data['education']);

        }
        if (!empty($data['workExperience'])) {
            $qb = $conn->createQueryBuilder();
            $qb->select("id, title")
                ->from('work_experience_types');
            $wets = $qb->execute()->fetchAll();
            foreach ($data['workExperience'] as &$wet) {
                unset($key);
                $key = array_search($wet['value'], array_column($wets, 'id'));
                $wet['title'] = is_int($key) ? $wets[$key]['title'] : "";
            }
            array_multisort(array_column($data['workExperience'], "count"), SORT_DESC, $data['workExperience']);
        }
        return $data;
    }

    private function getSidebarCreationIntervals()
    {
        return array(
            "24 часа" => time() - 84000,
            "3 дня" => time() - 84000 * 3,
            "10 дней" => time() - 84000 * 10,
            "30 дней" => time() - 84000 * 30,
        );
    }

    private function getSidebarAgeIntervals()
    {
        return array(
            "до 18 лет" => [0, 18],
            "от 18 до 24 лет" => [18, 24],
            "от 24 до 30 лет" => [24, 30],
            "от 30 до 40 лет" => [30, 40],
            "от 40 до 55" => [40, 55],
            "старше 55 лет" => [55, 100]
        );
    }

    private function getSidebarSalaries($form)
    {
        $ql = $this->createQuery()
            ->select("MIN(salary_from) as minval, MAX(salary_from) as maxval")
            ->from('rdw_job_new')
            ->where('status', '=', Job::STATUS_ACTIVE);
        $this->buildJobQuery($form, $ql);
        try {
            $results = $this->runQuery($ql);
        } catch (\Exception $e) {
            $results = $e->getMessage();
        }
        $steps = 3;
        $max = ($results[0][0]['maxval'] + 100) / 100 * 100;
        $intervals = [
            [0, $max],
            [200, $max],
            [350, $max],
            [600, $max],
        ];
        $min = 1000;
        $step = round(($max - $min) / $steps, -2);
        do {
            $intervals[] = [(int)$min, $max];
            $min += $step;
        } while ($min < $max);

        return $intervals;
    }

    private function getSidebarCvSalaries($form)
    {
        $ql = $this->createQuery()
            ->select("MIN(salary_from) as minval, MAX(salary_from) as maxval")
            ->from('rdw_cv_new')
            ->where('status', '=', Cv::STATUS_ACTIVE);
        $this->buildJobQuery($form, $ql);
        try {
            $results = $this->runQuery($ql);
        } catch (\Exception $e) {
            $results = $e->getMessage();
        }
        $steps = 10;
        $min = $results[0][0]['minval'] / 100 * 100;
        $max = ($results[0][0]['maxval'] + 100) / 100 * 100;
        $step = round(($max - $min) / $steps, -2);
        do {
            $intervals[] = [(int)$min, (int)($min + $step)];
            $min += $step;
        } while ($min < $max);

        return $intervals;
    }

    private function getJobSidebarBaseQuery($field)
    {
        return $this->createQuery()
            ->select("count(*) as count, $field as value")
            ->from('rdw_job_new')
            ->where('status', '=', Job::STATUS_ACTIVE);
    }

    private function getCvSidebarBaseQuery($field)
    {
        return $this->createQuery()
            ->select("count(*) as count, $field as value")
            ->from('rdw_cv_new')
            ->where('status', '=', Cv::STATUS_ACTIVE);
    }

    private function getCvSidebarPivotIds($form)
    {

        $ids = [];
        $ql = $this->createQuery()
            ->select("id")
            ->from('rdw_cv_new')
            ->where('status', '=', Cv::STATUS_ACTIVE);
        $this->setLimits($ql, 10000, 0);
        $this->buildCvQuery($form, $ql);
        $results = $this->runQuery($ql);
        foreach ($results[0] as $id) {
            $ids[] = $id['id'];
        }
        return $ids;
    }

    private function getJobSidebarPivotIds($form)
    {

        $ids = [];
        $ql = $this->createQuery()
            ->select("id")
            ->from('rdw_job_new')
            ->where('status', '=', Job::STATUS_ACTIVE);
        $this->setLimits($ql, 10000, 0);
        $this->buildJobQuery($form, $ql);
        $results = $this->runQuery($ql);
        foreach ($results[0] as $id) {
            $ids[] = $id['id'];
        }
        return $ids;
    }

    /**
     * @param Form $form
     * @param array $orderBy
     * @param null|int $limit
     * @param null|int $offset
     *
     * @throws DatabaseException
     * @throws RuntimeException
     * @throws SphinxQLException
     * @throws \OutOfBoundsException
     */
    public
    function filterCvs(Form $form, $orderBy = [], $limit = null, $offset = null)
    {
        $ids = $this->getPositionIdsByQuery($form->get('query')->getData());

        if ($form->get('query')->getData() && empty($ids)) {
            $this->results = [];
            $this->totalFound = 0;

            return;
        }

        if ($form->get('professionalArea')->getData()->count()) {

            $paIds = array_map(
                function (ProfessionalArea $pa) {
                    return $pa->getId();
                },
                $form->get('professionalArea')->getData()->toArray()
            );
            $ids = $this->getCvsByPa($paIds);

            if ($form->get('professionalArea')->getData()->count() && empty($ids)) {
                $this->results = [];
                $this->totalFound = 0;

                return;
            }
        }

        $ql = $this->getCvBaseQuery();

        if ($form->getData()->getStatuses()) {
            $ql->match('status', SphinxQL::expr('(' . implode('|', $form->getData()->getStatuses()) . ')'));
        }

        $this->buildCvQuery($form, $ql);

        $this->addSalaryQuery($ql, $form);

        $this->setOrderBy($ql, $orderBy);
        $this->setLimits($ql, $limit, $offset);

        $results = $this->runQuery($ql);

        $this->results = $this->extractIdsFromResult($results[0], 'cv_id');
        $this->totalFound = $results[1][1]['Value'];
    }

    /**
     * @param Filter $filterValue
     * @param string $field
     *
     * @return int
     * @throws DatabaseException
     * @throws SphinxQLException
     */
    public
    function getJobItemsCountByFilterValue(Filter $filterValue, $field)
    {
        $ql = $this->getJobBaseQuery()->where($field, '=', $filterValue->getId());
        $results = $this->runQuery($ql);

        return (int)$results[1][1]['Value'];
    }

    /**
     * @param Filter $filterValue
     * @param string $field
     *
     * @return int
     * @throws DatabaseException
     * @throws SphinxQLException
     */
    public
    function getCvItemsCountByFilterValue(Filter $filterValue, $field)
    {
        $ql = $this
            ->getCvBaseQuery();

        $value = $filterValue->getId();

        if ($filterValue instanceof EducationType) {
            $ql->where($field, 'IN', [$value]);
        } elseif ($filterValue instanceof WorkExperienceType) {
            $ql->where($field, 'IN', [0, $value]);
        } else {
            $ql->where($field, '=', $value);
        }

        $results = $this->runQuery($ql);

        return (int)$results[1][1]['Value'];
    }

    /**
     * @return mixed
     * @throws DatabaseException
     * @throws SphinxQLException
     */
    public
    function countActiveJobs()
    {
        $results = $this->runQuery($this->getJobBaseQuery());

        return $results[1][1]['Value'];
    }

    /**
     * @return array
     */
    public
    function getJobSalaryIntervals()
    {
        $query = 'select max(salary_from) as salary_from_max, max(salary_to) as salary_to_max from rdw_job_new where status = \'active\'';
        $maximums = $this->createQuery()->query($query)->execute();

        return isset($maximums[0]) ? $this->calculateRange($maximums[0]) : [];
    }

    /**
     * @return array
     */
    public
    function getCvSalaryIntervals()
    {
        $query = 'select max(salary_from) as salary_from_max, max(salary_to) as salary_to_max from rdw_cv_new where status = \'active\'';
        $maximums = $this->createQuery()->query($query)->execute();

        return isset($maximums[0]) ? $this->calculateRange($maximums[0]) : [];
    }

    /**
     * @param array $statuses
     *
     * @return mixed
     * @throws DatabaseException
     * @throws SphinxQLException
     */
    public
    function countActiveCvs(array $statuses)
    {
        if (empty($statuses)) {
            return null;
        }

        $ql = $this->getCvBaseQuery();
        $ql->match('status', SphinxQL::expr('(' . implode('|', $statuses) . ')'));

        $results = $this->runQuery($ql);

        return $results[1][1]['Value'];
    }

    /**
     * @return mixed
     */
    public
    function getResults()
    {
        return $this->results;
    }

    /**
     * @return int
     */
    public
    function getTotalFound()
    {
        return $this->totalFound;
    }

    /**
     * @param array $config
     */
    private
    function formatResult(array $config)
    {
        $config = $this->normalizeConfig($config);

        $results = [];

        foreach ($this->results as $result) {
            $results[] = [
                'value' => $result[$config[self::CONFIG_PARAM_ID]],
                'filter' => $config[self::CONFIG_PARAM_FILTER],
                'filter_title' => $config[self::CONFIG_PARAM_FILTER_TITLE],
                'title' => $result[$config[self::CONFIG_PARAM_TITLE]],
            ];
        }

        $this->results = $results;
    }

    /**
     * @param array $config
     *
     * @return array
     */
    private
    function normalizeConfig(array $config = [])
    {
        return array_merge(
            [
                self::CONFIG_PARAM_ID => null,
                self::CONFIG_PARAM_FILTER => null,
                self::CONFIG_PARAM_FILTER_TITLE => null,
                self::CONFIG_PARAM_TITLE => null,
            ],
            $config
        );
    }

    /**
     * @param array $config
     *
     * @throws \InvalidArgumentException
     */
    private
    function translateResult(array $config)
    {
        $results = [];

        foreach ($this->results as $result) {
            $item = $result;

            $filterTitle = $result[self::CONFIG_PARAM_FILTER_TITLE];

            /** @Ignore */
            $filterTitleTranslated = $this->translator->trans($filterTitle);

            $item[self::CONFIG_PARAM_FILTER_TITLE] =
                (isset($config['translate_filter_title']) && $config['translate_filter_title'])
                    ? $filterTitleTranslated
                    : $filterTitle;

            $results[] = $item;
        }

        $this->results = $results;
    }

    /**
     * @return SphinxQL
     */
    private
    function createQuery()
    {
        return SphinxQL::create($this->connection);
    }

    /**
     * @param FormInterface $formItem
     *
     * @return array
     */
    private
    function extractIds(FormInterface $formItem)
    {
        $ids = [];
        /** @var  $item */
        foreach ($formItem->getData()->getValues() as $item) {
            $ids[] = $item->getId();
        }

        return $ids;
    }

    /**
     * @return SphinxQL
     */
    private
    function getJobBaseQuery()
    {
        return $this->createQuery()
            ->select('job_id')
            ->from('rdw_job_new')
            ->where('status', '=', Job::STATUS_ACTIVE);
    }


    /**
     * @return SphinxQL
     */
    private
    function getCvBaseQuery()
    {
        return $this->createQuery()
            ->select('cv_id')
            ->from('rdw_cv_new')
            ->where('status', '=', Cv::STATUS_ACTIVE);
    }

    /**
     * @param array $values
     *
     * @return array
     */
    private
    function calculateRange(array $values)
    {
        // max value of results
        $max = (int)max(array_unique(array_values($values)));

        // calculate step size
        $length = mb_strlen(floor($max / 10));
        $step = str_pad(1, $length, 0, STR_PAD_RIGHT);

        return range(0, $max + $step, $step);
    }

    /**
     * @param array $results
     * @param string $key
     *
     * @return array
     */
    private
    function extractIdsFromResult(array $results, $key)
    {
        $ids = [];
        foreach ($results as $result) {
            $ids[] = $result[$key];
        }

        return $ids;
    }

    function getCvCount($form)
    {
        $ql = $this->createQuery()
            ->select("count(*) as count")
            ->from('rdw_cv_new')
            ->where('status', '=', Cv::STATUS_ACTIVE);
        $this->buildCvQuery($form, $ql);
        $results = $this->runQuery($ql);
        return $results[0][0]['count'];
    }

    function getVacansyCount($form)
    {
        $ql = $this->createQuery()
            ->select("count(*) as count")
            ->from('rdw_job_new')
            ->where('status', '=', Job::STATUS_ACTIVE);
        $this->buildJobQuery($form, $ql);
        $results = $this->runQuery($ql);
        return $results[0][0]['count'];
    }

    /**
     * @param FormInterface $form
     * @param SphinxQL $ql
     */
    private
    function buildJobQuery(FormInterface $form, SphinxQL $ql)
    {
        foreach ($form as $formItem) {
            if (!$formItem->getData() || 0 === count($formItem->getData())) {
                continue;
            }

            switch ($formItem->getName()) {
                case 'query':
                    $addQuery = [$formItem->getData()];
                    if ($form->get('position')->getData()) {
                        $positions = $this->container->get('doctrine')->getRepository(Position::class)->findBy(
                            ['id' => $form->get('position')->getData()]
                        );
                        if (count($positions)) {
                            foreach ($positions as $position) {
                                $addQuery[] = $position->getTitle();
                            }
                        }
                    }
                    $ids = $this->getPositionIdsByQuery(
                        SphinxQL::expr('(\'' . implode('\'|\'', $addQuery) . '\')')
                    );

                    if (!empty($ids)) {
                        $ql->where('position_id', 'IN', $ids);
                    }

                    break;
                case 'professionalArea':
                    if ($form->get('professionalArea')->getData()->count()) {

                        $paIds = array_map(
                            function (ProfessionalArea $pa) {
                                return $pa->getId();
                            },
                            $form->get('professionalArea')->getData()->toArray()
                        );
                        $ids = $this->getJobsByPa($paIds);

                        if (!empty($ids)) {
                            $ql->where('job_id', 'IN', $ids);
                        }
                    }
                    break;
                case 'driverLicenseCategories':
                    if ($form->get('driverLicenseCategories')->getData()->count()) {

                        $dlcIds = array_map(
                            function (DriverLicenseCategory $dlc) {
                                return $dlc->getId();
                            },
                            $form->get('driverLicenseCategories')->getData()->toArray()
                        );
                        //var_dump($dlcIds);
                        $ids = $this->getJobsByDlc($dlcIds);
                        //var_dump($ids);

                        if (!empty($ids)) {
                            $ql->where('job_id', 'IN', $ids);
                        }
                    }
                    break;
                case 'position':
                    $addQuery = [$formItem->getData()];
                    if ($form->get('position')->getData()) {
                        $positions = $this->container->get('doctrine')->getRepository(Position::class)->findBy(
                            ['id' => $form->get('position')->getData()]
                        );
                        if (count($positions)) {
                            foreach ($positions as $position) {
                                $addQuery[] = $position->getTitle();
                            }
                        }
                    }
                    $ids = $this->getPositionIdsByQuery(
                        SphinxQL::expr('(\'' . implode('\'|\'', $addQuery) . '\')')
                    );

                    if (!empty($ids)) {
                        $ql->where('position_id', 'IN', $ids);
                    }
                    //$ql->where('position_id', '=', $formItem->getData()->getId());
                    break;
                case 'cities':
                    $ql->where('working_place_city_id', 'IN', is_array($formItem) ? $formItem : $this->extractIds($formItem));
                    break;
                case 'scope':
                    $ql->where('scope_id', '=', $formItem->getData()->getId());
                    break;
                case 'workScheduleType':
                    $ql->where('work_schedule_type_id', 'IN', $this->extractIds($formItem));
                    break;
                case 'companyTitle':
                    $ql->match('company_title', $formItem->getData());
                    break;
                case 'languages':
                    $ql->where('language_ids', 'IN', $this->extractIds($formItem));
                    break;
                case 'workExperience':
                    $ql->where('work_experience_type_id', '=', $formItem->getData()->getId());
                    break;
                case 'employmentType':
                    $ql->where('employment_type_id', '=', $formItem->getData()->getId());
                    break;
                case 'education':
                    $ql->where('education_type_id', '=', $formItem->getData()->getId());
                    break;
                case 'withoutHigh':
                    $ql->where('education_type_id', '!=', 2);
                    break;
                case 'withoutExp':
                    $ql->where('work_experience_type_id', '=', 2);
                    break;
                case 'hideAgencies':
                    $ql->where('employer_type', '!=', 2);
                    break;
                case 'employerType':
                    $ql->where('employer_type', '=', $formItem->getData());
                    break;
                case 'salary':
                    $ql->where('salary_from', '>=', (int)$formItem->getData());
                    break;
                case 'onlyWithSalary':
                    $ql->where('salary_from', '>', 0);
                    break;
                case 'createdAt':
                    $period = $formItem->getData() * 84600;
                    $current_datetime = new \DateTime();
                    $createdAt = $current_datetime->getTimestamp() - $period;
                    $ql->where('updated_at', '>', $createdAt);
                    break;
                case 'updatedAt':
                    $updatedAt = (new \DateTime($formItem->getData()))->getTimestamp();
                    $ql->where('updated_at', '>=', $updatedAt);
                    break;
            }
        }
    }

    /**
     * @param FormInterface $form
     * @param SphinxQL $ql
     */
    private
    function buildCvQuery(FormInterface $form, SphinxQL $ql)
    {
        foreach ($form as $formItem) {
            if (!$formItem->getData() || count($formItem->getData()) === 0) {
                continue;
            }

            switch ($formItem->getName()) {
                case 'query':

                    $addQuery = [$formItem->getData()];
                    if ($form->get('positions')->getData()) {
                        $positions = $form->get('positions')->getData();
                        if (count($positions)) {
                            /** @var Position $position */
                            foreach ($positions as $position) {
                                $addQuery[] = $position->getTitle();
                            }
                        }
                    }
                    $ids = $this->getPositionIdsByQuery(
                        SphinxQL::expr('(' . implode('|', $addQuery) . ')')
                    );

                    if (!empty($ids)) {
                        $ql->where('position_id', 'IN', $ids);
                    }

                    break;
                case 'professionalArea':
                    if ($form->get('professionalArea')->getData()->count()) {

                        $paIds = array_map(
                            function (ProfessionalArea $pa) {
                                return $pa->getId();
                            },
                            $form->get('professionalArea')->getData()->toArray()
                        );
                        $ids = $this->getCvsByPa($paIds);

                        if (!empty($ids)) {
                            $ql->where('cv_id', 'IN', $ids);
                        }
                    }
                    break;
                case 'positions':
                    $addQuery = [];
                    if ($form->get('positions')->getData()) {
                        $positions = $form->get('positions')->getData();
                        if (count($positions)) {
                            foreach ($positions as $position) {
                                $addQuery[] = $position->getTitle();
                            }
                        }
                    }
                    $ids = $this->getPositionIdsByQuery(SphinxQL::expr('(' . implode('|', $addQuery) . ')'));

                    if (!empty($ids)) {
                        $ql->where('position_id', 'IN', $ids);
                    }
                    break;
                case 'workingPlaceCity':
                    $ql->where('working_place_city_id', '=', $formItem->getData()->getId());
                    break;
                case 'scopes':
                    $ql->where('scope_id', 'IN', is_array($formItem) ? $formItem : $this->extractIds($formItem));
                    break;
                case 'gender':
                    $ql->where('gender', '=', $formItem->getData());
                    break;
                case 'ageFrom':
                    $ql->where('age', '>=', $formItem->getData());
                    break;
                case 'ageTo':
                    $ql->where('age', '<=', $formItem->getData());
                    break;
                case 'workExperienceType':
                    if (WorkExperienceType::TYPE_NO_EXPERIENCE == $formItem->getData()->getType()) {
                        $ql->where('work_experience_type_id', 'IN', [0, $formItem->getData()->getId()]);
                    } else {
                        $ql->where('work_experience_type_id', '=', $formItem->getData()->getId());
                    }

                    break;
                case 'educationType':
                    $ql->where('education_ids', 'IN', [$formItem->getData()->getId()]);
                    break;
                case 'employmentType':
                    $ql->where('employment_type_id', '=', $formItem->getData()->getId());
                    break;
                case 'workScheduleType':
                    $ql->where('work_schedule_type_id', '=', $formItem->getData()->getId());
                    break;
                case 'businessTrip':
                    $ql->where('business_trip_id', '=', $formItem->getData()->getId());
                    break;
                case 'languages':
                    $ql->where('language_ids', 'IN', $this->extractIds($formItem));
                    break;
                case 'driverLicenseCategory':
                    $ql->where('driver_licence_ids', 'IN', [$formItem->getData()->getId()]);
                    break;
                case 'hasCar':
                    $ql->where('have_car', '=', $formItem->getData());
                    break;
                case 'updatedAt':
                    $updatedAt = (new \DateTime($formItem->getData()))->getTimestamp();
                    $ql->where('updated_at', '>=', $updatedAt);
                    break;
            }
        }
    }

    /**
     * @param SphinxQL $ql
     * @param int $limit
     * @param int $offset
     */
    private
    function setLimits(SphinxQL $ql, $limit = null, $offset = null)
    {
        if (null !== $limit && null !== $offset) {
            $ql->limit($offset, $limit);
            $ql->option('max_matches', $offset + $limit);
        }
    }

    /**
     * @param SphinxQL $ql
     * @param array $orderBy
     */
    private
    function setOrderBy(SphinxQL $ql, array $orderBy = [])
    {
        $ql->orderBy('is_top', 'DESC');
        $ql->orderBy('is_highlighted', 'DESC');

        if (!empty($orderBy)) {
            foreach ($orderBy as $field => $direction) {
                $ql->orderBy($field, mb_strtoupper($direction));
            }
        } else {
            $ql->orderBy('updated_at', 'DESC');
        }
    }

    /**
     * @param SphinxQL $ql
     *
     * @return MultiResultSetInterface
     * @throws DatabaseException
     * @throws SphinxQLException
     */
    private
    function runQuery(SphinxQL $ql)
    {
        $queue = [];
        $queue[] = $ql->compile()->getCompiled();
        $queue[] = $this->createQuery()->query('SHOW META')->compile()->getCompiled();

        return $this->createQuery()->getConnection()->multiQuery($queue);
    }

    /**
     * @param SphinxQL $ql
     * @param FormInterface $form
     *
     * @throws \OutOfBoundsException
     */
    private
    function addSalaryQuery(SphinxQL $ql, FormInterface $form)
    {
        $salaryFrom = $form->get('salaryFrom')->getData();
        $salaryTo = $form->get('salaryTo')->getData();

        if ($salaryFrom && $salaryTo) {
            $ql->where('salary_to', '>=', $salaryFrom);
            $ql->where('salary_from', '<=', $salaryTo);
        } elseif ($salaryFrom) {
            $ql->where('salary_from', '<=', $salaryFrom);
            $ql->where('salary_to', '>=', $salaryFrom);
        } elseif ($salaryTo) {
            $ql->where('salary_to', '<=', $salaryTo);
        }
    }

    /**
     * @param array $config
     * @param string $query
     *
     * @return SphinxQL
     */
    private
    function getSearchQuery(array $config, $query)
    {
        $ql = $this->createQuery()
            ->select($config['select'])
            ->from($config['index']);

        // select only not null fields
        if (2 == sizeof($config['select']) && in_array($config['group_by'], $config['select'])) {
            $ql->where($config['group_by'], '>', 0);
        }

        if ($query) {
            $ql->match($config['match_column'], $query);
        }
        if (sizeof($config['order_by']) >= 1) {
            foreach ($config['order_by'] as $orderBy) {
                $ql->orderBy($orderBy[0], $orderBy[1]);
            }
        }

        $ql->groupBy($config['group_by']);

        return $ql;
    }

    /**
     * @param string $query
     *
     * @return array
     */
    private
    function getPositionIdsByQuery($query)
    {
        $query = (mb_strlen(trim($query))) ? trim($query) : '';
        $synonym = $query;
        $positions = $this->getPositionsByQuery($query);

        // Not found? Autocorect keyword and search by suggestion
        if (empty($positions)) {
            $meta = $this->createQuery()->query('SHOW META')->execute();

            $metaMap = [];
            foreach ($meta as $m) {
                $metaMap[$m['Variable_name']] = $m['Value'];
            }

            $words = array();
            foreach ($metaMap as $k => $v) {
                if (preg_match('/keyword\[\d+]/', $k)) {
                    preg_match('/\d+/', $k, $key);
                    $key = $key[0];
                    $words[$key]['keyword'] = $v;
                }
                if (preg_match('/docs\[\d+]/', $k)) {
                    preg_match('/\d+/', $k, $key);
                    $key = $key[0];
                    $words[$key]['docs'] = $v;
                }
            }

            $suggest = $this->autoCorrectService->makePhraseSuggestion($words, $query);

            // suggestion not found, return empty positions result
            if (false !== $suggest) {
                $synonym = $suggest;
                $positions = $this->getPositionsByQuery($suggest);
            }
        }

        $ids = $this->getPositionIdsBySynonym($synonym);

        foreach ($positions as $position) {
            $ids[] = (int)$position['value'];
        }

        return array_unique($ids);
    }

    /**
     * @param string $query
     *
     * @return ResultSetInterface
     */
    private
    function getPositionsByQuery($query)
    {
        $config = $this->config['job'];
        $ql = $this->getSearchQuery($config, $query);

        // force limit for autocomplete. @todo refactor
        $this->setLimits($ql, 100000, 0);

        return $ql->execute();
    }

    /**
     * @return Registry|object
     */
    private
    function getDoctrine()
    {
        return $this->container->get('doctrine');
    }

    /**
     * @param array $pas
     *
     * @return array
     */
    public
    function getJobsByPa(array $pas)
    {
        /** @var Connection $conn */
        $conn = $this->getDoctrine()->getConnection();
        $qb = $conn->createQueryBuilder();
        $qb->select('DISTINCT job_id')
            ->from('job_professionalarea')
            ->where($qb->expr()->in(
                'professionalarea_id',
                array_map(
                    function ($item) {
                        return sprintf("'%s'", $item);
                    },
                    $pas
                )
            )
            );

        return array_map(function ($item) {
            return (int)$item;
        }, array_column($qb->execute()->fetchAll(), 'job_id'));
    }

    /**
     * @param array $pas
     *
     * @return array
     */
    public
    function getJobsByDlc(array $pas)
    {
        //var_dump($pas);
        /** @var Connection $conn */
        $conn = $this->getDoctrine()->getConnection();
        $qb = $conn->createQueryBuilder();
        $qb->select('DISTINCT driver_licence_category_id')
            ->from('job_driver_licences')
            ->where($qb->expr()->in(
                'job_id',
                array_map(
                    function ($item) {
                        return sprintf("'%s'", $item);
                    },
                    $pas
                )
            )
            );

        return array_map(function ($item) {
            return (int)$item;
        }, array_column($qb->execute()->fetchAll(), 'driver_licence_category_id'));
    }

    /**
     * @param array $pas
     *
     * @return array
     */
    public
    function getCvsByPa(array $pas)
    {
        /** @var Connection $conn */
        $conn = $this->getDoctrine()->getConnection();
        $qb = $conn->createQueryBuilder();
        $qb->select('DISTINCT cv_id')
            ->from('cv_professionalarea')
            ->where($qb->expr()->in(
                'professionalarea_id',
                array_map(
                    function ($item) {
                        return sprintf("'%s'", $item);
                    },
                    $pas
                )
            )
            );

        return array_map(function ($item) {
            return (int)$item;
        }, array_column($qb->execute()->fetchAll(), 'cv_id'));
    }

    /**
     * @param string $title
     *
     * @return array
     */
    private
    function getPositionIdsBySynonym($title)
    {
        if (!strlen(trim($title))) {
            return [];
        }

        $ql = $this->createQuery()
            ->select('synonym_id, position_ids')
            ->from('synonyms')
            ->match('title', $title);

        $this->setLimits($ql, 100000, 0);

        $synonyms = $ql->execute();

        $ids = [];
        foreach ($synonyms as $synonym) {
            $SPIds = array_map('intval', array_filter(explode(',', $synonym['position_ids']), 'is_numeric'));
            $ids = array_merge($ids, $SPIds);
        }

        return $ids;
    }
}
