<?php

namespace RDW\Bundle\AppBundle\Service;

use RDW\Bundle\OrderBundle\Entity\OrderItem;

class ActItemPriceService
{
    private $orderItem;
    private $dateFrom;
    private $dateTo;

    public function __construct(\DateTime $dateFrom, \DateTime $dateTo)
    {
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
    }

    public function setOrderItem(OrderItem $orderItem)
    {
        $this->orderItem = $orderItem;
    }

    public function getTotalDaysCountForPeriod()
    {
        return $this->dateTo->diff($this->dateFrom)->days + 1;
    }

    public function getPaidDaysCount()
    {
        return $this->orderItem->getPeriod();
    }

    public function getPricePerDay()
    {
        return $this->orderItem->getPrice() / $this->orderItem->getPeriod();
    }

    public function getPaidDaysCountInPeriod()
    {
        $paidAt = $this->orderItem->getPaidAt();
        $validTill = $this->orderItem->getValidTill();

        if ($paidAt < $this->dateFrom && $validTill > $this->dateTo) {
            $paidDays = $this->dateFrom->diff($this->dateTo)->days + 1;
        } elseif ($paidAt < $this->dateFrom && $validTill <= $this->dateTo) {
            $paidDays = $this->dateFrom->diff($validTill)->days + 1;
        } elseif ($paidAt >= $this->dateFrom && $validTill > $this->dateTo) {
            $paidDays = $paidAt->diff($this->dateTo)->days + 1;
        } elseif ($paidAt >= $this->dateFrom && $validTill <= $this->dateTo) {
            $paidDays = $paidAt->diff($validTill)->days;
        } else {
            throw new \RuntimeException('Unknonw case');
        }

        return $paidDays;
    }

    public function calculateSum()
    {
        return ($this->orderItem->getService()->getIsSingle() || ($this->orderItem->getPaidAt() >= $this->dateFrom && $this->orderItem->getValidTill() <= $this->dateTo))
            ? $this->calculateSingle()
            : $this->calculatePeriodical();
    }

    private function expiredInPeriod()
    {
        $expired = $this->orderItem->getValidTill()->getTimestamp();

        return ($expired >= $this->dateFrom->getTimestamp() && $expired <= $this->dateTo->getTimestamp());
    }

    private function calculateSingle()
    {
        return ($this->expiredInPeriod())
            ? $this->orderItem->getPrice()
            : null;
    }

    private function calculatePeriodical()
    {
        return floor($this->getPricePerDay() * $this->getPaidDaysCountInPeriod());
    }
}
