<?php

namespace RDW\Bundle\AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\ValidatorInterface;
use Ddeboer\DataImport\Reader\CsvReader;

abstract class AbstractImportService
{
	protected $om;
	protected $validator;

    private $keys;
	private $imported;
	private $errors;

	public function __construct(EntityManager $entityManager, ValidatorInterface $validator)
	{
		$this->om = $entityManager;
		$this->validator = $validator;
        $this->imported = 0;
        $this->errors = [];
        $this->keys = [];
	}

    public function getErrors()
	{
		return $this->errors;
	}

	public function getImportedCount()
	{
		return $this->imported;
	}

    protected function increaseImportedCount()
    {
        $this->imported++;
    }

    protected function addError($error)
	{
		$this->errors[] = $error;
	}

    /**
     * build Item from CSV row
     *
     * @return stdClass
     */
    protected function buildItem($row)
    {
    	$item = new \stdClass();

    	foreach ($this->keys as $column) {
			if (strlen($column) == 0) {
				continue;
			}
    		$item->$column = $row[$column];
    	}

    	return $item;
    }

	public function loadKeys(CsvReader $reader)
	{
		$reader->setHeaderRowNumber(0);
        $this->setKeys($reader->getColumnHeaders());
	}

	protected function checkRowsCount($expectedSize, $row, $rowNumber)
	{
		if ($expectedSize !== ($rowSize = sizeof($row))) {
			$this->addError(sprintf('Wrong size of row (%d). Expected %d, but got %d', $rowNumber, $expectedSize, $rowSize));

			return false;
		}

		return true;
	}

    private function setKeys($keys)
    {
        $this->keys = array_filter($keys);
    }

	public function getKeys()
	{
		return $this->keys;
	}
}
