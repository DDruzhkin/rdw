<?php

namespace RDW\Bundle\AppBundle\Service;

use Ddeboer\DataImport\Reader\CsvReader;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\OrderBundle\Entity\Order;
use RDW\Bundle\OrderBundle\RDWOrderStatus;
use RDW\Bundle\OrderBundle\Service\OrderManager;
use RDW\Bundle\OrderBundle\Service\OrderItemManager;
use RDW\Bundle\JobBundle\Entity\Position;
use RDW\Bundle\JobBundle\Entity\WorkExperienceType;
use RDW\Bundle\JobBundle\Entity\EducationType;
use RDW\Bundle\UserBundle\Entity\CompanyType;
use RDW\Bundle\ServiceBundle\Entity\Service;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\ValidatorInterface;

class JobImport extends AbstractImportService
{
	const COLUMNS_COUNT = 46;

	private $orderManager;
	private $orderItemManager;

	public function __construct(EntityManager $entityManager, ValidatorInterface $validator, OrderManager $orderManager, OrderItemManager $orderItemManager)
	{
		parent::__construct($entityManager, $validator);

		$this->orderManager = $orderManager;
		$this->orderItemManager = $orderItemManager;
	}

	public function import(CsvReader $reader)
	{
		$this->loadKeys($reader);

		$this->om->getConnection()->getConfiguration()->setSQLLogger(null);

		$jobRepository = $this->om->getRepository('RDWJobBundle:Job');
		$userRepository = $this->om->getRepository('RDWUserBundle:RegisteredUser');
		$positionRepository = $this->om->getRepository('RDWJobBundle:Position');
		$companyTypeRepository = $this->om->getRepository('RDWUserBundle:CompanyType');
		$workExperienceTypeRepository = $this->om->getRepository('RDWJobBundle:WorkExperienceType');
		$educationTypeRepository = $this->om->getRepository('RDWJobBundle:EducationType');

		$rowNumber = 0;
		foreach ($reader as $row) {
			$rowNumber++;

            if (! $this->checkRowsCount(self::COLUMNS_COUNT, $row, $rowNumber)) {
				continue;
			}

			$item = $this->buildItem($row);

			$job = $jobRepository->findOneByForeignId($item->crm_id);
			if (! $job instanceof Job) {
				$job = new Job();
			}

			$user = $userRepository->findOneBy(['foreignId' => $item->client_id]);
			if (!$user) {
                $this->addError(sprintf('Row %d. User %d not found. Job %d was not imported. Order %d', $rowNumber, $item->client_id, $item->crm_id, $item->crm_order_id));
				continue;
			}

			$position = $positionRepository->findOneByTitle($item->position);
			if (!$position) {
				$position = new Position();
				$position->setTitle($item->position);
				$this->om->persist($position);
			}

			$companyType = $companyTypeRepository->findOneByTitle($item->company_type);
			if (!$companyType) {
				$companyType = new CompanyType();
				$companyType->setTitle($item->company_type);
				$this->om->persist($companyType);
			}

			$job
				->setEmail($item->email)
				->setCrmOrderId(((int) $item->crm_order_id > 0) ? (int) $item->crm_order_id : null)
				->setStatus(Job::STATUS_ACTIVE)
				->setCompanyInfo($item->company_info)
				->setCompanyTitle($item->company_title)
				->setCompanyUrl($item->company_url)
				->setGender($item->gender)
				->setSalaryFrom($item->salary_from)
				->setSalaryTo($item->salary_to)
				->setContactPerson($item->contact_person_name)
				->setContactPersonSurname($item->contact_person_surname)
				->setContactPersonEmail($item->contact_person_email)
				->setForeignId($item->crm_id)
				->setAdditionalInformation($item->additional_info)
				->setValidTill(new \DateTime($item->valid_till))
				->setCarNeeded((int)$item->car_required)
				->setFbUrl($item->fb_contact)
				->setVkUrl($item->vk_contact)
				->setOkUrl($item->ok_contact)
				->setGpUrl($item->g_plus_contact)
				->setTwitterUrl($item->twitter_contact)
				->setLinkedinUrl($item->linkedin_contact)
				->setSkypeName($item->skype_contact)
				->setYoutubeUrl($item->youtube_video)
				->setRequirements($item->requirements)
				->setWorkingConditions($item->working_conditions)
				->setAddress($item->address)
				->setPhone($item->phone)
				->setResponsibilities($item->responsibilities)
				->setWorkType($item->work_type)
				->setEmployerType((int)$item->employer_type)
				->setPosition($position)
				->setCompanyType($companyType)
				->setUser($user)
				->setFullDescriptionUrl($item->full_description_url)			
				->setUpdatedAt(new \DateTime())			
				->setAllowExport(((int) $item->allow_export > 0))
				->setAllowApply(((int) $item->allow_apply > 0));
			;
			
			
			foreach (explode (",", $item->contact_person_phone) as $contact_phone)
			{
				$job->addContactPersonPhoneByString(trim($contact_phone));
			} 

			if ($item->pa) {
				$ProfessionalArea = $this->om->getRepository('RDWJobBundle:ProfessionalArea')->getPaByids($item->pa);
				if ($ProfessionalArea) {     
					$job->setProfessionalArea($ProfessionalArea);	
				} 
			}
			$scope = $this->om->getRepository('RDWUserBundle:Scope')->findOneByTitle($item->scope);
			if ($scope) {
				$job->setScope($scope);
			}

			$city = $this->om->getRepository('RDWLocationBundle:City')->findOneByTitle($item->working_place_city);
			if ($city) {
				$job->setWorkingPlaceCity($city);
			}

			$workScheduleType = $this->om->getRepository('RDWJobBundle:WorkScheduleType')->findOneByTitle($item->work_schedule_type);
			if ($workScheduleType) {
				$job->setWorkScheduleType($workScheduleType);
			}

			$employmentType = $this->om->getRepository('RDWJobBundle:EmploymentType')->findOneByTitle($item->employment_type);
			if ($employmentType) {
				$job->setEmploymentType($employmentType);
			}

			$workExperienceType = $workExperienceTypeRepository->findOneByTitle($item->work_experience);
			if ($workExperienceType) {
				$job->setWorkExperienceType($workExperienceType);
			}

			$educationType = $educationTypeRepository->findOneByTitle($item->education);
			if ($educationType) {
				$job->setRequiredEducationType($educationType);
			}

			$errors = $this->validator->validate($job, ['import']);
			if (count($errors) > 0) {
				$this->addError(sprintf("Row %d. Job (crm_id: %d) is not valid: \n %s", $rowNumber, $item->crm_id, (string)$errors));
				continue;
			}

			$this->om->persist($job);

			$this->createServices($job, $item);

			if ($this->getImportedCount() % 50 == 0) {
				$this->om->flush();
				$this->om->clear();
			}

			$this->increaseImportedCount();
		}

		$this->om->flush();
	}

	private function createServices(Job $job, $item)
	{
		$serviceRepository = $this->om->getRepository('RDWServiceBundle:Service');
		$services = [];

		$service = $serviceRepository->findOneBy(['isFor' => Service::FOR_EMPLOYER, 'type' => Service::TYPE_JOB_PUBLISHING]);
		$services[$service->getId()]['service'] = $service;
		$services[$service->getId()]['validTill'] = $job->getValidTill();

		if ($item->top_till) {
			$service = $serviceRepository->findOneBy(['isFor' => Service::FOR_EMPLOYER, 'type' => Service::TYPE_SHOW_IN_TOP]);
			$services[$service->getId()]['service'] = $service;
			$services[$service->getId()]['validTill'] = new \DateTime($item->top_till);
		}

		if ($item->highlight_till) {
			$service = $serviceRepository->findOneBy(['isFor' => Service::FOR_EMPLOYER, 'type' => Service::TYPE_HIGHLIGHT]);
			$services[$service->getId()]['service'] = $service;
			$services[$service->getId()]['validTill'] = new \DateTime($item->highlight_till);
		}

		if (! count($services)) {
			return;
		}

		$type = Order::TYPE_SYSTEM;
		$order = $this->orderManager->createNewOrderForUser($job->getUser(), $type);

		foreach ($services as $serviceId => $serviceMap) {
			$service = $serviceMap['service'];

			$orderItem = $this->orderManager->addNewOrderItem($order, $type);
			$orderItem->setValidTill($serviceMap['validTill']);
			$orderItem->setService($service);
			$orderItem->setItem($job);
			$orderItem->setTitle($service->getTitle());
			$orderItem->setPrice($service->getPrice());
			$orderItem->setPeriod($orderItem->calculatePeriod());
			$orderItem->setStatus(RDWOrderStatus::STATUS_PAID);
			$orderItem->setPaidAt(new \DateTime());

			$this->orderItemManager->manageAttributes($orderItem, true);
		}

		$this->orderManager->updateTotal($order);
	}
}
