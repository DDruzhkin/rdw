<?php

namespace RDW\Bundle\AppBundle\Tests\EventListener;

use RDW\Bundle\AppBundle\EventListener\SitemapListener;
use RDW\Bundle\MenuBundle\Entity\Menu;
use RDW\Bundle\SlugBundle\Entity\Slug;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class SitemapListenerTest
 * @package RDW\Bundle\AppBundle\Tests\EventListener
 */
class SitemapListenerTest extends WebTestCase
{
    /**
     * @var SitemapListener
     */
    protected $listener;

    /**
     * @var \Symfony\Component\DependencyInjection\Container;
     */
    protected $container;

    /**
     * @var string
     */
    protected $host;

    /**
     * setup
     */
    public function setUp()
    {
        $client = self::createClient();
        $this->container = $client->getContainer();

        $slugService = $this->getMockBuilder('RDW\Bundle\SlugBundle\Service\SlugService')
            ->disableOriginalConstructor()
            ->getMock();

        $slugService->expects($this->any())
            ->method('getSlug')
            ->will($this->returnValue('slug'));

        $this->container->set('slug_service', $slugService);

        $this->host = $client->getContainer()->getParameter('router.request_context.host');

        $this->listener = new SitemapListener();
        $this->listener->setContainer($this->container);
    }

    /**
     * @test
     */
    public function it_should_return_false_if_type_not_exists()
    {
        $defaultUrl = '//'.$this->host.'/slug';

        foreach (Menu::getTypes() as $type) {
            $menu = new Menu();
            $menu->setType($type);

            if (Menu::TYPE_SYSTEM == $type) {
                $slug = new Slug();
                $slug->setName('slug');
                $menu->setSlug($slug);
            } elseif (Menu::TYPE_URL == $type) {
                $menu->setUrl($defaultUrl);
            }

            if ($menu->isModal()) {
                $this->assertEquals('#', $this->listener->getUrl($menu));
            } else {
                $this->assertEquals($defaultUrl, $this->listener->getUrl($menu));
            }
        }
    }
}
