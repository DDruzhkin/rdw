<?php

namespace RDW\Bundle\AppBundle\Tests\EventListener;

use RDW\Bundle\AppBundle\EventListener\UserConfirmListener;

class UserConfirmListenerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var UserConfirmListener
     */
    private $listener;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $userManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $mailerManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $user;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $event;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $manager;

    /**
     * setup
     */
    public function setUp()
    {
        $this->userManager = $this->getMockBuilder('RDW\Bundle\UserBundle\Service\UserManager')->disableOriginalConstructor()->getMock();
        $this->mailerManager = $this->getMockBuilder('RDW\Bundle\MailerBundle\Service\MailerManager')->disableOriginalConstructor()->getMock();
        $this->user = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');
        $this->event = $this->getMockBuilder('FOS\UserBundle\Event\FilterUserResponseEvent')->disableOriginalConstructor()->getMock();
        $this->manager = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');

        $this->listener = new UserConfirmListener($this->userManager, $this->mailerManager);
    }

    /**
     * @test
     * @expectedException \Symfony\Component\Validator\Exception\UnexpectedTypeException
     */
    public function it_should_throw_exception_if_user_is_not_set_properly()
    {;
        $this->event->expects($this->once())->method('getUser')->willReturn(null);
        $this->listener->onUserConfirm($this->event);
    }

    /**
     * @test
     */
    public function it_should_return_null_if_user_has_manager()
    {
        $this->user->expects($this->once())->method('getManager')->willReturn($this->manager);
        $this->event->expects($this->once())->method('getUser')->willReturn($this->user);

        $this->assertNull($this->listener->onUserConfirm($this->event));
    }

    /**
     * @test
     */
    public function it_should_return_null_when_no_manager_exists()
    {
        $this->user->expects($this->once())->method('getManager')->willReturn(null);
        $this->event->expects($this->once())->method('getUser')->willReturn($this->user);
        $this->userManager->expects($this->once())->method('getManagerForSupportingNewUsers')->willReturn(null);

        $this->assertNull($this->listener->onUserConfirm($this->event));
    }

    /**
     * @test
     */
    public function it_should_set_manager_properly()
    {
        $this->user->expects($this->once())->method('getManager')->willReturn(null);
        $this->event->expects($this->once())->method('getUser')->willReturn($this->user);
        $this->userManager->expects($this->once())->method('getManagerForSupportingNewUsers')->willReturn($this->manager);

        $this->user->expects($this->once())->method('setManager')->with($this->manager);
        $this->user->expects($this->once())->method('setHasManagerFrom');

        $this->userManager->expects($this->once())->method('updateUser')->with($this->user);

        $this->mailerManager->expects($this->once())->method('sendEmailForManagerAboutNewUser')->with($this->user);
        $this->mailerManager->expects($this->once())->method('sendEmailForUserAboutAssignedManager')->with($this->user);

        $this->assertTrue($this->listener->onUserConfirm($this->event));
    }
}
