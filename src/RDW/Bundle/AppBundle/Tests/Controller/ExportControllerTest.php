<?php

namespace RDW\Bundle\AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ExportControllerTest extends WebTestCase
{

    private $container;

    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
        $this->container = $this->client->getContainer();
    }

    /**
     * test
     */
    public function it_should_export_xml()
    {
        $xmlName = 'jooble';
        $dir = $this->container->getParameter('jobs_xml_export_path');
        $filename = $dir . '/' . $xmlName .'.xml';
        touch($filename);

        $this->client->request('GET', '/export/' . $xmlName . '.xml');

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function it_should_return_404_if_xml_not_found()
    {
        $this->client->request('GET', '/export/no-existing.xml');
        $this->assertSame(404, $this->client->getResponse()->getStatusCode());
    }
}
