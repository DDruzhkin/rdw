<?php

namespace RDW\Bundle\AppBundle\Tests\Twig;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use RDW\Bundle\AppBundle\Twig\RefererExtension;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RefererExtensionTest
 * @package RDW\Bundle\AppBundle\Tests\Twig
 */
class RefererExtensionTest extends WebTestCase
{
    /**
     * @return array
     */
    public function urlList()
    {
        return [
            ['rdw.by/testas', 'rdw.by/testas?page=20', 'rdw.by/testas?page=20'],
            ['rdw.by/testas', 'rdw.by/testas', 'rdw.by/testas'],
            ['rdw.by/testas', null, 'rdw.by/testas'],
        ];
    }

    /**
     * @dataProvider urlList
     * @test
     *
     * @param string $url
     * @param string $referer
     * @param string $expected
     */
    public function it_should_return_correct_back_link($url, $referer, $expected)
    {
        $request = new Request();
        $request->headers->set('referer', $referer);

        $requestStack = $this->getMockBuilder('\Symfony\Component\HttpFoundation\RequestStack')
            ->disableOriginalConstructor()
            ->getMock();

        $requestStack->expects($this->any())
            ->method('getCurrentRequest')
            ->will($this->returnValue($request));

        $refererExtension = new RefererExtension($requestStack);

        $this->assertSame($expected, $refererExtension->getUrlReferer([$url, 'rdw.by/testas-ii']));
        $this->assertSame($expected, $refererExtension->getUrlReferer($url));
    }
}
