<?php
namespace RDW\Bundle\AppBundle\Tests\Twig;

use RDW\Bundle\AppBundle\Twig\SeoExtension;
use Doctrine\Common\Collections\ArrayCollection;
use RDW\Bundle\JobBundle\Entity\EducationType;
use RDW\Bundle\JobBundle\Entity\WorkExperienceType;
use RDW\Bundle\JobBundle\Entity\EmploymentType;
use RDW\Bundle\JobBundle\Entity\WorkScheduleType;

class SeoExtensionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var string
     */
    private $seoSeparator;

    /**
     * @var string
     */
    private $seoBrand;

    /**
     * @var int
     */
    private $descriptionLimit;

    /**
     * @var int
     */
    private $titleLimit;

    /**
     * @var SeoExtension
     */
    private $seoExtension;

    /**
     * set up
     */
    public function setUp()
    {
        $this->seoSeparator = '|';
        $this->seoBrand = 'RDW.by';
        $this->descriptionLimit = 50;
        $this->titleLimit = 50;

        $this->seoExtension = new SeoExtension($this->seoSeparator, $this->seoBrand, $this->descriptionLimit, $this->titleLimit);
    }

    /**
     * @test
     */
    function it_should_return_proper_functions()
    {
        $filters = [
            new \Twig_SimpleFunction('get_seo_title', [$this->seoExtension, 'getSeoTitle']),
            new \Twig_SimpleFunction('get_seo_description', [$this->seoExtension, 'getSeoDescription']),
            new \Twig_SimpleFunction('get_seo_keywords', [$this->seoExtension, 'getSeoKeywords']),
            new \Twig_SimpleFunction('get_img_title', [$this->seoExtension, 'getImageTitle']),
            new \Twig_SimpleFunction('meta_title', [$this->seoExtension, 'getMetaTitle']),
            new \Twig_SimpleFunction('title', [$this->seoExtension, 'getTitle'])
        ];

        $this->assertEquals($filters, $this->seoExtension->getFunctions());
    }

    /**
     * @test
     */
    public function it_should_return_default_title()
    {
        $this->assertEquals($this->seoBrand, $this->seoExtension->getSeoTitle());
    }

    /**
     * @test
     * @dataProvider titleProvider
     */
    public function it_should_format_title($params, $expected)
    {
        $this->assertEquals($expected, $this->seoExtension->getSeoTitle($params));
    }

    /**
     * @test
     * @dataProvider descriptionProvider
     */
    public function it_should_format_description($params, $expected)
    {
        $this->assertEquals($expected, $this->seoExtension->getSeoDescription($params));
    }

    /**
     * @test
     * @dataProvider keywordsProvider
     */
    public function it_should_format_keywords($params, $expected)
    {
        $this->assertEquals($expected, $this->seoExtension->getSeoKeywords($params));
    }

    /**
     * @test
     * @dataProvider imageTitleProvider
     */
    public function it_should_format_image_title($params, $expected)
    {
        $this->assertEquals($expected, $this->seoExtension->getImageTitle($params));
    }

    /**
     * @test
     */
    public function it_should_return_proper_name()
    {
        $this->assertEquals('rdw_seo_extension', $this->seoExtension->getName());
    }

    public function metaTitles()
    {
        $subDomainCity = new \RDW\Bundle\LocationBundle\Entity\City;
        $subDomainCity
            ->setTitle('Гродно')
            ->setTitleWhere('Гродно')
        ;

        $city = new \RDW\Bundle\LocationBundle\Entity\City;
        $city
            ->setTitle('Бресте')
            ->setTitleWhere('Бресте')
            ->setTitleInLatin('brest')
        ;

        $position = new \RDW\Bundle\JobBundle\Entity\Position;
        $position->setTitle('инженер');

        $scope = new \RDW\Bundle\UserBundle\Entity\Scope();
        $scope->setTitle('розничной торговли');

        $education = new EducationType();
        $education
            ->setTitle('Без образования')
            ->setType(EducationType::TYPE_WITHOUT_EDUCATION)
        ;

        $workExperience = new WorkExperienceType();
        $workExperience
            ->setTitle('Без опыта')
            ->setType(WorkExperienceType::TYPE_NO_EXPERIENCE)
        ;

        $employmentType = new EmploymentType();
        $employmentType
            ->setTitle('Частичная')
            ->setType(EmploymentType::TYPE_PARTIAL)
        ;

        $workScheduleType = new WorkScheduleType();

        $jobFormName = 'jobs';
        $cvFormName = 'cvs';

        return [
            [
                'Работа в Гродно, вакансии и резюме на grodno.rdw.by',
                new ArrayCollection([$subDomainCity]),
                'cities',
                'grodno.rdw.by',
                $jobFormName
            ],
            [
                'Работа в Бресте, вакансии и резюме на rdw.by',
                new ArrayCollection([$city]),
                'cities',
                'rdw.by',
                $jobFormName
            ],
            [
                'Работа по специальности инженер, вакансии на rdw.by',
                $position,
                'position',
                'rdw.by',
                $jobFormName
            ],
            [
                'Работа в сфере розничной торговли, вакансии на rdw.by',
                $scope,
                'scope',
                'rdw.by',
                $jobFormName
            ],
            [
                'Работа без образования в Минске и городах Беларуси|rdw.by.',
                $education,
                'education',
                'rdw.by',
                $jobFormName
            ],
            [
                'Работа без опыта в Минске и городах Беларуси|rdw.by.',
                $workExperience,
                'workExperience',
                'rdw.by',
                $jobFormName
            ],
            [
                'Работа со сменный график в Минске и городах Беларуси|rdw.by.',
                clone $workScheduleType
                    ->setTitle('Сменный график')
                    ->setType(WorkScheduleType::TYPE_ALTERNATIVE)
                ,
                'workScheduleType',
                'rdw.by',
                $jobFormName
            ],
            [
                'Подработка в Минске и городах Беларуси|rdw.by.',
                $employmentType,
                'employmentType',
                'rdw.by',
                $jobFormName
            ],
            [
                'Удаленная работа в Минске и городах Беларуси|rdw.by.',
                clone $workScheduleType
                    ->setTitle('Удаленная работа')
                    ->setType(WorkScheduleType::TYPE_REMOTE)
                ,
                'workScheduleType',
                'rdw.by',
                $jobFormName
            ],
            [
                'Гибкий график работы в Минске и городах Беларуси|rdw.by.',
                clone $workScheduleType
                    ->setTitle('Гибкий график')
                    ->setType(WorkScheduleType::TYPE_FLEXIBLE)
                ,
                'workScheduleType',
                'rdw.by',
                $jobFormName
            ],
            [
                'Работа в Бресте, вакансии и резюме на brest.rdw.by',
                $city,
                'workingPlaceCity',
                'rdw.by',
                $cvFormName
            ],
            [
                'Резюме инженер в Минске и городах Беларуси|rdw.by.',
                new ArrayCollection([$position]),
                'positions',
                'rdw.by',
                $cvFormName
            ],
            [
                'Работа в сфере розничной торговли, резюме на rdw.by',
                new ArrayCollection([$scope]),
                'scopes',
                'rdw.by',
                $cvFormName
            ],
            [
                'Резюме без образования в Минске и городах Беларуси|rdw.by.',
                $education,
                'educationType',
                'rdw.by',
                $cvFormName
            ],
            [
                'Резюме без опыта в Минске и городах Беларуси|rdw.by.',
                $workExperience,
                'workExperienceType',
                'rdw.by',
                $cvFormName
            ],
            [
                'Резюме со сменный график работы в Минске и городах Беларуси|rdw.by.',
                clone $workScheduleType
                    ->setTitle('Сменный график')
                    ->setType(WorkScheduleType::TYPE_ALTERNATIVE)
                ,
                'workScheduleType',
                'rdw.by',
                $cvFormName
            ],
            [
                'Резюме на подработку в Минске и городах Беларуси|rdw.by.',
                $employmentType,
                'employmentType',
                'rdw.by',
                $cvFormName
            ],
            [
                'Резюме на удаленная работа в Минске и городах Беларуси|rdw.by.',
                clone $workScheduleType
                    ->setTitle('Удаленная работа')
                    ->setType(WorkScheduleType::TYPE_REMOTE)
                ,
                'workScheduleType',
                'rdw.by',
                $cvFormName
            ],
            [
                'Резюме с гибкий график работы в Минске и городах Беларуси|rdw.by.',
                clone $workScheduleType
                    ->setTitle('Гибкий график')
                    ->setType(WorkScheduleType::TYPE_FLEXIBLE)
                ,
                'workScheduleType',
                'rdw.by',
                $cvFormName
            ]
        ];
    }

    /**
     * @test
     * @dataProvider metaTitles
     */
    public function it_should_generate_meta_title_for_titles(
        $expectedTitle,
        $fieldValue,
        $fieldName,
        $host,
        $formName
    ) {
        $form = $this
            ->getMockBuilder('\Symfony\Component\Form\Form')
            ->disableOriginalConstructor()
            ->setMethods(['all', 'getName'])
            ->getMock()
        ;

        $form
            ->expects($this->any())
            ->method('getName')
            ->willReturn($formName)
        ;

        $field = $this
            ->getMockBuilder('\Symfony\Component\Form\Form')
            ->disableOriginalConstructor()
            ->setMethods(['getData', 'getName'])
            ->getMock()
        ;

        $field
            ->expects($this->any())
            ->method('getData')
            ->willReturn($fieldValue)
        ;

        $field
            ->expects($this->once())
            ->method('getName')
            ->will($this->returnValue($fieldName))
        ;

        $form
            ->method('all')
            ->willReturn([$field])
        ;

        $this->assertEquals(
            $expectedTitle,
            $this->seoExtension->getMetaTitle($form, $host)
        );
    }

    /**
     * @test
     * @dataProvider titles
     */
    public function it_should_generate_title(
        $expectedTitle,
        $fieldValue,
        $fieldName,
        $host,
        $formName
    ) {
        $form = $this
            ->getMockBuilder('\Symfony\Component\Form\Form')
            ->disableOriginalConstructor()
            ->setMethods(['all', 'getName'])
            ->getMock()
        ;

        $form
            ->expects($this->any())
            ->method('getName')
            ->willReturn($formName)
        ;

        $field = $this
            ->getMockBuilder('\Symfony\Component\Form\Form')
            ->disableOriginalConstructor()
            ->setMethods(['getData', 'getName'])
            ->getMock()
        ;

        $field
            ->expects($this->any())
            ->method('getData')
            ->willReturn($fieldValue)
        ;

        $field
            ->expects($this->once())
            ->method('getName')
            ->will($this->returnValue($fieldName))
        ;

        $form
            ->method('all')
            ->willReturn([$field])
        ;

        $this->assertEquals(
            $expectedTitle,
            $this->seoExtension->getTitle($form, $host)
        );
    }

    public function titles()
    {
        $subDomainCity = new \RDW\Bundle\LocationBundle\Entity\City;
        $subDomainCity
            ->setTitle('Гродно')
            ->setTitleWhere('Гродно')
        ;

        $city = new \RDW\Bundle\LocationBundle\Entity\City;
        $city
            ->setTitle('Бресте')
            ->setTitleWhere('Бресте')
            ->setTitleInLatin('brest')
        ;

        $position = new \RDW\Bundle\JobBundle\Entity\Position;
        $position->setTitle('инженер');

        $scope = new \RDW\Bundle\UserBundle\Entity\Scope();
        $scope->setTitle('розничной торговли');

        $education = new EducationType();
        $education
            ->setTitle('Без образования')
            ->setType(EducationType::TYPE_WITHOUT_EDUCATION)
        ;

        $workExperience = new WorkExperienceType();
        $workExperience
            ->setTitle('Без опыта')
            ->setType(WorkExperienceType::TYPE_NO_EXPERIENCE)
        ;

        $employmentType = new EmploymentType();
        $employmentType
            ->setTitle('Частичная')
            ->setType(EmploymentType::TYPE_PARTIAL)
        ;

        $workScheduleType = new WorkScheduleType();

        $jobFormName = 'jobs';
        $cvFormName = 'cvs';

        return [
            [
                'Работа в Гродно, поиск работы в Гродно',
                new ArrayCollection([$subDomainCity]),
                'cities',
                'grodno.rdw.by',
                $jobFormName
            ],
            [
                'Работа в Бресте, поиск работы в Бресте',
                new ArrayCollection([$city]),
                'cities',
                'rdw.by',
                $jobFormName
            ],
            [
                'Вакансии по специальности инженер в Минске и городах Беларуси.',
                $position,
                'position',
                'rdw.by',
                $jobFormName
            ],
            [
                'Вакансии в сфере розничной торговли в Минске и городах Беларуси.',
                $scope,
                'scope',
                'rdw.by',
                $jobFormName
            ],
            [
                'Работа без образования в Минске и городах Беларуси.',
                $education,
                'education',
                'rdw.by',
                $jobFormName
            ],
            [
                'Работа без опыта в Минске и городах Беларуси.',
                $workExperience,
                'workExperience',
                'rdw.by',
                $jobFormName
            ],
            [
                'Работа со сменный график в Минске и городах Беларуси.',
                clone $workScheduleType
                    ->setTitle('Сменный график')
                    ->setType(WorkScheduleType::TYPE_ALTERNATIVE)
                ,
                'workScheduleType',
                'rdw.by',
                $jobFormName
            ],
            [
                'Подработка в Минске и городах Беларуси.',
                $employmentType,
                'employmentType',
                'rdw.by',
                $jobFormName
            ],
            [
                'Удаленная работа в Минске и городах Беларуси.',
                clone $workScheduleType
                    ->setTitle('Удаленная работа')
                    ->setType(WorkScheduleType::TYPE_REMOTE)
                ,
                'workScheduleType',
                'rdw.by',
                $jobFormName
            ],
            [
                'Гибкий график работы в Минске и городах Беларуси.',
                clone $workScheduleType
                    ->setTitle('Гибкий график')
                    ->setType(WorkScheduleType::TYPE_FLEXIBLE)
                ,
                'workScheduleType',
                'rdw.by',
                $jobFormName
            ],
            [
                'Работа в Бресте, поиск работы в Бресте',
                $city,
                'workingPlaceCity',
                'rdw.by',
                $cvFormName
            ],
            [
                'Резюме по специальности инженер в Минске и городах Беларуси.',
                new ArrayCollection([$position]),
                'positions',
                'rdw.by',
                $cvFormName
            ],
            [
                'Резюме в сфере розничной торговли в Минске и городах Беларуси.',
                new ArrayCollection([$scope]),
                'scopes',
                'rdw.by',
                $cvFormName
            ],
            [
                'Резюме без образования в Минске и городах Беларуси.',
                $education,
                'educationType',
                'rdw.by',
                $cvFormName
            ],
            [
                'Резюме без опыта в Минске и городах Беларуси.',
                $workExperience,
                'workExperienceType',
                'rdw.by',
                $cvFormName
            ],
            [
                'Резюме со сменный график работы в Минске и городах Беларуси.',
                clone $workScheduleType
                    ->setTitle('Сменный график')
                    ->setType(WorkScheduleType::TYPE_ALTERNATIVE)
                ,
                'workScheduleType',
                'rdw.by',
                $cvFormName
            ],
            [
                'Резюме на подработку в Минске и городах Беларуси.',
                $employmentType,
                'employmentType',
                'rdw.by',
                $cvFormName
            ],
            [
                'Резюме на удаленная работа в Минске и городах Беларуси.',
                clone $workScheduleType
                    ->setTitle('Удаленная работа')
                    ->setType(WorkScheduleType::TYPE_REMOTE)
                ,
                'workScheduleType',
                'rdw.by',
                $cvFormName
            ],
            [
                'Резюме с гибкий график работы в Минске и городах Беларуси.',
                clone $workScheduleType
                    ->setTitle('Гибкий график')
                    ->setType(WorkScheduleType::TYPE_FLEXIBLE)
                ,
                'workScheduleType',
                'rdw.by',
                $cvFormName
            ]
        ];
    }

    /**
     * @test
     */
    public function it_should_return_default_title_if_no_form_fields_are_passed()
    {
        $form = $this
            ->getMockBuilder('\Symfony\Component\Form\Form')
            ->disableOriginalConstructor()
            ->setMethods(['all', 'getName'])
            ->getMock()
        ;

        $form
            ->expects($this->any())
            ->method('getName')
            ->willReturn('jobs')
        ;

        $form
            ->method('all')
            ->willReturn([])
        ;

        $host = 'rdw.by';

        $this->assertEquals(
            'RDW.by',
            $this->seoExtension->getMetaTitle($form, $host)
        );
    }

    public function titleProvider()
    {
        return array(
            array(['Test', 10], 'Test|10|RDW.by'),
            array(['Test', 10, '<h1>Test</h1>'], 'Test|10|Test|RDW.by'),
            array(['TooLongAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'], 'TooLongAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA... RDW.by'),
        );
    }

    public function descriptionProvider()
    {
        return array(
            array(['Test', 10], 'Test 10'),
            array(['Test', 10, '<h1>Test</h1>'], 'Test 10 Test'),
            array(['TooLongAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'], 'TooLongAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA...'),
        );
    }

    public function imageTitleProvider()
    {
        return array(
            array(['Test', 10], 'Test 10'),
            array(['Test', 10, '<h1>Test</h1>'], 'Test 10 Test'),
            array(['TooLongAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'], 'TooLongAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'),
        );
    }

    public function keywordsProvider()
    {
        return array(
            array(['Test', 10], 'Test, 10'),
            array(['Test', 10, '<h1>Test</h1>'], 'Test, 10, Test'),
            array(['TooLongAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'], 'TooLongAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'),
        );
    }
}
