<?php

namespace RDW\Bundle\AppBundle\Tests\Twig;

use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use RDW\Bundle\AppBundle\Twig\BodyClassExtension;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * Class BodyClassExtensionTest
 * @package RDW\Bundle\AppBundle\Tests\Twig
 */
class BodyClassExtensionTest extends WebTestCase
{
    protected $twig;

    protected $container;

    public function setUp()
    {
        $client = self::createClient();
        $this->container = $client->getContainer();
    }

    public function userData()
    {
        return [
            ['red',  'cv', '', 'RDW\Bundle\CvBundle\Controller\CvController::indexAction', false],
            ['blue', 'job', '', 'RDW\Bundle\JobBundle\Controller\JobController::indexAction', false],
            ['red', 'cv', '', 'RDW\Bundle\CvBundle\Controller\CvController::viewAction', false],
            ['blue', 'job', '', 'RDW\Bundle\JobBundle\Controller\JobController::viewAction', false],
            ['blue', 'job', '', 'RDW\Bundle\TextBundle\Controller\TextController::viewAction', 'job'],
            ['blue', 'job', '', 'RDW\Bundle\UserBundle\Controller\UserController::loginAction', 'job'],
            ['red',  'cv', 'employer', 'RDW\Bundle\CvBundle\Controller\CvController::indexAction', false],
            ['red', 'job', 'employer', 'RDW\Bundle\JobBundle\Controller\JobController::indexAction', false],
            ['red', 'cv', 'employer', 'RDW\Bundle\CvBundle\Controller\CvController::viewAction', false],
            ['red', 'job', 'employer', 'RDW\Bundle\JobBundle\Controller\JobController::viewAction', false],
            ['red', 'cv', 'employer', 'RDW\Bundle\TextBundle\Controller\TextController::viewAction', 'job'],
            ['red', 'cv', 'employer', 'RDW\Bundle\TextBundle\Controller\TextController::viewAction', 'cv'],
            ['blue',  'cv', 'employee', 'RDW\Bundle\CvBundle\Controller\CvController::indexAction', false],
            ['blue', 'job', 'employee', 'RDW\Bundle\JobBundle\Controller\JobController::indexAction', false],
            ['blue', 'cv', 'employee', 'RDW\Bundle\CvBundle\Controller\CvController::viewAction', false],
            ['blue', 'job', 'employee', 'RDW\Bundle\JobBundle\Controller\JobController::viewAction', false],
            ['blue', 'job', 'employee', 'RDW\Bundle\TextBundle\Controller\TextController::viewAction', 'job'],
            ['blue', 'job', 'employee', 'RDW\Bundle\TextBundle\Controller\TextController::viewAction', 'cv'],
        ];
    }

    /**
     * @test
     * @dataProvider userData
     *
     * @param string $bodyClass
     * @param string $applicationType
     * @param string $userType
     * @param string $class
     * @param string $setApplicationType
     */
    public function it_should_return_correct_body_class_and_search_form(
        $bodyClass,
        $applicationType,
        $userType,
        $class,
        $setApplicationType
    ) {
        $this->assertSame(
            $bodyClass,
            $this->getTwig($userType, $class, $setApplicationType)->getBodyClass()
        );

        $this->assertSame(
            $applicationType,
            $this->getTwig($userType, $class, $setApplicationType)
                ->getSearchFormType()
        );
    }

    /**
     * @param $type
     * @param $class
     * @param $sessionValue
     *
     * @return BodyClassExtension
     */
    private function getTwig($type, $class, $sessionValue)
    {
        $token = $this->getMockBuilder('Symfony\Component\Security\Core\Authentication\Token\TokenInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $token->expects($this->any())
            ->method('getUser')
            ->will($this->returnValue($this->getUser($type)));

        $securityContext = $this->getMockBuilder('\Symfony\Component\Security\Core\SecurityContext')
            ->disableOriginalConstructor()
            ->getMock();

        $securityContext->expects($this->any())
            ->method('getToken')
            ->will($this->returnValue($token));

        $this->container->set('security.context', $securityContext);

        $session = $this->getMockBuilder('Symfony\Component\HttpFoundation\Session\Session')
            ->disableOriginalConstructor()
            ->getMock();

        $session->expects($this->any())
            ->method('get')
            ->with('application_type')
            ->will($this->returnValue($sessionValue));

        $request = $this->getMockBuilder('Symfony\Component\HttpFoundation\Request')
            ->disableOriginalConstructor()
            ->getMock();

        $request->expects($this->any())
            ->method('get')
            ->with('_controller')
            ->will($this->returnValue($class));

        $request->expects($this->any())
            ->method('getSession')
            ->will($this->returnValue($session));

        $requestStack = $this->getMockBuilder('Symfony\Component\HttpFoundation\RequestStack')
            ->disableOriginalConstructor()
            ->getMock();

        $requestStack->expects($this->any())
            ->method('getCurrentRequest')
            ->will($this->returnValue($request));

        $this->container->set('request_stack', $requestStack);

        return $this->twig = new BodyClassExtension($this->container);
    }

    /**
     * @param $type
     *
     * @return null|RegisteredUser
     */
    private function getUser($type)
    {
        $user = new RegisteredUser();

        switch ($type) {
            case 'employee':
                $user->setType(RegisteredUser::USER_TYPE_EMPLOYEE);
                return $user;
            case 'employer':
                $user->setType(RegisteredUser::USER_TYPE_EMPLOYER);
                return $user;
            default:
                return null;
        }
    }
}
