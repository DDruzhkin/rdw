<?php

namespace RDW\Bundle\AppBundle\Tests\Entity;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BelmetaJobExportTest extends WebTestCase
{

    /**
     * @test
     */
    public function it_should_generate_correct_xml()
    {
        date_default_timezone_set('Europe/Vilnius');

        $client = static::createClient();
        $routerContext = $client->getContainer()->get('router')->getContext();

        $baseUrl = sprintf(
            '%s://%s%s',
            $routerContext->getScheme(),
            $routerContext->getHost(),
            $routerContext->getBaseUrl()
        );

        $job = [
            'id' => 1,
            'position' => 'Position',
            'updated_at' => new \DateTime("2015-10-29 23:18:52"),
            'company_title' => '"Company title"',
            'city' => 'City',
            'additional_information' => '"description"',
            'salary_to' => 1500,
            'scope' => 'Scope',
            'employer_type' => 2,
            'requirements' => 'requirements',
            'responsibilities' => 'duties',
            'working_conditions' => 'terms',
            'education' => 'eduction',
            'employment_type' => 'jobtype',
            'work_schedule' => 'schedule',
            'slug' => 'slug'
        ];

        $xml = '<?xml version="1.0" encoding="utf-8"?>
<source>
            <job>
            <id><![CDATA[1]]></id>
            <title><![CDATA[Position]]></title>
            <date><![CDATA[2015-10-29T23:18:52+02:00]]></date>
            <url><![CDATA['.$baseUrl.'/vakansii/' . $job['slug'] . '/' . $job['id'] . ']]></url>
            <company><![CDATA["Company title"]]></company>
            <location><![CDATA[City]]></location>
            <salary><![CDATA[1500]]></salary>
            <category><![CDATA[Scope]]></category>
            <isagency><![CDATA[1]]></isagency>
            <requirements><![CDATA[requirements]]></requirements>
            <duties><![CDATA[duties]]></duties>
            <terms><![CDATA[terms]]></terms>
            <description><![CDATA["description"]]></description>
            <education><![CDATA[eduction]]></education>
            <jobtype><![CDATA[jobtype]]></jobtype>
            <schedule><![CDATA[schedule]]></schedule>
        </job>
    </source>
';

        $this->assertSame(
            $xml,
            $client->getContainer()->get('templating')->render(
                'RDWAppBundle:JobsExport:belmeta.xml.twig',
                ['jobs' => [$job]]
            )
        );
    }
}
