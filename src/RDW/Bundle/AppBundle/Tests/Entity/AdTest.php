<?php

namespace RDW\Bundle\AdBundle\Tests\Entity;

use Symfony\Component\Validator\Validation;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\JobBundle\Entity\Position;

class AdTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     * @dataProvider users
     */
    public function it_should_have_valid_user($class)
    {
        $ad = new $class();

        $this->assertNull($ad->getUser());

        $user = new RegisteredUser();

        $this->assertInstanceOf($class, $ad->setUser($user));
        $this->assertEquals($user, $ad->getUser());
    }

    public static function users()
    {
        return [
            ['\RDW\Bundle\JobBundle\Entity\Job', 5, 5, true],
            ['\RDW\Bundle\JobBundle\Entity\Job', 5, 6, false],
            ['\RDW\Bundle\CvBundle\Entity\Cv', 5, 5, true],
            ['\RDW\Bundle\CvBundle\Entity\Cv', 5, 6, false],
        ];
    }

    /**
     * @test
     * @dataProvider views
     */
    public function it_should_return_last_added_user_who_viewed($class, $viewClass)
    {
        $ad = new $class();
        $ad->setUser(new RegisteredUser());

        $this->assertEmpty($ad->getUsersWhoViewed());

        $view = $this->getMockBuilder($viewClass)->disableOriginalConstructor()->getMock();

        $this->assertInstanceOf($class, $ad->addUserWhoViewed($view));
        $this->assertNotEmpty($ad->getUsersWhoViewed());
        $this->assertEquals($view, $ad->getUsersWhoViewed()->last());
    }

    /**
     * @test
     * @dataProvider users
     */
    public function it_should_allow_delete_ad_for_owner($class, $ownerId, $userId, $isValid)
    {
        $user = $this->getMock('\RDW\Bundle\UserBundle\Entity\RegisteredUser');
        $owner = $this->getMock('\RDW\Bundle\UserBundle\Entity\RegisteredUser');

        $user
            ->expects($this->any())
            ->method('getId')
            ->will($this->returnValue($userId));

        $owner
            ->expects($this->any())
            ->method('getId')
            ->will($this->returnValue($ownerId));

        $ad = new $class();
        $ad->setUser($owner);

        if ($isValid) {
            $this->assertTrue($ad->isAllowedToDeleteByUser($user));
            $this->assertTrue($ad->isOwner($user));
        } else {
            $this->assertFalse($ad->isAllowedToDeleteByUser($user));
            $this->assertFalse($ad->isOwner($user));
        }
    }

    /**
     * @test
     * @dataProvider salaries
     */
    public function it_should_set_error_if_salary_too_long($class, $from, $to, $groups, $isValid)
    {
        $ad = new $class();
        $ad->setSalaryFrom($from);
        $ad->setSalaryTo($to);

        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();
        $errors = $validator->validate($ad, $groups);

        $errorPaths = [];

        foreach ($errors as $error) {
            $errorPaths[] = $error->getPropertyPath();
        }

        if (! $isValid) {
            $this->assertContains('salaryFrom', $errorPaths);
            $this->assertContains('salaryTo', $errorPaths);
        } else {
            $this->assertFalse(array_key_exists('salaryFrom', $errorPaths));
            $this->assertFalse(array_key_exists('salaryTo', $errorPaths));
        }
    }

    public static function salaries()
    {
        return [
            ['\RDW\Bundle\JobBundle\Entity\Job', 123456789, 123456789, ['publish'], false],
            ['\RDW\Bundle\JobBundle\Entity\Job', 1234567, 12345678, ['publish'], true],
            ['\RDW\Bundle\CvBundle\Entity\Cv', 123456789, 123456789, ['create'], false],
            ['\RDW\Bundle\CvBundle\Entity\Cv', 12345678, 1234567, ['create'], true],
        ];
    }

    /**
     * @test
     * @dataProvider views
     */
    public function it_should_return_valid_total_views_count($class, $viewClass, $total, $anonymousCount, array $registeredUsers) {
        $ad = new $class();
        $ad->setViewsCount($anonymousCount);

        foreach ($registeredUsers as $user) {
            $view = new $viewClass($ad, $user);
            $ad->addUserWhoViewed($view);
        }

        $this->assertEquals($total, $ad->getTotalViewsCount());
    }

    /**
     * data provider
     *
     * @return array
     */
    public static function views()
    {
        $user1 = new RegisteredUser();
        $user2 = new RegisteredUser();

        return [
            ['\RDW\Bundle\JobBundle\Entity\Job', 'RDW\Bundle\JobBundle\Entity\JobView', 4, 2, [$user1, $user2]],
            ['\RDW\Bundle\JobBundle\Entity\Job', 'RDW\Bundle\JobBundle\Entity\JobView', 2, 2, []],
            ['\RDW\Bundle\JobBundle\Entity\Job', 'RDW\Bundle\JobBundle\Entity\JobView', 2, 0, [$user1, $user2]],
            ['\RDW\Bundle\JobBundle\Entity\Job', 'RDW\Bundle\JobBundle\Entity\JobView', 0, 0, []],
            ['\RDW\Bundle\CvBundle\Entity\Cv', 'RDW\Bundle\CvBundle\Entity\CvView', 4, 2, [$user1, $user2]],
            ['\RDW\Bundle\CvBundle\Entity\Cv', 'RDW\Bundle\CvBundle\Entity\CvView', 2, 2, []],
            ['\RDW\Bundle\CvBundle\Entity\Cv', 'RDW\Bundle\CvBundle\Entity\CvView', 2, 0, [$user1, $user2]],
            ['\RDW\Bundle\CvBundle\Entity\Cv', 'RDW\Bundle\CvBundle\Entity\CvView', 0, 0, []],
        ];
    }

    /**
     * @test
     * @dataProvider positions
     */
    public function it_should_have_valid_position($class, $position)
    {
        $ad = new $class();

        $this->assertNull($ad->getPosition());

        $this->assertInstanceOf($class, $ad->setPosition($position));
        $this->assertEquals($position, $ad->getPosition());
    }

    public static function positions()
    {
        $position = new Position();
        $position->setTitle('Position title');

        return [
            ['\RDW\Bundle\JobBundle\Entity\Job', $position],
            ['\RDW\Bundle\CvBundle\Entity\Cv', $position],
        ];
    }
}
