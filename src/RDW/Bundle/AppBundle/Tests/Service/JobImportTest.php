<?php

namespace RDW\Bundle\AppBundle\Tests\Service;

use RDW\Bundle\AppBundle\Service\JobImport;

class JobImportTest extends AbstractImportTest
{
	/**
     * @var JobImport
     */
    protected $jobImportService;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $orderManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $orderItemManager;

	public function setUp()
	{
        $this->setCsvFileName('jobs.csv');

		parent::setUp();

        $this->orderManager = $this->getMockBuilder('RDW\Bundle\OrderBundle\Service\OrderManager')->disableOriginalConstructor()->getMock();
        $this->orderItemManager = $this->getMockBuilder('RDW\Bundle\OrderBundle\Service\OrderItemManager')->disableOriginalConstructor()->getMock();

        $this->jobImportService = new JobImport($this->om, $this->validator, $this->orderManager, $this->orderItemManager);
	}

	/**
	 * @test
	 */
	public function it_should_set_valid_keys()
	{
        $this->csvReader->setHeaderRowNumber(0);
        $keys = $this->csvReader->getColumnHeaders();

        $this->jobImportService->loadKeys($this->csvReader);
		$this->assertEquals($keys, $this->jobImportService->getKeys());
	}
}
