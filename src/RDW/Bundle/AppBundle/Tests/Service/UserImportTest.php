<?php

namespace RDW\Bundle\AppBundle\Tests\Service;

use RDW\Bundle\AppBundle\Service\UserImport;

class UserImportTest extends AbstractImportTest
{
    /**
     * @var UserImport
     */
    protected $userImportService;

    public function setUp()
    {
        $this->setCsvFileName('clients.csv');

        parent::setUp();

        // $configuration = $this->getMockBuilder('Doctrine\DBAL\Configuration')->disableOriginalConstructor()->getMock();
        // $configuration->expects($this->once())->method('setSQLLogger')->with(null)->willReturn(null);
        // $connection = $this->getMockBuilder('Doctrine\DBAL\Connection')->disableOriginalConstructor()->getMock();
        // $connection->expects($this->once())->method('getConfiguration')->willReturn($configuration);
        // $this->om->expects($this->once())->method('getConnection')->willReturn($connection);

        $this->userImportService = new UserImport($this->om, $this->validator);
    }

	/**
	 * @test
	 */
	public function it_should_set_valid_keys()
	{
		$keys = ['email', 'crm_id', 'employer_type', 'company_title', 'company_info', 'company_type', 'company_url', 'address', 'unp_code', 'phone', 'fax', 'name', 'surname', 'patronymic', 'gender', 'birthday', 'bank_account', 'bank_name', 'bank_code', 'scope', 'contact_person_name', 'contact_person_surname', 'contact_person_phone', 'contact_person_email', 'contact_person_position', 'contact_person_patronymic', 'fb_contact', 'vk_contact', 'ok_contact', 'g_plus_contact', 'linkedin_contact', 'twitter_contact', 'skype_contact', 'working_place_city'];

		$this->userImportService->loadKeys($this->csvReader);

		$this->assertEquals($keys, $this->userImportService->getKeys());
	}
}
