<?php

namespace RDW\Bundle\UserBundle\Tests\Entity;

use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\UserBundle\DataFixtures\ORM\LoadUserData;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use RDW\Bundle\JobBundle\Service\JobManager;

class RelatedJobsTest extends WebTestCase
{
    /**
     * @var JobManager
     */
    private $jobManager;

    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $em;

    /**
     * set up
     */
    public function setUp()
    {
        $client = static::createClient();
        $this->em = $client->getContainer()->get('doctrine')->getManager();
        $this->jobManager = $client->getContainer()->get('rdw_job.service.job_manager');
    }

    /**
     * @test
     */
    public function it_should_not_return_related_jobs_for_owner_and_managers()
    {
        $repo = $this->em->getRepository('RDWUserBundle:RegisteredUser');
        $owner = $repo->find(LoadUserData::NUMBER_OF_EMPLOYEES + 1);
        $manager = $repo->find(LoadUserData::NUMBER_OF_EMPLOYEES + 2);
        $user = $repo->find(LoadUserData::NUMBER_OF_EMPLOYEES + 3);
        $owner->setManager($manager);

        $job = $this->addJob($owner);
        $this->addJob($owner);

        $this->assertCount(1, $this->jobManager->getRelated($job, 1, $user));
        $this->assertCount(0, $this->jobManager->getRelated($job, 1, $owner));
        $this->assertCount(0, $this->jobManager->getRelated($job, 1, $manager));

    }

    /**
     * creates new Job object & persists it to DB
     */
    private function addJob($user)
    {
        $position = new \RDW\Bundle\JobBundle\Entity\Position();
        $position->setTitle('position');

        $job = new Job();
        $job
            ->setUser($user)
            ->setStatus(Job::STATUS_ACTIVE)
            ->setValidTill(new \DateTime('+1year'))
            ->setPosition(
                $this->em->getRepository('RDWJobBundle:Position')->findOneBy(['id' => 1])
            )
        ;

        $this->em->persist($job);
        $this->em->flush();

        return $job;
    }
}
