<?php

namespace RDW\Bundle\AppBundle\Tests\Service;

use RDW\Bundle\AppBundle\Service\ActItemPriceService;

class ActItemPriceServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ActItemPriceService
     */
    private $actItemPriceService;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $orderItem;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $service;

    /**
     * @var \DateTime
     */
    private $dateFrom;
    /**
     * @var \DateTime
     */
    private $dateTo;

    /**
     * setup
     */
    public function setUp()
    {
        $this->orderItem = $this->getMock('RDW\Bundle\OrderBundle\Entity\OrderItem');
        $this->service = $this->getMock('RDW\Bundle\ServiceBundle\Entity\Service');
    }

    private function getPricePerDay($price, $days)
    {
        return $price / $days;
    }

    private function expectsOrderItemBeConstructedWith($price, $paidAt, $validTill, $days)
    {
        $this->orderItem->expects($this->any())->method('getValidTill')->willReturn($validTill);
        $this->orderItem->expects($this->any())->method('getPaidAt')->willReturn($paidAt);
        $this->orderItem->expects($this->any())->method('getPrice')->willReturn($price);
        $this->orderItem->expects($this->any())->method('getPeriod')->willReturn($days);
    }

    private function setOrderItem($price, $paidAt, $validTill, $days)
    {
        $this->expectsOrderItemBeConstructedWith($price, $paidAt, $validTill, $days);
        $this->actItemPriceService->setOrderItem($this->orderItem);
    }

    /**
     * @test
     * @dataProvider getData
     */
    public function it_should_return_valid_price_per_day($dateFrom, $dateTo, $price, $paidAt, $validTill, $days, $isSingle)
    {
        $this->actItemPriceService = new ActItemPriceService($dateFrom, $dateTo);
        $this->setOrderItem($price, $paidAt, $validTill, $days);

        $this->assertEquals($this->getPricePerDay($price, $days), $this->actItemPriceService->getPricePerDay());
    }

    /**
     * @test
     * @dataProvider getData
     */
    public function it_should_return_valid_paid_days_count_in_period($dateFrom, $dateTo, $price, $paidAt, $validTill, $days, $isSingle)
    {
        $this->actItemPriceService = new ActItemPriceService($dateFrom, $dateTo);
        $this->setOrderItem($price, $paidAt, $validTill, $days);

        $paidDays = $this->getPaidDays($dateFrom, $dateTo, $paidAt, $validTill);
        $this->assertEquals($paidDays, $this->actItemPriceService->getPaidDaysCountInPeriod());
    }

    private function getPaidDays($dateFrom, $dateTo, $paidAt, $validTill)
    {
        if ($paidAt < $dateFrom && $validTill > $dateTo) {
            $paidDays = $dateFrom->diff($dateTo)->days + 1;
        } elseif ($paidAt < $dateFrom && $validTill <= $dateTo) {
            $paidDays = $dateFrom->diff($validTill)->days + 1;
        } elseif ($paidAt >= $dateFrom && $validTill > $dateTo) {
            $paidDays = $paidAt->diff($dateTo)->days + 1;
        } elseif ($paidAt >= $dateFrom && $validTill <= $dateTo) {
            $paidDays = $paidAt->diff($validTill)->days;
        } else {
            throw new \RuntimeException('Unknonw case');
        }

        return $paidDays;
    }

    /**
     * @test
     * @dataProvider getData
     */
    public function it_should_calculate_valid_price($dateFrom, $dateTo, $price, $paidAt, $validTill, $days, $isSingle)
    {
        $this->actItemPriceService = new ActItemPriceService($dateFrom, $dateTo);

        $this->expectsOrderItemBeConstructedWith($price, $paidAt, $validTill, $days);
        $this->service->expects($this->once())->method('getIsSingle')->willReturn($isSingle);
        $this->orderItem->expects($this->once())->method('getService')->willReturn($this->service);
        $this->actItemPriceService->setOrderItem($this->orderItem);

        $paidDays = $this->getPaidDays($dateFrom, $dateTo, $paidAt, $validTill);
        $expectedPrice = $this->getExpectedPrice($dateFrom, $dateTo, $price, $paidAt, $validTill, $days, $isSingle);

        $this->assertEquals($expectedPrice, $this->actItemPriceService->calculateSum());
    }

    private function isExpired($dateFrom, $dateTo, $validTill)
    {
        $expired = $validTill->getTimestamp();

        return ($expired >= $dateFrom->getTimestamp() && $expired <= $dateTo->getTimestamp());
    }

    private function getExpectedPrice($dateFrom, $dateTo, $price, $paidAt, $validTill, $days, $isSingle)
    {
        return ($isSingle)
            ? ($this->isExpired($dateFrom, $dateTo, $validTill)) ? $price : null
            : floor($this->getPaidDays($dateFrom, $dateTo, $paidAt, $validTill) * $this->getPricePerDay($price, $days));
    }

    public function getData()
    {
        return [
            [new \DateTime('2015-10-01'), new \DateTime('2015-10-31'), 42000, new \DateTime('2015-09-26'), new \DateTime('2015-11-03'), 38, false],
            [new \DateTime('2015-10-01'), new \DateTime('2015-10-31'), 42000, new \DateTime('2015-09-26'), new \DateTime('2015-10-31'), 35, false],
            [new \DateTime('2015-10-01'), new \DateTime('2015-10-31'), 42000, new \DateTime('2015-09-26'), new \DateTime('2015-10-28'), 32, false],
            [new \DateTime('2015-10-01'), new \DateTime('2015-10-31'), 42000, new \DateTime('2015-09-26'), new \DateTime('2015-10-31'), 35, false],
            [new \DateTime('2015-10-01'), new \DateTime('2015-10-31'), 42000, new \DateTime('2015-10-01'), new \DateTime('2015-11-03'), 33, false],
            [new \DateTime('2015-10-01'), new \DateTime('2015-10-31'), 42000, new \DateTime('2015-10-20'), new \DateTime('2015-11-03'), 14, false],
            [new \DateTime('2015-10-01'), new \DateTime('2015-10-31'), 42000, new \DateTime('2015-10-19'), new \DateTime('2015-10-26'), 7, false],
            [new \DateTime('2015-09-01'), new \DateTime('2015-09-30'), 42000, new \DateTime('2015-09-26'), new \DateTime('2015-11-21'), 56, false],
            [new \DateTime('2015-10-01'), new \DateTime('2015-10-31'), 42000, new \DateTime('2015-09-26'), new \DateTime('2015-11-21'), 56, false],
            [new \DateTime('2015-11-01'), new \DateTime('2015-11-30'), 42000, new \DateTime('2015-09-26'), new \DateTime('2015-11-21'), 56, false],
            [new \DateTime('2015-11-01'), new \DateTime('2015-11-30'), 42000, new \DateTime('2015-09-26'), new \DateTime('2015-11-21'), 56, true],
            [new \DateTime('2015-10-01'), new \DateTime('2015-10-31'), 42000, new \DateTime('2015-10-20'), new \DateTime('2015-11-03'), 14, true],
            [new \DateTime('2015-10-01'), new \DateTime('2015-10-31'), 42000, new \DateTime('2015-09-26'), new \DateTime('2015-10-28'), 32, true],
        ];
    }
}
