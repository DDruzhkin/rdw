<?php

namespace RDW\Bundle\AppBundle\Tests\Service;

use Ddeboer\DataImport\Reader\CsvReader;

abstract class AbstractImportTest extends \PHPUnit_Framework_TestCase
{
	/**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $om;

	/**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $validator;

	/**
     * @var CsvReader
     */
    protected $csvReader;

    private $csvFileName;

    public function setCsvFileName($csvFileName)
    {
        $this->csvFileName = $csvFileName;
    }

	public function setUp()
	{
		$this->om = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();
		$this->validator = $this->getMockBuilder('Symfony\Component\Validator\Validator')->disableOriginalConstructor()->getMock();
        $this->csvReader = new CsvReader(new \SplFileObject(__DIR__.'/../data/'.$this->csvFileName), '#');
	}
}
