<?php

namespace RDW\Bundle\AppBundle\Tests\Form\DataTransformer;

use RDW\Bundle\AppBundle\Form\DataTransformer\FormattedNumberTransformer;

/**
 * Class FormattedNumberTransformerTest
 * @package RDW\Bundle\AppBundle\Tests\Twig
 */
class FormattedNumberTransformerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function numbers()
    {
        return [
            ['10 000 000', 10000000],
            ['10 000', 10000],
            ['0', 0],
            [1000, 1000],
            ['', null],
            [null, null],
        ];
    }

    /**
     * @dataProvider numbers
     * @test
     */
    public function it_should_return_number_without_formatation($value, $expectedValue)
    {
        $transformer = new FormattedNumberTransformer();
        $this->assertSame($expectedValue, $transformer->reverseTransform($value));
        $this->assertSame($expectedValue, $transformer->transform($value));
    }
}