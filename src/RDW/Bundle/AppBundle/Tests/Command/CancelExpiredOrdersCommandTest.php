<?php

namespace RDW\Bundle\AppBundle\Tests\Command;
use RDW\Bundle\AppBundle\Command\CancelExpiredOrdersCommand;

/**
 * Class CancelExpiredOrdersCommandTest
 * @package RDW\Bundle\AppBundle\Tests\Command
 */
class CancelExpiredOrdersCommandTest extends \PHPUnit_Framework_TestCase
{
    public function queries()
    {
        return [
            [
                'orders',
                'UPDATE orders o
                SET o.status = ?
                WHERE o.status IN (?) AND o.created_at <= DATE_ADD(NOW(), INTERVAL -? DAY)
                AND NOT EXISTS (SELECT 1 FROM order_items i WHERE o.id = i.order_id AND i.status = ?)'
            ],
            [
                'items',
                'UPDATE order_items oi LEFT JOIN orders o ON oi.order_id = o.id
                SET oi.status = ?
                WHERE oi.status IN (?) AND o.created_at <= DATE_ADD(NOW(), INTERVAL -? DAY)
                AND oi.status != ?'
            ],
        ];
    }

    /**
     * @test
     * @dataProvider queries
     */
    public function it_should_return_query_by_type($type, $query)
    {
        $command = new CancelExpiredOrdersCommand();

        $this->assertEquals(
            $this->invokeMethod($command, 'getQuery', array($type)),
            $query
        );
    }

    /**
     * Call protected/private method of a class.
     *
     * @param object &$object    Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array  $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
