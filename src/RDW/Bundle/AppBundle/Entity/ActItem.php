<?php

namespace RDW\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\OrderBundle\Entity\OrderItem;

/**
 * Class ActItem
 *
 * @package RDW\Bundle\AppBundle\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="act_items")
 */
class ActItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Act
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\AppBundle\Entity\Act", inversedBy="items")
     * @ORM\JoinColumn(name="act_id", referencedColumnName="id")
     */
    private $act;

    /**
     * @var OrderItem
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\OrderBundle\Entity\OrderItem", inversedBy="actItems")
     * @ORM\JoinColumn(name="order_item_id", referencedColumnName="id")
     */
    private $orderItem;

    /**
     * @var float
     *
     * @ORM\Column(name="sum", type="float")
     */
    private $sum;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string")
     */
    private $subject;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return OrderItem
     */
    public function getOrderItem()
    {
        return $this->orderItem;
    }

    /**
     * @param OrderItem $orderItem
     *
     * @return ActItem
     */
    public function setOrderItem($orderItem)
    {
        $this->orderItem = $orderItem;

        return $this;
    }

    /**
     * @return float
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * @return float
     */
    public function getSumWithoutVat()
    {
        return $this->sum * (100 - $this->act->getVat()) / 100;
    }

    /**
     * @return float
     */
    public function getSumVat()
    {
        return $this->sum * ($this->act->getVat()) / 100;
    }

    /**
     * @param float $sum
     *
     * @return ActItem
     */
    public function setSum($sum)
    {
        $this->sum = $sum;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     *
     * @return ActItem
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @return ActItem
     */
    public function getAct()
    {
        return $this->act;
    }

    /**
     * @param Act $act
     *
     * @return ActItem
     */
    public function setAct($act)
    {
        $this->act = $act;

        return $this;
    }
}
