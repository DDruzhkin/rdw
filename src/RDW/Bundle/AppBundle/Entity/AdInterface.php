<?php

namespace RDW\Bundle\AppBundle\Entity;

interface AdInterface
{
    public function getId();
    public function isJob();
    public function isCv();
}
