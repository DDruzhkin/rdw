<?php

namespace RDW\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use RDW\Bundle\AppBundle\Entity\Traits\DatesAwareTrait;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\CvBundle\Entity\CvView;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\JobBundle\Entity\Position;
use Doctrine\Common\Collections\ArrayCollection;
use RDW\Bundle\OrderBundle\Entity\OrderItem;
use RDW\Bundle\JobBundle\Entity\View;
use RDW\Bundle\UserBundle\Entity\User;

abstract class AbstractAd implements DatesAwareInterface
{
    use DatesAwareTrait;

    protected $id;

    protected $user;

    protected $position;

    protected $workingPlaceCity;

    protected $services;

    protected $viewsCount;

    protected $viewsCountUpdatedAt;

    protected $usersWhoViewed;

    protected $usersWhoShowedContacts;

    protected $salaryFrom;

    protected $salaryTo;

    public function __construct()
    {
        $this->services = new ArrayCollection();
        $this->usersWhoViewed = new ArrayCollection();
        $this->usersWhoShowedContacts = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return User|RegisteredUser
     */
    public function getUser()
    {
        return $this->user;
    }

    public function setPosition(Position $position = null)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return Position
     */
    public function getPosition()
    {
        return $this->position;
    }

    public function getWorkingPlaceCity()
    {
        return $this->workingPlaceCity;
    }

    public function setWorkingPlaceCity($workingPlaceCity)
    {
        $this->workingPlaceCity = $workingPlaceCity;

        return $this;
    }

    public function getServices()
    {
        return $this->services;
    }

    public function getActiveServices()
    {
        $valid = [];

        foreach ($this->services as $service) {
            if ($service->isPaid() && $service->getValidDaysLeft() >= 0) {
                $valid[] = $service;
            }
        }

        return $valid;
    }

    public function addService(OrderItem $service)
    {
        $this->services[] = $service;

        return $this;
    }

    public function removeService(OrderItem $service)
    {
        $this->services->removeElement($service);
    }

    public function getViewsCount()
    {
        return $this->viewsCount;
    }

    public function setViewsCount($viewsCount)
    {
        $this->viewsCount = $viewsCount;

        return $this;
    }

    public function getViewsCountUpdatedAt()
    {
        return $this->viewsCountUpdatedAt;
    }

    public function setViewsCountUpdatedAt(\DateTime $viewsCountUpdatedAt)
    {
        $this->viewsCountUpdatedAt = $viewsCountUpdatedAt;

        return $this;
    }

    /**
     * @param int $salaryTo
     *
     * @return self
     */
    public function setSalaryTo($salaryTo)
    {
        $this->salaryTo = $salaryTo;

        return $this;
    }

    /**
     * @param int $salaryFrom
     *
     * @return $this
     */
    public function setSalaryFrom($salaryFrom)
    {
        $this->salaryFrom = $salaryFrom;

        return $this;
    }

    /**
     * @return int
     */
    public function getSalaryFrom()
    {
        return $this->salaryFrom;
    }

    /**
     * @return int
     */
    public function getSalaryTo()
    {
        return $this->salaryTo;
    }

    public function isAllowedToDeleteByUser(RegisteredUser $user)
    {
        return ($user->getId() == $this->getUser()->getId());
    }

    public function getUsersWhoViewed()
    {
        $usersWhoViewed = new ArrayCollection();

        /** @var CvView $cvView */
        foreach ($this->usersWhoViewed as $view) {
            if ($view->getUser() != $this->getUser()) {
                $usersWhoViewed->add($view);
            }
        }

        return $usersWhoViewed;
    }

    public function setUsersWhoViewed($usersWhoViewed)
    {
        $this->usersWhoViewed = $usersWhoViewed;

        return $this;
    }

    public function setUsersWhoShowedContacts($usersWhoShowedContacts)
    {
        $this->usersWhoShowedContacts = $usersWhoShowedContacts;

        return $this;
    }

    public function addUserWhoViewed(View $userWhoViewed)
    {
        if (!$this->usersWhoViewed->contains($userWhoViewed)) {
            $this->usersWhoViewed->add($userWhoViewed);
        }

        return $this;
    }

    public function addUserWhoShowedContact(View $usersWhoShowedContacts)
    {
        if (!$this->usersWhoShowedContacts->contains($usersWhoShowedContacts)) {
            $this->usersWhoShowedContacts->add($usersWhoShowedContacts);
        }

        return $this;
    }

    public function removeUserWhoViewed(View $userWhoViewed)
    {
        $this->usersWhoViewed->removeElement($userWhoViewed);

        return $this;
    }

    public function removeUserWhoShowedContacts(View $usersWhoShowedContacts)
    {
        $this->usersWhoShowedContacts->removeElement($usersWhoShowedContacts);

        return $this;
    }

    public function isOwner(User $user)
    {
        return ($user->getId() == $this->getUser()->getId());
    }

    public function isJob()
    {
        return ($this instanceof Job);
    }

    public function isCv()
    {
        return ($this instanceof Cv);
    }
}
