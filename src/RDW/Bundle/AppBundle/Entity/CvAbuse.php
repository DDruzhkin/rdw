<?php

namespace RDW\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\CvBundle\Entity\Cv;

/**
 * @ORM\Entity(repositoryClass="RDW\Bundle\CvBundle\Repository\AbuseRepository")
 * @ORM\Table(name="cv_abuses")
 */
class CvAbuse extends Abuse
{
    /**
     * @var Cv
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\CvBundle\Entity\Cv")
     * @ORM\JoinColumn(name="cv_id", referencedColumnName="id")
     */
    protected $ad;
}
