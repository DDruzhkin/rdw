<?php

namespace RDW\Bundle\AppBundle\Entity;

interface DatesAwareInterface extends CreatedAtAwareInterface, UpdatedAtAwareInterface
{

}
