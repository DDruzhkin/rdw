<?php

namespace RDW\Bundle\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\Table(name="position_synonyms")
 */
class PositionSynonym
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string")
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @ORM\ManyToMany(targetEntity="RDW\Bundle\JobBundle\Entity\Position")
     * @ORM\JoinTable(name="position_synonym_positions",
     *      joinColumns={@ORM\JoinColumn(name="position_synonym_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="position_id", referencedColumnName="id")}
     *      )
     */
    private $positions;

    public function __construct()
    {
        $this->positions = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getPositions()
    {
        return $this->positions;
    }

    public function setPositions($positions)
    {
        $this->positions = $positions;

        return $this;
    }
}
