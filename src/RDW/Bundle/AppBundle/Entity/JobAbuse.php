<?php

namespace RDW\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\JobBundle\Entity\Job;

/**
 * Class JobAbuse
 *
 * @package RDW\Bundle\AppBundle\Entity
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 *
 * @ORM\Entity(repositoryClass="RDW\Bundle\JobBundle\Repository\AbuseRepository")
 * @ORM\Table(name="job_abuses")
 */
class JobAbuse extends Abuse
{
     /**
     * @var Job
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\JobBundle\Entity\Job")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id")
     */
    protected $ad;
}
