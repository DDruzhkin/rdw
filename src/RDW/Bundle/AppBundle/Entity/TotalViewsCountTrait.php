<?php

namespace RDW\Bundle\AppBundle\Entity;

trait TotalViewsCountTrait
{
    public function getTotalViewsCount()
    {
        return $this->getUsersWhoViewed()->count() + $this->getViewsCount();
    }
}
