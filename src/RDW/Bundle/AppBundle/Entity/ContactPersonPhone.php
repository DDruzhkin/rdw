<?php

namespace RDW\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Position
 *
 * @package RDW\Bundle\AppBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="contact_phones")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discriminator", type="string")
 * @ORM\DiscriminatorMap({
 *     "job" = "RDW\Bundle\JobBundle\Entity\ContactPersonPhone",
 *     "cv" = "RDW\Bundle\CvBundle\Entity\ContactPersonPhone",
 * })
 */
class ContactPersonPhone
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string")
     * @Assert\NotBlank(groups={"create", "publish", "censor"})
     * @Assert\Regex(
     *      pattern="/^([\+0-9\s\-]+)$/",
     *      message="Field must be a valid phone number and can have only numbers, +, - and spaces",
     *      groups={"create", "publish", "censor"}
     * )
     */
    private $number;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     *
     * @return $this
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return object string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param object $entity
     *
     * @return $this
     */
    public function setEntity($entity = null)
    {
        $this->entity = $entity;

        return $this;
    }
}
