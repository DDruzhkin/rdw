<?php

namespace RDW\Bundle\AppBundle\Entity;

use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Abuse
 * @package RDW\Bundle\AppBundle\Entity
 *
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks()
 */
class Abuse
{
    const REASON_SPAM = 'spam';
    const REASON_COMPANY_INCORRECT = 'company_incorrect';
    const REASON_OTHER = 'other';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reason", type="text")
     *
     * @Assert\NotBlank(message="Reason must be selected", groups={"create_abuse"})
     */
    protected $reason;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     *
     * @Assert\NotBlank(message="Description can not be empty.", groups={"other"})
     */
    protected $description;

    /**
     * @var RegisteredUser
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\RegisteredUser")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var AbstractAd
     */
    protected $ad;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     *
     * @return $this
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return \RDW\Bundle\UserBundle\Entity\RegisteredUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param \RDW\Bundle\UserBundle\Entity\RegisteredUser $user
     *
     * @return $this
     */
    public function setUser(RegisteredUser $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }


    /**
     * @return AbstractAd
     */
    public function getAd()
    {
        return $this->ad;
    }

    /**
     * @param AbstractAd $ad
     *
     * @return $this
     */
    public function setAd(AbstractAd $ad)
    {
        $this->ad = $ad;

        return $this;
    }
}
