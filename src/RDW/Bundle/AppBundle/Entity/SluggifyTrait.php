<?php

namespace RDW\Bundle\AppBundle\Entity;

trait SluggifyTrait
{
    /**
    * @ORM\Column(type="string", length=255)
    */
    private $slug;

    /**
    * @ORM\PreFlush()
    *
    * generates slug
    *
    * @return Job
    */
    public function generateSlug()
    {
        if (is_null($this->slug)) {
            $this->slug = ($position = $this->getPosition())
                ? Transliterator::transliterate($position->getTitle())
                : null;
        }

        return $this;
    }

    /**
    * Get slug
    *
    * @return string
    */
    public function getSlug()
    {
        return $this->slug;
    }
}
