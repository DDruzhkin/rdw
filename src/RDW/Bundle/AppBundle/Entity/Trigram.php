<?php

namespace RDW\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="trigrams")
 * @UniqueEntity("keyword")
 * ORM\HasLifecycleCallbacks()
 */
class Trigram
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Position
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\JobBundle\Entity\Position")
     * @ORM\JoinColumn(name="position_id", referencedColumnName="id")
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="keyword", type="string", unique=true)
     */
    private $keyword;

    /**
     * @var string
     *
     * @ORM\Column(name="trigrams", type="string")
     */
    private $trigrams;

    /**
     * @var int
     *
     * @ORM\Column(name="frequency", type="integer")
     */
    private $frequency;

    public function __construct()
    {
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    public function getKeyword()
    {
        return $this->keyword;
    }

    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;

        return $this;
    }

    public function getTrigrams()
    {
        return $this->trigrams;
    }

    public function setTrigrams($trigrams)
    {
        $this->trigrams = $trigrams;

        return $this;
    }

    public function getFrequency()
    {
        return $this->frequency;
    }

    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;

        return $this;
    }
}
