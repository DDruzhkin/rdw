<?php

namespace RDW\Bundle\AppBundle\Entity;

trait TopTrait
{
    /**
     * @var boolean
     * @ORM\Column(name="is_top", type="boolean", nullable=true)
     */
    private $top;

    /**
     * @return bool
     */
    public function isTop()
    {
        return $this->top;
    }

    /**
     * @param bool $top
     *
     * @return $this
     */
    public function setTop($top)
    {
        $this->top = $top;

        return $this;
    }
}
