<?php

namespace RDW\Bundle\AppBundle\Entity\Traits;

trait DatesAwareTrait
{
    use CreatedAtAwareTrait;
    use UpdatedAtAwareTrait;
}
