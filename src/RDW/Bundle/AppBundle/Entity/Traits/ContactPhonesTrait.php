<?php

namespace RDW\Bundle\AppBundle\Entity\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use RDW\Bundle\AppBundle\Entity\ContactPersonPhone;

trait ContactPhonesTrait
{
    /**
     * @return ArrayCollection
     */
    public function getContactPersonPhones()
    {
        return $this->contactPersonPhones;
    }

    /**
     * @return string
     */
    public function getContactPersonPhonesAsString()
    {
        $phones = [];

        foreach ($this->getContactPersonPhones() as $phone) {
            $phones[] = $phone->getNumber();
        }

        return implode(', ', $phones);
    }

    /**
     * @param ArrayCollection $phones
     *
     * @return $this
     */
    public function setContactPersonPhones($phones)
    {
        $this->contactPersonPhones = $phones;

        return $this;
    }

    /**
     * @param ContactPersonPhone|null $phone
     *
     * @return self
     */
    public function addContactPersonPhone($phone)
    {
        if ($phone instanceof ContactPersonPhone && ! $this->contactPersonPhones->contains($phone) && $this->isContactPhoneNumberUnique($phone)) {
            $phone->setEntity($this);
            $this->contactPersonPhones->add($phone);
        }

        return $this;
    }

    /**
     * @param string $number
     *
     * @return self
     */
    public function addContactPersonPhoneByString($number)
    {
        if ($number) {
            $phone = $this::createContactPersonPhone()
                ->setEntity($this)
                ->setNumber($number)
            ;

            $this->addContactPersonPhone($phone);
        }

        return $this;
    }

    /**
     * @param ContactPersonPhone $phone
     *
     * @return bool
     */
    public function isContactPhoneNumberUnique($phone)
    {
        if (! $phone instanceof ContactPersonPhone) {
            return false;
        }

        return $this->contactPersonPhones->forAll(function ($key, $element) use ($phone) {
            /** @var ContactPersonPhone $element */
            return !($element->getNumber() == $phone->getNumber());
        });
    }

    /**
     * @param ContactPersonPhone $phone
     *
     * @return $this
     */
    public function removeContactPersonPhone(ContactPersonPhone $phone)
    {
        $this->contactPersonPhones->removeElement($phone);

        return $this;
    }
}