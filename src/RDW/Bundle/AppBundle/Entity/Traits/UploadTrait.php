<?php

namespace RDW\Bundle\AppBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

trait UploadTrait
{
    /**
     * @var UploadedFile
     *
     * @Assert\File(
     *     mimeTypes = { "image/png", "image/jpg", "image/jpeg"},
     *     mimeTypesMessage = "Please upload a valid file (.png, .jpg, .jpeg)"
     * )
     */
    protected $file;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    protected $path;

    /**
     * Avatar path by default if user nas not avatar
     */
    protected $defaultPath = "";

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return null|string
     */
    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir() . '/' . $this->path;
    }

    /**
     * @return null|string
     */
    public function getWebPath()
    {
        return null === $this->path
            ? null
            : $this->getUploadDir() . '/' . $this->path;
    }

    /**
     * @return string
     */
    public function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../../../../web' . $this->getUploadDir();
    }

    /**
     * @return $this
     */
    public function preUpload()
    {
        if (null !== $this->file) {
            $typeFile = $this->file->guessExtension();
            $generateName = uniqid();
            $this->path = $generateName . "." . $typeFile;
        }

        return $this;
    }

    public function upload()
    {
        if (null === $this->file) {
            return;
        }

        if ($this->path) {
            $file = $this->getAbsolutePath();
            if (file_exists($file)) {
                unlink($file);
            }
        }

        $this->file->move($this->getUploadRootDir(), $this->path);
    }

    /**
     * @return $this
     */
    public function remove()
    {
        if ($this->path) {
            if (file_exists($file = $this->getAbsolutePath())) {
                unlink($file);
            }
        }

        return $this;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return $this
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        if ($this->path != "" && $this->path != null) {
            return $this->path;
        }

        return $this->defaultPath;
    }

    /**
     * @return string
     */
    public function getDefaultPath()
    {
        return $this->defaultPath;
    }
}
