<?php

namespace RDW\Bundle\AppBundle\Entity;

trait HighlightedTrait
{
    /**
     * @var boolean
     * @ORM\Column(name="is_highlighted", type="boolean", nullable=true)
     */
    private $highlighted;

    /**
     * @return boolean
     */
    public function isHighlighted()
    {
        return $this->highlighted;
    }

    /**
     * @param boolean $highlighted
     *
     * @return $this
     */
    public function setHighlighted($highlighted)
    {
        $this->highlighted = $highlighted;

        return $this;
    }
}
