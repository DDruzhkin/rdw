<?php

namespace RDW\Bundle\AppBundle\Entity;


interface UploadFileInterface
{
    /**
     * @return string
     */
    public function getUploadDir();
}
