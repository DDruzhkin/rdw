(function( $ ){
    $.fn.countAgeHandler = function() {

        $(this).on('change', function(event){
            event.preventDefault();

            var $this = $(this);
            var $form = $this.closest('form');
            var data = {};

            data[$this.data('field') + '[year]'] = $('[name="' + $this.data('field') + '[year]"]').val();
            data[$this.data('field') + '[month]'] = $('[name="' + $this.data('field') + '[month]"]').val();
            data[$this.data('field') + '[day]'] = $('[name="' + $this.data('field') + '[day]"]').val();

            $.ajax({
                url : $form.attr('action'),
                type: $form.attr('method'),
                data : data,
                success: function(html) {
                    $('#age-value').replaceWith($(html).find('#age-value'));
                }
            });


        });
    };
})(jQuery);

$(document).ready(function() {
    $("[data-handler='ageCounter']").countAgeHandler();
});
