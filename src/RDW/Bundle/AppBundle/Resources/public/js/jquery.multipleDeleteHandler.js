(function( $ ){
    $.fn.multipleDeleteHandler = function(options) {
        var settings = $.extend({
            container: "#modal_container",
            content: '#modal_content',
            form: 'form[data-multiple-delete]'
        }, options);

        $(this).on('click', function() {
            var $this = $(this),
                $form = $(settings.form);

            // form not found
            if ( ! $form.length) {
                console.log('Multiple delete form not found');

                return false;
            }

            // no checked inputs
            if ( ! $form.find('li label.multi-checkbox input:checked').length) {
                console.log('Please select rows');

                return false;
            }

            $form.attr('action', $this.data('action'));

            // show title?
            if ($this.data('title')) {
                $(settings.content).find('.modal-title').html($this.data('title'));
                $(settings.content).find('.modal-title-header').show();
            } else {
                $(settings.content).find('.modal-title-header').hide();
            }

            // show message?
            if ($this.data('message')) {
                $(settings.content).find('.modal-body-content').html($this.data('message'));
                $(settings.content).find('.modal-body-content').show();
            } else {
                $(settings.content).find('.modal-body-content').hide();
            }

            $(settings.container).addClass('multiple-delete');
            $(settings.container).html($(settings.content).show()).modal();

            bindFormSubmit();

            return false;
        });

        function bindFormSubmit()
        {
            $('.multiple-delete .modal-footer .danger').on('click', function() {
                $(settings.form).submit();

                return false;
            });
        }
    };
})(jQuery);

$(function() {
    $('[data-handler="multipleDelete"]').multipleDeleteHandler();
});
