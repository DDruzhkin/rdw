(function( $ ){
    $.fn.submitModalFormHandler = function(options) {
        var settings = $.extend({}, options);

        $(this).on('click', function() {

            var form = $(this).parents('form');
            var $this = $(this);

            var options = {
                success:   showResponse,
                type:      'post',
                dataType:  'json',
                resetForm: false
            };

            form.ajaxForm(options);
            form.submit();

            $(document).loaderStart();

            // post-submit callback
            function showResponse(data) {
                if (data.success) {
                    if (typeof(data.popup) != 'undefined') {
                        $('#' + $this.data('content-holder')).modal('hide').html(data.popup).modal('show');
                    } else if ((typeof(data.redirect) != 'undefined') && (data.redirect.length > 0)) {
                        window.location = data.redirect;
                    } else {
                        window.location.reload();
                    }
                } else {
                    $('#' + $this.data('content-holder')).html(data.content);
                    $("[data-handler='" + $this.data('handler') + "']").submitModalFormHandler();
                }

                $(document).loaderStop();

                return false;
            }

            return false;
        });
    };
})(jQuery);

$(function() {
    $("[data-handler='submitModalForm']").submitModalFormHandler();
});
