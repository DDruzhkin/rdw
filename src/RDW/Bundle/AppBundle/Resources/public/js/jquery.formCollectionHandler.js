(function( $ ){
    $.fn.formCollection= function(options) {

        $(this).on('click', function(event){
            event.preventDefault();

            var $target = $(event.target);
            var collection = $('#' + $target.data('collection'));
            var newWidget = collection.attr('data-prototype');

            if (collection.find('li').length > 0) {
                var lastIndex = parseInt(collection.find('> li:last-child [name]').attr('name')
                    .match(/\[([0-9]*)\]/).pop());
            } else {
                var lastIndex = 0;
            }

            newWidget = newWidget.replace(/__name__/g, (lastIndex + 1));

            var newLi = $('<li></li>').html(newWidget);
            newLi.appendTo(collection);

            $('select.el_84').selecter({mobile: true,customClass: 'el_84'});
            $('select.el_123').selecter({mobile: true,customClass: 'el_123'});
            $('select:not(.search-form-select select)').selecter({mobile: true});
        });

        return false;
    };

})(jQuery);

(function( $ ){

    $.fn.autoInit = function(options) {
        $.each( $(this), function(key, obj) {
            var collection = $('#' + $(obj).data('collection'));

            if(collection.find('> li').length == 0) {
                $(obj).trigger('click');
            }
        });

        return false;
    };
})(jQuery);
