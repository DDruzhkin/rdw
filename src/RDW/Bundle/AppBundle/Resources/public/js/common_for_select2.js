$(document).ready(function() {


    // top_banner positioning
    $(function() {
        $(window).resize(function() {
            if ($("#logo").css("text-align") == "right") {
                $(".top_banner").insertAfter($(".content .filter_block"));
            } else {
                $(".top_banner").insertBefore($(".wrapper > .body"));
            }
        });
        $(window).resize();
    });


    // member_nav positioning
    $(function() {
        $(window).resize(function() {
            if ($("#logo").css("text-align") == "left") {
                $(".member_nav").prependTo($(".sidebar"));
            } else {
                $(".member_nav").insertBefore($(".content #logo"));
            }
        });
        $(window).resize();
    });


    // ad_list positioning
    $(function() {
        $(window).resize(function() {
            if ($("#logo").css("text-align") == "left") {
                $(".ad_list").insertAfter($(".sidebar .info_block"));
            } else if ($("#logo").css("text-align") == "center") {
                $(".ad_list").insertBefore($(".sidebar .info_block"));
            } else {
                $(".ad_list").insertBefore($(".content .mobile_banners"));
            }
        });
        $(window).resize();
    });


    // manager_block positioning
    $(function() {
        $(window).resize(function() {
            if ($("#logo").css("text-align") == "left") {
                $(".manager_block").insertAfter($(".sidebar .member_nav"));
            } else {
                $(".manager_block").insertAfter($(".content .menu_hold"));
            }
        });
        $(window).resize();
    });


    // manager_block foto positioning
    $(function() {
        $(window).resize(function() {
            if ($("#logo").css("text-align") == "center") {
                $(".manager_block .foto").insertBefore($(".manager_block h4"));
            } else {
                $(".manager_block .foto").insertBefore($(".manager_block .name"));
            }
        });
        $(window).resize();
    });


    // temp_msg positioning
    $(function() {
        $(window).resize(function() {
            if ($(".content .manager_block").length) {
                if ($("#logo").css("text-align") != "left") {
                    if ($(".content .ad_table").length) {
                        return
                    } else {
                        $(".content .temp_msg").insertBefore($(".content .manager_block"));
                    }
                }
            }
        });
        $(window).resize();
    });


    // small_container positioning
    $(function() {
        $(window).resize(function() {
            if ($("#logo").css("text-align") == "right") {
                $(".small_container.mobile_move").prependTo($(".col-sm-7.col-md-8 .content"));
            } else {
                $(".small_container.mobile_move").prependTo($(".col-sm-5.col-md-4 .content"));
            }
        });
        $(window).resize();
    });


    // share_block positioning
    $(function() {
        $(window).resize(function() {
            if ($("#logo").css("text-align") == "right") {
                $(".content .quick_info_bottom .share_block").insertAfter($(".content .quick_info_bottom .box_2"));
            } else {
                $(".content .quick_info_bottom .share_block").insertAfter($(".content .quick_info_bottom .box_1 .foto"));
            }
        });
        $(window).resize();
    });


    // top search url_tag remove
    $(function() {
        $(".top_row").on("click", ".url_tag", function(e) {
            var i = checkNumberOfElements();
            if (i <= 1) {
                $(this).parent().remove();
            } else {
                $(this).remove();
            }
            e.preventDefault();
        });

        function checkNumberOfElements() {
            var i = $(".url_tag").length;
            if (typeof i === undefined) {
                return 0;

            } else {
                return i;
            }
        }
    });


    // top search autoresize input
    $.fn.textWidth = function(text, font) {
        if (!$.fn.textWidth.fakeEl) $.fn.textWidth.fakeEl = $("<span>").hide().appendTo(document.body);
        $.fn.textWidth.fakeEl.text(text || this.val() || this.text()).css("font", font || this.css("font"));
        return $.fn.textWidth.fakeEl.width();
    };
    $(".content .filter_block .top_row .tags_box .input").on("input", function() {
        var padding = 15;
        var valWidth = ($(this).textWidth() + padding) + "px";
        $("#" + this.id + "-width").html(valWidth);
        $(".content .filter_block .top_row .tags_box .input").css("width", valWidth);
    }).trigger("input");


    // top search focus to input
    $(".content .filter_block .top_row .tags_box").on("click", function() {
        $(this).find(".input").focus();
    });


    // temp_msg close
    $(".temp_msg .url_close").on("click", function(e) {
        $(this).parent().hide();
        e.preventDefault();
    });


    // sliders
    if ($.fn.bxSlider) {

        // get viewport size
        function viewport() {
            var e = window,
                a = 'inner';
            if (!('innerWidth' in window)) {
                a = 'client';
                e = document.documentElement || document.body;
            }
            return {
                width: e[a + 'Width'],
                height: e[a + 'Height']
            };
        }

        // home page slider
        var elSlider = $(".bx_home_slider");
        if (elSlider.length > 0) {
            var sliderOptions_d = {
                    auto: false,
                    autoHover: true,
                    minSlides: 3,
                    maxSlides: 3,
                    moveSlides: 1,
                    slideWidth: 276,
                    slideMargin: 3,
                    infiniteLoop: false,
                    pager: false,
                },
                sliderOptions_t = {
                    auto: false,
                    autoHover: true,
                    minSlides: 3,
                    maxSlides: 3,
                    moveSlides: 1,
                    slideWidth: 226,
                    slideMargin: 2,
                    infiniteLoop: false,
                    pager: false,
                },
                sliderOptions_m = {
                    auto: false,
                    autoHover: true,
                    minSlides: 1,
                    maxSlides: 1,
                    moveSlides: 1,
                    infiniteLoop: false,
                    pager: false
                };

            //Init Slider
            elSlider.bxSlider(sliderOptions_d);
            if (viewport().width > 768 && viewport().width < 1220) {
                elSlider.reloadSlider(sliderOptions_t);
            } else if (viewport().width < 768) {
                elSlider.reloadSlider(sliderOptions_m);
            }

            //On Screen Resize
            $(window).resize(function() {
                if (viewport().width > 768 && viewport().width < 1220) {
                    elSlider.reloadSlider(sliderOptions_t);
                } else if (viewport().width < 768) {
                    elSlider.reloadSlider(sliderOptions_m);
                } else {
                    elSlider.reloadSlider(sliderOptions_d);
                }
            });
        }

    }


    // home page slider star
    $(document).on("click", ".url_star", function(e) {
        $(this).toggleClass("act");
        e.preventDefault();
    });


    // custom select
    if ($.fn.selecter) {
        $("select.el_84").selecter({
            mobile: true,
            customClass: 'el_84'
        });
        $("select.el_123").selecter({
            mobile: true,
            customClass: 'el_123'
        });
        $("select.el_133").selecter({
            mobile: true,
            customClass: 'el_133'
        });
        $("select:not('.filter_options select, .top_row select')").selecter({
            mobile: true
        });
    }


    // checkbox, radio button
    if ($.fn.iCheck) {
        $("input").iCheck({
            checkboxClass: "icheckbox_minimal checkbox",
            radioClass: "iradio_minimal radio"
        });
    }


    // checkbox selection
    $(function() {
        var checkAll = $(".content .sort_block .checkbox_box input");
        var checkboxes = $(".content .options_block li label input");

        checkAll.on("ifChecked ifUnchecked", function(event) {
            if (event.type == "ifChecked") {
                checkboxes.iCheck("check");
            } else {
                checkboxes.iCheck("uncheck");
            }
        });

        checkboxes.on("ifChanged", function(event) {
            if (checkboxes.filter(":checked").length == checkboxes.length) {
                checkAll.prop("checked", "checked");
            } else {
                checkAll.removeProp("checked");
            }
            checkAll.iCheck("update");
        });
    });

    $(".content .options_block li label input").on("ifChecked", function() {
        $(this).parent().parent().parent().addClass("selected");
    }).on("ifUnchecked", function() {
        $(this).parent().parent().parent().removeClass("selected");
    });


    // ad_table checkbox selection
    $(".content .ad_table tbody td input").on("ifChecked", function() {
        $(this).parent().parent().addClass("selected");
    }).on("ifUnchecked", function() {
        $(this).parent().parent().removeClass("selected");
    });


    // custom scroll
    if ($.fn.mCustomScrollbar) {
        $(".content .filter_block .filter_options > li .drop .hold_scroll, .selecter-options").mCustomScrollbar({
            mouseWheelPixels: 160,
            scrollInertia: 300,
            advanced: {
                updateOnContentResize: true,
                autoScrollOnFocus: false
            }
        });
        $(".content .menu_hold").mCustomScrollbar({
            scrollInertia: 300,
            horizontalScroll: "true",
            advanced: {
                autoExpandHorizontalScroll: true
            }
        });
    }


    // filter_options drop
    $("body").on("click", function() {
        $(".content .filter_block .filter_options li .url").removeClass("open");
        $(".content .filter_block .filter_options > li .drop").hide();
    });

    $(".content .filter_block .filter_options li .url, .content .filter_block .filter_options > li .drop .url_ok").on("click", function(e) {
        if ($(this).next().is(":visible")) {
            $(this).removeClass("open").next().hide();
        } else {
            $(".content .filter_block .filter_options li .url").removeClass("open");
            $(".content .filter_block .filter_options > li .drop").hide();
            $(this).addClass("open").next().show();
        }
        e.preventDefault();
    });

    $(".content .filter_block .filter_options li .url, .content .filter_block .filter_options > li .drop").on("click", function(e) {
        e.stopPropagation();
    });


    // filter hidden_form show
    $(".content .filter_block .button_box .url_toggle").on("click", function(e) {
        $(this).parent().hide().prev().show();
        e.preventDefault();
    });


    // filter hidden_form hide
    $(".content .filter_block .hidden_form .url_toggle").on("click", function(e) {
        $(this).parent().hide().next().show();
        e.preventDefault();
    });


    // google maps
    if ($(".contacts_map").length) {
        // map 1
        var latLog = new google.maps.LatLng(54.933193, 23.938618);
        var myOptions = {
            center: latLog,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map_1"), myOptions);
        var marker = new google.maps.Marker({
            position: latLog,
            map: map
        });

        // map 2
        var latLog = new google.maps.LatLng(54.933193, 23.938618);
        var myOptions = {
            center: latLog,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map_2"), myOptions);
        var marker = new google.maps.Marker({
            position: latLog,
            map: map
        });

        // map 3
        var latLog = new google.maps.LatLng(54.933193, 23.938618);
        var myOptions = {
            center: latLog,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map_3"), myOptions);
        var marker = new google.maps.Marker({
            position: latLog,
            map: map
        });

        // map 4
        var latLog = new google.maps.LatLng(54.933193, 23.938618);
        var myOptions = {
            center: latLog,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map_4"), myOptions);
        var marker = new google.maps.Marker({
            position: latLog,
            map: map
        });

        // map 5
        var latLog = new google.maps.LatLng(54.933193, 23.938618);
        var myOptions = {
            center: latLog,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var image = "img/map_dot.png";
        var map = new google.maps.Map(document.getElementById("map_5"), myOptions);
        var marker = new google.maps.Marker({
            position: latLog,
            map: map,
            icon: image
        });

        // google maps center on resize window
        var center;

        function calculateCenter() {
            center = map.getCenter();
        }
        google.maps.event.addDomListener(map, 'idle', function() {
            calculateCenter();
        });
        google.maps.event.addDomListener(window, 'resize', function() {
            map.setCenter(center);
        });
    }


    $("#modal_map").on("shown.bs.modal", function() {
        var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
    });


    // accordion
    if ($.fn.accordion) {
        $(".faq_list").accordion({
            heightStyle: "content",
            activate: function(event, ui) {
                var scrollTop = $(".faq_list").scrollTop();
                if (!ui.newHeader.length) return;
                var top = $(ui.newHeader).offset().top;
                if ($("#logo").css("text-align") != "left") {
                    $("html,body").animate({
                        scrollTop: scrollTop + top
                    }, "slow");
                }
            }
        });
    }


    // views_table time for mobile
    $(".content .views_table tr").each(function() {

        $(this).find("td:first-child").each(function() {
            $(this).append("<span class='txt_mob'></span>").find(".txt_mob").append($(this).parent().find("td.time").text());
        });

    });


    // custom upload
    $(".url_upload").on("click", function(e) {
        $(this).parent().find(".file_input").click();
        var file_input = $(this).parent().find(".file_input");
        e.preventDefault();

        $(file_input).on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).parent().find(".filename").html(fileName);
            $(this).parent().find(".no_file").hide();
        });
    });



    // manager_block show-hide on mobile
    $(".manager_block h4").on("click", function() {
        if ($("#logo").css("text-align") == "right") {
            $(this).toggleClass("open");
        }
    });


    // default_form show-hide
    $(".content .txt_container h2.toggle").on("click", function() {
        $(this).toggleClass("open");
    });


    // about_me_big height on desctop
    $(function() {
        $(window).resize(function() {
            if ($("#logo").css("text-align") == "left") {
                var col_wide = $(".col_wide").outerHeight();
                var about_me = $(".content .about_me").outerHeight();
                $(".content .about_me_big").css("min-height", col_wide - about_me - 22);
            } else {
                $(".content .about_me_big").removeAttr("style");
            }
        });
        $(window).resize();
    });

    // modal-backdrop fix
    $(".modal").on("shown.bs.modal", function() {
        $(".modal.in").modal("handleUpdate");
    });


    // options_block toogle_box on mobile
    $(".content .options_block li .toogle_box .url_toggle").on("click", function(e) {
        $(this).parent().toggleClass("open");
        e.preventDefault();
    });


    // calendar
    if ($.fn.datepicker) {
        $(".datepicker").datepicker({
            firstDay: 1,
            showOtherMonths: true,
            selectOtherMonths: true,
            showOn: "both",
            dateFormat: "mm.dd.yy",
            onSelect: function(dateText, inst) {
                $(this).parent().parent().parent().parent().parent().parent().find(".ui_date").text(dateText);
                $(this).parent().hide();
            }
        });
    }


    // calendar show
    $(".content .options_block li .toogle_box .url_list li a.icon_4").on("click", function(e) {
        $(this).next().toggle();
        e.preventDefault();
    });


    // note_form show
    $(".content .options_block li .toogle_box .url_list li a.icon_3").on("click", function(e) {
        $(this).parent().parent().parent().parent().parent().find(".note_form").toggleClass("open");
        $(this).parent().parent().parent().parent().parent().next(".note_form_mobile").toggleClass("open");
        e.preventDefault();
    });


    // select2 plugin
    if ($.fn.select2) {
        $(".blue #main_search").select2({
            minimumResultsForSearch: Infinity,
            placeholder: "Ищите вакансий..."
        });
        $(".red #main_search").select2({
            minimumResultsForSearch: Infinity,
            placeholder: "Ищите резюме..."
        });
    }


	// tooltip_box positioning
	$(".url_info").hover(function() {
		$(this).find(".tooltip_box").position({
		  my: "left-13 top+22",
		  at: "left top",
		  of: this,
		  collision: "flip none"
		});
	});
});
