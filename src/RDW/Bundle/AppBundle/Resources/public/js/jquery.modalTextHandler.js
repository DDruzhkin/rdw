(function( $ ){
    $.fn.modalText= function(options) {

        var settings = $.extend({
            container: "#modal_container",
            content: "#modal_content"
        }, options);

        var textString = '';

        $(this).on('click', function(event){

            if ($(event.target).attr('data-text') !== undefined) {
                textString = $(event.target).attr('data-text');
                $(settings.content).find('.modal-body-content').html(textString);
                $(settings.content).find('.modal-body-content').show();

                $(settings.content).find('.modal-title-header').hide();
                $(settings.content).find('.modal-footer').hide();
                $(settings.container).html($(settings.content).show()).modal();
            }

            return false;
        });
    };
})(jQuery);

$(function() {
    $("[data-handler='modalText']").modalText();
});

