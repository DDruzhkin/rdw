(function( $ ){
    $.fn.multipleSubmitHandler = function(options) {
        var settings = $.extend({
            form: 'form[data-multiple-submit]'
        }, options);

        $(this).on('click', function() {
            var $this = $(this),
                $form = $(settings.form);

            // form not found
            if ( ! $form.length) {
                console.log('Multiple submit form not found');

                return false;
            }

            // no checked inputs
            if ( ! $form.find('li label.multi-checkbox input:checked').length) {
                console.log('Please select rows');

                return false;
            }

            if ($this.data('method')) {
                $form.attr('method', $this.data('method'));
            }

            $form.attr('action', $this.data('action'));

            $(settings.container).addClass('multiple-confirm');
            $(settings.container).html($(settings.content).show()).modal();

            $(settings.form).submit();

            return false;
        });
    };
})(jQuery);

$(function() {
    $('[data-handler="multipleSubmit"]').multipleSubmitHandler();
});
