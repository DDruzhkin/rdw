(function( $ ){
    $.fn.select2Field = function(options) {
        var settings = $.extend({
            type: ''
        }, options);

        if (settings.type != 'single' && settings.type != 'multi') {
            console.log('Wrong select2Field.type', settings.type);

            return;
        }

        var selector = '.select2-hidden-' + settings.type,
            selectOptions = {
            formatResult: function (item) { return item.title; },
            formatSelection: function (item) { return item.title; },
            id: function (item) { return item.value; },
            ajax: {
                dataType: 'json',
                quietMillis: 500,
                data: function (term, page) {
                    return {
                        q: term
                    };
                },
                results: function (data, page) {
                    return { results: data };
                },
                cache: true
            },
            initSelection: function(element, callback) {
                $.ajax($(element).data('init-url'), {
                    data: formatSelect2MultiInitValue(element),
                    dataType: "json"
                }).done(function(response) {
                    callback(response[0]);
                });
            }
        };

        if ('multi' == settings.type) {
            selectOptions.multiple = true;
            selectOptions.closeOnSelect = false;
            selectOptions.initSelection = function(element, callback) {
                $.ajax($(element).data('init-url'), {
                    data: formatSelect2MultiInitValue(element),
                    dataType: "json"
                }).done(function(response) {
                    callback(response);
                });
            }
        }

        $(selector).select2(selectOptions);

        function formatSelect2MultiInitValue (element)
        {
            var ids = $(element).val().split(',');
            var values = [];

            $.each(ids, function(index, value) {
                values.push($(element).data('filter') + ':' + value);
            });

            return { val: values.join(',') };
        }
    };
})(jQuery);
