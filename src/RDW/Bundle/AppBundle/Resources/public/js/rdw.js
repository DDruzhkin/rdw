$(function() {
    $("[data-handler='modalLink']").modalLink({
        'modalContentHolder' : 'modal_container'
    });

    var subscribeEmail = {
        email: "",
        getEmail: function () {
            return this.email;
        },
        setEmail: function (email) {
            this.email = email;
            return this.email;
        }
    }


    $(".copy-url_order").on("click", function (event) {
        event.preventDefault();
        subscribeEmail.setEmail($(this).closest(".sub_box").find("input#prepare-subscribe").val());
        var blockError = $(this).closest(".sub_box").find(".error_txt");
        if (subscribeEmail.getEmail()) {
            if (validateEmail(subscribeEmail.getEmail())) {
                localStorage.setItem('subscribeEmail', subscribeEmail.getEmail());
                $(this).closest(".sub_box").find("[data-handler='modalLinkSubscribe']").modalLink({
                    'modalContentHolder' : 'modal_container'
                });
                $(this).closest(".sub_box").find("[data-handler='modalLinkSubscribe']").click();
                if (blockError.hasClass("hide") == false) {
                    blockError.addClass("hide");
                }
            } else {
                if (blockError.hasClass("hide")) {
                    blockError.removeClass("hide");
                }
                blockError.find("span").html(blockError.data("invalid-email"));
            }
        } else {
            if (blockError.hasClass("hide")) {
                blockError.removeClass("hide");
            }
            blockError.find("span").html(blockError.data("required"));
        }
    });

    $("[data-filter-specialization]").on("click", function(){
        $("[data-filter-specialization]").toggleClass("active");
        $("[data-show]").toggleClass("hidden");
    })

    $("[data-handler='form-collection']").formCollection();
    $("[data-autoinit]").autoInit();

    // $(function() {
    //     $("[data-handler='advanced-search']").on('click', function(e) {
    //         e.preventDefault();
    //         if (! $('.expand_form').hasClass('open')) {
    //             $('.expand_form').addClass('open');
    //         }
    //         $('.advanced-search-button').trigger('click');
    //     });
    // });

    $('.select2-hidden-multi').select2Field({'type': 'multi'});
    $('.select2-hidden-single').select2Field({'type': 'single'});

    var $positionAutocomlete = $('.position-autocomplete');
    var ajaxUrl = $positionAutocomlete.attr('data-ajax-url');
    var findPaUrl = $positionAutocomlete.attr('data-find-pa-url');
    $positionAutocomlete.autocomplete({
        source: function(request, response) {
            $.ajax( {
                url: ajaxUrl,
                dataType: "json",
                type : 'Get',
                data: {
                    q: request.term
                },
                success: function(data) {
                    var updatedData = [];
                    $.each(data, function(i, position) {
                        updatedData.push({
                            'value': position.title,
                            'id': position.value
                        })
                    });
                    response(updatedData);
                }
            });
        },
        select: function(event, ui) {
            $.ajax( {
                url: findPaUrl,
                dataType: "json",
                type : 'Get',
                data: {
                    id: ui.item.id
                },
                success: function(data) {
                    window.popupPa.removeAll();
                    window.popupPa.initializeIds(data);
                    window.popupPa.generateProfessionalAreaField();
                }
            });
        },
        minLength: 2
    });

    $('.money').mask('000 000 000 000.00', {reverse: true});
    $('.salary').mask('00 000 000', {reverse: true});
    $('.phone').mask('+000 00 000 00 00');

    if( typeof(CKEDITOR) !== "undefined" ) {
        CKEDITOR.on('instanceReady', function() {
            var defaultHtml = "<ul><li></li></ul>";
            var instances = ["rdw_job_requirements", "rdw_job_workingConditions", "rdw_job_responsibilities", "rdw_job_additionalInformation"];
            instances.forEach(function (item, i, arr) {
                if (CKEDITOR.instances[item] && CKEDITOR.instances[item].editable().getHtml() == "") {
                    CKEDITOR.instances[item].editable().setHtml(defaultHtml);
                }
            });
        });
    }

    $("#show-contact").on("click", function () {
        if ($(".list.list-contact").hasClass("open") == false) {
            if (typeof ga !== 'undefined') {
                ga('send', 'event', 'look_contacts', 'look_contacts');
            }
            $(this).find("span.text-show").text($(this).data("text-close"));
            $.ajax({
                type: "POST",
                data: {"show-contact": 1},
                beforeSend: function () {
                    $(".list.list-contact").toggleClass("open");
                }
            })
        } else {
            $(".list.list-contact").toggleClass("open");
            $(this).find("span.text-show").text($(this).data("text-open"));
        }
    })

    function validateEmail(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    }

});