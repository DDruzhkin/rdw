(function( $ ){
    $.fn.checkedItemsCountHandler = function(options) {
        var settings = $.extend({}, options);

        $(this).on('ifChanged', function() {
            var $this = $(this),
                checkItemsCount = $this.parents('form').find('li label.multi-checkbox input:checked').length;

            $('.checkbox-counter').html(checkItemsCount);
        });
    };
})(jQuery);

$(function() {
    $('[data-handler="checkedItemsCount"]').checkedItemsCountHandler();
});
