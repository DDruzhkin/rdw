(function( $ ){
    $.fn.confirmHandler= function(options) {
        var settings = $.extend({
            container: "#modal_container",
            content: '#modal_content'
        }, options);

        $(this).on('click', function(event){
            // remove multiple-delete/confirm class to avoid multiple delete submit
            $(settings.container).removeClass('multiple-delete');
            $(settings.container).removeClass('multiple-confirm');

            var element = $(event.target),
                requestUrl = '';

            if ($(event.target).attr('data-url') === undefined) {
                requestUrl = $(event.target).parents('a:first').attr('data-url');
            } else {
                requestUrl = $(event.target).attr('data-url');
            }

            // show title?
            if (element.data('title')) {
                $(settings.content).find('.modal-title').html(element.data('title'));
                $(settings.content).find('.modal-title-header').show();
            } else {
                $(settings.content).find('.modal-title-header').hide();
            }

            // show message?
            if (element.data('message')) {
                $(settings.content).find('.modal-body-content').html(element.data('message'));
                $(settings.content).find('.modal-body-content').show();
            } else {
                $(settings.content).find('.modal-body-content').hide();
            }

            // confirm button title?
            var $confirmButton = $(settings.content).find('.modal-footer a');

            if (element.data('confirm-title')) {
                $confirmButton.html(element.data('confirm-title'));
            } else {
                $confirmButton.html($confirmButton.data('title'));
            }

            $(settings.content).find('.danger').attr('href', requestUrl);
            $(settings.container).html($(settings.content).show()).modal();

            return false;
        });
    };
})(jQuery);

$(document).ready(function() {
    $("[data-handler='confirm']").confirmHandler();
});
