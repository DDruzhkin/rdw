(function( $ ){
    $.fn.GAEventHandler = function(options) {
        var settings = $.extend({}, options);

        $(this).on('click', function() {
            var $this = $(this),
                event = $this.data('event'),
                action = $this.data('action'),
                label = $this.data('label'),
                value = $this.data('value');

            if (! event.length) {
                console.log('Missing "event" attribute');

                return false;
            }

            if (! action.length) {
                console.log('Missing "action" attribute');

                return false;
            }

            ga('send', 'event', event, action, label, value);
        });
    };
})(jQuery);

$(function() {
    $('[data-handler="GAEventHandler"]').GAEventHandler();
});
