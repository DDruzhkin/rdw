(function( $ ){
    $.fn.perPageHandler = function() {

        $(this).on('change', function(event){
            event.preventDefault();

            var element = $(event.target),
                requestUrl = element.val();

            if (requestUrl.length != 0){
                window.location.href = requestUrl;
            }

            return false;
        });
    };
})(jQuery);

$(document).ready(function() {
    $("[data-handler='perPage']").perPageHandler();
});
