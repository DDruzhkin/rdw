(function( $ ){
    $.fn.thumbDeleteHandler = function(options) {
        var settings = $.extend({
            container: "#modal_container",
            content: '#modal_content'
        }, options);

        this.each(function() {
            bindClick($(this));
        });

        function bindClick($deleteLink)
        {
            $deleteLink.unbind();
            $deleteLink.on('click', function(event){
                handleClick(event);
                bindThumbDelete($deleteLink);

                return false;
            });
        }

        function handleClick(event)
        {
            // remove multiple-delete/confirm class to avoid multiple delete submit
            $(settings.container).removeClass('multiple-delete');
            $(settings.container).removeClass('multiple-confirm');

            var element = $(event.target),
                requestUrl = '';

            if ($(event.target).data('url') === undefined) {
                requestUrl = $(event.target).parents('a:first').data('url');
            } else {
                requestUrl = $(event.target).data('url');
            }

            // show title?
            if (element.data('title')) {
                $(settings.content).find('.modal-title').html(element.data('title'));
                $(settings.content).find('.modal-title-header').show();
            } else {
                $(settings.content).find('.modal-title-header').hide();
            }

            // show message?
            if (element.data('message')) {
                $(settings.content).find('.modal-body-content').html(element.data('message'));
                $(settings.content).find('.modal-body-content').show();
            } else {
                $(settings.content).find('.modal-body-content').hide();
            }

            // confirm button title?
            var $confirmButton = $(settings.content).find('.modal-footer a');

            if (element.data('confirm-title')) {
                $confirmButton.html(element.data('confirm-title'));
            } else {
                $confirmButton.html($confirmButton.data('title'));
            }

            $confirmButton.data('url', requestUrl);

            $(settings.container).html($(settings.content).show()).modal();

            return false;
        }

        function bindThumbDelete($deleteLink)
        {
            var $button = $(settings.content + ' .modal-footer .danger');

            $button.unbind();
            $button.on('click', function() {
                var $modalLink = $(this),
                    $imageHolder = $($deleteLink.data('image-holder')),
                    field = $deleteLink.data('field'),
                    filename = $deleteLink.data('filename'),
                    $file = $($deleteLink.data('file'));

                $imageHolder.html('');
                $file.val('');

                if (filename.length) {
                    $.ajax({
                        url: $deleteLink.data('url'),
                        data: {
                            'field': field,
                            'filename': filename
                        },
                        dataType: 'json',
                        success: function(data) {
                            if (data.success) {
                                $deleteLink.data('filename', '');

                                bindClick($deleteLink);
                            }
                        }
                    });
                }

                $(settings.container).html($(settings.content)).modal('hide');

                return false;
            });
        }
    };
})(jQuery);

$(document).ready(function() {
    $("[data-handler='thumbDelete']").thumbDeleteHandler();
});
