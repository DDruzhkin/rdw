(function( $ ){
    $.fn.modalTabsHandler = function(options) {
        var settings = $.extend({
            modal: "#modal_container",
        }, options);

        $(settings.modal).on("shown.bs.modal", function() {
            $(".modal .modal-dialog .tabs").responsiveTabs({
                setHash: false,
                collapsible: 'accordion',
                startCollapsed: 'accordion',
                activate: function(event, tab){
                    if ($.fn.iCheck) {
                        $("input").iCheck({
                            checkboxClass: "icheckbox_minimal checkbox"
                        });
                    }
                }
            });
        });
    };
})(jQuery);
