(function( $ ){
    $.fn.registrationPopupHandler = function(options) {

        var settings = $.extend({
            url: ''
        }, options);

        if (settings.url.length == 0) {
            throw 'Missing url';
        }

        $(document).loaderStart();

        $.ajax({
            url: settings.url,
            dataType: 'json',
            success: function(data) {
                $('#modal_container').modal('hide').html(data.popup).modal('show');
            },
            complete: function () {
                $(document).loaderStop();
            }
        });

        return false;
    };
})(jQuery);
