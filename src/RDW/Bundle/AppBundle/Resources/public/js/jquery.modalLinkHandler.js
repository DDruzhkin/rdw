(function( $ ){
    $.fn.modalLink= function(options) {

        var settings = $.extend({
            contentHolder: "#modal_container"
        }, options);

        $(this).on('click', function(event){
            var $el = $(this);
            if ($el.data('clicked')){
                // Previously clicked, stop actions
                event.preventDefault();
                event.stopPropagation();
            } else {
                // Mark to ignore next click
                $el.data('clicked', true);

                var requestUrl = '';

                if ($(event.target).attr('data-url') === undefined) {
                    requestUrl = $(event.target).parents('a:first').attr('data-url');
                } else {
                    requestUrl = $(event.target).attr('data-url');
                }

                $(document).loaderStart();

                $.ajax({
                    url: requestUrl,
                    dataType: 'html',
                    success: function(data) {
                        $(settings.contentHolder).html(data).modal();
                    },
                    complete: function () {
                        $(document).loaderStop();

                        window.setTimeout(function(){
                            $el.removeData('clicked');
                        }, 1000)
                    }
                });
            }

            return false;
        });

        return false;
    };
})(jQuery);