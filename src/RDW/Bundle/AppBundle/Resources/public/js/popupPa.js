    $(function(){
    $("body").on("click", ".pick-label", function () {
        var inst = $('[data-remodal-id=modal]').remodal();
        inst.open();
    });

    $("body").on("click", ".parent-name", function(){
        $(this).parent().find(".children").toggleClass("open");
        $(this).parent().toggleClass("open");
    });

    $("body").on("click", ".pa-name", function(){
        var checkInput = $(this).parent().find("input[type=checkbox]");
        var checkStatus = checkInput.is(":checked") ? 'uncheck' : 'check';
        checkInput.iCheck(checkStatus);
    });

    $('body').on('ifChecked', ".popup .tree input[type=checkbox]", function(e) {
        professionalArea.check(e);

    });

    $('body').on('ifUnchecked', ".popup .tree input[type=checkbox]", function(e) {
        professionalArea.unCheck(e);
    });

    $("body").on("keyup", "#filter-pa", function(){
        var parents = $(".parent");
        var searchText = $(this).val().toLowerCase();
        $(".child").removeClass("show").css({"display":"none"});

        if (searchText.length <= 1) {
            $(".child").removeClass("show").css({"display":"block"});
            $(".children, .parent").removeClass("open");
            $(".parent").css({"display":"block"});
            return 0;
        }
        
        for(var i = 0; i < parents.length; i++) {
            var childsPa = parents[i].children[1].children;

            for (var j = 0; j < childsPa.length; j++) {
                if (
                    $(parents[i].children[1].children[j]).find(".pa-name").text().toLowerCase().indexOf(searchText) + 1 ||
                    $(parents[i]).find(".parent-name").text().toLowerCase().indexOf(searchText) + 1) {
                    $(parents[i].children[1].children[j]).addClass("show");
                }
            }
        }
        professionalArea.showOrHideParentArea();
    });

    $("body").on("click", "#pa-pick", function() {
        var lists = "";
        $(".pa-list").text("");

        professionalArea.generateProfessionalAreaField();

        var inst = $('[data-remodal-id=modal]').remodal();
        inst.close();
    });

    $("body").on("click", "span.delete-pa", function(){

        var all = $(this).parent().hasClass("parent-wr") ? 1 : 0;
        var parentId = $(this).parent().hasClass("parent-wr") ? $(this).parent().data("id") : $(this).parent().data("parent");

        professionalArea.removeId($(this).parent().data("id"), parentId, all);

        if ($(this).closest(".child-pa-list").children().length - 1 == 0) {
            $(this).closest(".parent-wr").remove();
        } else {
            $(this).parent().remove();
        }
     });

    var professionalArea = {
        count: 0,
        maxCount: 6,
        ids: {},
        inc: function() {
            if (this.count < this.maxCount) {
                this.count++;
            }
            this.disabledOrEnabledInputs();
        },
        dec: function() {
            if (this.count > 0) {
                this.count--;
            }
            this.disabledOrEnabledInputs();
        },
        disabledOrEnabledInputs: function() {
            if (this.count != this.maxCount) {
                $('.popup .tree input').iCheck('enable');
                return 0;
            }

            $(".popup .tree input[type=checkbox]").each(function(item, i, arr){
                if ($(i).is(":checked") == false) { 
                    $(i).iCheck('disable');
                }
            })                
        },
        showOrHideParentArea: function() {
            $(".child").parent().removeClass("open").parent().removeClass("open").css({"display":"none"});
            $(".child.show").parent().addClass("open").parent().addClass("open").css({"display":"block"});
        },
        addId: function(id, parent) {
            //parent = parseInt(parent);
            if (parent in this.ids == false) {
                this.ids[parent] = [];
            }

            this.ids[parent].push(id.toString());
            var idsInString = this.arrayIdsToString();

            $(".pick-professional-area + input[type=hidden]").val(idsInString);

        },
        arrayIdsToString: function() {
            var allIdsWithParent = [];

            if (Object.keys(this.ids).length) {
                
                for (item in this.ids) {
                    if (this.ids[item].length != 0) {
                        allIdsWithParent.push(this.ids[item]);
                        allIdsWithParent.push(item);
                    }
                }
            }

            return allIdsWithParent.join();
        },
        removeId: function(id, parent, all) {
            id = id.toString();
            if (Object.keys(this.ids).length) {
                if (all && parent in this.ids) {
                    for (var i = 0; i <= this.ids[parent].length; i++) {
                        $(".popup .tree input[name="+this.ids[parent][0]+"]").iCheck('uncheck');
                    }
                    delete this.ids[parent];
                } else {
                    if (this.ids[parent].indexOf(id) + 1) {
                        this.ids[parent].splice(this.ids[parent].indexOf(id), 1);
                        $(".popup .tree input[name="+id+"]").iCheck('uncheck');
                    }
                }
            }
            var idsInString = this.arrayIdsToString();

            $(".pick-professional-area + input[type=hidden]").val(idsInString);
            
        },
        removeAll: function () {
            $('.pick-professional-area .pa-list .parent-wr').each(function () {
                professionalArea.removeId($(this).data("id"), $(this).data("id"), 1);
                $(this).remove();
            });
            this.ids = {};
        },
        initializeIds: function(allIds) {
            if (!allIds) {
                var paStrIds = $(".pick-professional-area + input[type=hidden]").val();
                allIds = paStrIds != "" ? paStrIds.split(","): 0;
            }
            if (allIds) {
                allIds.forEach(function(i, item, arr){
                    $(".pa-check[name="+i+"]").iCheck('check');
                });
            }
        },
        generateProfessionalAreaField: function() {
            if (Object.keys(this.ids).length) {
                for (var parent in this.ids) {
                    var parentName = $(".pa-check[data-parent-id='"+parent+"']").data("parent-name");
                    var ulWr = "<li class='parent-wr' data-id="+parent+"><span class='delete-pa'></span><span class='pa-name'>"+parentName+"</span><ul class='child-pa-list'>";
                    
                    if (! this.ids[parent].length) {
                        continue;
                    }

                    for (var i = 0; i < this.ids[parent].length; i++) {
                        var item = $("input.pa-check[name="+this.ids[parent][i]+"]").closest(".child").find(".pa-name").text();
                        ulWr += "<li data-parent="+parent+" data-id='"+this.ids[parent][i]+"'><span class='pa-name'>"+item+"</span><span class='delete-pa'></span></li>"
                    
                    }
                    ulWr += "</ul></li>";
                    $(".pa-list").append(ulWr);
                }
            }
        },
        check: function(e) {
            this.inc();
            this.addId(parseInt(e.target.value), e.target.dataset.parentId);
        },
        unCheck: function(e) {
            this.dec();
            this.removeId(parseInt(e.target.value), e.target.dataset.parentId, 0);
        }
    };

    window.popupPa = professionalArea;

    if ($(".popup").length) {
        professionalArea.initializeIds();
        professionalArea.generateProfessionalAreaField();
    }
})