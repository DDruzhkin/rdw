(function( $ ){
    $.fn.loader = function(options) {

        var settings = $.extend({
            loaderClass: 'page_loader'
        }, options);


        this.getLoader = function() {
            var $loader = $('.' + settings.loaderClass);

            if ($loader.length > 0) {
                return $loader;
            }

            return false;
        };

        this.createLoader = function() {

            var $loader = $('body').append('<div class="' + settings.loaderClass + '"></div>');

            $loader.hide();

            return $loader;
        };

        var $loader = this.getLoader() || this.createLoader();

        if (typeof settings.start != 'undefined') {
            $loader.show();
        } else {
            $loader.hide();
        }

    };

    $.fn.loaderStart = function () {
        this.loader({ 'start' : true });
    };

    $.fn.loaderStop = function () {
        this.loader({ 'stop' : true });
    };
})(jQuery);