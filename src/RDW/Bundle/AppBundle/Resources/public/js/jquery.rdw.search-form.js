$(function() {
    $.fn.searchForm = function(options) {
        return this.each(function() {
            var e = $(this),
                advancedBlock = $(this).find('div[class~="center-block"]')
            ;

            e.find('a[data-event="advanced"]').click(function() {
                var advancedLink = $(this),
                    transitionSpeed = 600
                ;

                if (advancedBlock.css('display') === 'none') {
                    advancedBlock.slideDown(transitionSpeed, function() {
                        advancedLink.find('span').removeClass('glyphicon-chevron-down');
                        advancedLink.find('span').addClass('glyphicon-chevron-up');
                    });
                } else {
                    advancedBlock.slideUp(transitionSpeed, function() {
                        advancedLink.find('span').addClass('glyphicon-chevron-down');
                        advancedLink.find('span').removeClass('glyphicon-chevron-up');
                    });
                }
            });
        });
    }

    $('[data-type="search-form"]').searchForm();
});