(function( $ ){
    $.fn.fileUploadHandler = function(options) {

        var settings = $.extend({
            //
        }, options);

        this.fileupload({
            maxChunkSize: 1000000, // bytes; don't do too much or mobile browsers will break
            dataType: 'json',
            done: function (e, data) {
                var $this = $(this),
                    $fileElement = $('#' + $this.data('file-element')),
                    $resultElement = $($this.data('result-element'));

                if (data.result.success) {
                    $fileElement.val(data.result.file_name);

                    if ($this.data('type') == 'image') {
                        $resultElement.html('<img src="' + data.result.file_path + '" />');
                    } else {
                        $resultElement.html(data.result.file_name);
                    }

                    $resultElement.show();

                    $this.parent().removeClass('error');
                    $this.removeClass('error');
                    $this.parent().find('ul').remove();
                    $this.parent().find('span.error_txt').remove();

                } else if ( ! data.result.success && data.result.error) {
                    $fileElement.val('');

                    if ($this.data('type') == 'image') {
                        $resultElement.find('img').attr('src', '');
                        $resultElement.find('img').hide();
                    }

                    $this.parent().addClass('error');
                    $this.addClass('error');

                    if ($this.parent().find('span.error_txt').length > 0) {
                        $this.parent().find('span.error_txt').html(data.result.error + '<br />');
                    } else {
                        $this.after('<span class="no-left-padding error_txt">' + data.result.error + '<br /></span>');
                    }
                }
            }
        });

        if (this.data('filter')) {
            this.bind('fileuploadsubmit', function (e, data) {
                data.formData = {
                    filter: $(this).data('filter')
                };
            });
        }

    };
})(jQuery);

$(document).ready(function() {
    $('[data-handler="photoUploadHandler"]').fileUploadHandler();
    $('[data-handler="fileUploadHandler"]').fileUploadHandler();

    $('[data-handler="activateUploadHandler"]').on('click', function(e) {
        e.preventDefault();

        var $this = $(this);
        $field = $('#' + $this.data('activate-field'));
        $field.trigger('click');
    });


});
