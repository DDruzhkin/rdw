<?php
namespace RDW\Bundle\AppBundle\Validator\Constraints;

use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;

/**
 * Class DateRangeValidator
 * @package RDW\Bundle\AppBundle\Validator\Constraints
 */
class DateRangeValidator
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    public function __construct()
    {
        //$this->translator = $translator;
    }

    /**
     * @param FormEvent $event
     */
    public function validate(FormEvent $event)
    {
        $form = $event->getForm();
        $entity = $form->getData();

        if($entity) {
            $entity->getDateFrom();
            $entity->getDateTo();

            if ($entity->getDateFrom() > $entity->getDateTo()) {
                $form->get('dateFrom')->addError(new FormError('Date from must be before the end date', 'validators', []));
            }
        }
    }

}