<?php
namespace RDW\Bundle\AppBundle\Validator\Constraints;

use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Sum captcha validator
 */
class SumCaptchaValidator
{
    /**
     * @var SessionInterface
     */
    protected $session;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @param SessionInterface    $session
     * @param TranslatorInterface $translator
     */
    public function __construct(SessionInterface $session, TranslatorInterface $translator)
    {
        $this->session = $session;
        $this->translator = $translator;
    }

    /**
     * @param FormEvent $event
     */
    public function validate(FormEvent $event)
    {
        $form = $event->getForm();
        $sum = $form->getData();
        $realSum = $this->getCaptchaSum();

        if ($sum != $realSum) {
            $form->addError(new FormError($this->translator->trans('Please enter valid sum'), 'validators', []));
        }
    }

    /**
     * @return int
     */
    protected function getCaptchaSum()
    {
        return (int) $this->session->get('_sum_captcha');
    }
}