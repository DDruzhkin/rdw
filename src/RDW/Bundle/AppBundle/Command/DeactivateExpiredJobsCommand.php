<?php
namespace RDW\Bundle\AppBundle\Command;

use RDW\Bundle\JobBundle\Entity\Job;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Class TranslationCommand
 *
 * @package RDW\Bundle\TranslationBundle\Command
 */
class DeactivateExpiredJobsCommand extends ContainerAwareCommand
{
    /**
     * Configure
     */
    protected function configure()
    {
        $this
            ->setName('rdw:job:deactivate-expired')
            ->setDescription('Deactivate jobs with expired publishing service');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $stopwatch = new Stopwatch();
        $output->writeln('Started...');
        $stopwatch->start('remove_highlighted');

        $this->deactivate($output);

        $event = $stopwatch->stop('remove_highlighted');
        $output->writeln(sprintf('Executed in %s milliseconds', $event->getDuration()));
        $output->writeln('Finished!');
    }

    /**
     * @param OutputInterface $output
     *
     * @return null
     */
    protected function deactivate(OutputInterface $output)
    {
        $connection = $this->getContainer()->get('doctrine.dbal.default_connection');

        $sql = '
            UPDATE jobs j
            LEFT JOIN order_items oi ON oi.item_id = j.id AND oi.type = \'job\'
            SET j.status = \'inactive\'
            WHERE j.status = \'active\'
	        AND (
                (oi.id IS NULL AND j.valid_till < DATE(NOW()))
               OR
        		(oi.id IS NOT NULL AND oi.valid_till < DATE(NOW()) AND j.valid_till < DATE(NOW()))
        	)
        ';

        $connection->executeQuery($sql);
    }
}
