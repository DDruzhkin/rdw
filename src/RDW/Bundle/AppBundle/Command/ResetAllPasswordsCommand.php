<?php

namespace RDW\Bundle\AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ResetAllPasswordsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('rdw:user:reset-all-passwords')
            ->setDescription('Reset password for all users.');
    }

    /**
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $password = 'Rdw2015BY';
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $userManager = $this->getContainer()->get('rdw_user.service.user_manager');
        $userRepository = $em->getRepository('RDWUserBundle:RegisteredUser');

        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        $users = $userRepository->findBy(['passwordReset' => null, 'lastLogin' => null], [], 1000, 0);

        $count = 1;
        foreach ($users as $user) {
            $user->setPlainPassword($password);
            $user->setPasswordReset(true);
            $userManager->updatePassword($user);

            $output->write('.');

            $em->persist($user);

            if ($count % 50 == 0) {
                $em->flush();
            }

            $count++;
        }

        $em->flush();
    }
}
