<?php

namespace RDW\Bundle\AppBundle\Command;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\OrderBundle\Entity\OrderItem;
use RDW\Bundle\OrderBundle\RDWOrderStatus;
use RDW\Bundle\ServiceBundle\Entity\Service;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class SendJobOfferCommand
 *
 * @package RDW\Bundle\AppBundle\Command
 */
class SendJobOfferCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * Configure
     */
    protected function configure()
    {
        $this
            ->setName('rdw:job:send-offer')
            ->setDescription('Send job offers for employees');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->connection = $this->getContainer()->get('doctrine.dbal.default_connection');

        $startTime = time();
        $jobIds = [];

        list($jobPositions, $jobCities) = $this->getFiltersData();

        $output->writeln(sprintf('<comment>Found positions (%s)</comment>', implode(',', $jobPositions)));
        $output->writeln(sprintf('<comment>Found cities (%s)</comment>', implode(',', $jobCities)));

        foreach ($this->getEmployees($jobPositions, $jobCities) as $employee) {
            list($cvPositions, $cvCities) = $this->constructFilterDataByEmployee($employee, $jobPositions, $jobCities);

            // get all jobs by user cv positions and cities
            $jobs = $this->getJobsMatchingCvs($cvPositions, $cvCities);

            if (empty($jobs)) {
                continue;
            } else {
                $output->writeln(sprintf('<comment>Searching jobs by positions (%s)</comment>', implode(',', $cvPositions)));
                $output->writeln(sprintf('<comment>Searching jobs by cities (%s)</comment>', implode(',', $cvCities)));
            }

            foreach ($jobs as $job) {
                if (!in_array($job->getId(), $jobIds)) {
                    $jobIds[] = $job->getId();
                }
            }

            // send email
            $output->writeln(sprintf('<comment>Sending offer for user %d, matching jobs count %d. Job ids (%s)</comment>', $employee->getId(), sizeof($jobs), implode(',', $jobIds)));
            $this->getContainer()->get('rdw_mailer.service.mailer_manager')->sendJobOffer($employee, $jobs);
        }

        $this->disableOrderItems($output, $jobIds);

        $output->writeln(sprintf('<info>Sending offers took: %d seconds</info>', time() - $startTime));
    }

    /**
     * Get positions and cities of jobs,
     * which owners have active subscription service
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    private function getFiltersData()
    {
        // all job positions and cities which are "paid"
        $sql = "select
                    j.position_id, j.working_place_city_id as city_id
                from users u
                inner join jobs j on (j.user_id = u.id and j.deleted_at is null and j.status = :job_status)
                inner join order_items oi on (oi.item_id = j.id and oi.type = :order_item_type)
                inner join services s on s.id = oi.service_id
                where
                    u.object_type = :user_object_type
                    and u.deleted_at is null
                    and u.type = :user_type
                    and oi.status = :order_item_status
                    and oi.valid_till >= :service_valid_till
                    and s.is_for = :service_is_for
                    and s.type = :service_type
                    and s.is_single = :service_is_single
                    group by j.position_id, j.working_place_city_id
                ";

        $params = [
            'job_status'            => Job::STATUS_ACTIVE,
            'order_item_type'       => OrderItem::TYPE_JOB,
            'user_object_type'      => RegisteredUser::OBJECT_TYPE_REGISTERED,
            'user_type'             => RegisteredUser::USER_TYPE_EMPLOYER,
            'order_item_status'     => RDWOrderStatus::STATUS_PAID,
            'service_is_for'        => Service::FOR_EMPLOYER,
            'service_type'          => Service::TYPE_SEND_EMAIL,
            'service_is_single'     => true,
            'service_valid_till'    => (new \DateTime())->format('Y-m-d H:i:s'),
        ];

        $rawSql = $this->connection->prepare($sql);
        $rawSql->execute($params);
        $results = $rawSql->fetchAll();

        return $this->formatFilterResult($results);
    }

    /**
     * Get all employees, who have cvs with "paid" positions and cities
     *
     * @param array         $positions
     * @param array         $cities
     *
     * @return array
     */
    private function getEmployees(array $positions, array $cities)
    {
        $qb = $this->em->getRepository('RDWUserBundle:RegisteredUser')->createQueryBuilder('user');
        $qb
            ->select('user', 'cv')
            ->join('user.cvs', 'cv')
            ->join('cv.workingPlaceCity', 'city')
            ->andWhere('user.type = :type_employee')
            ->andWhere('cv.deletedAt IS NULL')
            ->andWhere('cv.status IN (:statuses)')
            ->andWhere('cv.position IN (:positions)')
            ->andWhere('city.id IN (:cities)')
            ->setParameter('type_employee', RegisteredUser::USER_TYPE_EMPLOYEE)
            ->setParameter('statuses', [Cv::STATUS_ACTIVE])
            ->setParameter('positions', $positions)
            ->setParameter('cities', $cities);

        return $qb->getQuery()->getResult();
    }

    /**
     * Construct positions and cities from employee cvs,
     * which are matching "paid" positions and cities
     *
     * @param RegisteredUser $employee
     * @param array          $positions
     * @param array          $cities
     *
     * @return array
     */
    private function constructFilterDataByEmployee(RegisteredUser $employee, $positions, $cities)
    {
        // get all positions and cities from user cvs
        $sql = "select cv.position_id, cv.working_place_city_id as city_id
                from cvs cv
                inner join users u on (cv.user_id = u.id and u.deleted_at is null)
                where
                    cv.deletedAt is null
                    and cv.status in (?)
                    and cv.position_id in (?)
                    and cv.working_place_city_id in (?)
                    and u.id = ?
                    ";

        $params = [[Cv::STATUS_ACTIVE], $positions, $cities, $employee->getId()];
        $paramsTypes = [Connection::PARAM_STR_ARRAY, Connection::PARAM_INT_ARRAY, Connection::PARAM_INT_ARRAY, \PDO::PARAM_INT];
        $results = $this->connection->executeQuery($sql, $params, $paramsTypes)->fetchAll();

        return $this->formatFilterResult($results);
    }

    /**
     * Get job offers by user positions and cvs
     *
     * @param array $positions
     * @param array $cities
     *
     * @return array
     */
    private function getJobsMatchingCvs(array $positions, array $cities)
    {
        $qb = $this->em->getRepository('RDWJobBundle:Job')->createQueryBuilder('job');
        $qb
            ->join('job.user', 'user')
            ->join('job.services', 'order_item')
            ->join('order_item.service', 'service')
            ->andWhere('user.deletedAt IS NULL')
            ->andWhere('user INSTANCE OF RDW\Bundle\UserBundle\Entity\RegisteredUser')
            ->andWhere('user.type = :user_type')
            ->andWhere('order_item.status = :status_paid')
            ->andWhere('order_item.validTill >= :valid_till')
            ->andWhere('service.isFor = :is_for')
            ->andWhere('service.type = :service_type')
            ->andWhere('service.isSingle = :service_is_single')
            ->andWhere('job.deletedAt IS NULL')
            ->andWhere('job.status = :job_status_active')
            ->andWhere('job.position IN (:positions)')
            ->andWhere('job.workingPlaceCity IN (:cities)')
            ->setParameter('user_type', RegisteredUser::USER_TYPE_EMPLOYER)
            ->setParameter('status_paid', RDWOrderStatus::STATUS_PAID)
            ->setParameter('valid_till', new \DateTime())
            ->setParameter('is_for', Service::FOR_EMPLOYER)
            ->setParameter('service_type', Service::TYPE_SEND_EMAIL)
            ->setParameter('service_is_single', true)
            ->setParameter('job_status_active', Job::STATUS_ACTIVE)
            ->setParameter('positions', $positions)
            ->setParameter('cities', $cities);

        return $qb->getQuery()->getResult();
    }

    /**
     * Disable order items by jobs offers which were sent to employees
     *
     * @param OutputInterface $output
     * @param array           $jobIds
     */
    private function disableOrderItems(OutputInterface $output, array $jobIds)
    {
        $qb = $this->em->getRepository('RDWOrderBundle:OrderItemJob')->createQueryBuilder('order_item');
        $qb
            ->join('order_item.item', 'job')
            ->join('order_item.service', 'service')
            ->andWhere('service.isFor = :service_is_for')
            ->andWhere('service.type = :service_type')
            ->andWhere('service.isSingle = :service_is_single')
            ->andWhere('order_item.validTill >= :oi_valid_till')
            ->andWhere('order_item.status = :oi_status_paid')
            ->andWhere('job.id IN (:job_ids)')
            ->setParameter('service_is_for', Service::FOR_EMPLOYER)
            ->setParameter('service_type', Service::TYPE_SEND_EMAIL)
            ->setParameter('service_is_single', true)
            ->setParameter('oi_valid_till', new \DateTime())
            ->setParameter('oi_status_paid', RDWOrderStatus::STATUS_PAID)
            ->setParameter('job_ids', $jobIds);

        $orderItemManager = $this->getContainer()->get('rdw_order.service.order_item_manager');

        foreach ($qb->getQuery()->getResult() as $orderItem) {
            $output->writeln(sprintf('<comment>Disabling order item %d</comment>', $orderItem->getId()));
            $orderItem->setValidTill(new \DateTime());

            $orderItemManager->update($orderItem);
        }
    }

    /**
     * @param $results
     *
     * @return array
     */
    private function formatFilterResult($results)
    {
        $positions = [];
        $cities = [];

        foreach ($results as $item) {
            $positions[] = (int) $item['position_id'];
            $cities[] = (int) $item['city_id'];
        }

        $positions = array_unique($positions);
        $cities = array_unique($cities);

        return array($positions, $cities);
    }
}
