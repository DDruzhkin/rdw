<?php

namespace RDW\Bundle\AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

class GenerateSlugsCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('rdw:slugs:re-generate')
            ->setDescription('Re-Generates slugs for Jobs, CVs & Users')
            ->addArgument('type', InputArgument::OPTIONAL, 'Data type', 'job')
            ->addOption('offset', null, InputOption::VALUE_OPTIONAL, '', 0)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '1024M');
        $em = $this->getContainer()->get('doctrine')->getManager();
        $limit = 2000;
        $offset = $input->getOption('offset');

        switch ($input->getArgument('type')) {
            case 'job':
                $type = 'RDWJobBundle:Job';
                break;
            case 'cv':
                $type = 'RDWCvBundle:Cv';
                break;
            case 'user':
                $type = 'RDWUserBundle:RegisteredUser';
                break;
            case 'city':
                $repo = $em->getRepository('RDWLocationBundle:City');
                $template = "UPDATE cities SET title_in_latin = '%s' WHERE id = %d;";
                $sqls = [];
                foreach ($repo->findAll() as $item) {
                    $sqls[] = sprintf($template, $item->generateTitleInLatin(), $item->getId());
                }
                echo sprintf(implode("\r\n", $sqls));
                return;
            default:
                throw new \InvalidArgumentException('Wrong type provided');
                break;
        }

        $query = $em->createQueryBuilder();
        $query
            ->select('j')
            ->from($type, 'j')
            ->where("j.slug IS NOT NULL")
            ->orderBy('j.id', 'asc')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
        ;

        $iteration = 1;
        $sqls = [];
        $template = "UPDATE %ss SET slug = '%s' WHERE id = %d";
        while (count($items = $query->getQuery()->getResult()) > 0) {
            foreach ($items as $item) {
                try {
                    $item->generateSlug();
                    $sqls[] = sprintf($template, $input->getArgument('type'), $item->getSlug(), $item->getId());
                } catch (\Doctrine\ORM\EntityNotFoundException $e) {
                    $output->writeln(sprintf('<error>%d : %s</error>', $item->getId(), $e->getMessage()));
                }
            }

            file_put_contents(sprintf('sqls/%s-%d.sql;', $input->getArgument('type'), $iteration), implode("\r\n", $sqls));

            $offset += $limit;
            $query->setFirstResult($offset);
            $iteration++;
        }
    }
}
