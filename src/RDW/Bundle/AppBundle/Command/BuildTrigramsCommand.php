<?php

namespace RDW\Bundle\AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BuildTrigramsCommand extends ContainerAwareCommand
{
    private $connection;
    private $client;

    protected function configure()
    {
        $this
            ->setName('rdw:trigrams:generate')
            ->setDescription('Generates trigrams')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '1024M');

        $this->connection = $this->getContainer()->get('doctrine.dbal.default_connection');
        $this->client = new \Sphinx\SphinxClient();
        $this->client->Open();

        $this->connection->executeQuery('truncate trigrams;');
        $this->buildPositionTrigrams();
        $this->buildSynonymTrigrams();
    }

    public function buildPositionTrigrams()
    {
        $config = $this->getContainer()->getParameter('rdw_search')['job'];
        $positions = $this->connection->fetchAll('select * from job_positions');

        $data = [];

        foreach ($positions as $position) {
            $data[] = [
                'title' => $position['title'],
                'position_id' => (int) $position['id'],
            ];
        }

        $this->buildTrigrams($config['index'], $data);
    }

    public function buildSynonymTrigrams()
    {
        $synonyms = $this->connection->fetchAll('select * from position_synonyms');

        $data = [];

        foreach ($synonyms as $synonym) {
            $data[] = [
                'title' => $synonym['title'],
                'position_id' => null,
            ];
        }

        $this->buildTrigrams('synonyms', $data);
    }

    private function buildTrigrams($index, array $data)
    {
        foreach ($data as $item) {
            $keywords = $this->client->BuildKeywords($item['title'], $index, true);

            if (!$keywords) {
                continue;
            }

            foreach ($keywords as $keyword) {
                $trigram = $this->connection->fetchAll(sprintf('select * from trigrams where keyword = \'%s\' LIMIT 1', $keyword['tokenized']));

                // trigram found, do nothing
                if (!empty($trigram)) {
                    continue;
                }

                $trigrams = $this->getContainer()->get('auto_correct_service')->buildTrigrams($keyword['tokenized']);

                $this->connection->executeQuery(sprintf(
                    'insert into trigrams (id, position_id, keyword, trigrams, frequency) values (null, %s, \'%s\', \'%s\', %d);',
                    ($item['position_id'] ? $item['position_id'] : 'NULL'), $keyword['tokenized'], $trigrams, $keyword['hits']
                ));
            }
        }
    }
}
