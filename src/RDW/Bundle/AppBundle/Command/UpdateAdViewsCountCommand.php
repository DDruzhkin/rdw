<?php

namespace RDW\Bundle\AppBundle\Command;

use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateAdViewsCountCommand extends ContainerAwareCommand
{
    private $input;
    private $output;
    private $connection;
    private $progress;
    private $query;
    private $service;
    private $maxResults = 999999;

    protected function configure()
    {
        $this
            ->setName('rdw:ad:update-views-count')
            ->setDescription('Update views count for ads');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;
        $this->connection = $this->getContainer()->get('database_connection');
        $this->query = $this->getContainer()->get('widop_google_analytics.query');
        $this->service = $this->getContainer()->get('widop_google_analytics');

        $this->update();
    }

    public function update()
    {
        $this->output->writeln('<info>Gettings statistics</info>');

        $configuration = [
            ['pattern' => '/vakansii/', 'table' => 'jobs'],
            ['pattern' => '/resumes/', 'table' => 'cvs'],
        ];

        foreach ($configuration as $config) {
            $this->query->setStartDate(new \DateTime('yesterday'));
            $this->query->setEndDate(new \DateTime('yesterday'));
            $this->query->setMetrics(['ga:pageviews']);
            $this->query->setDimensions(['ga:pagePath']);
            $this->query->setFilters([sprintf('ga:pagePathLevel1==%s', $config['pattern'])]);
            $this->query->setMaxResults($this->maxResults);

            $response = $this->service->query($this->query);

            $this->output->writeln(sprintf('<comment>Total %s found: %d</comment>', $config['table'], $response->getTotalResults()));

            $updateSql = '';

            $this->output->writeln(sprintf('<comment>Creating update sql for %s...</comment>', $config['table']));

            $progress = $this->getHelper('progress');
            $progress->start($this->output, $response->getTotalResults());

            foreach ($response->getRows() as $row) {
                $uriLevels = explode('/', $row[0]);
                $itemId = (int) end($uriLevels);
                $viewsCount = (int) $row[1];

                $item = $this->getItem($config['table'], $itemId);

                if ($item && $this->needsUpdate($item)) {
                    $updateSql .= sprintf(
                        "UPDATE %s SET views_count_updated_at = '%s', views_count = %d WHERE id = %d;\n",
                        $config['table'], (new \DateTime())->format('Y-m-d H:i:s'), (int) $item['views_count'] + $viewsCount, $itemId
                    );
                }

                $progress->advance();
            }

            $progress->finish();

            $this->output->write(sprintf('<comment>Executing queries for %s... </comment>', $config['table']));
            $this->output->writeln('<info>√</info>');

            if (strlen($updateSql)) {
                $this->connection->executeQuery($updateSql);
            }
        }
    }

    private function getItem($table, $itemId)
    {
        $selectSql = sprintf(
            'SELECT id, views_count, views_count_updated_at FROM %s WHERE id = %d',
            $table, $itemId
        );
        $statement = $this->connection->prepare($selectSql);
        $statement->execute();
        $items = $statement->fetchAll();

        return (!empty($items)) ? $items[0] : null;
    }

    private function needsUpdate(array $item)
    {
        if (null === $item['views_count_updated_at']) {
            return true;
        }

        $updatedAt = new \DateTime($item['views_count_updated_at']);
        $diff = (new \DateTime())->diff($updatedAt);

        return ($diff->d > 0) ? true : false;
    }
}
