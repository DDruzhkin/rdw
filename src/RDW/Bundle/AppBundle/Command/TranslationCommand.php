<?php
namespace RDW\Bundle\AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class TranslationCommand
 *
 * @package RDW\Bundle\TranslationBundle\Command
 */
class TranslationCommand extends ContainerAwareCommand
{
    /**
     * Configure
     */
    protected function configure()
    {
        $this
            ->setName('translation:cache-clear')
            ->setDescription('Clear translations cache');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filesystem = new Filesystem();

        $formatter = $this->getHelper('formatter');
        $kernelCacheDir = $this->getContainer()->getParameter('kernel.cache_dir');
        $translationsCacheDir = $kernelCacheDir . '/translations';

        if ($filesystem->exists($translationsCacheDir)) {
            $filesystem->remove($translationsCacheDir);

            $successMessages = ['Success!', 'Translations cache cleared'];
            $formattedBlock = $formatter->formatBlock($successMessages, 'info');
            $output->writeln($formattedBlock, 'info');
        } else {
            $errorMessages = [
                'Error!',
                sprintf('Translations cache folder: "%s" was not found', $translationsCacheDir)
            ];
            $formattedBlock = $formatter->formatBlock($errorMessages, 'error');
            $output->writeln($formattedBlock, 'error');
        }
    }
}
