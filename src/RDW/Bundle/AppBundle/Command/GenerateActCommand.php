<?php

namespace RDW\Bundle\AppBundle\Command;

use RDW\Bundle\AppBundle\Entity\Act;
use RDW\Bundle\AppBundle\Entity\ActItem;
use RDW\Bundle\OrderBundle\Entity\Order;
use RDW\Bundle\OrderBundle\Entity\OrderItem;
use RDW\Bundle\OrderBundle\RDWOrderStatus;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use RDW\Bundle\AppBundle\Service\ActItemPriceService;

/**
 * Class GenerateActCommand
 *
 * @package RDW\Bundle\AppBundle\Command
 */
class GenerateActCommand extends ContainerAwareCommand
{
    private $em;

    /**
     * Configure
     */
    protected function configure()
    {
        $this
            ->setName('rdw:act:generate')
            ->setDescription('Generate acts')
            ->addArgument('from', InputArgument::OPTIONAL, 'Date from')
            ->addArgument('till', InputArgument::OPTIONAL, 'Date till');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $startTime = time();

        $dateFrom = ($input->getArgument('from'))
            ? new \DateTime($input->getArgument('from'))
            : new \DateTime("first day of last month");

        $dateTo = ($input->getArgument('till'))
            ? new \DateTime($input->getArgument('till'))
            : new \DateTime("last day of last month");

        $vat = $this->getContainer()->getParameter('vat');

        $priceService = new ActItemPriceService($dateFrom, $dateTo);

        $orderItems = $this->getOrderItems($dateFrom, $dateTo);
        $output->writeln(sprintf('<info>Order items in last month found %d</info>', count($orderItems)));

        if (empty($orderItems)) {
            return;
        }

        $map = $this->createActMap($orderItems);
        $output->writeln(sprintf('<comment>Acts should be generated: %d</comment>', count($map)));
        unset($orderItems);

        $actsTotal = 0;
        foreach ($map as $userId => $item) {
            $act = new Act();
            $act->setUser($item['user']);
            $act->setDateFrom($dateFrom);
            $act->setDateTo($dateTo);
            $act->setVat($vat);

            $this->em->persist($act);

            $actSum = 0;
            foreach ($item['order_items'] as $orderItem) {
                $priceService->setOrderItem($orderItem);
                $sum = $priceService->calculateSum();

                if (null === $sum) {
                    $output->writeln(sprintf('<comment>OrderItem %d sum is null. Skiping...</comment>', $orderItem->getId()));
                    continue;
                }

                $actItem = new ActItem();
                $actItem->setAct($act);
                $actItem->setOrderItem($orderItem);
                $actItem->setSum($sum);
                $actItem->setSubject($orderItem->getTitle());

                $act->addItem($actItem);
                $actSum += $actItem->getSum();

                $this->em->persist($actItem);
            }

            $act->setSum($actSum);

            if (0 === $act->getItems()->count()) {
                $this->em->remove($act);
                $output->writeln(sprintf('<comment>Act has no items. User %d. Skiping...</comment>', $userId));
            }

            $actsTotal++;
        }

        $output->writeln(sprintf('<comment>Acts was generated: %d</comment>', $actsTotal));

        $this->em->flush();

        $output->writeln(sprintf('<info>Generating acts took: %d seconds</info>', time() - $startTime));
    }

    private function getOrderItems($dateFrom, $dateTo)
    {
        // all order items used in last month
        $qb = $this->em->getRepository('RDWOrderBundle:OrderItem')->createQueryBuilder('orderItem');
        $qb
            ->innerJoin('orderItem.order', 'orderObject')
            ->innerJoin('orderObject.user', 'user')
            ->andWhere('orderItem.status = :status')
            ->andWhere('orderItem.paidAt <= :date_to')
            ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->gte('orderItem.validTill', ':date_to'),
                    $qb->expr()->andX(
                        $qb->expr()->gte('orderItem.validTill', ':date_from'),
                        $qb->expr()->lte('orderItem.validTill', ':date_to')
                    )
                )
            )
            ->andWhere('orderObject.type != :order_type_system')
            ->setParameter('order_type_system', Order::TYPE_SYSTEM)
            ->setParameter('status', RDWOrderStatus::STATUS_PAID)
            ->setParameter('date_from', $dateFrom)
            ->setParameter('date_to', $dateTo);

        return $qb->getQuery()->getResult();
    }

    private function createActMap(array $orderItems = [])
    {
        $map = [];

        /** @var \RDW\Bundle\OrderBundle\Entity\OrderItem $orderItem */
        foreach ($orderItems as $orderItem) {
            $user = $orderItem->getOrder()->getUser();
            $userId = $user->getId();
            $map[$userId]['order_items'][] = $orderItem;
            $map[$userId]['user'] = $user;
        }

        unset($orderItems);
        unset($orderItem);
        unset($user);

        return $map;
    }
}
