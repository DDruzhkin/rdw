<?php

namespace RDW\Bundle\AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use RDW\Bundle\ServiceBundle\Entity\Service;

class CreateJobPublishingServiceCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('rdw:job:create-publishing-service')
            ->setDescription('Creates publishing service for old system orders')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $startTime = time();

        $connection = $this->getContainer()->get('doctrine.dbal.default_connection');

        $service = $this->getContainer()->get('doctrine.orm.entity_manager')
            ->getRepository('RDWServiceBundle:Service')
            ->findOneBy(['isFor' => Service::FOR_EMPLOYER, 'type' => Service::TYPE_JOB_PUBLISHING]);

        // system orders without job publishing service
        $sql = 'select
                	o.id, o.created_at,
                	(select count(oi1.id) from order_items oi1 where oi1.order_id = o.id and oi1.service_id = 8) as twp
                from orders o
                inner join order_items oi on oi.order_id = o.id and oi.type = \'job\'
                inner join jobs j on j.id = oi.item_id
                where o.type = \'system\'
                group by o.id
                having twp = 0
                order by o.id desc;';

        $rawSql = $connection->prepare($sql);
        $rawSql->execute();
        $orders = $rawSql->fetchAll();

        $insertSql = 'insert into order_items (order_id, service_id, valid_till, price, period, title, status, type, item_id) values ';
        $insertValues = [];

        foreach ($orders as $order) {
            // select job
            $sql = sprintf('select j.id, j.valid_till from jobs j where j.id = (select item_id from order_items oi where oi.order_id = %d limit 1)', $order['id']);
            $rawSql = $connection->prepare($sql);
            $rawSql->execute();
            $job = $rawSql->fetchAll()[0];

            $insertValues[] = sprintf('(%d, %d, \'%s\', 0, DATEDIFF(\'%s\', \'%s\'), \'%s\', \'paid\', \'job\', %d)', $order['id'], $service->getId(), $job['valid_till'], $job['valid_till'], $order['created_at'], $service->getTitle(), $job['id']);
        }

        if (! empty($insertValues)) {
            $sql = $insertSql . implode(',', $insertValues);
            $connection->executeQuery($sql);
        }

        $finishTime = time();

        $output->writeln('<info>Done.</info>');
        $output->writeln(sprintf('<info>Migration took: %s seconds</info>', $finishTime - $startTime));
    }
}
