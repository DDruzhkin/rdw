<?php
namespace RDW\Bundle\AppBundle\Command;

use RDW\Bundle\OrderBundle\RDWOrderStatus;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Class CancelExpiredOrdersCommand
 * @package RDW\Bundle\AppBundle\Command
 */
class CancelExpiredOrdersCommand extends ContainerAwareCommand
{
    /**
     * Configure
     */
    protected function configure()
    {
        $this
            ->setName('rdw:order:cancel-expired')
            ->setDescription('Cancel expired orders');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $stopwatch = new Stopwatch();
        $output->writeln('Started...');
        $stopwatch->start('cancel_expired_orders');

        $this->executeQuery('orders', $output);
        $this->executeQuery('items', $output);

        $event = $stopwatch->stop('cancel_expired_orders');
        $output->writeln(sprintf('Executed in %s milliseconds', $event->getDuration()));
        $output->writeln('Finished!');
    }

    /**
     * @param string          $type
     * @param OutputInterface $output
     *
     * @return null
     */
    protected function executeQuery($type, OutputInterface $output)
    {
        $connection = $this->getContainer()->get('doctrine.dbal.default_connection');

        $query = $connection->executeQuery(
            $this->getQuery($type),
            [
                RDWOrderStatus::STATUS_CANCELLED,
                [RDWOrderStatus::STATUS_PENDING, RDWOrderStatus::STATUS_SUBMITTED],
                $this->getContainer()->getParameter('order.cancel.days'),
                RDWOrderStatus::STATUS_PAID
            ],
            [
                \PDO::PARAM_STR,
                \Doctrine\DBAL\Connection::PARAM_STR_ARRAY,
                \PDO::PARAM_INT,
                \PDO::PARAM_STR,
            ]
        );

        $updatedItems = $query->rowCount();

        $output->writeln(sprintf('Updated %d items', $updatedItems));
    }

    /**
     * @return \Doctrine\DBAL\Driver\Statement
     * @throws \Doctrine\DBAL\DBALException
     */
    private function getQuery($type)
    {
        switch ($type) {
            case 'orders':
                return 'UPDATE orders o
                SET o.status = ?
                WHERE o.status IN (?) AND o.created_at <= DATE_ADD(NOW(), INTERVAL -? DAY)
                AND NOT EXISTS (SELECT 1 FROM order_items i WHERE o.id = i.order_id AND i.status = ?)';
            case 'items':
                return 'UPDATE order_items oi LEFT JOIN orders o ON oi.order_id = o.id
                SET oi.status = ?
                WHERE oi.status IN (?) AND o.created_at <= DATE_ADD(NOW(), INTERVAL -? DAY)
                AND oi.status != ?';
        }
    }
}
