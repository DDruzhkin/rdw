<?php

namespace RDW\Bundle\AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Filesystem\Filesystem;
use RDW\Bundle\JobBundle\Entity\Job;

class JobsExportXmlCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('rdw:export:jobs')
            ->setDescription('Export jobs to XML files')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '256M');
        $startTime = time();

        $container = $this->getContainer();
        $connection = $container->get('doctrine.dbal.default_connection');

        $output->writeln(sprintf('<info>%s</info>', 'Exporting jobs...'));

        $jobs = $connection->fetchAll(sprintf("
            SELECT
                j.id, j.company_title, j.updated_at, j.company_info, j.scope_id, j.salary_to, j.salary_from, j.valid_till,
                j.requirements as `description`, city.title as city, scope.title as scope, pos.title AS `position`, j.employer_type, j.requirements, j.responsibilities, j.working_conditions, et.title as `education`, emt.title as `employment_type`, ws.title as `work_schedule`, j.additional_information, j.slug, j.is_secret
            FROM jobs j
            LEFT JOIN cities city ON city.id = j.working_place_city_id
            LEFT JOIN scopes scope ON scope.id = j.scope_id
            LEFT JOIN job_positions pos ON pos.id = j.position_id
            LEFT JOIN education_types et ON et.id = j.requiredEducationType_id
            LEFT JOIN employment_types emt ON emt.id = j.employment_type_id
            LEFT JOIN work_schedules ws ON ws.id = j.work_schedule_type_id
            INNER JOIN users u ON u.id = j.user_id
            WHERE j.status = '%s' and j.deleted_at is null and u.deleted_at is null and j.allow_export = 1
        ", Job::STATUS_ACTIVE));
 
        $fs = new Filesystem();
        $dir = $container->getParameter('jobs_xml_export_path');

        if (!$fs->exists($dir)) {
            $fs->mkdir($dir);
        }

        foreach (['belmeta', 'jooble'] as $name) {
            $fs->dumpFile(
                sprintf('%s/%s.xml', $dir, $name),
                $container->get('templating')->render(sprintf('RDWAppBundle:JobsExport:%s.xml.twig', $name), ['jobs' => $jobs])
            );
        }

        $output->writeln(sprintf('<info>Done. Export took: %d seconds</info>', time() - $startTime));
    }
}
