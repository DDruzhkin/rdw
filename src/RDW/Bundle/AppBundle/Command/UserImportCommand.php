<?php

namespace RDW\Bundle\AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Ddeboer\DataImport\Reader\CsvReader;

class UserImportCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('rdw:import:users')
            ->setDescription('Imports user from CSV file')
            ->addArgument('filename', InputArgument::REQUIRED, 'Path to CSV file')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!file_exists($input->getArgument('filename'))) {
    		$output->writeln('<error>Wrong path provided. File wasn\'t found.</error>');
    		return;
    	}

        $startTime = time();

		$file = new \SplFileObject($input->getArgument('filename'));
		$reader = new CsvReader($file, '#');

        $importService = $this->getContainer()->get('rdw_user_import_service');
        $importService->import($reader);

        $finishTime = time();

        foreach ($importService->getErrors() as $error) {
            $output->writeln(sprintf('<error>%s</error>', $error));
        }

		$output->writeln(sprintf('<info>Done. Imported %s items.</info>', $importService->getImportedCount()));
        $output->writeln(sprintf('<info>Migration took: %s seconds</info>', $finishTime - $startTime));
    }
}
