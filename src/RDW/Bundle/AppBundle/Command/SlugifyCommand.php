<?php

namespace RDW\Bundle\AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SlugifyCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('rdw:slugify')
            ->setDescription('Generates slugs for static blocks')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $slugifier = $this->getContainer()->get('slug_service');

        $output->writeln('generating scopes');
        $entities = $em->getRepository('RDWUserBundle:Scope')->findAll();
        foreach ($entities as $entity) {
            $slugifier->saveSlug($entity->getId(), get_class($entity), implode(' ', [$entity->getTitle()]));
        }
        $output->writeln('done');

        $output->writeln('generating positions');
        $entities = $em->getRepository('RDWJobBundle:Position')->findAll();
        foreach ($entities as $entity) {
            $slugifier->saveSlug($entity->getId(), get_class($entity), implode(' ', [$entity->getTitle()]));
        }
        $output->writeln('done');

        $output->writeln('generating static filters');
        $entities = $this->getContainer()->get('popular_ad_service')->getJobCategoriesByFilterValues();
        foreach ($entities as $entity) {
            $item = $entity['item'];
            $slugifier->saveSlug($item->getId(), get_class($item), implode(' ', [$entity['title']]));
        }
        $output->writeln('done');
    }
}
