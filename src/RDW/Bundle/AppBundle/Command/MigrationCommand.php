<?php

namespace RDW\Bundle\AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use RDW\Bundle\UserBundle\Entity\RegisteredUser as User;

class MigrationCommand extends ContainerAwareCommand
{
    private $connection;
    private $output;

    protected function configure()
    {
        $this
            ->setName('rdw:migrate')
            ->setDescription('Migrates data from old version');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '256M');
        $startTime = time();

        $this->connection = $this->getContainer()->get('doctrine.dbal.default_connection');
        $this->output = $output;

        $this->output->writeln('<error>Migrations done. Don\'t migrate again.</error>');
        return;

        $this->connection->executeQuery('SET foreign_key_checks = 0');

        $defaultCompanyType = 16;
        $defaultPosition = 2388;

        $config = [
            [
                'title' => 'Migrating CONFIGS',
                'queries' => [
                    '-> Cities:' => 'insert into rdwby.cities (id, title, position) (select id, name, id from rdw_by_old.fo_cities)',
                    '-> Work Experiences:' => 'insert into rdwby.work_experience_types (id, title, position) (select id, name, id from rdw_by_old.fo_experience where id <> 6)', // id:6 - net_dannyih
                    '-> Scopes:' => 'insert into rdwby.scopes (id, title, position) (select id, name, id from rdw_by_old.fo_activities)',
                    '-> Education Types:' => 'insert into rdwby.education_types (id, title, position) (select id, name, sort from rdw_by_old.fo_edu)',
                    '-> Updating positions of education types:' => 'update education_types set position = case when (position = 0) then 1 when (position = 1) then 2 when (position = 2) then 3 when (position = 3) then 4 when (position = 4) then 5 when (position = 8) then 6 end',
                    '-> Texts' => 'insert into rdwby.texts (title, short_title, content, type, created_at, status) (select title, name, content, \'text\', NOW(), \'active\' from rdw_by_old.fo_page)',
                    '-> Work schedules' => 'insert into rdwby.work_schedules (id, title, position) (select id, name, id from rdw_by_old.fo_worktime)',
                    '-> Job positions' => file_get_contents('migrate/job_positions.sql'),
                    '-> Services' => file_get_contents('migrate/services.sql'),
                    '-> Company types' => file_get_contents('migrate/company_types.sql'),
                    '-> Languages' => file_get_contents('migrate/languages.sql'),
                    '-> Language levels' => file_get_contents('migrate/language_levels.sql'),
                    '-> Employment types' => file_get_contents('migrate/employment_types.sql'),
                    '-> Texts' => file_get_contents('migrate/texts.sql'),
                    '-> Business trips' => file_get_contents('migrate/business_trips.sql'),
                    '-> Driving license categories' => file_get_contents('migrate/driving_license_categories.sql'),
                    '-> Marital statuses' => file_get_contents('migrate/marital_statuses.sql'),
                ],
            ],
            [
                'title' => 'Migrating USERS',
                'queries' => [
                    '-> Base' => 'insert ignore into dev_rdw.users (id, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, password, object_type, type, salt, roles, deleted) (select id, email, email, email, email, confirmed, password, \'registered\', \'employee\', SHA1(UUID()), \'a:1:{i:0;s:13:"ROLE_EMPLOYEE";}\', 0 from rdw_by_old.fo_members)',
                    '-> Employers' => 'update rdwby.users u inner join rdw_by_old.fo_companies c on c.fk_member = u.id set u.type = \'employer\', u.roles = \'a:1:{i:0;s:13:"ROLE_EMPLOYER";}\', u.company_title = c.name, u.contact_person = c.contact, u.contact_person_phone = c.phone, u.url_address = c.website, u.company_info = c.info;',
                    '-> Employees' => 'update rdwby.users u inner join rdw_by_old.fo_registrants r on r.fk_member = u.id set u.name = r.fname, u.surname = r.lname, u.gender = IF(r.sex <> 0, \'f\', \'m\'), u.birthday = IF (r.dob <> \'0000-00-00\', r.dob, null)',
                    '-> Anonymous' => 'insert ignore into rdwby.users (username, username_canonical, email, email_canonical, enabled, salt, password, roles, object_type, locked, expired, credentials_expired, is_unlimited) (select sha1(email), sha1(email), sha1(email), sha1(email), 0, SHA1(UUID()), SHA1(CONCAT(CURRENT_TIME, email)), \'a:1:{i:0;s:14:"ROLE_ANONYMOUS";}\', \'anonymous\', 0, 0, 0, 0 from rdw_by_old.fo_scribe_res fsr where email NOT IN (select email_canonical from rdwby.users))',
                    '-> Anonymous2' => 'insert ignore into rdwby.users (username, username_canonical, email, email_canonical, enabled, salt, password, roles, object_type) (select sha1(email), sha1(email), sha1(email), sha1(email), 0, SHA1(UUID()), SHA1(CONCAT(CURRENT_TIME, email)), \'a:1:{i:0;s:14:"ROLE_ANONYMOUS";}\', \'anonymous\' from rdw_by_old.fo_scribe_vac where email NOT IN (select email_canonical from rdwby.users))',
                    '-> Administrators' => 'insert into rdwby.administrators (username, email, password, type, name, status, roles, created_at, phone) (select email, email, sha1(password), \'admin\', \'name\', \'active\', \'a:1:{i:0;s:10:"ROLE_ADMIN";}\', NOW(), \'80000\' from rdw_by_old.fo_user)',
                    '-> Adding new administrator (gediminas@idea.lt : ideaidea)' => 'INSERT INTO `administrators` (`username`, `password`, `name`, `surname`, `email`, `type`, `status`, `roles`, `created_at`, `updated_at`, `phone`) VALUES (\'gediminas@idea.lt\', \'HRZhhyhfbFf3qv+81KoFY5n862PuIaI3keUK0tfdeCG76UmCLmch8mkz9oL94DB+hp/h5/r+1McgAcYK2NRDHQ==\', \'Gediminas\', \'Molis\', \'gediminas@idea.lt\', \'admin\', \'active\', \'a:1:{i:0;s:10:\"ROLE_ADMIN\";}\', \'2015-08-11 10:29:58\', \'2015-08-11 13:35:38\', \'800000\');',
                ],
            ],
            [
                'title' => 'Migrating JOBS',
                'queries' => [
                    '-> Base' => 'insert into rdwby.jobs (id, user_id, employer_type, employment_type_id, working_place_city_id, salary_to, requirements, company_title, contact_person, contact_person_email, contact_person_phone, contact_person_city_id, created_at, is_sent_for_censor, updated_at, status, is_secret, is_vip, show_in_carousel, scope_id, work_schedule_type_id, workExperienceType_id, requiredEducationType_id, gender, valid_till) (select id, fk_member, 1, 1, IF (town > 0, town, null), IF (salary_currency = 1, IF (salary > 0, salary, null), null), description, company_name, contact, email, phone, IF (town > 0, town, null), creation_date, 0, creation_date, IF(status = 0 AND active = 1, \'active\', \'inactive\'), 0, 0, 0, sphere, IF(schedule > 0, schedule, NULL), IF(experience > 0 and experience <> 6, experience, NULL), IF(education > 0, education, NULL), IF(sex = 2,  null, IF(sex = 1, \'female\', \'male\')), \''.(new \DateTime('+1month'))->format('Y-m-d').'\' from rdw_by_old.fo_vacancies3)',
                    '-> Assigning cities' => 'insert into rdwby.job_city (job_id, city_id) (select id, town from rdw_by_old.fo_vacancies3)',
                    '-> Assigning positions' => 'update rdwby.jobs j inner join rdw_by_old.fo_vacancies3 old_j on old_j.id = j.id set j.position_id = (select jp.id from rdwby.job_positions jp where jp.title = old_j.name limit 1);',
                    '-> Setting default position for not mapped positions' => 'update rdwby.jobs set position_id = '.$defaultPosition.' where position_id is null',
                    '-> Assigning company types' => 'update rdwby.jobs j inner join rdw_by_old.fo_vacancies3 old_j on old_j.id = j.id set j.company_type_id = (select id from rdwby.company_types ct where ct.title = old_j.company_name limit 1)',
                    '-> Assigning company types to Users' => 'update rdwby.users u set u.company_type_id = (select company_type_id from rdwby.jobs j where j.user_id = u.id limit 1)',
                    '-> Setting default company types for Jobs' => 'update rdwby.jobs set company_type_id = '.$defaultCompanyType.' where company_type_id is null',
                    '-> Setting default company types for Users' => 'update rdwby.users set company_type_id = '.$defaultCompanyType.' where company_type_id is null',
                ],
            ],
            [
                'title' => 'Migrating CVS',
                'queries' => [
                    '-> Allow position_id null' => 'ALTER TABLE `cvs` CHANGE `position_id` `position_id` INT(11) DEFAULT NULL;',
                    '-> Base' => 'insert into rdwby.cvs (id, user_id, name, surname, birthday, phone, email, salary_to, other_information, created_at, updated_at, gender, valid_till, scope_id, work_schedule_type_id, status, employment_type_id, working_place_city_id) (select id, fk_member, fname, lname, IF (dob <> \'0000-00-00\', dob, null), phone, email, IF (salary_currency = 1, IF (salary > 0, salary, null), null), navyki, creation_date, creation_date, IF(sex <> 0, \'f\', \'m\'), \''.(new \DateTime('+12months'))->format('Y-m-d').'\', sphere, IF(schedule > 0, schedule, NULL), IF (status = 0, IF (hide_phone = 1 or hide_email = 1, \'limited\', \'active\'), \'inactive\'), 1, IF (town > 0, town, null) from rdw_by_old.fo_resumes2)',
                    '-> Educations' => 'insert into rdwby.cv_educations (cv_id, education_type_id, discr) (select id, education, \'education\' from rdw_by_old.fo_resumes2)',
                    '-> Assigning cities' => 'insert into rdwby.cv_city (cv_id, city_id) (select id, town from rdw_by_old.fo_resumes2)',
                    '-> Assigning positions' => 'update rdwby.cvs cv inner join rdw_by_old.fo_resumes2 old_cv on old_cv.id = cv.id set cv.position_id = (select jp.id from rdwby.job_positions jp where jp.title = old_cv.name limit 1);',
                    '-> Setting default position for not mapped positions' => 'update rdwby.cvs set position_id = '.$defaultPosition.' where position_id is null or position_id = 0',
                ],
            ],
            [
                'title' => 'Migrating SUBSCRIPTIONS',
                'queries' => [
                    '-> CVs' => 'insert into rdwby.subscriptions (email, type, status, token, frequency) (select email, \'cv\', \'active\', SHA1(CONCAT(CURRENT_TIME, email)), 7 from rdw_by_old.fo_scribe_res)',
                    '-> Jobs' => 'insert into rdwby.subscriptions (email, type, status, token, frequency) (select email, \'job\', \'active\', SHA1(CONCAT(CURRENT_TIME, email)), 7 from rdw_by_old.fo_scribe_vac)',
                    '-> Assigning to Anonymous users' => 'update rdwby.subscriptions s set s.user_id = (select u.id from rdwby.users u where u.email_canonical = sha1(s.email))',
                 ],
            ],
            [
                'title' => 'Post migration data fixes',
                'queries' => [
                    '-> Job.scope should be null' => 'update rdwby.jobs set scope_id = null where scope_id = 0',
                    '-> CV.scope should be null' => 'update rdwby.cvs set scope_id = null where scope_id = 0',
                    '-> Job.user should be null' => 'update rdwby.jobs set user_id = null where user_id = 0',
                 ],
            ],
        ];

        foreach ($config as $item) {
            $this->output->writeln(sprintf('<info>%s</info>', $item['title']));
            foreach ($item['queries'] as $message => $sql) {
                $this->output->write(sprintf('<comment>%s</comment>', (string) $message));
                $this->connection->executeQuery((string) $sql);
                $this->output->write('<info> ✓</info>');
                $this->output->writeln('');
            }
        }

        $this->output->writeln(sprintf('<info>%s</info>', 'Migrating ORDERS'));

        $this->output->write(sprintf('<comment>%s</comment>', '-> Show vacancies in top'));
        $vipJobs = $this->connection->fetchAll('select job_old.* from rdw_by_old.fo_vacancies3 job_old where job_old.top_to >= NOW() and fk_member is not null and fk_member > 0;');
        $showInTopService = $this->connection->fetchAll('select * from services where id = 10')[0];
        $highlightService = $this->connection->fetchAll('select * from services where id = 4')[0];

        foreach ($vipJobs as $job) {
            $this->connection->executeQuery(
                'insert into rdwby.orders (id, user_id, payment_id, created_at, paid_at, updated_at, status, type, currency, vat, total, payment_type) values (null, '.$job['fk_member'].', null, NOW(), NOW(), NOW(), \'paid\', \'calculator\', \'BYR\', 20, '.$showInTopService['price'].', \'transfer\');'
            );
            $this->connection->executeQuery(
                'insert into rdwby.order_items (id, order_id, service_id, valid_till, price, period, title, status, type, item_id, paid_at) values (null, LAST_INSERT_ID(), '.$showInTopService['id'].', \''.$job['top_to'].'\', '.$showInTopService['price'].', CEIL(DATEDIFF(\''.$job['top_to'].'\', CURDATE()) / 7), \''.$showInTopService['title'].'\', \'paid\', \'job\', \''.$job['id'].'\', \''.$job['top_from'].'\');'
            );

            // if job vip=1 add highlight service fot it
            if (1 === (int) $job['vip']) {
                $this->connection->executeQuery(
                    'insert into rdwby.order_items (id, order_id, service_id, valid_till, price, period, title, status, type, item_id) values (null, LAST_INSERT_ID(), '.$highlightService['id'].', \''.$job['top_to'].'\', '.$highlightService['price'].', CEIL(DATEDIFF(\''.$job['top_to'].'\', CURDATE()) / 7), \''.$highlightService['title'].'\', \'paid\', \'job\', '.$job['id'].');'
                );
                $this->connection->executeQuery('update rdwby.jobs set is_highlighted = 1 where id = '.$job['id']);
            }
        }

        $this->output->write('<info> ✓</info>');
        $this->output->writeln('');

        $this->output->write(sprintf('<comment>%s</comment>', '-> Disallow position_id null'));
        $this->connection->executeQuery('ALTER TABLE `cvs` CHANGE `position_id` `position_id` INT(11) NOT NULL;');
        $this->output->write('<info> ✓</info>');
        $this->output->writeln('');

        $this->connection->executeQuery('SET foreign_key_checks = 1');

        $this->migrateFiles();

        $this->output->writeln(sprintf('<info>Migration took: %d seconds</info>', time() - $startTime));
    }

    private function migrateFiles()
    {
        $this->output->writeln(sprintf('<info>%s</info>', 'Migrating static content'));
        $this->output->write(sprintf('<comment>%s</comment>', '-> CVS photos and documents'));

        $oldDocumentPath = $this->getContainer()->getParameter('kernel.root_dir').'/../../rdw_old/files/resumes/';
        $newPath = $this->getContainer()->getParameter('kernel.root_dir').'/../web/uploads/';

        $cvs = $this->connection->fetchAll('select id, IF(LENGTH(TRIM(REPLACE(file, \'\t\', \'\'))) > 0, TRIM(REPLACE(file, \'\t\', \'\')), NULL) as document, IF(LENGTH(TRIM(REPLACE(photo, \'\t\', \'\'))) > 0, TRIM(REPLACE(photo, \'\t\', \'\')), NULL) as photo from rdw_by_old.fo_resumes2 having (document or photo) IS NOT NULL');

        foreach ($cvs as $cv) {
            if ((!empty($cv['document']) && file_exists($oldDocumentPath.$cv['document']))) {
                $newName = $this->getNewName($cv['document']);

                if (rename($oldDocumentPath.$cv['document'], $newPath.$newName)) {
                    $this->connection->executeQuery(sprintf('update rdwby.cvs set document=\'%s\' where id=%d', $newName, $cv['id']));
                }
            }
        }

        $this->output->write('<info> ✓</info>');
        $this->output->writeln('');
    }

    /**
     * @param string $name
     *
     * @return string
     */
    private function getNewName($name)
    {
        $explodedName = explode('.', $name);

        return sprintf('%s.%s', uniqid(), end($explodedName));
    }
}
