<?php

namespace RDW\Bundle\AppBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use RDW\Bundle\JobBundle\Event\ApplyEvent;
use RDW\Bundle\JobBundle\RDWJobEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Http\SecurityEvents;
use RDW\Bundle\JobBundle\Service\JobApplyManager;
use RDW\Bundle\JobBundle\Entity\Apply;

/**
 * Class JobApplyCreateSubscriber
 * @package RDW\Bundle\AppBundle\EventListener
 */
class JobApplyCreateSubscriber extends AbstractCreateSubscriber
{
    /**
     * @var JobApplyManager
     */
    protected $jobApplyManager;

    /**
     * JobApplyCreateSubscriber constructor.
     * @param Session $session
     * @param JobApplyManager $jobApplyManager
     * @param EventDispatcherInterface $eventDispatcher
     * @param $sessionKey
     */
    public function __construct(
        Session $session,
        JobApplyManager $jobApplyManager,
        EventDispatcherInterface $eventDispatcher,
        $sessionKey
    )
    {
        parent::__construct($session, $eventDispatcher, $sessionKey);

        $this->jobApplyManager = $jobApplyManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            'rdw_app.job_apply.pre_create' => 'onPreCreate',
            SecurityEvents::INTERACTIVE_LOGIN => 'onSecurityInteractiveLogin',
            FOSUserEvents::SECURITY_IMPLICIT_LOGIN => 'onImplicitLogin',
            FOSUserEvents::REGISTRATION_CONFIRM => 'onRegistrationConfirm',
        );
    }

    /**
     * @param UserInterface $user
     */
    public function create($user)
    {
        $apply = $this->session->get($this->sessionKey);

        if ($user instanceof UserInterface && $apply instanceof Apply) {
            $apply = $this->jobApplyManager->merge($apply);
            $apply->setUser($user);

            $this->jobApplyManager->update($apply);

            $this->session->remove($this->sessionKey);
            $this->session->getFlashBag()->add('success', 'Your apply for job was sent successfully');

            $event = new ApplyEvent($apply);
            $this->eventDispatcher->dispatch(RDWJobEvents::APPLY_SUCCESS, $event);
        }
    }
}
