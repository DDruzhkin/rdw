<?php

namespace RDW\Bundle\AppBundle\EventListener;

use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;
use RDW\Bundle\AppBundle\Service\AdManager;
use RDW\Bundle\AppBundle\Entity\AdInterface;
use RDW\Bundle\CvBundle\Event\CvEvent;
use RDW\Bundle\CvBundle\RDWCvEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Routing\RouterInterface;
use RDW\Bundle\JobBundle\Event\JobEvent;
use RDW\Bundle\JobBundle\RDWJobEvents;
use Symfony\Component\Translation\TranslatorInterface;

class AdCreateSubscriber extends AbstractCreateSubscriber
{
    /** @var TranslatorInterface */
    protected $translator;

    /**
     * @var AdManager
     */
    private $adManager;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * AdCreateSubscriber constructor.
     *
     * @param Session $session
     * @param AdManager $adManager
     * @param EventDispatcherInterface $dispatcher
     * @param RouterInterface $router
     * @param $sessionKey
     */
    public function __construct(
        Session $session,
        AdManager $adManager,
        EventDispatcherInterface $dispatcher,
        RouterInterface $router,
        $sessionKey,
        TranslatorInterface $translator
    ) {
        parent::__construct($session, $dispatcher, $sessionKey);

        $this->adManager = $adManager;
        $this->router = $router;
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            'rdw_app.ad.pre_create' => 'onPreCreate',
            SecurityEvents::INTERACTIVE_LOGIN => 'onSecurityInteractiveLogin',
            FOSUserEvents::SECURITY_IMPLICIT_LOGIN => 'onImplicitLogin',
            FOSUserEvents::REGISTRATION_CONFIRM => 'onRegistrationConfirm',
        );
    }

    /**
     * @param InteractiveLoginEvent $event
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $url = $this->create($event->getAuthenticationToken()->getUser());
        $url = $url ? $url : $event->getRequest()->request->get('_target_path');

        $event->getRequest()->request->set('_target_path', $url);
    }

    /**
     * @param UserEvent $event
     */
    public function onImplicitLogin(UserEvent $event)
    {
        $url = $this->create($event->getUser());
        $url = $url ? $url : $event->getRequest()->request->get('_target_path');

        $event->getRequest()->request->set('_target_path', $url);
    }

    /**
     * @param UserInterface $user
     */
    public function create($user)
    {
        $ad = $this->session->get($this->sessionKey);
        $url = null;

        if ($user instanceof UserInterface && $ad instanceof AdInterface) {
            $ad = $this->adManager->map($this->session);
            $ad->setUser($user);

            if ($ad->isCv()) {
                $adEvent = new CvEvent($ad);
                $this->eventDispatcher->dispatch(RDWCvEvents::CV_PUBLISH, $adEvent);
                $this->session->getFlashBag()->add('success', $this->translator->trans('Your cv was created successfully'));

                $url = $this->router->generate('rdw_cv.cv.edit', ['id' => $ad->getId()], true);
            } elseif ($ad->isJob()) {
                $adEvent = new JobEvent($ad);
                $this->eventDispatcher->dispatch(RDWJobEvents::JOB_SAVE, $adEvent);
                $this->session->getFlashBag()->add('success', $this->translator->trans('Your job was created successfully'));

                $url = $this->router->generate('rdw_job.job.edit', ['id' => $ad->getId()], true);
            } else {
                throw new \RuntimeException(sprintf('Wrong Ad instance: "%s"', get_class($ad)));
            }

            $this->session->remove($this->sessionKey);
        }

        return $url;
    }
}
