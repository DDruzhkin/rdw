<?php

namespace RDW\Bundle\AppBundle\EventListener;

use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\FOSUserEvents;
use RDW\Bundle\AppBundle\Event\GenericGuestEvent;
use RDW\Bundle\CvBundle\Entity\CvMessage;
use RDW\Bundle\CvBundle\Event\CvMessageEvent;
use RDW\Bundle\CvBundle\RDWCvEvents;
use RDW\Bundle\CvBundle\Service\CvMessageManager;
use RDW\Bundle\JobBundle\Event\ApplyEvent;
use RDW\Bundle\JobBundle\RDWJobEvents;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use RDW\Bundle\UserBundle\Entity\UserInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;
use RDW\Bundle\JobBundle\Entity\Apply;

/**
 * Class CvMessageCreateSubscriber
 * @package RDW\Bundle\AppBundle\EventListener
 */
class CvMessageCreateSubscriber implements EventSubscriberInterface
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var CvMessageManager
     */
    private $cvMessageManager;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    /**
     * CvSendMessageSubscriber constructor.
     * @param Session $session
     * @param CvMessageManager $cvMessageManager
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        Session $session,
        CvMessageManager $cvMessageManager,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->session = $session;
        $this->cvMessageManager = $cvMessageManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            'rdw_app.cv_message.pre_create' => 'onPreCreate',
            SecurityEvents::INTERACTIVE_LOGIN => 'onSecurityInteractiveLogin',
            FOSUserEvents::SECURITY_IMPLICIT_LOGIN => 'onImplicitLogin',
            FOSUserEvents::REGISTRATION_CONFIRM => 'onRegistrationConfirm',
        );
    }

    /**
     * @param GenericEvent $event
     */
    public function onPreCreate(GenericGuestEvent $event)
    {
        $cvMessage = $event->getSubject();
        $request = $event->getRequest();

        $this->session->set('cv_message', $cvMessage);
        $this->session->set('referer', $request->headers->get('referer'));
    }

    /**
     * @param InteractiveLoginEvent $event
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $this->createCvMessage($event->getAuthenticationToken()->getUser());
    }

    /**
     * @param UserEvent $event
     */
    public function onImplicitLogin(UserEvent $event)
    {
        $this->createCvMessage($event->getUser());
    }

    /**
     * @param GetResponseUserEvent $event
     */
    public function onRegistrationConfirm(GetResponseUserEvent $event)
    {
        if ($referer = $this->session->get('referer')) {
            $event->setResponse(new RedirectResponse($referer));
        }
    }

    /**
     * @param UserInterface $user
     */
    private function createCvMessage($user)
    {
        $cvMessage = $this->session->get('cv_message');

        if ($user instanceof UserInterface && $cvMessage instanceof CvMessage) {
            $cvMessage = $this->cvMessageManager->merge($cvMessage);
            $cvMessage->setUser($user);

            $this->cvMessageManager->update($cvMessage);

            $this->session->remove('cv_message');

            $event = new CvMessageEvent($cvMessage, $user);
            $this->eventDispatcher->dispatch(RDWCvEvents::CV_MESSAGE_SAVE, $event);
        }
    }
}
