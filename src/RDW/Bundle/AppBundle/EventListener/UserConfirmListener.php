<?php

namespace RDW\Bundle\AppBundle\EventListener;

use RDW\Bundle\MailerBundle\Service\MailerManager;
use RDW\Bundle\UserBundle\Service\UserManager;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;

/**
 * Class UserConfirmListener
 *
 * @package RDW\Bundle\AppBundle\EventListener
 */
class UserConfirmListener
{
    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var MailerManager
     */
    private $mailerManager;

    /**
     * @param UserManager   $userManager
     * @param MailerManager $mailerManager
     */
    public function __construct(UserManager $userManager, MailerManager $mailerManager)
    {
        $this->userManager = $userManager;
        $this->mailerManager = $mailerManager;
    }

    /**
     * @param FilterUserResponseEvent $event
     *
     * @throws \Symfony\Component\Validator\Exception\UnexpectedTypeException
     */
    public function onUserConfirm(FilterUserResponseEvent $event)
    {
        $user = $event->getUser();

        if ( ! $user instanceof RegisteredUser) {
            throw new UnexpectedTypeException($user, 'RDW\Bundle\UserBundle\Entity\RegisteredUser');
        }

        // set manager only if user has no manager
        if ($user->isTypeEmployee() || $user->getManager() instanceof RegisteredUser) {
            return;
        }

        $manager = $this->userManager->getManagerForSupportingNewUsers();

        if (! $manager instanceof RegisteredUser) {
            return;
        }

        $user->setManager($manager);
        $user->setHasManagerFrom(new \DateTime());

        $this->userManager->updateUser($user);

        // send email for manager
        $this->mailerManager->sendEmailForManagerAboutNewUser($user);

        // send email for user
        $this->mailerManager->sendEmailForUserAboutAssignedManager($user);

        return true;
    }
}
