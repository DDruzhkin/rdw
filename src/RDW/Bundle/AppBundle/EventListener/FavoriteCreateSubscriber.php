<?php

namespace RDW\Bundle\AppBundle\EventListener;

use RDW\Bundle\AppBundle\Event\GenericGuestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use RDW\Bundle\UserBundle\Entity\UserInterface;
use RDW\Bundle\UserBundle\Service\FavoriteManager;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;
use RDW\Bundle\UserBundle\Entity\Favorite;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Translation\TranslatorInterface;

class FavoriteCreateSubscriber implements EventSubscriberInterface
{
    /** @var Session */
    private $session;

    /** @var FavoriteManager */
    private $favoriteManager;

    /** @var TranslatorInterface */
    private $translator;

    public function __construct(Session $session, FavoriteManager $favoriteManager, TranslatorInterface $translator)
    {
        $this->session = $session;
        $this->favoriteManager = $favoriteManager;
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            'rdw_app.favorite.pre_create' => 'onPreCreate',
            SecurityEvents::INTERACTIVE_LOGIN => 'onSecurityInteractiveLogin',
            FOSUserEvents::SECURITY_IMPLICIT_LOGIN => 'onImplicitLogin',
            FOSUserEvents::REGISTRATION_CONFIRM => 'onRegistrationConfirm',
        ];
    }

    /**
     * @param GenericGuestEvent $event
     */
    public function onPreCreate(GenericGuestEvent $event)
    {
        $favorite = $event->getSubject();
        $request = $event->getRequest();

        $this->session->set('favorite', $favorite);
        $this->session->set('referer', $request->headers->get('referer'));
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();
        $favorite = $this->session->get('favorite');

        if ($user instanceof UserInterface && $favorite instanceof Favorite) {
            $favorite = $this->favoriteManager->merge($favorite);

            $this->favoriteManager->add($favorite->getAd(), $user);

            $this->setFlashMessage($favorite);
            $this->session->remove('favorite');
        }
    }

    public function onImplicitLogin(UserEvent $event)
    {
        $user = $event->getUser();
        $favorite = $this->session->get('favorite');

        if ($user instanceof UserInterface && $favorite instanceof Favorite) {
            $favorite = $this->favoriteManager->merge($favorite);

            $this->favoriteManager->add($favorite->getAd(), $user);

            $this->setFlashMessage($favorite);
            $this->session->remove('favorite');
        }
    }

    public function onRegistrationConfirm(GetResponseUserEvent $event)
    {
        if ($referer = $this->session->get('referer')) {
            $event->setResponse(new RedirectResponse($referer));
        }
    }

    private function setFlashMessage(Favorite $favorite)
    {
        $message = ($favorite->getAd()->isJob())
            ? $this->translator->trans('Job was added to your dashboard')
            : $this->translator->trans('Cv was added to your dashboard');

        $this->session->getFlashBag()->add('success', $message);
    }
}
