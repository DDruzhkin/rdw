<?php

namespace RDW\Bundle\AppBundle\EventListener;

use RDW\Bundle\MenuBundle\Entity\Menu;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Presta\SitemapBundle\Service\SitemapListenerInterface;
use Presta\SitemapBundle\Event\SitemapPopulateEvent;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;
use Symfony\Component\DependencyInjection\ContainerAware;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class SitemapListener
 *
 * @package RDW\Bundle\AppBundle\EventListener
 */
class SitemapListener extends ContainerAware implements SitemapListenerInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param SitemapPopulateEvent $event
     */
    public function populateSitemap(SitemapPopulateEvent $event)
    {
        $container = $this->container;
        $em = $container->get('doctrine');

        $data = [
            'pages' => [
                'items' => new ArrayCollection($em->getRepository('RDWMenuBundle:Menu')->findAllActive()),
            ],
        ];

        foreach ($data as $type => $row) {
            $row['items']->map(function ($entity) use ($type, $event, $container) {
                    if ($entity->isModal()) {
                        return false;
                    }

                    $event->getGenerator()->addUrl(
                        new UrlConcrete(
                            $this->getUrl($entity),
                            new \DateTime(),
                            UrlConcrete::CHANGEFREQ_DAILY,
                            1
                        ),
                        $type
                    );
                });

        }
    }

    /**
     * @param Menu $item
     *
     * @return string
     */
    public function getUrl(Menu $item)
    {
        if (! $item->isModal()) {
            return $this->container->get('rdw_menu.service.menu_manager')->getUrl($item, true);
        } else {
            return '#';
        }
    }
}
