<?php

namespace RDW\Bundle\AppBundle\EventListener;

use RDW\Bundle\AppBundle\Event\AdAbuseEvent;
use RDW\Bundle\AppBundle\Entity\Abuse;
use RDW\Bundle\MailerBundle\Service\MailerManager;
use RDW\Bundle\ManageBundle\Service\UserManager;
use RDW\Bundle\HistoryBundle\Service\HistoryManager;
use Braincrafted\Bundle\BootstrapBundle\Session\FlashMessage;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use RDW\Bundle\HistoryBundle\Entity\History;
use RDW\Bundle\HistoryBundle\RDWHistoryEvents;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class AbuseListener
 * @package RDW\Bundle\AppBundle\EventListener
 */
class AdAbuseListener
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var FlashMessage
     */
    private $flashMessage;

    /**
     * @var MailerManager
     */
    private $mailerManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var HistoryManager
     */
    private $historyManager;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param ObjectManager $objectManager
     * @param FlashMessage $flashMessage
     * @param MailerManager $mailerManager
     * @param UserManager $userManager
     * @param EventDispatcherInterface $eventDispatcher
     * @param HistoryManager $historyManager
     * @param TranslatorInterface $translator
     */
    public function __construct(
        ObjectManager $objectManager,
        FlashMessage $flashMessage,
        MailerManager $mailerManager,
        UserManager $userManager,
        EventDispatcherInterface $eventDispatcher,
        HistoryManager $historyManager,
        TranslatorInterface $translator
    ) {
        $this->objectManager = $objectManager;
        $this->flashMessage = $flashMessage;
        $this->mailerManager = $mailerManager;
        $this->userManager = $userManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->historyManager = $historyManager;
        $this->translator = $translator;
    }

    /**
     * @param AdAbuseEvent $event
     *
     * @throws \Symfony\Component\Validator\Exception\UnexpectedTypeException
     */
    public function onAbuse(AdAbuseEvent $event)
    {
        $abuse = $event->getAbuse();

        $abuse->setAd($event->getAd());
        $abuse->setUser($event->getUser());

        if (Abuse::REASON_SPAM == $abuse->getReason()) {
            $abuse->setDescription(null);
        }

        $this->objectManager->persist($abuse);
        $this->objectManager->flush();

        $censors = $this->userManager->getAllCensors();

        $this->mailerManager->sendAbuseEmailForUser($abuse);
        $this->mailerManager->sendAbuseEmailForCensor($abuse, $censors);

        $this->flashMessage->success($this->translator->trans('Your abuse sent. Thank You.'));

        // save history
        $this->historyManager->logEvent($event->getUser(), History::ACTION_ABUSE, $event->getAd(), RDWHistoryEvents::ABUSE_OBJECT);
    }
}
