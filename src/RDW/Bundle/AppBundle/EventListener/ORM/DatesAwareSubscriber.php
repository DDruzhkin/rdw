<?php

namespace RDW\Bundle\AppBundle\EventListener\ORM;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Util\ClassUtils;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Mapping\ClassMetadata;

use RDW\Bundle\AppBundle\Entity\CreatedAtAwareInterface;
use RDW\Bundle\AppBundle\Entity\UpdatedAtAwareInterface;

class DatesAwareSubscriber implements EventSubscriber
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var ClassMetadata[]
     */
    protected $metadataCache = [];

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return array(
            'onFlush',
        );
    }

    /**
     * @param OnFlushEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args)
    {
        $this->entityManager = $args->getEntityManager();
        $unitOfWork = $this->entityManager->getUnitOfWork();

        $newEntities = $unitOfWork->getScheduledEntityInsertions();
        $updateEntities = $unitOfWork->getScheduledEntityUpdates();

        foreach ($newEntities as $entity) {
            if ($this->updateCreatedAt($entity)) {
                $this->updateChangeSets($entity);
            }
            if ($this->updateUpdatedAt($entity)) {
                $this->updateChangeSets($entity);
            }
        }
        foreach ($updateEntities as $entity) {
            if ($this->updateUpdatedAt($entity)) {
                $this->updateChangeSets($entity);
            }
        }
    }

    /**
     * @param object $entity
     */
    protected function updateChangeSets($entity)
    {
        $metadata = $this->getMetadataForEntity($entity);
        $this->entityManager->getUnitOfWork()
            ->recomputeSingleEntityChangeSet($metadata, $entity);
    }

    /**
     * @param object $entity
     *
     * @return ClassMetadata
     */
    protected function getMetadataForEntity($entity)
    {
        $class = ClassUtils::getClass($entity);
        if (!isset($this->metadataCache[$class])) {
            $this->metadataCache[$class] = $this->entityManager->getClassMetadata($class);
        }

        return $this->metadataCache[$class];
    }

    /**
     * @param object $entity
     *
     * @return bool
     */
    protected function updateCreatedAt($entity)
    {
        if ($entity instanceof CreatedAtAwareInterface && !$entity->getCreatedAt()) {
            $entity->setCreatedAt($this->getNowDate());

            return true;
        }

        return false;
    }

    /**
     * @param object $entity
     *
     * @return bool
     */
    protected function updateUpdatedAt($entity)
    {
        if ($entity instanceof UpdatedAtAwareInterface && !$entity->getUpdatedAt()) {
            $entity->setUpdatedAt($this->getNowDate());

            return true;
        }

        return false;
    }

    /**
     * @return \DateTime
     */
    protected function getNowDate()
    {
        return new \DateTime('now', new \DateTimeZone('UTC'));
    }
}
