<?php

namespace RDW\Bundle\AppBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use RDW\Bundle\SubscriptionBundle\Service\SubscriptionManager;
use Symfony\Component\Security\Http\SecurityEvents;
use RDW\Bundle\SubscriptionBundle\Entity\Subscription;

class SubscriptionCreateSubscriber extends AbstractCreateSubscriber
{
    /**
     * @var SubscriptionManager
     */
    protected $subscriptionManager;

    /**
     * SubscriptionCreateSubscriber constructor.
     * @param Session $session
     * @param SubscriptionManager $subscriptionManager
     * @param EventDispatcherInterface $eventDispatcher
     * @param $sessionKey
     */
    public function __construct(
        Session $session,
        SubscriptionManager $subscriptionManager,
        EventDispatcherInterface $eventDispatcher,
        $sessionKey
    )
    {
        parent::__construct($session, $eventDispatcher, $sessionKey);

        $this->subscriptionManager = $subscriptionManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            'rdw_app.subscription.pre_create' => 'onPreCreate',
            SecurityEvents::INTERACTIVE_LOGIN => 'onSecurityInteractiveLogin',
            FOSUserEvents::SECURITY_IMPLICIT_LOGIN => 'onImplicitLogin',
            FOSUserEvents::REGISTRATION_CONFIRM => 'onRegistrationConfirm',
        );
    }

    /**
     * @param UserInterface $user
     */
    public function create($user)
    {
        $subscription = $this->session->get($this->sessionKey);

        if ($user instanceof UserInterface && $subscription instanceof Subscription) {
            $subscription->setUser($user);
            $subscription->setEmail($user->getEmail());

            $this->subscriptionManager->update($subscription);
            $this->session->getFlashBag()->add('success', 'You successfully subscribe new subscription');

            $this->session->remove($this->sessionKey);
        }
    }
}
