<?php

namespace RDW\Bundle\AppBundle\EventListener;

use RDW\Bundle\CvBundle\Controller\CvController;
use RDW\Bundle\JobBundle\Controller\JobController;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

/**
 * Class SitemapListener
 *
 * @package RDW\Bundle\AppBundle\EventListener
 */
class SelectedSearchListener extends ContainerAware
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        if (!is_array($controller)) {
            return;
        }

        if (isset($controller[1]) && ! in_array($controller[1], ['indexAction', 'listAction', 'viewAction'])) {
            return;
        }

        if ($controller[0] instanceof CvController) {
            $this->container->get('request')->getSession()->set('application_type', 'cv');
        }

        if ($controller[0] instanceof JobController) {
            $this->container->get('request')->getSession()->set('application_type', 'job');
        }


    }
}
