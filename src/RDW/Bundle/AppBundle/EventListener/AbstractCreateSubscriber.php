<?php

namespace RDW\Bundle\AppBundle\EventListener;

use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Model\UserInterface;
use RDW\Bundle\AppBundle\Event\GenericGuestEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * Class AbstractCreateSubscriber
 * @package RDW\Bundle\AppBundle\EventListener
 */
abstract class AbstractCreateSubscriber implements EventSubscriberInterface
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * @var string
     */
    protected $sessionKey;

    /**
     * AbstractCreateSubscriber constructor.
     * @param Session $session
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        Session $session,
        EventDispatcherInterface $eventDispatcher,
        $sessionKey
    )
    {
        $this->session = $session;
        $this->eventDispatcher = $eventDispatcher;
        $this->sessionKey = $sessionKey;
    }

    /**
     * @param GenericGuestEvent $event
     */
    public function onPreCreate(GenericGuestEvent $event)
    {
        $subject = $event->getSubject();
        $request = $event->getRequest();

        $this->session->set($this->sessionKey, $subject);
        $this->session->set('referer', $request->headers->get('referer'));
    }

    /**
     * @param GetResponseUserEvent $event
     */
    public function onRegistrationConfirm(GetResponseUserEvent $event)
    {
        if ($referer = $this->session->get('referer')) {
            $event->setResponse(new RedirectResponse($referer));
        }
    }

    /**
     * @param InteractiveLoginEvent $event
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $this->create($event->getAuthenticationToken()->getUser());
    }

    /**
     * @param UserEvent $event
     */
    public function onImplicitLogin(UserEvent $event)
    {
        $this->create($event->getUser());
    }

    /**
     * @param UserInterface $user
     */
    abstract public function create($user);
}
