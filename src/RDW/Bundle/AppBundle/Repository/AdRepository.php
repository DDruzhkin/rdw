<?php

namespace RDW\Bundle\AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use RDW\Bundle\OrderBundle\RDWOrderStatus;
use RDW\Bundle\ServiceBundle\Entity\Service;

class AdRepository extends EntityRepository
{
    public function findAllExpiredWithAttributesByServiceTypeAndRepository($repository, $serviceTypeId)
    {
        if ($repository === 'RDWJobBundle:Job') {
            $fromTable = 'jobs';
            $serviceType = 'job';
        } else {
            $fromTable = 'cvs';
            $serviceType = 'cv';
        }

        $sql = sprintf(
            "SELECT i.id, i.is_top, i.is_highlighted FROM %s i
            INNER JOIN (SELECT MAX(oi.valid_till) as valid_till, oi.service_id, oi.item_id FROM order_items oi WHERE oi.type = :serviceType GROUP BY oi.item_id, oi.service_id) oi_max ON (oi_max.item_id = i.id)
            INNER JOIN order_items oi on oi.valid_till = oi_max.valid_till AND oi.service_id = oi_max.service_id AND oi.item_id = i.id AND oi.valid_till < :validTill
            INNER JOIN services s ON oi.service_id = s.id
            WHERE s.type = :serviceTypeId AND oi.status = :orderItemStatus AND i.deleted_at IS NULL",
            $fromTable
        );

        if (Service::TYPE_SHOW_IN_TOP == $serviceTypeId) {
            $sql .= " AND i.is_top = 1";
        } elseif (Service::TYPE_HIGHLIGHT == $serviceTypeId) {
            $sql .= " AND i.is_highlighted = 1";
        } else {
            return [];
        }

        $rsm = new \Doctrine\ORM\Query\ResultSetMapping;
        $rsm->addEntityResult($repository, 'i');
        $rsm->addFieldResult('i','id','id');
        $rsm->addFieldResult('i','is_top','top');
        $rsm->addFieldResult('i','is_highlighted','highlighted');

        $query = $this->_em->createNativeQuery($sql, $rsm);
        $query->setParameters([
            'serviceType' => $serviceType,
            'serviceTypeId' => $serviceTypeId,
            'validTill' => new \DateTime(),
            'orderItemStatus' => RDWOrderStatus::STATUS_PAID
        ]);

        return $query->getResult();
    }

    protected function getPopularByParams($repositoryName, $limit, $joinItems, $status)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('i', 'COUNT(joinedItem.id) AS totalCount')
            ->from($repositoryName, 'i')
            ->leftJoin($joinItems, 'joinedItem')
            ->where('joinedItem.status = :status')
            ->setParameter('status', $status)
            ->groupBy('i.id')
            ->orderBy('totalCount', 'DESC')
            ->setMaxResults($limit);

        $results = [];

        foreach ($queryBuilder->getQuery()->getResult() as $item) {
            $result = $item[0];

            $result->setEntitiesCount($item['totalCount']);
            $results[] = $result;
        }

        return $results;
    }
}
