<?php

namespace RDW\Bundle\AppBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class OverrideOneupUploaderValidatorsCompilerPass
 *
 * @package RDW\Bundle\AppBundle\DependencyInjection\Compiler
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class OverrideOneupUploaderValidatorsCompilerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $maxFileSizeValidator = $container->getDefinition('oneup_uploader.validation_listener.max_size');
        $maxFileSizeValidator->setClass('RDW\Bundle\AppBundle\Uploader\Validation\MaxSizeValidationListener');
        $maxFileSizeValidator->addArgument(new Reference('translator'));

        $allowedMimeTypeValidator = $container->getDefinition('oneup_uploader.validation_listener.allowed_mimetype');
        $allowedMimeTypeValidator->setClass('RDW\Bundle\AppBundle\Uploader\Validation\AllowedMimeTypeValidationListener');
        $allowedMimeTypeValidator->addArgument(new Reference('translator'));

        $disallowedMimeTypeValidator = $container->getDefinition('oneup_uploader.validation_listener.disallowed_mimetype');
        $disallowedMimeTypeValidator->setClass('RDW\Bundle\AppBundle\Uploader\Validation\DisallowedMimeTypeValidationListener');
        $disallowedMimeTypeValidator->addArgument(new Reference('translator'));
    }
}
