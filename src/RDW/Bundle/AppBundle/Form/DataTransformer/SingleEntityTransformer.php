<?php

namespace RDW\Bundle\AppBundle\Form\DataTransformer;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class SingleEntityTransformer
 *
 * @package RDW\Bundle\AppBundle\Form\DataTransformer
 */
class SingleEntityTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    protected $om;

    /**
     * @var string
     */
    protected $repositoryName;

    /**
     * @param ObjectManager $om
     * @param string        $repositoryName
     */
    public function __construct(ObjectManager $om, $repositoryName)
    {
        $this->om = $om;
        $this->repositoryName = $repositoryName;
    }

    /**
     * Transforms an object to a string.
     *
     * @param  object|null $item
     *
     * @return string
     */
    public function transform($item)
    {
        if (null === $item) {
            return "";
        }

        return $item->getId();
    }

    /**
     * Transforms a string to an object.
     *
     * @param string $id
     *
     * @return object|null
     */
    public function reverseTransform($id)
    {
        if (! $id) {
            return null;
        }

        return $this->om->getRepository($this->repositoryName)->find($id);
    }
}
