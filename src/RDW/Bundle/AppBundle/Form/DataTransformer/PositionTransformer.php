<?php

namespace RDW\Bundle\AppBundle\Form\DataTransformer;

use RDW\Bundle\JobBundle\Entity\Position;

/**
 * Class SingleEntityTransformer
 *
 * @package RDW\Bundle\AppBundle\Form\DataTransformer
 */
class PositionTransformer extends SingleEntityTransformer
{
    /**
     * Transforms a string to an object.
     *
     * @param string $id
     *
     * @return object|null
     */
    public function reverseTransform($id)
    {
        if (!$id) {
            return null;
        }

        $found = null;
        if (is_numeric($id)) {
            $found = $this->om->getRepository($this->repositoryName)->find($id);
        }

        if (!$found && is_string($id)) {
            $found = $this->om->getRepository($this->repositoryName)->findOneBy(['title' => $id]);
            if(!$found){
                $found = new Position();
                $found->setTitle($id);
                $this->om->persist($found);
                $this->om->flush($found);
            }
        }

        return $found;
    }
}
