<?php

namespace RDW\Bundle\AppBundle\Form\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class EntityCollectionTransformer
 *
 * @package RDW\Bundle\AppBundle\Form\DataTransformer
 */
class EntityCollectionTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @var string
     */
    private $repositoryName;

    /**
     * @param ObjectManager $om
     * @param string        $repositoryName
     */
    public function __construct(ObjectManager $om, $repositoryName)
    {
        $this->om = $om;
        $this->repositoryName = $repositoryName;
    }

    /**
     * @param ArrayCollection $items
     *
     * @return string
     */
    public function transform($items)
    {
        if (null === $items) {
            return null;
        }

        $ids = [];

        if ($items->count() > 0) {
            foreach ($items as $item) {
                $ids[] = $item->getId();
            }
        }

        return implode(',', $ids);
    }

    /**
     * @param string $ids
     *
     * @return ArrayCollection
     */
    public function reverseTransform($ids)
    {
        $items = new ArrayCollection();

        if (is_string($ids)) {
            foreach (array_filter(explode(',', $ids)) as $id) {
                $item = $this->om->getRepository($this->repositoryName)->find($id);

                if (! $items->contains($item)) {
                    $items->add($item);
                }
            }    
        }

        return $items;
    }
}
