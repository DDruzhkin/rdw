<?php

namespace RDW\Bundle\AppBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class FormattedNumberTransformer
 * @package RDW\Bundle\AppBundle\Form\DataTransformer
 */
class FormattedNumberTransformer implements DataTransformerInterface
{
    /**
     * @param string|number $number
     * @return int|null
     */
    public function transform($number)
    {
        return $this->transformString($number);
    }

    /**
     * @param string|number $input
     * @return int|null
     */
    public function reverseTransform($input)
    {
        return $this->transformString($input);
    }

    /**
     * @param $input
     * @return int|null
     */
    private function transformString($input)
    {
        return (empty($input) && $input !== '0') ? null : (double) str_replace(" ", "", $input);
    }
}
