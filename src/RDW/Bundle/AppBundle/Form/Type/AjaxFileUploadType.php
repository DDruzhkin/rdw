<?php

namespace RDW\Bundle\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class AjaxFileUploadType
 *
 * @package RDW\Bundle\AppBundle\Form\Type
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class AjaxFileUploadType extends AbstractType
{
    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
                'mapped' => false,
                'required' => false,
                /** @Ignore */
                'label' => false,
                'file_property' => null,
            ]);
    }

    /**
     * {@inheritDoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['file_name'] = $this->getValue($form, $options['file_property']);
        $view->vars['photo'] = ('document' != $options['file_property']) ? true : false;
    }

    /**
     * @param FormInterface $form
     * @param string        $fileProperty
     *
     * @return null
     */
    private function getValue(FormInterface $form, $fileProperty)
    {
        $formData = $form->getParent()->getData();

        if (is_object($formData) && ($value = $formData->{'get' . ucfirst($fileProperty)}())) {
            return $value;
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function getParent()
    {
        return 'file';
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'ajax_upload';
    }
}
