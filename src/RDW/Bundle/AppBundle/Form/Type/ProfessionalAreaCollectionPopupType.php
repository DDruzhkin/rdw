<?php

namespace RDW\Bundle\AppBundle\Form\Type;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\AppBundle\Form\DataTransformer\EntityCollectionTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class PositionCustom
 *
 * @package RDW\Bundle\AppBundle\Form\Type
 */
class ProfessionalAreaCollectionPopupType extends AbstractType
{
    /**
     * @var ObjectManager
     */
    protected $om;

    /**
     * @var string
     */
    private $filter;

    /**
     * @var string
     */
    protected $repositoryName;


    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * @param string $filter
     */
    public function setFilter($filter)
    {
        $this->filter = $filter;
    }

    /**
     * @param string $repositoryName
     */
    public function setRepositoryName($repositoryName)
    {
        $this->repositoryName = $repositoryName;
    }

    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->setAttribute('pa_list', $options['pa_list']);
        $builder->addModelTransformer(new EntityCollectionTransformer($this->om, $this->repositoryName));
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'pa_list' => $this->getProfessionalAreaData(),
            'mapped' => true,
            'error_bubbling' => false,
            'required' => false,
            'label' => ucfirst($this->filter),
            'attr' => [
                'placeholder' => 'Choose an option',
                'class' => 'professional-area-collection-popup',
                'data-filter' => $this->filter,
            ],

        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);
        $view->vars['pa_list'] = $options['pa_list'];
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'professional_area_collection_popup';
    }

    /**
     * {@inheritDoc}
     */
    public function getParent()
    {
        return HiddenType::class;
    }

    /**
     * Get PA entities and prepare data from it.
     *
     * @return array
     */
    protected function getProfessionalAreaData() {
        $professionalAreaResponse = [];
        $repository = $this->om->getRepository($this->repositoryName);
        $professionalAreaEntities = $repository->findAll();
        /** @var \RDW\Bundle\JobBundle\Entity\ProfessionalArea $professionalAreaEntity */
        foreach ($professionalAreaEntities as $professionalAreaEntity) {
            if ($professionalAreaEntity->isParent()) {
                $parentId = $professionalAreaEntity->getId();
                /** @var \Doctrine\ORM\PersistentCollection $collection */
                $collection = $professionalAreaEntity->getChildren();
                if ($children = $collection->getValues()) {
                    $professionalAreaResponse[$parentId] = [
                        'name' => $professionalAreaEntity->getTitle(),
                        'id' => $parentId,
                    ];

                    usort($children, function ($a, $b) {
                        return strcmp($a->getTitle(), $b->getTitle());
                    });

                    /** @var \RDW\Bundle\JobBundle\Entity\ProfessionalArea $child */
                    foreach ($children as $child) {
                        $professionalAreaResponse[$parentId]['children'][] = [
                            'name' => $child->getTitle(),
                            'id' => $child->getId(),
                        ];
                    }
                }
            }
        }

        usort($professionalAreaResponse, function ($a, $b) {
            return strcmp($a['name'], $b['name']);
        });

        return $professionalAreaResponse;
    }
}
