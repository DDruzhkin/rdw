<?php

namespace RDW\Bundle\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;

/**
 * Class SalaryType
 * @package RDW\Bundle\AppBundle\Form\Type
 */
class SalaryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'text';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'salary';
    }
}
