<?php

namespace RDW\Bundle\AppBundle\Form\Type;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\AbstractType;
use RDW\Bundle\AppBundle\Form\DataTransformer\PositionAutocompleteTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


/**
 * Class PositionAutocompleteType
 *
 * @package RDW\Bundle\AppBundle\Form\Type
 */
class PositionAutocompleteType extends AbstractType
{
    /**
     * @var ObjectManager
     */
    protected $om;

    /**
     * @var string
     */
    protected $filter;

    /**
     * @var string
     */
    protected $repositoryName;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * @param string $filter
     */
    public function setFilter($filter)
    {
        $this->filter = $filter;
    }

    /**
     * @param string $repositoryName
     */
    public function setRepositoryName($repositoryName)
    {
        $this->repositoryName = $repositoryName;
    }

    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->addModelTransformer(new PositionAutocompleteTransformer($this->om, $this->repositoryName));
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {

        $resolver->setDefaults([
            'mapped' => true,
            'error_bubbling' => false,
            'required' => false,
            'label' => ucfirst($this->filter),
            'attr' => [
                'placeholder' => 'Choose option',
                'class' => 'position-autocomplete',
                'data-filter' => $this->filter,
            ],

        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'position_autocomplete';
    }

    /**
     * {@inheritDoc}
     */
    public function getParent()
    {
        return TextType::class;
    }
}
