<?php

namespace RDW\Bundle\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use RDW\Bundle\AppBundle\Form\EventListener\BuildDocumentUploadFormListener;

abstract class AbstractImportType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventSubscriber(new BuildDocumentUploadFormListener());
    }
}
