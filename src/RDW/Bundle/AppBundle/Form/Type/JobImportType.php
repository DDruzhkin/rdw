<?php

namespace RDW\Bundle\AppBundle\Form\Type;

class JobImportType extends AbstractImportType
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_job_import';
    }
}
