<?php

namespace RDW\Bundle\AppBundle\Form\Type;

use RDW\Bundle\AppBundle\Entity\Abuse;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class AbuseType
 * @package RDW\Bundle\CvBundle\Form\Type
 */
class AbuseType extends AbstractType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reason', 'choice', [
                'choices' => $this->getReasons($builder->getData()),
                'label' => 'Reason',
                'required' => true,
                'multiple' => false,
                'expanded' => true,
            ])
            ->add('description', 'textarea', [
                'label' => 'Abuse description',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Abuse description placeholder'
                ]
            ])
            ->add('submit', 'button', [
                'label' => 'Send',
            ]);
    }

    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'cascade_validation' => true,
            'validation_groups' =>
                function (FormInterface $form)
                {
                    $groups = ['create_abuse'];
                    $reasonsOther = [Abuse::REASON_COMPANY_INCORRECT, Abuse::REASON_OTHER];

                    if (in_array($form->getData()->getReason(), $reasonsOther)) {
                        $groups[] = 'other';
                    }

                    return $groups;
                }
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'rdw_abuse';
    }

    private function getReasons($entity)
    {
        if ($entity instanceof \RDW\Bundle\AppBundle\Entity\JobAbuse) {
            return [
                Abuse::REASON_SPAM => 'This Job is considered as spam',
                Abuse::REASON_COMPANY_INCORRECT => 'Job information not correct (please describe)',
                Abuse::REASON_OTHER => 'Other (please describe)',
            ];
        } elseif ($entity instanceof \RDW\Bundle\AppBundle\Entity\CvAbuse) {
            return [
                Abuse::REASON_SPAM => 'This CV is considered as spam',
                Abuse::REASON_COMPANY_INCORRECT => 'CV information not correct (please describe)',
                Abuse::REASON_OTHER => 'Other (please describe)',
            ];
        } else {
            throw new UnexpectedTypeException($entity, 'RDW\Bundle\AppBundle\Entity\Abuse');
        }
    }
}
