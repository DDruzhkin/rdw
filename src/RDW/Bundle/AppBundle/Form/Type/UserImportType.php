<?php

namespace RDW\Bundle\AppBundle\Form\Type;

class UserImportType extends AbstractImportType
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_user_import';
    }
}
