<?php

namespace RDW\Bundle\AppBundle\Form\Type\Select2;

use RDW\Bundle\AppBundle\Form\DataTransformer\PositionTransformer;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class PositionType
 *
 * @package RDW\Bundle\AppBundle\Form\Type\Select2
 */
class PositionType extends AbstractSelect2Type
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->addModelTransformer(new PositionTransformer($this->om, $this->repositoryName));
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'position';
    }
}
