<?php

namespace RDW\Bundle\AppBundle\Form\Type\Select2;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class AbstractSphinxSelect2Type
 *
 * @package RDW\Bundle\AppBundle\Form\Type
 */
abstract class AbstractSelect2Type extends AbstractType
{
    /**
     * @var ObjectManager
     */
    protected $om;

    /**
     * @var string
     */
    private $filter;

    /**
     * @var string
     */
    protected $repositoryName;

    /**
     * @var bool
     */
    private $multiple;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
        $this->multiple = false;
    }

    /**
     * @param string $filter
     */
    public function setFilter($filter)
    {
        $this->filter = $filter;
    }

    /**
     * @param string $repositoryName
     */
    public function setRepositoryName($repositoryName)
    {
        $this->repositoryName = $repositoryName;
    }

    /**
     * @param bool $multiple
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;
    }

    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $class = (false === $this->multiple) ? 'select2-hidden-single' : 'select2-hidden-multi';

        $resolver->setDefaults([
            'mapped' => true,
            'error_bubbling' => false,
            'required' => false,
            'label' => ucfirst($this->filter),
            'attr' => [
                'placeholder' => 'Choose option',
                'class' => $class,
                'data-filter' => $this->filter,
            ],
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function getParent()
    {
        return 'hidden';
    }
}
