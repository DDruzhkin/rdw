<?php

namespace RDW\Bundle\AppBundle\Form\Type\Select2;

use RDW\Bundle\AppBundle\Form\DataTransformer\EntityCollectionTransformer;
use Symfony\Component\Form\FormBuilderInterface;

class ProfessionalAreaCollectionType extends AbstractSelect2Type
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->addModelTransformer(new EntityCollectionTransformer($this->om, $this->repositoryName));
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'professional_area_collection';
    }
}
