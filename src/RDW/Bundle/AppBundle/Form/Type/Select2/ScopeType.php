<?php

namespace RDW\Bundle\AppBundle\Form\Type\Select2;

use RDW\Bundle\AppBundle\Form\DataTransformer\SingleEntityTransformer;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class ScopeType
 *
 * @package RDW\Bundle\AppBundle\Form\Type\Select2
 */
class ScopeType extends AbstractSelect2Type
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->addModelTransformer(new SingleEntityTransformer($this->om, $this->repositoryName));
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'scope';
    }
}
