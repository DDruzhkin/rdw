<?php

namespace RDW\Bundle\AppBundle\Form\Type\Select2;

use RDW\Bundle\AppBundle\Form\DataTransformer\SingleEntityTransformer;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class CityType
 *
 * @package RDW\Bundle\AppBundle\Form\Type\Select2
 */
class CityType extends AbstractSelect2Type
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->addModelTransformer(new SingleEntityTransformer($this->om, $this->repositoryName));
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'city';
    }
}
