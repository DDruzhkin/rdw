<?php

namespace RDW\Bundle\AppBundle\Form\Type;

use RDW\Bundle\AppBundle\Validator\Constraints\SumCaptchaValidator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Two digits sum captcha form type
 */
class SumCaptchaFormType extends AbstractType
{
    /**
     * @var SessionInterface
     */
    protected $session;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var int
     */
    protected $digit1;

    /**
     * @var int
     */
    protected $digit2;

    /**
     * @param SessionInterface    $session
     * @param TranslatorInterface $translator
     */
    public function __construct(SessionInterface $session, TranslatorInterface $translator)
    {
        $this->session = $session;
        $this->translator = $translator;
    }

    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'mapped' => false,
            'required' => false,
            /** @Ignore */
            'label' => false
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $validator = new SumCaptchaValidator($this->session, $this->translator);

        $builder->addEventListener(FormEvents::POST_SUBMIT, [$validator, 'validate']);
    }

    /**
     * {@inheritDoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if ($this->isNeedRegenerateDigitsInForm($form)) {
            $this->generateDigits();

            $view->vars['digit1'] = $this->digit1;
            $view->vars['digit2'] = $this->digit2;
            $view->vars['value'] = '';
        } else {
            $view->vars['digit1'] = $this->session->get('_digit1');
            $view->vars['digit2'] = $this->session->get('_digit2');
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getParent()
    {
        return 'text';
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'sum_captcha';
    }

    /**
     * Set two digits sum to session
     *
     * @param int $sum
     */
    protected function setCaptchaSum($sum)
    {
        $this->session->set('_sum_captcha', $sum);
    }

    /**
     * @param FormInterface $form
     *
     * @return bool
     */
    protected function isNeedRegenerateDigitsInForm(FormInterface $form)
    {
        if (!$this->session->get('_digit1') || !$this->session->get('_digit2')) {
            return true;
        }

        if (!$form->getData() || $form->getErrors()) {
            return true;
        }

        return false;
    }

    /**
     * Generate digits and save captcha sum
     */
    protected function generateDigits()
    {
        $this->digit1 = rand(1, 5);
        $this->digit2 = rand(6, 9);
        $this->session->set('_digit1', $this->digit1);
        $this->session->set('_digit2', $this->digit2);
        $this->session->set('_sum_captcha', $this->digit1 + $this->digit2);
    }
}
