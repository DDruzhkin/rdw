<?php

namespace RDW\Bundle\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\NotBlank;
use RDW\Bundle\AppBundle\Form\DataTransformer\FormattedNumberTransformer;

/**
 * Class BalanceFormType
 *
 * @package RDW\Bundle\AppBundle\Form\Type
 */
class BalanceType extends AbstractType
{
    /**
     * @var string
     */
    private $currency;

    /**
     * @param string $currency
     */
    public function __construct($currency)
    {
        $this->currency = $currency;
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('price', 'text', [
                'required' => false,
                'label' => 'Add money to balance',
                'constraints' => [
                    new NotBlank(),
                    new GreaterThan(['value' => 0]),
                ],
                'mapped' => false,
            ]);

            $builder
                ->get('price')
                ->addModelTransformer(new FormattedNumberTransformer());
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_balance';
    }
}
