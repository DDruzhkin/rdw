<?php

namespace RDW\Bundle\AppBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class BuildAgeFormListener
 * @package RDW\Bundle\AppBundle\Form\EventListener
 */
class BuildAgeFormListener implements EventSubscriberInterface
{
    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function preSetData(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();

        $form
            ->add('age', 'integer', [
                'label' => 'Age',
                'required' => false,
                'mapped'   => false,
                'data'     => $data->getAge()
            ]);
    }
}
