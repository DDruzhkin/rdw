<?php

namespace RDW\Bundle\AppBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class BuildContactPersonPhotoUploadFormListener
 * @package RDW\Bundle\AppBundle\Form\EventListener
 */
class BuildContactPersonPhotoUploadFormListener implements EventSubscriberInterface
{
    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function preSetData(FormEvent $event)
    {
        $form = $event->getForm();

        $form
            ->add('contactPersonPhoto', 'hidden', [
                /** @Ignore */
                'label' => false,
                'required' => false,
                'attr' => [
                    'data-clear-person' => '',
                ],
            ])
            ->add('uploadContactPersonPhoto', 'ajax_upload', [
                'label' => 'Contact person photo',
                'required' => false,
                'attr' => [
                    'data-hide' => '',
                    'data-clear-person' => '',
                ],
                'mapped' => false,
                'file_property' => 'contactPersonPhoto',
            ]);
    }
}
