<?php

namespace RDW\Bundle\AppBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class BuildPhotoUploadFormListener
 *
 * @package RDW\Bundle\AppBundle\Form\EventListener
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class BuildPhotoUploadFormListener implements EventSubscriberInterface
{
    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function preSetData(FormEvent $event)
    {
        $form = $event->getForm();

        $form
            ->add('photo', 'hidden', [
                /** @Ignore */
                'label' => false,
                'required' => false,
                'attr' => [
                    'data-clear-company' => '',
                ],
            ])
            ->add('uploadPhoto', 'ajax_upload', [
                'label' => 'Company logo',
                'required' => false,
                'attr' => [
                    'data-hide' => '',
                    'data-clear-company' => '',
                ],
                'mapped' => false,
                'file_property' => 'photo',
            ]);
    }
}
