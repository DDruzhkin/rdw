<?php

namespace RDW\Bundle\AppBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class BuildDocumentUploadFormListener
 *
 * @package RDW\Bundle\AppBundle\Form\EventListener
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class BuildDocumentUploadFormListener implements EventSubscriberInterface
{
    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function preSetData(FormEvent $event)
    {
        $form = $event->getForm();

        $form
            ->add('document', 'hidden', [
                /** @Ignore */
                'label' => false,
                'required' => false,
            ])
            ->add('uploadDocument', 'ajax_upload', [
                'label' => 'Document',
                'required' => false,
                'attr' => ['data-hide' => ''],
                'mapped' => false,
                'file_property' => 'document',
            ]);
    }
}
