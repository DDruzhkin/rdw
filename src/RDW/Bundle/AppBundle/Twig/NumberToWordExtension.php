<?php

namespace RDW\Bundle\AppBundle\Twig;

use RDW\Bundle\AppBundle\Templating\Helper\NumberToWordHelper;

/**
 * Class NumberToWordExtension
 *
 * @package RDW\Bundle\AppBundle\Twig
 */
class NumberToWordExtension extends \Twig_Extension
{
    /**
     * @var NumberToWordHelper
     */
    private $helper;

    /**
     * @param NumberToWordHelper $helper
     */
    public function __construct(NumberToWordHelper $helper)
    {
        $this->helper = $helper;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('number_to_word', [$this, 'numberToWord']),
            new \Twig_SimpleFilter('number_to_currency', [$this, 'numberToCurrency']),
        ];
    }


    /**
     * @param int $number
     *
     * @return string
     */
    public function numberToWord($number)
    {
        return $this->helper->numberToWord($number);
    }

    /**
     * @param int $number
     *
     * @return string
     */
    public function numberToCurrency($number)
    {
        return $this->helper->numberToCurrency($number);
    }


    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'number_to_word_extension';
    }
}
