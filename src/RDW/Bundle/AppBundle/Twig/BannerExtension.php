<?php

namespace RDW\Bundle\AppBundle\Twig;

class BannerExtension extends \Twig_Extension
{
	public function getFunctions()
	{
		return [
			new \Twig_SimpleFunction('banner_id', [$this, 'getBannerId'])
		];
	}

	public function getBannerId($zone, $deviceType)
	{
        $zones = [
            'top' => [
                'mobile' => 27,
                'tablet' => 32,
                'desktop' => 1,
            ],
            'sidebar-1' => [
                'mobile' => 23,
                'tablet' => 28,
                'desktop' => 3,
            ],
            'sidebar-2' => [
                'mobile' => 24,
                'tablet' => 29,
                'desktop' => 11,
            ],
            'sidebar-3' => [
                'mobile' => 25,
                'tablet' => 30,
                'desktop' => 14,
            ],
            'sidebar-4' => [
                'mobile' => 26,
                'tablet' => 31,
                'desktop' => 17,
            ],
        ];

        if (array_key_exists($zone, $zones) && array_key_exists($deviceType, $zones[$zone])) {
            return $zones[$zone][$deviceType];
        }

        return null;
	}

	public function getName()
	{
		return 'rdw_banner_extension';
	}
}
