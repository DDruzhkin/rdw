<?php

namespace RDW\Bundle\AppBundle\Twig;

use Doctrine\Common\Collections\ArrayCollection;
use RDW\Bundle\JobBundle\Entity\WorkScheduleType;
use Symfony\Component\Form\Form;

/**
 * Class SeoExtension
 */
class SeoExtension extends \Twig_Extension
{
    /**
     * @var string
     */
    private $seoSeparator;

    /**
     * @var string
     */
    private $seoBrand;

    /**
     * @var int
     */
    private $descriptionLimit;

    /**
     * @var int
     */
    private $titleLimit;

    /**
     * @param string      $seoSeparator
     * @param string      $seoBrand
     * @param int         $descriptionLimit
     * @param int         $titleLimit
     */
    public function __construct(
        $seoSeparator,
        $seoBrand,
        $descriptionLimit,
        $titleLimit
    )
    {
        $this->seoSeparator = $seoSeparator;
        $this->seoBrand = $seoBrand;
        $this->descriptionLimit = $descriptionLimit;
        $this->titleLimit = $titleLimit;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('get_seo_title', [$this, 'getSeoTitle']),
            new \Twig_SimpleFunction('get_seo_description', [$this, 'getSeoDescription']),
            new \Twig_SimpleFunction('get_seo_keywords', [$this, 'getSeoKeywords']),
            new \Twig_SimpleFunction('get_img_title', [$this, 'getImageTitle']),
            new \Twig_SimpleFunction('meta_title', [$this, 'getMetaTitle']),
            new \Twig_SimpleFunction('title', [$this, 'getTitle']),
        ];
    }


    /**
     * Get seo title
     *
     * @param array $params
     *
     * @return string
     */
    public function getSeoTitle($params = null)
    {
        $title = '';

        if (!empty($params)) {
            foreach ($params as $key => $value) {
                $title .= strip_tags($value) . $this->seoSeparator;
            }
        }

        $title = (mb_strlen($title) > $this->titleLimit) ? mb_substr($title, 0, $this->titleLimit, 'utf-8') . '... ' : $title;

        return $title . $this->seoBrand;
    }

    /**
     * Get seo description
     *
     * @param array $params
     *
     * @return string
     */
    public function getSeoDescription($params = null)
    {
        $description = $this->cleanAndImplodeToString($params, ' ');

        $description = (strlen($description) > $this->descriptionLimit) ? mb_substr($description, 0, $this->descriptionLimit, 'utf-8') . '...' : $description;

        return $description;
    }

    /**
     * Get seo description
     *
     * @param array $params
     *
     * @return string
     */
    public function getSeoKeywords($params = null)
    {
        $keywords = $this->cleanAndImplodeToString($params, ', ');

        return $keywords;
    }

    /**
     * Get image title
     *
     * @param array $params
     *
     * @return string
     */
    public function getImageTitle($params = null)
    {
        $title = $this->cleanAndImplodeToString($params, ' ');

        return $title;
    }

    /**
     * builds meta title from Form object
     *
     * @return string
     */
    public function getMetaTitle(Form $form, $host)
    {
        $isJobForm = $form->getName() === 'jobs';
        $isCvForm = $form->getName() === 'cvs';

        if (!$isJobForm && !$isCvForm) {
            throw new \RuntimeException('Unsupported form provided');
        }

        $values = [];
        foreach ($form->all() as $field) {
            $hasValue = $field->getData() instanceof ArrayCollection ? count($field->getData()) > 0 : !empty($field->getData());
            if ($hasValue) {
                $values[] = ['key' => $field->getName(), 'data' => $field->getData()];
            }
        }

        if ($this->hasSubdomain($host)) {
            $city = null;
            foreach ($values as $value) {
                if ($value['key'] === 'cities') {
                    $city = $value['data']->first();
                    break;
                } else if ($value['key'] === 'workingPlaceCity') {
                    $city = $value['data'];
                }
            }

            if ($city) {
                return sprintf(
                    'Работа в %s, вакансии и резюме на %s',
                    $city->getTitleWhere(),
                    $host
                );
            } else {
                return sprintf(
                    'Работа в Минске и городах Беларуси, вакансии и резюме на %s',
                    $host
                );
            }
        } else if (count($values) === 1) {
            $value = array_shift($values);
            switch ($value['key']) {
                case 'cities':
                    $city = $value['data']->first();
                    return sprintf(
                        'Работа в %s, вакансии и резюме на %s',
                        $city->getTitleWhere(),
                        $host
                    );
                case 'position':
                    return sprintf(
                        'Работа по специальности %s, вакансии на %s',
                        mb_strtolower($value['data']->getTitle()),
                        $host
                    );
                case 'scope':
                    return sprintf(
                        'Работа в сфере %s, вакансии на %s',
                        mb_strtolower($value['data']->getTitle()),
                        $host
                    );
                case 'educationType':
                case 'education':
                    if ($value['data']->isWithoutEducation()) {
                        $title = $isJobForm ? 'Работа' : 'Резюме';
                        return sprintf(
                            '%s %s в Минске и городах Беларуси|%s.',
                            $title,
                            mb_strtolower($value['data']->getTitle()),
                            $host
                        );
                    }
                    break;
                case 'workExperienceType':
                case 'workExperience':
                    if ($value['data']->isWithoutExeperience()) {
                        $title = $isJobForm ? 'Работа' : 'Резюме';
                        return sprintf(
                            '%s %s в Минске и городах Беларуси|%s.',
                            $title,
                            mb_strtolower($value['data']->getTitle()),
                            $host
                        );
                    }
                    break;
                case 'workScheduleType':
                    if ($value['data']->getType()) {
                        switch ($value['data']->getType()) {
                            case WorkScheduleType::TYPE_REMOTE:
                                if ($isCvForm) {
                                    return sprintf(
                                        'Резюме на %s в Минске и городах Беларуси|%s.',
                                        mb_strtolower($value['data']->getTitle()),
                                        $host
                                    );
                                }
                                $titlePattern =  '%s в Минске и городах Беларуси|%s.';
                                break;
                            case WorkScheduleType::TYPE_ALTERNATIVE:
                                $titlePattern = $isJobForm ? 'Работа со %s в Минске и городах Беларуси|%s.' : 'Резюме со %s работы в Минске и городах Беларуси|rdw.by.';
                                return sprintf(
                                    $titlePattern,
                                    mb_strtolower($value['data']->getTitle()),
                                    $host
                                );
                            default:
                                if ($isCvForm) {
                                    return sprintf(
                                        'Резюме с %s работы в Минске и городах Беларуси|%s.',
                                        mb_strtolower($value['data']->getTitle()),
                                        $host
                                    );
                                }

                                $titlePattern = '%s работы в Минске и городах Беларуси|%s.';
                                break;
                        }

                        return sprintf($titlePattern, $value['data']->getTitle(), $host);
                    }
                    break;
                case 'employmentType':
                    if ($value['data']->isPartialEmployment()) {
                        if ($isCvForm) {
                            return sprintf(
                                'Резюме на подработку в Минске и городах Беларуси|%s.',
                                $host
                            );
                        }

                        return sprintf(
                            'Подработка в Минске и городах Беларуси|%s.',
                            $host
                        );
                    }
                    break;
                case 'workingPlaceCity':
                    $city = $value['data'];
                    return sprintf(
                        'Работа в %s, вакансии и резюме на %s.%s',
                        $city->getTitleWhere(),
                        $city->getTitleInLatin(),
                        $host
                    );
                case 'positions':
                    $position = $value['data']->first();
                    return sprintf(
                        'Резюме %s в Минске и городах Беларуси|%s.',
                        $position->getTitle(),
                        $host
                    );
                case 'scopes':
                    $scope = $value['data']->first();
                    return sprintf(
                        'Работа в сфере %s, резюме на %s',
                        $scope->getTitle(),
                        $host
                    );
            }
        }

        return $this->getSeoTitle();
    }

    public function getTitle(Form $form, $host)
    {
        $isJobForm = $form->getName() === 'jobs';
        $isCvForm = $form->getName() === 'cvs';

        if (!$isJobForm && !$isCvForm) {
            throw new \RuntimeException('Unsupported form provided');
        }

        $values = [];
        foreach ($form->all() as $field) {
            $hasValue = $field->getData() instanceof ArrayCollection ? count($field->getData()) > 0 : !empty($field->getData());
            if ($hasValue) {
                $values[] = ['key' => $field->getName(), 'data' => $field->getData()];
            }
        }

        if ($this->hasSubdomain($host)) {
            $city = null;
            foreach ($values as $value) {
                if ($value['key'] === 'cities') {
                    $city = $value['data']->first();
                    break;
                } else if ($value['key'] === 'workingPlaceCity') {
                    $city = $value['data'];
                }
            }

            if ($city) {
                return sprintf(
                    'Работа в %s, поиск работы в %s',
                    $city->getTitleWhere(),
                    $city->getTitleWhere()
                );
            } else {
                return 'Работа в Минске и городах Беларуси, вакансии и резюме на rdw.by';
            }
        } else if (count($values) === 1) {
            $value = array_shift($values);
            switch ($value['key']) {
                case 'cities':
                    $city = $value['data']->first();
                    return sprintf(
                        'Работа в %s, поиск работы в %s',
                        $city->getTitleWhere(),
                        $city->getTitleWhere()
                    );
                case 'position':
                    return sprintf(
                        'Вакансии по специальности %s в Минске и городах Беларуси.',
                        mb_strtolower($value['data']->getTitle())
                    );
                case 'scope':
                    return sprintf(
                        'Вакансии в сфере %s в Минске и городах Беларуси.',
                        mb_strtolower($value['data']->getTitle())
                    );
                case 'educationType':
                case 'education':
                    if ($value['data']->isWithoutEducation()) {
                        $title = $isJobForm ? 'Работа' : 'Резюме';
                        return sprintf(
                            '%s %s в Минске и городах Беларуси.',
                            $title,
                            mb_strtolower($value['data']->getTitle())
                        );
                    }
                    break;
                case 'workExperienceType':
                case 'workExperience':
                    if ($value['data']->isWithoutExeperience()) {
                        $title = $isJobForm ? 'Работа' : 'Резюме';
                        return sprintf(
                            '%s %s в Минске и городах Беларуси.',
                            $title,
                            mb_strtolower($value['data']->getTitle())
                        );
                    }
                    break;
                case 'workScheduleType':
                    if ($value['data']->getType()) {
                        switch ($value['data']->getType()) {
                            case WorkScheduleType::TYPE_REMOTE:
                                if ($isCvForm) {
                                    return sprintf(
                                        'Резюме на %s в Минске и городах Беларуси.',
                                        mb_strtolower($value['data']->getTitle())
                                    );
                                }
                                $titlePattern =  '%s в Минске и городах Беларуси.';
                                break;
                            case WorkScheduleType::TYPE_ALTERNATIVE:
                                $titlePattern = $isJobForm ? 'Работа со %s в Минске и городах Беларуси.' : 'Резюме со %s работы в Минске и городах Беларуси.';
                                return sprintf(
                                    $titlePattern,
                                    mb_strtolower($value['data']->getTitle())
                                );
                            default:
                                if ($isCvForm) {
                                    return sprintf(
                                        'Резюме с %s работы в Минске и городах Беларуси.',
                                        mb_strtolower($value['data']->getTitle())
                                    );
                                }

                                $titlePattern = '%s работы в Минске и городах Беларуси.';
                                break;
                        }

                        return sprintf(
                            $titlePattern,
                            $value['data']->getTitle()
                        );
                    }
                    break;
                case 'employmentType':
                    if ($value['data']->isPartialEmployment()) {
                        if ($isCvForm) {
                            return sprintf(
                                'Резюме на подработку в Минске и городах Беларуси.'
                            );
                        }

                        return sprintf('Подработка в Минске и городах Беларуси.');
                    }
                    break;
                case 'workingPlaceCity':
                    $city = $value['data'];
                    return sprintf(
                        'Работа в %s, поиск работы в %s',
                        $city->getTitleWhere(),
                        $city->getTitleWhere()
                    );
                case 'positions':
                    $position = $value['data']->first();
                    return sprintf(
                        'Резюме по специальности %s в Минске и городах Беларуси.',
                        $position->getTitle()
                    );
                case 'scopes':
                    $scope = $value['data']->first();
                    return sprintf(
                        'Резюме в сфере %s в Минске и городах Беларуси.',
                        $scope->getTitle(),
                        $host
                    );
            }
        }

        return false;
    }

    /**
     * checks whether host has subodmain
     *
     * @return boolean
     */
    private function hasSubdomain($host)
    {
        return count(explode('.', $host)) === 3;
    }

    /**
     * @param array $params
     * @param string $delimiter
     * @return string
     */
    private function cleanAndImplodeToString(array $params = null, $delimiter)
    {
        $string = '';

        if (!empty($params)) {
            $values = [];
            foreach ($params as $key => $value) {
                $values[]= strip_tags($value);
            }

            $string .= implode($delimiter, $values);
        }

        return $string;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_seo_extension';
    }
}
