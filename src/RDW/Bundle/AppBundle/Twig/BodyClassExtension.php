<?php

namespace RDW\Bundle\AppBundle\Twig;

use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Class BodyClassExtension
 *
 * @package RDW\Bundle\AppBundle\Twig
 */
class BodyClassExtension extends \Twig_Extension
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var string
     */
    protected $applicationType = null;

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct($container)
    {
        $this->container = $container;

        $request = $this->container->get('request_stack')->getCurrentRequest();

        if ($request) {
            $this->request = $request;

            if ($type = $this->request->getSession()->get('application_type')) {
                $this->applicationType = $type;
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('get_body_class', [$this, 'getBodyClass']),
            new \Twig_SimpleFunction('get_search_form_type', [$this, 'getSearchFormType']),
            new \Twig_SimpleFunction('get_opposite_search_form_type', [$this, 'getOppositeSearchFormType']),
            new \Twig_SimpleFunction('is_url_match_manage', [$this, 'isUrlMatchManage']),
            new \Twig_SimpleFunction('get_menu_class', [$this, 'getMenuClass']),
        ];
    }


    /**
     * Get body class by search form type
     *
     * @return string
     */
    public function getBodyClass()
    {
        $user = $this->getUser();

        if ($user instanceof RegisteredUser) {
            return ($user->isTypeEmployer() && !$user->isManager()) ? 'red' : 'blue';
        }

        return ('cv' !== $this->getControllerName()) ? 'blue' : 'red';
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function isUrlMatchManage(Request $request)
    {
        $pattern = "#/manage#";
        $matches = array();

        preg_match($pattern, $request->getRequestUri(), $matches);

        if (!empty($matches)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get search form type by controller
     *
     * @return string
     */
    public function getSearchFormType($request = null)
    {

        if($request != null) {

            $this->request = $request;
        }

        $user = $this->getUser();
        $controllerName = $this->getControllerName();

        if ($user instanceof RegisteredUser && in_array($controllerName, ['job', 'cv']) && $this->isSearchList()) {
            return $controllerName;
        } elseif ($user instanceof RegisteredUser) {
            return $user->isTypeEmployer() ? 'cv' : 'job';
        } elseif (!$user && $this->applicationType) {
            return $this->applicationType;
        }

        return ('cv' !== $this->getControllerName()) ? 'job' : 'cv';
    }

    /**
     * Get search form type by controller
     *
     * @return string
     */
    public function getOppositeSearchFormType()
    {
        return ('cv' !== $this->getSearchFormType()) ? 'cv' : 'job';
    }

    /**
     * @return string
     */
    private function getControllerName()
    {

        if (null !== $this->request) {

            $pattern = "#Controller\\\([a-zA-Z]*)Controller#";
            $matches = array();
            preg_match($pattern, $this->request->get('_controller'), $matches);

            if( isset($matches[1]) ) {
                return strtolower($matches[1]);
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isSearchList()
    {
        if (null !== $this->request) {
            $params = explode('::', $this->request->get('_controller'));
            $actionName = substr($params[1], 0, -6);

            if (in_array($actionName, ['index', 'list', 'view'])) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get menu class by user type
     *
     * @return string
     */
    public function getMenuClass()
    {
        $user = $this->getUser();

        if ($user instanceof RegisteredUser) {
            return ($user->isTypeEmployer() && ! $user->isManager()) ? 'red' : 'blue';
        }

        return 'blue';
    }

    /**
     * @return UserInterface|null
     */
    private function getUser()
    {
        /** @var $token TokenInterface */
        $token = $this->container->get('security.context')->getToken();

        return ($token) ? $token->getUser() : null;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_app_body_class_extension';
    }
}
