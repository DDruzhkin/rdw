<?php

namespace RDW\Bundle\AppBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ThumbExtension
 *
 * @package RDW\Bundle\AppBundle\Twig
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class ThumbExtension extends \Twig_Extension implements ContainerAwareInterface
{
    /**
     * Container
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('rdw_thumb', [$this, 'thumb']),

            // @todo move to right class
            new \Twig_SimpleFilter('full_url', [$this, 'getFullUrl']),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('defaultImageUrl', [$this, 'getDefaultImageUrl']),
        ];
    }

    /**
     * @param string $filename
     * @param string $filter
     * @param bool   $absolute
     * @param string $defaultImg
     *
     * @return mixed|string
     */
    public function thumb($filename, $filter, $absolute = true, $defaultImg = 'rdw_app.user_profile_no_logo')
    {
        return ($filename)
            ? $this->container->get('imagine.cache.path.resolver')->getBrowserPath($filename, $filter, $absolute)
            : $this->container->getParameter($defaultImg);
    }

    /**
     * @param string $uri
     *
     * @return string
     */
    public function getFullUrl($uri)
    {
        return $this->container->getParameter('assets_base_url') . $uri;
    }

    /**
     * @param string $type
     * @param string $size
     *
     * @return mixed|string
     */
    public function getDefaultImageUrl($type = 'job', $size = '')
    {
        $defaultImageUrl  = 'rdw_app.user_profile_no_';
        $defaultImageUrl .= ('job' != $type) ? 'person' : 'logo';
        $defaultImageUrl .= (!empty($size)) ? '_'.$size : '';

        return $this->container->getParameter('assets_base_url') . $this->container->getParameter($defaultImageUrl);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_app_thumb_extension';
    }
}
