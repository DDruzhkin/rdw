<?php

namespace RDW\Bundle\AppBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ThumbExtension
 *
 * @package RDW\Bundle\AppBundle\Twig
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class PerPageExtension extends \Twig_Extension implements ContainerAwareInterface
{
    /**
     * @var \Twig_Environment
     */
    protected $environment;

    /**
     * Container
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function initRuntime(\Twig_Environment $environment)
    {
        $this->environment = $environment;
    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('rdw_per_page_render', [$this, 'render'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * @param string $perPageOptionsParameter
     * @param string $routeName
     * @param array  $urlParams
     *
     * @return string
     *
     * @throws \Exception
     */
    public function render($perPageOptionsParameter, $routeName, $urlParams = [])
    {
        if (! $this->container->hasParameter($perPageOptionsParameter)) {
            throw new \Exception(sprintf('No parameter with key %s exist', $perPageOptionsParameter));
        }

        $perPageOptions = $this->container->getParameter($perPageOptionsParameter);

        //return $this->environment->render('RDWAppBundle::perPage.html.twig', [
        //    'options' => $perPageOptions,
        //    'route' => $routeName,
        //    'url_params' => $urlParams,
        //]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_per_page';
    }
}
