<?php

namespace RDW\Bundle\AppBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use RDW\Bundle\AppBundle\Entity\Setting;

class SettingExtension extends \Twig_Extension implements ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('rdw_setting', [$this, 'getSetting']),
        ];
    }

    public function getSetting($key)
    {
        $setting = $this
            ->container->get('doctrine.orm.entity_manager')
            ->getRepository('RDWAppBundle:Setting')
            ->findOneBy(['key' => $key]);

        return ($setting instanceof Setting) ? $setting->getValue() : null;
    }

    public function getName()
    {
        return 'rdw_app_setting_extension';
    }
}
