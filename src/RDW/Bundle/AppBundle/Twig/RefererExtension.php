<?php

namespace RDW\Bundle\AppBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class RefererExtension
 * @package RDW\Bundle\AppBundle\Twig
 */
class RefererExtension extends \Twig_Extension
{
    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var string
     */
    protected $referer = null;

    /**
     * Constructor
     *
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
        $request = $this->requestStack->getCurrentRequest();

        if ($request) {
            $this->referer = $request->headers->get('referer');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('get_url_referer', [$this, 'getUrlReferer']),
        ];
    }


    /**
     * Get body class by search form type
     *
     * @param array $urls
     * @return string
     */
    public function getUrlReferer($urls)
    {
        if (! is_array($urls)) {
            if (strpos($this->referer, $urls) !== false) {
                return $this->referer;
            } else {
                return $urls;
            }
        }

        foreach ($urls as $url) {
            if (strpos($this->referer, $url) !== false) {
                return $this->referer;
            }
        }

        return $urls[0];
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_app_url_referer_extension';
    }
}
