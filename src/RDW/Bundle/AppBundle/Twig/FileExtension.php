<?php

namespace RDW\Bundle\AppBundle\Twig;

use Symfony\Bundle\FrameworkBundle\Routing\Router;

/**
 * Class FileExtension
 *
 * @package RDW\Bundle\AppBundle\Twig
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class FileExtension extends \Twig_Extension
{
    /**
     * @var Router
     */
    private $router;

    /**
     * @param Router $router
     */
    public function __construct(\Symfony\Cmf\Component\Routing\ChainRouter $router)
    {

        $this->router = $router;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('rdw_file', [$this, 'getFileUrl']),
        ];
    }

    /**
     * @param string $filename
     * @param string $storage
     *
     * @return string
     */
    public function getFileUrl($filename, $storage)
    {
        return $this->router->generate('rdw_app.get_file', [
            'filename' => $filename,
            'storage' => $storage,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_app_file_extension';
    }
}
