<?php

namespace RDW\Bundle\AppBundle\Twig;

class AgeExtension extends \Twig_Extension
{
	public function getFunctions()
	{
		return [
			new \Twig_SimpleFunction('age_text', [$this, 'getAgeText'])
		];
	}

	public function getAgeText($age)
	{
		$age = (int)$age;
		$lastDigit = mb_substr($age, -1, 1);

		if ($age <= 4 || $age > 20) {
			switch ($lastDigit) {
				case 1:
					return 'год';
				case 2:
				case 3:
				case 4:
					return 'года';
			}
		}

		return 'лет';
	}

	public function getName()
	{
		return 'rdw_age_extension';
	}
}