<?php

namespace RDW\Bundle\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\ServiceBundle\Entity\Service;

/**
 * Class LoadServiceData
 *
 * @package RDW\Bundle\AppBundle\DataFixtures\ORM
 */
class LoadServiceData extends AbstractFixture
{
    /**
     * @var array
     */
    private $services = [
        [
            'title' => 'Highlight CV',
            'description' => 'Highlight CV in list',
            'price' => 10000,
            'for' => Service::FOR_EMPLOYEE,
            'personal' => false,
            'single' => false,
            'order_from_list' => true,
            'sale' => false,
            'sale_text' => '',
            'type' => Service::TYPE_HIGHLIGHT,
            'sms_number' => 9005,
            'visible' => true,
        ],
        [
            'title' => 'Show CV in top',
            'description' => 'Show CV in top of the list',
            'price' => 20000,
            'for' => Service::FOR_EMPLOYEE,
            'personal' => false,
            'single' => false,
            'order_from_list' => true,
            'sale' => false,
            'sale_text' => '',
            'type' => Service::TYPE_SHOW_IN_TOP,
            'sms_number' => 9010,
            'visible' => true,
        ],
        [
            'title' => 'Show CV in carousel',
            'description' => 'Show CV in carousel',
            'price' => 30000,
            'for' => Service::FOR_EMPLOYEE,
            'personal' => false,
            'single' => false,
            'order_from_list' => true,
            'sale' => false,
            'sale_text' => '',
            'type' => Service::TYPE_CAROUSEL,
            'sms_number' => 9010,
            'visible' => true,
        ],
        [
            'title' => 'Show vacancy in carousel',
            'description' => 'Show vacancy in carousel',
            'price' => 10000,
            'for' => Service::FOR_EMPLOYER,
            'personal' => false,
            'single' => false,
            'order_from_list' => true,
            'sale' => false,
            'sale_text' => '',
            'type' => Service::TYPE_CAROUSEL,
            'sms_number' => null,
            'visible' => true,
        ],
        [
            'title' => 'VIP',
            'description' => 'VIP employer',
            'price' => 50000,
            'for' => Service::FOR_EMPLOYER,
            'personal' => true,
            'single' => false,
            'order_from_list' => false,
            'sale' => false,
            'sale_text' => '',
            'type' => Service::TYPE_VIP,
            'sms_number' => null,
            'visible' => true,
        ],
        [
            'title' => 'Send email for employees',
            'description' => 'Send email for employees about job (matching by city and position)',
            'price' => 20000,
            'for' => Service::FOR_EMPLOYER,
            'personal' => false,
            'single' => true,
            'order_from_list' => false,
            'sale' => false,
            'sale_text' => '',
            'type' => Service::TYPE_SEND_EMAIL,
            'sms_number' => null,
            'visible' => true,
        ],
        [
            'title' => 'Highlight vacancy',
            'description' => 'Highlight vacancy in the list',
            'price' => 30000,
            'for' => Service::FOR_EMPLOYER,
            'personal' => false,
            'single' => false,
            'order_from_list' => true,
            'sale' => false,
            'sale_text' => '',
            'type' => Service::TYPE_HIGHLIGHT,
            'sms_number' => null,
            'visible' => true,
        ],
        [
            'title' => 'Publish vacancy',
            'description' => 'Make vacancy visible for users',
            'price' => 40000,
            'for' => Service::FOR_EMPLOYER,
            'personal' => false,
            'single' => false,
            'order_from_list' => true,
            'sale' => false,
            'sale_text' => '',
            'type' => Service::TYPE_JOB_PUBLISHING,
            'sms_number' => null,
            'visible' => true,
        ],
        [
            'title' => 'Show vacancy in top',
            'description' => 'Show vacancy in top of the list',
            'price' => 20000,
            'for' => Service::FOR_EMPLOYER,
            'personal' => false,
            'single' => false,
            'order_from_list' => true,
            'sale' => false,
            'sale_text' => '',
            'type' => Service::TYPE_SHOW_IN_TOP,
            'sms_number' => null,
            'visible' => true,
        ],
        [
            'title' => 'Add money to balance',
            'description' => 'Add money to balance',
            'price' => 0,
            'for' => Service::FOR_EMPLOYER,
            'personal' => true,
            'single' => true,
            'order_from_list' => false,
            'sale' => false,
            'sale_text' => '',
            'type' => Service::TYPE_MONEY_TO_BALANCE,
            'sms_number' => null,
            'visible' => false,
        ],
        [
            'title' => 'Add money to balance',
            'description' => 'Add money to balance',
            'price' => 0,
            'for' => Service::FOR_EMPLOYEE,
            'personal' => true,
            'single' => true,
            'order_from_list' => false,
            'sale' => false,
            'sale_text' => '',
            'type' => Service::TYPE_MONEY_TO_BALANCE,
            'sms_number' => null,
            'visible' => false,
        ],
    ];

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $entityManager)
    {
        foreach ($this->services as $key => $service) {
            $item = new Service();
            $item->setTitle($service['title']);
            $item->setDescription($service['description']);
            $item->setPrice($service['price']);
            $item->setIsFor($service['for']);
            $item->setIsPersonal($service['personal']);
            $item->setIsSingle($service['single']);
            $item->setAllowToOrderFormList($service['order_from_list']);
            $item->setIsSale($service['sale']);
            $item->setSaleText($service['sale_text']);
            $item->setSmsNumber($service['sms_number']);
            $item->setType($service['type']);
            $item->setVisible($service['visible']);

            $this->addReference(sprintf('rdw.service.%d_%d', $service['for'], $service['type']), $item);
            $entityManager->persist($item);
        }

        $entityManager->flush();
    }
}
