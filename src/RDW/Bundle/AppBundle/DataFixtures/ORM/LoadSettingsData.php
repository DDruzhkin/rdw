<?php

namespace RDW\Bundle\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\AppBundle\Entity\Setting;

/**
 * Class LoadSettingsData
 *
 * @package RDW\Bundle\AppBundle\DataFixtures\ORM
 */
class LoadSettingsData extends AbstractFixture
{
    /**
     * @var array
     */
    private $settings = [
        ['title' => 'VKontakte page url', 'key' => 'social_vk_link'],
        ['title' => 'Facebook page url', 'key' => 'social_fb_link'],
        ['title' => 'Odnoklassniki page url', 'key' => 'social_ok_link'],
        ['title' => 'Telegram url', 'key' => 'social_telegram_link'],
        ['title' => 'Viber url', 'key' => 'social_viber_link'],
        ['title' => 'Instagram url', 'key' => 'social_inst_link'],
        ['title' => 'Youtube url', 'key' => 'social_yo_link'],
        ['title' => 'New vacancies title', 'key' => 'new_vacancies_title'],
        ['title' => 'New vacancies icon url', 'key' => 'new_vacancies_icon'],
    ];

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $entityManager)
    {
        foreach ($this->settings as $setting) {
            $item = new Setting();
            $item->setTitle($setting['title']);
            $item->setKey($setting['key']);
            $item->setValue('');

            $entityManager->persist($item);
        }

        $entityManager->flush();
    }
}
