<?php

namespace RDW\Bundle\AppBundle;

use RDW\Bundle\AppBundle\DependencyInjection\Compiler\OverrideOneupUploaderValidatorsCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class RDWAppBundle
 *
 * @package RDW\Bundle\AppBundle
 */
class RDWAppBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new OverrideOneupUploaderValidatorsCompilerPass());
    }
}
