<?php

namespace RDW\Bundle\PaymentBundle\Controller;

use RDW\Bundle\PaymentBundle\Event\PaymentEvent;
use RDW\Bundle\PaymentBundle\Model\OrderInterface;
use RDW\Bundle\PaymentBundle\RDWPaymentEvents;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class PaymentController
 *
 * @package RDW\Bundle\PaymentBundle\Controller
 *
 * @Route("/payment")
 */
class PaymentController extends Controller
{
    /**
     * @param Request $request
     *
     * @Route("/success", name="rdw_payment.payment.success")
     * @Method({"GET"})
     *
     * @throws NotFoundHttpException

     * @return Response
     */
    public function successAction(Request $request)
    {
        $params = [
            'id' => $request->query->get('id'),
            'hash' => $request->query->get('hash'),
        ];

        $manager = $this->container->get('rdw_payment.service.payment_manager');
        $payment = $manager->getOneBy($params);

        if ( ! $payment) {
            throw new NotFoundHttpException(sprintf('Payment %s (%s) not found', $params['id'], $params['hash']));
        }

        $event = new PaymentEvent($payment);
        $this->container->get('event_dispatcher')->dispatch(RDWPaymentEvents::PAYMENT_RESPONSE_SUCCESS, $event);

        if ($response = $event->getResponse()) {
            return $response;
        }

        return $this->redirect(
            $this->generateUrl('rdw_order.order.view', [
                'id' => $payment->getOrder()->getId()
            ])
        );
    }

    /**
     * @param Request $request
     *
     * @Route("/not-success", name="rdw_payment.payment.not_success")
     *
     * @return RedirectResponse|Response
     */
    public function notSuccessAction(Request $request)
    {
        $params = [
            'id' => $request->query->get('id'),
            'hash' => $request->query->get('hash'),
        ];

        $manager = $this->container->get('rdw_payment.service.payment_manager');
        $payment = $manager->getOneBy($params);

        if ( ! $payment) {
            throw new NotFoundHttpException(sprintf('Payment %s (%s) not found', $params['id'], $params['hash']));
        }

        $event = new PaymentEvent($payment);
        $this->container->get('event_dispatcher')->dispatch(RDWPaymentEvents::PAYMENT_RESPONSE_NOT_SUCCESS, $event);

        if ($response = $event->getResponse()) {
            return $response;
        }

        return $this->redirect(
            $this->generateUrl('rdw_order.order.view', [
                'id' => $payment->getOrder()->getId()
            ])
        );
    }

    /**
     * @param Request $request
     *
     * @Route("/cancel", name="rdw_payment.payment.cancel")
     *
     * @return RedirectResponse|Response
     */
    public function cancelAction(Request $request)
    {
        $params = [
            'id' => $request->query->get('id'),
            'hash' => $request->query->get('hash'),
        ];

        $manager = $this->container->get('rdw_payment.service.payment_manager');
        $payment = $manager->getOneBy($params);

        if ( ! $payment) {
            throw new NotFoundHttpException(sprintf('Payment %s (%s) not found', $params['id'], $params['hash']));
        }

        $event = new PaymentEvent($payment);
        $this->container->get('event_dispatcher')->dispatch(RDWPaymentEvents::PAYMENT_RESPONSE_CANCEL, $event);

        if ($response = $event->getResponse()) {
            return $response;
        }

        return $this->forward('RDWPaymentBundle:Payment:notSuccess', ['request' => $request]);
    }

    /**
     * Handle response
     *
     * @param Request $request
     * @param string  $provider
     * @param int     $id
     * @param string  $hash
     *
     * @Route("/handle/{provider}/{id}/{hash}", name="rdw_payment.payment.handle")
     *
     * @return Response
     */
    public function handleAction(Request $request, $provider, $id, $hash)
    {
        // set to query @todo - fix this
        $request->query->set('id', $id);
        $request->query->set('hash', $hash);

        /** @var \RDW\Bundle\PaymentBundle\Service\HandlerManager $handlerManager */
        $handlerManager = $this->container->get('rdw_payment.service.handler_manager');

        $response = $handlerManager->handle($request, $provider);

        return $response;
    }

    /**
     * @param OrderInterface $order
     *
     * @Route("/pay/{id}", name="rdw_payment.payment.pay")
     * @ParamConverter("order", class="RDWOrderBundle:Order")
     * @Method({"GET"})
     *
     * @throws \Exception|AccessDeniedException

     * @return RedirectResponse
     */
    public function pay(OrderInterface $order)
    {
        $user = $this->getUser();

        if ( ! $user instanceof RegisteredUser || $user->getId() != $order->getUser()->getId()) {
            throw new AccessDeniedException();
        }

        $paymentType = 'webpay';
        $handlerManager = $this->container->get('rdw_payment.service.handler_manager');

        /** @var \RDW\Bundle\PaymentBundle\Handler\HandlerInterface $handler */
        $handler = $handlerManager->getHandler($paymentType);

        $paymentManager = $this->container->get('rdw_payment.service.payment_manager');
        $payment = $paymentManager->create($order, $paymentType, $handler->getDefaultCurrency());

        if ( ! $payment) {
            throw new \Exception('Error while creating payment');
        }

        $paymentManager->update($payment);

        return $handlerManager->process($payment);
    }
}
