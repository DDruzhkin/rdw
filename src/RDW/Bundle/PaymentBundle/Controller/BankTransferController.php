<?php

namespace RDW\Bundle\PaymentBundle\Controller;

use RDW\Bundle\OrderBundle\Entity\Order;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class BankTransferController
 *
 * @package RDW\Bundle\PaymentBundle\Controller
 *
 * @Route("/transfer")
 */
class BankTransferController extends Controller
{
    /**
     * @param Order $order
     *
     * @Route ("/pay/{id}", name="rdw_payment.transfer.pay")
     * @ParamConverter("order", class="RDWOrderBundle:Order")
     *
     * @return Response
     *
     * @throws NotFoundHttpException
     */
    public function payAction(Order $order)
    {
        if ( ! $order) {
            throw new NotFoundHttpException(sprintf('Order was not found'));
        }

        $paymentType = 'transfer';

        $paymentManager = $this->container->get('rdw_payment.service.payment_manager');
        $paymentManager->create($order, $paymentType, $this->container->getParameter('rdw_money.currency'), $order->getTotal());

        return $this->redirect($this->generateUrl('rdw_order.order.view', ['id' => $order->getId()]));
    }

    /**
     * @param Order $order
     *
     * @Route ("/finish/{id}", name="rdw_payment.transfer.finish")
     * @ParamConverter("order", class="RDWOrderBundle:Order")
     * @Template()
     *
     * @return Response
     *
     * @throws NotFoundHttpException
     */
    public function finishAction(Order $order)
    {
        if ( ! $order) {
            throw new NotFoundHttpException(sprintf('Order was not found'));
        }

        return [
            'order' => $order
        ];
    }
}
