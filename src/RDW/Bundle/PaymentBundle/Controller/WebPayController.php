<?php

namespace RDW\Bundle\PaymentBundle\Controller;

use RDW\Bundle\PaymentBundle\Model\PaymentInterface;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class WebPayController
 *
 * @package RDW\Bundle\PaymentBundle\Controller
 *
 * @Route("/webpay")
 */
class WebPayController extends Controller
{
    /**
     * @param PaymentInterface $payment
     *
     * @Route ("/pay/{id}", name="rdw_payment.webpay.pay")
     * @Template()
     * @ParamConverter("payment", class="RDWPaymentBundle:Payment")
     *
     * @throws AccessDeniedException
     *
     * @return Response
     */
    public function payAction(PaymentInterface $payment)
    {
        $user = $this->getUser();
        $order = $payment->getOrder();

        if ( ! $user instanceof RegisteredUser || $user->getId() != $order->getUser()->getId()) {
            throw new AccessDeniedException();
        }

        /** @var \RDW\Bundle\PaymentBundle\Handler\WebPayHandler $handler */
        $handler = $this->container->get('rdw_payment.service.handler_manager')->getHandler($payment->getProvider());

        return [
            'url' => $handler->getPostUrl(),
            'params' => $handler->getPostParams($payment),
        ];
    }
}
