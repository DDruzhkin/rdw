<?php

namespace RDW\Bundle\PaymentBundle\Model;

/**
 * Class OrderStatus
 *
 * @package RDW\Bundle\PaymentBundle\Model
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
final class OrderStatus
{
    /**
     * Order is submitted only
     */
    const STATUS_SUBMITTED = 'submitted';

    /**
     * Order is waiting response from bank
     */
    const STATUS_PENDING = 'pending';

    /**
     * Order is paid
     */
    const STATUS_PAID = 'paid';
}
