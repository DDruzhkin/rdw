<?php

namespace RDW\Bundle\PaymentBundle\Model;

/**
 * Interface PaymentResponseInterface
 *
 * @package RDW\Bundle\PaymentBundle\Model
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
interface PaymentResponseInterface
{
    /**
     * @return string
     */
    public function getStatus();

    /**
     * @return PaymentInterface
     */
    public function getPayment();

    /**
     * @return string
     */
    public function getTransactionId();

    /**
     * @return string
     */
    public function getProvider();
}
