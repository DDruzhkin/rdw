<?php

namespace RDW\Bundle\PaymentBundle\Model;

/**
 * Interface OrderInterface
 *
 * @package RDW\Bundle\PaymentBundle\Model
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
interface OrderInterface
{
    /**
     * @return integer
     */
    public function getId();

    /**
     * @return integer
     */
    public function getTotal();

    /**
     * @return string
     */
    public function getEmail();

    /**
     * @return string
     */
    public function getPhone();
}
