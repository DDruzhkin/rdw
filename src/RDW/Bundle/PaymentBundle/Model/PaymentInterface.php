<?php

namespace RDW\Bundle\PaymentBundle\Model;

/**
 * Interface PaymentInterface
 *
 * @package RDW\Bundle\PaymentBundle\Model
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
interface PaymentInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return OrderInterface
     */
    public function getOrder();

    /**
     * @return string
     */
    public function getProvider();

    /**
     * @return bool
     */
    public function isPaid();

    /**
     * @param \DateTime $paidAt
     *
     * @return $this
     */
    public function setPaidAt(\DateTime $paidAt);

    /**
     * @param string $transactionId
     *
     * @return $this
     */
    public function setTransactionId($transactionId);

    /**
     * @param string $provider
     *
     * @return $this
     */
    public function setProvider($provider);

    /**
     * @return string
     */
    public function getHash();
}
