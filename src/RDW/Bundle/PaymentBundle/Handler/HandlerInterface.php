<?php

namespace RDW\Bundle\PaymentBundle\Handler;

use RDW\Bundle\PaymentBundle\Model\OrderInterface;
use RDW\Bundle\PaymentBundle\Model\PaymentInterface;
use RDW\Bundle\PaymentBundle\Model\PaymentResponseInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Interface HandlerInterface
 *
 * @package RDW\Bundle\PaymentBundle\Handler
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
interface HandlerInterface
{
    /**
     * @param PaymentInterface $payment
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function process(PaymentInterface $payment);

    /**
     * @param Request $request
     *
     * @return PaymentResponseInterface
     */
    public function handle(Request $request);

    /**
     * @return void
     */
    public function setHandlerConfig();

    /**
     * @return string
     */
    public function getDefaultCurrency();

    /**
     * @return bool
     */
    public function isTestMode();

    /**
     * @param OrderInterface $order
     *
     * @return string
     */
    public function getOrderTitle(OrderInterface $order);
}
