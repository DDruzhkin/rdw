<?php

namespace RDW\Bundle\PaymentBundle\Handler;

use RDW\Bundle\PaymentBundle\Entity\PaymentResponse;
use RDW\Bundle\PaymentBundle\Model\OrderInterface;
use RDW\Bundle\PaymentBundle\Model\PaymentInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class WebPayHandler extends AbstractHandler
{
    const STATUS_COMPLETED  = 1; // OK
    const STATUS_DECLINED   = 2;
    const STATUS_PENDING    = 3;
    const STATUS_AUTHORIZED = 4; // OK
    const STATUS_REFUNDED   = 5; // NOK
    const STATUS_SYSTEM     = 6;
    const STATUS_VOIDED     = 7; // NOK

    /**
     * @var string
     */
    private $handler = 'webpay';

    /**
     * {@inheritdoc}
     */
    public function setHandlerConfig()
    {
        if ( ! isset($this->config['handlers'][$this->handler])) {
            throw new \Exception(sprintf('Handler config %s is not defined', $this->handler));
        }

        $this->handlerConfig = $this->config['handlers'][$this->handler];
    }

    /**
     * {@inheritdoc}
     */
    public function process(PaymentInterface $payment)
    {
        $url = $this->router->generate('rdw_payment.webpay.pay', ['id' => $payment->getId()], true);

        return new RedirectResponse($url);
    }

    /**
     * {@inheritdoc}
     */
    public function getOrderTitle(OrderInterface $order)
    {
        return sprintf('Payment for order #%s', $order->getId());
    }

    /**
     * {@inheritdoc}
     */
    public function handle(Request $request)
    {
        $this->logger->info(sprintf('Handler %s->handle()', get_class($this)));

        // emulate some params
        /*$request->request->set('batch_timestamp', '1422858663');
        $request->request->set('currency_id', 'BYR');
        $request->request->set('amount', '55000');
        $request->request->set('payment_method', 'test');
        $request->request->set('order_id', '24358');
        $request->request->set('site_order_id', '116');
        $request->request->set('transaction_id', '734829718');
        $request->request->set('payment_type', '4');
        $request->request->set('rrn', '142285868023');
        $request->request->set('action', '0');
        $request->request->set('rc', 'W0269(00)');
        $request->request->set('approval', '000000');
        $request->request->set('wsb_signature', '026a93a8000038c3cf07914a0c9321b9');

        $request->query->set('id', 22);
        $request->query->set('hash', '6a1c315886bf88cf3dea6c9f8549f6bfef9663c2');*/

        $paymentId = $request->query->get('id');
        $paymentHash = $request->query->get('hash');

        $payment = $this->paymentManager->getOneBy(['id' => $paymentId, 'hash' => $paymentHash]);

        if ( ! $payment) {
            $this->logger->info('Payment not found', ['payment' => $payment]);

            throw new \Exception(sprintf('Payment not found. Params: id => %s, hash => %s', $paymentId, $paymentHash));
        }

        // check if response is from valid sender
        if ( ! $this->isResponseSignValid($request)) {
            $this->logger->info('Response sign is invalid. ', [
                'calculatedSignature' => $this->createResponseSignature($request),
                'responseSignature' => $request->request->get('wsb_signature')
            ]);

            throw new \Exception(
                sprintf(
                    'Payment response sign is not valid. Params: transaction_id => %s, IP => %s',
                    $request->request->get('transaction_id'),
                    $request->getClientIp()
                )
            );
        }

        // at this moment, we're interested only if status is OK
        switch ($request->request->get('payment_type')) {
            case self::STATUS_COMPLETED:
            case self::STATUS_AUTHORIZED:
                $status = PaymentResponse::PAYMENT_RESPONSE_STATUS_SUCCESS;
                break;
            default:
                $status = PaymentResponse::PAYMENT_RESPONSE_STATUS_UNSUCCESSFUL;
        }

        $this->logger->info('Response valid.', ['status' => $status]);

        $responseJson = json_encode($request->request->all());

        $paymentResponse = new PaymentResponse();
        $paymentResponse->setPayment($payment);
        $paymentResponse->setProvider($this->handler);
        $paymentResponse->setResponse($responseJson);
        $paymentResponse->setStatus($status);
        $paymentResponse->setTransactionId($request->request->get('transaction_id'));

        return $paymentResponse;
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    private function isResponseSignValid(Request $request)
    {
        $signature = $this->createResponseSignature($request);

        return ($request->request->get('wsb_signature') == $signature);
    }

    private function createResponseSignature(Request $request)
    {
        return md5(
            $request->request->get('batch_timestamp')
            . $request->request->get('currency_id')
            . $request->request->get('amount')
            . $request->request->get('payment_method')
            . $request->request->get('order_id')
            . $request->request->get('site_order_id')
            . $request->request->get('transaction_id')
            . $request->request->get('payment_type')
            . $request->request->get('rrn')
            . $this->handlerConfig['secret']
        );
    }

    /**
     * @param PaymentInterface $payment
     *
     * @return array
     */
    public function getPostParams(PaymentInterface $payment)
    {
        $urlParams = [
            'provider' => $this->handler,
            'id' => $payment->getId(),
            'hash' => $payment->getHash(),
        ];

        $params = [
            'wsb_test' => $this->handlerConfig['test_mode'],
            'wsb_storeid' => $this->handlerConfig['store_id'],
            'wsb_store' => $this->handlerConfig['store_name'],
            'wsb_order_num' => $payment->getOrder()->getId(),
            'wsb_currency_id' => $this->handlerConfig['currency'],
            'wsb_version' => $this->handlerConfig['version'],
            'wsb_language_id' => $this->handlerConfig['language'],
            'wsb_seed' => microtime() . $payment->getOrder()->getId(),
            'wsb_return_url' => $this->router->generate($this->config['url_ok'], $urlParams, true),
            'wsb_cancel_return' => $this->router->generate($this->config['url_cancel'], $urlParams, true),
            'wsb_notify_url' => $this->router->generate($this->config['url_ack'], $urlParams, true),
            'wsb_email' => $payment->getOrder()->getEmail(),
            'wsb_phone' => $payment->getOrder()->getPhone(),
        ];

        // order items
        $params['wsb_invoice_item_name[]'] = $this->getOrderTitle($payment->getOrder());
        $params['wsb_invoice_item_quantity[]'] = '1';
        $params['wsb_invoice_item_price[]'] = $payment->getOrder()->getTotal();
        $params['wsb_total'] = $payment->getOrder()->getTotal();

        $params['wsb_signature'] = sha1(
            $params['wsb_seed']
            . $params['wsb_storeid']
            . $params['wsb_order_num']
            . $params['wsb_test']
            . $params['wsb_currency_id']
            . $params['wsb_total']
            . $this->handlerConfig['secret']
        );

        return $params;
    }

    /**
     * @return string
     */
    public function getPostUrl()
    {
        return $this->handlerConfig['test_mode']
            ? $this->handlerConfig['post_url_test']
            : $this->handlerConfig['post_url'];
    }

    /**
     * @return {@inheritdoc}
     */
    public function isTestMode()
    {
        return $this->handlerConfig['test_mode'];
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultCurrency()
    {
        return $this->handlerConfig['currency'];
    }
}
