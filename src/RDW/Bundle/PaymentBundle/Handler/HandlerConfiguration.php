<?php

namespace RDW\Bundle\PaymentBundle\Handler;

/**
 * Class HandlerConfiguration
 *
 * @package RDW\Bundle\PaymentBundle\Handler
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class HandlerConfiguration
{
    /**
     * @var array
     */
    protected $handlers = [];

    /**
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        if (isset($config['handlers'])) {
            $this->handlers = $config['handlers'];
        }
    }

    /**
     * @param string $handler
     *
     * @return array
     * @throws \Exception
     */
    public function get($handler)
    {
        if (empty($this->handlers[$handler])) {
            throw new \Exception(sprintf('Handler "%s" is not defined'));
        }

        return $this->handlers[$handler];
    }

    /**
     * @param string $handler
     * @param array  $handlerConfig
     *
     * @return array
     */
    public function set($handler, array $handlerConfig)
    {
        return $this->handlers[$handler] = $handlerConfig;
    }
}
