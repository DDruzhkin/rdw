<?php

namespace RDW\Bundle\PaymentBundle\Handler;

use RDW\Bundle\PaymentBundle\Model\PaymentInterface;
use RDW\Bundle\PaymentBundle\Service\PaymentManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bridge\Monolog\Logger;

abstract class AbstractHandler implements HandlerInterface
{
    /**
     * @var array
     */
    protected $handlerConfig;

    /**
     * @var
     */
    protected $config;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\Routing\Router
     */
    protected $router;

    /**
     * @var PaymentManager
     */
    protected $paymentManager;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @param Router         $router
     * @param PaymentManager $paymentManager
     * @param array          $config
     */
    public function __construct(
        \Symfony\Cmf\Component\Routing\ChainRouter $router,
        PaymentManager $paymentManager,
        Logger $logger,
        array $config
    )
    {
        $this->router = $router;
        $this->paymentManager = $paymentManager;
        $this->logger = $logger;
        $this->config = $config;
    }

    /**
     * {@inheritdoc}
     */
    public function process(PaymentInterface $payment)
    {
        $class = get_class($this);
        throw new Exception("The process method is not implemented in the $class handler");
    }

    /**
     * {@inheritdoc}
     */
    public function handle(Request $request)
    {
        $class = get_class($this);
        throw new Exception("The handle method is not implemented in the $class handler");
    }
}
