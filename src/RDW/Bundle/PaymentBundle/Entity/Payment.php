<?php

namespace RDW\Bundle\PaymentBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\PaymentBundle\Model\PaymentInterface;
use RDW\Bundle\PaymentBundle\Model\PaymentResponseInterface;
use RDW\Bundle\PaymentBundle\Model\OrderInterface;

/**
 * Payment
 *
 * @ORM\Table("payments")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Payment implements PaymentInterface
{
    const PAYMENT_PROVIDER_WEBPAY = 'webpay';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var OrderInterface
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\OrderBundle\Entity\Order", inversedBy="payments")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $order;

    /**
     * @var string
     *
     * @ORM\Column(name="provider", type="string", length=255)
     */
    private $provider;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="paid_at", type="datetime", nullable=true)
     */
    private $paidAt;

    /**
     * @var string
     *
     * @ORM\Column(name="transaction_id", type="string", length=255, nullable=true)
     */
    private $transactionId;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\PaymentBundle\Entity\PaymentResponse", mappedBy="payment")
     */
    private $responses;

    /**
     * @var float
     *
     * @ORM\Column(name="amount_total", type="decimal", precision=16, scale=2)
     */
    private $amountTotal;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=255, nullable=true)
     */
    protected $currency;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=255)
     */
    protected $hash;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->responses = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \RDW\Bundle\PaymentBundle\Model\OrderInterface
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return string
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param string $provider
     *
     * @return $this
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return boolean
     */
    public function isPaid()
    {
        return (null != $this->getPaidAt());
    }

    /**
     * @param \DateTime $paidAt
     *
     * @return $this
     */
    public function setPaidAt(\DateTime $paidAt = null)
    {
        $this->paidAt = $paidAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPaidAt()
    {
        return $this->paidAt;
    }

    /**
     * @param string $transactionId
     *
     * @return $this
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;

        return $this;
    }

    /**
     * @return string
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * @param OrderInterface $order
     *
     * @return $this
     */
    public function setOrder(OrderInterface $order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @param PaymentResponseInterface $response
     *
     * @return $this
     */
    public function addResponse(PaymentResponseInterface $response)
    {
        $this->responses[] = $response;

        return $this;
    }

    /**
     * @param PaymentResponseInterface $response
     */
    public function removeResponse(PaymentResponseInterface $response)
    {
        $this->responses->removeElement($response);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResponses()
    {
        return $this->responses;
    }

    /**
     * @return array
     */
    public static function getProviders()
    {
        $providers = [
            self::PAYMENT_PROVIDER_WEBPAY => 'WebPay',
        ];

        return $providers;
    }

    /**
     * @param string $provider
     *
     * @return string
     */
    public function getProviderLabel($provider)
    {
        $providers = $this->getProviders();

        if (isset($providers[$provider])) {
            return $providers[$provider];
        }

        return 'Undefined provider';
    }

    /**
     * @param string $currency
     *
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param int $amountTotal
     *
     * @return $this
     */
    public function setAmountTotal($amountTotal)
    {
        $this->amountTotal = $amountTotal;

        return $this;
    }

    /**
     * @return int
     */
    public function getAmountTotal()
    {
        return $this->amountTotal;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @ORM\PrePersist
     */
    public function setHashValue()
    {
        $this->hash = sha1(sha1($this->getOrder()->getId()) . sha1($this->getProvider()));
    }
}
