<?php

namespace RDW\Bundle\PaymentBundle\Service;

use Symfony\Bridge\Monolog\Logger;
use RDW\Bundle\PaymentBundle\RDWPaymentEvents;
use RDW\Bundle\PaymentBundle\Event\PaymentEvent;
use RDW\Bundle\PaymentBundle\Handler\HandlerInterface;
use RDW\Bundle\PaymentBundle\Handler\HandlerConfiguration;
use RDW\Bundle\PaymentBundle\Model\PaymentInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HandlerManager
{
    const STATUS_WAITING = 'waiting';
    const STATUS_SUCCESS = 'success';
    const STATUS_NOT_SUCCESS = 'not_success';
    const STATUS_NOT_CANCELED = 'canceled';

    /**
     * @var HandlerConfiguration
     */
    protected $handlerConfig;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @param ContainerInterface       $container
     * @param HandlerConfiguration     $handlerConfig
     * @param EventDispatcherInterface $eventDispatcher
     * @param Logger                   $logger
     */
    public function __construct(
        ContainerInterface $container,
        HandlerConfiguration $handlerConfig,
        EventDispatcherInterface $eventDispatcher,
        Logger $logger
    )
    {
        $this->container = $container;
        $this->handlerConfig = $handlerConfig;
        $this->eventDispatcher = $eventDispatcher;
        $this->logger = $logger;
    }

    /**
     * @param string $provider
     *
     * @return HandlerInterface
     * @throws \Exception
     */
    public function getHandler($provider)
    {
        $handlerConfig = $this->getHandlerConfiguration()->get($provider);

        if ( ! $this->container->has($handlerConfig['service'])) {
            throw new \Exception(sprintf('Handler service "%s" was not found', $handlerConfig['service']));
        }

        /** @var HandlerInterface $handler */
        $handler = $this->container->get($handlerConfig['service']);
        $handler->setHandlerConfig();

        return $handler;
    }

    /**
     * @param PaymentInterface $payment
     *
     * @return RedirectResponse
     */
    public function process(PaymentInterface $payment)
    {
        $response = $this->getHandler($payment->getProvider())->process($payment);

        return $response;
    }

    /**
     * @param Request $request
     * @param string  $provider
     *
     * @return Response
     * @throws \Exception
     */
    public function handle(Request $request, $provider)
    {
        $handler = $this->getHandler($provider);

        $this->logger->info('Payment handle: Started', ['request' => $request->request, 'query' => $request->query]);

        $paymentResponse = $handler->handle($request);

        if ( ! $paymentResponse) {
            $this->logger->info('No payment response', ['paymentResponse' => $paymentResponse]);

            throw new \Exception('No payment response');
        }

        $paymentResponse = $this->getPaymentResponseManager()->update($paymentResponse);

        $logParams = [
            'provider' => $paymentResponse->getProvider(),
            'id' => $paymentResponse->getId(),
            'response' => $paymentResponse->getResponse(),
        ];
        $this->logger->info('Payment handle: PaymentResponse', $logParams);

        $payment = $paymentResponse->getPayment();

        if ( ! $payment->isPaid() && self::STATUS_SUCCESS === $paymentResponse->getStatus()) {
            $payment->setTransactionId($paymentResponse->getTransactionId());
            $payment->setPaidAt(new \DateTime());
            $payment->setProvider($paymentResponse->getProvider());

            $this->getPaymentManager()->update($payment);

            $this->logger->info('Payment paid!', ['payment' => $payment]);

            $event = new PaymentEvent($payment);
            $this->eventDispatcher->dispatch(RDWPaymentEvents::PAYMENT_HANDLE_COMPLETE, $event);
        }

        return new Response('OK');
    }

    /**
     * Get handler configuration
     *
     * @return HandlerConfiguration
     */
    public function getHandlerConfiguration()
    {
        return $this->handlerConfig;
    }

    /**
     * @return \RDW\Bundle\PaymentBundle\Service\PaymentManager
     */
    public function getPaymentManager()
    {
        return $this->container->get('rdw_payment.service.payment_manager');
    }

    /**
     * @return \RDW\Bundle\PaymentBundle\Service\PaymentResponseManager
     */
    public function getPaymentResponseManager()
    {
        return $this->container->get('rdw_payment.service.payment_response_manager');
    }
}
