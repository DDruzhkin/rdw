<?php

namespace RDW\Bundle\PaymentBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\PaymentBundle\Entity\Payment;
use RDW\Bundle\PaymentBundle\Model\OrderInterface;
use RDW\Bundle\PaymentBundle\Model\PaymentInterface;

/**
 * Class PaymentManager
 *
 * @package RDW\Bundle\PaymentBundle\Service
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class PaymentManager
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    protected $objectManager;

    /**
     * Constructor
     *
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * Create payment
     *
     * @param OrderInterface $order
     * @param string         $paymentType
     * @param string         $paymentCurrency
     *
     * @return PaymentInterface
     */
    public function create(OrderInterface $order, $paymentType, $paymentCurrency)
    {
        $payment = new Payment();
        $payment->setOrder($order);
        $payment->setProvider($paymentType);
        $payment->setCurrency($paymentCurrency);
        $payment->setAmountTotal($order->getTotal());

        $this->objectManager->persist($payment);
        $this->objectManager->flush();

        return $payment;
    }

    /**
     * @param PaymentInterface $payment
     * @param bool             $andFlush
     *
     * @return PaymentInterface
     */
    public function update(PaymentInterface $payment, $andFlush = true)
    {
        $this->objectManager->persist($payment);

        if ($andFlush) {
            $this->objectManager->flush();
        }

        return $payment;
    }

    /**
     * @param array $params
     *
     * @return PaymentInterface
     */
    public function getOneBy(array $params)
    {
        return $this->objectManager->getRepository('RDWPaymentBundle:Payment')->findOneBy($params);
    }

    /**
     * @param int $id
     *
     * @return PaymentInterface
     */
    public function getById($id)
    {
        return $this->objectManager->getRepository('RDWPaymentBundle:Payment')->find($id);
    }
}
