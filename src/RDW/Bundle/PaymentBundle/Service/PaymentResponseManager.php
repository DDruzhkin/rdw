<?php

namespace RDW\Bundle\PaymentBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\PaymentBundle\Model\PaymentResponseInterface;

/**
 * Class PaymentResponseManager
 *
 * @package RDW\Bundle\PaymentBundle\Service
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class PaymentResponseManager
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    protected $objectManager;

    /**
     * Constructor
     *
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param PaymentResponseInterface $paymentResponse
     * @param bool                     $andFlush
     *
     * @return PaymentResponseInterface
     */
    public function update(PaymentResponseInterface $paymentResponse, $andFlush = true)
    {
        $this->objectManager->persist($paymentResponse);

        if ($andFlush) {
            $this->objectManager->flush();
        }

        return $paymentResponse;
    }
}
