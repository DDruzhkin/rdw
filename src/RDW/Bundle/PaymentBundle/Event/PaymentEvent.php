<?php

namespace RDW\Bundle\PaymentBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use RDW\Bundle\PaymentBundle\Model\PaymentInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PaymentEvent
 *
 * @package RDW\Bundle\PaymentBundle\Event
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class PaymentEvent extends Event
{
    /**
     * @var PaymentInterface
     */
    private $payment;

    /**
     * @var RedirectResponse|Response
     */
    private $response;

    /**
     * @param PaymentInterface $payment
     */
    public function __construct(PaymentInterface $payment)
    {
        $this->payment = $payment;
    }

    /**
     * @return PaymentInterface
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @return Response|RedirectResponse
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param Response|RedirectResponse $response
     *
     * @return $this
     */
    public function setResponse($response)
    {
        $this->response = $response;

        return $this;
    }
}
