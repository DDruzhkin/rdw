<?php

namespace RDW\Bundle\PaymentBundle\EventListener;

use RDW\Bundle\MailerBundle\Service\MailerManager;
use RDW\Bundle\OrderBundle\Entity\Order;
use RDW\Bundle\OrderBundle\RDWOrderStatus;
use RDW\Bundle\OrderBundle\Service\OrderItemManager;
use RDW\Bundle\OrderBundle\Service\OrderManager;
use RDW\Bundle\PaymentBundle\Event\PaymentEvent;
use RDW\Bundle\PaymentBundle\RDWPaymentEvents;
use RDW\Bundle\WalletBundle\Service\WalletManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;

class PaymentCompletedListener implements EventSubscriberInterface
{
    /**
     * @var \RDW\Bundle\OrderBundle\Service\OrderManager
     */
    private $orderManager;

    /**
     * @var MailerManager
     */
    private $mailerManager;

    /**
     * @var WalletManager
     */
    private $walletManager;

    /**
     * @var \RDW\Bundle\OrderBundle\Repository\OrderRepository
     */
    private $orderRepository;

    /**
     * @param OrderManager     $orderManager
     * @param MailerManager    $mailerManager
     * @param WalletManager    $walletManager
     * @param OrderItemManager $orderItemManager
     */
    public function __construct(
        OrderManager $orderManager,
        MailerManager $mailerManager,
        WalletManager $walletManager,
        OrderItemManager $orderItemManager
    )
    {
        $this->orderManager     = $orderManager;
        $this->mailerManager    = $mailerManager;
        $this->walletManager    = $walletManager;
        $this->orderItemManager = $orderItemManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            RDWPaymentEvents::PAYMENT_MARK_AS_PAID => 'onPaymentSuccess',
            RDWPaymentEvents::PAYMENT_HANDLE_COMPLETE => 'onPaymentSuccess',
        ];
    }

    /**
     * @param PaymentEvent $event
     *
     * @throws UnexpectedTypeException
     */
    public function onPaymentSuccess(PaymentEvent $event)
    {
        $payment = $event->getPayment();
        $order = $payment->getOrder();

        if (! $order instanceof Order) {
            throw new UnexpectedTypeException(
                $order,
                'RDW\Bundle\OrderBundle\Entity\OrderInterface'
            );
        }

        // set order is paid
        $user = $order->getUser();
        $paidAt = new \DateTime();
        $payment->setPaidAt($paidAt);
        $order->setPaidAt($paidAt);
        $order->setStatus(RDWOrderStatus::STATUS_PAID);
        $order->setSuccessfulPayment($payment);

        // order paid success add money to user wallet if order was not paid from wallet
        if (in_array($order->getPaymentType(), Order::getPaymentTypesToAddMoneyToWallet())) {
            $this->walletManager->add($user, $order->getTotal(), sprintf('Payment for order %s', $order->getId()));
        }

        // activate services for user
        $this->orderItemManager->activateServicesForUser($order->getItems(), $user);
        $this->orderItemManager->setPaidAtForAll($order);

        // services is activated, deposit money from wallet
        if ($order->getType() == Order::TYPE_CALCULATOR) {
            $this->walletManager->add($user, -$order->getTotal(), 'Services is activated');
        }

        $this->mailerManager->sendOrderPaidEmailForUser($order);

        $this->orderManager->update($order);
    }
}
