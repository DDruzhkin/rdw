<?php

namespace RDW\Bundle\PaymentBundle;

/**
 * Class RDWPaymentEvents
 *
 * @package RDW\Bundle\PaymentBundle
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class RDWPaymentEvents
{
    const PAYMENT_RESPONSE_SUCCESS = 'rdw_payment.event.response_success';
    const PAYMENT_RESPONSE_NOT_SUCCESS = 'rdw_payment.event.response_not_success';
    const PAYMENT_RESPONSE_CANCEL = 'rdw_payment.event.response_cancel';

    const PAYMENT_PROCESS_INIT = 'rdw_payment.event.process_init';

    const PAYMENT_HANDLE_INIT = 'rdw_payment.event.handle_init';
    const PAYMENT_HANDLE_PRE_COMPLETE = 'rdw_payment.event.handle_pre_complete';
    const PAYMENT_HANDLE_COMPLETE = 'rdw_payment.event.handle_complete';

    const PAYMENT_MARK_AS_PAID = 'rdw_payement.event.mark_as_paid';
}
