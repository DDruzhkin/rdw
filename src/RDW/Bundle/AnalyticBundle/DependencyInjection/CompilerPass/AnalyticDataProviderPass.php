<?php

namespace RDW\Bundle\AnalyticBundle\DependencyInjection\CompilerPass;

use RDW\Component\DependencyInjection\Compiler\TaggedServicesCompilerPassTrait;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class AnalyticDataProviderPass implements CompilerPassInterface
{
    use TaggedServicesCompilerPassTrait;

    const SERVICE_TAG = 'rdw_analytic.data_provider';
    const REGISTRY_SERVICE_NAME = 'rdw_analytic.registry';

    /**
     * {@inheritDoc}
     */
    public function process(ContainerBuilder $container)
    {
        $this->registerTaggedServices($container, self::REGISTRY_SERVICE_NAME, self::SERVICE_TAG, 'addProvider');
    }
}
