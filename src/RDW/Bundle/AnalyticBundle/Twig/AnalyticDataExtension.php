<?php

namespace RDW\Bundle\AnalyticBundle\Twig;

use RDW\Bundle\AnalyticBundle\Model\AnalyticDataModel;
use RDW\Bundle\AnalyticBundle\Registry\AnalyticDataRegistry;
use Symfony\Component\VarDumper\VarDumper;

class AnalyticDataExtension extends \Twig_Extension
{
    /** @var bool */
    protected $enabled = true;

    /** @var AnalyticDataRegistry */
    protected $registry;

    /**
     * @param AnalyticDataRegistry $registry
     */
    public function __construct(AnalyticDataRegistry $registry)
    {
        $this->registry = $registry;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('is_analytic_enabled', [$this, 'isAnalyticEnabled']),
            new \Twig_SimpleFunction('get_analytic_data', [$this, 'getAnalyticData']),
        );
    }

    /**
     * @return bool
     */
    public function isAnalyticEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param bool $value
     *
     * @return $this
     */
    public function setAnalyticEnabled($value)
    {
        $this->enabled = $value;

        return $this;
    }

    /**
     * @param $uri
     *
     * @return AnalyticDataModel
     */
    public function getAnalyticData($uri)
    {
        return $this->registry->getData($uri);
    }
}
