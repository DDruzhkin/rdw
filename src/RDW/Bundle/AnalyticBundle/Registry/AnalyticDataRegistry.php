<?php

namespace RDW\Bundle\AnalyticBundle\Registry;

use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\Cache\CacheProvider;
use RDW\Bundle\AnalyticBundle\Model\AnalyticDataModel;
use RDW\Bundle\AnalyticBundle\Model\AnalyticDataProviderInterface;

class AnalyticDataRegistry
{
    /** @var array|AnalyticDataProviderInterface[] */
    protected $providers = [];

    /** @var CacheProvider */
    protected $cacheProvider;

    /**
     * AnalyticDataRegistry constructor.
     *
     * @param CacheProvider|null $cacheProvider
     */
    public function __construct(CacheProvider $cacheProvider = null)
    {
        $this->cacheProvider = $cacheProvider ?: new ArrayCache();
    }

    /**
     * @param AnalyticDataProviderInterface $provider
     * @param $alias
     *
     * @return $this
     */
    public function addProvider(AnalyticDataProviderInterface $provider, $alias)
    {
        $this->providers[$alias] = $provider;

        return $this;
    }

    /**
     * @param string $uri
     *
     * @return AnalyticDataModel
     */
    public function getData($uri)
    {
//        if ($this->cacheProvider->contains(md5($uri))) {
//            return $this->cacheProvider->fetch(md5($uri));
//        }

        $data = new AnalyticDataModel();

        foreach ($this->providers as $provider) {
            if ($provider->isUriSupported($uri)) {
                $provider->fillData($uri, $data);
            }
        }

        $this->cacheProvider->save(md5($uri), $data);

        return $data;
    }
}
