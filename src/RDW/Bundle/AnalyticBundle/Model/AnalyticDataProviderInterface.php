<?php

namespace RDW\Bundle\AnalyticBundle\Model;

interface AnalyticDataProviderInterface
{
    /**
     * @param $uri
     *
     * @return bool
     */
    public function isUriSupported($uri);

    /**
     * @param string $uri
     * @param AnalyticDataModel $dataModel
     */
    public function fillData($uri, AnalyticDataModel $dataModel);
}
