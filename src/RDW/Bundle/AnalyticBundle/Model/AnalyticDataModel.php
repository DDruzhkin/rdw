<?php

namespace RDW\Bundle\AnalyticBundle\Model;

class AnalyticDataModel
{
    /** @var string */
    protected $uri;

    /** @var int */
    protected $viewsCount = 0;

    /** @var int */
    protected $viewsUniqueCount = 0;

    /**
     * seconds
     *
     * @var float
     */
    protected $averageSpentTime = 0;

    /** @var int */
    protected $contactsShown = 0;

    /** @var int */
    protected $replied = 0;

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     *
     * @return $this
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * @return int
     */
    public function getViewsCount()
    {
        return $this->viewsCount;
    }

    /**
     * @param int $viewsCount
     *
     * @return $this
     */
    public function setViewsCount($viewsCount)
    {
        $this->viewsCount = $viewsCount;

        return $this;
    }

    /**
     * @return int
     */
    public function getViewsUniqueCount()
    {
        return $this->viewsUniqueCount;
    }

    /**
     * @param int $viewsUniqueCount
     *
     * @return $this
     */
    public function setViewsUniqueCount($viewsUniqueCount)
    {
        $this->viewsUniqueCount = $viewsUniqueCount;

        return $this;
    }

    /**
     * @return float
     */
    public function getAverageSpentTime()
    {
        return $this->averageSpentTime;
    }

    /**
     * @param float $averageSpentTime
     *
     * @return $this
     */
    public function setAverageSpentTime($averageSpentTime)
    {
        $this->averageSpentTime = $averageSpentTime;

        return $this;
    }

    /**
     * @return int
     */
    public function getContactsShown()
    {
        return $this->contactsShown;
    }

    /**
     * @param int $contactsShown
     *
     * @return $this
     */
    public function setContactsShown($contactsShown)
    {
        $this->contactsShown = $contactsShown;

        return $this;
    }

    /**
     * @return int
     */
    public function getReplied()
    {
        return $this->replied;
    }

    /**
     * @param int $replied
     *
     * @return $this
     */
    public function setReplied($replied)
    {
        $this->replied = $replied;

        return $this;
    }
}
