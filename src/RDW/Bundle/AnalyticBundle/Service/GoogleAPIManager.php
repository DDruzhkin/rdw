<?php

namespace RDW\Bundle\AnalyticBundle\Service;

use Symfony\Component\Security\Acl\Exception\Exception;

class GoogleAPIManager
{
    /** @var \Google_Service_Analytics */
    protected $analytics;

    /** @var string */
    protected $profileId;

    /** @var array */
    protected $scopes;

    /** @var string */
    protected $accCredentialsFile;

    /**
     * GoogleAPIManager constructor.
     *
     * @param string $accCredentialsFile
     * @param array $scopes
     *
     * @throws Exception
     */
    public function __construct(
        $accCredentialsFile,
        array $scopes = ['https://www.googleapis.com/auth/analytics.readonly']
    ) {
        $this->scopes = $scopes;
        $this->accCredentialsFile = $accCredentialsFile;
    }

    /**
     * @return \Google_Service_Analytics
     */
    protected function initializeAnalytics()
    {
        if (!$this->analytics) {
            if (!is_readable($this->accCredentialsFile)) {
                throw new \RuntimeException(
                    sprintf('Unable to read credentials file, got "%s"', $this->accCredentialsFile)
                );
            }

            // Create and configure a new client object.
            $client = new \Google_Client();
            $client->setApplicationName("RDW Analytics Reporting");
            $client->setAuthConfig($this->accCredentialsFile);
            $client->setScopes($this->scopes);
            $this->analytics = new \Google_Service_Analytics($client);
        }

        return $this->analytics;
    }

    /**
     * Get the user's first view (profile) ID.
     *
     * @return mixed
     */
    protected function getFirstProfileId()
    {
        if ($this->profileId) {
            return $this->profileId;
        }

        // Get the list of accounts for the authorized user.
        /** @var \Google_Service_Analytics_Accounts $accounts */
        $accounts = $this->analytics->management_accounts->listManagementAccounts();

        if ($accounts->count() > 0) {
            /** @var \Google_Service_Analytics_Account[] $items */
            $items = $accounts->getItems();
            $firstAccountId = $items[0]->getId();

            // Get the list of properties for the authorized user.
            $properties = $this->analytics->management_webproperties
                ->listManagementWebproperties($firstAccountId);

            if (count($properties->getItems()) > 0) {
                $items = $properties->getItems();
                $firstPropertyId = $items[0]->getId();

                // Get the list of views (profiles) for the authorized user.
                $profiles = $this->analytics
                    ->management_profiles
                    ->listManagementProfiles($firstAccountId, $firstPropertyId);

                if (count($profiles->getItems()) > 0) {
                    $items = $profiles->getItems();

                    $this->profileId = $items[0]->getId();

                    return $this->profileId;
                } else {
                    throw new Exception('No views (profiles) found for this user.');
                }
            } else {
                throw new Exception('No properties found for this user.');
            }
        } else {
            throw new Exception('No accounts found for this user.');
        }
    }

    public function init()
    {
        try {
            $this->initializeAnalytics();
            $this->getFirstProfileId();
        } catch (Exception $e) {
            throw new Exception('Error connect for Google API');
        }
    }

    /**
     * @param $uri
     *
     * @return object
     */
    public function getStatistic($uri)
    {
        $this->init();

        $statistic = $this->analytics->data_ga->get(
            'ga:' . $this->profileId,
            '90daysAgo',
            'today',
            'ga:sessions,ga:pageviews,ga:uniquePageviews,ga:avgTimeOnPage',
            [
                'filters' => 'ga:pagePath==' . $uri,
                'dimensions' => 'ga:pagePath,ga:source,ga:medium',
                'metrics' => 'ga:pageviews,ga:uniquePageviews,ga:avgTimeOnPage'
            ]);

        return $statistic->getTotalsForAllResults();
    }
}
