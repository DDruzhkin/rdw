<?php

namespace RDW\Bundle\AnalyticBundle;

use RDW\Bundle\AnalyticBundle\DependencyInjection\CompilerPass\AnalyticDataProviderPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class RDWAnalyticBundle extends Bundle
{
    /**
     * {@inheritDoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new AnalyticDataProviderPass());
    }
}
