<?php
namespace RDW\Bundle\AnalyticBundle\Provider;

use RDW\Bundle\AnalyticBundle\Model\AnalyticDataModel;
use RDW\Bundle\AnalyticBundle\Model\AnalyticDataProviderInterface;
use RDW\Bundle\AnalyticBundle\Service\GoogleAPIManager;

class GoogleAnalyticDataProvider implements AnalyticDataProviderInterface
{
    /** @var GoogleAPIManager */
    protected $googleAPIManager;

    /**
     * @param GoogleAPIManager $googleAPIManager
     */
    public function __construct(GoogleAPIManager $googleAPIManager)
    {
        $this->googleAPIManager = $googleAPIManager;
    }

    /**
     * {@inheritDoc}
     */
    public function isUriSupported($uri)
    {
        try {
            $this->googleAPIManager->init();

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function fillData($uri, AnalyticDataModel $dataModel)
    {
        $googleData = $this->googleAPIManager->getStatistic($uri);

        $dataModel
            ->setUri($uri)
            ->setViewsUniqueCount($googleData['ga:uniquePageviews'])
            ->setViewsCount($googleData['ga:pageviews'])
            ->setAverageSpentTime(round($googleData['ga:avgTimeOnPage']));
    }
}
