<?php

namespace RDW\Bundle\JobBundle\ImportExport;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use RDW\Bundle\AppBundle\Entity\Transliterator;
use RDW\Bundle\ImportExportBundle\Model\ImportModelInterface;
use RDW\Bundle\ImportExportBundle\Registry\ImportProcessorInterface;
use RDW\Bundle\JobBundle\Entity\Position;
use RDW\Bundle\JobBundle\Entity\ProfessionalArea;
use RDW\Bundle\JobBundle\Model\ImportExport\ProfessionalAreaImportModel;
use RDW\Bundle\JobBundle\Repository\ProfessionalAreaRepository;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class ProfessionalAreaImportProcessor implements ImportProcessorInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var Registry
     */
    protected $doctrine;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var ObjectRepository|ProfessionalAreaRepository
     */
    protected $repositoryPA;

    /**
     * @var ObjectRepository|ProfessionalAreaRepository
     */
    protected $repositoryPosition;

    /** @var LoggerInterface */
    protected $logger;

    public function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
        $this->em = $doctrine->getManager();
        $this->repositoryPA = $doctrine->getRepository(ProfessionalArea::class);
        $this->repositoryPosition = $doctrine->getRepository(Position::class);
        $this->logger = new NullLogger();
    }

    /**
     * {@inheritdoc}
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function getDescription()
    {
        return "";
    }

    /**
     * @param $source
     *
     * @return \PHPExcel
     */
    protected function negotiatePhpExcelReader($source)
    {
        $readers = [
            new \PHPExcel_Reader_Excel5(),
            new \PHPExcel_Reader_Excel2003XML(),
            new \PHPExcel_Reader_Excel2007(),
        ];
        /** @var \PHPExcel_Reader_Abstract $reader */
        foreach ($readers as $reader) {
            if ($reader->canRead($source)) {
                return $reader->load($source);
            }
        }

        throw new \RuntimeException(sprintf('Reader was not found for "%s"', $source));
    }

    /**
     * @inheritDoc
     */
    public function processSource($source)
    {
        $records = [];

        try {
            $locatedSource = $this->container->get('kernel')->locateResource($source);
        } catch (\Exception $e) {
            $locatedSource = null;
        }

        if ($locatedSource) {
            $source = $locatedSource;
        }
        if (!is_file($source) || !is_readable($source)) {
            $message = sprintf('Unable to access file "%s"', $source);
            $this->logger->critical($message);
            throw new \RuntimeException($message);
        }

        $phpexcel = $this->negotiatePhpExcelReader($source);

        $ws = $phpexcel->setActiveSheetIndex(0);
        /** @var \PHPExcel_Worksheet_Row $row */
        foreach ($ws->getRowIterator(2) as $row) {
            $model = new ProfessionalAreaImportModel();

            $model->setPositionTitle($ws->getCellByColumnAndRow(0, $row->getRowIndex())->getFormattedValue());
            $model->setBasePATitle($ws->getCellByColumnAndRow(2, $row->getRowIndex())->getFormattedValue());
            $model->setChildPATitle($ws->getCellByColumnAndRow(1, $row->getRowIndex())->getFormattedValue());
            $records[] = $model;
        }

        $this->logger->info(sprintf("Found %d records for source %s", count($records), $source));

        return $records;
    }

    /**
     * @param ProfessionalAreaImportModel|ImportModelInterface $record
     *
     * @return bool
     */
    public function processRecord(ImportModelInterface $record)
    {
        if (!mb_strlen($record->getBasePATitle())) {
            return false;
        }

        $basePA = $this->findProfessionalArea($record->getBasePATitle());
        $childPA = null;

        if (!$basePA) {
            return false;
        }

        if (mb_strlen($record->getChildPATitle())) {
            $childPA = $this->findSpecialization($record->getChildPATitle(), $basePA);
        }

        $position = $this->findPosition($record->getPositionTitle());
        if ($position) {
            $position->addProfessionalArea($basePA);
            if ($childPA) {
                $position->addProfessionalArea($childPA);
            }
            $this->em->flush();
        } else {
            $this->logger->debug(sprintf(
                'Relations for position "%s" skipped? because position can\'t be found',
                $record->getPositionTitle()
            ));
        }

        return true;
    }

    /**
     * @param $title
     *
     * @return Position
     */
    protected function findPosition($title)
    {
        static $cache;

        if (!$cache) {
            $cache = [];
        }

        if (!isset($cache[$title])) {
            $entity = $this->repositoryPosition->findOneBy(['title' => $title]);
            if (!$entity) {
                //We should skipp creation of positions

                //$entity = new Position();
                //$entity->setTitle($title)
                //    ->setSystem(true);
                //$this->em->persist($entity);
            }
            $cache[$title] = $entity;
        }

        return $cache[$title];
    }

    /**
     * @param string $name
     * @param ProfessionalArea $professionalArea
     *
     * @return ProfessionalArea
     */
    protected function findSpecialization($name, ProfessionalArea $professionalArea)
    {
        $title = mb_strcut($name, 0, 255);
        $titleInLatin = mb_strcut(Transliterator::transliterate($title), 0, 100);

        $key = $title . '-' . $professionalArea->getTitle();
        static $cache;
        if (!$cache) {
            $cache = [];
        }

        if (!isset($cache[$key])) {

            $entity = $this->repositoryPA->findSpecializationByTitle($title, $professionalArea);

            if (!$entity) {
                //Try to check in latin
                $entity = $this->repositoryPA->findOneBy([
                    'titleInLatin' => $titleInLatin,
                    'parent' => $professionalArea,
                ]);
            }
            if (!$entity) {
                $entity = new ProfessionalArea();
                $entity->setTitle($title)
                    ->setTitleInLatin($titleInLatin)
                    ->setTitleWhere($title)
                    ->setParent($professionalArea)
                    ->setSystem(true);
                $this->em->persist($entity);
                $this->em->flush($entity);
            }
            $cache[$key] = $entity;
        }

        return $cache[$key];
    }

    /**
     * @param $name
     *
     * @return ProfessionalArea
     */
    protected function findProfessionalArea($name)
    {
        $title = mb_strcut($name, 0, 255);
        $titleInLatin = mb_strcut(Transliterator::transliterate($title), 0, 100);

        static $cache;
        if (!$cache) {
            $cache = [];
        }

        if (!isset($cache[$title])) {

            $entity = $this->repositoryPA->findProfessionalAreaByTitle($title);

            if (!$entity) {
                $entity = $this->repositoryPA->findProfessionalAreaByTitleInLatin($titleInLatin);
            }

            if (!$entity) {
                $entity = new ProfessionalArea();
                $entity->setTitle($title)
                    ->setTitleInLatin($titleInLatin)
                    ->setTitleWhere($title)
                    ->setParent(null)
                    ->setSystem(true);
                $this->em->persist($entity);
                $this->em->flush($entity);
            }
            $cache[$name] = $entity;
        }

        return $cache[$name];
    }
}
