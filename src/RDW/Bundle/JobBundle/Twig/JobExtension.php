<?php

namespace RDW\Bundle\JobBundle\Twig;

use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PropertyAccess\Exception\NoSuchPropertyException;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class JobExtension
 *
 * @package RDW\Bundle\JobBundle\Twig
 */
class JobExtension extends \Twig_Extension
{
    /**
     * Container
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var TranslatorInterface;
     */
    private $translator;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('secret_field', [$this, 'getSecretField']),
            new \Twig_SimpleFilter('sort_by_field', [$this, 'sortByField']),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('companyImageUrl', [$this, 'getCompanyImageUrl']),
            new \Twig_SimpleFunction('companyContactPersonPhoto', [$this, 'getCompanyContactPersonPhoto']),
            new \Twig_SimpleFunction('countActiveJobs', [$this, 'getCountActiveJobs']),
            new \Twig_SimpleFunction('job_uri', [$this, 'getJobUri']),
        ];
    }

    /**
     * @param Job $job
     * @param string $field
     * @param bool $returnBool
     *
     * @return string
     * @throws \Symfony\Component\PropertyAccess\Exception\NoSuchPropertyException
     */
    public function getSecretField(Job $job, $field, $returnBool = false)
    {
        if (!property_exists($job, $field)) {
            throw new NoSuchPropertyException(
                sprintf('Property %s in class %s does not exist.', $field, get_class($job))
            );
        }

        if (false === $job->getSecret()) {
            if ($returnBool) {
                return true;
            } else {
                return $job->{'get' . ucfirst($field)}();
            }
        } else {
            if ($returnBool) {
                return false;
            } else {
                return $this->translator->trans('Title is not public');
            }
        }
    }

    /**
     * @param array $data
     * @param string $sortField
     *
     * @return array
     */
    public function sortByField(array $data, $sortField)
    {
        $sorted = $this->array_orderby($data, $sortField, SORT_DESC);

        return $sorted;
    }

    /**
     * Sorted array by field
     *
     * @param array $data
     * @param string $sortField
     * @param $sortBy
     */
    public function array_orderby(array $data, $sortField, $sortBy)
    {
        $args = [$data, $sortField, $sortBy];

        foreach ($args as $n => $field) {
            if (is_string($field)) {
                $tmp = [];

                foreach ($data as $key => $row) {
                    $tmp[$key] = $row[$field];
                    $args[$n] = $tmp;
                }
            }
        }
        $args[] = &$data;

        array_multisort($args);

        return array_pop($args);
    }

    /**
     * @param Job $job
     * @param string $filter
     * @param bool $absolute
     *
     * @return mixed|string
     */
    public function getCompanyImageUrl(Job $job, $filter = 'company_thumb', $absolute = true, $small = false)
    {
        $defaultLogoName = ($small) ? 'rdw_app.user_profile_no_logo_small' : 'rdw_app.user_profile_no_logo';
        $logo = $job->getLogo();

        $photo = (!empty($logo) && $this->isAllowedToShow($job))
            ? $this->container->get('imagine.cache.path.resolver')->getBrowserPath($logo, $filter, $absolute)
            : $this->container->getParameter('assets_base_url') . $this->container->getParameter($defaultLogoName);

        return $photo;
    }

    /**
     * @param Job $job
     * @param string $filter
     * @param bool $absolute
     *
     * @return mixed|string
     */
    public function getCompanyContactPersonPhoto(Job $job, $filter = 'contact_thumb', $absolute = true)
    {
        $contactPersonPhoto = $job->getContactPersonPhoto();

        $photo = !empty($contactPersonPhoto)
            ? $this->container->get('imagine.cache.path.resolver')->getBrowserPath($contactPersonPhoto, $filter, $absolute)
            : $this->container->getParameter('assets_base_url') . $this->container->getParameter('rdw_app.user_profile_no_logo');

        return $photo;
    }

    /**
     * @param Job $job
     * @param string $type
     *
     * @return bool
     */
    private function isAllowedToShow(Job $job, $type = 'photo')
    {
        $user = $this->container->get('security.context')->getToken()->getUser();

        // if preview as not logged in
        $allowedToShow = false;

        if (!$user instanceof RegisteredUser && $this->getSecretField($job, $type, true)) {
            $allowedToShow = true;
        }

        // if preview as logged in but not job author
        if ($user instanceof RegisteredUser && $user->getId() != $job->getUser()->getId() && $this->getSecretField($job, $type, true)) {
            $allowedToShow = true;
        }

        // if job author
        if ($user instanceof RegisteredUser && $user->getId() == $job->getUser()->getId()) {
            $allowedToShow = true;
        }

        // if censor or admin or manager
        if ($this->container->get('security.context')->isGranted('ROLE_CENSOR')) {
            $allowedToShow = true;
        }

        return $allowedToShow;
    }

    /**
     * @return int
     */
    public function getCountActiveJobs()
    {
        return $this->container->get('sphinx_search_service')->countActiveJobs();
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_job_twig_extension';
    }

    /**
     * @param Job $job
     *
     * @return string
     */
    public function getJobUri(Job $job)
    {
        return $this->container->get('router')->generate(
            'rdw_job.job.view',
            [
                'id' => $job->getId(),
                'slug' => $job->getSlug(),
            ]
        );
    }
}
