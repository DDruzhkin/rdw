<?php

namespace RDW\Bundle\JobBundle\EventListener;

use Braincrafted\Bundle\BootstrapBundle\Session\FlashMessage;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Event\BlockEvent;
use RDW\Bundle\JobBundle\Service\JobManager;
use RDW\Bundle\MailerBundle\Service\MailerManager;
use RDW\Bundle\ManageBundle\Entity\User;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class JobBlockListener
 *
 * @package RDW\Bundle\JobBundle\EventListener
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class JobBlockListener
{
    /**
     * @var \RDW\Bundle\JobBundle\Service\JobManager
     */
    private $jobManager;

    /**
     * @var \Braincrafted\Bundle\BootstrapBundle\Session\FlashMessage
     */
    private $flashMessage;

    /**
     * @var \RDW\Bundle\MailerBundle\Service\MailerManager
     */
    private $mailerManager;

    /**
     * @param JobManager    $jobManager
     * @param FlashMessage  $flashMessage
     * @param MailerManager $mailerManager
     */
    public function __construct(
        JobManager $jobManager,
        FlashMessage $flashMessage,
        MailerManager $mailerManager
    )
    {
        $this->jobManager = $jobManager;
        $this->flashMessage = $flashMessage;
        $this->mailerManager = $mailerManager;
    }

    /**
     * @param BlockEvent $event
     *
     * @throws \Symfony\Component\Validator\Exception\UnexpectedTypeException
     */
    public function onJobBlock(BlockEvent $event)
    {
        $job = $event->getJob();

        if ( ! $job instanceof Job) {
            throw new UnexpectedTypeException($job, 'RDW\Bundle\JobBundle\Entity\Job');
        }

        $user = $event->getUser();

        if ( ! $user instanceof User) {
            throw new UnexpectedTypeException($job, 'RDW\Bundle\ManageBundle\Entity\User');
        }

        // always set status as blocked
        $job->setStatus(Job::STATUS_BLOCKED);

        $this->jobManager->update($job);
        $this->flashMessage->success('Job was blocked');

        $this->mailerManager->sendEmailForUserJobBlocked($job, $user);
    }
}
