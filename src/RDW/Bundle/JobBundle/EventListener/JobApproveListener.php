<?php

namespace RDW\Bundle\JobBundle\EventListener;

use Braincrafted\Bundle\BootstrapBundle\Session\FlashMessage;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Event\JobEvent;
use RDW\Bundle\JobBundle\Service\JobManager;
use RDW\Bundle\MailerBundle\Service\MailerManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class JobApproveListener
 *
 * @package RDW\Bundle\JobBundle\EventListener
 */
class JobApproveListener
{
    /**
     * @var \RDW\Bundle\JobBundle\Service\JobManager
     */
    private $jobManager;

    /**
     * @var \Braincrafted\Bundle\BootstrapBundle\Session\FlashMessage
     */
    private $flashMessage;

    /**
     * @var \RDW\Bundle\MailerBundle\Service\MailerManager
     */
    private $mailerManager;

    /**
     * @var Router
     */
    private $router;

    /**
     * @param JobManager    $jobManager
     * @param FlashMessage  $flashMessage
     * @param MailerManager $mailerManager
     * @param Router        $router
     */
    public function __construct(
        JobManager $jobManager,
        FlashMessage $flashMessage,
        MailerManager $mailerManager,
        \Symfony\Cmf\Component\Routing\ChainRouter $router
    )
    {
        $this->jobManager = $jobManager;
        $this->flashMessage = $flashMessage;
        $this->mailerManager = $mailerManager;
        $this->router = $router;
    }

    /**
     * @param JobEvent $event
     *
     * @throws \Symfony\Component\Validator\Exception\UnexpectedTypeException
     */
    public function onJobApprove(JobEvent $event)
    {
        $job = $event->getJob();

        if (! $job instanceof Job) {
            throw new UnexpectedTypeException($job, 'RDW\Bundle\JobBundle\Entity\Job');
        }

        if ($job->getStatus() == Job::STATUS_BLOCKED) {
            // still not change status, we will updated it when submitting block job form
            $job->setStatus($job->getOldStatus());
            $this->jobManager->update($job);

            $url = $this->router->generate('rdw_job.job_admin.block', ['id' => $job->getId()]);
            $event->setResponse(new RedirectResponse($url));
        } else {
            $this->jobManager->update($job);

            $this->flashMessage->success('Job successfully updated.');

            // if old status was waiting and current is confirmed
            if (($job->getOldStatus() == Job::STATUS_WAITING && $job->getStatus() == Job::STATUS_ACTIVE)
                ||($job->getOldStatus() == Job::STATUS_BLOCKED && $job->getStatus() == Job::STATUS_ACTIVE)
            ) {
                $this->mailerManager->sendEmailForUserJobApproved($job);
            }

            $url = $this->router->generate('rdw_job.job_admin.list');
            $event->setResponse(new RedirectResponse($url));
        }
    }
}
