<?php

namespace RDW\Bundle\JobBundle\EventListener;

use RDW\Bundle\ConversationBundle\Service\MessageManager;
use RDW\Bundle\ConversationBundle\Service\ConversationManager;
use RDW\Bundle\HistoryBundle\Service\HistoryManager;
use RDW\Bundle\JobBundle\Event\MultiApplyEvent;
use RDW\Bundle\JobBundle\RDWJobEvents;
use RDW\Bundle\JobBundle\Service\JobApplyManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class MultiApplyListener
 *
 * @package RDW\Bundle\JobBundle\EventListener
 */
class MultiApplyListener extends ApplyListener implements EventSubscriberInterface
{
    /**
     * @var JobApplyManager
     */
    private $applyManager;

    /**
     * @param ConversationManager      $conversationManager
     * @param MessageManager           $messageManager
     * @param EventDispatcherInterface $dispatcher
     * @param HistoryManager           $historyManager
     * @param JobApplyManager          $applyManager
     */
    public function __construct(
        ConversationManager $conversationManager,
        MessageManager $messageManager,
        EventDispatcherInterface $dispatcher,
        HistoryManager $historyManager,
        JobApplyManager $applyManager
    )
    {
        parent::__construct($conversationManager, $messageManager, $dispatcher, $historyManager);
        $this->applyManager = $applyManager;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            RDWJobEvents::MULTI_APPLY_SUCCESS => 'onMultiApplySuccess',
        ];
    }

    /**
     * @param MultiApplyEvent $event
     */
    public function onMultiApplySuccess(MultiApplyEvent $event)
    {
        $apply = $event->getApply();
        $jobs = $event->getJobs();

        $this->setUploadType($apply);

        foreach ($jobs as $job) {
            $newApply = $this->applyManager->createApplyFromJobAndUser($job, $apply->getUser());
            $newApply->setMessage($apply->getMessage());

            $this->createMessage($newApply, $job);
        }
    }
}
