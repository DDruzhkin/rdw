<?php

namespace RDW\Bundle\JobBundle\EventListener;

use RDW\Bundle\ConversationBundle\Service\MessageManager;
use RDW\Bundle\HistoryBundle\Service\HistoryManager;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\ConversationBundle\Entity\Conversation;
use RDW\Bundle\HistoryBundle\Entity\History;
use RDW\Bundle\HistoryBundle\RDWHistoryEvents;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\JobBundle\Entity\Apply;
use RDW\Bundle\JobBundle\Event\ApplyEvent;
use RDW\Bundle\ConversationBundle\Event\MessageEvent;
use RDW\Bundle\ConversationBundle\RDWConversationEvents;
use RDW\Bundle\ConversationBundle\Service\ConversationManager;
use RDW\Bundle\JobBundle\RDWJobEvents;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class ApplyListener
 *
 * @package RDW\Bundle\JobBundle\EventListener
 */
class ApplyListener implements EventSubscriberInterface
{
    /**
     * @var \RDW\Bundle\ConversationBundle\Service\ConversationManager
     */
    protected $conversationManager;

    /**
     * @var MessageManager
     */
    protected $messageManager;

    /**
     * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @var HistoryManager
     */
    protected $historyManager;

    /**
     * @param ConversationManager      $conversationManager
     * @param MessageManager           $messageManager
     * @param EventDispatcherInterface $dispatcher
     * @param HistoryManager           $historyManager
     */
    public function __construct(
        ConversationManager $conversationManager,
        MessageManager $messageManager,
        EventDispatcherInterface $dispatcher,
        HistoryManager $historyManager
    )
    {
        $this->conversationManager = $conversationManager;
        $this->messageManager = $messageManager;
        $this->dispatcher = $dispatcher;
        $this->historyManager = $historyManager;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            RDWJobEvents::APPLY_SUCCESS => 'onApplySuccess',
        ];
    }

    /**
     * Save applied message
     *
     * @param ApplyEvent $event
     *
     * @return boolean
     */
    public function onApplySuccess(ApplyEvent $event)
    {
        $apply = $event->getApply();
        $job = $apply->getJob();

        $this->setUploadType($apply);

        return $this->createMessage($apply, $job);
    }

    /**
     * @param Apply $apply
     */
    protected function setUploadType(Apply $apply)
    {
        if ($apply->getCv() instanceof Cv) {
            $apply->setCvUploadType(Apply::CV_UPLOAD_TYPE_MY);
        } else {
            $apply->setCvUploadType(Apply::CV_UPLOAD_TYPE_UPLOAD);
        }
    }

    /**
     * @param Apply $apply
     * @param Job   $job
     *
     * @return boolean
     */
    protected function createMessage(Apply $apply, Job $job)
    {
        $sender = $apply->getUser();

        $conversation = $this->conversationManager->findByApply($apply);

        if (! $conversation instanceof Conversation) {
            $conversation = $this->conversationManager->create($apply->getJob()->getUser(), $apply->getUser());
            $this->conversationManager->addJobApply($conversation, $apply);
        }

        $message = $this->messageManager->createFromJobApply($apply);
        $this->conversationManager->addMessage($conversation, $message);
        $this->conversationManager->save($conversation);

        // send email and log history only when applies registered user
        if ($sender instanceof RegisteredUser) {
            $messageEvent = new MessageEvent($conversation, $message);
            $this->dispatcher->dispatch(RDWConversationEvents::SEND_MESSAGE_EMAIL, $messageEvent);

            $this->historyManager->logEvent($sender, History::ACTION_APPLY, $job, RDWHistoryEvents::APPLY_JOB);
        }

        return true;
    }
}
