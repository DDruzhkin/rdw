<?php

namespace RDW\Bundle\JobBundle\EventListener;

use Braincrafted\Bundle\BootstrapBundle\Session\FlashMessage;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Event\JobEvent;
use RDW\Bundle\JobBundle\Service\JobManager;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class JobSaveListener
 *
 * @package RDW\Bundle\JobBundle\EventListener
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class JobSaveListener
{
    /**
     * @var \RDW\Bundle\JobBundle\Service\JobManager
     */
    private $jobManager;

    /**
     * @var \Braincrafted\Bundle\BootstrapBundle\Session\FlashMessage
     */
    private $flashMessage;

    /**
     * @param JobManager    $jobManager
     * @param FlashMessage  $flashMessage
     */
    public function __construct(JobManager $jobManager, FlashMessage $flashMessage)
    {
        $this->jobManager = $jobManager;
        $this->flashMessage = $flashMessage;
    }

    /**
     * @param JobEvent $event
     *
     * @throws \Symfony\Component\Validator\Exception\UnexpectedTypeException
     */
    public function onJobSave(JobEvent $event)
    {
        $job = $event->getJob();

        if ( ! $job instanceof Job) {
            throw new UnexpectedTypeException($job, 'RDW\Bundle\JobBundle\Entity\Job');
        }

        $job->setStatus(Job::STATUS_INACTIVE);

        $this->jobManager->update($job);

        $this->flashMessage->success('The job was successfully saved');
    }
}
