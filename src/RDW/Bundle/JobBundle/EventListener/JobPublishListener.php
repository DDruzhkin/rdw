<?php

namespace RDW\Bundle\JobBundle\EventListener;

use Braincrafted\Bundle\BootstrapBundle\Session\FlashMessage;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Event\JobEvent;
use RDW\Bundle\JobBundle\Service\JobManager;
use RDW\Bundle\MailerBundle\Service\MailerManager;
use RDW\Bundle\ManageBundle\Service\UserManager;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use RDW\Bundle\AppBundle\Service\JobPublishingService;

/**
 * Class JobSaveListener
 *
 * @package RDW\Bundle\JobBundle\EventListener
 */
class JobPublishListener
{
    /**
     * @var \RDW\Bundle\JobBundle\Service\JobManager
     */
    private $jobManager;

    /**
     * @var \Braincrafted\Bundle\BootstrapBundle\Session\FlashMessage
     */
    private $flashMessage;

    /**
     * @var \RDW\Bundle\MailerBundle\Service\MailerManager
     */
    private $mailerManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var JobPublishingService
     */
    private $jobPublishingService;

    /**
     * @param JobManager           $jobManager
     * @param FlashMessage         $flashMessage
     * @param MailerManager        $mailerManager
     * @param UserManager          $userManager
     * @param RouterInterface      $router
     * @param TranslatorInterface  $translator
     * @param JobPublishingService $jobPublishingService
     */
    public function __construct(
        JobManager $jobManager,
        FlashMessage $flashMessage,
        MailerManager $mailerManager,
        UserManager $userManager,
        RouterInterface $router,
        TranslatorInterface $translator,
        JobPublishingService $jobPublishingService
    )
    {
        $this->jobManager = $jobManager;
        $this->flashMessage = $flashMessage;
        $this->mailerManager = $mailerManager;
        $this->userManager = $userManager;
        $this->router = $router;
        $this->translator = $translator;
        $this->jobPublishingService = $jobPublishingService;
    }

    /**
     * @param JobEvent $event
     *
     * @throws \Symfony\Component\Validator\Exception\UnexpectedTypeException
     */
    public function onJobPublish(JobEvent $event)
    {
        $job = $event->getJob();

        if (! $job instanceof Job) {
            throw new UnexpectedTypeException($job, 'RDW\Bundle\JobBundle\Entity\Job');
        }

        // create, cause we need job id
        $job->setStatus(Job::STATUS_PUBLISHED);
        $this->jobManager->update($job);

        if ($this->jobManager->hasActivePublishingService($job)) {
            $this->jobPublishingService->publish($job);

            $message = 'Your job offer was successfully updated. Job offer will be available on the site after site administrators confirmation.';
            $this->flashMessage->success($message);
        } else {
            $message = 'Please order a Job Publishing service. Job offer will be available on the site after site administrators confirmation.';
            $this->flashMessage->alert($message);
        }

        $this->jobManager->update($job);
    }
}
