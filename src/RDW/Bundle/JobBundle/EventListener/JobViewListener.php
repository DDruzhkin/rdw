<?php

namespace RDW\Bundle\JobBundle\EventListener;

use RDW\Bundle\HistoryBundle\Service\HistoryManager;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Entity\JobView;
use RDW\Bundle\JobBundle\Event\JobEvent;
use RDW\Bundle\JobBundle\Service\JobManager;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use RDW\Bundle\HistoryBundle\Entity\History;
use RDW\Bundle\HistoryBundle\Event\HistoryEvent;
use RDW\Bundle\HistoryBundle\RDWHistoryEvents;

/**
 * Class JobViewListener
 *
 * @package RDW\Bundle\JobBundle\EventListener
 */
class JobViewListener
{
    /**
     * @var \RDW\Bundle\JobBundle\Service\JobManager
     */
    private $jobManager;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var HistoryManager
     */
    private $historyManager;

    /**
     * @param JobManager               $jobManager
     * @param EventDispatcherInterface $eventDispatcher
     * @param HistoryManager           $historyManager
     */
    public function __construct(JobManager $jobManager, EventDispatcherInterface $eventDispatcher, HistoryManager $historyManager)
    {
        $this->jobManager      = $jobManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->historyManager  = $historyManager;
    }

    /**
     * @param JobEvent $event
     *
     * @return null|void
     * @throws UnexpectedTypeException
     */
    public function onJobView(JobEvent $event)
    {
        $user = $event->getUser();

        $job = $event->getJob();

        if (! $job instanceof Job) {
            throw new UnexpectedTypeException($job, 'RDW\Bundle\JobBundle\Entity\Job');
        }

        $jobId = $job->getId();

        $isViewedToday = isset($_COOKIE["show_v_id_$jobId"]) ? 1 : 0;

        if (! $isViewedToday) {
            if ($user) {
                if (!$user->isManager() && $job->getUser() != $user && !$this->jobManager->existsView($job, $user)) {
                    $job->addUserWhoViewed(new JobView($job, $user));
                }
                // save history
                $this->historyManager->logEvent($user, History::ACTION_VIEW, $job, RDWHistoryEvents::VIEW_OBJECT);
            } elseif ($user == null) {
                $job->incView();
            }

            setcookie("show_v_id_" . $job->getId(), 1 ,time()+86400); // 24 hours

            $this->jobManager->update($job);
        }
    }
}
