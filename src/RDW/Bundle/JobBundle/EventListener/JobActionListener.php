<?php

namespace RDW\Bundle\JobBundle\EventListener;

use RDW\Bundle\JobBundle\Entity\Job;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class JobActionListener implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::SUBMIT   => 'onSubmit',
        );
    }

    /**
     * @param FormEvent $event
     *
     */
    public function onSubmit(FormEvent $event)
    {
        $entity = $event->getForm()->getData();

        if ($entity instanceof Job) {
            $entity->setUpdatedAt(new \DateTime());
        }
    }
}