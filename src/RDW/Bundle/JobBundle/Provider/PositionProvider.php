<?php

namespace RDW\Bundle\JobBundle\Provider;

use Doctrine\Bundle\DoctrineBundle\Registry;
use RDW\Bundle\JobBundle\Entity\Position;
use RDW\Bundle\JobBundle\Repository\PositionRepository;
use Symfony\Component\HttpFoundation\Request;

class PositionProvider
{
    /** @var Registry */
    protected $doctrine;

    /**
     * @var PositionRepository
     */
    protected $positionRepository;

    /**
     * PositionProvider constructor.
     *
     * @param Registry $doctrine
     */
    public function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @param Request $request
     * @return array
     */
    public function findPositionsByRequest(Request $request)
    {
        $repo = $this->doctrine->getRepository(Position::class);
        $result = $repo->getPositionsByParams($request->query->get('position_filter'));

        return $result;
    }

    /**
     * @param $id
     * @return array
     */
    public function findPositionById($id)
    {
        $repo = $this->doctrine->getRepository(Position::class);
        $result = $repo->getPositionsById($id);

        return $result;
    }

    /**
     * @param int $count
     * @param int $limit
     * @return array|Position[]
     */
    public function getPopular($count = 1, $limit = 1)
    {
        $repo = $this->doctrine->getRepository(Position::class);

        return $repo->getPopular($count, $limit);
    }

    /**
     * @param Request $request
     * @param $limit - limit result positions
     * @return array
     */
    public function findApprovedPositionByRequest(Request $request, $limit = null) {
        $response = [];
        $query = $request->query->get('q');
        $repo = $this->doctrine->getRepository(Position::class);
        $result = $repo->getPositionByQuery($query, $limit);

        /** @var Position $position */
        foreach ($result as $position) {
            $response[] = [
                'value' => $position->getId(),
                'title' => $position->getTitle(),
                'filter' => 'professional_area',
                'filter_title' => 'Professional Area',
            ];
        }

        return $response;
    }
}
