<?php

namespace RDW\Bundle\JobBundle\Provider;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\DBAL\Connection;
use RDW\Bundle\JobBundle\Entity\Position;
use RDW\Bundle\JobBundle\Entity\ProfessionalArea;
use RDW\Bundle\JobBundle\Repository\ProfessionalAreaRepository;
use Symfony\Component\HttpFoundation\Request;

class ProfessionalAreaProvider
{
    /** @var Registry */
    protected $doctrine;

    /**
     * @var ProfessionalAreaRepository
     */
    protected $professionalAreaRepository;

    /**
     * ProfessionalAreaProvider constructor.
     *
     * @param Registry $doctrine
     */
    public function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @param int $count
     * @param int $limit
     *
     * @return array|ProfessionalArea[]
     */
    public function getPopular($count = 1, $limit = 1)
    {
        $repo = $this->doctrine->getRepository(ProfessionalArea::class);

        return $repo->getPopular($count, $limit);
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function findPaByRequest(Request $request)
    {
        $response = [];
        $query = $request->query->get('q');
        $repo = $this->doctrine->getRepository(ProfessionalArea::class);
        $result = $repo->getPaByQuery($query);

        /** @var ProfessionalArea $professionalArea */
        foreach ($result as $professionalArea) {
            $response[] = [
                'value' => $professionalArea->getId(),
                'title' => $professionalArea->getTitle(),
                'filter' => 'professional_area',
                'filter_title' => 'Professional Area',
            ];
        }

        return $response;
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function findPaByPosition(Request $request)
    {
        $response = [];
        $positionId = $request->query->get('id');
        $repo = $this->doctrine->getRepository(ProfessionalArea::class);
        $result = $repo->getPaByPosition($positionId);

        /** @var ProfessionalArea $professionalArea */
        foreach ($result as $professionalArea) {
            $response[] = $professionalArea->getId();
        }

        return $response;
    }

    /**
     * @param Position $position
     *
     * @return array|ProfessionalArea[]
     */
    public function findUnapproved(Position $position)
    {
        /** @var Connection $connection */
        $connection = $this->doctrine->getConnection();
        $result = $connection->executeQuery(sprintf('
            SELECT
                jpa.professionalarea_id
            FROM
                job_professionalarea AS jpa
            JOIN jobs ON jobs.id = jpa.job_id
            AND jobs.position_id = %d
            WHERE
                jpa.professionalarea_id NOT IN (
                    SELECT
                        ppa.professionalarea_id
                    FROM
                        position_professionalarea AS ppa
                    WHERE
                        ppa.position_id = %d
                )        
        ', $position->getId(), $position->getId()));

        $result = $result->fetchAll();
        $result = array_map(function ($element) {
            return $this->doctrine->getManager()->find(ProfessionalArea::class, $element['professionalarea_id']);
        }, $result);

        return array_unique($result);
    }

    /**
     * @param int $count
     * @param int $limit
     * @param int $id
     *
     * @return array|ProfessionalArea[]
     */
    public function getPopularSpecializationInPa($id, $count = 1, $limit = 1)
    {
        $repo = $this->doctrine->getRepository(ProfessionalArea::class);

        return $repo->getPopularSpecializationInPa($id, $count, $limit);
    }
}
