<?php

namespace RDW\Bundle\JobBundle\Provider;

use Doctrine\Common\Persistence\ManagerRegistry;
use RDW\Bundle\AnalyticBundle\Model\AnalyticDataModel;
use RDW\Bundle\AnalyticBundle\Model\AnalyticDataProviderInterface;
use RDW\Bundle\JobBundle\Entity\Job;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

class JobAnalyticDataProvider implements AnalyticDataProviderInterface
{
    /** @var ManagerRegistry */
    protected $doctrine;

    /** @var RequestStack */
    protected $requestStack;

    /** @var RouterInterface */
    protected $router;

    /**
     * @param ManagerRegistry $doctrine
     * @param RouterInterface $router
     */
    public function __construct(ManagerRegistry $doctrine, RouterInterface $router)
    {
        $this->doctrine = $doctrine;
        $this->router = $router;
    }

    /**
     * {@inheritDoc}
     */
    public function isUriSupported($uri)
    {
        $route = $this->router->match($uri);
        if (!isset($route['_route']) || !($route['_route'] === 'rdw_job.job.view') || !isset($route['id'])) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function fillData($uri, AnalyticDataModel $dataModel)
    {
        $route = $this->router->match($uri);
        $id = $route['id'];
        $job = $this->doctrine->getManagerForClass(Job::class)->find(Job::class, $id);
        if ($job) {
            $dataModel
                ->setUri($uri)
                ->setViewsUniqueCount($job->getViewsCount())
                ->setViewsCount($job->getTotalViewsCount())
                ->setContactsShown($job->getTotalViewContactCount())
                ->setReplied($job->getAppliesCount());
        }
    }
}
