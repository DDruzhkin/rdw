<?php

namespace RDW\Bundle\JobBundle\Controller;

use RDW\Bundle\AppBundle\Form\Type\AbuseType;
use RDW\Bundle\AppBundle\RDWAppEvents;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\AppBundle\Event\AdAbuseEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class JobAbuseController
 *
 * @package RDW\Bundle\JobBundle\Controller
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 *
 * @Route("/job")
 */
class JobAbuseController extends Controller
{
    /**
     * @param Job $job
     *
     * @Route("/abuse/{id}", name="rdw_job.job.abuse", requirements={"id" = "\d+"})
     * @Method({"GET"})
     * @Template()
     *
     * @return Response|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function abuseAction(Job $job)
    {
        $user = $this->getUser();

        if ( ! $user) {
            return $this->redirect($this->generateUrl('rdw_job.job.view', ['id' => $job->getId(), 'slug' => $job->getSlug()]));
        }

        $abuse = $this->get('rdw_job.service.job_abuse_manager')->create();

        $form = $this->createForm(new AbuseType(), $abuse, ['data_class' => 'RDW\Bundle\AppBundle\Entity\JobAbuse']);

        return [
            'form' => $form->createView(),
            'abuse' => $abuse,
            'id' => $job->getId(),
        ];
    }

    /**
     * @param Request $request
     * @param Job     $job
     *
     * @Route("/save-abuse/{id}",name="rdw_job.job.save_abuse", requirements={"id" = "\d+"})
     * @Method({"POST"})
     *
     * @return JsonResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function saveAbuseAction(Request $request, Job $job)
    {
        $data['success'] = false;
        $response = new JsonResponse();

        $user = $this->getUser();

        if ( ! $user) {
            $response->setData($data);

            return $response;
        }

        $abuse = $this->get('rdw_job.service.job_abuse_manager')->create();

        $form = $this->createForm(new AbuseType(), $abuse, ['data_class' => 'RDW\Bundle\AppBundle\Entity\JobAbuse']);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new AdAbuseEvent($form->getData(), $job, $user);
            $this->container->get('event_dispatcher')->dispatch(RDWAppEvents::AD_ABUSE, $event);

            $data['url'] = $this->generateUrl('rdw_job.job.view', ['id' => $job->getId(), 'slug' => $job->getSlug()]);
            $data['success'] = true;
        }

        $data['content'] = $this->renderView('RDWJobBundle:JobAbuse:_form.html.twig', [
            'form' => $form->createView(),
            'abuse' => $abuse,
            'id' => $job->getId(),
        ]);

        $response = new JsonResponse();
        $response->setData($data);

        return $response;
    }
}
