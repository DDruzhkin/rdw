<?php

namespace RDW\Bundle\JobBundle\Controller;

use RDW\Bundle\HistoryBundle\Entity\History;
use RDW\Bundle\HistoryBundle\Event\HistoryEvent;
use RDW\Bundle\HistoryBundle\RDWHistoryEvents;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Event\JobEvent;
use RDW\Bundle\JobBundle\Form\Type\ActivateJobType;
use RDW\Bundle\JobBundle\Form\Type\EmailForEmployeeType;
use RDW\Bundle\JobBundle\Form\Type\JobCollectionType;
use RDW\Bundle\JobBundle\Form\Type\JobType;
use RDW\Bundle\JobBundle\Model\ActivateJob;
use RDW\Bundle\JobBundle\Model\EmailForEmployee;
use RDW\Bundle\JobBundle\RDWJobEvents;
use RDW\Bundle\JobBundle\Service\JobManager;
use RDW\Bundle\ServiceBundle\Entity\Service;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class JobClientController
 *
 * @package RDW\Bundle\JobBundle\Controller
 *
 * @Route("/job/client")
 * @Security("has_role('ROLE_MANAGER')")
 */
class JobClientController extends Controller
{
    /**
     * @param Request        $request
     * @param RegisteredUser $client
     *
     * @Route("/list/{id}", name="rdw_job.job_client.list", requirements={"id" = "\d+"})
     * @Route("/list", name="rdw_job.job_client.list_all")
     * @Template()
     * @Method({"GET"})
     *
     * @return array
     * @throws AccessDeniedHttpException
     */
    public function listAction(Request $request, RegisteredUser $client = null)
    {
        $this->validateAccess($client);

        $pagination = $this->get('rdw_job.service.job_manager')->getPaginatedListForManager(
            $this->getUser(),
            $request->query->get('page', 1),
            $this->getPerPageNumber(),
            $client
        );

        $form = $this->createForm(new JobCollectionType($pagination->getItems()));

        return [
            'client' => $client,
            'pagination' => $pagination,
            'form' => $form->createView(),
        ];
    }

    /**
     * @param Request        $request
     * @param RegisteredUser $client
     *
     * @Route("/create/{id}", name="rdw_job.job_client.create", requirements={"id" = "\d+"})
     * @Template()
     * @Method({"GET", "POST"})
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction(Request $request, RegisteredUser $client)
    {
        $this->validateAccess($client);

        $job = $this->get('rdw_job.service.job_manager')->createFromUser($client);

        $form = $this->createForm(new JobType(), $job);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $dispatcher = $this->container->get('event_dispatcher');

            $event = new JobEvent($form->getData());

            /** @var SubmitButton $submitAndPublish */
            $submitAndPublish = $form->get('submitAndPublish');

            if ($submitAndPublish->isClicked()) {
                $dispatcher->dispatch(RDWJobEvents::JOB_PUBLISH, $event);
            } else {
                $dispatcher->dispatch(RDWJobEvents::JOB_SAVE, $event);
            }

            $this->get('rdw_history.service.history_manager')->logEvent($this->getUser(), History::ACTION_CREATE, $job, RDWHistoryEvents::CREATE_OBJECT);

            return $this->redirect(
                $this->get('router')->generate('rdw_job.job_client.list', ['id' => $client->getId()])
            );
        }

        return [
            'form' => $form->createView(),
            'job' => $job,
        ];
    }

    /**
     * @param Request $request
     * @param Job     $job
     *
     * @Route("/edit/{id}", name="rdw_job.job_client.edit", requirements={"id" = "\d+"})
     * @Template()
     * @Method({"GET", "POST"})
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function editAction(Request $request, Job $job)
    {
        $this->validateAccess($job->getUser());

        // can edit only jobs with statuses new, waiting, inactive, active
        if (! $job->isEditable()) {
            return $this->redirect(
                $this->get('router')->generate('rdw_job.job_client.list', ['id' => $job->getUser()->getId()])
            );
        }

        $form = $this->createForm(new JobType(), $job);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $dispatcher = $this->container->get('event_dispatcher');

            $event = new JobEvent($form->getData());

            /** @var SubmitButton $submitAndPublish */
            $submitAndPublish = $form->get('submitAndPublish');

            if ($submitAndPublish->isClicked()) {
                $dispatcher->dispatch(RDWJobEvents::JOB_PUBLISH, $event);
            } else {
                $dispatcher->dispatch(RDWJobEvents::JOB_SAVE, $event);
            }

            $this->get('rdw_history.service.history_manager')->logEvent($this->getUser(), History::ACTION_EDIT, $job, RDWHistoryEvents::EDIT_OBJECT);

            return $this->redirect($this->get('router')->generate('rdw_job.job_client.edit', ['id' => $job->getId()]));
        }

        return [
            'form' => $form->createView(),
            'job' => $job,
        ];
    }

    /**
     * @param Job $job
     *
     * @Route(
     *      "/inactivate/{id}",
     *      name="rdw_job.job_client.inactivate",
     *      requirements={"id" = "\d+"}
     * )
     * @Method({"GET"})
     *
     * @return RedirectResponse
     */
    public function inactivateAction(Job $job)
    {
        $client = $job->getUser();

        $this->validateAccess($client);

        if ($job->isAllowedToChangeStatusForManager()) {
            $this->get('rdw_job.service.job_manager')->changeStatus($job, Job::STATUS_INACTIVE);
        }

        return $this->redirect($this->generateUrl('rdw_job.job_client.list', ['id' => $client->getId()]));
    }

    /**
     * @param Request $request
     * @param Job     $job
     *
     * @Route(
     *      "/activate/{id}",
     *      name="rdw_job.job_client.activate",
     *      requirements={"id" = "\d+"}
     * )
     * @Method({"GET","POST"})
     * @Template()
     *
     * @return Response|RedirectResponse
     */
    public function activateAction(Request $request, Job $job)
    {
        $client = $job->getUser();

        $this->validateAccess($client);

        $clientListRedirectResponse = $this->redirect($this->generateUrl('rdw_job.job_client.list', ['id' => $client->getId()]));

        if (! $job->isAllowedToChangeStatusForManager()) {
            return $clientListRedirectResponse;
        }

        $form = $this->createForm(new ActivateJobType(), new ActivateJob());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $job->setCreatedAt(new \DateTime());
            $job->setValidTill($form->getData()->getValidTill());

            $event = new JobEvent($job);
            $this->container->get('event_dispatcher')->dispatch(RDWJobEvents::JOB_PUBLISH, $event);

            return $clientListRedirectResponse;
        }

        return [
            'job' => $job,
            'form' => $form->createView(),
        ];
    }

    /**
     * @param Job $job
     *
     * @Route("/view/{id}", name="rdw_job.job_client.view", requirements={"id" = "\d+"})
     * @Template()
     * @Method({"GET"})
     *
     * @return array
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function viewAction(Job $job)
    {
        $this->validateAccess($job->getUser());

        $form = $this->createForm(new JobType(), $job, ['disabled' => true]);
        $services = $this->get('rdw_service.service.service_manager')->getServicesAllowedToOrderFromList(Service::FOR_EMPLOYER);

        return [
            'job' => $job,
            'form' => $form->createView(),
            'services' => $services,
        ];
    }

    /**
     * @param Job $job
     *
     * @Route("/delete/{id}", name="rdw_job.job_client.delete", requirements={"id" = "\d+"})
     * @Method({"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Job $job)
    {
        $client = $job->getUser();

        $this->validateAccess($client);

        $this->get('rdw_job.service.job_manager')->delete($job);
        $this->container->get('braincrafted_bootstrap.flash')->error('Successfully deleted');

        $this->get('rdw_history.service.history_manager')->logEvent($this->getUser(), History::ACTION_DELETE, $job, RDWHistoryEvents::DELETE_OBJECT);

        return $this->redirect($this->generateUrl('rdw_job.job_client.list', ['id' => $client->getId()]));
    }

    /**
     * @param Request        $request
     * @param RegisteredUser $client
     *
     * @Route("/multiple-delete/{id}", name="rdw_job.job_client.multiple_delete_by_client", requirements={"id" = "\d+"})
     * @Route("/multiple-delete", name="rdw_job.job_client.multiple_delete")
     *
     * @return RedirectResponse
     * @throws AccessDeniedHttpException
     */
    public function multipleDeleteAction(Request $request, RegisteredUser $client = null)
    {
        $this->validateAccess($client);

        $ids = $request->request->get('job_collection', ['id' => []])['id'];
        $form = $this->createForm(new JobCollectionType($ids));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->get('rdw_job.service.job_manager')->multipleDelete($form->get('id')->getData(), $this->getUser());
            $this->container->get('braincrafted_bootstrap.flash')->success('Successfully deleted');
        }

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @param Job $job
     *
     * @Route("/email-employees-form/{id}", name="rdw_job.job_client.email_employees_form", requirements={"id" = "\d+"})
     * @Method({"GET"})
     * @Template("RDWJobBundle:JobClient:emailEmployees.html.twig")
     *
     * @return array
     *
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function emailEmployeesFormAction(Job $job)
    {
        $this->validateAccess($job->getUser());

        $email = new EmailForEmployee();
        $email->setCompanyTitle($job->getCompanyTitle());

        $form = $this->createForm(new EmailForEmployeeType(), $email);

        return [
            'form' => $form->createView(),
            'job' => $job,
        ];
    }

    /**
     * @param Request $request
     * @param Job     $job
     *
     * @Route("/email-employees/{id}", name="rdw_job.job_client.email_employees", requirements={"id" = "\d+"})
     * @Method({"POST"})
     *
     * @return array
     *
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function emailEmployeesAction(Request $request, Job $job)
    {
        $client = $job->getUser();

        $this->validateAccess($client);

        $data['success'] = false;

        $email = new EmailForEmployee();
        $email->setCompanyTitle($client->getCompanyTitle());

        $form = $this->createForm(new EmailForEmployeeType(), $email);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $criteria = [
                'type' => RegisteredUser::USER_TYPE_EMPLOYEE,
                'enabled' => true,
            ];

            $employees = $this->container
                ->get('doctrine.orm.entity_manager')
                ->getRepository('RDWUserBundle:RegisteredUser')
                ->findBy($criteria);

            $this->container->get('rdw_mailer.service.mailer_manager')->sendEmailForEmployees($form->getData(), $job, $employees);
            $this->container->get('braincrafted_bootstrap.flash')->success('Email sent');

            $data['success'] = true;
        }

        $data['content'] = $this->renderView('RDWJobBundle:JobClient:_form_email_employees.html.twig', [
            'form' => $form->createView(),
            'job' => $job,
        ]);

        $response = new JsonResponse();
        $response->setData($data);

        return $response;
    }

    private function validateAccess(RegisteredUser $client = null)
    {
        $user  = $this->getUser();

        if ($client && ! $client->isUserManager($user)) {
            throw new AccessDeniedHttpException();
        }
    }

    /**
     * Extract per page param from query string
     *
     * @return int
     */
    private function getPerPageNumber()
    {
        $current = $this->get('request')->query->get('per_page');
        $options = $this->container->getParameter('rdw_job.list.per_page.options');

        $perPage = ($current && in_array($current, $options)) ? $current : $options[0];

        return $perPage;
    }
}
