<?php

namespace RDW\Bundle\JobBundle\Controller\Manage;

use RDW\Bundle\JobBundle\Entity\Position;
use RDW\Bundle\JobBundle\Entity\ProfessionalArea;
use RDW\Bundle\JobBundle\Form\Type\Manage\PositionFilterType;
use RDW\Bundle\JobBundle\Form\Type\Manage\PositionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("manage/position")
 */
class PositionController extends Controller
{
    /**
     * @Route("/list", name="manage_position_index")
     *
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $positionProvider = $this->container->get('rdw_job.provider.position');
        $positionFilter = $request->query->get('position_filter');
        if ($positionFilter) {
            $positionFilter['id'] = $positionProvider->findPositionById($positionFilter['id']) ?: null;
        }
        $filterForm = $this->createForm(PositionFilterType::class, $positionFilter);
        $pagination = $this->get('knp_paginator')->paginate(
            $positionProvider->findPositionsByRequest($request),
            $this->get('request')->query->get('page', 1),
            $this->container->getParameter('rdw_job.job_admin.paginator.per_page')
        );

        return [
            'pagination' => $pagination,
            'filter_form' => $filterForm->createView(),
        ];
    }

    /**
     * @Route("/create/", name="manage_position_create")
     *
     * @Template()
     *
     * @param Request $request
     *
     * @return array|RedirectResponse
     */
    public function createAction(Request $request)
    {
        return $this->handle(new Position(), $request);
    }

    /**
     * @param Position $position
     * @param ProfessionalArea $pa
     *
     * @Route(
     *     "/{id}/approve/{paid}/",
     *     name="manage_position_approve_pa",
     *     requirements={"id"="\d+","paid"="\d+"}
     * )
     *
     * @ParamConverter("pa", class="RDWJobBundle:ProfessionalArea", options={"id"="paid"})
     *
     * @return RedirectResponse
     */
    public function approveAction(Position $position, ProfessionalArea $pa)
    {
        $position->addProfessionalArea($pa);
        $this->getDoctrine()->getManager()->flush();

        //TODO: Make it using AJAX

        return $this->redirectToRoute('rdw_manage.filter.edit', [
            'type' => 'position',
            'id' => $position->getId()
        ]);
    }

    /**
     * @param Position $position
     * @param ProfessionalArea $pa
     *
     * @Route(
     *     "/{id}/edit/approve/{paid}/",
     *     name="manage_position_approve_pa_temp",
     *     requirements={"id"="\d+","paid"="\d+"}
     * )
     *
     * @ParamConverter("pa", class="RDWJobBundle:ProfessionalArea", options={"id"="paid"})
     *
     * @return RedirectResponse
     */
    public function approveActionTemp(Position $position, ProfessionalArea $pa)
    {
        $position->addProfessionalArea($pa);
        $this->getDoctrine()->getManager()->flush();

        //TODO: Make it using AJAX

        return $this->redirectToRoute('manage_position_edit', [
            'type' => 'position',
            'id' => $position->getId()
        ]);
    }

    /**
     * @Route("/{id}/edit/", name="manage_position_edit", requirements={"id"="\d+"})
     *
     * @Template()
     *
     * @param Position $position
     * @param Request $request
     *
     * @return array|RedirectResponse
     */
    public function updateAction(Position $position, Request $request)
    {
        return $this->handle($position, $request);
    }

    /**
     * @param Position $position
     * @param Request $request
     *
     * @return array|RedirectResponse
     */
    protected function handle(Position $position, Request $request)
    {
        $form = $this->createForm(PositionType::class, $position);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $em = $this->getDoctrine()->getManagerForClass(Position::class);
                $em->persist($data);
                $em->flush();

                //TODO Add Flash message

                return $this->redirectToRoute('manage_position_index');
            }
        }

        return [
            'form' => $form->createView(),
            'entity' => $form->getData(),
            'unapproved_pa' => $this->get('rdw_job.provider.professional_area')->findUnapproved($position),
        ];
    }
}
