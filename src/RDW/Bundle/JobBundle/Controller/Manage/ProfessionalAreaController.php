<?php

namespace RDW\Bundle\JobBundle\Controller\Manage;

use RDW\Bundle\JobBundle\Entity\ProfessionalArea;
use RDW\Bundle\JobBundle\Form\Type\Manage\ProfessionalAreaType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("manage/pa")
 */
class ProfessionalAreaController extends Controller
{
    /**
     * @Route("/", name="manage_professional_area_index")
     *
     * @Template()
     */
    public function indexAction()
    {
        $pagination = $this->get('knp_paginator')->paginate(
            $this->getDoctrine()->getRepository(ProfessionalArea::class)->createQueryBuilder('pa'),
            $this->get('request')->query->get('page', 1),
            $this->container->getParameter('rdw_job.job_admin.paginator.per_page')
        );

        return [
            'pagination' => $pagination,
        ];
    }

    /**
     * @Route("/create/", name="manage_professional_area_create")
     *
     * @Template()
     *
     * @param Request $request
     *
     * @return array|RedirectResponse
     */
    public function createAction(Request $request)
    {
        return $this->handle(new ProfessionalArea(), $request);
    }

    /**
     * @Route("/{id}/edit/", name="manage_professional_area_edit", requirements={"id"="\d+"})
     *
     * @Template()
     *
     * @param ProfessionalArea $area
     * @param Request $request
     *
     * @return array|RedirectResponse
     */
    public function updateAction(ProfessionalArea $area, Request $request)
    {
        return $this->handle($area, $request);
    }

    /**
     * @param ProfessionalArea $area
     * @param Request $request
     *
     * @return array|RedirectResponse
     */
    protected function handle(ProfessionalArea $area, Request $request)
    {
        $form = $this->createForm(ProfessionalAreaType::class, $area);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $data = $form->getData();
                $em = $this->getDoctrine()->getManagerForClass(ProfessionalArea::class);
                $em->persist($data);
                $em->flush();

                //TODO Add Flash message

                return $this->redirectToRoute('manage_professional_area_index');
            }
        }

        return [
            'form' => $form->createView(),
            'entity' => $form->getData(),
        ];
    }
}
