<?php

namespace RDW\Bundle\JobBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class JobAbuseAdminController
 *
 * @package RDW\Bundle\JobBundle\Controller
 *
 * @Route("/manage/job/abuse")
 */
class JobAbuseAdminController extends Controller
{
    /**
     * @param Request $request
     *
     * @Route("/list", name="rdw_job.job_abuse_admin.list")
     * @Template()
     * @Method({"GET"})
     *
     * @return array
     */
    public function listAction(Request $request)
    {
        $abuses = $this->get('rdw_job.service.job_abuse_manager')->getPaginatedList(
            $request->query->get('page', 1),
            $this->container->getParameter('rdw_job.job_abuse_admin.paginator.per_page')
        );

        return [
            'pagination' => $abuses,
        ];
    }
}
