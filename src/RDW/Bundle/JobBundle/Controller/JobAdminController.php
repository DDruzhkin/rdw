<?php

namespace RDW\Bundle\JobBundle\Controller;

use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Event\BlockEvent;
use RDW\Bundle\JobBundle\Event\JobEvent;
use RDW\Bundle\JobBundle\Form\Type\JobActivateType;
use RDW\Bundle\JobBundle\Model\JobFilter;
use RDW\Bundle\JobBundle\RDWJobEvents;
use RDW\Bundle\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use RDW\Bundle\UserBundle\Event\SingleUserEvent;
use RDW\Bundle\UserBundle\RDWUserEvents;

/**
 * Class JobAdminController
 *
 * @package RDW\Bundle\JobBundle\Controller
 *
 * @Route("/manage/job")
 */
class JobAdminController extends Controller
{
    /**
     * @Route("/list", name="rdw_job.job_admin.list")
     * @Template()
     *
     * @param Request $request
     * @return array|Response
     */
    public function listAction(Request $request)
    {
        $jobFilter = new JobFilter();

        // if logged in as censor apply filter
        $user = $this->getUser();

        if ($user->isLoggedInAsCensor()) {
            $jobFilter->setStatuses([Job::STATUS_WAITING, Job::STATUS_ACTIVE]);
            $jobFilter->setOnlyPaid(true);
        }

        $filterForm = $this->createForm('job_admin_filter', $jobFilter);
        $filterForm->handleRequest($request);

        if ($filterForm->get('clear')->isClicked()) {
            return $this->redirect($this->get('router')->generate('rdw_job.job_admin.list'));
        }

        $pagination = $this->getJobManager()->getPaginatedAdminList(
            $jobFilter,
            $this->get('request')->query->get('page', 1),
            $this->container->getParameter('rdw_job.job_admin.paginator.per_page')
        );

        return [
            'filterForm' => $filterForm->createView(),
            'pagination' => $pagination,
        ];
    }

    /**
     * @param Request $request
     * @param User    $client
     *
     * @Route("/client/create/{id}", name="rdw_job.job_admin.create", requirements={"id" = "\d+"})
     * @Method({"GET", "POST"})
     * @Template()
     *
     * @return array|RedirectResponse
     * @throws NotFoundHttpException
     */
    public function createAction(Request $request, User $client)
    {
        $job = $this->getJobManager()->createFromUser($client);

        $form = $this->createForm('rdw_job_admin', $job);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getJobManager()->update($job);
            $this->container->get('braincrafted_bootstrap.flash')->success('The job was successfully saved');

            return new RedirectResponse($this->generateUrl('rdw_job.job_admin.client_list', ['id' => $client->getId()]));
        }

        return [
            'form' => $form->createView(),
            'client' => $client,
            'job' => $job,
        ];
    }

    /**
     * @param User $user
     *
     * @Route("/client/list/{id}", name="rdw_job.job_admin.client_list", requirements={"id" = "\d+"})
     * @Template()
     * @Method({"GET"})
     *
     * @throws NotFoundHttpException
     * @return array
     */
    public function clientListAction(User $user)
    {
        $this->isGrantedForList($user);

        return [
            'user' => $user,
        ];
    }

    /**
     * @param int $id
     *
     * @Route("/activate/{id}", name="rdw_job.job_admin.activate", requirements={"id" = "\d+"})
     * @Template()
     *
     * @throws NotFoundHttpException
     * @return array
     */
    public function activateAction($id)
    {
        $job = $this->getJobManager()->getById($id);

        if (! $job) {
            throw new NotFoundHttpException(sprintf('The job with id "%d" does not exist.', $id));
        }

        $this->isGrantedLocal($job);

        $form = $this->createForm(new JobActivateType(), $job);

        return [
            'form' => $form->createView(),
            'job' => $job,
            'id' => $id,
        ];
    }

    /**
     * @param int $id
     *
     * @Route("/deactivate/{id}", name="rdw_job.job_admin.deactivate", requirements={"id" = "\d+"})
     * @Template()
     *
     * @throws NotFoundHttpException
     * @return array
     */
    public function deactivateAction($id)
    {
        $job = $this->getJobManager()->getById($id);

        if (! $job) {
            throw new NotFoundHttpException(sprintf('The job with id "%d" does not exist.', $id));
        }

        $this->isGrantedLocal($job);

        return [
            'job' => $job,
            'id' => $id,
        ];
    }

    /**
     * @param int $id
     *
     * @Route("/save-as-deactivate/{id}", name="rdw_job.job_admin.mark_as_un_activate", requirements={"id" = "\d+"})
     *
     * @throws NotFoundHttpException
     * @return RedirectResponse
     */
    public function markAsUnActiveAction($id)
    {
        $job = $this->getJobManager()->getById($id);

        if (! $job) {
            throw new NotFoundHttpException(sprintf('The job with id "%d" does not exist.', $id));
        }

        $this->isGrantedLocal($job);
        $this->getJobManager()->setAsUnActive($job);

        return new RedirectResponse($this->generateUrl('rdw_job.job_admin.client_list', ['userId' => $job->getUser()->getId()]));
    }

    /**
     * @param Request $request
     * @param int     $id
     *
     * @Route("/save-status/{id}",name="rdw_job.job_admin.save_status", requirements={"id" = "\d+"})
     * @Method({"POST"})
     *
     * @return JsonResponse
     * @throws NotFoundHttpException
     */
    public function saveStatusAction(Request $request, $id)
    {
        $data['success'] = false;
        $response = new JsonResponse();

        $job = $this->getJobManager()->getById($id);

        if (! $job) {
            throw new NotFoundHttpException(sprintf('Job with id %d was not found.', $id));
        }

        $this->isGrantedLocal($job);

        $form = $this->createForm(new JobActivateType(), $job);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = $event = new JobEvent($form->getData());
            $this->getEventDispatcher()->dispatch(RDWJobEvents::JOB_PUBLISH, $event);

            $data['url'] = $this->generateUrl('rdw_job.job_admin.client_list', ['userId' => $job->getUser()->getId()]);
            $data['success'] = true;
        }

        $data['content'] = $this->renderView('RDWJobBundle:JobAdmin:_activate_form.html.twig', [
            'form' => $form->createView(),
            'job' => $job,
            'id' => $id,
        ]);

        $response->setData($data);

        return $response;
    }

    /**
     * @param Request $request
     * @param Job     $job
     *
     * @Route("/edit/{id}", name="rdw_job.job_admin.edit", requirements={"id" = "\d+"})
     * @Method({"GET", "POST"})
     * @Template()
     *
     * @return array|RedirectResponse
     * @throws NotFoundHttpException
     */
    public function editAction(Request $request, Job $job)
    {
        $this->isGrantedLocal($job);

        $job->setOldStatus($job->getStatus());

        $form = $this->createForm('rdw_job_admin', $job);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new JobEvent($form->getData());
            $this->getEventDispatcher()->dispatch(RDWJobEvents::JOB_APPROVE, $event);

            $userEvent = new SingleUserEvent($job->getUser());
            $this->getEventDispatcher()->dispatch(RDWUserEvents::UPDATE_USER_PROFILE_PERCENT, $userEvent);

            if (! $request->query->get('redirectUrl') && $event->getResponse() instanceof Response) {
                return $event->getResponse();
            }

            $url = $request->query->get('redirectUrl')
                ? $request->query->get('redirectUrl')
                : $this->generateUrl('rdw_job.job_admin.edit', ['id' => $job->getId()]);

            return new RedirectResponse($url);
        }

        return [
            'form' => $form->createView(),
            'job' => $job,
        ];
    }

    /**
     * @param int $id
     *
     * @Route("/view/{id}", name="rdw_job.job_admin.view", requirements={"id" = "\d+"})
     * @Method({"GET", "POST"})
     * @Template()
     *
     * @return array|RedirectResponse
     * @throws NotFoundHttpException
     */
    public function viewAction($id)
    {
        $job = $this->getJobManager()->getById($id);
        $job->setOldStatus($job->getStatus());

        if (! $job) {
            throw new NotFoundHttpException(sprintf('The job with id "%d" does not exist.', $id));
        }

        $this->isGrantedLocal($job);

        $form = $this->createForm('rdw_job_admin', $job, ['disabled' => true]);

        return [
            'form' => $form->createView(),
            'job' => $job,
        ];
    }

    /**
     * @param Request $request
     * @param int     $id
     *
     * @Route("/block/{id}", name="rdw_job.job_admin.block", requirements={"id" = "\d+"})
     * @Method({"GET", "POST"})
     * @Template()
     *
     * @return array|RedirectResponse
     * @throws NotFoundHttpException
     */
    public function blockAction(Request $request, $id)
    {
        $this->isGrantedAsCensor();

        $job = $this->getJobManager()->getById($id);

        if (! $job) {
            throw new NotFoundHttpException(sprintf('The job with id "%d" does not exist.', $id));
        }

        $form = $this->createForm('rdw_job_block', $job);
        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var SubmitButton $cancelButton */
            $cancelButton = $form->get('cancel');

            if ($cancelButton->isClicked()) {
                return new RedirectResponse($this->generateUrl('rdw_job.job_admin.edit', ['id' => $id]));
            } else {
                $dispatcher = $this->getEventDispatcher();

                $event = new BlockEvent($form->getData(), $this->getUser());
                $dispatcher->dispatch(RDWJobEvents::JOB_BLOCK, $event);

                return new RedirectResponse($this->generateUrl('rdw_job.job_admin.list'));
            }
        }

        return [
            'form' => $form->createView(),
            'job' => $job,
        ];
    }

    /**
     * @param Job $job
     *
     * @Route("/delete/{id}", requirements={"id" = "\d+"}, name="rdw_job.job_admin.delete")
     * @Method({"GET"})
     *
     * @throws NotFoundHttpException
     *
     * @return RedirectResponse
     */
    public function deleteAction(Job $job)
    {
        $this->isGrantedAsCensor();

        if (! $job) {
            throw new NotFoundHttpException('The job with does not exist');
        }

        $this->getJobManager()->delete($job);

        return new RedirectResponse($this->generateUrl('rdw_job.job_admin.list'));
    }

    /**
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    private function isGrantedAsCensor()
    {
        if (! $this->container->get('security.context')->isGranted('ROLE_CENSOR')) {
            throw new AccessDeniedException();
        }
    }

    /**
     * @param Job $job
     */
    private function isGrantedLocal(Job $job)
    {
        $isAllowed = true;

        if (! $this->container->get('security.context')->isGranted('ROLE_CENSOR')) {
            if (! $job->getUser()->getManager() || $job->getUser()->getManager()->getId() != $this->getUser()->getId()) {
                $isAllowed = false;
            }
        }

        if (! $isAllowed) {
            throw new AccessDeniedException();
        }
    }

    /**
     * @param User $user
     */
    private function isGrantedForList(User $user)
    {
        $isAllowed = true;

        if (! $this->container->get('security.context')->isGranted('ROLE_CENSOR')) {
            if (! $user->isUserManager($this->getUser())) {
                $isAllowed = false;
            }
        }

        if (! $isAllowed) {
            throw new AccessDeniedException();
        }
    }

    /**
     * @return \RDW\Bundle\JobBundle\Service\JobManager
     */
    private function getJobManager()
    {
        return $this->container->get('rdw_job.service.job_manager');
    }

    /**
     * @return \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    private function getEventDispatcher()
    {
        return $this->container->get('event_dispatcher');
    }
}
