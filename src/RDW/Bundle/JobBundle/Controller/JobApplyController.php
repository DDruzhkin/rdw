<?php

namespace RDW\Bundle\JobBundle\Controller;

use RDW\Bundle\AppBundle\Event\GenericGuestEvent;
use RDW\Bundle\JobBundle\Entity\Apply;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Event\ApplyEvent;
use RDW\Bundle\JobBundle\Event\MultiApplyEvent;
use RDW\Bundle\JobBundle\Form\Type\JobApplyCommentType;
use RDW\Bundle\JobBundle\Form\Type\JobApplyFilterType;
use RDW\Bundle\JobBundle\Form\Type\JobApplyType;
use RDW\Bundle\JobBundle\Form\Type\MultiJobApplyType;
use RDW\Bundle\JobBundle\Model\ApplyFilter;
use RDW\Bundle\JobBundle\RDWJobEvents;
use RDW\Bundle\JobBundle\Service\JobApplyManager;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\User;
use RDW\Bundle\UserBundle\Form\Type\FavoriteCollectionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class JobAbuseController
 *
 * @package RDW\Bundle\JobBundle\Controller
 *
 * @Route("/job")
 */
class JobApplyController extends Controller
{
    /**
     * @param Job $job
     *
     * @Route("/apply/{id}", name="rdw_job.job.apply", requirements={"id" = "\d+"})
     * @Method({"GET"})
     * @Template()
     *
     * @return Response|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function applyAction(Job $job)
    {
        $user = $this->getUser();
        $apply = $this->getJobApplyManager()->createApplyFromJobAndUser($job, $user);

        $form = $this->createForm(new JobApplyType($user), $apply);

        return [
            'form' => $form->createView(),
            'id' => $job->getId(),
            'apply' => $apply,
        ];
    }

    /**
     * @param Request $request
     *
     * @Route("/multiple-apply", name="rdw_job.job_apply.multiple_create")
     * @Method({"POST"})
     * @Template()
     *
     * @return Response
     */
    public function multiCreateAction(Request $request)
    {
        $data['success'] = false;
        $data['errors'] = [];
        $response = new JsonResponse();

        $user = $this->getUser();

        if (! $user) {
            $data['errors'][] = 'User not found';
            $response->setData($data);

            return $response;
        }

        $ids = $request->request->get('favorite_collection', ['id' => []])['id'];
        $favoritesForm = $this->createForm(new FavoriteCollectionType($ids, 'job'));
        $favoritesForm->handleRequest($request);

        if ($favoritesForm->isValid()) {
            $jobs = $this->container
                ->get('rdw_user.service.favorite_manager')
                ->getJobsFromFavorites($favoritesForm->get('id')->getData());

            $apply = $this->container->get('rdw_job.service.job_apply_manager')->createMultipleApplies($user, $jobs);
            $form = $this->createForm(new MultiJobApplyType($user, $this->get('doctrine.orm.entity_manager')), $apply);

            $data['success'] = true;
            $data['content'] = $this->renderView('RDWJobBundle:JobApply:multiCreate.html.twig', [
                'form' => $form->createView(),
            ]);
        } else {
            $data['errors'] = $favoritesForm->getErrors();
        }

        $response->setData($data);

        return $response;
    }

    /**
     * @param Request $request
     *
     * @Route("/multiple-save", name="rdw_job.job_apply.multiple_save")
     * @Method({"POST"})
     *
     * @return Response
     */
    public function multiSaveAction(Request $request)
    {
        $data['success'] = false;
        $data['errors'] = [];
        $response = new JsonResponse();

        $sender = $this->getUser();

        if (! $sender) {
            $data['errors'][] = 'Sender not found';
            $response->setData($data);

            return $response;
        }

        $apply = $this->container->get('rdw_job.service.job_apply_manager')->createMultipleApplies($sender);

        $form = $this->createForm(new MultiJobApplyType($sender, $this->get('doctrine.orm.entity_manager')), $apply);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $apply = $form->getData();
            $jobs = $apply->getJobs();

            $event = new MultiApplyEvent($jobs, $apply);
            $this->container->get('event_dispatcher')->dispatch(RDWJobEvents::MULTI_APPLY_SUCCESS, $event);

            $data['success'] = true;

            if ($this->container->get('security.context')->isGranted('ROLE_REGISTERED')) {
                $this->get('braincrafted_bootstrap.flash')->success('Your apply for jobs was sent successfully');
            } else {
                $data['popup'] = $this->getRegistrationPopup();
            }
        }

        $data['content'] = $this->renderView('RDWJobBundle:JobApply:_form.html.twig', [
            'form' => $form->createView(),
            'action' => $this->generateUrl('rdw_job.job_apply.multiple_save'),
        ]);

        $response->setData($data);

        return $response;
    }

    /**
     * @param Request $request
     *
     * @Route("/apply/list", name="rdw_job.job_apply.list")
     * @Template()
     * @Method({"GET"})
     *
     * @return array
     */
    public function listAction(Request $request)
    {
        $jobId = $request->query->get('job', 0);
        $job = $this->container->get('rdw_job.service.job_manager')->getById($jobId);

        $this->isGrantedLocal($job);

        $userId = $request->query->get('user', 0);
        $user = $this->container->get('rdw_user.service.user_manager')->findUserBy(['id' => $userId]);

        $applyFilter = new ApplyFilter();
        $applyFilter->setUser($user);

        $filterForm = $this->createForm(new JobApplyFilterType($user), $applyFilter);
        $filterForm->handleRequest($request);

        $pagination = $this->getJobApplyManager()->getPaginatedList(
            $applyFilter,
            $this->get('request')->query->get('page', 1),
            $this->getPerPageNumber()
        );

        return [
            'pagination' => $pagination,
            'form' => $filterForm->createView(),
            'job' => $job,
        ];
    }

    /**
     * @param Apply $apply
     *
     * @Route("/apply/comment/{id}", name="rdw_job.job_apply.comment", requirements={"id" = "\d+"})
     * @Method({"GET"})
     * @Template()
     *
     * @return array
     *
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function commentAction(Apply $apply)
    {
        $this->isGrantedLocal($apply->getJob());

        $form = $this->createForm(new JobApplyCommentType(), $apply);

        return [
            'form' => $form->createView(),
            'apply' => $apply,
        ];
    }

    /**
     * @param Request $request
     * @param Apply   $apply
     *
     * @Route("/apply/save-comment/{id}", name="rdw_job.job_apply.save_comment", requirements={"id" = "\d+"})
     * @Method({"POST"})
     *
     * @return array
     *
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function saveCommentAction(Request $request, Apply $apply)
    {
        $this->isGrantedLocal($apply->getJob());

        $data['success'] = false;

        $form = $this->createForm(new JobApplyCommentType(), $apply);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getJobApplyManager()->update($apply);
            $this->get('braincrafted_bootstrap.flash')->success('Your comment saved');
            $data['success'] = true;
        }

        $data['content'] = $this->renderView('RDWJobBundle:JobApply:_form_comment.html.twig', [
            'form' => $form->createView(),
            'apply' => $apply,
        ]);

        $response = new JsonResponse();
        $response->setData($data);

        return $response;
    }

    /**
     * @param Request $request
     * @param Job     $job
     *
     * @Route("/save-apply/{id}",name="rdw_job.job.save_apply", requirements={"id" = "\d+"})
     * @Method({"POST"})
     *
     * @return JsonResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function saveApplyAction(Request $request, Job $job)
    {
        $data = [];
        $data['success'] = true;

        $jobApplyManager = $this->get('rdw_job.service.job_apply_manager');

        $user = $this->getUser();
        $apply = $jobApplyManager->createApplyFromJobAndUser($job, $user);

        $form = $this->createForm(new JobApplyType($user), $apply);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new GenericGuestEvent($request, $apply);
            $this->container->get('event_dispatcher')->dispatch('rdw_app.job_apply.pre_create', $event);

            if ($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
                $event = new ApplyEvent($apply);
                $this->container->get('event_dispatcher')->dispatch(RDWJobEvents::APPLY_SUCCESS, $event);

                $apply->setUser($this->getUser());
                $data['updated'] = $jobApplyManager->update($apply);

                $this->get('braincrafted_bootstrap.flash')->success('Your apply for job was sent successfully');
            }

            $data['success'] = true;
        } else {
            $data['content'] = $this->renderView('RDWJobBundle:JobApply:_form.html.twig', [
                'form' => $form->createView(),
                'id' => $job->getId(),
                'apply' => $apply,
                'action' => $this->generateUrl('rdw_job.job.save_apply', ['id' => $job->getId()]),
            ]);

            $data['success'] = false;
        }

        $response = new JsonResponse();
        $response->setData($data);

        return $response;
    }

    /**
     * @param Job $job
     *
     * @return bool
     *
     */
    private function isGrantedLocal(Job $job = null)
    {
        $user = $this->getUser();

        // session user must the same loggedInUser or manager of loggedInUser
        if (! $user instanceof RegisteredUser
            || (
                $job && (
                    $user->getId() != $job->getUser()->getId()
                    && ! $job->getUser()->isUserManager($user)
                )
            )
        ) {
            throw new AccessDeniedException();
        }

        return true;
    }

    /**
     * @return JobApplyManager
     */
    private function getJobApplyManager()
    {
        return $this->get('rdw_job.service.job_apply_manager');
    }

    /**
     * Extract per page param from query string
     *
     * @return int
     */
    private function getPerPageNumber()
    {
        $current = $this->get('request')->query->get('per_page');
        $options = $this->container->getParameter('rdw_job.list.per_page.options');

        $perPage = ($current && in_array($current, $options)) ? $current : $options[0];

        return $perPage;
    }
}
