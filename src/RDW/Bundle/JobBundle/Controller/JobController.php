<?php

namespace RDW\Bundle\JobBundle\Controller;

use RDW\Bundle\AppBundle\Event\GenericGuestEvent;
use RDW\Bundle\HistoryBundle\Entity\History;
use RDW\Bundle\HistoryBundle\RDWHistoryEvents;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Event\JobEvent;
use RDW\Bundle\JobBundle\Form\Type\JobCollectionType;
use RDW\Bundle\JobBundle\Form\Type\JobType;
use RDW\Bundle\JobBundle\Model\JobFilter;
use RDW\Bundle\JobBundle\Form\Type\JobFilterType;
//use RDW\Bundle\JobBundle\Form\Type\JobSidebarFilterType;
use RDW\Bundle\JobBundle\RDWJobEvents;
use RDW\Bundle\JobBundle\Service\JobManager;
use RDW\Bundle\ServiceBundle\Entity\Service;
use RDW\Bundle\SlugBundle\Entity\Slug;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use RDW\Bundle\JobBundle\Model\ApplyFilter;
use RDW\Bundle\JobBundle\Form\Type\JobViewFilterType;
use RDW\Bundle\LocationBundle\Entity\City;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use RDW\Bundle\JobBundle\Form\Type\JobShowContactsFilterType;
use Symfony\Component\VarDumper\VarDumper;

/**
 * Class JobController
 *
 * @package RDW\Bundle\JobBundle\Controller
 *
 */
class JobController extends Controller
{

    /**
     * @var bool
     */
    protected $isPathWithSlug;
    protected $request;

    /**
     * @Route("/jobs", name="rdw_job.job.index")
     * @Template("RDWAppBundle:Default:index.html.twig")
     *
     * @return array
     */
    public function indexAction()
    {
        return [];
    }

    /**
     * @param Request $request
     *
     * @Route(
     *     "/vakansii", name="rdw_job.job.list",
     *     host="{domain}",
     *     defaults={"domain":"%base_domain%"},
     *     requirements={"domain":"%base_domain%"}
     *     )
     *
     * Yandex constantly crawls this URL
     * @Route("/rabota", name="rdw_job.job.list.yandex")
     *
     * @return Response
     */
    public function listAction(Request $request)
    {
        $filter = new JobFilter();
        $filter->setStatuses([Job::STATUS_ACTIVE]);

        $filterForm = $this->createForm('jobs', $filter);
        $filterForm->handleRequest($request);

        // save filter object to session
        $session = $request->getSession();
        $session->set('job_filter', $filterForm->getData());
        $sphinx = $this->container->get('sphinx_search_service');

        $limit = $this->getPerPageNumber();
        $page = $request->query->get('page', 1);
        $offset = abs($page - 1) * $limit;
        $orderBy = $filter->getOrderByFromRequestQuery($request->query->all());

        $sphinx->filterJobs($filterForm, $orderBy, $limit, $offset);

        $items = $this->container
            ->get('doctrine')
            ->getRepository('RDWJobBundle:Job')
            ->getByIds($sphinx->getResults());

        $pagination = $this->get('knp_paginator')->paginate($items, 1, $limit);
        $pagination->setTotalItemCount($sphinx->getTotalFound());
        $pagination->setCurrentPageNumber($page);

        $specializationPa = $mostPopularSpecializationPa = [];
        if ($this->isPathWithSlug) {
            if (!empty($request->query->get('jobs')['professionalArea'])) {
                $professionalAreaId = $request->query->get('jobs')['professionalArea'];
                $professionalArea = $this->getDoctrine()->getRepository('RDWJobBundle:ProfessionalArea')->find($professionalAreaId);
                if ($professionalArea) {
                    if (!$professionalArea->isParent()) {
                        $professionalArea = $professionalArea->getParent();
                        $professionalAreaId = $professionalArea->getId();
                    }
                    $popularSpecializationPa = $this->get('rdw_job.provider.professional_area')->getPopularSpecializationInPa($professionalAreaId, 1, 100);
                    if ($popularSpecializationPa) {
                        $mostPopularSpecializationPa = array_chunk(array_slice($popularSpecializationPa, 0, 8), 4);
                        $specializationPa = array_chunk($popularSpecializationPa, ceil(count($popularSpecializationPa) / 4));
                    }
                }
            }

        }

        $data = [
            'pagination' => $pagination,
            'form' => $filterForm,
            'specializationPa' => $specializationPa,
            'popularSpecializationPa' => $mostPopularSpecializationPa,
        ];

        $viewFile = ($request->query->get('renderedAsEsi'))
            ? 'RDWJobBundle:Job:_listContent.html.twig'
            : 'RDWJobBundle:Job:list.html.twig';

        return $this->render($viewFile, $data);
    }

    /**
     * @Cache(smaxage="600")
     *
     * @return Response
     */
    public function vipJobsAction()
    {
        $limit = $this->container->getParameter('rdw_job.vip_jobs_limit');
        $jobs = $this->getDoctrine()->getRepository('RDWOrderBundle:OrderItemJob')->findAllForVipJobs($limit);

        return $this->render('RDWJobBundle:Job:vipJobs.html.twig', ['jobs' => $jobs]);
    }

    /**
     * @Route("/job/my-list", name="rdw_job.job.my_list")
     * @Template()
     * @Method({"GET"})
     *
     * @return array|RedirectResponse
     */
    public function myListAction()
    {
        $this->isGrantedLocal();

        /** @var User $user */
        $user = $this->getUser();

        $pagination = $this->getJobManager()->getPaginatedUserJobsList(
            $user,
            $this->get('request')->query->get('page', 1),
            $this->getPerPageNumber()
        );

        $form = $this->createForm(new JobCollectionType($pagination->getItems()));
        $services = $this->get('rdw_service.service.service_manager')->getServicesAllowedToOrderFromList(Service::FOR_EMPLOYER);

        return [
            'pagination' => $pagination,
            'services' => $services,
            'form' => $form->createView(),
        ];
    }

    /**
     * @param RegisteredUser $user
     * @param Request $request
     *
     * @Route("/job/users-who-viewed/{id}", requirements={"id" = "\d+"}, name="rdw_job.job.users_who_viewed_list")
     * @Template()
     *
     * @return array
     */
    public function usersViewedAction(RegisteredUser $user, Request $request)
    {
        $jobId = $request->query->get('job', 0);
        $job = $this->container->get('rdw_job.service.job_manager')->getById($jobId);

        if ($job) {
            $this->isOwner($job);
        }

        $applyFilter = new ApplyFilter();
        $applyFilter->setUser($user);

        $filterForm = $this->createForm(new JobViewFilterType($user), $applyFilter);
        $filterForm->handleRequest($request);

        $pagination = $this->get('rdw_job.service.job_view_manager')->getPaginatedList(
            $applyFilter,
            $this->get('request')->query->get('page', 1),
            $this->getPerPageNumber()
        );

        return [
            'pagination' => $pagination,
            'form' => $filterForm->createView(),
            'job' => $job,
            'user' => $user,
        ];
    }

    /**
     * @param RegisteredUser $user
     * @param Request $request
     *
     * @Route("/job/users-who-showed-contacts/{id}", requirements={"id" = "\d+"}, name="rdw_job.job.users_who_showed_contacts_list")
     * @Template()
     *
     * @return array
     */
    public function usersShowedContactsAction(RegisteredUser $user, Request $request)
    {
        $jobId = $request->query->get('job', 0);
        $job = $this->container->get('rdw_job.service.job_manager')->getById($jobId);

        if ($job) {
            $this->isOwner($job);
        }

        $applyFilter = new ApplyFilter();
        $applyFilter->setUser($user);

        $filterForm = $this->createForm(new JobShowContactsFilterType($user), $applyFilter);
        $filterForm->handleRequest($request);

        $pagination = $this->get('rdw_job.service.job_show_contacts_manager')->getPaginatedList(
            $applyFilter,
            $this->get('request')->query->get('page', 1),
            $this->getPerPageNumber()
        );

        return [
            'pagination' => $pagination,
            'form' => $filterForm->createView(),
            'job' => $job,
            'user' => $user,
        ];
    }

    /**
     * @Template()
     *
     * @return array
     */
    public function lastViewedBlockAction()
    {
        $limit = $this->container->getParameter('rdw_job.last_viewed_block_limit');
        $jobs = $this->container
            ->get('doctrine')
            ->getRepository('RDWJobBundle:Job')
            ->getHavingViews($this->getUser(), $limit);

        return [
            'jobs' => $jobs,
        ];
    }

    /**
     * @param int $id
     *
     * @Route("/job/my-view/{id}", name="rdw_job.job.my_view", requirements={"id" = "\d+"})
     * @Template()
     * @Method({"GET"})
     *
     * @return array
     * @throws NotFoundHttpException
     */
    public function myViewAction($id)
    {
        $job = $this->getJobManager()->getById($id);

        if (!$job || ($job && !$this->isOwner($job))) {
            throw new NotFoundHttpException('Job not found');
        }

        $form = $this->createForm(new JobType(), $job, ['disabled' => true]);
        $services = $this->get('rdw_service.service.service_manager')->getServicesAllowedToOrderFromList(Service::FOR_EMPLOYER);

        return [
            'job' => $job,
            'form' => $form->createView(),
            'services' => $services,
        ];
    }

    /**
     * @param integer $id
     *
     * @Route("/vakansii/{slug}/{id}", name="rdw_job.job.view", requirements={"id" = "\d+"})
     * @Template()
     * @Method({"GET", "POST"})
     *
     * Cache(smaxage="300")
     *
     * @return array
     * @throws NotFoundHttpException
     */
    public function viewAction($id)
    {
        $job = $this->getJobManager()->getByIdIncludingDeleted($id);

        if (!$job) {
            throw new NotFoundHttpException(sprintf('The job with id "%d" does not exist.', $id));
        }

        $relatedJobs = $this->getJobManager()->getRelated(
            $job,
            $this->container->getParameter('rdw_job.job_view.related.per_page'),
            $this->getUser()
        );

        $event = new JobEvent($job, $this->getUser());
        $this->container->get('event_dispatcher')->dispatch(RDWJobEvents::JOB_VIEW, $event);

        $this->getJobManager()->eventShowContact($job, $this->getUser());

        return [
            'job' => $job,
            'relatedJobs' => $relatedJobs,
            'isOwner' => $this->isOwner($job),
        ];
    }

    /**
     * @param Request $request
     *
     * @Route("/job/create", name="rdw_job.job.create")
     * @Route("/job/guest-create", name="rdw_job.job.guest_create")
     * @Template()
     * @Security("is_anonymous() or has_role('ROLE_EMPLOYER')")
     *
     * @return array|RedirectResponse
     */
    public function createAction(Request $request)
    {
        $job = $this->container->get('session')->get('ad');
        if ($job) {
            $job = $this->container->get('doctrine.orm.entity_manager')->merge($job);
        } else {
            $job = $this->getJobManager()->create($this->getUser());
        }

        $form = $this->createForm(new JobType(), $job);
        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
                $url = $this->handleSubmit($job, $form, History::ACTION_CREATE, RDWHistoryEvents::CREATE_OBJECT);

                return $this->redirect($url);
            } else {
                $event = new GenericGuestEvent($request, $job);
                $this->container->get('event_dispatcher')->dispatch('rdw_app.ad.pre_create', $event);

                return $this->redirect($this->generateUrl('rdw_job.job.guest_create'));
            }
        }

        return [
            'form' => $form->createView(),
            'job' => $job,
            'showRegistrationPopup' => ('rdw_job.job.guest_create' == $request->get('_route')),
        ];
    }

    /**
     * @param Request $request
     * @param Job $job
     *
     * @Route("/job/edit/{id}", name="rdw_job.job.edit", requirements={"id" = "\d+"})
     * @Template()
     *
     * @return array|RedirectResponse
     * @throws NotFoundHttpException
     */
    public function editAction(Request $request, Job $job)
    {
        $this->isGrantedLocal();

        if (!$job || ($job && !$this->isOwner($job))) {
            throw new NotFoundHttpException('Job not found');
        }

        // can edit only jobs with statuses new, waiting, inactive, active
        if (!$job->isEditable()) {
            return $this->redirect($this->get('router')->generate('rdw_job.job.my_list'));
        }

        $form = $this->createForm(new JobType(), $job);
        $form->handleRequest($request);

        // show validation errors without for submit
        if ($request->query->get('validate')) {
            $errors = $this->get('validator')->validate($job, ['publish']);
            foreach ($errors as $error) {
                $property = $error->getPropertyPath();
                $form->get($property)->addError(new FormError($error->getMessage()));
            }
        }

        if ($form->isValid()) {
            $url = $this->handleSubmit($job, $form, History::ACTION_EDIT, RDWHistoryEvents::EDIT_OBJECT);

            return $this->redirect($url);
        }

        return [
            'form' => $form->createView(),
            'job' => $job,
        ];
    }

    /**
     * @param Job $job
     *
     * @Route("/job/delete/{id}", name="rdw_job.job.delete", requirements={"id" = "\d+"})
     *
     * @return RedirectResponse
     */
    public function deleteAction(Job $job)
    {
        $this->isGrantedLocal();

        if ($job && $this->isOwner($job)) {
            $this->getJobManager()->delete($job);

            $this->container->get('braincrafted_bootstrap.flash')->error(
                $this->get('translator')->trans('Successfully deleted')
            );

            $this->get('rdw_history.service.history_manager')->logEvent($this->getUser(), History::ACTION_DELETE, $job, RDWHistoryEvents::DELETE_OBJECT);
        }

        return $this->redirect($this->generateUrl('rdw_job.job.my_list'));
    }

    /**
     * @param Request $request
     * @param RegisteredUser $user
     *
     * @Route("/job/multiple-delete/{id}", name="rdw_job.job.multiple_delete", requirements={"id" = "\d+"})
     *
     * @return RedirectResponse
     * @throws AccessDeniedException
     */
    public function multipleDeleteAction(Request $request, RegisteredUser $user)
    {
        $loggedInUser = $this->getUser();

        if ($loggedInUser->getId() != $user->getId() && !$user->isUserManager($loggedInUser)) {
            throw new AccessDeniedException();
        }

        $ids = $request->request->get('job_collection', ['id' => []])['id'];
        $form = $this->createForm(new JobCollectionType($ids));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getJobManager()->multipleDelete($form->get('id')->getData(), $loggedInUser);

            $this->container->get('braincrafted_bootstrap.flash')->success(
                $this->get('translator')->trans('Successfully deleted')
            );
        }

        return $this->redirect($this->generateUrl('rdw_job.job.my_list'));
    }

    /**
     * @Cache(smaxage="1800")
     *
     * @return Response
     */
    public function carouselAction()
    {
        $limit = $this->container->getParameter('rdw_job.job.carousel_limit');
        $orderItems = $this->getDoctrine()->getRepository('RDWOrderBundle:OrderItemJob')->findAllForCarousel($limit);

        return $this->render('RDWJobBundle:Job:carousel.html.twig', ['orderItems' => $orderItems]);
    }

    /**
     * @param Request $request
     * @param Job $job
     *
     * @Route("/job/publish/{id}", name="rdw_job_publish", requirements={"id" = "\d+"})
     *
     * @return RedirectResponse
     * @throws AccessDeniedException
     */
    public function publishAction(Request $request, Job $job)
    {
        if (!$this->isOwner($job)) {
            throw new AccessDeniedException();
        }

        $flashService = $this->container->get('braincrafted_bootstrap.flash');
        $errors = $this->get('validator')->validate($job, ['publish']);

        $hasPublishing = $this->get('rdw_job.service.job_manager')->hasActivePublishingService($job);

        if (0 === count($errors) && $hasPublishing) {
            $this->get('rdw_job_publishing_service')->publish($job);
            $flashService->success(
                $this->get('translator')->trans('Job offer will be available on the site after site administrators confirmation.')
            );

            $url = $this->generateUrl('rdw_job.job.my_list');
        } elseif (count($errors) > 0) {
            $flashService->error(
                $this->get('translator')->trans('Please fix errors before publishing this job')
            );
            $url = $this->generateUrl('rdw_job.job.edit', ['id' => $job->getId(), 'validate' => true]);
        } else {
            $job->setStatus(Job::STATUS_PUBLISHED);
            $this->getJobManager()->update($job);

            $flashService->error(
                $this->get('translator')->trans('Please order publishing service')
            );
            $url = $this->generateUrl('rdw_service.service.matrix_employer', ['id' => $job->getUser()->getId(), 'job_collection[id][]' => $job->getId()]);
        }

        return new RedirectResponse($url);
    }

    /**
     * @param Request $request
     * @param Job $job
     *
     * @Route("/job/deactivate/{id}", name="rdw_job_deactivate", requirements={"id" = "\d+"})
     *
     * @return RedirectResponse
     * @throws AccessDeniedException
     */
    public function deactivateAction(Request $request, Job $job)
    {
        if (!$this->isOwner($job) || !$job->isEditable()) {
            throw new AccessDeniedException();
        }

        $job->setStatus(Job::STATUS_INACTIVE);
        $this->getJobManager()->update($job);

        $this->container
            ->get('braincrafted_bootstrap.flash')
            ->success(
                $this->get('translator')->trans('Job offer deactivated.')
            );

        return $this->redirect($this->generateUrl('rdw_job.job.my_list'));
    }

    /**
     * @Template()
     * @ParamConverter("city", class="RDWLocationBundle:City", options={"mapping": {"city": "titleInLatin"}})
     * @param City $city
     * @param Request $request
     *
     * @return Response
     * @throws \InvalidArgumentException
     */
    public function byCityRedirectAction(City $city, Request $request)
    {

        return new RedirectResponse(
            'http://rdw.by/' . $city->getTitleInLatin() . '/vakansii' . http_build_query($request->query->all()),
            301
        );
    }

    /**
     * @Template()
     * @ParamConverter("city", class="RDWLocationBundle:City", options={"mapping": {"city": "titleInLatin"}})
     * @param City $city
     * @param Request $request
     *
     * @return Response
     * @throws \InvalidArgumentException
     */
    public function byCityAction(City $city, Request $request)
    {

        $parameters = $request->query->has('jobs') ? $request->query->get('jobs') : [];
        if (isset($parameters['cities'])) {
            $cities = explode(',', $parameters['cities']);
            if (!in_array($city->getId(), $cities)) {
                return new RedirectResponse(
                    $this->generateUrl(
                        'rdw_job.job.list',
                        $request->query->all(),
                        UrlGeneratorInterface::ABSOLUTE_URL
                    )
                );
            }
        } else {
            $parameters['cities'] = implode(',', [$city->getId()]);
        }

        $request->query->set('jobs', $parameters);

        return $this->listAction($request);
    }

    /**
     * @Route("/vakansii/advanced", name="rdw_job.job.advanced")
     * @param Request $request
     *
     * @return Response
     * @throws NotFoundHttpException
     * @throws \InvalidArgumentException
     */
    public function advancedAction(Request $request)
    {

        $form = $this->createForm("jobs");
        $this->request = $request;
        $form->handleRequest($request);

        if($request->query->has('count')){
            $sphinx = $this->container->get('sphinx_search_service');
            return new Response($sphinx->getVacansyCount($form));
        }
        $data = [
            'form' => $form->createView(),
        ];

        return $this->render('RDWJobBundle:Job:_filter.html.twig', $data);
    }


    /**
     *
     * @return Response
     * @throws NotFoundHttpException
     * @throws \InvalidArgumentException
     */
    public function sidebarAction()
    {
        $form = $this->createForm("jobs");
        $master_request = Request::createFromGlobals();

        $form->handleRequest($master_request);
        //$form->handleRequest($this->request);
        $formdata = $form->all();
        $fields = [];
        foreach ($formdata as $field) {
            $fields[] = $field->getName();
        }
        $excluded_params = [
            'query',
            'salaryFrom',
            'salaryTo',
            'onlyWithSalary',
            'hideAgencies',
            'language',
            'updatedAt',
            'employerType'
        ];
        $query_all = $master_request->query->all();
        if (isset($query_all['jobs']))
            $params = array_keys(array_filter($query_all['jobs']));
        $params = !empty($params) ? array_merge($params, $excluded_params) : $excluded_params;
        $fields = array_diff($fields, $params);
        $sphinx = $this->container->get('sphinx_search_service');
        //echo "<pre>";
        //var_dump($sphinx->getVacancySidebar($form, $fields));
        //echo "</pre>";

        $data = [
            'form' => $form->createView(),
            'data' => $sphinx->getVacancySidebar($form, $fields)
        ];

        return $this->render('RDWJobBundle:Job:_sidebar_filter.html.twig', $data);
    }

    /**
     * @Route("/vakansii/{slug}", name="rdw_job.job.by_filter_value")
     * @ParamConverter("slug", class="RDWSlugBundle:Slug", options={"mapping": {"slug": "name"}})
     * @param Slug $slug
     * @param Request $request
     *
     * @return Response
     * @throws NotFoundHttpException
     * @throws \InvalidArgumentException
     */
    public function byFilterValueAction(Slug $slug, Request $request)
    {
        $parameters = $request->query->has('jobs') ? $request->query->get('jobs') : [];

        switch ($slug->getType()) {
            case 'RDW\Bundle\UserBundle\Entity\Scope':
                $key = 'scope';
                break;
            case 'RDW\Bundle\JobBundle\Entity\EducationType':
                $key = 'education';
                break;
            case 'RDW\Bundle\JobBundle\Entity\WorkExperienceType':
                $key = 'workExperience';
                break;
            case 'RDW\Bundle\JobBundle\Entity\WorkScheduleType':
                $key = 'workScheduleType';
                break;
            case 'RDW\Bundle\JobBundle\Entity\EmploymentType':
                $key = 'employmentType';
                break;
            case 'RDW\Bundle\JobBundle\Entity\Position':
                $key = 'position';
                break;
            case 'RDW\Bundle\JobBundle\Entity\ProfessionalArea':
                $key = 'professionalArea';
                break;
            default:
                throw $this->createNotFoundException();
        }

        $parameters[$key] = $slug->getItemId();
        $request->query->set('jobs', $parameters);
        $this->isPathWithSlug = true;

        return $this->listAction($request);
    }

    /**
     * Extract per page param from query string
     *
     * @return int
     */
    private function getPerPageNumber()
    {
        $current = $this->get('request')->query->get('per_page');
        $options = $this->container->getParameter('rdw_job.list.per_page.options');

        $perPage = ($current && in_array($current, $options)) ? $current : $options[0];

        return $perPage;
    }

    /**
     * @throws AccessDeniedException
     */
    private function isGrantedLocal()
    {
        if (!$this->container->get('security.context')->isGranted('ROLE_EMPLOYER')) {
            throw new AccessDeniedException();
        }
    }

    /**
     * @param Job $job
     *
     * @return bool
     */
    private function isOwner(Job $job)
    {
        $user = $this->getUser();

        if ($this->container->get('security.context')->isGranted('ROLE_EMPLOYER')
            && $user instanceof User
            && (
                $user->getId() == $job->getUser()->getId() // if owner
                || $job->getUser()->isUserManager($user) // is manager of job owner
            )
        ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return JobManager
     */
    private function getJobManager()
    {
        return $this->get('rdw_job.service.job_manager');
    }

    /**
     * @param Job $job
     * @param Form $form
     * @param string $historyEventAction
     * @param string $historyEventType
     *
     * @return string
     */
    private function handleSubmit(Job $job, Form $form, $historyEventAction, $historyEventType)
    {
        $dispatcher = $this->container->get('event_dispatcher');

        $event = new JobEvent($form->getData());

        /** @var SubmitButton $submitAndPublish */
        $submitAndPublish = $form->get('submitAndPublish');

        if ($submitAndPublish->isClicked()) {
            $dispatcher->dispatch(RDWJobEvents::JOB_PUBLISH, $event);
        } else {
            $dispatcher->dispatch(RDWJobEvents::JOB_SAVE, $event);
        }

        $url = (!$submitAndPublish->isClicked() || ($submitAndPublish->isClicked() && $this->get('rdw_job.service.job_manager')->hasActivePublishingService($job)))
            ? $this->get('router')->generate('rdw_job.job.my_list')
            : $this->get('router')->generate('rdw_service.service.matrix_employer', ['id' => $job->getUser()->getId(), 'job_collection' => ['id' => [$job->getId()]]]);

        $this->get('rdw_history.service.history_manager')->logEvent($this->getUser(), $historyEventAction, $job, $historyEventType);

        return $url;
    }


    /**
     * New jobs on main page
     *
     * @return array
     */
    public function dailyJobsAction()
    {
        $newDailyJobs = $this->getJobManager()->getThisDayJobs();

        return $this->render('RDWJobBundle:Job:dailyJobs.html.twig', ['newDailyJobs' => $newDailyJobs]);
    }
}
