<?php

namespace RDW\Bundle\JobBundle\Service;

use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;
use RDW\Bundle\JobBundle\Entity\JobShowContacts;
use RDW\Bundle\JobBundle\Repository\JobRepository;
use RDW\Bundle\OrderBundle\Entity\OrderItemJob;
use Symfony\Component\DependencyInjection\ContainerInterface;
use RDW\Bundle\AppBundle\Service\AdManager;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Model\JobFilter;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\Scope;
use RDW\Bundle\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class JobManager
 *
 * @package RDW\Bundle\JobBundle\Service
 */
class JobManager extends AdManager
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function create(RegisteredUser $user = null)
    {
        if ($user) {
            return $this->createFromUser($user);
        } else {
            return new Job();
        }
    }

    /**
     * @param RegisteredUser $user
     *
     * @return Job
     */
    public function createFromUser(RegisteredUser $user)
    {
        $job = new Job();
        $job->setEmployerType($user->getEmployerType());
        $job->setCompanyType($user->getCompanyType());
        $job->setCompanyTitle($user->getCompanyTitle());
        $job->setCompanyTradeMark($user->getCompanyTradeMark());
        $job->setCompanyUrl($user->getUrlAddress());
        $job->setCompanyInfo($user->getCompanyInfo());
        $job->setCities($user->getCities());
        $job->setAddress($user->getAddress());
        $job->setPhone($user->getPhone());
        $job->setFax($user->getFax());
        $job->setFbUrl($user->getFbContact());
        $job->setVkUrl($user->getVkContact());
        $job->setOkUrl($user->getOkContact());
        $job->setGpUrl($user->getGPlusContact());
        $job->setTwitterUrl($user->getTwitterContact());
        $job->setLinkedInUrl($user->getLinkedInContact());
        $job->setSkypeName($user->getSkypeContact());

        if (count($user->getCities()) > 0) {
            $job->setWorkingPlaceCity($user->getCities()->first());
        }

        if (($scope = $user->getScope()) instanceof Scope) {
            $job->setScope($scope);
        }

        $job->setYoutubeUrl($user->getYoutubeVideo());
        $job->setContactPerson($user->getContactPerson());
        $job->setContactPersonPatronymic($user->getContactPersonPatronymic());
        $job->setContactPersonSurname($user->getContactPersonSurname());
        $job->setContactPersonPhoto($user->getContactPersonPhoto());
        $job->setContactPersonEmail($user->getContactPersonEmail());

        $job->addContactPersonPhoneByString($user->getContactPersonPhone());

        $job->setPhoto($user->getPhoto());
        $job->setEmail($user->getEmail());

        $job->setUser($user);

        return $job;
    }

    /**
     * @param int $id
     *
     * @return Job
     */
    public function getById($id)
    {
        return $this->getRepository()->getById($id);
    }

    /**
     * @param int $id
     *
     * @return Job
     */
    public function getByIdIncludingDeleted($id)
    {
        $this->objectManager->getFilters()->disable('softdeleteable');
        $job = $this->getById($id);
        $this->objectManager->getFilters()->enable('softdeleteable');

        return $job;
    }

    /**
     * @param Job $job
     * @param string $status
     *
     * @return Job
     */
    public function changeStatus(Job $job, $status)
    {
        if (Job::STATUS_INACTIVE == $status) {
            $job->setValidTill(new \DateTime());
            $job->setStatus($status);
        }

        $this->update($job);

        return $job;
    }

    /**
     * @return JobRepository
     */
    private function getRepository()
    {
        return $this->objectManager->getRepository('RDWJobBundle:Job');
    }

    /**
     * @param User $user
     * @param int $page
     * @param int $limit
     *
     * @return PaginationInterface
     */
    public function getPaginatedUserJobsList(User $user, $page, $limit)
    {
        $query = $this->getRepository()->getUserJobsListQuery($user);

        $pagination = $this->paginator->paginate($query, $page, $limit);

        return $pagination;
    }

    /**
     * @param User $user
     *
     * @return array
     */
    public function getAllUserActiveJobs(User $user)
    {
        $query = $this->getRepository()->getUserJobsListQuery($user, ['status' => Job::STATUS_ACTIVE, 'hide_secret' => true]);
        $query->setMaxResults(50);

        return $query->getResult();
    }

    /**
     * @param User $user
     * @param string $order
     *
     * @return array
     */
    public function getOrderedUserActiveJobs(User $user, $order = 'ASC')
    {
        // Getting user jobs from default method.
        $companyJobs = $this->getAllUserActiveJobs($user);

        // Group jobs by the cities.
        $sortedCompanyJobs = [];
        if (!empty($companyJobs)) {
            foreach ($companyJobs as $job) {
                $sortedCompanyJobs[$job->getWorkingPlaceCity()->getPosition()][] = $job;
            }
            switch ($order) {
                case 'ASC':
                    ksort($sortedCompanyJobs);
                    break;
                case 'DESC':
                    krsort($sortedCompanyJobs);
            }
        }

        return $sortedCompanyJobs;
    }

    /**
     * @param User $user
     * @param string $order
     *
     * @return array
     */
    public function getCountUserActiveJobs(User $user)
    {
        // Getting user jobs from default method.
        $companyJobs = $this->getAllUserActiveJobs($user);

        return count($companyJobs);
    }

    /**
     * @param RegisteredUser $manager
     * @param int $page
     * @param int $limit
     * @param RegisteredUser $client
     *
     * @return PaginationInterface|null
     */
    public function getPaginatedListForManager(RegisteredUser $manager, $page, $limit, RegisteredUser $client = null)
    {
        $query = $this->getRepository()->getListForManagerQueryBuilder($manager, $client);

        return ($query) ? $this->paginator->paginate($query, $page, $limit) : $this->paginator->paginate([], $page, $limit);
    }

    /**
     * @param int $page
     * @param int $limit
     * @param JobFilter $filter
     *
     * @return PaginationInterface
     */
    public function getPaginatedList($page, $limit, JobFilter $filter = null)
    {
        $query = $this->getRepository()->getListQuery($filter);

        $pagination = $this->paginator->paginate($query, $page, $limit);

        return $pagination;
    }

    /**
     * @param JobFilter $filter
     * @param int $page
     * @param int $limit
     *
     * @return PaginationInterface
     */
    public function getPaginatedAdminList(JobFilter $filter, $page, $limit)
    {
        $query = $this->getRepository()->findAllForCensor($filter);

        $pagination = $this->paginator->paginate($query, $page, $limit);

        return $pagination;
    }

    /**
     * @param Job $job
     *
     * @return bool
     */
    public function hasActivePublishingService(Job $job)
    {
        return $this->objectManager->getRepository(OrderItemJob::class)->hasActiveJobPublishingService($job);
    }

    /**
     * @param Job $job
     */
    public function setAsUnActive(Job $job)
    {
        $job->setStatus(Job::STATUS_INACTIVE);
        $job->setValidTill(new \DateTime('-1D'));

        $this->update($job);
    }

    /**
     * @param Job $job
     * @param int $limit
     * @param User $user
     *
     * @return PaginationInterface
     */
    public function getRelated(Job $job, $limit, User $user = null)
    {
        $notIds = [$job->getId()];

        if ($user instanceof RegisteredUser) {
            // don't show related jobs for owners and managers
            if ($user->getId() == $job->getUser()->getId() || $job->getUser()->isUserManager($user)) {
                return $this->paginator->paginate([], 1, $limit);
            }

            $viewed = $this->getRepository()->getViewedByUser($user);

            foreach ($viewed as $viewedJob) {
                $notIds[] = $viewedJob->getId();
            }
        }

        $relatedJobsFilter = new JobFilter();
        $relatedJobsFilter->setNotIds($notIds);
        $relatedJobsFilter->setStatuses([Job::STATUS_ACTIVE]);
        $relatedJobsFilter->setShowValidTillJobs(true);
        $relatedJobsFilter->setWorkingPlaceCity($job->getWorkingPlaceCity());

        $relatedJobsFilterWithPosition = clone $relatedJobsFilter;
        $relatedJobsFilterWithPosition->setPosition($job->getPosition());

        /** @var Job[] $result */
        $result = $this->getRepository()->getListQuery($relatedJobsFilterWithPosition)->getResult();
        if ($job->getProfessionalArea()->getValues() && count($result) < $limit) {
            foreach ($result as $job) {
                $notIds[] = $job->getId();
            }
            $relatedJobsFilterWithPA = clone $relatedJobsFilter;
            $relatedJobsFilterWithPA->setProfessionalArea($job->getProfessionalArea());
            $relatedJobsFilterWithPA->setNotIds($notIds);
            $result = array_merge($result, $this->getRepository()->getListQuery($relatedJobsFilterWithPA)->getResult());
        }
        $pagination = $this->paginator->paginate($result, 1, $limit);

        return $pagination;
    }

    /**
     * @param Job $job
     * @param User $user
     *
     * @return boolean
     */
    public function existsView(Job $job, User $user)
    {
        $view = $this->objectManager->getRepository('RDWJobBundle:JobView')->findOneBy(['job' => $job, 'user' => $user]);

        return $view;
    }

    /**
     * @param Job $job
     * @param RegisteredUser $user
     *
     * @return Response
     */
    public function eventShowContact(Job $job, RegisteredUser $user = null)
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();

        if ($request->isMethod($request::METHOD_POST) && $request->request->get('show-contact')) {
            $jobId = $job->getId();
            $isViewedContactMonth = isset($_COOKIE["show_contact_v_id_$jobId"]) ? 1 : 0;
            if (!$isViewedContactMonth) {
                if ($user) {
                    if ($user->getId() != $job->getUser()->getId() && !$user->isManager() && $this->existsView($job, $user)) { // if not owner
                        $lastShowContact = $this->objectManager->getRepository('RDWUserBundle:User')->getLastShowContacts($user, $job);
                        $date = new \DateTime();
                        $date->modify('-30 day');
                        $lastShowContact = $lastShowContact == null ? true : $lastShowContact->getUpdatedAt() < $date;
                        if ($lastShowContact) { // if last show contacts more then 30 days
                            $job->addUserWhoShowedContact(new JobShowContacts($job, $user));

                            $this->container->get('rdw_job.service.job_manager')->update($job);

                            return new Response(
                                'Success',
                                Response::HTTP_OK
                            );
                        }

                        return new Response(
                            'In less than 30 days',
                            Response::HTTP_INTERNAL_SERVER_ERROR
                        );
                    }
                } elseif ($user == null) {
                    $job->incViewContactCount();
                    $this->container->get('rdw_job.service.job_manager')->update($job);
                }

                setcookie("show_contact_v_id_" . $job->getId(), 1, time() + 2592000); // 30 days
            }
        }
    }

    /**
     * Returns Jobs for corrent day
     *
     * @return array|Job[]
     */
    public function getThisDayJobs()
    {
        $limit = $this->container->getParameter('rdw_job.job_count_new_main_page');

        return $this->getRepository()->getJobsToday(new \DateTime(), $limit);
    }
}
