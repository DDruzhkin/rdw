<?php

namespace RDW\Bundle\JobBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Entity\Apply;
use RDW\Bundle\JobBundle\Model\ApplyFilter;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Knp\Component\Pager\Paginator;
use RDW\Bundle\UserBundle\Entity\UserInterface;

/**
 * Class JobViewManager
 *
 * @package RDW\Bundle\JobBundle\Service
 * @author  Paulius Aleliūnas <paulius@eface.lt>
 */
class JobViewManager
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $entityManager;

    /**
     * @var \Knp\Component\Pager\Paginator
     */
    private $paginator;

    /**
     * @param ObjectManager $entityManager
     * @param Paginator     $paginator
     */
    public function __construct(ObjectManager $entityManager, Paginator $paginator)
    {
        $this->entityManager = $entityManager;
        $this->paginator = $paginator;
    }

    /**
     * @param ApplyFilter $filters
     * @param int         $page
     * @param int         $limit
     *
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function getPaginatedList(ApplyFilter $filters, $page, $limit)
    {
        $query = $this->getRepository()->getListQuery($filters);

        $pagination = $this->paginator->paginate($query, $page, $limit);

        return $pagination;
    }

    /**
     * @return \RDW\Bundle\JobBundle\Repository\ViewRepository
     */
    protected function getRepository()
    {
        return $this->entityManager->getRepository('RDWJobBundle:JobView');
    }

}
