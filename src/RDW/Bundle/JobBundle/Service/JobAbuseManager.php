<?php

namespace RDW\Bundle\JobBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use Knp\Component\Pager\Paginator;
use RDW\Bundle\AppBundle\Entity\JobAbuse;

/**
 * Class JobAbuseManager
 *
 * @package RDW\Bundle\JobBundle\Service
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class JobAbuseManager
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $entityManager;

    /**
     * @var \Knp\Component\Pager\Paginator
     */
    private $paginator;

    /**
     * @param ObjectManager $entityManager
     * @param Paginator     $paginator
     */
    public function __construct(ObjectManager $entityManager, Paginator $paginator)
    {
        $this->entityManager = $entityManager;
        $this->paginator = $paginator;
    }

    /**
     * @return JobAbuse
     */
    public function create()
    {
        $abuse = new JobAbuse();

        return $abuse;
    }

    /**
     * @param JobAbuse $abuse
     * @param bool  $andFlush
     *
     * @return bool
     */
    public function update(JobAbuse $abuse, $andFlush = true)
    {
        $this->entityManager->persist($abuse);

        if ($andFlush) {
            $this->entityManager->flush();
        }

        return true;
    }

    /**
     * @param int $page
     * @param int $limit
     *
     * @return mixed
     */
    public function getPaginatedList($page, $limit)
    {
        $query = $this->getRepository()->getListQuery();

        $pagination = $this->paginator->paginate($query, $page, $limit);

        return $pagination;
    }

    /**
     * @return \RDW\Bundle\JobBundle\Repository\AbuseRepository
     */
    private function getRepository()
    {
        return $this->entityManager->getRepository('RDWAppBundle:JobAbuse');
    }
}