<?php

namespace RDW\Bundle\JobBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Entity\Apply;
use RDW\Bundle\JobBundle\Model\ApplyFilter;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Knp\Component\Pager\Paginator;
use RDW\Bundle\UserBundle\Entity\UserInterface;

/**
 * Class JobApplyManager
 *
 * @package RDW\Bundle\JobBundle\Service
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class JobApplyManager
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $entityManager;

    /**
     * @var \Knp\Component\Pager\Paginator
     */
    private $paginator;

    /**
     * @param ObjectManager $entityManager
     * @param Paginator     $paginator
     */
    public function __construct(ObjectManager $entityManager, Paginator $paginator)
    {
        $this->entityManager = $entityManager;
        $this->paginator = $paginator;
    }

    /**
     * @param Job  $job
     * @param UserInterface $user
     *
     * @return null|object
     */
    public function findByJobAndUser(Job $job, UserInterface $user = null)
    {
        return $this->getRepository()->findOneBy([
            'job' => $job,
            'user' => $user,
        ]);
    }

    /**
     * @param Cv   $cv
     * @param UserInterface $user
     *
     * @return null|object
     */
    public function findByCvAndUser(Cv $cv, UserInterface $user = null)
    {
        return $this->getRepository()->findOneBy([
            'cv' => $cv,
            'user' => $user,
        ]);
    }

    /**
     * @param UserInterface $employer
     * @param Cv            $cv
     *
     * @return null|Apply
     */
    public function hasApplyFromCv(UserInterface $employer, Cv $cv)
    {
        $hasApplies = false;
        $apply = $this->findByCvAndUser($cv, $cv->getUser());

        if ($apply instanceof Apply) {
            // if applied job was created by employer
            if ($apply->getJob()->getUser()->getId() == $employer->getId()) {
                $hasApplies = true;
            }
        }

        return $hasApplies;
    }

    /**
     * @param Job  $job
     * @param UserInterface $user
     *
     * @return Apply
     */
    public function createApplyFromJobAndUser(Job $job, UserInterface $user = null)
    {
        // check is old exist
        $apply = $this->findByJobAndUser($job, $user);

        if (! $apply instanceof Apply) {
            $apply = new Apply();
            $apply->setCvUploadType(Apply::CV_UPLOAD_TYPE_UPLOAD);

            // set these fields only for registered user
            if ($user instanceof RegisteredUser) {
                $apply->setCv($user->getFirstActiveCv());
                $apply->setName($user->getName().' '.substr($user->getSurname(), 0, 1));
                $apply->setPhone($user->getPhone());
                $apply->setCvUploadType(Apply::CV_UPLOAD_TYPE_MY);
            }

            $apply->setPosition($job->getPosition()->getTitle());
            $apply->setJob($job);
            $apply->setUser($user);
        }

        return $apply;
    }

    /**
     * @param User            $user
     * @param ArrayCollection $jobs
     *
     * @return Apply
     */
    public function createMultipleApplies(User $user, ArrayCollection $jobs = null)
    {
        $apply = new Apply();
        $apply->setCvUploadType(Apply::CV_UPLOAD_TYPE_UPLOAD);

        // set these fields only for registered user
        if ($user instanceof RegisteredUser) {
            $apply->setCv($user->getFirstActiveCv());
            $apply->setName($user->getName().' '.substr($user->getSurname(), 0, 1));
            $apply->setPhone($user->getPhone());
            $apply->setCvUploadType(Apply::CV_UPLOAD_TYPE_MY);
        }

        $apply->setUser($user);

        if ($jobs) {
            $apply->setJobs($jobs);
        }

        return $apply;
    }

    /**
     * @param $apply
     * @return object
     */
    public function merge($apply)
    {
        return $this->entityManager->merge($apply);
    }

    /**
     * @param Apply $apply
     * @param bool  $andFlush
     *
     * @return bool
     */
    public function update(Apply $apply, $andFlush = true)
    {
        $this->entityManager->persist($apply);

        if ($andFlush) {
            $this->entityManager->flush();
        }

        return true;
    }

    /**
     * @param ApplyFilter $filters
     * @param int         $page
     * @param int         $limit
     *
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function getPaginatedList(ApplyFilter $filters, $page, $limit)
    {
        $query = $this->getRepository()->getListQuery($filters);

        $pagination = $this->paginator->paginate($query, $page, $limit);

        return $pagination;
    }

    /**
     * @return \RDW\Bundle\JobBundle\Repository\ApplyRepository
     */
    protected function getRepository()
    {
        return $this->entityManager->getRepository('RDWJobBundle:Apply');
    }

}
