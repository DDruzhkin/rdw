<?php

namespace RDW\Bundle\JobBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class RDWJobBundle
 *
 * @package RDW\Bundle\JobBundle
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class RDWJobBundle extends Bundle
{
}
