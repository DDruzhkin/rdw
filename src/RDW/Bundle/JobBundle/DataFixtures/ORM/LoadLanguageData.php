<?php

namespace RDW\Bundle\JobBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\JobBundle\Entity\Language;
use Doctrine\Common\DataFixtures\AbstractFixture;

/**
 * Class LoadLanguageData
 *
 * @package RDW\Bundle\JobBundle\DataFixtures\ORM
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class LoadLanguageData extends AbstractFixture
{
    const REFERENCE_STRING = 'rdw.language.';
    const COUNT = 4;

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $entityManager)
    {
        $titles = [
            1 => 'Russian',
            2 => 'English',
            3 => 'Spanish',
            4 => 'French',
        ];

        foreach ($titles as $key => $title) {
            $entity = new Language();
            $entity->setTitle($title);

            $entityManager->persist($entity);
            $this->addReference(self::REFERENCE_STRING . $key, $entity);
        }

        $entityManager->flush();
    }
}