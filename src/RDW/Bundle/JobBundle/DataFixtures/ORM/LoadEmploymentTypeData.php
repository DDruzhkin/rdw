<?php

namespace RDW\Bundle\JobBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\JobBundle\Entity\EmploymentType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadEducationTypeData
 *
 * @package RDW\Bundle\JobBundle\DataFixtures\ORM
 * @author  Paulius Aleliūnas <paulius@eface.lt>
 */
class LoadEmploymentTypeData extends AbstractFixture implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    const REFERENCE_STRING = 'rdw.employment_type.';
    const COUNT = 2;

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $entityManager)
    {
        $items = [
            1 => ['title' => 'Full'],
            2 => ['title' => 'Partial', 'type' => EmploymentType::TYPE_PARTIAL],
        ];

        $isPersisted = false;
        foreach ($items as $key => $item) {
            $employmentType = new EmploymentType();
            $employmentType->setTitle($item['title']);

            if (isset($item['type'])) {
                $employmentType->setType($item['type']);

                $entityManager->persist($employmentType);
                $entityManager->flush();

                $this->container->get('slug_service')->saveSlug($employmentType->getId(), get_class($employmentType), implode(' ', [$item['title']]));

                $isPersisted = true;
            }

            $this->setReference(self::REFERENCE_STRING . $key, $employmentType);

            if (!$isPersisted) {
                $entityManager->persist($employmentType);
            }
            $isPersisted = false;
        }

        $entityManager->flush();
    }
}
