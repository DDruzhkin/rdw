<?php

namespace RDW\Bundle\JobBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\JobBundle\Entity\WorkScheduleType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class LoadWorkScheduleTypeData
 *
 * @package RDW\Bundle\JobBundle\DataFixtures\ORM
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class LoadWorkScheduleTypeData extends AbstractFixture implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    const REFERENCE_STRING = 'rdw.work_schedule_type.';
    const COUNT = 5;

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $entityManager)
    {
        $items = [
            1 => ['title' => 'Full time'],
            2 => ['title' => 'Part time'],
            3 => ['title' => 'Flexible part time', 'type' => WorkScheduleType::TYPE_FLEXIBLE],
            4 => ['title' => 'Remote', 'type' => WorkScheduleType::TYPE_REMOTE],
            5 => ['title' => 'Alternative', 'type' => WorkScheduleType::TYPE_ALTERNATIVE]
        ];

        $isPersisted = false;
        foreach ($items as $key => $item) {
            $workScheduleType = new WorkScheduleType();
            $workScheduleType->setTitle($item['title']);

            if (isset($item['type'])) {
                $workScheduleType->setType($item['type']);

                $entityManager->persist($workScheduleType);
                $entityManager->flush();

                $this->container->get('slug_service')->saveSlug($workScheduleType->getId(), get_class($workScheduleType), implode(' ', [$item['title']]));

                $isPersisted = true;
            }

            $this->addReference(self::REFERENCE_STRING . $key, $workScheduleType);

            if (!$isPersisted) {
                $entityManager->persist($workScheduleType);
            }
            $isPersisted = false;
        }

        $entityManager->flush();
    }
}
