<?php

namespace RDW\Bundle\JobBundle\DataFixtures\ORM;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\LocationBundle\DataFixtures\ORM\LoadCityData;
use RDW\Bundle\UserBundle\DataFixtures\ORM\LoadCompanyTypeData;
use RDW\Bundle\UserBundle\DataFixtures\ORM\LoadScopeData;
use RDW\Bundle\UserBundle\DataFixtures\ORM\LoadUserData;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\Scope;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Entity\Position;
use RDW\Bundle\JobBundle\Entity\EducationType;
use RDW\Bundle\JobBundle\Entity\WorkExperienceType;
use RDW\Bundle\JobBundle\Entity\WorkScheduleType;
use RDW\Bundle\JobBundle\Entity\EmploymentType;
use RDW\Bundle\UserBundle\Entity\CompanyType;
use Faker\Factory as FakerFactory;
use Faker\Generator;
use RDW\Bundle\UserBundle\Entity\User;

/**
 * Class LoadJobData
 *
 * @package RDW\Bundle\JobBundle\DataFixtures\ORM
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class LoadJobData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * Faker.
     *
     * @var Generator
     */
    protected $faker;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->faker = FakerFactory::create();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    function getDependencies()
    {
        return [
            LoadUserData::class,
            LoadCompanyTypeData::class,
            LoadCityData::class,
            LoadScopeData::class,
            LoadPositionData::class,
            LoadEducationTypeData::class,
            LoadWorkExperienceTypeData::class,
            LoadWorkScheduleTypeData::class,
            LoadEmploymentTypeData::class

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $numberOfJobs = 10;
        $genders = ['male', 'female'];

        $positions = $manager->getRepository(Position::class)->findAll();

        foreach ($positions as $position) {

            for ($i = 1; $i <= $numberOfJobs; $i++) {

                /** @var $user User */
                $user = $this->getReference(LoadUserData::REFERENCE_STRING . rand(
                        LoadUserData::NUMBER_OF_EMPLOYEES + 1,
                        LoadUserData::NUMBER_OF_EMPLOYEES + LoadUserData::NUMBER_OF_EMPLOYERS)
                );
                $rand = rand(0, 1);
                $gender = $genders[$rand];

                $job = new Job();
                $job->setUser($user);
                $job->setEmployerType(RegisteredUser::EMPLOYER_TYPE_COMPANY);

                /** @var $companyType CompanyType */
                $companyType = $this->getReference(
                    LoadCompanyTypeData::REFERENCE_STRING . rand(1, LoadCompanyTypeData::COUNT)
                );
                $job->setCompanyType($companyType);

                $job->setCompanyTitle($this->faker->word);
                $job->setCompanyTradeMark($this->faker->word);
                $job->setCompanyUrl($this->faker->url);
                $job->setCompanyInfo($this->faker->text(800));
                $job->setCities(
                    new ArrayCollection([$this->getReference(LoadCityData::REFERENCE_STRING . rand(1, 4))])
                );
                $job->setWorkingPlaceCity(
                    $this->getReference(LoadCityData::REFERENCE_STRING . rand(1, LoadCityData::COUNT))
                );
                $job->setAddress($this->faker->address);
                $job->setPhone($this->faker->phoneNumber);
                $job->setFax($this->faker->phoneNumber);
                $job->setFbUrl($this->faker->url);
                $job->setVkUrl($this->faker->url);
                $job->setOkUrl($this->faker->url);
                $job->setOkUrl($this->faker->url);
                $job->setGpUrl($this->faker->url);
                $job->setTwitterUrl($this->faker->url);
                $job->setLinkedInUrl($this->faker->url);
                $job->setSkypeName($this->faker->word);

                /** @var $scope Scope */
                $scope = $this->getReference(LoadScopeData::REFERENCE_STRING . rand(1, LoadScopeData::COUNT));
                $job->setScope($scope);

                $job->setYoutubeUrl('https://www.youtube.com/watch?v=4hKQz-WeHpQ');

                $job->setPosition($position);

                $job->setRequirements($this->faker->text(100));
                $job->setResponsibilities($this->faker->text(50));
                $job->setWorkingConditions($this->faker->sentence());
                $job->setGender($gender);
                $job->setWorkType($this->faker->word);
                $job->setSalaryFrom($this->faker->randomDigit);
                $job->setSalaryTo($this->faker->randomDigit);
                $job->setAdditionalInformation($this->faker->text(800));
                $job->setContactPerson($this->faker->name);
                $job->setContactPersonEmail($this->faker->email);
                $job->setValidTill(new \DateTime('+10year'));
                $job->setStatus(Job::STATUS_ACTIVE);
                $job->addContactPersonPhoneByString($this->faker->phoneNumber);

                /** @var $educationType EducationType */
                $educationType = $this->getReference(
                    LoadEducationTypeData::REFERENCE_STRING . rand(1, LoadEducationTypeData::COUNT)
                );
                $job->setRequiredEducationType($educationType);

                /** @var $workExperience WorkExperienceType */
                $workExperience = $this->getReference(
                    LoadWorkExperienceTypeData::REFERENCE_STRING . rand(1, LoadWorkExperienceTypeData::COUNT)
                );
                $job->setWorkExperienceType($workExperience);

                /** @var $workSchedule WorkScheduleType */
                $workSchedule = $this->getReference(
                    LoadWorkScheduleTypeData::REFERENCE_STRING . rand(1, LoadWorkScheduleTypeData::COUNT)
                );
                $job->setWorkScheduleType($workSchedule);

                /** @var $employmentType EmploymentType */
                $employmentType = $this->getReference(
                    LoadEmploymentTypeData::REFERENCE_STRING . rand(1, LoadEmploymentTypeData::COUNT)
                );
                $job->setEmploymentType($employmentType);

                $manager->persist($job);
            }

            $manager->flush();
        }
    }
}
