<?php

namespace RDW\Bundle\JobBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\JobBundle\Entity\WorkExperienceType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class LoadWorkExperienceTypeData
 *
 * @package RDW\Bundle\JobBundle\DataFixtures\ORM
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class LoadWorkExperienceTypeData extends AbstractFixture implements ContainerAwareInterface
{

    const REFERENCE_STRING = 'rwd.work_experience_type.';
    const COUNT = 5;

    use ContainerAwareTrait;

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $entityManager)
    {
        $titles = [
            'No experience' => WorkExperienceType::TYPE_NO_EXPERIENCE,
            'Less than 1 year' => WorkExperienceType::TYPE_LESS_YEAR,
            'More than 1 year' => WorkExperienceType::TYPE_ONE_YEAR,
            'More than 3 years' => WorkExperienceType::TYPE_THREE_YEAR,
            'More than 5 years' => WorkExperienceType::TYPE_FIVE_YEAR,
        ];

        $key = 1;
        foreach ($titles as $title => $type) {
            $workExperienceType = new WorkExperienceType();
            $workExperienceType->setTitle($title);
            $workExperienceType->setType($type);

            $this->addReference(self::REFERENCE_STRING . $key, $workExperienceType);
            $key++;

            $entityManager->persist($workExperienceType);
            $entityManager->flush();

            $this->container->get('slug_service')->saveSlug($workExperienceType->getId(), get_class($workExperienceType), implode(' ', [$title]));
        }
    }
}
