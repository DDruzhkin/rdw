<?php

namespace RDW\Bundle\JobBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\JobBundle\Entity\EducationType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class LoadEducationTypeData
 *
 * @package RDW\Bundle\JobBundle\DataFixtures\ORM
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class LoadEducationTypeData extends AbstractFixture implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    const REFERENCE_STRING = 'rdw.education_type.';
    const COUNT = 4;

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $entityManager)
    {
        $titles = [
            1 => ['title' => 'Secondary'],
            2 => ['title' => 'Special secondary'],
            3 => ['title' => 'Higher'],
            4 => ['title' => 'Without education', 'type' => EducationType::TYPE_WITHOUT_EDUCATION]
        ];

        $isPersisted = false;
        foreach ($titles as $key => $item) {
            $educationType = new EducationType();
            $educationType->setTitle($item['title']);

            if (isset($item['type'])) {
                $educationType->setType($item['type']);

                $entityManager->persist($educationType);
                $entityManager->flush();

                $this->container->get('slug_service')->saveSlug($educationType->getId(), get_class($educationType), implode(' ', [$item['title']]));

                $isPersisted = true;
            }

            $this->setReference(self::REFERENCE_STRING . $key, $educationType);

            if (!$isPersisted) {
                $entityManager->persist($educationType);
            }
            $isPersisted = false;
        }

        $entityManager->flush();
    }
}
