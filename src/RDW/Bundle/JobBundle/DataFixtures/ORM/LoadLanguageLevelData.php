<?php

namespace RDW\Bundle\JobBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use RDW\Bundle\JobBundle\Entity\LanguageLevel;

/**
 * Class LoadLanguageLevelData
 *
 * @package RDW\Bundle\JobBundle\DataFixtures\ORM
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class LoadLanguageLevelData extends AbstractFixture
{
    const REFERENCE_STRING = 'rdw.language_level.';
    const COUNT = 4;

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $entityManager)
    {
        $titles = [
            1 => 'Some',
            2 => 'Well',
            3 => 'Perfectly',
            4 => 'Mother tongue',
        ];

        foreach ($titles as $key => $title) {
            $entity = new LanguageLevel();
            $entity->setTitle($title);

            $entityManager->persist($entity);
            $this->addReference(self::REFERENCE_STRING . $key, $entity);
        }

        $entityManager->flush();
    }
}