<?php

namespace RDW\Bundle\JobBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\JobBundle\Entity\Position;
use Doctrine\Common\DataFixtures\AbstractFixture;

/**
 * Class LoadPositionData
 *
 * @package RDW\Bundle\JobBundle\DataFixtures\ORM
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class LoadPositionData extends AbstractFixture
{
    const REFERENCE_STRING = 'rdw.position.';
    const COUNT = 5;
    const RANDOM_COUNT = 10;

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $entityManager)
    {
        $titles = [
            0 => 'PHP/Symfony3 developer',
            1 => 'PHP/Symfony2 developer',
            2 => 'Frontend developer',
            3 => 'Account manager',
            4 => 'Manager',
            5 => 'System architect'
        ];

        foreach ($titles as $key => $title) {
            $entity = new Position();
            $entity->setTitle($title);

            $this->addReference(self::REFERENCE_STRING . $key, $entity);
            $entityManager->persist($entity);
        }

        $entityManager->flush();

        $this->addRandomPositions($entityManager);
    }

    /**
     * @param ObjectManager $entityManager
     */
    protected function addRandomPositions(ObjectManager $entityManager)
    {
        for ($i = 0; $i <= self::RANDOM_COUNT; $i++) {
            $entity = new Position();
            $entity->setTitle(uniqid('Position ', true));

            $this->addReference(self::REFERENCE_STRING . 'random.' . $i, $entity);
            $entityManager->persist($entity);

            if (0 === $i % 50) {
                $entityManager->flush();
            }
        }

        $entityManager->flush();
    }
}
