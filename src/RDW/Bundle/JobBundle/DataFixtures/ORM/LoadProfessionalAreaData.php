<?php

namespace RDW\Bundle\JobBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\JobBundle\Entity\ProfessionalArea;
use Doctrine\Common\DataFixtures\AbstractFixture;

/**
 * Class LoadProfessionalAreaData
 *
 * @package RDW\Bundle\JobBundle\DataFixtures\ORM
 */
class LoadProfessionalAreaData extends AbstractFixture
{
    const REFERENCE_STRING = 'rdw.professional_area.';

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $entityManager)
    {
        $titles = [
            0 => 'IT',
            1 => 'Management',
        ];

        foreach ($titles as $key => $title) {
            $entity = new ProfessionalArea();
            $entity->setTitle($title);
            $entity->setTitleInLatin($entity->generateTitleInLatin());
            $entity->setTitleWhere($entity->generateTitleInLatin());

            $this->addReference(self::REFERENCE_STRING . $key, $entity);
            $entityManager->persist($entity);
        }

        for ($i = 0; $i <= 50; $i++) {
            $entity = new ProfessionalArea();
            $entity->setTitle(sprintf("PA_%d_%s", $i, uniqid()));
            $entity->setTitleInLatin($entity->generateTitleInLatin());
            $entity->setTitleWhere($entity->generateTitleInLatin());
            $entityManager->persist($entity);
        }

        $entityManager->flush();
    }

}
