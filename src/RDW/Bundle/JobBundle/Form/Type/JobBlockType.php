<?php

namespace RDW\Bundle\JobBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class JobBlockType
 *
 * @package RDW\Bundle\JobBundle\Form\Type
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class JobBlockType extends AbstractType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('blockReason', 'textarea', [
                'label' => 'Block reason',
                'required' => true,
                'attr' => ['placeholder' => 'Block reason placeholder']
            ])
            ->add('submit', 'submit', [
                'label' => 'Save',
                'validation_groups' => ['block']
            ])
            ->add('cancel', 'submit', [
                'label' => 'Cancel',
            ]);
    }

    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\JobBundle\Entity\Job'
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'rdw_job_block';
    }
}
