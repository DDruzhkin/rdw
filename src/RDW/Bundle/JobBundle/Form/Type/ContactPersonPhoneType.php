<?php

namespace RDW\Bundle\JobBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class ContactPersonPhoneType
 */
class ContactPersonPhoneType extends \RDW\Bundle\AppBundle\Form\Type\ContactPersonPhoneType
{
    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\JobBundle\Entity\ContactPersonPhone',
            'validation_groups' => ['publish', 'create', 'censor'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_job_contact_person_phone';
    }
}