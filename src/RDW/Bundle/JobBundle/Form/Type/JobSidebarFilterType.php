<?php

namespace RDW\Bundle\JobBundle\Form\Type;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityRepository;
use RDW\Bundle\AppBundle\Form\DataTransformer\EntityCollectionTransformer;
use RDW\Bundle\AppBundle\Form\DataTransformer\SingleEntityTransformer;
use RDW\Bundle\AppBundle\Service\SalaryRangeService;
use RDW\Bundle\JobBundle\Model\JobFilter;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class JobFilterType
 *
 * @package RDW\Bundle\JobBundle\Form\Type
 */
class JobSidebarFilterType extends AbstractType
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @var SalaryRangeService
     */
    private $salaryRangeService;

    /**
     * @param ObjectManager      $om
     * @param SalaryRangeService $salaryRangeService
     */
    public function __construct(ObjectManager $om, SalaryRangeService $salaryRangeService)
    {
        $this->om = $om;
        $this->salaryRangeService = $salaryRangeService;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $citiesTransformer = new EntityCollectionTransformer($this->om, 'RDWLocationBundle:City');
        $scopeTransformer = new SingleEntityTransformer($this->om, 'RDWUserBundle:Scope');
        $professionalAreaTransformer = new EntityCollectionTransformer($this->om, 'RDWJobBundle:ProfessionalArea');
        $positionTransformer = new SingleEntityTransformer($this->om, 'RDWJobBundle:Position');
        $languagesTransformer = new EntityCollectionTransformer($this->om, 'RDWJobBundle:Language');

        $builder->setMethod('get');

        $builder->add(
            $builder
                ->create('cities', 'hidden', [
                    'required' => false,
                    'label' => 'City',
                    'attr' => [
                        'data-handler' => 'filterTag',
                        'data-filter' => 'city',
                        'data-multiple' => 'true',
                    ],
                ])
                ->addModelTransformer($citiesTransformer)
        );
        $builder->add(
            $builder
                ->create('languages', 'hidden', [
                    'required' => false,
                    'attr' => [
                        'data-handler' => 'filterTag',
                        'data-filter' => 'language',
                        'data-multiple' => 'true',
                        'placeholder' => 'No matter',
                    ],
                ])
                ->addModelTransformer($languagesTransformer)
        );

        $builder->add(
            $builder->create('scope', 'hidden', [
                'required' => false,
                'label' => 'Scope',
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'scope',
                    'data-multiple' => 'false',
                ],
            ])->addModelTransformer($scopeTransformer)
        );

        $builder->add(
            $builder
                ->create('professionalArea', 'hidden', [
                    'required' => false,
                    'label' => 'Professional Area',
                    'attr' => [
                        'data-handler' => 'filterTag',
                        'data-filter' => 'professional_area',
                        'data-multiple' => 'true',
                    ],
                ])
                ->addModelTransformer($professionalAreaTransformer)
        );

        $builder->add(
            $builder->create('position', 'hidden', [
                'required' => false,
                'label' => 'Position',
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'position',
                    'data-multiple' => 'false',
                ],
            ])->addModelTransformer($positionTransformer)
        );



        $builder
            ->add('query', 'hidden', [
                'required' => false,
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'query',
                    'data-multiple' => 'false',
                    'data-load-from-hidden' => true,
                ],
            ])
            ->add('createdAt', 'choice', [
                'choices' => ['1'=>1,'3'=>3,'10'=>10,'30'=>30],
                /** @Ignore */
                'label' => 'Опубликовано за',
                'empty_value' => false,
                'expanded' => true,
                'multiple' => false,
                'required' => false,
                'mapped' => false,
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'createdAt',
                    'data-multiple' => 'false',
                ],
            ])
            ->add('salaryFrom', 'choice', [
                'choices' => $this->salaryRangeService->getJobSalaryIntervals(),

                'label' => false,
                'empty_value' => 'Choose from',
                'required' => false,
                'mapped' => false,
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'salaryFrom',
                    'data-multiple' => 'false',
                ],
            ])
            ->add('salaryTo', 'choice', [
                'choices' => $this->salaryRangeService->getJobSalaryIntervals(),
                /** @Ignore */
                'label' => false,
                'empty_value' => 'Choose to',
                'required' => false,
                'mapped' => false,
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'salaryTo',
                    'data-multiple' => 'false',
                ],
            ])
            ->add('onlyWithSalary', 'choice', [
                'choices'=>['1'=>'не показывать без зарплаты'],
                'label' => false,
                'empty_value' => false,
                'required' => false,
                'expanded'=>true,
                'multiple'=>true,
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'onlyWithSalary',
                    'data-multiple' => 'false',
                ],
            ])
            ->add('withoutHigh', 'choice', [
                'choices'=>['1'=>'Высшее образование не требуется'],
                'label' => false,
                'empty_value' => false,
                'required' => false,
                'expanded'=>true,
                'multiple'=>true,
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'withoutHigh',
                    'data-multiple' => 'false',
                ],
            ])
            ->add('withoutExp', 'choice', [
                'choices'=>['1'=>'Без опыта работы'],
                'label' => false,
                'empty_value' => false,
                'required' => false,
                'expanded'=>true,
                'multiple'=>true,
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'withoutExp',
                    'data-multiple' => 'false',
                ],
            ])
            ->add('hideAgencies', 'choice', [
                'choices'=>['1'=>'Скрыть вакансии агенств'],
                'label' => false,
                'empty_value' => false,
                'required' => false,
                'expanded'=>true,
                'multiple'=>true,
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'hideAgencies',
                    'data-multiple' => 'false',
                ],
            ])
            ->add('employmentType', 'entity', [
                'class' => 'RDWJobBundle:EmploymentType',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('i')
                        ->orderBy('i.position', 'ASC');
                },
                'empty_value' => 'No matter',
                'property' => 'title',
                'required' => false,
                'label' => 'Employment type',
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'employmentType',
                    'data-multiple' => 'false',
                ],
            ])
            ->add('companyTitle', 'hidden', [
                'required' => false,
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'companyTitle',
                    'data-multiple' => 'false',
                    'data-load-from-hidden' => true,
                    'placeholder' => 'No matter',
                ],
            ])
            ->add('workScheduleType', 'entity', [
                'class' => 'RDWJobBundle:WorkScheduleType',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('i')
                        ->orderBy('i.position', 'ASC');
                },
                'empty_value' => 'No matter',
                'property' => 'title',
                'required' => false,
                'expanded'=>true,
                'multiple'=>true,
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'workScheduleType',
                    'data-multiple' => 'false',
                ],
            ])
            ->add('driverLicenseCategories', 'entity', [
                'label' => 'Driver license category',
                'class' => 'RDWJobBundle:DriverLicenseCategory',
                'empty_value' => 'No matter',
                'property' => 'title',
                'expanded' => true,
                'multiple' => true,
                'required' => false,
            ])
            ->add('employerType', 'choice', [
                'choices'   => RegisteredUser::getEmployerTypeChoiceList(),
                'label' => 'Employer type',
                'empty_value' => 'No matter',
                'required' => false,
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'employerType',
                    'data-multiple' => 'false',
                ],
            ])
            ->add('updatedAt', 'choice', [
                'choices'   => JobFilter::getCreatedAtChoiceList(),
                'empty_value' => 'Job created',
                'required' => false,
                'label' => 'Date',
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'updatedAt',
                    'data-multiple' => 'false',
                ],
            ])
            ->add('education', 'entity', [
                'class' => 'RDWJobBundle:EducationType',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('i')
                        ->orderBy('i.position', 'ASC');
                },
                'empty_value' => 'No matter',
                'property' => 'title',
                'required' => false,
                'label' => 'Education type',
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'education',
                    'data-multiple' => 'false',
                ],
            ])
            ->add('workExperience', 'entity', [
                'class' => 'RDWJobBundle:WorkExperienceType',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('i')
                        ->orderBy('i.position', 'ASC');
                },
                'empty_value' => 'No matter',
                'property' => 'title',
                'required' => false,
                'attr' => [
                    'data-handler' => 'filterTag',
                    'data-filter' => 'workExperience',
                    'data-multiple' => 'false',
                ],
            ])
            /*->add('onlyWithSalary', 'checkbox', [
                'label' => 'Hide results without salary',
                'required' => false,
            ])*/
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\JobBundle\Model\JobFilter',
            'csrf_protection'   => false,
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'sidebar_jobs';
    }
}
