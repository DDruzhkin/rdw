<?php

namespace RDW\Bundle\JobBundle\Form\Type\Manage;

use RDW\Bundle\AppBundle\Form\Type\Select2\ProfessionalAreaType as Select2ProfessionalAreaType;
use RDW\Bundle\JobBundle\Entity\ProfessionalArea;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfessionalAreaType extends AbstractType
{
    protected $dataClass = ProfessionalArea::class;

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title')
            ->add('type')
            ->add('titleInLatin')
            ->add('titleWhere')
            ->add('parent', Select2ProfessionalAreaType::class, ['label' => 'Parent'])
        ;
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => $this->dataClass,
        ]);
    }

    /**
     * @param $dataClass
     */
    public function setDataClass($dataClass)
    {
        $this->dataClass = $dataClass;
    }
}
