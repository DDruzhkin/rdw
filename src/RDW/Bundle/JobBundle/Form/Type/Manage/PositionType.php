<?php

namespace RDW\Bundle\JobBundle\Form\Type\Manage;

use RDW\Bundle\AppBundle\Form\Type\ProfessionalAreaCollectionPopupType;
use RDW\Bundle\JobBundle\Entity\Position;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PositionType extends AbstractType
{
    protected $dataClass = Position::class;

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Position $position */
        $position = $options['data'];
        $builder->add('title')
            ->add('type')
            ->add('status', ChoiceType::class, [
                'choices'  => [
                    Position::STATUS_NEW => 'New',
                    Position::STATUS_APPROVED => 'Approved',
                    Position::STATUS_DECLINED => 'Declined',
                ],
                'data' => $position ? $position->getStatus() : 'new',
            ])
            ->add('professionalArea', ProfessionalAreaCollectionPopupType::class)
            ->add('professionalArea', ProfessionalAreaCollectionPopupType::class);
        ;
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => $this->dataClass,
        ]);
    }

    /**
     * @param $dataClass
     */
    public function setDataClass($dataClass)
    {
        $this->dataClass = $dataClass;
    }
}
