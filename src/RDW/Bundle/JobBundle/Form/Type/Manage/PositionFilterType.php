<?php

namespace RDW\Bundle\JobBundle\Form\Type\Manage;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\AppBundle\Form\Type\Select2\PositionType as PositionSelect2Type;
use RDW\Bundle\JobBundle\Entity\Position;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PositionFilterType extends AbstractType
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $positionFilter = isset($options['data']) ? $options['data'] : [];
        $builder->setMethod('get');

        $builder
            ->add('id', PositionSelect2Type::class, ['required' => false])
            ->add('status', ChoiceType::class, [
                'choices'  => [
                    Position::STATUS_NEW => 'New',
                    Position::STATUS_APPROVED => 'Approved',
                    Position::STATUS_DECLINED => 'Declined',
                ],
                'data' => $positionFilter ? $positionFilter['status'] : Position::STATUS_APPROVED,
                'required' => false,
            ])
            ->add('filter', 'submit')
            ->add('clear', 'submit')
        ;
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'position_filter';
    }
}
