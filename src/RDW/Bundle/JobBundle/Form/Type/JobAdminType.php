<?php

namespace RDW\Bundle\JobBundle\Form\Type;

use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\EventListener\JobActionListener;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * Class JobType
 *
 * @package RDW\Bundle\JobBundle\Form\Type
 */
class JobAdminType extends BaseJobType
{
    /**
     * @var SecurityContext
     */
    private $securityContext;

    /**
     * @param SecurityContext $securityContext
     */
    public function __construct(SecurityContext $securityContext)
    {
        $this->securityContext = $securityContext;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        // grab the user, do a quick sanity check that one exists
        $user = $this->securityContext->getToken()->getUser();

        if (! $user) {
            throw new \LogicException(
                'The JobAdminType cannot be used without an authenticated user!'
            );
        }

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($user, $options) {
                $form = $event->getForm();

                $choices = [];

                // only is allowed to change all statuses
                if ($this->securityContext->isGranted('ROLE_ADMIN', $user)) {
                    $choices = $event->getData()->getStatuses();
                } elseif ($this->securityContext->isGranted('ROLE_CENSOR', $user)) {
                    $choices = $event->getData()->getStatusesForCensor();
                }

                $formOptions = [
                    'choices' => $choices,
                    'empty_value' => 'Choose option',
                    'required' => false,
                    'disabled' => $options['disabled'],
                ];
                $form->add('status', 'choice', $formOptions);
            }
        );

        $builder
            ->add('requirements', 'ckeditor', [
                'config' => ['width' => '100%','inline' => true],
                'config_name' => 'jobCvFields',
                'required' => true
            ])
            ->add('responsibilities', 'ckeditor', [
                'label' => 'Responsibilities',
                'config' => ['width' => '100%', 'inline' => true],
                'config_name' => 'jobCvFields',
                'required' => true
            ])
            ->add('workingConditions', 'ckeditor', [
                'label' => 'Working conditions',
                'config' => ['width' => '100%', 'inline' => true],
                'config_name' => 'jobCvFields',
                'required' => true
            ])
            ->add('additionalInformation', 'ckeditor', [
                'label' => 'Information',
                'config' => ['width' => '100%', 'inline' => true],
                'config_name' => 'jobCvFields',
                'required' => true
            ])
            ->add('vip', 'choice', [
                'choices'   => [
                    0 => 'No',
                    1 => 'Yes',
                ],
                'label' => 'VIP ad',
                'required' => true,
                'expanded' => true,
            ])
            ->add('showInCarousel', 'choice', [
                'choices'   => [
                    0 => 'No',
                    1 => 'Yes',
                ],
                'label' => 'Show in carousel',
                'required' => true,
                'expanded' => true,
            ]);

        if (! $options['disabled']) {
            $builder
                ->add('submit', 'submit', [
                    'label' => 'Save',
                ]);
        }

        $builder->addEventSubscriber(new JobActionListener());
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\JobBundle\Entity\Job',
            'disabled' => false,
            'validation_groups' =>
                function (FormInterface $form) {
                    $groups = [];

                    if ($form->getData()->getId()) {
                        $groups[] = 'censor';
                    } else {
                        $groups[] = 'publish';
                    }

                    return $groups;
                },
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_job_admin';
    }
}
