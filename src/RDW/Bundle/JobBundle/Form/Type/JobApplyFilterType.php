<?php

namespace RDW\Bundle\JobBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\UserBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class JobApplyFilterType
 *
 * @package RDW\Bundle\JobBundle\Form\Type
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class JobApplyFilterType extends AbstractType
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('GET');

        $builder
            ->add('job', 'entity', [
                'class' => 'RDWJobBundle:Job',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('j')
                        ->where('j.user = :user')
                        ->setParameter('user', $this->user)
                       // ->andWhere('j.status IN (:status)')
                        ->leftJoin('j.applies', 'applies')
                        ->orderBy('j.updatedAt', 'DESC');
                },
                'property' => 'position.title',
                'empty_value' => 'All',
                'required' => false,
                'label' => 'Job',
            ])
            ->add('user', 'hidden', ['data' => $this->user])
            ->add('search', 'submit', array(
                'label' => 'Search'
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RDW\Bundle\JobBundle\Model\ApplyFilter',
            'csrf_protection' => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return '';
    }
}
