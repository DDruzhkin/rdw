<?php

namespace RDW\Bundle\JobBundle\Form\Type;

use RDW\Bundle\JobBundle\Repository\JobRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class JobCollectionType
 *
 * @package RDW\Bundle\JobBundle\Form\Type
 */
class JobCollectionType extends AbstractType
{
    /**
     * @var array
     */
    private $items;

    /**
     * @param array $items
     */
    public function __construct(array $items)
    {
        $this->items = $items;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', 'entity', [
            'required' => false,
            'class' => 'RDWJobBundle:Job',
            'property' => 'id',
            'property_path' => '[id]',
            'multiple' => true,
            'expanded' => true,
            'query_builder' => function (JobRepository $repository) {
                return $repository
                    ->createQueryBuilder('j')
                    ->where('j.id IN (:ids)')
                    ->setParameter('ids', $this->items);
            },
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
            'csrf_protection' => false,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'job_collection';
    }
}
