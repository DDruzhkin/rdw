<?php

namespace RDW\Bundle\JobBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use RDW\Bundle\AppBundle\Form\EventListener\BuildDocumentUploadFormListener;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\JobBundle\Entity\Apply;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\User;
use RDW\Bundle\UserBundle\Entity\UserInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class JobApplyType
 *
 * @package RDW\Bundle\JobBundle\Form\Type
 */
class JobApplyType extends AbstractType
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @param UserInterface $user
     */
    public function __construct(UserInterface $user = null)
    {
        $this->user = $user;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('position', 'text', [
                'read_only' => true,
                'label' => 'Job position',
                'required' => false,
                'attr' => ['placeholder' => 'Position placeholder'],
            ])
            ->add('name', 'text', [
                'read_only' => ($this->user instanceof RegisteredUser && $this->user->getName()),
                'label' => 'Name',
                'required' => ($this->user instanceof RegisteredUser && $this->user->getName()) ? false : true,
                'attr' => ['placeholder' => 'Name placeholder'],
            ])
            ->add('phone', 'text', [
                'label' => 'Phone',
                'required' => false,
                'attr' => ['placeholder' => '+375 29 100 000 00'],
            ])
            ->add('message', 'textarea', [
                'label' => 'Message',
                'required' => true,
                'attr' => ['placeholder' => 'Message placeholder'],
            ])
        ;

        if ($this->user instanceof RegisteredUser) {
            $builder
                ->add('submit', 'button', ['label' => 'Send'])
                ->add('cv', 'entity', [
                    'class' => 'RDWCvBundle:Cv',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                            ->where('c.user = :user')
                            ->andWhere('c.status IN (:statuses)')
                            ->setParameter('user', $this->user)
                            ->setParameter('statuses', [Cv::STATUS_ACTIVE])
                            ->orderBy('c.updatedAt', 'DESC');
                    },
                    'property' => 'position.title',
                    'empty_value' => 'Other',
                    'required' => false,
                ])
            ;
        } else {
            $builder->add('cv', 'hidden', ['required' => false, 'label' => false]);
        }

        $builder->addEventSubscriber(new BuildDocumentUploadFormListener());
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\JobBundle\Entity\Apply',
            'validation_groups' =>
                function (FormInterface $form) {
                    $groups = ['apply'];

                    if ($form->getData()->getCv() instanceof Cv) {
                        $groups[] = 'apply_my';
                    } else {
                        $groups[] = 'apply_upload';
                    }

                    return $groups;
                },
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_job_apply';
    }
}
