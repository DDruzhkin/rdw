<?php

namespace RDW\Bundle\JobBundle\Form\Type;

use RDW\Bundle\JobBundle\Entity\Job;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class JobActivateType
 *
 * @package RDW\Bundle\JobBundle\Form\Type
 * @author  Vytautas Beliunas <giedrius@eface.lt>
 */
class JobActivateType extends AbstractType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('validTill', 'date', [
                'required' => false,
            ])
            ->add('submit', 'button', [
                'label' => 'Activate',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\JobBundle\Entity\Job',
            'validation_groups' => ['activate']
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'job_atctivate';
    }
}
