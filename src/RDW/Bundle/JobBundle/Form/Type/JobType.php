<?php

namespace RDW\Bundle\JobBundle\Form\Type;

use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\EventListener\JobActionListener;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class JobType
 *
 * @package RDW\Bundle\JobBundle\Form\Type
 */
class JobType extends BaseJobType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Job $job */
        $job = $builder->getForm()->getData();

        parent::buildForm($builder, $options);

        if (! $options['disabled']) {
            $builder
                ->add('submit', 'submit', [
                    'label' => 'Save',
                    'validation_groups' => ['create'],
                ]);

            if ($job->getUser() instanceof RegisteredUser) {
                $builder
                    ->add('submitAndPublish', 'submit', [
                        'label' => 'Publish',
                        'validation_groups' => ['publish'],
                    ]);
            }
        }

        $builder->addEventSubscriber(new JobActionListener());
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\JobBundle\Entity\Job',
            'cascade_validation' => true,
            'disabled' => false,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_job';
    }
}
