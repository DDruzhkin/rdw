<?php

namespace RDW\Bundle\JobBundle\Form\Type;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\AppBundle\Form\DataTransformer\SingleEntityTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class JobAdminFilterType
 * @package RDW\Bundle\JobBundle\Form\Type
 */
class JobAdminFilterType extends AbstractType
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $positionTransformer = new SingleEntityTransformer($this->om, 'RDWJobBundle:Position');

        $builder->setMethod('get');

        $builder
            ->add('id', 'number', [
                'label' => 'ID',
                'required' => false,
            ])
            ->add('position', 'position', ['required' => false])
            ->add('companyTitle', 'text', [
                'label' => 'Company title',
                'required' => false,
            ])
            ->add('contactEmail', 'text', [
                'label' => 'Contact email',
                'required' => false,
            ])
            ->add('contactPhone', 'text', [
                'label' => 'Contact phone',
                'required' => false,
            ])
            ->add('crmOrderId', 'text', [
                'label' => 'CRM order ID',
                'required' => false,
            ])
            ->add('foreignId', 'text', [
                'label' => 'CRM ID',
                'required' => false,
            ])
            ->add('filter', 'submit')
            ->add('clear', 'submit')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\JobBundle\Model\JobFilter',
            'csrf_protection'   => false,
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'job_admin_filter';
    }
}
