<?php

namespace RDW\Bundle\JobBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class LanguageFormType
 *
 * @author Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class LanguageFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('language', 'entity', [
                'class' => 'RDWJobBundle:Language',
                'empty_value' => 'Choose option',
                'property' => 'title',
                'required' => false,
                'label' => 'Language',
            ])
            ->add('level', 'entity', [
                'class' => 'RDWJobBundle:LanguageLevel',
                'empty_value' => 'Choose option',
                'property' => 'title',
                'required' => false,
                'label' => 'Level',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\JobBundle\Entity\JobLanguage',
            'cascade_validation' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_job_language';
    }
}