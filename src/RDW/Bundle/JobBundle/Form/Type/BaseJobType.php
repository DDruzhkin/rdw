<?php

namespace RDW\Bundle\JobBundle\Form\Type;

use RDW\Bundle\AppBundle\Form\EventListener\BuildPhotoUploadFormListener;
use RDW\Bundle\AppBundle\Form\EventListener\BuildContactPersonPhotoUploadFormListener;
use RDW\Bundle\AppBundle\Form\DataTransformer\FormattedNumberTransformer;
use RDW\Bundle\AppBundle\Form\Type\ProfessionalAreaCollectionPopupType;
use RDW\Bundle\AppBundle\Form\Type\PositionAutocompleteType;
use RDW\Bundle\JobBundle\Entity\Job;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class BaseJobType
 *
 * @package RDW\Bundle\JobBundle\Form\Type
 */
class BaseJobType extends AbstractType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (! $options['disabled']) {
            $builder->addEventSubscriber(new BuildPhotoUploadFormListener());
            $builder->addEventSubscriber(new BuildContactPersonPhotoUploadFormListener());
        }

        $builder
            ->add('employerType', 'choice', [
                'choices'   => Job::getEmployerTypeChoiceList(),
                'label' => 'Employer type',
                'empty_value' => 'Choose type',
                'required' => true,
                'attr' => [
                    'data-handler' => 'employerType',
                    'data-clear-company' => '',
                ],
                'disabled' => ($options['disabled']) ? true : false,
            ])
            ->add('companyType', 'entity', [
                'empty_value' => 'Choose company type',
                'label' => 'Company type',
                'property' => 'title',
                'class' => 'RDWUserBundle:CompanyType',
                'required' => true,
                'attr' => [
                    'data-hide' => '',
                    'data-clear-company' => '',
                ],
            ])
            ->add('workingPlaceCity', 'city', [
                'label' => 'Working place city',
            ])
            ->add('companyTitle', 'text', [
                'label' => 'Company title',
                'required' => true,
                'attr' => [
                    'data-hide' => '',
                    'placeholder' => 'Company title placeholder',
                    'data-clear-company' => '',
                ],
            ])
            ->add('companyTradeMark', 'text', [
              'label' => 'Company trade mark',
              'required' => false,
              'attr' => [
                'placeholder' => 'Please fill company trade mark',
                'data-hide' => '',
                'data-clear-company' => '',
              ],
            ])
            ->add('companyUrl', 'text', [
                'label' => 'Company URL',
                'required' => false,
                'attr' => [
                    'data-hide' => '',
                    'placeholder' => 'Company url placeholder',
                    'data-clear-company' => '',
                ],
            ])
            ->add('companyInfo', 'textarea', [
                'label' => 'Company info',
                'required' => false,
                'attr' => [
                    'data-hide' => '',
                    'placeholder' => 'Company info placeholder',
                    'data-clear-company' => '',
                ],
            ])
            ->add('cities', 'city_collection', [
                'label' => 'Cities',
            ])
            ->add('address', 'text', [
                'label' => 'Address',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Address placeholder',
                    'data-clear-company' => '',
                ],
            ])
            ->add('phone', 'text', [
                'label' => 'Phone',
                'required' => false,
                'attr' => [
                    'placeholder' => '+375 29 100 000 00',
                    'data-clear-company' => '',
                ],
            ])
            ->add('fax', 'text', [
                'label' => 'Fax',
                'required' => false,
                'attr' => [
                    'placeholder' => '+375 29 100 000 00',
                    'data-clear-company' => '',
                ],
            ])
            ->add('fbUrl', 'text', [
                'label' => 'Facebook URL',
                'required' => false,
                'attr' => ['placeholder' => 'Facebook URL placeholder'],
            ])
            ->add('vkUrl', 'text', [
                'label' => 'VK URL',
                'required' => false,
                'attr' => ['placeholder' => 'VK URL placeholder'],
            ])
            ->add('okUrl', 'text', [
                'label' => 'Odnoklassniki URL',
                'required' => false,
                'attr' => ['placeholder' => 'Odnoklassniki URL placeholder'],
            ])
            ->add('gpUrl', 'text', [
                'label' => 'Google+ URL',
                'required' => false,
                'attr' => ['placeholder' => 'Google+ URL placeholder'],
            ])
            ->add('twitterUrl', 'text', [
                'label' => 'Twitter URL',
                'required' => false,
                'attr' => ['placeholder' => 'Twitter URL placeholder'],
            ])
            ->add('linkedInUrl', 'text', [
                'label' => 'Linked In URL',
                'required' => false,
                'attr' => ['placeholder' => 'LinkedIn URL placeholder'],
            ])
            ->add('skypeName', 'text', [
                'label' => 'Skype name',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Skype name placeholder',
                    'data-clear-company' => '',
                ],
            ])
            ->add('scope', 'scope')
            ->add('youtubeUrl', 'text', [
                'label' => 'Youtube Url',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Youtube URL placeholder',
                    'data-clear-company' => '',
                ],
            ])
            ->add('position', PositionAutocompleteType::class, ['required' => true])
            ->add('professionalArea', ProfessionalAreaCollectionPopupType::class, [
                'required' => true,
                'label' => 'Professional Area',
            ])
            ->add('requirements', 'ckeditor', [
                'config' => ['width' => '280px', 'inline' => true],
                'config_name' => 'jobCvFields',
                'required' => true
            ])
            ->add('responsibilities', 'ckeditor', [
                'label' => 'Responsibilities',
                'config' => ['width' => '280px', 'inline' => true],
                'config_name' => 'jobCvFields',
                'required' => true
            ])
            ->add('workingConditions', 'ckeditor', [
                'label' => 'Working conditions',
                'config' => ['width' => '280px', 'inline' => true],
                'config_name' => 'jobCvFields',
                'required' => true
            ])
            ->add('additionalInformation', 'ckeditor', [
                'label' => 'Information',
                'config' => ['width' => '280px', 'inline' => true],
                'config_name' => 'jobCvFields',
                'required' => true
            ])
            ->add('requiredEducationType', 'entity', [
                'label' => 'Required education',
                'class' => 'RDWJobBundle:EducationType',
                'empty_value' => 'No mattter',
                'property' => 'title',
                'required' => false,
            ])
            ->add('workExperienceType', 'entity', [
                'label' => 'Work experience',
                'class' => 'RDWJobBundle:WorkExperienceType',
                'empty_value' => 'No matter',
                'property' => 'title',
                'required' => false,
            ])
            ->add('salaryFrom', 'salary', [
                'required' => true,
                'label' => 'Salary from',
                'attr' => ['placeholder' => 'Salary from placeholder'],
            ])
            ->add('salaryTo', 'salary', [
                'required' => false,
                'label' => 'Salary to',
                'attr' => ['placeholder' => 'Salary to placeholder'],
            ])
            ->add('workScheduleType', 'entity', [
                'class' => 'RDWJobBundle:WorkScheduleType',
                'empty_value' => 'Choose option',
                'property' => 'title',
                'required' => false,
            ])
            ->add('languages', 'bootstrap_collection', [
                'type' => new LanguageFormType(),
                'allow_add' => (! $options['disabled']) ? true : false,
                'allow_delete' => (! $options['disabled']) ? true : false,
                'by_reference' => false,
                'required' => false,
                'label' => 'Languages',
            ])
            ->add('contactPerson', 'text', [
                'label' => 'Contact person name',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Contact person name placeholder',
                    'data-clear-person' => '',
                ],
            ])
            ->add('contactPersonPatronymic', 'text', [
                'label' => 'Contact person patronymic',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Contact person patronymic placeholder',
                    'data-clear-person' => '',
                ],
            ])
            ->add('contactPersonSurname', 'text', [
                'label' => 'Contact person surname',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Contact person surname placeholder',
                    'data-clear-person' => '',
                ],
            ])
            ->add('contactPersonPhones', 'bootstrap_collection', [
                'type' => new ContactPersonPhoneType(),
                'allow_add' => (! $options['disabled']) ? true : false,
                'allow_delete' => (! $options['disabled']) ? true : false,
                'by_reference' => false,
                'required' => false,
                'label' => 'Contact phone',
            ])
            ->add('contactPersonEmail', 'text', [
                'label' => 'Contact email',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Contact email placeholder',
                    'data-clear-person' => '',
                ],
            ])
            ->add('contactPersonSkype', 'text', [
                'label' => 'Contact skype',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Contact skype placeholder',
                    'data-clear-person' => '',
                ],
            ])
            ->add('contactPersonAddress', 'text', [
                'label' => 'Contact address',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Contact address placeholder',
                    'data-clear-person' => '',
                ],
            ])
            ->add('contactPersonCity', 'city', [
                'label' => 'Contact city',
            ])
            ->add('validTill', 'date', [
                'required' => true,
            ])
            ->add('secret', 'choice', [
                'choices'   => [
                    0 => 'Show company info',
                    1 => 'Do not show company info',
                ],
                'label' => 'Company info',
                'required' => true,
                'expanded' => true,
            ])
            ->add('driverLicenseCategories', 'entity', [
                'label' => 'Driver license category',
                'class' => 'RDWCvBundle:DriverLicenseCategory',
                'property' => 'title',
                'expanded' => true,
                'multiple' => true,
                'required' => false,
            ])
            ->add('carNeeded', 'choice', [
                'label' => 'Car',
                'choices'  => [
                    false => 'Not required',
                    true  => 'Required',
                ],
                'expanded' => true,
                'multiple' => false,
                'required' => false,
                'empty_value' => false,
            ])
            ->add('email', 'text', [
                'label' => 'Email',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Email placeholder',
                    'data-clear-company' => '',
                ],
            ])
            ->add('employmentType', 'entity', [
                'class' => 'RDWJobBundle:employmentType',
                'empty_value' => 'Choose option',
                'property' => 'title',
                'required' => false,
            ]);

            $builder
                ->get('salaryFrom')
                ->addModelTransformer(new FormattedNumberTransformer());
            $builder
                ->get('salaryTo')
                ->addModelTransformer(new FormattedNumberTransformer());
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return '';
    }
}
