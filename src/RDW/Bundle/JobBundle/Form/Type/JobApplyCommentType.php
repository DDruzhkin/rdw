<?php

namespace RDW\Bundle\JobBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class JobApplyCommentType
 *
 * @package RDW\Bundle\JobBundle\Form\Type
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class JobApplyCommentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('employerComment', 'textarea', [
                'required' => false,
                'attr' => ['placeholder' => 'Employer comment placeholder']
            ])
            ->add('submit', 'submit', array(
                'label' => 'Save'
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RDW\Bundle\JobBundle\Entity\Apply',
            'csrf_protection' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_job_apply_comment';
    }
}
