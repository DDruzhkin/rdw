<?php

namespace RDW\Bundle\JobBundle\Form\Type;

use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Repository\JobRepository;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class JobViewFilterType
 * @package RDW\Bundle\JobBundle\Form\Type
 *
 * @author Paulius Aleliūnas <paulius@eface.lt>
 */
class JobViewFilterType extends AbstractType
{
    /**
     * @var RegisteredUser
     */
    protected $user;

    /**
     * @param RegisteredUser $user
     */
    public function __construct(RegisteredUser $user)
    {
        $this->user = $user;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('GET');

        $builder
            ->add('job', 'entity', [
                'class' => 'RDWJobBundle:Job',
                'query_builder' => function (JobRepository $er) {
                    return $er->getMyListQueryBuilder($this->user);
                },
                'property' => 'position.title',
                'empty_value' => 'All jobs',
                'required' => false,
                'label' => 'Filter by job',
            ])
            ->add('search', 'submit', ['label' => 'Search']);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RDW\Bundle\JobBundle\Model\ApplyFilter',
            'csrf_protection' => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return '';
    }
}
