<?php

namespace RDW\Bundle\JobBundle\Form\Type;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\JobBundle\Entity\Apply;
use RDW\Bundle\JobBundle\Form\DataTransformer\JobsTransformer;
use RDW\Bundle\UserBundle\Entity\User;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class MultiJobApplyType
 *
 * @package RDW\Bundle\JobBundle\Form\Type
 */
class MultiJobApplyType extends JobApplyType
{
    private $objectManager;

    /**
     * @param User          $user
     * @param ObjectManager $objectManager
     */
    public function __construct(User $user, ObjectManager $objectManager)
    {
        parent::__construct($user);

        $this->objectManager = $objectManager;
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $jobsTransformer = new JobsTransformer($this->objectManager);
        $builder->add(
            $builder->create('jobs', 'hidden', [
                'required' => false,
            ])->addModelTransformer($jobsTransformer)
        );

        $builder->remove('position');
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\JobBundle\Entity\Apply',
            'validation_groups' =>
                function (FormInterface $form) {
                    $groups = ['multi_apply'];

                    if ($form->getData()->getCv() instanceof Cv) {
                        $groups[] = 'apply_my';
                    } else {
                        $groups[] = 'apply_upload';
                    }

                    return $groups;
                },
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_multi_job_apply';
    }
}
