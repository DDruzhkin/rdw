<?php

namespace RDW\Bundle\JobBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class EmailForEmployeeType
 *
 * @package RDW\Bundle\JobBundle\Form\Type
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class EmailForEmployeeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('companyTitle', 'text', [
                'required' => false,
                'attr' => ['placeholder' => 'Company title placeholder']
            ])
            ->add('text', 'textarea', [
                'required' => false,
                'attr' => ['placeholder' => 'Message placeholder']
            ])
            ->add('submit', 'submit', array(
                'label' => 'Send'
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\JobBundle\Model\EmailForEmployee',
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'email_for_employee';
    }
}
