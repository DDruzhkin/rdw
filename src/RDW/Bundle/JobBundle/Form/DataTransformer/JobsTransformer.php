<?php

namespace RDW\Bundle\JobBundle\Form\DataTransformer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\PersistentCollection;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\JobBundle\Entity\Job;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class JobsTransformer
 *
 * @package RDW\Bundle\JobBundle\Form\DataTransformer
 */
class JobsTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $entityManager;

    /**
     * @param ObjectManager $entityManager
     */
    public function __construct(ObjectManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Transform to objects to string of objects' ids
     *
     * @param PersistentCollection $entity
     *
     * @return string
     */
    public function transform($entity)
    {
        $ids = [];

        if (null !== $entity && ($entity->count())) {
            foreach ($entity as $key => $value) {
                $ids[] = $value->getId();
            }
        }

        return implode(',', $ids);
    }

    /**
     * @param string $propertyValueIds
     *
     * @return mixed|object
     */
    public function reverseTransform($propertyValueIds)
    {
        $collection = new ArrayCollection();
        $ids = explode(',', $propertyValueIds);

        $repository = $this->entityManager->getRepository('RDWJobBundle:Job');

        foreach ($ids as $id) {
            $job = $repository->find(trim($id));

            if ($job instanceof Job) {
                $collection->add($job);
            }
        }

        return $collection;
    }
}
