<?php

namespace RDW\Bundle\JobBundle;

/**
 * Class RDWJobEvents
 * Contains all events thrown in the RDWJobBundle
 *
 * @package RDW\Bundle\JobBundle
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
final class RDWJobEvents
{
    /**
     * This event occurs then job form is submitted by pressing Save button
     */
    const JOB_SAVE = 'rdw_job.event.job_save';

    /**
     * This event occurs then job form is submitted by pressing Publish button
     */
    const JOB_PUBLISH = 'rdw_job.event.job_publish';

    /**
     * This event occurs then censor approves job
     */
    const JOB_APPROVE = 'rdw_job.event.job_approve';

    /**
     * This event occurs then censor successfully submits block form
     */
    const JOB_BLOCK = 'rdw_job.event.job_block';

    /**
     * The APPLY_SUCCESS event occurs when the User successfully applies to looking job
     */
    const APPLY_SUCCESS = 'rdw_job.event.apply_success';

    /**
     * The MULTI_APPLY_SUCCESS event occurs when the User successfully applies to multiple jobs
     */
    const MULTI_APPLY_SUCCESS = 'rdw_job.event.multi_apply_success';

    /**
     * This event occurs when job is viewed
     */
    const JOB_VIEW = 'rdw_job.event.job_view';
}
