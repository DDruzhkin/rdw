<?php

namespace RDW\Bundle\JobBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\ManageBundle\Entity\Filter;

/**
 * Class WorkExperienceType
 *
 * @package RDW\Bundle\JobBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="work_experience_types")
 */
class WorkExperienceType extends Filter
{
    const TYPE_NO_EXPERIENCE = 'no_experience';
    const TYPE_LESS_YEAR = 'less_year';
    const TYPE_ONE_YEAR = 'one_year';
    const TYPE_THREE_YEAR = 'three_year';
    const TYPE_FIVE_YEAR = 'five_year';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\JobBundle\Entity\Job", mappedBy="workExperienceType", fetch="EXTRA_LAZY")
     */
    protected $jobs;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\CvBundle\Entity\Cv", mappedBy="workExperienceType", fetch="EXTRA_LAZY")
     */
    protected $cvs;

    /**
     * constructor
     */
    public function __construct()
    {
        $this->jobs = new ArrayCollection();
        $this->cvs = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function isDeletable()
    {
        return !($this->jobs->count() > 0 || $this->cvs->count() > 0 || $this->isSystem());
    }

    /**
     * @param Job $job
     * @return $this
     */
    public function addJob(Job $job)
    {
        $this->jobs->add($job);

        return $this;
    }

    /**
     * @param Job $job
     * @return $this
     */
    public function removeJob(Job $job)
    {
        $this->jobs->removeElement($job);

        return $this;
    }

    /**
     * @param Cv $cv
     * @return $this
     */
    public function addCv(Cv $cv)
    {
        $this->cvs->add($cv);

        return $this;
    }

    /**
     * @param Cv $cv
     * @return $this
     */
    public function removeCv(Cv $cv)
    {
        $this->cvs->removeElement($cv);

        return $this;
    }

    /**
     * @param ArrayCollection $jobs
     * @return $this
     */
    public function setJobs($jobs)
    {
        $this->jobs = $jobs;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * @param ArrayCollection $cvs
     * @return $this
     */
    public function setCvs($cvs)
    {
        $this->cvs = $cvs;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getCvs()
    {
        return $this->cvs;
    }

    /**
     * @return boolean
     */
    public function isWithoutExeperience()
    {
        return self::TYPE_NO_EXPERIENCE === $this->getType();
    }
}
