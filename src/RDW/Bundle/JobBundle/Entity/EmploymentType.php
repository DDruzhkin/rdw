<?php

namespace RDW\Bundle\JobBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\ManageBundle\Entity\Filter;

/**
 * Class Language
 *
 * @package RDW\Bundle\JobBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="employment_types")
 */
class EmploymentType extends Filter
{
    const TYPE_PARTIAL = 'partial';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\JobBundle\Entity\Job", mappedBy="employmentType", fetch="EXTRA_LAZY")
     */
    private $jobs;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\CvBundle\Entity\Cv", mappedBy="employmentType", fetch="EXTRA_LAZY")
     */
    private $cvs;

    /**
     * constructor
     */
    public function __construct()
    {
        $this->jobs = new ArrayCollection();
        $this->cvs = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function isDeletable()
    {
        return ! ($this->jobs->count() > 0 || $this->cvs->count() > 0 || $this->isSystem());
    }

    public function isPartialEmployment()
    {
        return self::TYPE_PARTIAL === $this->getType();
    }
}
