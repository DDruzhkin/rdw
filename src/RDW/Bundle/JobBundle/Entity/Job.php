<?php

namespace RDW\Bundle\JobBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\AppBundle\Entity\Traits\ContactPhonesTrait;
use RDW\Bundle\LocationBundle\Entity\City;
use RDW\Bundle\OrderBundle\Entity\OrderItemJob;
use RDW\Bundle\ServiceBundle\Entity\HighlightedItemInterface;
use RDW\Bundle\ServiceBundle\Entity\TopItemInterface;
use RDW\Bundle\ServiceBundle\Entity\Service;
use RDW\Bundle\UserBundle\Entity\CompanyType;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\Scope;
use RDW\Bundle\UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use RDW\Bundle\JobBundle\Validator\Constraints as RDWAssert;
use RDW\Bundle\CvBundle\Entity\DriverLicenseCategory;
use RDW\Bundle\AppBundle\Entity\HighlightedTrait;
use RDW\Bundle\AppBundle\Entity\TopTrait;
use RDW\Bundle\AppBundle\Entity\TotalViewsCountTrait;
use RDW\Bundle\AppBundle\Entity\SluggifyTrait;
use RDW\Bundle\AppBundle\Entity\AbstractAd;
use RDW\Bundle\AppBundle\Entity\AdInterface;

/**
 * Class Job
 *
 * @package RDW\Bundle\JobBundle\Entity
 *
 * @ORM\Entity(repositoryClass="RDW\Bundle\JobBundle\Repository\JobRepository")
 * @ORM\Table(
 *      name="jobs",
 *      indexes={
 *          @ORM\Index(name="foreign_id_idx", columns={"foreign_id"}),
 *          @ORM\Index(name="slug_idx", columns={"slug"})
 *      }
 * )
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Job extends AbstractAd implements HighlightedItemInterface, TopItemInterface, AdInterface
{
    use HighlightedTrait;
    use TopTrait;
    use SluggifyTrait;
    use TotalViewsCountTrait;
    use ContactPhonesTrait;

    const GENDER_MALE   = 'male';
    const GENDER_FEMALE = 'female';

    const STATUS_INACTIVE = 'inactive'; // when job saved without validation or status inactivated by manager
    const STATUS_PUBLISHED = 'published'; // when job published but not paid
    const STATUS_WAITING  = 'waiting';  // when job is waiting for censors confirmation
    const STATUS_ACTIVE   = 'active';   // when job is activated by censor
    const STATUS_BLOCKED  = 'blocked';
    const STATUS_DELETED  = 'deleted';

    const EMPLOYER_TYPE_COMPANY = 1;
    const EMPLOYER_TYPE_AGENCY  = 2;
    const EMPLOYER_TYPE_PERSON  = 3;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="employer_type", type="integer", nullable=true)
     * @Assert\NotBlank(message="Employer type cannot be empty", groups={"publish", "censor"})
     */
    private $employerType;

    /**
     * @var CompanyType
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\CompanyType", inversedBy="jobs", cascade={"merge", "persist"})
     * @ORM\JoinColumn(name="company_type_id", referencedColumnName="id")
     *
     * @Assert\NotNull(message="Company type cannot be empty", groups={"create", "publish", "censor", "import"})
     */
     private $companyType;

    /**
     * @var string
     *
     * @ORM\Column(name="company_title", type="string", nullable=true)
     * @Assert\NotBlank(message="Company title cannot be empty", groups={"publish", "censor", "import"})
     */
    private $companyTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="company_trade_mark", type="string", nullable=true)
     */
    private $companyTradeMark;

    /**
     * @var City
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\LocationBundle\Entity\City", inversedBy="jobs", cascade={"merge", "persist"})
     * @ORM\JoinColumn(name="working_place_city_id", referencedColumnName="id")
     */
    protected $workingPlaceCity;

    /**
     * @var bool
     */
    private $isApprovedByCensor;

    /**
     * @var string
     *
     * @ORM\Column(name="company_url", type="string", nullable=true)
     */
    private $companyUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="company_info", type="text", nullable=true)
     * @Assert\Length(
     *      max = "1000",
     *      maxMessage = "Company info cannot be longer than {{ limit }} characters length",
     *      groups={"create", "publish", "censor"}
     * )
     */
    private $companyInfo;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="RDW\Bundle\LocationBundle\Entity\City", cascade={"persist", "merge"})
     */
    private $cities;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", nullable=true)
     * @Assert\Regex(
     *      pattern="/^([\+0-9\s\-]+)$/",
     *      message="Field must be a valid phone number and can have only numbers, +, - and spaces",
     *      groups={"create", "publish", "censor"}
     * )
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", nullable=true)
     */
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", nullable=true)
     * @Assert\NotBlank(message="Email is required", groups={"import", "create"})
     * @Assert\Email(message="Email must be a valid email address", groups={"create", "publish", "censor", "import"})
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="fb_url", type="string", nullable=true)
     * @Assert\Url(message="Facebook url must be a valid url address", groups={"publish", "censor"})
     */
    private $fbUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="block_reason", type="text", nullable=true)
     * @Assert\NotBlank(message="Please enter block reason", groups={"block"})
     */
    private $blockReason;

    /**
     * @var string
     *
     * @ORM\Column(name="vk_url", type="string", nullable=true)
     * @Assert\Url(message="VK url must be a valid url address", groups={"publish", "censor"})
     */
    private $vkUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="ok_url", type="string", nullable=true)
     * @Assert\Url(message="Odnoklasniki url must be a valid url address", groups={"publish", "censor"})
     */
    private $okUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="gp_url", type="string", nullable=true)
     * @Assert\Url(message="Google+ url must be a valid url address", groups={"publish", "censor"})
     */
    private $gpUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter_url", type="string", nullable=true)
     * @Assert\Url(message="Twitter url must be a valid url address", groups={"publish", "censor"})
     */
    private $twitterUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="linked_in_url", type="string", nullable=true)
     * @Assert\Url(message="Linked In url must be a valid url address", groups={"publish", "censor"})
     */
    private $linkedInUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="skype_name", type="string", nullable=true)
     */
    private $skypeName;

    /**
     * @var Scope
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\Scope", inversedBy="jobs", cascade={"merge", "persist"})
     * @ORM\JoinColumn(name="scope_id", referencedColumnName="id")
     */
    private $scope;

    /**
     * @var ProfessionalArea
     *
     * @ORM\ManyToMany(
     *     targetEntity="RDW\Bundle\JobBundle\Entity\ProfessionalArea",
     *     cascade={"persist"},
     *     fetch="EXTRA_LAZY",
     *     inversedBy="jobs"
     * )
     * @orm\JoinTable(name="job_professionalarea",
     *      joinColumns={@ORM\JoinColumn(name="job_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="professionalarea_id", referencedColumnName="id")}
     *      )
     * @Assert\NotBlank(message="Professional Area cannot be empty", groups={"create", "publish", "censor", "import"})
     */
    private $professionalArea;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\RegisteredUser", inversedBy="jobs", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\JobBundle\Entity\JobView", mappedBy="job", cascade={"persist", "merge"})
     */
    protected $usersWhoViewed;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\JobBundle\Entity\JobShowContacts", mappedBy="job", cascade={"persist", "merge"})
     */
    protected $usersWhoShowedContacts;

    /**
     * @var string
     *
     * @ORM\Column(name="youtube_url", type="string", nullable=true)
     * @Assert\Url(message="Youtube url must be a valid url address", groups={"publish", "censor"})
     */
    private $youtubeUrl;

    /**
     * @var Position
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\JobBundle\Entity\Position", inversedBy="jobs", cascade={"merge", "persist"})
     * @ORM\JoinColumn(name="position_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank(message="Position cannot be empty", groups={"create", "publish", "censor", "import"})
     */
    protected $position;

    /**
     * @var string
     *
     * @ORM\Column(name="requirements", type="text", nullable=true)
     * @Assert\NotBlank(message="Requirements cannot be empty", groups={"publish", "censor"})
     * @Assert\Length(
     *      max = "1000",
     *      maxMessage = "Requirements cannot be longer than {{ limit }} characters length",
     *      groups={"create", "publish", "censor"}
     * )
     */
    private $requirements;

    /**
     * @var string
     *
     * @ORM\Column(name="responsibilities", type="text", nullable=true)
     * @Assert\Length(
     *      max = "1000",
     *      maxMessage = "Responsibilities cannot be longer than {{ limit }} characters length",
     *      groups={"create", "publish", "censor"}
     * )
     */
    private $responsibilities;

    /**
     * @var string
     *
     * @ORM\Column(name="working_conditions", type="text", nullable=true)
     * @Assert\Length(
     *      max = "1000",
     *      maxMessage = "Working conditions cannot be longer than {{ limit }} characters length",
     *      groups={"create", "publish", "censor"}
     * )
     */
    private $workingConditions;

    /**
     * @var EducationType
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\JobBundle\Entity\EducationType", inversedBy="jobs", cascade={"persist", "merge"})
     */
    private $requiredEducationType;

    /**
     * @var WorkExperienceType
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\JobBundle\Entity\WorkExperienceType", inversedBy="jobs", cascade={"persist", "merge"})
     */
    private $workExperienceType;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="text", nullable=true)
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="work_type", type="text", nullable=true)
     */
    private $workType;

    /**
     * @var WorkScheduleType
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\JobBundle\Entity\WorkScheduleType", inversedBy="jobs", cascade={"persist", "merge"})
     * @ORM\JoinColumn(name="work_schedule_type_id", referencedColumnName="id")
     */
    private $workScheduleType;

    /**
     * @var integer
     *
     * @ORM\Column(name="salary_from", type="integer", nullable=true)
     * @Assert\NotBlank(message="Salary cannot be empty", groups={"publish", "censor"})
     * @Assert\Regex(pattern="/\d+/", message="The value {{ value }} is not a valid {{ type }}.", groups={"publish", "censor"})
     * @Assert\GreaterThanOrEqual(value = 0, groups={"publish", "censor"})
     * @Assert\Length(
     *      max = "8",
     *      maxMessage = "Salary cannot be longer than {{ limit }} characters length",
     *      groups={"create", "publish", "censor"}
     * )
     */
    protected $salaryFrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="salary_to", type="integer", nullable=true)
     * @Assert\Regex(pattern="/\d+/", message="The value {{ value }} is not a valid {{ type }}.", groups={"publish", "censor", "import"})
     * @Assert\GreaterThanOrEqual(value = 0, groups={"publish", "censor", "import"})
     * @Assert\Length(
     *      max = "8",
     *      maxMessage = "Salary cannot be longer than {{ limit }} characters length",
     *      groups={"create", "publish", "censor"}
     * )
     */
    protected $salaryTo;

    /**
     * @var string
     *
     * @ORM\Column(name="additional_information", type="text", nullable=true)
     * @Assert\Length(
     *      max = "1000",
     *      maxMessage = "Additional information cannot be longer than {{ limit }} characters length",
     *      groups={"create", "publish", "censor"}
     * )
     */
    private $additionalInformation;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_person", type="text", nullable=true)
     * @Assert\NotBlank(message="Contact person cannot be empty", groups={"publish", "censor"})
     */
    private $contactPerson;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_person_surname", type="text", nullable=true)
     */
    private $contactPersonSurname;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_person_patronymic", type="text", nullable=true)
     */
    private $contactPersonPatronymic;

    /**
     * @var string
     * @ORM\Column(name="contact_person_photo", type="string", nullable=true)
     */
    protected $contactPersonPhoto;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\JobBundle\Entity\ContactPersonPhone", mappedBy="entity", cascade={"persist", "remove", "merge"}, orphanRemoval=true)
     */
    private $contactPersonPhones;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_person_email", type="text", nullable=true)
     * @Assert\Email(message="Contact email must be a valid email address", groups={"create", "publish", "censor"})
     */
    private $contactPersonEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_person_skype", type="text", nullable=true)
     */
    private $contactPersonSkype;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_person_address", type="text", nullable=true)
     */
    private $contactPersonAddress;

    /**
     * @var City
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\LocationBundle\Entity\City", cascade={"persist", "merge"})
     * @ORM\JoinColumn(name="contact_person_city_id", referencedColumnName="id")
     */
    private $contactPersonCity;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="valid_till", type="date", nullable=true)
     * @Assert\NotBlank(message="Please enter date", groups={"activate", "import"})
     * @RDWAssert\DateRange(min="today", groups={"activate", "import"})
     */
    private $validTill;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="text", length=100)
     * @Assert\NotBlank(groups={"censor"})
     */
    private $status;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_secret", type="boolean")
     */
    private $secret;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_vip", type="boolean")
     */
    private $vip;

    /**
     * @var bool
     *
     * @ORM\Column(name="show_in_carousel", type="boolean")
     */
    private $showInCarousel;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var string
     * @ORM\Column(name="photo", type="string", nullable=true)
     */
    private $photo;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\JobBundle\Entity\JobLanguage", mappedBy="job", cascade={"persist", "remove", "merge"}, orphanRemoval=true)
     */
    private $languages;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\OrderBundle\Entity\OrderItemJob", mappedBy="item", cascade={"persist", "merge"})
     */
    protected $services;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\JobBundle\Entity\Apply", mappedBy="job", cascade={"persist", "merge"})
     */
    private $applies;

    /**
     * @var string
     */
    private $oldStatus;

    /**
     * @var int
     * @ORM\Column(type="integer", name="views_count", nullable=true, options={"default" = 0})
     */
    protected $viewsCount;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", name="views_count_updated_at", nullable=true)
     */
    protected $viewsCountUpdatedAt;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="RDW\Bundle\CvBundle\Entity\DriverLicenseCategory", cascade={"persist", "merge"})
     * @ORM\JoinTable(name="job_driver_licences",
     *      joinColumns={@ORM\JoinColumn(name="driver_licence_category_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="job_id", referencedColumnName="id")}
     * )
     */
    private $driverLicenseCategories;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true, name="car_needed")
     */
    private $carNeeded;

    /**
     * @var EmploymentType
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\JobBundle\Entity\EmploymentType", inversedBy="jobs")
     * @ORM\JoinColumn(name="employment_type_id", referencedColumnName="id")
     */
    private $employmentType;

    /**
     * @var int
     *
     * @ORM\Column(name="foreign_id", type="integer", nullable=true)
     * @Assert\NotBlank(groups={"import"})
     */
    private $foreignId;

    /**
     * @var int
     *
     * @ORM\Column(name="crm_order_id", type="integer", nullable=true)
     */
    private $crmOrderId;

    /**
     * @var string
     *
     * @ORM\Column(name="full_description_url", type="string", nullable=true)
     * @Assert\Url(message="Full description url must be a valid url address", groups={"publish", "censor", "import"})
     */
    private $fullDescriptionUrl;

    /**
     * @ORM\Column(name="allow_export", type="boolean", nullable=true, options={"default" = 1})
     */
    private $allowExport;

    /**
     * @ORM\Column(name="allow_apply", type="boolean", nullable=true, options={"default" = 1})
     */
    private $allowApply;

    /**
     * @var int
     * @ORM\Column(type="integer", name="views_contact_count", nullable=true, options={"default" = 0})
     */
    protected $viewsContactCount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->status = self::STATUS_INACTIVE;
        $this->secret = false;
        $this->vip = false;
        $this->showInCarousel = false;
        $this->cities = new ArrayCollection();
        $this->languages = new ArrayCollection();
        $this->applies = new ArrayCollection();
        $this->driverLicenseCategories = new ArrayCollection();
        $this->contactPersonPhones = new ArrayCollection();
        $this->professionalArea = new ArrayCollection();

        // default +1 year
        $this->validTill = new \DateTime();
        $this->validTill->add(new \DateInterval('P1Y'));
        $this->isApprovedByCensor = false;
        $this->allowExport = true;
        $this->allowApply = true;
        $this->viewsCount = 0;
        $this->viewsContactCount = 0;
    }

    /**
     * @return string
     */
    public function getRequirements()
    {
        return $this->requirements;
    }

    /**
     * @param string $requirements
     *
     * @return $this
     */
    public function setRequirements($requirements)
    {
        $this->requirements = $requirements;

        return $this;
    }

    /**
     * @return string
     */
    public function getResponsibilities()
    {
        return $this->responsibilities;
    }

    /**
     * @param string $responsibilities
     *
     * @return $this
     */
    public function setResponsibilities($responsibilities)
    {
        $this->responsibilities = $responsibilities;

        return $this;
    }

    /**
     * @return string
     */
    public function getWorkingConditions()
    {
        return $this->workingConditions;
    }

    /**
     * @return null|string
     */
    public function getEmployerTypeTitle()
    {
        $employerTypes = RegisteredUser::getEmployerTypeChoiceList();

        if (array_key_exists($this->getEmployerType(), $employerTypes)) {
            return  $employerTypes[$this->getEmployerType()];
        }

        return null;
    }

    /**
     * @param string $workingConditions
     *
     * @return $this
     */
    public function setWorkingConditions($workingConditions)
    {
        $this->workingConditions = $workingConditions;

        return $this;
    }

    /**
     * @return EducationType
     */
    public function getRequiredEducationType()
    {
        return $this->requiredEducationType;
    }

    /**
     * @param EducationType $requiredEducationType
     *
     * @return $this
     */
    public function setRequiredEducationType($requiredEducationType)
    {
        $this->requiredEducationType = $requiredEducationType;

        return $this;
    }

    /**
     * @return WorkExperienceType
     */
    public function getWorkExperienceType()
    {
        return $this->workExperienceType;
    }

    /**
     * @param WorkExperienceType $workExperienceType
     *
     * @return $this
     */
    public function setWorkExperienceType($workExperienceType)
    {
        $this->workExperienceType = $workExperienceType;

        return $this;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     *
     * @return $this
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return array
     */
    public static function getGenderChoices()
    {
        return [
            self::GENDER_MALE => 'Male',
            self::GENDER_FEMALE => 'Female',
        ];
    }

    /**
     * @return string
     */
    public function getWorkType()
    {
        return $this->workType;
    }

    /**
     * @param string $workType
     *
     * @return $this
     */
    public function setWorkType($workType)
    {
        $this->workType = $workType;

        return $this;
    }

    /**
     * @return WorkScheduleType
     */
    public function getWorkScheduleType()
    {
        return $this->workScheduleType;
    }

    /**
     * @param WorkScheduleType $workScheduleType
     *
     * @return $this
     */
    public function setWorkScheduleType($workScheduleType)
    {
        $this->workScheduleType = $workScheduleType;

        return $this;
    }

    /**
     * @return string
     */
    public function getAdditionalInformation()
    {
        return $this->additionalInformation;
    }

    /**
     * @param string $additionalInformation
     *
     * @return $this
     */
    public function setAdditionalInformation($additionalInformation)
    {
        $this->additionalInformation = $additionalInformation;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * @param string $contactPerson
     *
     * @return $this
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactPersonSurname()
    {
        return $this->contactPersonSurname;
    }

    /**
     * @param string $contactPersonSurname
     *
     * @return $this
     */
    public function setContactPersonSurname($contactPersonSurname)
    {
        $this->contactPersonSurname = $contactPersonSurname;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactPersonEmail()
    {
        return $this->contactPersonEmail;
    }

    /**
     * @param string $contactPersonEmail
     *
     * @return $this
     */
    public function setContactPersonEmail($contactPersonEmail)
    {
        $this->contactPersonEmail = $contactPersonEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactPersonPatronymic()
    {
        return $this->contactPersonPatronymic;
    }

    /**
     * @param string $contactPersonPatronymic
     *
     * @return $this
     */
    public function setContactPersonPatronymic($contactPersonPatronymic)
    {
        $this->contactPersonPatronymic = $contactPersonPatronymic;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactPersonAddress()
    {
        return $this->contactPersonAddress;
    }

    /**
     * @param string $contactPersonAddress
     *
     * @return $this
     */
    public function setContactPersonAddress($contactPersonAddress)
    {
        $this->contactPersonAddress = $contactPersonAddress;

        return $this;
    }

    /**
     * @return City
     */
    public function getContactPersonCity()
    {
        return $this->contactPersonCity;
    }

    /**
     * @param City $contactPersonCity
     *
     * @return $this
     */
    public function setContactPersonCity($contactPersonCity)
    {
        $this->contactPersonCity = $contactPersonCity;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactPersonSkype()
    {
        return $this->contactPersonSkype;
    }

    /**
     * @param string $contactPersonSkype
     *
     * @return $this
     */
    public function setContactPersonSkype($contactPersonSkype)
    {
        $this->contactPersonSkype = $contactPersonSkype;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getValidTill()
    {
        return $this->validTill;
    }

    /**
     * @param \DateTime $validTill
     *
     * @return $this
     */
    public function setValidTill($validTill)
    {
        $this->validTill = $validTill;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * @param boolean $secret
     *
     * @return $this
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getVip()
    {
        return $this->vip;
    }

    /**
     * @param boolean $vip
     *
     * @return $this
     */
    public function setVip($vip)
    {
        $this->vip = $vip;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getShowInCarousel()
    {
        return $this->showInCarousel;
    }

    /**
     * @param boolean $showInCarousel
     *
     * @return $this
     */
    public function setShowInCarousel($showInCarousel)
    {
        $this->showInCarousel = $showInCarousel;

        return $this;
    }

    /**
     * @return int
     */
    public function getEmployerType()
    {
        return $this->employerType;
    }

    /**
     * @param int $employerType
     *
     * @return $this
     */
    public function setEmployerType($employerType)
    {
        $this->employerType = $employerType;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyTitle()
    {
        return $this->companyTitle;
    }

    /**
     * @param string $companyTitle
     *
     * @return $this
     */
    public function setCompanyTitle($companyTitle)
    {
        $this->companyTitle = $companyTitle;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyUrl()
    {
        return $this->companyUrl;
    }

    /**
     * @param string $companyUrl
     *
     * @return $this
     */
    public function setCompanyUrl($companyUrl)
    {
        $this->companyUrl = $companyUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyInfo()
    {
        return $this->companyInfo;
    }

    /**
     * @param string $companyInfo
     *
     * @return $this
     */
    public function setCompanyInfo($companyInfo)
    {
        $this->companyInfo = $companyInfo;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @return string
     */
    public function getCitiesAsString()
    {
        $titles = [];

        foreach ($this->getCities() as $city) {
            $titles[] = $city->getTitle();
        }

        return implode(', ', $titles);
    }

    /**
     * @param ArrayCollection $cities
     *
     * @return $this
     */
    public function setCities($cities)
    {
        $this->cities = $cities;

        return $this;
    }

    /**
     * @param City $city
     *
     * @return self
     */
    public function addCity($city)
    {
        if ($city && ! $this->cities->contains($city)) {
            $this->cities->add($city);
        }

        return $this;
    }

    /**
     * @param City $city
     *
     * @return $this
     */
    public function removeCity(City $city)
    {
        $this->cities->removeElement($city);

        return $this;
    }

    /**
     * @return CompanyType
     */
    public function getCompanyType()
    {
        return $this->companyType;
    }

    /**
     * @param CompanyType $companyType
     *
     * @return $this
     */
    public function setCompanyType($companyType)
    {
        $this->companyType = $companyType;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     *
     * @return $this
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param string $fax
     *
     * @return $this
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * @return string
     */
    public function getFbUrl()
    {
        return $this->fbUrl;
    }

    /**
     * @param string $fbUrl
     *
     * @return $this
     */
    public function setFbUrl($fbUrl)
    {
        $this->fbUrl = $fbUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getVkUrl()
    {
        return $this->vkUrl;
    }

    /**
     * @param string $vkUrl
     *
     * @return $this
     */
    public function setVkUrl($vkUrl)
    {
        $this->vkUrl = $vkUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getOkUrl()
    {
        return $this->okUrl;
    }

    /**
     * @param string $okUrl
     *
     * @return $this
     */
    public function setOkUrl($okUrl)
    {
        $this->okUrl = $okUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getGpUrl()
    {
        return $this->gpUrl;
    }

    /**
     * @param string $gpUrl
     *
     * @return $this
     */
    public function setGpUrl($gpUrl)
    {
        $this->gpUrl = $gpUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getTwitterUrl()
    {
        return $this->twitterUrl;
    }

    /**
     * @param string $twitterUrl
     *
     * @return $this
     */
    public function setTwitterUrl($twitterUrl)
    {
        $this->twitterUrl = $twitterUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getLinkedInUrl()
    {
        return $this->linkedInUrl;
    }

    /**
     * @param string $linkedInUrl
     *
     * @return $this
     */
    public function setLinkedInUrl($linkedInUrl)
    {
        $this->linkedInUrl = $linkedInUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getSkypeName()
    {
        return $this->skypeName;
    }

    /**
     * @param string $skypeName
     *
     * @return $this
     */
    public function setSkypeName($skypeName)
    {
        $this->skypeName = $skypeName;

        return $this;
    }

    /**
     * @return Scope
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * @param Scope $scope
     *
     * @return $this
     */
    public function setScope($scope)
    {
        $this->scope = $scope;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getProfessionalArea()
    {
        return $this->professionalArea;
    }

    /**
     * @param ProfessionalArea $professionalArea
     *
     * @return $this
     */
    public function setProfessionalArea($professionalArea)
    {
        $this->professionalArea = $professionalArea;

        return $this;
    }

    /**
     * @return string
     */
    public function getYoutubeUrl()
    {
        return $this->youtubeUrl;
    }

    /**
     * @param string $youtubeUrl
     *
     * @return $this
     */
    public function setYoutubeUrl($youtubeUrl)
    {
        $this->youtubeUrl = $youtubeUrl;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasVideo()
    {
        return ($this->getYoutubeUrl() != '') ? true : false;
    }

    /**
     * @return array
     */
    public static function getEmployerTypeChoiceList()
    {
        return [
            self::EMPLOYER_TYPE_COMPANY => 'Employer type company',
            self::EMPLOYER_TYPE_AGENCY => 'Employer type agency',
            self::EMPLOYER_TYPE_PERSON => 'Employer type person',
        ];
    }

    /**
     * @return array
     */
    public function getStatuses()
    {
        return [
            self::STATUS_INACTIVE => self::STATUS_INACTIVE,
            self::STATUS_BLOCKED => self::STATUS_BLOCKED,
            self::STATUS_DELETED => self::STATUS_DELETED,
            self::STATUS_PUBLISHED => self::STATUS_PUBLISHED,
            self::STATUS_WAITING => self::STATUS_WAITING,
            self::STATUS_ACTIVE => self::STATUS_ACTIVE,
        ];
    }

    /**
     * @return array
     */
    public function getStatusesForCensor()
    {
        return [
            Job::STATUS_WAITING => 'Waiting',
            Job::STATUS_PUBLISHED => 'Published',
            Job::STATUS_ACTIVE => 'Active',
            Job::STATUS_BLOCKED => 'Blocked',
        ];
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     *
     * @return $this
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return !is_null($this->getDeletedAt());
    }

    /**
     * Get job or job owner logo
     *
     * @return string
     */
    public function getLogo()
    {
        return ($this->photo) ? $this->photo : $this->getUser()->getPhoto();
    }

    /**
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param string $photo
     *
     * @return self
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactPersonPhoto()
    {
        return $this->contactPersonPhoto;
    }

    /**
     * @param string $contactPersonPhoto
     *
     * @return self
     */
    public function setContactPersonPhoto($contactPersonPhoto)
    {
        $this->contactPersonPhoto = $contactPersonPhoto;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @param JobLanguage $language
     *
     * @return $this
     */
    public function addLanguage($language)
    {
        if ($language && ! $this->languages->contains($language)) {
            $language->setJob($this);
            $this->languages->add($language);
        }

        return $this;
    }

    /**
     * @param JobLanguage $language
     *
     * @return $this
     */
    public function removeLanguage(JobLanguage $language)
    {
        $this->languages->removeElement($language);

        return $this;
    }

    /**
     * @param Apply $apply
     *
     * @return $this
     */
    public function addApply(Apply $apply)
    {
        $this->applies[] = $apply;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasActivePublishingService()
    {
        $services = $this->getActiveServices();

        /** @var OrderItemJob $service */
        foreach ($services as $service) {
            if (Service::TYPE_JOB_PUBLISHING == $service->getService()->getType()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Apply $apply
     *
     * @return $this
     */
    public function removeApply(Apply $apply)
    {
        $this->applies->removeElement($apply);

        return $this;
    }

    /**
     * @return ArrayCollection|Apply[]
     */
    public function getApplies()
    {
        return $this->applies;
    }

    /**
     * @return boolean
     */
    public function getIsApprovedByCensor()
    {
        return $this->isApprovedByCensor;
    }

    /**
     * @param mixed $isApprovedByCensor
     *
     * @return $this
     */
    public function setIsApprovedByCensor($isApprovedByCensor)
    {
        $this->isApprovedByCensor = $isApprovedByCensor;

        return $this;
    }

    /**
     * @return string
     */
    public function getOldStatus()
    {
        return $this->oldStatus;
    }

    /**
     * @param string $oldStatus
     *
     * @return $this
     */
    public function setOldStatus($oldStatus)
    {
        $this->oldStatus = $oldStatus;

        return $this;
    }

    /**
     * @return string
     */
    public function getBlockReason()
    {
        return $this->blockReason;
    }

    /**
     * @param string $blockReason
     *
     * @return $this
     */
    public function setBlockReason($blockReason)
    {
        $this->blockReason = $blockReason;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAllowedToChangeStatusForManager()
    {
        return ($this->isStatusActive() || $this->isStatusInActive()) ? true : false;
    }

    /**
     * @return bool
     */
    public function isStatusActive()
    {
        return ($this->getStatus() == self::STATUS_ACTIVE) ? true : false;
    }

    /**
     * @return bool
     */
    public function isStatusInActive()
    {
        return ($this->getStatus() == self::STATUS_INACTIVE) ? true : false;
    }

    /**
     * @return bool
     */
    public function isPublished()
    {
        $timeNow = new \DateTime();
        $time = $timeNow->format('Y-m-d');

        $validTill = $this->getValidTill();
        $validTill = $validTill->format('Y-m-d 00:00:00');


        return ($this->getStatus() == self::STATUS_ACTIVE && $validTill >= $time);
    }

    /**
     * @return boolean
     */
    public function hasApplies()
    {
        $statuses = [
            self::STATUS_INACTIVE => self::STATUS_INACTIVE,
            self::STATUS_ACTIVE => self::STATUS_ACTIVE,
            self::STATUS_WAITING => self::STATUS_WAITING,
        ];

        return ($this->getApplies()->count() && in_array($this->getStatus(), $statuses));
    }

    /**
     * @return bool
     */
    public function isEditable()
    {
        $statuses = [Job::STATUS_PUBLISHED, Job::STATUS_WAITING, Job::STATUS_INACTIVE, Job::STATUS_ACTIVE];

        return in_array($this->status, $statuses) ? true : false;
    }

    /**
     * @return bool
     */
    public function canPublish()
    {
        return (Job::STATUS_INACTIVE === $this->status);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->getCompanyTitle();
    }

    /**
     * @param User $manager
     *
     * @return bool
     */
    public function isManagerAllowedDelete(User $manager)
    {
        $allowed = false;
        $userManager = $this->getUser()->getManager();

        if (RegisteredUser::USER_TYPE_EMPLOYER == $manager->getType()
            && $manager->hasRole(RegisteredUser::ROLE_MANAGER)
            && $userManager
            && $userManager->getId() == $manager->getId()
        ) {
            $allowed = true;
        }

        return $allowed;
    }

    /**
     * @return bool
     */
    public function isStatusWaiting()
    {
        return (self::STATUS_WAITING == $this->getStatus());
    }

    /**
     * @return ArrayCollection
     */
    public function getDriverLicenseCategories()
    {
        return $this->driverLicenseCategories;
    }

    /**
     * @param ArrayCollection $categories
     *
     * @return $this
     */
    public function setDriverLicenseCategories(ArrayCollection $categories)
    {
        $this->driverLicenseCategories = $categories;

        return $this;
    }

    /**
     * @param DriverLicenseCategory $category
     *
     * @return $this
     */
    public function addDriverLicenseCategories(DriverLicenseCategory $category)
    {
        $this->driverLicenseCategories->add($category);

        return $this;
    }

    /**
     * @param DriverLicenseCategory $category
     *
     * @return $this
     */
    public function removeDriverLicenseCategories(DriverLicenseCategory $category)
    {
        $this->driverLicenseCategories->remove($category);

        return $this;
    }

    /**
     * @return boolean
     */
    public function isCarNeeded()
    {
        return $this->carNeeded;
    }

    /**
     * @param boolean $carNeeded
     *
     * @return $this
     */
    public function setCarNeeded($carNeeded)
    {
        $this->carNeeded = $carNeeded;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasPhoto()
    {
        return ! is_null($this->getLogo());
    }

    /**
     * @return EmploymentType
     */
    public function getEmploymentType()
    {
        return $this->employmentType;
    }

    /**
     * @param EmploymentType $employmentType
     *
     * @return $this
     */
    public function setEmploymentType($employmentType)
    {
        $this->employmentType = $employmentType;

        return $this;
    }

    /**
     * @return int
     */
    public function getAppliesCount()
    {
        $total = 0;

        foreach ($this->getApplies() as $apply) {
            if ($apply->getUser() instanceof RegisteredUser) {
                $total++;
            }
        }

        return $total;
    }

    /**
     * @return int
     */
    public function getForeignId()
    {
        return $this->foreignId;
    }

    /**
     * @param int $foreignId
     *
     * @return $this
     */
    public function setForeignId($foreignId)
    {
        $this->foreignId = $foreignId;

        return $this;
    }

    /**
     * @return int
     */
    public function getCrmOrderId()
    {
        return $this->crmOrderId;
    }

    /**
     * @param int $crmOrderId
     *
     * @return $this
     */
    public function setCrmOrderId($crmOrderId)
    {
        $this->crmOrderId = $crmOrderId;

        return $this;
    }

    /**
     * @return array
     */
    public function getStatusLabels()
    {
        return [
            self::STATUS_INACTIVE => 'Job inactive',
            self::STATUS_ACTIVE => 'Job active',
            self::STATUS_BLOCKED => 'blocked',
            self::STATUS_DELETED => 'deleted',
            self::STATUS_WAITING => 'waiting',
            self::STATUS_PUBLISHED => 'published',
        ];
    }

    /**
     * @return string
     */
    public function getStatusLabel()
    {
        $labels = $this->getStatusLabels();

        if (array_key_exists($this->status, $labels)) {
            return $labels[$this->status];
        }

        return 'status_undefined';
    }

    /**
     * Get carNeeded
     *
     * @return boolean
     */
    public function getCarNeeded()
    {
        return $this->carNeeded;
    }

    /**
     * Get highlighted
     *
     * @return boolean
     */
    public function getHighlighted()
    {
        return $this->highlighted;
    }

    /**
     * Get top
     *
     * @return boolean
     */
    public function getTop()
    {
        return $this->top;
    }

    public function setFullDescriptionUrl($url)
    {
        $this->fullDescriptionUrl = $url;

        return $this;
    }

    public function getFullDescriptionUrl()
    {
        return $this->fullDescriptionUrl;
    }

    /**
     * Add driverLicenseCategories
     *
     * @param \RDW\Bundle\CvBundle\Entity\DriverLicenseCategory $driverLicenseCategories
     * @return Job
     */
    public function addDriverLicenseCategory(\RDW\Bundle\CvBundle\Entity\DriverLicenseCategory $driverLicenseCategories)
    {
        $this->driverLicenseCategories[] = $driverLicenseCategories;

        return $this;
    }

    /**
     * Remove driverLicenseCategories
     *
     * @param \RDW\Bundle\CvBundle\Entity\DriverLicenseCategory $driverLicenseCategories
     */
    public function removeDriverLicenseCategory(\RDW\Bundle\CvBundle\Entity\DriverLicenseCategory $driverLicenseCategories)
    {
        $this->driverLicenseCategories->removeElement($driverLicenseCategories);
    }

    public function isAllowedToDeleteByUser(RegisteredUser $user)
    {
        $allow = false;

        if (($user->isManager() && $this->isManagerAllowedDelete($user))
            || $this->isOwner($user)
        ) {
            $allow = true;
        }

        return $allow;
    }

    public function setAllowExport($allowExport)
    {
        $this->allowExport = $allowExport;

        return $this;
    }

    public function getAllowExport()
    {
        return $this->allowExport;
    }

    public function setAllowApply($allowApply)
    {
        $this->allowApply = $allowApply;

        return $this;
    }

    public function getAllowApply()
    {
        return $this->allowApply;
    }

    /**
     * @param string $number
     *
     * @return self
     */
    public function addContactPersonPhoneByString($number)
    {
        if ($number) {
            $phone = (new ContactPersonPhone())
                ->setEntity($this)
                ->setNumber($number)
            ;

            $this->addContactPersonPhone($phone);
        }

        return $this;
    }

    /**
     * @return ContactPersonPhone
     */
    public static function createContactPersonPhone()
    {
        return new ContactPersonPhone();
    }

    /**
     * Set companyTradeMark
     *
     * @param string $companyTradeMark
     * @return $this
     */
    public function setCompanyTradeMark($companyTradeMark)
    {
        $this->companyTradeMark = $companyTradeMark;

        return $this;
    }

    /**
     * Get companyTradeMark
     *
     * @return string 
     */
    public function getCompanyTradeMark()
    {
        return $this->companyTradeMark;
    }

    /**
     * Get total count showed contacts
     */
    public function totalShowedContacts()
    {
        return $this->usersWhoShowedContacts->count();
    }

    /**
     * Increment view job
     */
    public function incView()
    {
        $this->viewsCount++;

        return $this;
    }

    /**
     * Increment view contact job
     */
    public function incViewContactCount()
    {
        $this->viewsContactCount++;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalViewContactCount()
    {
        return $this->usersWhoShowedContacts->count() + $this->viewsContactCount;
    }

    public function __toString()
    {
        return $this->getCompanyTitle();
    }
}
