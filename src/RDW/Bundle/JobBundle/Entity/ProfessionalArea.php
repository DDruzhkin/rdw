<?php

namespace RDW\Bundle\JobBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\AppBundle\Entity\Transliterator;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\ManageBundle\Entity\Filter;
use RDW\Bundle\SubscriptionBundle\Entity\Subscription;
use RDW\Bundle\SlugBundle\Annotations as Slugifier;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProfessionalArea
 *
 * @package RDW\Bundle\JobBundle\Entity
 *
 * @ORM\Entity(repositoryClass="RDW\Bundle\JobBundle\Repository\ProfessionalAreaRepository")
 * @ORM\Table(name="professional_area")
 * @Slugifier\Slug(fields={"slug"})
 */
class ProfessionalArea extends Filter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var ArrayCollection|Position[]
     *
     * @ORM\ManyToMany(
     *     targetEntity="RDW\Bundle\JobBundle\Entity\Position",
     *     mappedBy="professionalArea",
     *     fetch="EXTRA_LAZY"
     * )
     */
    protected $positions;

    /**
     * @var ArrayCollection|$this[]
     *
     * @ORM\OneToMany(
     *     targetEntity="RDW\Bundle\JobBundle\Entity\ProfessionalArea",
     *     mappedBy="parent",
     *     fetch="EXTRA_LAZY",
     *     cascade={"persist"}
     * )
     */
    protected $children;

    /**
     * @var ProfessionalArea
     *
     * @ORM\ManyToOne(
     *     targetEntity="RDW\Bundle\JobBundle\Entity\ProfessionalArea",
     *     inversedBy="children",
     *     fetch="EXTRA_LAZY"
     * )
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    protected $parent;

    /**
     * @var ArrayCollection|Job[]
     *
     * @ORM\ManyToMany(targetEntity="RDW\Bundle\JobBundle\Entity\Job", mappedBy="professionalArea")
     */
    protected $jobs;

    /**
     * @var ArrayCollection|Cv[]
     *
     * @ORM\ManyToMany(targetEntity="RDW\Bundle\CvBundle\Entity\Cv", mappedBy="professionalArea")
     */
    protected $cvs;

    /**
     * @var int
     */
    protected $entitiesCount;

    /**
     * @ORM\Column(name="title_in_latin", type="string", length=100, nullable=false)
     */
    protected $titleInLatin;

    /**
     * @ORM\Column(name="title_where", type="string", length=100)
     * @Assert\NotNull()
     */
    protected $titleWhere;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(
     *     targetEntity="RDW\Bundle\SubscriptionBundle\Entity\Subscription",
     *     mappedBy="professionalAreas",
     *     fetch="EXTRA_LAZY"
     * )
     */
    protected $subscriptions;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->positions = new ArrayCollection();
        $this->children = new ArrayCollection();
        $this->subscriptions = new ArrayCollection();
        $this->jobs = new ArrayCollection();
        $this->cvs = new ArrayCollection();
    }

    public function __toString()
    {
        if ($this->isParent()) {
            return $this->titleWhere;
        } else {
            return $this->parent->titleWhere . " - " . $this->titleWhere;
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function isDeletable()
    {
        return !($this->positions->count() > 0 || $this->isSystem());
    }

    public function setEntitiesCount($entitiesCount)
    {
        $this->entitiesCount = $entitiesCount;

        return $this;
    }

    public function getEntitiesCount()
    {
        return $this->entitiesCount;
    }

    /**
     * Add positions
     *
     * @param \RDW\Bundle\JobBundle\Entity\Position $positions
     *
     * @return ProfessionalArea
     */
    public function addPosition(Position $positions)
    {
        if (!$this->positions->contains($positions)) {
            $this->positions->add($positions);
        }

        return $this;
    }

    /**
     * Remove positions
     *
     * @param \RDW\Bundle\JobBundle\Entity\Position $positions
     */
    public function removePosition(Position $positions)
    {
        $this->positions->removeElement($positions);
    }

    /**
     * Get positions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPositions()
    {
        return $this->positions;
    }

    /**
     * generates title in latin
     *
     * @return string
     */
    public function generateTitleInLatin()
    {
        return Transliterator::transliterate($this->getTitle());
    }

    /**
     * Set titleInLatin
     *
     * @param string $titleInLatin
     *
     * @return $this
     */
    public function setTitleInLatin($titleInLatin)
    {
        $this->titleInLatin = mb_strcut($titleInLatin, 0, 100);

        return $this;
    }

    /**
     * Get titleInLatin
     *
     * @return string
     */
    public function getTitleInLatin()
    {
        return $this->titleInLatin;
    }

    /**
     * @return mixed
     */
    public function getTitleWhere()
    {
        return $this->titleWhere;
    }

    /**
     * @param mixed $titleWhere
     *
     * @return $this
     */
    public function setTitleWhere($titleWhere)
    {
        $this->titleWhere = mb_strcut($titleWhere, 0, 100);

        return $this;
    }

    /**
     * @return bool
     */
    public function isParent()
    {
        if ($this->parent) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @return ProfessionalArea
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param ProfessionalArea $parent
     *
     * @return $this
     */
    public function setParent(ProfessionalArea $parent = null)
    {
        /** Avoid Recursion */
        if ($parent === $this) {
            return $this;
        }

        $this->parent = $parent;

        return $this;
    }

    /**
     * @return $this[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param ArrayCollection $children
     *
     * @return $this
     */
    public function setChildren(ArrayCollection $children = null)
    {
        if (!$children) {
            $this->children->clear();
        } else {
            $this->children = $children;
        }

        return $this;
    }

    /**
     * @param ProfessionalArea $child
     *
     * @return $this
     */
    public function addChild(ProfessionalArea $child)
    {
        if (!$this->children->contains($child)) {
            $this->children->add($child);
        }

        return $this;
    }

    /**
     * @param ProfessionalArea $child
     *
     * @return $this
     */
    public function removeChild(ProfessionalArea $child)
    {
        if ($this->children->contains($child)) {
            $this->children->removeElement($child);
        }

        return $this;
    }

    /**
     * @return ArrayCollection|Job[]
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * @param ArrayCollection|Job[] $jobs
     *
     * @return $this
     */
    public function setJobs($jobs)
    {
        $this->jobs = $jobs;

        return $this;
    }

    /**
     * @return ArrayCollection|Cv[]
     */
    public function getCvs()
    {
        return $this->cvs;
    }

    /**
     * @param ArrayCollection|Cv[] $cvs
     *
     * @return $this
     */
    public function setCvs($cvs)
    {
        $this->cvs = $cvs;

        return $this;
    }

    /**
     * Add subscriptions
     *
     * @param Subscription $subscriptions
     * @return ProfessionalArea
     */
    public function addSubscription(Subscription $subscriptions)
    {
        $this->subscriptions[] = $subscriptions;

        return $this;
    }

    /**
     * Remove subscriptions
     *
     * @param Subscription $subscriptions
     */
    public function removeSubscription(Subscription $subscriptions)
    {
        $this->subscriptions->removeElement($subscriptions);
    }

    /**
     * Get subscriptions
     *
     * @return ArrayCollection
     */
    public function getSubscriptions()
    {
        return $this->subscriptions;
    }

    /**
     * Get Professional Area type slug.
     *
     * @return string
     */
    public function getSlug() {
        if ($this->isParent()) {
            return $this->getTitle();
        }
        else {
            return $this->getParent()->getTitle() . '-' . $this->getTitle();
        }
    }
}
