<?php

namespace RDW\Bundle\JobBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ContactPersonPhone
 *
 * @package RDW\Bundle\JobBundle\Entity
 *
 * @ORM\Entity
 */
class ContactPersonPhone extends \RDW\Bundle\AppBundle\Entity\ContactPersonPhone
{
    /**
     * @var Job
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\JobBundle\Entity\Job", inversedBy="contactPersonPhones")
     * @ORM\JoinColumn(name="discriminator_id", referencedColumnName="id")
     */
    protected $entity;
}
