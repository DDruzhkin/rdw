<?php

namespace RDW\Bundle\JobBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\ManageBundle\Entity\Filter;

/**
 * Class WorkScheduleType
 *
 * @package RDW\Bundle\JobBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="work_schedules")
 */
class WorkScheduleType extends Filter
{
    const TYPE_ALTERNATIVE = 'alternative';
    const TYPE_REMOTE = 'remote';
    const TYPE_FLEXIBLE = 'flexible';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\JobBundle\Entity\Job", mappedBy="workScheduleType", fetch="EXTRA_LAZY")
     */
    private $jobs;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\CvBundle\Entity\Cv", mappedBy="workScheduleType", fetch="EXTRA_LAZY")
     */
    protected $cvs;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="RDW\Bundle\SubscriptionBundle\Entity\Subscription", mappedBy="workSchedules", fetch="EXTRA_LAZY")
     */
    protected $subscriptions;

    /**
     * constructor
     */
    public function __construct()
    {
        $this->jobs = new ArrayCollection();
        $this->cvs = new ArrayCollection();
        $this->subscriptions = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function isDeletable()
    {
        return ! ($this->subscriptions->count() > 0 || $this->jobs->count() > 0 || $this->cvs->count() > 0 || $this->isSystem());
    }
}
