<?php

namespace RDW\Bundle\JobBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\ManageBundle\Entity\Filter;

/**
 * Class Language
 *
 * @package RDW\Bundle\JobBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="languages")
 */
class Language extends Filter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\JobBundle\Entity\JobLanguage", mappedBy="language", fetch="EXTRA_LAZY")
     */
    protected $jobLanguages;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\CvBundle\Entity\CvLanguage", mappedBy="language", fetch="EXTRA_LAZY")
     */
    protected $cvLanguages;

    /**
     * constructor
     */
    public function __construct()
    {
        $this->jobLanguages = new ArrayCollection();
        $this->cvLanguages  = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function isDeletable()
    {
        return ! ($this->jobLanguages->count() > 0 || $this->cvLanguages->count() > 0 || $this->isSystem());
    }
}
