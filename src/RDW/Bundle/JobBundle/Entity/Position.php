<?php

namespace RDW\Bundle\JobBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use RDW\Bundle\ManageBundle\Entity\Filter;
use RDW\Bundle\SlugBundle\Annotations as Slugifier;

/**
 * Class Position
 *
 * @package RDW\Bundle\JobBundle\Entity
 *
 * @ORM\Entity(repositoryClass="RDW\Bundle\JobBundle\Repository\PositionRepository")
 * @ORM\Table(name="job_positions", indexes={@Index(name="status", columns={"status"})})
 * @Slugifier\Slug(fields={"title"})
 */
class Position extends Filter
{
    const STATUS_NEW  = 'new';
    const STATUS_APPROVED  = 'approved';
    const STATUS_DECLINED  = 'declined';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\JobBundle\Entity\Job", mappedBy="position", fetch="EXTRA_LAZY")
     */
    private $jobs;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\CvBundle\Entity\Cv", mappedBy="position", fetch="EXTRA_LAZY")
     */
    private $cvs;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="RDW\Bundle\SubscriptionBundle\Entity\Subscription", mappedBy="positions",
     *     fetch="EXTRA_LAZY")
     */
    protected $subscriptions;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="RDW\Bundle\JobBundle\Entity\ProfessionalArea", cascade={"persist"},
     *     inversedBy="positions", fetch="EXTRA_LAZY")
     */
    protected $professionalArea;

    /**
     * @var int
     */
    protected $entitiesCount;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", nullable=true,  options={"default" : "new"})
     */
    protected $status = 'new';

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->jobs = new ArrayCollection();
        $this->cvs = new ArrayCollection();
        $this->subscriptions = new ArrayCollection();
        $this->professionalArea = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function isDeletable()
    {
        return !($this->jobs->count() > 0 || $this->cvs->count() > 0 || $this->subscriptions->count() > 0 || $this->isSystem());
    }

    public function setEntitiesCount($entitiesCount)
    {
        $this->entitiesCount = $entitiesCount;

        return $this;
    }

    public function getEntitiesCount()
    {
        return $this->entitiesCount;
    }

    /**
     * @return ArrayCollection
     */
    public function getProfessionalArea()
    {
        return $this->professionalArea;
    }

    /**
     * @param ArrayCollection $professionalArea
     *
     * @return $this
     */
    public function setProfessionalArea(ArrayCollection $professionalArea = null)
    {
        if (!$professionalArea) {
            $this->professionalArea->clear();
        } else {
            $this->professionalArea = $professionalArea;
        }

        return $this;
    }

    /**
     * @param ProfessionalArea $professionalArea
     *
     * @return $this
     *
     */
    public function addProfessionalArea(ProfessionalArea $professionalArea)
    {
        if (!$this->professionalArea->contains($professionalArea)) {
            $this->professionalArea->add($professionalArea);
        }

        return $this;
    }

    /**
     * @param ProfessionalArea $professionalArea
     *
     * @return $this
     */
    public function removeProfessionalArea(ProfessionalArea $professionalArea)
    {
        if ($this->professionalArea->contains($professionalArea)) {
            $this->professionalArea->removeElement($professionalArea);
        }

        return $this;
    }

    /**
     * @param ProfessionalArea $professionalArea
     *
     * @return bool
     */
    public function hasProfessionalArea(ProfessionalArea $professionalArea)
    {
        return $this->professionalArea->contains($professionalArea);
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }
}
