<?php

namespace RDW\Bundle\JobBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityNotFoundException;

/**
 * Class Abuse
 *
 * @package RDW\Bundle\JobBundle\Entity
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 *
 * @ORM\Entity(repositoryClass="RDW\Bundle\JobBundle\Repository\ApplyRepository")
 * @ORM\Table(name="job_applies")
 */
class Apply
{
    const CV_UPLOAD_TYPE_MY = 'my';
    const CV_UPLOAD_TYPE_UPLOAD = 'upload';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="text")
     * @Assert\NotBlank(groups={"apply"})
     */
    protected $position;

    /*
     * @var string
     *
     * @ORM\Column(name="company_title", type="text")
     * @Assert\NotBlank(groups={"apply", "multi_apply"})
     */
//    protected $companyTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text")
     * @Assert\NotBlank(groups={"apply", "multi_apply"})
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="text", nullable=true)
     * @Assert\Regex(pattern="/^([\+0-9\s\-]+)$/", message="Field must be a valid phone number and can have only numbers, +, - and spaces", groups={"apply", "multi_apply"})
     */
    protected $phone;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Insert message content", groups={"apply", "multi_apply"})
     */
    protected $message;

    /**
     * @var Cv
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\CvBundle\Entity\Cv")
     * @ORM\JoinColumn(name="cv_id", referencedColumnName="id", nullable=true)
     *
     * @Assert\NotBlank(groups={"apply_my"})
     */
    protected $cv;

    /**
     * @var Job
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\JobBundle\Entity\Job", inversedBy="applies")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id")
     */
    protected $job;

    /**
     * @var ArrayCollection
     */
    protected $jobs;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\User", inversedBy="jobApplies")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="cv_upload_type")
     */
    protected $cvUploadType;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="document")
     */
    protected $document;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true, name="employer_comment")
     */
    private $employerComment;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\ConversationBundle\Entity\Conversation", mappedBy="apply")
     */
    protected $conversations;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param string $position
     *
     * @return $this
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return \RDW\Bundle\CvBundle\Entity\Cv
     */
    public function getCv()
    {
        return $this->cv;
    }

    /**
     * @return bool
     */
    public function hasCv()
    {
        try {
            $cv = $this->cv;
            if (isset($cv) && ! $cv->isDeleted()) {
                return true;
            }

            return false;
        } catch (EntityNotFoundException $e) {
            return false;
        }
    }

    /**
     * @param \RDW\Bundle\CvBundle\Entity\Cv $cv
     *
     * @return $this
     */
    public function setCv($cv = null)
    {
        $this->cv = $cv;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /*
     * @return string
     */
    /*public function getCompanyTitle()
    {
        return $this->companyTitle;
    }*/

    /*
     * @param string $companyTitle
     *
     * @return $this
     */
    /*public function setCompanyTitle($companyTitle)
    {
        $this->companyTitle = $companyTitle;

        return $this;
    }*/

    /**
     * @return array
     */
    public static function getCvUploadTypeChoices()
    {
        return [self::CV_UPLOAD_TYPE_MY => 'From my cv list', self::CV_UPLOAD_TYPE_UPLOAD => 'Upload file'];
    }

    /**
     * @return string
     */
    public function getCvUploadType()
    {
        return $this->cvUploadType;
    }

    /**
     * @param string $cvUploadType
     *
     * @return $this
     */
    public function setCvUploadType($cvUploadType)
    {
        $this->cvUploadType = $cvUploadType;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmployerComment()
    {
        return $this->employerComment;
    }

    /**
     * @param string $employerComment
     *
     * @return $this
     */
    public function setEmployerComment($employerComment)
    {
        $this->employerComment = $employerComment;

        return $this;
    }

    /**
     * @return \RDW\Bundle\JobBundle\Entity\Job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * @param \RDW\Bundle\JobBundle\Entity\Job $job
     *
     * @return $this
     */
    public function setJob($job)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * @return \RDW\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param \RDW\Bundle\UserBundle\Entity\User $user
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * @param string $document
     *
     * @return $this
     */
    public function setDocument($document)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * @return bool
     */
    public function isTypeDocument()
    {
        return ($this->getCvUploadType() == self::CV_UPLOAD_TYPE_UPLOAD) ? true : false;
    }

    /**
     * @return ArrayCollection
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * @param ArrayCollection $jobs
     */
    public function setJobs($jobs)
    {
        $this->jobs = $jobs;
    }

    /**
     * @return ArrayCollection
     */
    public function getConversations()
    {
        return $this->conversations;
    }
}
