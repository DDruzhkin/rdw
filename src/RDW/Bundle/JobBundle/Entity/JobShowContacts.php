<?php

namespace RDW\Bundle\JobBundle\Entity;

use RDW\Bundle\UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class Job Show Contacts
 *
 * @package RDW\Bundle\JobBundle\Entity
 *
 * @ORM\Table(name="job_show_contacts")
 * @ORM\Entity(repositoryClass="RDW\Bundle\JobBundle\Repository\JobShowContactsRepository")
 */
class JobShowContacts extends View
{
    /**
     * @var Job
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\JobBundle\Entity\Job", inversedBy="usersWhoShowedContacts")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id")
     */
    protected $job;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\RegisteredUser", inversedBy="jobsShowedContacts")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * Constructor
     *
     * @param Job  $job
     * @param User $user
     */
    public function __construct(Job $job, User $user)
    {
        $this->job = $job;
        $this->user = $user;
    }

    /**
     * @return Job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * @param Job $job
     *
     * @return $this
     */
    public function setJob($job)
    {
        $this->job = $job;

        return $this;
    }


    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }
}
