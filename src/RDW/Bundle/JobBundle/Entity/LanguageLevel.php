<?php

namespace RDW\Bundle\JobBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\ManageBundle\Entity\Filter;

/**
 * Class LanguageLevel
 *
 * @package RDW\Bundle\JobBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="language_levels")
 */
class LanguageLevel extends Filter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\JobBundle\Entity\JobLanguage", mappedBy="level", fetch="EXTRA_LAZY")
     */
    protected $jobLevels;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\CvBundle\Entity\CvLanguage", mappedBy="level", fetch="EXTRA_LAZY")
     */
    protected $cvLevels;

    /**
     * constructor
     */
    public function __construct()
    {
        $this->jobLevels = new ArrayCollection();
        $this->cvLevels = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function isDeletable()
    {
        return ! ($this->cvLevels->count() > 0 || $this->jobLevels->count() > 0 || $this->isSystem());
    }
}
