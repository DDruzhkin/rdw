<?php

namespace RDW\Bundle\JobBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\ManageBundle\Entity\Filter;

/**
 * Class EducationType
 *
 * @package RDW\Bundle\JobBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="education_types")
 */
class EducationType extends Filter
{
    const TYPE_WITHOUT_EDUCATION = 'without_education';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\JobBundle\Entity\Job", mappedBy="requiredEducationType", fetch="EXTRA_LAZY")
     */
    protected $jobs;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\CvBundle\Entity\Education", mappedBy="educationType", fetch="EXTRA_LAZY")
     */
    protected $educations;

    /**
     * constructor
     */
    public function __construct()
    {
        $this->jobs = new ArrayCollection();
        $this->educations = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function isDeletable()
    {
        return ! ($this->jobs->count() > 0 || $this->educations->count() > 0 || $this->isSystem());
    }

    /**
     * checks if entity is type without education
     *
     * @return boolean
     */
    public function isWithoutEducation()
    {
        return self::TYPE_WITHOUT_EDUCATION === $this->getType();
    }
}
