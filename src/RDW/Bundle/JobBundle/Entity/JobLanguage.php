<?php

namespace RDW\Bundle\JobBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class JobLanguage
 *
 * @package RDW\Bundle\JobBundle\Entity
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 *
 * @ORM\Table(name="job_languages")
 * @ORM\Entity()
 */
class JobLanguage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Job
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\JobBundle\Entity\Job", inversedBy="languages")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id")
     */
    private $job;

    /**
     * @var Language
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\JobBundle\Entity\Language", inversedBy="jobLanguages")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     */
    private $language;

    /**
     * @var LanguageLevel
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\JobBundle\Entity\LanguageLevel", inversedBy="jobLevels")
     * @ORM\JoinColumn(name="level_id", referencedColumnName="id")
     */
    private $level;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Job $job
     *
     * @return $this
     */
    public function setJob(Job $job)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * @return Job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * @param Language $language
     *
     * @return $this
     */
    public function setLanguage(Language $language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @return Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param LanguageLevel $level
     *
     * @return $this
     */
    public function setLevel(LanguageLevel $level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * @return LanguageLevel
     */
    public function getLevel()
    {
        return $this->level;
    }
}
