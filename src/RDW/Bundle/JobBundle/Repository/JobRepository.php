<?php

namespace RDW\Bundle\JobBundle\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use RDW\Bundle\AppBundle\Repository\AdRepository;
use Doctrine\ORM\QueryBuilder;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Entity\ProfessionalArea;
use RDW\Bundle\JobBundle\Model\JobFilter;
use RDW\Bundle\OrderBundle\RDWOrderStatus;
use RDW\Bundle\ServiceBundle\Entity\Service;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\User;

class JobRepository extends AdRepository
{
    /**
     * @param JobFilter $filter
     *
     * @return \Doctrine\ORM\Query
     */
    public function getListQuery(JobFilter $filter = null)
    {
        $queryBuilder = $this->getBaseQuery();

        if ($filter) {
            $queryBuilder
                ->orderBy('job.top', 'DESC')
                ->addOrderBy('job.highlighted', 'DESC')
                ->addOrderBy('job.updatedAt', 'DESC');

            $queryBuilder = $this->applyFilters($filter, $queryBuilder);
        } else {
            $queryBuilder
                ->orderBy('job.updatedAt DESC');
        }

        return $queryBuilder->getQuery();
    }

    /**
     * @param RegisteredUser $user
     * @param int $limit
     *
     * @return array
     */
    public function getHavingViews(RegisteredUser $user, $limit)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('j', 'user', 'COUNT(v) as viewers')
            ->from('RDWJobBundle:Job', 'j')
            ->join('j.user', 'user')
            ->leftJoin('j.usersWhoViewed', 'v')
            ->join('v.user', 'userWhoViewed')
            ->andWhere('j.user = :user')
            ->andWhere('v.user <> :viewUser')
            ->having('viewers > 0')
            ->setParameter('user', $user)
            ->setParameter('viewUser', $user)
            ->groupBy('j.id')
            ->orderBy('j.updatedAt', 'DESC')
            ->setMaxResults($limit);

        $results = [];

        foreach ($queryBuilder->getQuery()->getResult() as $item) {
            $job = $item[0];
            $views = $this->getViewers($job);
            $job->setUsersWhoViewed($views);

            $results[] = $job;
        }

        return $results;
    }

    /**
     * @return mixed
     */
    public function countActiveJobs()
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('COUNT(j.id)')
            ->from('RDWJobBundle:Job', 'j')
            ->join('j.user', 'user')
            ->andWhere('j.status = :status')
            ->setParameter('status', Job::STATUS_ACTIVE);

        $total = $queryBuilder->getQuery()->getSingleScalarResult();

        return $total;
    }

    /**
     * @param RegisteredUser $manager
     * @param RegisteredUser $client
     *
     * @return QueryBuilder|null
     */
    public function getListForManagerQueryBuilder(RegisteredUser $manager, RegisteredUser $client = null)
    {
        $assignedUsers = $manager->getAssignedUsers();

        if (!$assignedUsers->count() && !$client) {
            return null;
        }

        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('job')
            ->from('RDWJobBundle:Job', 'job')
            ->join('job.user', 'user')
            ->orderBy('job.updatedAt', 'DESC');

        if ($client) {
            $queryBuilder->andWhere('user = :client')->setParameter('client', $client);
        } elseif ($assignedUsers) {
            $queryBuilder->andWhere('user IN (:assignedUsers)')->setParameter('assignedUsers', $assignedUsers->getValues());
        }

        return $queryBuilder;
    }

    /**
     * @param User $user
     * @param array $criteria
     *
     * @return \Doctrine\ORM\Query
     */
    public function getUserJobsListQuery(User $user, array $criteria = [])
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('j', 'u', 's')
            ->from('RDWJobBundle:Job', 'j')
            ->join('j.user', 'u')
            ->leftJoin('j.services', 's', 'WITH', 's.validTill >= :validTill')
            ->andWhere('j.user = :user')
            ->orderBy('j.updatedAt', 'DESC')
            ->setParameter('validTill', new \DateTime())
            ->setParameter('user', $user);

        if (!empty($criteria['status'])) {
            $queryBuilder
                ->andWhere('j.status = :status')
                ->setParameter('status', $criteria['status']);
        }

        if (!empty($criteria['statuses']) && is_array($criteria['statuses'])) {
            $queryBuilder
                ->andWhere('j.status IN (:statuses)')
                ->setParameter('statuses', $criteria['statuses']);
        }

        if (!empty($criteria['only_items']) && $criteria['only_items'] instanceof ArrayCollection) {
            /** @var ArrayCollection $items */
            $items = $criteria['only_items'];
            $queryBuilder
                ->andWhere('j IN (:jobs)')
                ->setParameter('jobs', $items->getValues());
        }

        if (!empty($criteria['hide_secret'])) {
            $queryBuilder
                ->andWhere('j.secret = false');
        }

        $queryBuilder->groupBy('j.id');

        return $queryBuilder->getQuery();
    }

    /**
     * @param $serviceType
     *
     * @return array
     */
    public function findAllExpiredWithAttributesByServiceType($serviceType)
    {
        return $this->findAllExpiredWithAttributesByServiceTypeAndRepository('RDWJobBundle:Job', $serviceType);
    }

    /**
     * @param JobFilter $filter
     *
     * @return mixed
     */
    public function findAllForCensor(JobFilter $filter)
    {
        $queryBuilder = $this->getBaseQuery();

        if ($statuses = $filter->getStatuses()) {
            $queryBuilder
                ->andWhere('job.status IN (:statuses)')
                ->setParameter('statuses', $statuses);
        }

        if ($filter->getOnlyPaid()) {
            // show only jobs which owner has active job publishing service
            $queryBuilder
                ->innerJoin('job.services', 'oi')
                ->innerJoin('oi.service', 'service')
                ->andWhere('oi.status = :orderItemStatus')
                ->andWhere('service.type = :serviceType')
                ->andWhere('oi.validTill >= :validTill')
                ->setParameter('validTill', new \DateTime())
                ->setParameter('orderItemStatus', RDWOrderStatus::STATUS_PAID)
                ->setParameter('serviceType', Service::TYPE_JOB_PUBLISHING);
        }

        if ($id = $filter->getId()) {
            $queryBuilder
                ->andWhere('job.id = :id')
                ->setParameter('id', $id);
        }

        if ($foreignId = $filter->getForeignId()) {
            $queryBuilder
                ->andWhere('job.foreignId = :foreignId')
                ->setParameter('foreignId', $foreignId);
        }

        if ($crmOrderId = $filter->getCrmOrderId()) {
            $queryBuilder
                ->andWhere('job.crmOrderId = :crmOrderId')
                ->setParameter('crmOrderId', $crmOrderId);
        }

        if ($email = $filter->getContactEmail()) {
            $queryBuilder
                ->andWhere('job.contactPersonEmail LIKE :email')
                ->setParameter('email', '%' . $email . '%');
        }

        if ($phone = $filter->getContactPhone()) {
            $queryBuilder
                ->join('job.contactPersonPhones', 'cpp')
                ->andWhere('cpp.number LIKE :number')
                ->setParameter('number', '%' . $phone . '%');
        }

        if ($companyTitle = $filter->getCompanyTitle()) {
            $queryBuilder
                ->andWhere('job.companyTitle LIKE :companyTitle')
                ->setParameter('companyTitle', '%' . $companyTitle . '%');
        }

        if ($position = $filter->getPosition()) {
            $queryBuilder
                ->andWhere('position = :position')
                ->setParameter('position', $position);
        }

        $queryBuilder
            ->groupBy('job.id')
            ->orderBy('job.createdAt', 'DESC');

        return $queryBuilder->getQuery();
    }

    /**
     * @param JobFilter $filter
     * @param QueryBuilder $queryBuilder
     *
     * @return QueryBuilder
     */
    protected function applyFilters(JobFilter $filter, QueryBuilder $queryBuilder)
    {
        if ($notIds = $filter->getNotIds()) {
            $queryBuilder
                ->andWhere('job.id NOT IN (:notIds)')
                ->setParameter('notIds', $notIds);
        }

        if ($filter->getWorkingPlaceCity()) {
            $queryBuilder
                ->andWhere('job.workingPlaceCity = :working_city')
                ->setParameter('working_city', $filter->getWorkingPlaceCity());
        }

        if ($filter->getCities() && $filter->getCities()->count() > 0) {
            $queryBuilder
                ->andWhere('job.workingPlaceCity IN (:cities)')
                ->setParameter('cities', $filter->getCities()->getValues());
        }

        if ($filter->getProfessionalArea() && $filter->getProfessionalArea()->count() > 0) {
            $professionalAreas = $filter->getProfessionalArea()->getValues();
            /** @var ProfessionalArea $professionalArea */
            foreach ($professionalAreas as $key => $professionalArea) {
                if ($professionalArea->isParent()) {
                    unset($professionalAreas[$key]);
                }
            }
            $queryBuilder
                ->andWhere('professionalArea IN (:professionalArea)')
                ->setParameter('professionalArea', $professionalAreas);
        }

        if ($filter->getPosition()) {
            $queryBuilder
                ->andWhere('job.position = :position')
                ->setParameter('position', $filter->getPosition());
        }

        if ($filter->getShowValidTillJobs()) {
            $dateToCompare = new \DateTime();
            $queryBuilder
                ->andWhere('job.validTill >= :validTill')
                ->setParameter('validTill', $dateToCompare->format('Y-m-d 00:00:00'));
        }

        if ($statuses = $filter->getStatuses()) {
            $queryBuilder
                ->andWhere('job.status IN (:statuses)')
                ->setParameter('statuses', $statuses);
        }

        return $queryBuilder;
    }

    /**
     * @param RegisteredUser $user
     *
     * @return QueryBuilder
     */
    public function getMyListQueryBuilder(RegisteredUser $user)
    {
        $queryBuilder = $this->getBaseQuery();

        $queryBuilder
            ->where('job.user = :user')
            ->setParameter('user', $user)
            ->orderBy('job.updatedAt', 'DESC');

        return $queryBuilder;
    }

    public function getById($id)
    {
        $queryBuilder = $this->getBaseQuery();

        $queryBuilder
            ->andWhere('job.id = :id')
            ->setParameter('id', $id);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    /**
     * @param array $ids
     *
     * @return array
     */
    public function getByIds(array $ids = [])
    {
        $queryBuilder = $this->getBaseQuery();

        $queryBuilder
            ->andWhere('job.id IN (:ids)')
            ->setParameter('ids', $ids);

        if (!empty($ids)) {
            $queryBuilder
                ->select('job, FIELD(job.id, ' . implode(',', $ids) . ') AS HIDDEN ordering')
                ->orderBy('ordering');
        }

        return $queryBuilder->getQuery()->getResult();
    }

    public function getPopularScopes($limit)
    {
        return $this->getPopular('RDWUserBundle:Scope', $limit);
    }

    public function getPopularPositions($limit)
    {
        return $this->getPopular('RDWJobBundle:Position', $limit);
    }

    public function getPopularCities($limit)
    {
        return $this->getPopular('RDWLocationBundle:City', $limit);
    }

    /**
     * @param RegisteredUser $user
     *
     * @return array|Job[]
     */
    public function getViewedByUser(RegisteredUser $user)
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('job')
            ->from('RDWJobBundle:Job', 'job')
            ->innerJoin('job.usersWhoViewed', 'userWhoViewed')
            ->andWhere('userWhoViewed.user = :user')
            ->setParameter('user', $user)
            ->getQuery()->getResult();
    }

    /**
     * @return QueryBuilder
     */
    private function getBaseQuery()
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('job', 'scope', 'workScheduleType', 'position', 'professionalArea', 'cities', 'user', 'workingPlaceCity')
            ->from('RDWJobBundle:Job', 'job')
            ->leftJoin('job.scope', 'scope')
            ->leftJoin('job.workScheduleType', 'workScheduleType')
            ->leftJoin('job.position', 'position')
            ->leftJoin('job.professionalArea', 'professionalArea')
            ->leftJoin('job.workingPlaceCity', 'workingPlaceCity')
            ->innerJoin('job.user', 'user')
            ->leftJoin('job.cities', 'cities');

        return $queryBuilder;
    }

    private function getViewers(Job $job)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('view', 'user')
            ->from('RDWJobBundle:JobView', 'view')
            ->innerJoin('view.user', 'user')
            ->where('view.job = :job')
            ->setParameter('job', $job);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $repositoryName
     * @param $limit
     *
     * @return array
     */
    private function getPopular($repositoryName, $limit)
    {
        $joinItems = 'i.jobs';
        $status = Job::STATUS_ACTIVE;

        return $this->getPopularByParams($repositoryName, $limit, $joinItems, $status);
    }

    /**
     *
     * @return array - count jobs in this professional area
     *
     */
    public function getCountJobsInProfessionalArea()
    {
        $queryBuilder = $this->getBaseQuery();
        $queryBuilder
            ->addSelect('COUNT(job) AS countJobs')
            ->addSelect('pa.id AS professionalAreaId')
            ->join('job.professionalArea', 'pa')
            ->groupBy('pa.id')
            ->orderBy('countJobs', 'DESC');

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param ProfessionalArea $professionalArea
     * @param int $limit
     *
     * @return array|Job[]
     */
    public function getByProfessionalArea(ProfessionalArea $professionalArea, $limit = 10)
    {
        $qb = $this->createQueryBuilder('j');
        $qb
            ->join('j.professionalArea', 'pa')
            ->setMaxResults($limit)
            ->andWhere($qb->expr()->eq('pa.id', $professionalArea->getId()));

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $yesterday
     * @param $today
     * @param int $limit
     *
     * @return array|Job[]
     */
    public function getJobsToday($today, $limit)
    {
        $queryBuilder = $this->getBaseQuery();

        $queryBuilder
            ->where('job.status = :status')
            ->andWhere('job.validTill >= :today')
            ->setParameter('status', Job::STATUS_ACTIVE)
            ->setParameter('today', $today)
            ->setMaxResults($limit)
            ->orderBy('job.updatedAt', 'DESC')
            ->groupBy('job.id');

        return $queryBuilder->getQuery()->getResult();
    }
}
