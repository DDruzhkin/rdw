<?php

namespace RDW\Bundle\JobBundle\Repository;

use RDW\Bundle\AppBundle\Repository\AdRepository;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Entity\ProfessionalArea;

class ProfessionalAreaRepository extends AdRepository
{
    /**
     * @param int $count
     * @param int $limit
     *
     * @return array|ProfessionalArea[]
     */
    public function getPopular($count = 1, $limit = 1)
    {
        $professionalAreas = [];
        $qb = $this->createQueryBuilder('pa');
        $qb->join('pa.jobs', 'j')
            ->addSelect('pa', 'COUNT(j.id) AS totalCount')
            ->where('pa.parent IS NULL')
            ->andWhere('j.status = :status')
            ->setParameter('status', Job::STATUS_ACTIVE)
            ->having(sprintf('count(j.id) >= %d', $count))
            ->groupBy('pa.id')
            ->orderBy('totalCount', 'DESC')
            ->setMaxResults($limit);

        $result = $qb->getQuery()->getResult();
        foreach ($result as $item) {
            /** @var ProfessionalArea $professionalArea */
            $professionalArea = $item[0];
            $professionalArea->setEntitiesCount($item['totalCount']);
            $professionalAreas[] = $professionalArea;
        }

        return $professionalAreas;
    }

    /**
     * @param $query
     *
     * @return array|ProfessionalArea[]
     */
    public function getPaByQuery($query)
    {
        $qb = $this->createQueryBuilder('pa');
        $qb->where('pa.parent IS NULL');
        if ($query) {
            $qb->andWhere('pa.title LIKE :query')
                ->setParameter('query', '%' . $query . '%');
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $id
     *
     * @return array|ProfessionalArea[]
     */
    public function getPaByids($ids)
    {
        $professionalAreas = [];
	foreach (explode (",", $ids) as $id) {

	    $qb = $this->createQueryBuilder('pa');
	    $qb->Where('pa.id = :id')
	            ->setParameter('id', $id);
            $professionalAreas[] = $qb->getQuery()->getResult()[0];
	}

        return $professionalAreas;
    }

    /**
     * @param $positionId
     *
     * @return array|ProfessionalArea[]
     */
    public function getPaByPosition($positionId)
    {
        $qb = $this->createQueryBuilder('pa');
        $qb->join('pa.positions', 'p')
            ->andWhere(
                $qb->expr()->eq('p.id', $positionId)
            );

        return $qb->getQuery()->getResult();
    }

    /**
     * @param int $count
     * @param int $limit
     * @param int $id
     *
     * @return array|ProfessionalArea[]
     */
    public function getPopularSpecializationInPa($id, $count = 1, $limit = 1)
    {
        $qb = $this->createQueryBuilder('pa');
        $qb->select('pa AS entity')
            ->join('pa.jobs', 'j')
            ->where('pa.parent = :id')
            ->andWhere('j.status = :status')
            ->setParameter('status', Job::STATUS_ACTIVE)
            ->having(sprintf('count(j.id) >= %d', $count))
            ->setParameter('id', $id)
            ->addSelect('COUNT(j.id) AS jobsCount')
            ->orderBy('jobsCount', 'DESC')
            ->groupBy('pa.id')
            ->setMaxResults($limit);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param string $title
     *
     * @return null|object|ProfessionalArea
     */
    public function findProfessionalAreaByTitle($title)
    {
        $qb = $this->createQueryBuilder('pa');
        $qb->andWhere('pa.title = :title')
            ->setParameter('title', $title, \PDO::PARAM_STR)
            ->andWhere($qb->expr()->isNull('pa.parent'));

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param string $titleInLatin
     *
     * @return null|object|ProfessionalArea
     */
    public function findProfessionalAreaByTitleInLatin($titleInLatin)
    {
        $qb = $this->createQueryBuilder('pa');
        $qb->andWhere('pa.titleInLatin = :titleInLatin')
            ->setParameter('titleInLatin', $titleInLatin, \PDO::PARAM_STR)
            ->andWhere($qb->expr()->isNull('pa.parent'));

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param string $title
     * @param ProfessionalArea $pa
     *
     * @return null|object|ProfessionalArea
     */
    public function findSpecializationByTitle($title, ProfessionalArea $pa)
    {
        return $this->findOneBy(['title' => $title, 'parent' => $pa]);
    }

    /**
     * @param string $title
     * @param ProfessionalArea $pa
     *
     * @return null|object|ProfessionalArea
     */
    public function findSpecializationByTitleInLatin($title, ProfessionalArea $pa)
    {
        return $this->findOneBy(['titleInLatin' => $title, 'parent' => $pa]);
    }
}
