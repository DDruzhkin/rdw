<?php

namespace RDW\Bundle\JobBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class AbuseRepository
 *
 * @package RDW\Bundle\JobBundle\Repository
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class AbuseRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\Query
     */
    public function getListQuery()
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('a', 'job')
            ->from('RDWAppBundle:JobAbuse', 'a')
            ->join('a.ad', 'job')
            ->orderBy('a.createdAt', 'DESC');

        return $queryBuilder->getQuery();
    }
}
