<?php

namespace RDW\Bundle\JobBundle\Repository;

use Doctrine\ORM\EntityRepository;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Model\ApplyFilter;
use RDW\Bundle\UserBundle\Entity\User;

/**
 * Class ViewRepository
 *
 * @package RDW\Bundle\JobBundle\Repository
 * @author  Paulius Aleliūnas <paulius@eface.lt>
 */
class JobViewRepository extends EntityRepository
{
    /**
     * @param ApplyFilter $filters
     *
     * @return \Doctrine\ORM\Query
     */
    public function getListQuery(ApplyFilter $filters)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('v')
            ->from('RDWJobBundle:JobView', 'v')
            ->join('v.job', 'j')
            ->join('v.user', 'user');

        if ($filters->getJob() instanceof Job) {
            $queryBuilder
                ->andWhere('v.job = :job')
                ->setParameter('job', $filters->getJob());
        }

        if ($filters->getUser() instanceof User) {
            $queryBuilder
                ->andWhere('j.user = :jobUser')
                ->andWhere('v.user <> :user')
                ->setParameter('jobUser', $filters->getUser())
                ->setParameter('user', $filters->getUser());
        }

        return $queryBuilder->getQuery();
    }
}
