<?php

namespace RDW\Bundle\JobBundle\Repository;

use Doctrine\ORM\EntityRepository;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Model\ApplyFilter;
use RDW\Bundle\UserBundle\Entity\User;

/**
 * Class ApplyRepository
 *
 * @package RDW\Bundle\JobBundle\Repository
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class ApplyRepository extends EntityRepository
{
    /**
     * @param ApplyFilter $filters
     *
     * @return \Doctrine\ORM\Query
     */
    public function getListQuery(ApplyFilter $filters)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('a')
            ->from('RDWJobBundle:Apply', 'a')
            ->join('a.job', 'j')
            ->join('a.conversations', 'conversations')
            ->leftJoin('conversations.sender', 'sender')
            ->leftJoin('conversations.receiver', 'receiver')

             // only between registered users
            ->andWhere('
                sender INSTANCE OF RDW\Bundle\UserBundle\Entity\RegisteredUser
                AND receiver INSTANCE OF RDW\Bundle\UserBundle\Entity\RegisteredUser
            ');

        if ($filters->getJob() instanceof Job) {
            $queryBuilder
                ->andWhere('a.job = :job')
                ->setParameter('job', $filters->getJob());
        }

        if ($filters->getUser() instanceof User) {
            $queryBuilder
                ->andWhere('j.user = :user')
                ->setParameter('user', $filters->getUser());
        }

        return $queryBuilder->getQuery();
    }
}
