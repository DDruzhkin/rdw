<?php

namespace RDW\Bundle\JobBundle\Repository;

use RDW\Bundle\AppBundle\Repository\AdRepository;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Entity\Position;

class PositionRepository extends AdRepository
{
    /**
     * @return array|Position[]
     */
    public function getApproved()
    {
        $qb = $this->createQueryBuilder('p');
        $qb->where('p.status=:status');
        $qb->setParameter('status', Position::STATUS_APPROVED);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $params
     * @return array|Position[]
     */
    public function getPositionsByParams($params)
    {
        $qb = $this->createQueryBuilder('p');
        if ($params['id']) {
            $qb->andWhere('p.id=:id');
            $qb->setParameter('id', $params['id']);
        }
        if ($params['status']) {
            $qb->andWhere('p.status=:status');
            $qb->setParameter('status', $params['status']);
        }
        else {
            $qb->andWhere('p.status=:status');
            $qb->setParameter('status', Position::STATUS_APPROVED);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $id
     * @return array|Position[]
     */
    public function getPositionsById($id)
    {
        $qb = $this->createQueryBuilder('p');
        $qb->where('p.id=:id');
        $qb->setParameter('id', $id);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param int $count
     * @param int $limit
     * @return array|Position[]
     */
    public function getPopular($count = 1, $limit = 1)
    {
        $results = [];
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select('p', 'COUNT(j.id) AS totalCount');
        $qb->from($this->_entityName, 'p');
        $qb->join('p.jobs', 'j');
        $qb->where('p.status=:status');
        $qb->setParameter('status', Position::STATUS_APPROVED);
        $qb->andWhere('j.status=:job_status');
        $qb->setParameter('job_status', Job::STATUS_ACTIVE);
        $qb->having(sprintf('count(j.id) >= %d', $count));
        $qb->groupBy('p.id');
        $qb->orderBy('totalCount', 'DESC');
        $qb->setMaxResults($limit);

        $op = $qb->getQuery()->getResult();
        foreach ($op as $item) {
            /** @var Position $result */
            $result = $item[0];
            $result->setEntitiesCount($item['totalCount']);
            $results[] = $result;
        }
        return $results;
    }

    /**
     * @param $query
     * @param $limit
     * @return array|Position[]
     */
    public function getPositionByQuery($query, $limit = null)
    {
        $qb = $this->createQueryBuilder('p');
        if ($query) {
            $qb->andWhere('p.title LIKE :query');
            $qb->setParameter('query', '%'.$query.'%');
        }
        if ($limit) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }
}