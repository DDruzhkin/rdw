<?php

namespace RDW\Bundle\JobBundle\Tests\Entity;

use RDW\Bundle\JobBundle\Entity\EmploymentType;

/**
 * Class EmploymentTypeTest
 *
 * @package RDW\Bundle\JobBundle\Tests\Entity
 * @author  Paulius Aleliūnas <paulius@eface.lt>
 */
class EmploymentTypeTest extends \PHPUnit_Framework_TestCase
{
    const CLASS_NAME = 'RDW\Bundle\JobBundle\Entity\EmploymentType';

    /**
     * @test
     */
    public function it_should_return_title()
    {
        $title = 'Title test';

        $employmentType = new EmploymentType();
        $this->assertEmpty($employmentType->getTitle());

        $this->assertInstanceOf(self::CLASS_NAME, $employmentType->setTitle($title));
        $this->assertEquals($title, $employmentType->getTitle());
    }
}
