<?php

namespace RDW\Bundle\JobBundle\Tests\Entity;

use RDW\Bundle\JobBundle\Entity\ContactPersonPhone;
use RDW\Bundle\JobBundle\Entity\Job;

/**
 * Class ContactPersonPhoneTest
 *
 * @package RDW\Bundle\JobBundle\Tests\Entity
 */
class ContactPersonPhoneTest extends \PHPUnit_Framework_TestCase
{
    const CLASS_NAME = 'RDW\Bundle\JobBundle\Entity\ContactPersonPhone';

    /**
     * Test set job
     */
    public function test_set_job()
    {
        $job1 = new Job();
        $phone = new ContactPersonPhone();
        $phone
            ->setEntity($job1)
            ->setNumber('+370 11 000 222');

        $this->assertEquals($phone->getEntity(), $job1);

        $job2 = new Job();

        $this->assertInstanceOf(self::CLASS_NAME, $phone->setEntity($job2));
        $this->assertEquals($job2, $phone->getEntity());
    }
}
