<?php

namespace RDW\Bundle\JobBundle\Tests\Entity;

use RDW\Bundle\JobBundle\Entity\ContactPersonPhone;
use RDW\Bundle\JobBundle\Entity\EducationType;
use RDW\Bundle\JobBundle\Entity\EmploymentType;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Entity\JobLanguage;
use RDW\Bundle\JobBundle\Entity\Position;
use RDW\Bundle\JobBundle\Entity\WorkExperienceType;
use RDW\Bundle\JobBundle\Entity\WorkScheduleType;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\Scope;
use RDW\Bundle\LocationBundle\Entity\City;

/**
 * Class JobTest
 *
 * @package RDW\Bundle\JobBundle\Tests\Entity
 */
class JobTest extends \PHPUnit_Framework_TestCase
{
    const CLASS_NAME = 'RDW\Bundle\JobBundle\Entity\Job';

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $user;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $owner;

    public function setUp()
    {
        $this->user = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');
        $this->owner = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');
    }

    public function jobSettersDataProvider()
    {
        return [
            ['getRequirements', 'setRequirements', 'test text'],
            ['getResponsibilities', 'setResponsibilities', 'test text'],
            ['getWorkingConditions', 'setWorkingConditions', 'test text'],
            ['getGender', 'setGender', 'male'],
            ['getWorkType', 'setWorkType', 'test'],
            ['getAdditionalInformation', 'setAdditionalInformation', 'test'],
            ['getContactPerson', 'setContactPerson', 'Name Surname'],
            ['getContactPersonEmail', 'setContactPersonEmail', 'rdw@nebijokit.lt'],
            ['getCompanyTitle', 'setCompanyTitle', 'JSC Company'],
            ['getCompanyUrl', 'setCompanyUrl', 'http://google.com'],
            ['getCompanyInfo', 'setCompanyInfo', 'test'],
            ['getAddress', 'setAddress', 'Street 55, City'],
            ['getFax', 'setFax', 'Fax 55-55-22'],
            ['getFbUrl', 'setFbUrl', 'http://some-url.com'],
            ['getVkUrl', 'setVkUrl', 'http://some-url.com'],
            ['getOkUrl', 'setOkUrl', 'http://some-url.com'],
            ['getGpUrl', 'setGpUrl', 'http://some-url.com'],
            ['getTwitterUrl', 'setTwitterUrl', 'http://some-url.com'],
            ['getLinkedInUrl', 'setLinkedInUrl', 'http://some-url.com'],
            ['getYoutubeUrl', 'setYoutubeUrl', 'http://some-url.com'],
            ['getSkypeName', 'setSkypeName', 'test'],
            ['getPhoto', 'setPhoto', 'test.jpg'],
            ['getSalaryFrom', 'setSalaryFrom', 200],
            ['getSalaryTo', 'setSalaryTo', 500],
            ['getEmployerType', 'setEmployerType', Job::EMPLOYER_TYPE_COMPANY],
            ['getRequiredEducationType', 'setRequiredEducationType', (new EducationType())->setTitle('test')],
            ['getWorkExperienceType', 'setWorkExperienceType', (new WorkExperienceType())->setTitle('test')],
            ['getWorkScheduleType', 'setWorkScheduleType', (new WorkScheduleType())->setTitle('test')],
            ['getScope', 'setScope', (new Scope())->setTitle('test')],
            ['getEmploymentType', 'setEmploymentType', (new EmploymentType())->setTitle('test')],
            ['getCreatedAt', 'setCreatedAt', new \DateTime()],
            ['getDeletedAt', 'setDeletedAt', new \DateTime()],
            ['getStatus', 'setStatus', Job::STATUS_ACTIVE, Job::STATUS_INACTIVE],
            ['getSecret', 'setSecret', true, false],
            ['getVip', 'setVip', true, false],
            ['getShowInCarousel', 'setShowInCarousel', true, false],
        ];
    }

    /**
     * @dataProvider jobSettersDataProvider
     *
     * @param $getMethod
     * @param $setMethod
     */
    public function test_setters($getMethod, $setMethod, $value, $baseValue = null)
    {
        $job = $this->getJob();

        $this->assertEquals($baseValue, $job->{$getMethod}());

        $this->assertInstanceOf(self::CLASS_NAME, $job->{$setMethod}($value));
        $this->assertEquals($value, $job->{$getMethod}());
    }

    /**
     * Test phone
     */
    public function test_phone()
    {
        $job = $this->getJob();

        $this->assertNull($job->getPhone());

        $string = '+370 600-11 222';

        $this->assertInstanceOf(self::CLASS_NAME, $job->setPhone($string));
        $this->assertEquals($string, $job->getPhone());
        $this->assertRegExp('/^([\+0-9\s\-]+)$/', $job->getPhone());
    }

    /**
     * Test valid till
     */
    public function test_valid_till()
    {
        $job = $this->getJob();

        $defaultDate= new \DateTime();
        $defaultDate->add(new \DateInterval('P1Y'));

        $this->assertInstanceOf(self::CLASS_NAME, $job->setValidTill($defaultDate));
        $this->assertEquals($defaultDate, $job->getValidTill());
        $this->assertInstanceOf('DateTime', $job->getValidTill());

        $date = new \DateTime();

        $this->assertInstanceOf(self::CLASS_NAME, $job->setValidTill($date));
        $this->assertEquals($date, $job->getValidTill());
        $this->assertInstanceOf('DateTime', $job->getValidTill());
    }

    /**
     * Test gender choices
     */
    public function test_get_gender_choices()
    {
        $this->assertEmpty(array_diff(Job::getGenderChoices(), [Job::GENDER_MALE => 'Male', Job::GENDER_FEMALE => 'Female']));
    }

    /**
     * Test contact phone
     */
    public function test_it_should_add_contact_phone()
    {
        $job = $this->getJob();

        $this->assertTrue($job->getContactPersonPhones()->isEmpty());

        $phone = new ContactPersonPhone();
        $phone
            ->setEntity($job)
            ->setNumber('+370 600-11 222');

        $this->assertInstanceOf(self::CLASS_NAME, $job->addContactPersonPhone($phone));

        $this->assertEquals($phone, $job->getContactPersonPhones()->first());
    }

    /**
     * Test contact phone
     */
    public function test_it_should_not_add_contact_phone_if_null_given()
    {
        $job = $this->getJob()->addContactPersonPhone(null);

        $this->assertTrue($job->getContactPersonPhones()->isEmpty());
    }

    /**
     * Test contact phone
     */
    public function test_it_should_return_phones_as_string()
    {
        $job = $this->getJob();
        $number1 = '+370 600-11 222';
        $number2 = '+370 600-11 333';

        $phone1 = new ContactPersonPhone();
        $phone1
            ->setEntity($job)
            ->setNumber($number1);
        $phone2 = new ContactPersonPhone();
        $phone2
            ->setEntity($job)
            ->setNumber($number2);

        $job->addContactPersonPhone($phone1);
        $job->addContactPersonPhone($phone2);

        $this->assertEquals($job->getContactPersonPhonesAsString(), sprintf('%s, %s', $number1, $number2));
    }

    /**
     * Test contact phone is unique
     */
    public function test_it_should_check_if_phone_number_is_unique()
    {
        $job = $this->getJob();
        $number = '+370 600-11 222';

        $phone = (new ContactPersonPhone())->setNumber($number);

        $this->assertTrue($job->isContactPhoneNumberUnique($phone));

        $job->addContactPersonPhone($phone);

        $duplicatedPhone = (new ContactPersonPhone())->setNumber($number);

        $this->assertFalse($job->isContactPhoneNumberUnique($duplicatedPhone));
    }

    /**
     * Add contact phone by given string
     */
    public function test_it_should_add_contact_phone_by_given_string()
    {
        $job = $this->getJob();
        $number = '+370 600-11 222';

        $this->assertTrue($job->getContactPersonPhones()->isEmpty());

        $job->addContactPersonPhoneByString($number);

        $contactPersonPhone = $job->getContactPersonPhones()->first();

        $this->assertEquals($number, $contactPersonPhone->getNumber());
        $this->assertEquals($job, $contactPersonPhone->getEntity());
    }

    /**
     * Don't add contact phone when empty string given
     */
    public function test_it_should_not_add_contact_phone_by_given_empty_string()
    {
        $job = $this->getJob()->addContactPersonPhoneByString('');

        $this->assertTrue($job->getContactPersonPhones()->isEmpty());
    }

    /**
     * Test available statuses
     */
    public function test_statuses_should_be()
    {
        $statuses = [
            Job::STATUS_INACTIVE,
            Job::STATUS_PUBLISHED,
            Job::STATUS_WAITING,
            Job::STATUS_DELETED,
            Job::STATUS_BLOCKED,
            Job::STATUS_ACTIVE,
        ];

        $this->assertEmpty(array_diff($this->getJob()->getStatuses(), $statuses));
    }

    /**
     * test
     * Allow to change status for admin (TEMPORARY!)
     */
    public function it_should_not_return_active_status_in_all_statuses()
    {
        $this->assertFalse(in_array(Job::STATUS_ACTIVE, $this->getJob()->getStatuses()));
    }

    /**
     * Test available employer types
     */
    public function test_employer_types_should_be()
    {
        $types = [
            Job::EMPLOYER_TYPE_COMPANY,
            Job::EMPLOYER_TYPE_AGENCY,
            Job::EMPLOYER_TYPE_PERSON,
        ];

        $this->assertEmpty(array_diff(array_keys(Job::getEmployerTypeChoiceList()), $types));
    }

    /**
     * @test
     */
    public function it_should_implement_highlighted_item_interface()
    {
        $this->assertInstanceOf(self::CLASS_NAME, $this->getJob());
    }

    /**
     * @test
     */
    public function it_should_allow_delete_job_for_manager_of_job_owner()
    {
        $id = 5;
        $this->user->expects($this->once())->method('isManager')->willReturn(true);
        $this->user->expects($this->any())->method('getId')->willReturn($id);
        $this->user->expects($this->once())->method('getType')->willReturn(RegisteredUser::USER_TYPE_EMPLOYER);
        $this->user->expects($this->once())->method('hasRole')->with(RegisteredUser::ROLE_MANAGER)->willReturn(true);
        $this->owner->expects($this->once())->method('getManager')->willReturn($this->user);
        $this->owner->expects($this->any())->method('getId')->willReturn($id);

        $job = $this->getJob();
        $job->setUser($this->owner);

        $this->assertTrue($job->isAllowedToDeleteByUser($this->user));
    }

    /**
     * Test languages
     */
    public function test_languages_add()
    {
        $job = $this->getJob();

        $this->assertTrue($job->getLanguages()->isEmpty());

        $language = new JobLanguage();

        $this->assertInstanceOf(self::CLASS_NAME, $job->addLanguage($language));
        $this->assertNotTrue($job->getLanguages()->isEmpty());
        $this->assertEquals($language, $job->getLanguages()->last());
    }

    /**
     * @return Job
     */
    private function getJob()
    {
        return new Job();
    }

    /**
     * data provider
     *
     * @return array
     */
    public static function slugs()
    {
        $position = new Position();
        $position->setTitle('Position');
        $city = new City();
        $city->setTitle('city');
        $scope = new Scope();
        $scope->setTitle('Scope');

        return [
            ['position', 'Position', 'City', 'Scope'],
            ['position', 'Position', 'City'],
            ['position', 'Position', null, 'Scope'],
            ['position', 'Position', null, null],
            ['position-with-spaces', 'Position with spaces', 'city', 'scope'],
            ['position-one-position-two', 'Position one,position two', 'city', 'scope'],
            ['kurer', 'Курьер', 'Минск', null],
            ['zhurnalist', 'Журналист', 'Минск', 'IT, телекоммуникации, связь']
        ];
    }

    /**
     * @test
     * @dataProvider slugs
     */
    public function it_should_generate_slug(
        $slug,
        $positionTitle,
        $cityTitle = null,
        $scopeTitle = null
    ) {
        $job = $this->getJob();

        $position = new Position();
        $position->setTitle($positionTitle);
        $job->setPosition($position);

        if ($cityTitle) {
            $city = new City();
            $city->setTitle($cityTitle);
            $job->setWorkingPlaceCity($city);
        }

        if ($scopeTitle) {
            $scope = new Scope();
            $scope->setTitle($scopeTitle);
            $job->setScope($scope);
        }

        $job->generateSlug();

        $this->assertSame($slug, $job->getSlug());
    }

    /**
     * @test
     */
    public function it_should_allow_apply_by_default()
    {
        $job = $this->getJob();

        $this->assertTrue($job->getAllowApply());
    }

    /**
     * @test
     */
    public function it_should_allow_export_by_default()
    {
        $job = $this->getJob();

        $this->assertTrue($job->getAllowExport());
    }
}
