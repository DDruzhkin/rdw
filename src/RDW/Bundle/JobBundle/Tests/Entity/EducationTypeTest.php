<?php

namespace RDW\Bundle\JobBundle\Tests\Entity;

use RDW\Bundle\JobBundle\Entity\EducationType;

/**
 * Class EducationTypeTest
 *
 * @package RDW\Bundle\JobBundle\Tests\Entity
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class EducationTypeTest extends \PHPUnit_Framework_TestCase
{
    const CLASS_NAME = 'RDW\Bundle\JobBundle\Entity\EducationType';

    /**
     * Test set title
     */
    public function test_set_title()
    {
        $title = 'Title test';

        $educationType = new EducationType();
        $result = $educationType->setTitle($title);

        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals($title, $educationType->getTitle());
    }
}
