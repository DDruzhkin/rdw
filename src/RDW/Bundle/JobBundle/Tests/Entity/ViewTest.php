<?php

namespace RDW\Bundle\JobBundle\Tests\Entity;

use RDW\Bundle\JobBundle\Entity\View;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;

/**
 * Class ViewTest
 * @package RDW\Bundle\JobBundle\Tests\Entity
 */
class ViewTest extends \PHPUnit_Framework_TestCase
{
    const CLASS_NAME = 'RDW\Bundle\JobBundle\Entity\View';

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $user;

    /**
     * @var View
     */
    protected $view;

    /**
     * set up
     */
    public function setUp()
    {
        $this->user = $this->getMock('\RDW\Bundle\UserBundle\Entity\RegisteredUser');
        $this->view = new View();
    }

    /**
     * @test
     */
    public function it_should_return_id_null_by_default()
    {
        $this->assertInstanceOf(self::CLASS_NAME, $this->view);
        $this->assertNull($this->view->getId());
    }
}
