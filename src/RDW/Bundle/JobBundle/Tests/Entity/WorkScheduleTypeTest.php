<?php

namespace RDW\Bundle\JobBundle\Tests\Entity;

use RDW\Bundle\JobBundle\Entity\WorkScheduleType;

/**
 * Class WorkScheduleTypeTest
 *
 * @package RDW\Bundle\JobBundle\Tests\Entity
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class WorkScheduleTypeTest extends \PHPUnit_Framework_TestCase
{
    const CLASS_NAME = 'RDW\Bundle\JobBundle\Entity\WorkScheduleType';

    /**
     * Test set title
     */
    public function test_set_title()
    {
        $title = 'Title test';

        $workScheduleType = new WorkScheduleType();
        $result = $workScheduleType->setTitle($title);

        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals($title, $workScheduleType->getTitle());
    }
}
