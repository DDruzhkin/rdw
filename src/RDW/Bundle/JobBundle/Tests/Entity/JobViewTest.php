<?php

namespace RDW\Bundle\JobBundle\Tests\Entity;

use RDW\Bundle\JobBundle\Entity\JobView;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;

/**
 * Class JobViewTest
 * @package RDW\Bundle\JobBundle\Tests\Entity
 */
class JobViewTest extends \PHPUnit_Framework_TestCase
{
    const CLASS_NAME = 'RDW\Bundle\JobBundle\Entity\JobView';

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $user;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $job;

    /**
     * @var JobView
     */
    protected $jobView;

    /**
     * set up
     */
    public function setUp()
    {
        $this->user = $this->getMock('\RDW\Bundle\UserBundle\Entity\RegisteredUser');
        $this->job = $this->getMock('\RDW\Bundle\JobBundle\Entity\Job');
        $this->jobView = new JobView($this->job, $this->user);
    }

    /**
     * @test
     */
    public function it_should_return_user_job_after_instance()
    {
        $this->assertInstanceOf(self::CLASS_NAME, $this->jobView);
        $this->assertEquals($this->user, $this->jobView->getUser());
        $this->assertEquals($this->job, $this->jobView->getJob());
    }

    /**
     * @test
     */
    public function it_should_return_created_at()
    {
        $this->assertNull($this->jobView->getCreatedAt());

        $date = new \DateTime();

        $this->assertInstanceOf(self::CLASS_NAME, $this->jobView->setCreatedAt($date));
        $this->assertEquals($date, $this->jobView->getCreatedAt());
        $this->assertInstanceOf('DateTime', $this->jobView->getCreatedAt());
    }

    /**
     * @test
     */
    public function it_should_return_different_user()
    {
        $this->assertEquals($this->user, $this->jobView->getUser());

        $user = new RegisteredUser();

        $this->assertInstanceOf(self::CLASS_NAME, $this->jobView->setUser($user));
        $this->assertEquals($user, $this->jobView->getUser());
    }

    /**
     * @test
     */
    public function it_should_return_different_job()
    {
        $this->assertEquals($this->job, $this->jobView->getJob());

        $job = new Job();

        $this->assertInstanceOf(self::CLASS_NAME, $this->jobView->setJob($job));
        $this->assertEquals($job, $this->jobView->getJob());
    }
}
