<?php

namespace RDW\Bundle\JobBundle\Tests\Entity;

use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Entity\WorkExperienceType;

/**
 * Class WorkExperienceTypeTest
 *
 * @package RDW\Bundle\JobBundle\Tests\Entity
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class WorkExperienceTypeTest extends \PHPUnit_Framework_TestCase
{
    const CLASS_NAME = 'RDW\Bundle\JobBundle\Entity\WorkExperienceType';

    /**
     * @test
     */
    public function it_should_set_title_correctly()
    {
        $title = 'Title test';

        $workExperienceType = new WorkExperienceType();
        $result = $workExperienceType->setTitle($title);

        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals($title, $workExperienceType->getTitle());
    }

    /**
     * @test
     */
    public function it_should_not_be_deletable_if_has_jobs()
    {
        $workExperienceType = new WorkExperienceType();
        $this->assertTrue($workExperienceType->isDeletable());

        $job = new Job();
        $workExperienceType->addJob($job);

        $this->assertFalse($workExperienceType->isDeletable());
    }

    /**
     * @test
     */
    public function it_should_not_be_deletable_if_has_cvs()
    {
        $workExperienceType = new WorkExperienceType();
        $this->assertTrue($workExperienceType->isDeletable());

        $cv = new Cv();
        $workExperienceType->addCv($cv);

        $this->assertFalse($workExperienceType->isDeletable());
    }

}
