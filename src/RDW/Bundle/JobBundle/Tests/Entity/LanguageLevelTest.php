<?php

namespace RDW\Bundle\JobBundle\Tests\Entity;

use RDW\Bundle\JobBundle\Entity\LanguageLevel;

/**
 * Class LanguageLevelTest
 *
 * @package RDW\Bundle\JobBundle\Tests\Entity
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class LanguageLevelTest extends \PHPUnit_Framework_TestCase
{

    const CLASS_NAME = 'RDW\Bundle\JobBundle\Entity\LanguageLevel';

    /**
     * Test set title
     */
    public function test_set_title()
    {
        $title = 'Title test';

        $languageLevel = new LanguageLevel();
        $result = $languageLevel->setTitle($title);

        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals($title, $languageLevel->getTitle());
    }
}
