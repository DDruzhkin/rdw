<?php

namespace RDW\Bundle\JobBundle\Tests\Entity;

use RDW\Bundle\JobBundle\Entity\JobLanguage;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Entity\Language;
use RDW\Bundle\JobBundle\Entity\LanguageLevel;

/**
 * Class JobLanguageTest
 *
 * @package RDW\Bundle\JobBundle\Tests\Entity
 * @author  Paulius Aleliūnas <paulius@eface.lt>
 */
class JobLanguageTest extends \PHPUnit_Framework_TestCase
{
    const CLASS_NAME = 'RDW\Bundle\JobBundle\Entity\JobLanguage';

    /**
     * Test set job
     */
    public function test_set_job()
    {
        $jobLanguage = $this->getJobLanguage();

        $this->assertNull($jobLanguage->getJob());

        $job = new Job();

        $this->assertInstanceOf(self::CLASS_NAME, $jobLanguage->setJob($job));
        $this->assertEquals($job, $jobLanguage->getJob());
    }

    /**
     * Test set language
     */
    public function test_set_language()
    {
        $jobLanguage = $this->getJobLanguage();

        $this->assertNull($jobLanguage->getLanguage());

        $language = new Language();

        $this->assertInstanceOf(self::CLASS_NAME, $jobLanguage->setLanguage($language));
        $this->assertEquals($language, $jobLanguage->getLanguage());
    }

    /**
     * Test set language level
     */
    public function test_set_language_level()
    {
        $jobLanguage = $this->getJobLanguage();

        $this->assertNull($jobLanguage->getLevel());

        $level = new LanguageLevel();

        $this->assertInstanceOf(self::CLASS_NAME, $jobLanguage->setLevel($level));
        $this->assertEquals($level, $jobLanguage->getLevel());
    }

    /**
     * @return JobLanguage
     */
    private function getJobLanguage()
    {
        return new JobLanguage();
    }
}
