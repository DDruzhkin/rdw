<?php

namespace RDW\Bundle\JobBundle\Tests\Entity;

use RDW\Bundle\JobBundle\Entity\Language;

/**
 * Class LanguageTest
 *
 * @package RDW\Bundle\JobBundle\Tests\Entity
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class LanguageTest extends \PHPUnit_Framework_TestCase
{
    const CLASS_NAME = 'RDW\Bundle\JobBundle\Entity\Language';

    /**
     * Test set title
     */
    public function test_set_title()
    {
        $title = 'Title test';

        $language = new Language();
        $result = $language->setTitle($title);

        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals($title, $language->getTitle());
    }
}
