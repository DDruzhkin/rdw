<?php

namespace RDW\Bundle\JobBundle\Tests\EventListener;

use RDW\Bundle\JobBundle\Event\JobEvent;
use RDW\Bundle\JobBundle\EventListener\JobViewListener;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Entity\JobView;

/**
 * Class JobViewListenerTest
 * @package RDW\Bundle\JobBundle\Tests\EventListener
 */
class JobViewListenerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $jobManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $dispatcher;

    /**
     * @var JobViewListener
     */
    private $listener;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $user;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $owner;

    /**
     * @var Job
     */
    private $job;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $jobView;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $jobEvent;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $historyManager;

    /**
     * Set up
     */
    public function setUp()
    {
        $this->jobManager = $this->getMockBuilder('RDW\Bundle\JobBundle\Service\JobManager')
            ->disableOriginalConstructor()->getMock();
        $this->dispatcher = $this->getMock('Symfony\Component\EventDispatcher\EventDispatcher');

        $this->historyManager = $this->getMockBuilder('\RDW\Bundle\HistoryBundle\Service\HistoryManager')
            ->disableOriginalConstructor()->getMock();

        $this->listener = new JobViewListener($this->jobManager, $this->dispatcher, $this->historyManager);

        $this->user = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');
        $this->owner = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');

        $this->job = new Job();
        $this->job->setUser($this->owner);
        $this->jobView = $this->getMock('RDW\Bundle\JobBundle\Entity\JobView', [], [$this->job, $this->user]);

        $this->jobEvent = $this->getMockBuilder('RDW\Bundle\JobBundle\Event\JobEvent')->disableOriginalConstructor()->getMock();
    }

    /**
     * @test
     */
    public function it_should_add_job_view_to_job()
    {
        $this->jobEvent
            ->expects($this->once())
            ->method('getUser')
            ->willReturn($this->user);

        $this->jobEvent
            ->expects($this->once())
            ->method('getJob')
            ->willReturn($this->job);

        $this->jobManager
            ->expects($this->once())
            ->method('existsView')
            ->with($this->job, $this->user)
            ->willReturn(false);

        $this->assertEmpty($this->job->getUsersWhoViewed());
        $this->listener->onJobView($this->jobEvent);
        $this->assertNotEmpty($this->job->getUsersWhoViewed());
    }

    /**
     * @test
     */
    public function it_should_not_add_job_view_to_job_if_exists()
    {
        $this->jobEvent
            ->expects($this->once())
            ->method('getUser')
            ->willReturn($this->user);

        $this->jobEvent
            ->expects($this->once())
            ->method('getJob')
            ->willReturn($this->job);

        $this->jobManager
            ->expects($this->once())
            ->method('existsView')
            ->with($this->job, $this->user)
            ->willReturn(true);

        $this->assertEmpty($this->job->getUsersWhoViewed());
        $this->listener->onJobView($this->jobEvent);
        $this->assertEmpty($this->job->getUsersWhoViewed());
    }

    /**
     * @test
     */
    public function it_should_not_add_job_view_to_job_if_user_is_owner()
    {
        $this->job->setUser($this->user);

        $this->jobEvent
            ->expects($this->once())
            ->method('getUser')
            ->willReturn($this->user);

        $this->jobEvent
            ->expects($this->once())
            ->method('getJob')
            ->willReturn($this->job);

        $this->jobManager
            ->expects($this->any())
            ->method('existsView')
            ->with($this->job, $this->user)
            ->willReturn(true);

        $this->assertEmpty($this->job->getUsersWhoViewed());
        $this->listener->onJobView($this->jobEvent);
        $this->assertEmpty($this->job->getUsersWhoViewed());
    }

    /**
     * @test
     */
    public function it_should_not_add_job_view_to_job_if_user_is_manager()
    {
        $this->jobEvent
            ->expects($this->once())
            ->method('getUser')
            ->willReturn($this->user);

        $this->jobEvent
            ->expects($this->once())
            ->method('getJob')
            ->willReturn($this->job);

        $this->jobManager
            ->expects($this->any())
            ->method('existsView')
            ->with($this->job, $this->user)
            ->willReturn(false);

        $this->user
            ->expects($this->once())
            ->method('isManager')
            ->willReturn(true);

        $this->assertEmpty($this->job->getUsersWhoViewed());
        $this->listener->onJobView($this->jobEvent);
        $this->assertEmpty($this->job->getUsersWhoViewed());
    }

    /**
     * @test
     */
    public function it_should_return_null_if_user_not_set()
    {
        $this->jobEvent
            ->expects($this->once())
            ->method('getUser')
            ->willReturn(null);

        $this->assertNull($this->listener->onJobView($this->jobEvent));
    }
    
    /**
     * @test
     *
     * @expectedException  \Symfony\Component\Form\Exception\UnexpectedTypeException
     */
    public function it_should_throw_unexpected_type_exception_with_wrong_job()
    {
        $this->jobEvent
            ->expects($this->once())
            ->method('getUser')
            ->willReturn($this->user);

        $this->jobEvent
            ->expects($this->once())
            ->method('getJob')
            ->willReturn(null);

        $this->listener->onJobView($this->jobEvent);
    }
}
