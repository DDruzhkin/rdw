<?php

namespace RDW\Bundle\JobBundle\Tests\Service;

use RDW\Bundle\JobBundle\EventListener\ApplyListener;

/**
 * Class ApplyListenerTest
 *
 * @package RDW\Bundle\JobBundle\Tests\Service
 */
class ApplyListenerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $conversationManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $messageManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $historyManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $dispatcher;

    /**
     * @var ApplyListener
     */
    private $listener;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $applyEvent;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $apply;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $job;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $message;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $conversation;

    public function setUp()
    {
        $this->conversationManager = $this->getMockBuilder('RDW\Bundle\ConversationBundle\Service\ConversationManager')
            ->disableOriginalConstructor()->getMock();
        $this->messageManager = $this->getMockBuilder('RDW\Bundle\ConversationBundle\Service\MessageManager')
            ->disableOriginalConstructor()->getMock();
        $this->historyManager = $this->getMockBuilder('RDW\Bundle\HistoryBundle\Service\HistoryManager')
            ->disableOriginalConstructor()->getMock();
        $this->dispatcher = $this->getMock('Symfony\Component\EventDispatcher\EventDispatcher');
        $this->listener = new ApplyListener($this->conversationManager, $this->messageManager, $this->dispatcher, $this->historyManager);
        $this->applyEvent = $this->getMockBuilder('RDW\Bundle\JobBundle\Event\ApplyEvent')
            ->disableOriginalConstructor()->getMock();

        $this->job = $this->getMock('RDW\Bundle\JobBundle\Entity\Job');
        $this->apply = $this->getMock('RDW\Bundle\JobBundle\Entity\Apply');
        $this->message = $this->getMock('RDW\Bundle\ConversationBundle\Entity\Message');
        $this->conversation = $this->getMock('RDW\Bundle\ConversationBundle\Entity\Conversation');

        $this->apply->expects($this->once())->method('getJob')->willReturn($this->job);
        $this->applyEvent->expects($this->once())->method('getApply')->willReturn($this->apply);
    }

    /**
     * @test
     */
    public function it_should_create_new_conversation_when_it_not_exists()
    {
        $sender = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');
        $receiver = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');

        $this->conversationManager
            ->expects($this->once())
            ->method('findByApply')
            ->with($this->apply)
            ->willReturn(null);

        $this->conversationManager
            ->expects($this->once())
            ->method('create')
            ->with($receiver, $sender)
            ->willReturn($this->conversation);

        $this->conversationManager
            ->expects($this->once())
            ->method('addJobApply')
            ->with($this->conversation, $this->apply)
            ->willReturn($this->conversation);

        $this->addMessage();
    }

    /**
     * @test
     */
    public function it_should_not_create_new_conversation_when_it_exists()
    {
        $sender = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');
        $receiver = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');

        $this->conversationManager
            ->expects($this->once())
            ->method('findByApply')
            ->with($this->apply)
            ->willReturn(null);

        $this->conversationManager
            ->expects($this->exactly(0))
            ->method('create')
            ->with($receiver, $sender)
            ->willReturn($this->conversation);

        $this->conversationManager
            ->expects($this->exactly(0))
            ->method('addJobApply')
            ->with($this->apply)
            ->willReturn($this->conversation);

        $this->addMessage($this->conversation);
    }

    private function addMessage()
    {
        $this->messageManager
            ->expects($this->once())
            ->method('createFromJobApply')
            ->with($this->apply)
            ->willReturn($this->message);

        $this->conversationManager
            ->expects($this->once())
            ->method('addMessage')
            ->with($this->conversation, $this->message)
            ->willReturn($this->conversation);

        $this->conversationManager
            ->expects($this->once())
            ->method('update')
            ->with($this->conversation)
            ->willReturn(true);

        $result = $this->listener->onApplySuccess($this->applyEvent);
        $this->assertTrue($result);
    }
}
