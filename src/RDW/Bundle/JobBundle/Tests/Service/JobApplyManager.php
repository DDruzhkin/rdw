<?php

namespace RDW\Bundle\JobBundle\Tests\Service;

use RDW\Bundle\JobBundle\Entity\Abuse;
use RDW\Bundle\JobBundle\Service\JobAbuseManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class JobApplyManagerTest extends WebTestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $em;

    /**
     * @var
     */
    private $repo;

    /**
     * @var JobAbuseManager
     */
    private $abuseManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $paginator;

    /**
     * @var JobAbuseManager
     */
    private $sut;

    /**
     * set up
     */
    public function setUp()
    {
        $client = static::createClient();
        $container = $client->getContainer();

        $this->em = $this->getMockBuilder('\Doctrine\Common\Persistence\ObjectManager')->getMock();
        $this->repo = $container
            ->get('doctrine.orm.entity_manager')
            ->getRepository('RDWJobBundle:Apply');

        $this->paginator = $this->getMock('Knp\Component\Pager\Paginator');

        $this->sut = $container->get('rdw_job.service.job_apply_manager');
    }

    public function it_should_create_apply_from_job_and_user()
    {

    }
}
