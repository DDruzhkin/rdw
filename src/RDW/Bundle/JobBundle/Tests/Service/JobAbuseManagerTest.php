<?php

namespace RDW\Bundle\JobBundle\Tests\Service;

use RDW\Bundle\AppBundle\Entity\JobAbuse;
use RDW\Bundle\JobBundle\Service\JobAbuseManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class JobAbuseManagerTest extends WebTestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $em;

    /**
     * @var
     */
    private $repo;

    /**
     * @var JobAbuseManager
     */
    private $abuseManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $paginator;

    /**
     * @var JobAbuseManager
     */
    private $sut;

    /**
     * set up
     */
    public function setUp()
    {
        $client = static::createClient();
        $container = $client->getContainer();

        $this->em = $this->getMockBuilder('\Doctrine\Common\Persistence\ObjectManager')->getMock();
        $this->repo = $container
            ->get('doctrine.orm.entity_manager')
            ->getRepository('RDWAppBundle:JobAbuse');

        $this->paginator = $this->getMock('Knp\Component\Pager\Paginator');

        $this->sut = $container->get('rdw_job.service.job_abuse_manager');
        $this->abuseManager = new JobAbuseManager($this->em, $this->paginator);
    }

    /**
     * @test
     */
    public function it_should_create_new_abuse_object()
    {
        $abuse = new JobAbuse();
        $this->assertEquals($abuse, $this->abuseManager->create());
    }

    /**
     * @test
     */
    public function it_should_update_and_flush_object()
    {
        $object = new JobAbuse();

        $this->em
            ->expects($this->once())
            ->method('persist')
            ->will($this->returnValue(true));

        $this->em
            ->expects($this->once())
            ->method('flush')
            ->will($this->returnValue(true));

        $result = $this->abuseManager->update($object);
        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function it_should_create_new_object()
    {
        $object = new JobAbuse();
        $object->setReason('reason');

        $this->sut->update($object);

        $this->assertNotNull($object->getId());
        $this->assertInstanceOf('DateTime', $object->getCreatedAt());
    }

    /**
     * @test
     */
    public function it_should_not_create_new_object()
    {
        $object = new JobAbuse();
        $object->setReason('reason');

        $this->sut->update($object, false);

        $this->assertNull($object->getId());
    }

    /**
     * @test
     */
    public function it_should_persist_and_not_flush()
    {
        $object = new JobAbuse();

        $this->em
            ->expects($this->once())
            ->method('persist')
            ->will($this->returnValue(true));

        $this->em
            ->expects($this->never())
            ->method('flush')
            ->will($this->returnValue(true));

        $result = $this->abuseManager->update($object, false);
        $this->assertTrue($result);
    }
}
