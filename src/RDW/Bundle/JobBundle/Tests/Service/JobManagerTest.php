<?php

namespace RDW\Bundle\JobBundle\Tests\Service;

use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Service\JobManager;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;

/**
 * Class JobManagerTest
 *
 * @package RDW\Bundle\JobBundle\Tests\Service
 */
class JobManagerTest extends \PHPUnit_Framework_TestCase
{
    const CLASS_JOB = 'RDW\Bundle\JobBundle\Entity\Job';

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $em;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $paginator;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $user;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $companyType;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $arrayCollection;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $scope;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $job;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $repository;

    /**
     * @var JobManager
     */
    private $jobManager;

    /**
     * @var array
     */
    private $data;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $historyManager;

    /**
     * set up
     */
    public function setUp()
    {
        $this->em = $this->getMockBuilder('\Doctrine\Common\Persistence\ObjectManager')->getMock();
        $this->paginator = $this->getMock('Knp\Component\Pager\Paginator');
        $this->companyType = $this->getMock('RDW\Bundle\UserBundle\Entity\CompanyType');
        $this->arrayCollection = $this->getMock('Doctrine\Common\Collections\ArrayCollection');
        $this->scope = $this->getMock('RDW\Bundle\UserBundle\Entity\Scope');
        $this->user = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');
        $this->job = $this->getMock('RDW\Bundle\JobBundle\Entity\Job');
        $this->repository = $this->getMockBuilder('RDW\Bundle\JobBundle\Repository\JobRepository')->disableOriginalConstructor()->getMock();
        $this->historyManager = $this->getMockBuilder('RDW\Bundle\HistoryBundle\Service\HistoryManager')->disableOriginalConstructor()->getMock();
        $this->setData();

        $this->jobManager = new JobManager($this->em, $this->paginator, $this->historyManager);
    }

    /**
     * @test
     */
    public function it_should_use_job_repository_method_for_getting_object()
    {
        $id = 5;
        $this->repository->expects($this->once())->method('getById')->with($id);
        $this->em->expects($this->once())->method('getRepository')->with('RDWJobBundle:Job')->willReturn($this->repository);

        $this->jobManager->getById($id);
    }

    /**
     * @test
     */
    public function it_should_create_job_with_filled_user_data_when_user_has_scope()
    {
        $this->expectsThatUserHasAllRequiredData();
        $this->expectsThatUserHasScope(true);

        $result = $this->jobManager->createFromUser($this->user);

        $this->assertInstanceOf(self::CLASS_JOB, $result);
        $this->assertEquals($this->scope, $result->getScope());
        $this->assertRequiredData($result);
    }

    /**
     * @test
     */
    public function it_should_create_job_with_filled_user_data_when_user_has_no_scope()
    {
        $this->expectsThatUserHasAllRequiredData();
        $this->expectsThatUserHasScope(false);

        $result = $this->jobManager->createFromUser($this->user);

        $this->assertInstanceOf(self::CLASS_JOB, $result);
        $this->assertEquals(null, $result->getScope());
        $this->assertRequiredData($result);
    }

    private function setData()
    {
        $this->data = [
            'employer_type' => RegisteredUser::EMPLOYER_TYPE_COMPANY,
            'company_type' => $this->companyType,
            'company_title' => 'OOO',
            'url_address' => 'http://rdw.by/company/cool',
            'company_info' => 'Info about company',
            'cities' => $this->arrayCollection,
            'address' => 'Parko st. 15, Naujoji Vilnia, Vilnius',
            'phone' => '+370 4 933333 3',
            'fax' => '+370 4 933333 5',
            'fb_contact' => 'contact.fb',
            'vk_contact' => 'contact.vk',
            'ok_contact' => 'contact.ok',
            'gplus_contact' => 'contact.gplus',
            'twitter_contact' => 'contact.twitter',
            'linkedin_contact' => 'contact.linkedin',
            'skype_contact' => 'contact.skype',
            'youtube_url' => 'http://youtube.com/v/rdw',
            'contact_person' => 'Piotr',
            'contact_person_patronymic' => 'Uljanovic',
            'contact_person_surname' => 'Uljanov',
            'contact_person_photo' => 'random_img.png',
            'contact_person_email' => 'piotr@rdw.devnet.lt',
            'contact_person_phone' => '+370 93 33333',
            'photo' => 'random_img.jpg',
            'email' => 'user@rdw.by',
        ];
    }

    private function expectsThatUserHasAllRequiredData()
    {
        $this->user->expects($this->once())->method('getEmployerType')->willReturn($this->data['employer_type']);
        $this->user->expects($this->once())->method('getCompanyType')->willReturn($this->data['company_type']);
        $this->user->expects($this->once())->method('getCompanyTitle')->willReturn($this->data['company_title']);
        $this->user->expects($this->once())->method('getUrlAddress')->willReturn($this->data['url_address']);
        $this->user->expects($this->once())->method('getCompanyInfo')->willReturn($this->data['company_info']);
        $this->user->expects($this->any())->method('getCities')->willReturn($this->data['cities']);
        $this->user->expects($this->once())->method('getAddress')->willReturn($this->data['address']);
        $this->user->expects($this->once())->method('getPhone')->willReturn($this->data['phone']);
        $this->user->expects($this->once())->method('getFax')->willReturn($this->data['fax']);
        $this->user->expects($this->once())->method('getFbContact')->willReturn($this->data['fb_contact']);
        $this->user->expects($this->once())->method('getVkContact')->willReturn($this->data['vk_contact']);
        $this->user->expects($this->once())->method('getOKContact')->willReturn($this->data['ok_contact']);
        $this->user->expects($this->once())->method('getGPlusContact')->willReturn($this->data['gplus_contact']);
        $this->user->expects($this->once())->method('getTwitterContact')->willReturn($this->data['twitter_contact']);
        $this->user->expects($this->once())->method('getLinkedInContact')->willReturn($this->data['linkedin_contact']);
        $this->user->expects($this->once())->method('getSkypeContact')->willReturn($this->data['skype_contact']);
        $this->user->expects($this->once())->method('getYoutubeVideo')->willReturn($this->data['youtube_url']);
        $this->user->expects($this->once())->method('getContactPerson')->willReturn($this->data['contact_person']);
        $this->user->expects($this->once())->method('getContactPersonPatronymic')->willReturn($this->data['contact_person_patronymic']);
        $this->user->expects($this->once())->method('getContactPersonSurname')->willReturn($this->data['contact_person_surname']);
        $this->user->expects($this->once())->method('getContactPersonPhoto')->willReturn($this->data['contact_person_photo']);
        $this->user->expects($this->once())->method('getContactPersonEmail')->willReturn($this->data['contact_person_email']);
        $this->user->expects($this->once())->method('getContactPersonPhone')->willReturn($this->data['contact_person_phone']);
        $this->user->expects($this->once())->method('getPhoto')->willReturn($this->data['photo']);
        $this->user->expects($this->once())->method('getEmail')->willReturn($this->data['email']);
    }

    private function expectsThatUserHasScope($hasScope)
    {
        $return = (true === $hasScope) ? $this->scope : null;

        $this->user->expects($this->once())->method('getScope')->willReturn($return);
    }

    /**
     * @param Job $result
     */
    private function assertRequiredData(Job $result)
    {
        $this->assertEquals($this->data['employer_type'], $result->getEmployerType());
        $this->assertEquals($this->data['company_type'], $result->getCompanyType());
        $this->assertEquals($this->data['company_title'], $result->getCompanyTitle());
        $this->assertEquals($this->data['url_address'], $result->getCompanyUrl());
        $this->assertEquals($this->data['company_info'], $result->getCompanyInfo());
        $this->assertEquals($this->data['cities'], $result->getCities());
        $this->assertEquals($this->data['address'], $result->getAddress());
        $this->assertEquals($this->data['phone'], $result->getPhone());
        $this->assertEquals($this->data['fax'], $result->getFax());
        $this->assertEquals($this->data['fb_contact'], $result->getFbUrl());
        $this->assertEquals($this->data['vk_contact'], $result->getVkUrl());
        $this->assertEquals($this->data['ok_contact'], $result->getOkUrl());
        $this->assertEquals($this->data['gplus_contact'], $result->getGpUrl());
        $this->assertEquals($this->data['twitter_contact'], $result->getTwitterUrl());
        $this->assertEquals($this->data['linkedin_contact'], $result->getLinkedInUrl());
        $this->assertEquals($this->data['skype_contact'], $result->getSkypeName());
        $this->assertEquals($this->data['youtube_url'], $result->getYoutubeUrl());
        $this->assertEquals($this->data['contact_person'], $result->getContactPerson());
        $this->assertEquals($this->data['contact_person'], $result->getContactPerson());
        $this->assertEquals($this->data['contact_person_patronymic'], $result->getContactPersonPatronymic());
        $this->assertEquals($this->data['contact_person_surname'], $result->getContactPersonSurname());
        $this->assertEquals($this->data['contact_person_photo'], $result->getContactPersonPhoto());
        $this->assertEquals($this->data['contact_person_email'], $result->getContactPersonEmail());
        $this->assertEquals($this->data['contact_person_phone'], $result->getContactPersonPhonesAsString());
        $this->assertEquals($this->data['photo'], $result->getPhoto());
        $this->assertEquals($this->data['email'], $result->getEmail());
        $this->assertEquals($this->user, $result->getUser());
    }
}
