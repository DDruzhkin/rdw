(function( $ ){
    $.fn.saveJobAbuseHandler = function(options) {
        var settings = $.extend({}, options);

        $(this).on('click', function() {

            var form = $(this).parents('form');
            var $this = $(this);

            var options = {
                //beforeSubmit:  validate,  // pre-submit callback
                success:       showResponse,
                type:      'post',
                dataType:  'json',
                resetForm: false
            };

            form.ajaxForm(options);
            form.submit();

            // post-submit callback
            function showResponse(data)  {
                if (data.success) {
                    window.location = data.url;
                } else {
                    $('#' +$this.attr('data-content-holder')).html(data.content);
                    $("[data-handler='saveJobAbuse']").saveJobAbuseHandler();
                    $("[data-handler='abuseReason']").abuseReasonHandler();

                    // checkbox, radio button
                    if ($.fn.iCheck) {
                        $("input").iCheck({
                            radioClass: "iradio_minimal radio"
                        });
                    }
                }
                return false;
            }
        });

    };
})(jQuery);