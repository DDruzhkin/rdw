$(function() {
    $("[data-handler='saveJobAbuse']").saveJobAbuseHandler();
    $("[data-handler='abuseReason']").abuseReasonHandler();

    // checkbox, radio button
    if ($.fn.iCheck) {
        $("input").iCheck({
            radioClass: "iradio_minimal radio"
        });
    }
});