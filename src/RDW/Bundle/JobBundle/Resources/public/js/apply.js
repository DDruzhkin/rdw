$(function() {
    $("[data-handler='saveApply']").saveApplyHandler();
    $("[data-handler='applyForm']").applyFormHandler();
    $('[data-handler="fileUploadHandler"]').fileUploadHandler();
});
