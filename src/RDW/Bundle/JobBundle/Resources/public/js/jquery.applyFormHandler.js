(function( $ ){
    $.fn.applyFormHandler = function(options) {

        var settings = $.extend({
            cvUploadTypeDataElement: 'rdw_job_apply[cv]',
            cvUploadDataElement: 'description-element'
        }, options);

        var selectedElement = $("select[name='"+ settings.cvUploadTypeDataElement + "']");

        selectedElement.on('change', function(event) {
            var selectedValue = $(this).val();
            if (selectedValue > 0) {
                $('#upload-cv-holder').hide();
            } else {
                $('#upload-cv-holder').show();
            }
        });
    };
})(jQuery);
