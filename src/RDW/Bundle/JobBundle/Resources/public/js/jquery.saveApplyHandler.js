(function( $ ){
    $.fn.saveApplyHandler = function(options) {
        var settings = $.extend({
            contentHolder: "#modal_container"
        }, options);

        $(this).on('click', function() {

            var form = $(this).parents('form');
            var $this = $(this);

            var options = {
                success:       showResponse,
                type:      'post',
                dataType:  'json',
                resetForm: false
            };

            form.ajaxForm(options);
            form.submit();

            $(document).loaderStart();

            // post-submit callback
            function showResponse(data)  {
                if (data.success) {
                    // show registration popup?
                    if (typeof(data.popup) != 'undefined') {
                        $(settings.contentHolder).removeData('bs.modal').html(data.popup).modal('show');
                    } else {
                        window.location.reload();
                    }
                } else {
                    $('#' +$this.attr('data-content-holder')).html(data.content);
                    $("[data-handler='saveApply']").saveApplyHandler();
                    $("[data-handler='applyForm']").applyFormHandler();
                    $('[data-handler="fileUploadHandler"]').fileUploadHandler();
                }

                $(document).loaderStop();
            }
        });

    };
})(jQuery);
