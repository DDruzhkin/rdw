(function( $ ){
    $.fn.abuseReasonHandler = function(options) {

        var settings = $.extend({
            otherOptionsDataAttribute: 'other-reasons',
            reasonDataElement: 'reason-element',
            descriptionDataElement: 'description-element'
        }, options);

        if (
            $(this).data(settings.reasonDataElement) == typeof('undefined')
            || $(this).data(settings.otherOptionsDataAttribute) == typeof('undefined')
            ) {
            return false;
        }

        settings.otherOptionsValues = $(this).data(settings.otherOptionsDataAttribute).split(',');

        var selectElement = $('#' + $(this).data(settings.reasonDataElement));
        var descriptionElement = $('#' + $(this).data(settings.descriptionDataElement));

        $('#' + $(this).data(settings.reasonDataElement) + ' input').on('ifChecked', function(event){
            var selectedOther = false;

            for(var i = 0; i < settings.otherOptionsValues.length; i++) {
                if(settings.otherOptionsValues[i] == $(event.target).val()) {
                    selectedOther = true;
                    break;
                }
            }

            if (selectedOther) {
                descriptionElement.show();
            } else {
                descriptionElement.hide();
            }
        });
    };
})(jQuery);