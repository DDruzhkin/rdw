(function( $ ){
    $.fn.clearDataHandler = function(options) {
        var settings = $.extend({}, options);

        $(this).on('click', function(e) {
            e.preventDefault();

            $group = $(this).data('group');

            if($group !== undefined) {
                $element = ($group == 'person') ? $("[data-clear-person]") : $("[data-clear-company]");

                $element.val('').trigger("change");
                $element.find('img').hide();
                $element.find('li').remove();
            }
        });

    };
})(jQuery);

$(function() {
    $("[data-handler='clearData']").clearDataHandler();
});