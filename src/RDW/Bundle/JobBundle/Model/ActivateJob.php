<?php

namespace RDW\Bundle\JobBundle\Model;

use Symfony\Component\Validator\Constraints as Assert;
use RDW\Bundle\JobBundle\Validator\Constraints as RDWAssert;

/**
 * Class ActivateJob
 *
 * @package RDW\Bundle\JobBundle\Model
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class ActivateJob
{
    /**
     * @var \DateTime
     *
     * @Assert\NotBlank(message="Please enter date")
     * @RDWAssert\DateRange(min="today")
     */
    protected $validTill;

    /**
     * @return \DateTime
     */
    public function getValidTill()
    {
        return $this->validTill;
    }

    /**
     * @param \DateTime $validTill
     *
     * @return $this
     */
    public function setValidTill($validTill)
    {
        $this->validTill = $validTill;

        return $this;
    }
}
