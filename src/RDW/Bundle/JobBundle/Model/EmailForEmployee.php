<?php

namespace RDW\Bundle\JobBundle\Model;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class EmailForEmployee
 *
 * @package RDW\Bundle\JobBundle\Model
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class EmailForEmployee
{
    /**
     * @var string
     *
     * @Assert\NotBlank(message="Company title can not be empty")
     */
    protected $companyTitle;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Text can not be empty")
     */
    protected $text;

    /**
     * @return string
     */
    public function getCompanyTitle()
    {
        return $this->companyTitle;
    }

    /**
     * @param string $companyTitle
     *
     * @return $this
     */
    public function setCompanyTitle($companyTitle)
    {
        $this->companyTitle = $companyTitle;

        return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     *
     * @return $this
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }
}
