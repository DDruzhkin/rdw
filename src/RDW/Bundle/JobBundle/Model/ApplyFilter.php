<?php

namespace RDW\Bundle\JobBundle\Model;

use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\UserBundle\Entity\User;

/**
 * Class ApplyFilter
 *
 * @package RDW\Bundle\JobBundle\Model
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class ApplyFilter
{
    /**
     * @var Job
     */
    protected $job;

    /**
     * @var User
     */
    protected $user;

    /**
     * @return Job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * @param Job $job
     *
     * @return $this
     */
    public function setJob(Job $job)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * @return \RDW\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param \RDW\Bundle\UserBundle\Entity\User $user
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }
}
