<?php

namespace RDW\Bundle\JobBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use RDW\Bundle\JobBundle\Entity\Language;
use RDW\Bundle\JobBundle\Entity\ProfessionalArea;
use RDW\Bundle\LocationBundle\Entity\City;
use RDW\Bundle\SubscriptionBundle\Entity\Subscription;
use RDW\Bundle\SubscriptionBundle\Model\FilterInterface as SubscriptionFilterInterface;
use RDW\Bundle\UserBundle\Entity\User;

/**
 * Class JobFilter
 *
 * @package RDW\Bundle\JobBundle\Model
 */
class JobFilter implements SubscriptionFilterInterface
{
    const CREATED_AT_1_MONTH = '-1 month';
    const CREATED_AT_10_DAYS = '-10 days';
    const CREATED_AT_3_DAYS = '-3 days';
    const CREATED_AT_1_DAY = '-1 day';

    /**
     * @var ArrayCollection
     */
    protected $cities;

    /**
     * @var array
     */
    protected $notIds;

    /**
     * @var string
     */
    protected $gender;

    /**
     * @var string
     */
    protected $salary;

    /**
     * @var \RDW\Bundle\JobBundle\Entity\WorkScheduleType
     */
    protected $workScheduleType;

    /**
     * @var \RDW\Bundle\JobBundle\Entity\EmploymentType
     */
    protected $employmentType;

    /**
     * @var \RDW\Bundle\JobBundle\Entity\Language
     */
    // @todo remove language, use -> languages
    protected $language;

    /**
     * @var ArrayCollection
     */
    protected $languages;

    /**
     * @var string
     */
    protected $updatedAt;

    /**
     * @var string
     */
    protected $companyTitle;

    /**
     * @var \RDW\Bundle\UserBundle\Entity\Scope
     */
    protected $scope;

    /**
     * @var \RDW\Bundle\JobBundle\Entity\Position
     */
    protected $position;

    /**
     * @var ArrayCollection
     */
    protected $professionalArea;

    /**
     * @var bool
     */
    protected $notShowViewedJobs;

    /**
     * @var \RDW\Bundle\JobBundle\Entity\EducationType
     */
    protected $education;

    /**
     * @var \RDW\Bundle\JobBundle\Entity\WorkExperienceType
     */
    protected $workExperience;
    protected $withoutHigh;
    protected $withoutExp;
    protected $hideAgencies;

    /**
     * @var string
     */
    protected $employerType;

    /**
     * @var array
     */
    protected $statuses;

    /**
     * @var bool
     */
    protected $onlyWithSalary;

    /**
     * @var bool
     */
    protected $onlyPaid;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var bool
     */
    protected $showValidTillJobs;

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $contactEmail;

    /**
     * @var string
     */
    protected $contactPhone;

    protected $foreignId;
    protected $crmOrderId;

    protected $workingPlaceCity;
    protected $driverLicenseCategories;

    protected $query;

    /**
     * constructor
     */
    public function __construct()
    {
        $this->notShowViewedJobs = false;
        $this->cities = new ArrayCollection();
        $this->professionalArea = new ArrayCollection();
        $this->languages = new ArrayCollection();
    }

    /**
     * @return array
     */
    public static function getCreatedAtChoiceList()
    {
        return [
            self::CREATED_AT_1_DAY => '1 day ago',
            self::CREATED_AT_3_DAYS => '3 days ago',
            self::CREATED_AT_10_DAYS => '10 days ago',
            self::CREATED_AT_1_MONTH => '1 month ago',
        ];
    }

    /**
     * @return ArrayCollection
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @param ArrayCollection $cities
     *
     * @return $this
     */
    public function setCities($cities)
    {
        $this->cities = $cities;

        return $this;
    }

    /**
     * @param City $city
     *
     * @return $this
     */
    public function addCity(City $city)
    {
        $this->cities->add($city);

        return $this;
    }

    /**
     * @param City $city
     *
     * @return $this
     */
    public function removeCity(City $city)
    {
        if ($this->cities->contains($city)) {
            $this->cities->removeElement($city);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getEmployerType()
    {
        return $this->employerType;
    }

    /**
     * @param string $employerType
     *
     * @return $this
     */
    public function setEmployerType($employerType)
    {
        $this->employerType = $employerType;

        return $this;
    }

    /**
     * @return \RDW\Bundle\JobBundle\Entity\WorkExperienceType
     */
    public function getWorkExperience()
    {
        return $this->workExperience;
    }

    /**
     * @param \RDW\Bundle\JobBundle\Entity\WorkExperienceType $workExperience
     *
     * @return $this
     */
    public function setWorkExperience($workExperience)
    {
        $this->workExperience = $workExperience;

        return $this;
    }

    /**
     * @return \RDW\Bundle\JobBundle\Entity\EducationType
     */
    public function getEducation()
    {
        return $this->education;
    }

    /**
     * @param \RDW\Bundle\JobBundle\Entity\EducationType $education
     *
     * @return $this
     */
    public function setEducation($education)
    {
        $this->education = $education;

        return $this;
    }
    /**
     * @return \RDW\Bundle\JobBundle\Entity\EducationType
     */
    public function getWithoutHigh()
    {
        return $this->withoutHigh;
    }

    /**
     * @param \RDW\Bundle\JobBundle\Entity\EducationType $education
     *
     * @return $this
     */
    public function setWithoutHigh($withoutHigh)
    {
        $this->withoutHigh = $withoutHigh;

        return $this;
    }
    /**
     * @return \RDW\Bundle\JobBundle\Entity\EducationType
     */
    public function getWithoutExp()
    {
        return $this->withoutExp;
    }

    /**
     * @param \RDW\Bundle\JobBundle\Entity\EducationType $education
     *
     * @return $this
     */
    public function setWithoutExp($withoutExp)
    {
        $this->withoutExp = $withoutExp;

        return $this;
    }

    /**
     * @return \RDW\Bundle\JobBundle\Entity\EducationType
     */
    public function getHideAgencies()
    {
        return $this->hideAgencies;
    }

    /**
     * @param \RDW\Bundle\JobBundle\Entity\EducationType $education
     *
     * @return $this
     */
    public function setHideAgencies($hideAgencies)
    {
        $this->hideAgencies = $hideAgencies;

        return $this;
    }

    /**
     * @return \RDW\Bundle\JobBundle\Entity\Position
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param \RDW\Bundle\JobBundle\Entity\Position $position
     *
     * @return $this
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getProfessionalArea()
    {
        return $this->professionalArea;
    }

    /**
     * @param ArrayCollection $professionalArea
     *
     * @return $this
     */
    public function setProfessionalArea($professionalArea)
    {
        $this->professionalArea = $professionalArea;

        return $this;
    }

    /**
     * @param ProfessionalArea $professionalArea
     * @return $this
     *
     */
    public function addProfessionalArea(ProfessionalArea $professionalArea)
    {
        $this->professionalArea->add($professionalArea);

        return $this;
    }

    /**
     * @param ProfessionalArea $professionalArea
     *
     * @return $this
     */
    public function removeProfessionalArea(ProfessionalArea $professionalArea)
    {
        if ($this->professionalArea->contains($professionalArea)) {
            $this->professionalArea->removeElement($professionalArea);
        }

        return $this;
    }

    /**
     * @return \RDW\Bundle\UserBundle\Entity\Scope
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * @param \RDW\Bundle\UserBundle\Entity\Scope $scope
     *
     * @return $this
     */
    public function setScope($scope)
    {
        $this->scope = $scope;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyTitle()
    {
        return $this->companyTitle;
    }

    /**
     * @param string $companyTitle
     *
     * @return $this
     */
    public function setCompanyTitle($companyTitle)
    {
        $this->companyTitle = $companyTitle;

        return $this;
    }

    /**
     * @return \RDW\Bundle\JobBundle\Entity\Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param \RDW\Bundle\JobBundle\Entity\Language $language
     *
     * @return $this
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @return \RDW\Bundle\JobBundle\Entity\WorkScheduleType
     */
    public function getWorkScheduleType()
    {
        return $this->workScheduleType;
    }

    /**
     * @param \RDW\Bundle\JobBundle\Entity\WorkScheduleType $workScheduleType
     *
     * @return $this
     */
    public function setWorkScheduleType($workScheduleType)
    {
        $this->workScheduleType = $workScheduleType;

        return $this;
    }

    /**
     * @return string
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * @param string $salary
     *
     * @return $this
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;

        return $this;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     *
     * @return $this
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return array
     */
    public function getStatuses()
    {
        return $this->statuses;
    }

    /**
     * @param array $statuses
     *
     * @return $this
     */
    public function setStatuses(array $statuses)
    {
        $this->statuses = $statuses;

        return $this;
    }

    /**
     * @return array
     */
    public function getNotIds()
    {
        return $this->notIds;
    }

    /**
     * @param array $notIds
     *
     * @return $this
     */
    public function setNotIds(array $notIds = [])
    {
        $this->notIds = $notIds;

        return $this;
    }

    /**
     * @return \RDW\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param \RDW\Bundle\UserBundle\Entity\User $user
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getShowValidTillJobs()
    {
        return $this->showValidTillJobs;
    }

    /**
     * @param boolean $showValidTillJobs
     *
     * @return $this
     */
    public function setShowValidTillJobs($showValidTillJobs)
    {
        $this->showValidTillJobs = $showValidTillJobs;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getOnlyWithSalary()
    {
        return $this->onlyWithSalary;
    }

    /**
     * @param boolean $onlyWithSalary
     *
     * @return $this
     */
    public function setOnlyWithSalary($onlyWithSalary)
    {
        $this->onlyWithSalary = $onlyWithSalary;

        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param string $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getOnlyPaid()
    {
        return $this->onlyPaid;
    }

    /**
     * @param boolean $onlyPaid
     */
    public function setOnlyPaid($onlyPaid)
    {
        $this->onlyPaid = $onlyPaid;
    }

    /**
     * {@inheritdoc}
     */
    public function update(Subscription $subscription)
    {
        $workScheduleType = ($subscription->getWorkSchedules()) ? $subscription->getWorkSchedules()->first() : null;
        $scope = ($subscription->getScopes()) ? $subscription->getScopes()->first() : null;
        $position = ($subscription->getPositions()) ? $subscription->getPositions()->first() : null;

        $this->setCities($subscription->getCities());
        $this->setWorkScheduleType($workScheduleType);
        $this->setScope($scope);
        $this->setPosition($position);

        return $this;
    }

    /**
     * @return \RDW\Bundle\JobBundle\Entity\EmploymentType
     */
    public function getEmploymentType()
    {
        return $this->employmentType;
    }

    /**
     * @param \RDW\Bundle\JobBundle\Entity\EmploymentType $employmentType
     */
    public function setEmploymentType($employmentType)
    {
        $this->employmentType = $employmentType;
    }

    /**
     * @return ArrayCollection
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @param ArrayCollection $languages
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;
    }

    /**
     * @param Language $language
     *
     * @return $this
     */
    public function addLanguage(Language $language)
    {
        $this->languages->add($language);

        return $this;
    }

    /**
     * @param Language $language
     *
     * @return $this
     */
    public function removeLanguage(Language $language)
    {
        if ($this->languages->contains($language)) {
            $this->languages->removeElement($language);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * @param string $contactEmail
     *
     * @return $this
     */
    public function setContactEmail($contactEmail)
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactPhone()
    {
        return $this->contactPhone;
    }

    /**
     * @param string $contactPhone
     *
     * @return $this
     */
    public function setContactPhone($contactPhone)
    {
        $this->contactPhone = $contactPhone;

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getCrmOrderId()
    {
        return $this->crmOrderId;
    }

    public function setCrmOrderId($crmOrderId)
    {
        $this->crmOrderId = $crmOrderId;

        return $this;
    }

    public function getForeignId()
    {
        return $this->foreignId;
    }

    public function setForeignId($foreignId)
    {
        $this->foreignId = $foreignId;

        return $this;
    }

    /**
     * @return City
     */
    public function getWorkingPlaceCity()
    {
        return $this->workingPlaceCity;
    }

    /**
     * @param City $workingPlaceCity
     */
    public function setWorkingPlaceCity($workingPlaceCity)
    {
        $this->workingPlaceCity = $workingPlaceCity;
    }

    public function getDriverLicenseCategories()
    {
        return $this->driverLicenseCategories;
    }


    public function setDriverLicenseCategories($driverLicenseCategories)
    {
        $this->driverLicenseCategories = $driverLicenseCategories;
    }

    public function getShowWithSalary()
    {
        return $this->showWithSalary;
    }


    public function setShowWithSalary($showWithSalary)
    {
        $this->showWithSalary = $showWithSalary;
    }

    public function getOrderByFromRequestQuery(array $query = [])
    {
        $orderBy = [];
        $allowedOrderByFields = $this->getAllowedOrderByFields();
        $direction = (isset($query['direction']) && 'asc' === $query['direction']) ? 'ASC' : 'DESC';

        if (isset($query['sort']) && array_key_exists($query['sort'], $allowedOrderByFields)) {
            $orderBy[$allowedOrderByFields[$query['sort']]] = $direction;
        }

        return $orderBy;
    }

    public function getQuery()
    {
        return $this->query;
    }

    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }

    private function getAllowedOrderByFields()
    {
        return [
            'scope' => 'scope_title',
            'salaryFrom' => 'salary_from',
            'updatedAt' => 'updated_at',
        ];
    }
}
