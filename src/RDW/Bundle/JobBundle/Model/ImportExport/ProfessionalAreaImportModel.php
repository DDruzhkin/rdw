<?php

namespace RDW\Bundle\JobBundle\Model\ImportExport;

use RDW\Bundle\ImportExportBundle\Model\ImportModelInterface;

class ProfessionalAreaImportModel implements ImportModelInterface
{
    /**
     * @var string
     */
    protected $childPATitle;

    /**
     * @var string
     */
    protected $basePATitle;

    /**
     * @var string
     */
    protected $positionTitle;

    /**
     * @return string
     */
    public function getChildPATitle()
    {
        return $this->childPATitle;
    }

    /**
     * @param string $childPATitle
     *
     * @return $this
     */
    public function setChildPATitle($childPATitle)
    {
        $this->childPATitle = $childPATitle;

        return $this;
    }

    /**
     * @return string
     */
    public function getBasePATitle()
    {
        return $this->basePATitle;
    }

    /**
     * @param string $basePATitle
     *
     * @return $this
     */
    public function setBasePATitle($basePATitle)
    {
        $this->basePATitle = $basePATitle;

        return $this;
    }

    /**
     * @return string
     */
    public function getPositionTitle()
    {
        return $this->positionTitle;
    }

    /**
     * @param string $positionTitle
     *
     * @return $this
     */
    public function setPositionTitle($positionTitle)
    {
        $this->positionTitle = $positionTitle;

        return $this;
    }

    /**
     * @return array
     */
    public function getFields()
    {
        return [
            'Position',
            'Title',
            'SubTitle',
        ];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'Position' => $this->getPositionTitle(),
            'Title' => $this->getBasePATitle(),
            'SubTitle' => $this->getChildPATitle(),
        ];
    }
}
