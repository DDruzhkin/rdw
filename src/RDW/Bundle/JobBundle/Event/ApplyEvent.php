<?php

namespace RDW\Bundle\JobBundle\Event;

use RDW\Bundle\JobBundle\Entity\Apply;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class ApplyEvent occurs on apply for job
 *
 * @package Conversation
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class ApplyEvent extends Event
{
    /**
     * @var Apply
     */
    private $apply;

    /**
     * @param Apply $apply
     */
    public function __construct(Apply $apply)
    {
        $this->apply = $apply;
    }

    /**
     * @return \RDW\Bundle\JobBundle\Entity\Apply
     */
    public function getApply()
    {
        return $this->apply;
    }
}
