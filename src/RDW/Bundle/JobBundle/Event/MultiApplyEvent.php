<?php

namespace RDW\Bundle\JobBundle\Event;

use Doctrine\Common\Collections\ArrayCollection;
use RDW\Bundle\JobBundle\Entity\Apply;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class MultiApplyEvent
 *
 * @package RDW\Bundle\JobBundle\Event
 */
class MultiApplyEvent extends ApplyEvent
{
    /**
     * @var ArrayCollection
     */
    private $jobs;

    /**
     * @param ArrayCollection $jobs
     * @param Apply           $apply
     */
    public function __construct(ArrayCollection $jobs, Apply $apply)
    {
        parent::__construct($apply);

        $this->jobs = $jobs;
    }

    /**
     * @return ArrayCollection
     */
    public function getJobs()
    {
        return $this->jobs;
    }
}
