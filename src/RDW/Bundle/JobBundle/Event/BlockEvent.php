<?php

namespace RDW\Bundle\JobBundle\Event;

use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\ManageBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class BlockEvent
 *
 * @package RDW\Bundle\JobBundle\Event
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class BlockEvent extends Event
{
    /**
     * @var \RDW\Bundle\JobBundle\Entity\Job
     */
    private $job;

    /**
     * @var User
     */
    private $user;

    /**
     * Constructor
     *
     * @param Job  $job
     * @param User $user
     */
    public function __construct(Job $job, User $user)
    {
        $this->job = $job;
        $this->user = $user;
    }

    /**
     * @return Job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * @return \RDW\Bundle\ManageBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
