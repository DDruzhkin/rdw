<?php

namespace RDW\Bundle\JobBundle\Event;

use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class JobEvent
 *
 * @package RDW\Bundle\JobBundle\Event
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class JobEvent extends Event
{
    /**
     * @var Job
     */
    private $job;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Response
     */
    private $response;

    /**
     * @param Job  $job
     * @param User $user
     */
    public function __construct(Job $job, User $user = null)
    {
        $this->job = $job;
        $this->user = $user;
    }

    /**
     * @return Job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param Response $response
     *
     * @return $this
     */
    public function setResponse(Response $response)
    {
        $this->response = $response;

        return $this;
    }
}
