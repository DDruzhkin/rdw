<?php

namespace RDW\Bundle\JobBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Form\Type\LanguageFormType;

class JobAdmin extends AbstractAdmin
{

    protected $datagridValues = array(

        // display the first page (default = 1)
        '_page' => 1,

        // reverse order (default = 'ASC')
        '_sort_order' => 'DESC',

        // name of the ordered field (default = the model's id field, if any)
        '_sort_by' => 'createdAt',
    );

    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('employerType', 'choice', [
                'choices'   => Job::getEmployerTypeChoiceList(),
                'label' => 'Employer type',
                'empty_value' => 'Choose type',
                'required' => true,
                'attr' => [
                    'data-handler' => 'employerType',
                    'data-clear-company' => '',
                ]
            ])
            ->add('companyType', 'entity', [
                'empty_value' => 'Choose company type',
                'label' => 'Company type',
                'property' => 'title',
                'class' => 'RDWUserBundle:CompanyType',
                'required' => true,
                'attr' => [
                    'data-hide' => '',
                    'data-clear-company' => '',
                ],
            ])
            ->add('workingPlaceCity', 'entity', array(
                'required' => true,
                'class' => 'RDW\Bundle\LocationBundle\Entity\City',
                'label' => 'Working place city'
            ))
            ->add('companyTitle', 'text', [
                'label' => 'Company title',
                'required' => true,
                'attr' => [
                    'data-hide' => '',
                    'placeholder' => 'Company title placeholder',
                    'data-clear-company' => '',
                ],
            ])
            ->add('companyTradeMark', 'text', [
                'label' => 'Company trade mark',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Please fill company trade mark',
                    'data-hide' => '',
                    'data-clear-company' => '',
                ],
            ])
            ->add('companyUrl', 'text', [
                'label' => 'Company URL',
                'required' => false,
                'attr' => [
                    'data-hide' => '',
                    'placeholder' => 'Company url placeholder',
                    'data-clear-company' => '',
                ],
            ])
            ->add('companyInfo', 'textarea', [
                'label' => 'Company info',
                'required' => false,
                'attr' => [
                    'data-hide' => '',
                    'placeholder' => 'Company info placeholder',
                    'data-clear-company' => '',
                ],
            ])
            ->add('cities', 'entity', array(
                'required' => true,
                'class' => 'RDW\Bundle\LocationBundle\Entity\City',
                'multiple' => true,
                'label' => 'Cities',
            ))
            ->add('address', 'text', [
                'label' => 'Address',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Address placeholder',
                    'data-clear-company' => '',
                ],
            ])
            ->add('phone', 'text', [
                'label' => 'Phone',
                'required' => false,
                'attr' => [
                    'placeholder' => '+375 29 100 000 00',
                    'data-clear-company' => '',
                ],
            ])
            ->add('fax', 'text', [
                'label' => 'Fax',
                'required' => false,
                'attr' => [
                    'placeholder' => '+375 29 100 000 00',
                    'data-clear-company' => '',
                ],
            ])
            ->add('fbUrl', 'text', [
                'label' => 'Facebook URL',
                'required' => false,
                'attr' => ['placeholder' => 'Facebook URL placeholder'],
            ])
            ->add('vkUrl', 'text', [
                'label' => 'VK URL',
                'required' => false,
                'attr' => ['placeholder' => 'VK URL placeholder'],
            ])
            ->add('okUrl', 'text', [
                'label' => 'Odnoklassniki URL',
                'required' => false,
                'attr' => ['placeholder' => 'Odnoklassniki URL placeholder'],
            ])
            ->add('gpUrl', 'text', [
                'label' => 'Google+ URL',
                'required' => false,
                'attr' => ['placeholder' => 'Google+ URL placeholder'],
            ])
            ->add('twitterUrl', 'text', [
                'label' => 'Twitter URL',
                'required' => false,
                'attr' => ['placeholder' => 'Twitter URL placeholder'],
            ])
            ->add('linkedInUrl', 'text', [
                'label' => 'Linked In URL',
                'required' => false,
                'attr' => ['placeholder' => 'LinkedIn URL placeholder'],
            ])
            ->add('skypeName', 'text', [
                'label' => 'Skype name',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Skype name placeholder',
                    'data-clear-company' => '',
                ],
            ])
            ->add('scope', 'entity', array(
                'property' => 'title',
                'class' => 'RDW\Bundle\UserBundle\Entity\Scope',
                'required' => false
            ))
            ->add('youtubeUrl', 'text', [
                'label' => 'Youtube Url',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Youtube URL placeholder',
                    'data-clear-company' => '',
                ],
            ])
            ->add('position', 'sonata_type_model_autocomplete', array(
                'property' => 'title',
                'class' => 'RDW\Bundle\JobBundle\Entity\Position',
                'required' => true
            ))
            ->add('professionalArea', 'sonata_type_model_autocomplete', [
                'multiple' => true,
                'required' => true,
                'property' => 'title',
                'class' => 'RDW\Bundle\JobBundle\Entity\ProfessionalArea',
                'label' => 'Professional Area'
            ])
            ->add('requirements', 'ckeditor', [
                'config' => ['width' => '100%','inline' => true],
                'config_name' => 'jobCvFields',
                'required' => true
            ])
            ->add('responsibilities', 'ckeditor', [
                'label' => 'Responsibilities',
                'config' => ['width' => '100%', 'inline' => true],
                'config_name' => 'jobCvFields',
                'required' => true
            ])
            ->add('workingConditions', 'ckeditor', [
                'label' => 'Working conditions',
                'config' => ['width' => '100%', 'inline' => true],
                'config_name' => 'jobCvFields',
                'required' => true
            ])
            ->add('additionalInformation', 'ckeditor', [
                'label' => 'Information',
                'config' => ['width' => '100%', 'inline' => true],
                'config_name' => 'jobCvFields',
                'required' => true
            ])
            ->add('requiredEducationType', 'entity', [
                'label' => 'Required education',
                'class' => 'RDWJobBundle:EducationType',
                'empty_value' => 'No mattter',
                'property' => 'title',
                'required' => false,
            ])
            ->add('workExperienceType', 'entity', [
                'label' => 'Work experience',
                'class' => 'RDWJobBundle:WorkExperienceType',
                'empty_value' => 'No matter',
                'property' => 'title',
                'required' => false,
            ])
            ->add('salaryFrom', 'text', [
                'required' => false,
                "label" => 'Зарплата в BYR Oт'
            ])
            ->add('salaryTo', 'text', [
                'required' => false,
                "label" => 'Зарплата в BYR До'
            ])
            ->add('workScheduleType', 'entity', [
                'class' => 'RDWJobBundle:WorkScheduleType',
                'empty_value' => 'Choose option',
                'property' => 'title',
                'required' => false,
            ])
            ->add('languages', 'sonata_type_collection', [
                'required' => false,
                'by_reference' => false,
                'label' => 'Languages',
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
                'admin_code' => 'admin.job_language'
            ])
            ->add('contactPerson', 'text', [
                'label' => 'Contact person name',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Contact person name placeholder',
                    'data-clear-person' => '',
                ],
            ])
            ->add('contactPersonPatronymic', 'text', [
                'label' => 'Contact person patronymic',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Contact person patronymic placeholder',
                    'data-clear-person' => '',
                ],
            ])
            ->add('contactPersonSurname', 'text', [
                'label' => 'Contact person surname',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Contact person surname placeholder',
                    'data-clear-person' => '',
                ],
            ])
            ->add('contactPersonPhones', 'sonata_type_collection', [
                'by_reference' => false,
                'required' => false,
                'label' => 'Contact phone',
            ], [
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'position',
                'admin_code' => 'admin.contact_person_phone'
            ])
            ->add('contactPersonEmail', 'text', [
                'label' => 'Contact email',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Contact email placeholder',
                    'data-clear-person' => '',
                ],
            ])
            ->add('contactPersonSkype', 'text', [
                'label' => 'Contact skype',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Contact skype placeholder',
                    'data-clear-person' => '',
                ],
            ])
            ->add('contactPersonAddress', 'text', [
                'label' => 'Contact address',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Contact address placeholder',
                    'data-clear-person' => '',
                ],
            ])
            ->add('contactPersonCity', 'entity', array(
                'required' => true,
                'class' => 'RDW\Bundle\LocationBundle\Entity\City',
                'label' => 'Contact city',
            ))
            ->add('validTill', 'date', [
                'required' => true,
            ])
            ->add('secret', 'choice', [
                'choices'   => [
                    0 => 'Show company info',
                    1 => 'Do not show company info',
                ],
                'label' => 'Company info',
                'required' => true,
                'expanded' => true,
            ])
            ->add('driverLicenseCategories', 'entity', [
                'label' => 'Driver license category',
                'class' => 'RDWCvBundle:DriverLicenseCategory',
                'property' => 'title',
                'expanded' => true,
                'multiple' => true,
                'required' => false,
            ])
            ->add('carNeeded', 'choice', [
                'label' => 'Car',
                'choices'  => [
                    false => 'Not required',
                    true  => 'Required',
                ],
                'expanded' => true,
                'multiple' => false,
                'required' => false,
                'empty_value' => false,
            ])
            ->add('email', 'text', [
                'label' => 'Email',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Email placeholder',
                    'data-clear-company' => '',
                ],
            ])
            ->add('employmentType', 'entity', [
                'class' => 'RDWJobBundle:employmentType',
                'empty_value' => 'Choose option',
                'property' => 'title',
                'required' => false,
            ])
            ->add('vip', 'choice', [
                'choices'   => [
                    0 => 'No',
                    1 => 'Yes',
                ],
                'label' => 'VIP ad',
                'required' => true,
                'expanded' => true,
            ])
            ->add('showInCarousel', 'choice', [
                'choices'   => [
                    0 => 'No',
                    1 => 'Yes',
                ],
                'label' => 'Show in carousel',
                'required' => true,
                'expanded' => true,
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('position', null, array(
                //'operator_type' => 'sonata_type_model_autocomplete',
                'property' => 'title',
                'class' => 'RDW\Bundle\JobBundle\Entity\Position',
                'required' => true
            ))
            ->add("companyTitle")
            //->add('contactEmail')
            //->add('contactPhone')
            ->add('crmOrderId')
            ->add('foreignId')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('position', 'entity', array(
                'property' => 'title',
                'class' => 'RDW\Bundle\JobBundle\Entity\Position',
                'required' => true
            ))
            ->add('createdAt', 'date', [
                'label' => 'Создан в'
            ])
            ->add('validTill', 'date', [
                'label' => 'Срок действия до',
            ])
            ->add('workingPlaceCity', 'entity', array(
                'required' => true,
                'class' => 'RDW\Bundle\LocationBundle\Entity\City',
                'label' => 'Working place city'
            ))
            ->add('scope', 'entity', array(
                'property' => 'title',
                'class' => 'RDW\Bundle\UserBundle\Entity\Scope',
                'required' => false
            ))
            ->add('companyTitle', 'text', [
                'label' => 'Company title',
                'required' => true,
                'attr' => [
                    'data-hide' => '',
                    'placeholder' => 'Company title placeholder',
                    'data-clear-company' => '',
                ],
            ])
            ->add('crmOrderId')
            ->add('status')
        ;

        $listMapper->add('_action', 'actions', array('actions' => array(
            'edit'       => array(),
            'delete'     => array('label' => 'delete')
        )));
    }
}