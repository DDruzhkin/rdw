<?php

namespace RDW\Bundle\JobBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use RDW\Bundle\JobBundle\Entity\Position;

class PositionAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('title', 'text')
            ->add('type')
            ->add('status', 'choice', [
                'choices'  => [
                    Position::STATUS_NEW => 'New',
                    Position::STATUS_APPROVED => 'Approved',
                    Position::STATUS_DECLINED => 'Declined',
                ]
            ])
            ->add('professionalArea', 'sonata_type_model_autocomplete', array(
                'property' => 'title',
                'class' => 'RDW\Bundle\JobBundle\Entity\ProfessionalArea',
                'label' => 'Parent',
                'multiple' => true
            ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title')
        ;

        $listMapper->add('_action', 'actions', array('actions' => array(
            'edit'       => array(),
            'delete'     => array('label' => 'delete')
        )));
    }
}