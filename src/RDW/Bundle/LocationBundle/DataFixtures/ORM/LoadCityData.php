<?php

namespace RDW\Bundle\LocationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\LocationBundle\Entity\City;

/**
 * Loads city data
 *
 * @author Vytautas Beliunas <vytautas@eface.lt>
 */
class LoadCityData extends AbstractFixture
{
    const REFERENCE_STRING = 'rdw.city.';
    const COUNT = 4;

    /**
     * Loads fixture
     *
     * @param ObjectManager $manager Object manager
     *
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $cities = [
            1 => 'Minsk',
            2 => 'Brest',
            3 => 'Pinsk',
            4 => 'Vitebsk',
        ];
        foreach ($cities as $key => $city) {
            $entity = new City();
            $entity->setTitle($city);
            $entity->setTitleWhere($city);
            $entity->setTitleInLatin($city);
            $manager->persist($entity);
            $this->addReference(self::REFERENCE_STRING . $key, $entity);
        }

        $manager->flush();
    }
}
