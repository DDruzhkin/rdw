<?php

namespace RDW\Bundle\LocationBundle\Tests\Entity;

use RDW\Bundle\LocationBundle\Entity\City;

class CityTest extends \PHPUnit_Framework_TestCase
{

    public function cities()
    {
        return [
            ['Минск', 'minsk'],
            ['тест', 'test'],
            ['Брест', 'brest']
        ];
    }

    /**
     * @test
     * @dataProvider cities
     */
    public function it_should_generate_latin_title($title, $titleInLatin)
    {
        $city = new City();
        $city->setTitle($title);

        $this->assertNull($city->getTitleInLatin());

        $city->setTitleInLatin($city->generateTitleInLatin());

        $this->assertSame($titleInLatin, $city->getTitleInLatin());
    }
}
