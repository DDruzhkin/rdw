<?php

namespace RDW\Bundle\LocationBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\AppBundle\Entity\UploadFileInterface;
use RDW\Bundle\ManageBundle\Entity\Filter;
use RDW\Bundle\AppBundle\Entity\Transliterator;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use RDW\Bundle\AppBundle\Entity\Traits\UploadTrait;

/**
 * Class City
 *
 * @package RDW\Bundle\LocationBundle\Entity
 *
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cities")
 * @UniqueEntity("titleInLatin")
 */
class City extends Filter implements UploadFileInterface
{
    use UploadTrait;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="title_in_latin", type="string", length=100, nullable=true, unique=true)
     */
    protected $titleInLatin;

    /**
     * @ORM\Column(name="title_where", type="string", length=100)
     * @Assert\NotNull()
     */
    protected $titleWhere;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="RDW\Bundle\UserBundle\Entity\User", mappedBy="cities", fetch="EXTRA_LAZY")
     */
    protected $users;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\CvBundle\Entity\Cv", mappedBy="workingPlaceCity", fetch="EXTRA_LAZY")
     */
    protected $cvs;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\JobBundle\Entity\Job", mappedBy="workingPlaceCity", fetch="EXTRA_LAZY")
     */
    protected $jobs;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="RDW\Bundle\SubscriptionBundle\Entity\Subscription", mappedBy="cities", fetch="EXTRA_LAZY")
     */
    protected $subscriptions;

    /**
     * @var int
     */
    protected $entitiesCount;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->cvs = new ArrayCollection();
        $this->jobs = new ArrayCollection();
        $this->subscriptions = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * {@inheritdoc}
     */
    public function isDeletable()
    {
        return ! ($this->cvs->count() > 0 || $this->users->count() > 0 || $this->jobs->count() > 0 || $this->subscriptions->count() > 0 || $this->isSystem());
    }

    public function setEntitiesCount($entitiesCount)
    {
        $this->entitiesCount = $entitiesCount;

        return $this;
    }

    public function getEntitiesCount()
    {
        return $this->entitiesCount;
    }

    /**
     * generates title in latin
     *
     * @ORM\PrePersist
     *
     * @return string
     */
    public function generateTitleInLatin()
    {
        return Transliterator::transliterate($this->getTitle());
    }

    /**
     * Set titleInLatin
     *
     * @param string $titleInLatin
     * @return City
     */
    public function setTitleInLatin($titleInLatin)
    {
        $this->titleInLatin = $titleInLatin;

        return $this;
    }

    /**
     * Get titleInLatin
     *
     * @return string
     */
    public function getTitleInLatin()
    {
        return $this->titleInLatin;
    }

    /**
     * Add users
     *
     * @param \RDW\Bundle\UserBundle\Entity\User $users
     * @return City
     */
    public function addUser(\RDW\Bundle\UserBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \RDW\Bundle\UserBundle\Entity\User $users
     */
    public function removeUser(\RDW\Bundle\UserBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Add cvs
     *
     * @param \RDW\Bundle\CvBundle\Entity\Cv $cvs
     * @return City
     */
    public function addCv(\RDW\Bundle\CvBundle\Entity\Cv $cvs)
    {
        $this->cvs[] = $cvs;

        return $this;
    }

    /**
     * Remove cvs
     *
     * @param \RDW\Bundle\CvBundle\Entity\Cv $cvs
     */
    public function removeCv(\RDW\Bundle\CvBundle\Entity\Cv $cvs)
    {
        $this->cvs->removeElement($cvs);
    }

    /**
     * Get cvs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCvs()
    {
        return $this->cvs;
    }

    /**
     * Add jobs
     *
     * @param \RDW\Bundle\JobBundle\Entity\Job $jobs
     * @return City
     */
    public function addJob(\RDW\Bundle\JobBundle\Entity\Job $jobs)
    {
        $this->jobs[] = $jobs;

        return $this;
    }

    /**
     * Remove jobs
     *
     * @param \RDW\Bundle\JobBundle\Entity\Job $jobs
     */
    public function removeJob(\RDW\Bundle\JobBundle\Entity\Job $jobs)
    {
        $this->jobs->removeElement($jobs);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * Add subscriptions
     *
     * @param \RDW\Bundle\SubscriptionBundle\Entity\Subscription $subscriptions
     * @return City
     */
    public function addSubscription(\RDW\Bundle\SubscriptionBundle\Entity\Subscription $subscriptions)
    {
        $this->subscriptions[] = $subscriptions;

        return $this;
    }

    /**
     * Remove subscriptions
     *
     * @param \RDW\Bundle\SubscriptionBundle\Entity\Subscription $subscriptions
     */
    public function removeSubscription(\RDW\Bundle\SubscriptionBundle\Entity\Subscription $subscriptions)
    {
        $this->subscriptions->removeElement($subscriptions);
    }

    /**
     * Get subscriptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubscriptions()
    {
        return $this->subscriptions;
    }

    /**
     * @return string
     */
    public function getTitleWhere()
    {
        return $this->titleWhere;
    }

    /**
     * @param string $titleWhere
     *
     * @return $this
     */
    public function setTitleWhere($titleWhere)
    {
        $this->titleWhere = $titleWhere;

        return $this;
    }

    public function getUploadDir()
    {
        return "/uploads/cities";
    }
}
