<?php

namespace RDW\Bundle\LocationBundle\Behat;

use Behat\Gherkin\Node\TableNode;
use RDW\Bundle\AppBundle\Behat\DefaultContext;
use RDW\Bundle\LocationBundle\Entity\City;

/**
 * Class CityContext. Context related with location
 */
class LocationContext extends DefaultContext
{
    /**
     * @param TableNode $table
     *
     * @Given /^there are following cities:$/
     */
    public function thereAreFollowingCities(TableNode $table)
    {
        foreach ($table->getHash() as $data) {
            $this->thereIsCity($data['title']);
        }

        $this->getEntityManager()->flush();
    }

    /**
     * @param string $title
     * @param bool   $flush
     *
     * @return City
     */
    public function thereIsCity($title, $flush = true)
    {
        $city = $this->getEntityManager()->getRepository('RDWLocationBundle:City')->findOneBy(['title' => $title]);

        if (null === $city) {
            $city = new City();
            $city
                ->setTitle($title)
                ->setTitleWhere($title)
            ;

            $this->getEntityManager()->persist($city);

            if ($flush) {
                $this->getEntityManager()->flush();
            }
        }

        return $city;
    }
}
