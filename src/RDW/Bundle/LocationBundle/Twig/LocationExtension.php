<?php

namespace RDW\Bundle\LocationBundle\Twig;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;


/**
 * Class MenuExtension
 *
 * @author Vytautas Beliunas <vytautas@eface.lt>
 */
class LocationExtension extends \Twig_Extension
{
    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('cities', [$this, 'getCitiesString']),
        ];
    }

    /**
     * @param PersistentCollection $cities
     *
     * @return string
     */
    public function getCitiesString(PersistentCollection $cities)
    {
        $titles = [];
        foreach ($cities as $city) {
            $titles[] = $city->getTitle();
        }

        return implode(', ', $titles);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_location_extension';
    }
}
