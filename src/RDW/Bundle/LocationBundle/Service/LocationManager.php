<?php
namespace RDW\Bundle\LocationBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\LocationBundle\Entity\City;

/**
 *  Location service object
 */
class LocationManager
{
    protected $objectManager;

    /**
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @return City|null
     */
    public function getDefaultCity()
    {
        return $this->objectManager->getRepository('RDWLocationBundle:City')->findOneBy(['title' => 'Minsk']);
    }
}