$(document).ready(function() {
    var menuType = $("[data-handler='changeMenuType']");
    if (menuType.length > 0) {
        menuType.on('change', function(){

            menuType.closest('form').find('div').show();

            if (menuType.val() == menuType.attr('data-static-type') || menuType.val() == menuType.attr('data-static-modal-type')) {
                $("[data-hide-slug], [data-hide-title], [data-hide-url]").each(function( index ) {
                    $( this ).hide();
                });
            }

            if(menuType.val() == menuType.attr('data-system-type')) {
                $("[data-hide-text], [data-hide-url]").each(function( index ) {
                    $( this ).hide();
                });
            }

            if(menuType.val() == menuType.attr('data-url-type')) {
                $("[data-hide-text], [data-hide-slug]").each(function( index ) {
                    $( this ).hide();
                });
            }

            if(
                menuType.val() != menuType.attr('data-static-type')
                && menuType.val() != menuType.attr('data-system-type')
                && menuType.val() != menuType.attr('data-url-type')
            ) {
                $("[data-hide-text], [data-hide-url], [data-hide-slug]").each(function( index ) {
                    $( this ).hide();
                });
            }
        });
    }
});