<?php

namespace RDW\Bundle\MenuBundle\Repository;

use Gedmo\Sortable\Entity\Repository\SortableRepository;
use RDW\Bundle\ContentBundle\Entity\Text;
use RDW\Bundle\MenuBundle\Entity\Menu;

/**
 * Class UserRepository
 *
 * @package RDW\Bundle\ManageBundle\Repository
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class MenuRepository extends SortableRepository
{
    /**
     * @param string $position
     *
     * @return array
     */
    public function getMenuByPosition($position)
    {
        $queryBuilder = $this->getBySortableGroupsQueryBuilder((['position' => $position]));
        $queryBuilder->addSelect('t');
        $queryBuilder
            ->leftJoin('n.text', 't')
        ;

        $queryBuilder->andWhere(' (CASE WHEN (n.type = :type) THEN t.status ELSE \'' . Text::STATUS_ACTIVE . '\' END) = :status')
            ->setParameter('status', Text::STATUS_ACTIVE)
            ->setParameter('type', Menu::TYPE_STATIC)
        ;

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @return array
     */
    public function findAllActive()
    {
        $queryBuilder = $this->createQueryBuilder('n');
        $queryBuilder
            ->leftJoin('n.text', 't')
            ->andWhere(' (CASE WHEN (n.type = :type) THEN t.status ELSE \'' . Text::STATUS_ACTIVE . '\' END) = :status')
            ->setParameter('status', Text::STATUS_ACTIVE)
            ->setParameter('type', Menu::TYPE_STATIC)
            ->groupBy('n.slug, n.text, n.url')
        ;

        return $queryBuilder->getQuery()->getResult();
    }
}
