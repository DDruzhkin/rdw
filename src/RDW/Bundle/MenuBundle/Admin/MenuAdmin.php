<?php

namespace RDW\Bundle\MenuBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use RDW\Bundle\ContentBundle\Entity\Text;
use RDW\Bundle\MenuBundle\Entity\Menu;
use Doctrine\ORM\EntityRepository;

class MenuAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('type', 'choice', [
                'choices'   => Menu::getTypeChoiceList(),
                'empty_value' => 'Choose option',
                'label' => 'Menu type',
                'required' => false,
            ])
            ->add('text', 'entity', [
                'class' => 'RDWContentBundle:Text',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->where('t.status = :status')
                        ->setParameter('status', Text::STATUS_ACTIVE);
                },
                'required' => false,
                'empty_value' => 'Choose option',
                'property' => 'title',
                'label' => 'Text page',
            ])
            ->add('slug', 'entity', [
                'class' => 'RDWSlugBundle:Slug',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->where('s.active = :active')
                        ->andWhere('s.system = :system')
                        ->setParameter('active', true)
                        ->setParameter('system', true);
                },
                'required' => false,
                'empty_value' => 'Choose option',
                'property' => 'name',
                'label' => 'Slug',
            ])
            ->add('title', 'text', [
                'label' => 'Title',
                'required' => false,
            ])
            ->add('url', 'text', [
                'label' => 'URL',
                'required' => false,
            ])
            ->add('position', 'choice', [
                'choices'   => Menu::getPositionChoiceList(),
                'empty_value' => 'Choose option',
                'label' => 'Menu position',
                'required' => false,
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title')
        ;

        $listMapper->add('_action', 'actions', array('actions' => array(
            'edit'       => array(),
            'delete'     => array('label' => 'delete')
        )));
    }
}