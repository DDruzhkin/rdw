<?php

namespace RDW\Bundle\MenuBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use RDW\Bundle\ContentBundle\Entity\Text;
use RDW\Bundle\SlugBundle\Entity\Slug;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Main menu entity class
 *
 * @ORM\Entity(repositoryClass="RDW\Bundle\MenuBundle\Repository\MenuRepository")
 * @ORM\Table(name="menu")
 */
class Menu
{
    const POSITION_HEADER = 'header';
    const POSITION_FOOTER_LEFT = 'footer_left';
    const POSITION_FOOTER_CENTER = 'footer_center';
    const POSITION_FOOTER_RIGHT = 'footer_right';

    const TYPE_STATIC = 1;
    const TYPE_SYSTEM = 2;
    const TYPE_URL = 3;
    const TYPE_STATIC_MODAL = 4;
    const TYPE_SUBSCRIPTION_JOB_MODAL = 5;
    const TYPE_SUBSCRIPTION_CV_MODAL = 6;
    const TYPE_NEWSLETTER_MODAL = 7;

    private static $types = [
        self::TYPE_STATIC,
        self::TYPE_SYSTEM,
        self::TYPE_URL,
        self::TYPE_STATIC_MODAL,
        self::TYPE_SUBSCRIPTION_JOB_MODAL,
        self::TYPE_SUBSCRIPTION_CV_MODAL,
        self::TYPE_NEWSLETTER_MODAL,
    ];

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @Gedmo\SortablePosition
     * @ORM\Column(name="priority", type="integer")
     */
    private $priority;

    /**
     * @var Text
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\ContentBundle\Entity\Text", inversedBy="menuItems")
     * @ORM\JoinColumn(name="text_id", referencedColumnName="id")
     *
     * @Assert\NotBlank(message="Text cannot be empty", groups={"static"})
     */
    protected $text;

    /**
     * @var Slug
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\SlugBundle\Entity\Slug")
     * @ORM\JoinColumn(name="slug_id", referencedColumnName="id")
     *
     * @Assert\NotBlank(message="Slug cannot be empty", groups={"system"})
     */
    protected $slug;

    /**
     * @Gedmo\SortableGroup
     * @ORM\Column(name="position", type="string", length=128)
     *
     * @Assert\NotBlank(message="Position cannot be empty", groups={"create", "edit"})
     */
    private $position;

    /**
     * @var int
     * @ORM\Column(name="type", type="smallint")
     *
     * @Assert\NotBlank(message="Type cannot be empty", groups={"create", "edit"})
     */
    private $type;

    /**
     * @ORM\Column(name="title", type="string", nullable=true)
     *
     * @Assert\NotBlank(message="Title cannot be empty", groups={"system", "url"})
     */
    private $title;

    /**
     * @ORM\Column(name="url", type="string", nullable=true)
     *
     * @Assert\NotBlank(message="Url cannot be empty", groups={"url"})
     * @Assert\Url(
     *    message = "The url '{{ value }}' is not a valid url",
     *    groups={"url"}
     * )
     */
    private $url;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Move up
     *
     * @return self
     */
    public function moveUp()
    {
        $priority = $this->getPriority();

        $this->setPriority($priority - 1);

        return $this;
    }

    /**
     * Move down
     *
     * @return self
     */
    public function moveDown()
    {
        $priority = $this->getPriority();

        $this->setPriority($priority + 1);

        return $this;
    }

    /**
     * @return bool
     */
    public function isFromHeader()
    {
        return ($this->getPosition() == self::POSITION_HEADER) ? true : false;
    }

    /**
     * @return array
     */
    static public function getPositionChoiceList()
    {
        return [
            self::POSITION_HEADER => 'Header',
            self::POSITION_FOOTER_LEFT => 'Footer left',
            self::POSITION_FOOTER_CENTER => 'Footer center',
            self::POSITION_FOOTER_RIGHT => 'Footer right',
        ];
    }

    /**
     * @return array
     */
    static public function getTypeChoiceList()
    {
        return [
            self::TYPE_STATIC => 'Static',
            self::TYPE_SYSTEM => 'System',
            self::TYPE_URL => 'Url',
            self::TYPE_STATIC_MODAL => 'Static modal',
            self::TYPE_SUBSCRIPTION_JOB_MODAL => 'Subscription job modal',
            self::TYPE_SUBSCRIPTION_CV_MODAL => 'Subscription cv modal',
            self::TYPE_NEWSLETTER_MODAL => 'Subscription newsletter modal',
        ];
    }

    /**
     * @return Text
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param Text $text
     *
     * @return self
     */
    public function setText(Text $text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return Slug
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param Slug $slug
     *
     * @return $this
     */
    public function setSlug(Slug $slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @param int $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        if ($this->isStatic()) {
            return ($this->getText()) ? $this->getText()->getTitle() : '';
        }

        return (! $this->title) ? ($this->getSlug() instanceof Slug) ? $this->getSlug()->getName() : '' : $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return bool
     */
    public function isStatic()
    {
        return in_array($this->getType(), [self::TYPE_STATIC, self::TYPE_STATIC_MODAL]);
    }

    /**
     * @return bool
     */
    public function isUrl()
    {
        return ($this->getType() == self::TYPE_URL);
    }

    /**
     * @return bool
     */
    public function isSystem()
    {
        return ($this->getType() == self::TYPE_SYSTEM);
    }

    /**
     * @return bool
     */
    public function isModal()
    {
        return in_array($this->getType(), [self::TYPE_STATIC_MODAL, self::TYPE_SUBSCRIPTION_JOB_MODAL, self::TYPE_SUBSCRIPTION_CV_MODAL, self::TYPE_NEWSLETTER_MODAL]);
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return self::$types;
    }
}
