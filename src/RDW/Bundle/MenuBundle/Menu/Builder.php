<?php

namespace RDW\Bundle\MenuBundle\Menu;

use Knp\Menu\FactoryInterface;
use RDW\Bundle\MenuBundle\Entity\Menu;
use Symfony\Component\DependencyInjection\ContainerAware;
use \Knp\Menu\ItemInterface;

/**
 * Class MenuBuilder
 *
 * @package RDW\Bundle\MenuBundle\Builder
 */
class Builder extends ContainerAware
{
    /**
     * @param FactoryInterface $factory
     * @param array            $options
     *
     * @return ItemInterface
     */
    public function createMainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        // get header menu
        $items = $this->container->get('rdw_menu.service.menu_manager')->getMenuList(Menu::POSITION_HEADER);

        foreach ($items as $item) {
            $this->addChild($menu, $item);
        }

        return $menu;
    }

    /**
     * @param FactoryInterface $factory
     * @param array            $options
     *
     * @return ItemInterface
     */
    public function footerLeftMenu(FactoryInterface $factory, array $options)
    {
        $menu = $this->createMenu($factory);

        // get footer menu
        $items =  $this->container->get('rdw_menu.service.menu_manager')->getMenuList(Menu::POSITION_FOOTER_LEFT);

        foreach ($items as $item) {
            $this->addChild($menu, $item);
        }

        return $menu;
    }

    /**
     * @param FactoryInterface $factory
     * @param array            $options
     *
     * @return ItemInterface
     */
    public function footerRightMenu(FactoryInterface $factory, array $options)
    {
        $menu = $this->createMenu($factory);

        // get footer menu
        $items =  $this->container->get('rdw_menu.service.menu_manager')->getMenuList(Menu::POSITION_FOOTER_RIGHT);

        foreach ($items as $item) {
            $this->addChild($menu, $item);
        }

        return $menu;
    }

    /**
     * @param FactoryInterface $factory
     * @param array            $options
     *
     * @return ItemInterface
     */
    public function footerCenterMenu(FactoryInterface $factory, array $options)
    {
        $menu = $this->createMenu($factory);

        // get footer menu
        $items =  $this->container->get('rdw_menu.service.menu_manager')->getMenuList(Menu::POSITION_FOOTER_CENTER);

        foreach ($items as $item) {
            $this->addChild($menu, $item);
        }

        return $menu;
    }

    /**
     * @param ItemInterface $menu
     * @param Menu          $item
     *
     * @return ItemInterface
     */
    protected function addChild(ItemInterface $menu, Menu $item)
    {
        if (! $item->isModal()) {
            $menu->addChild($item->getTitle(), ['uri' => $this->container->get('rdw_menu.service.menu_manager')->getUrl($item)]);
        } else {
            $menu->addChild($item->getTitle(), ['uri' => '#']);
            $menu[$item->getTitle()]->setLinkAttribute('data-handler', 'modalLink');
            $menu[$item->getTitle()]->setLinkAttribute('data-url', $this->container->get('rdw_menu.service.menu_manager')->getUrl($item));
        }

        return $menu;
    }

    /**
     * @param FactoryInterface $factory
     *
     * @return ItemInterface
     */
    protected function createMenu(FactoryInterface $factory)
    {
        return $factory->createItem('root', [
            'childrenAttributes'  => [
                'class' => 'url_list'
            ]
        ]);
    }
}
