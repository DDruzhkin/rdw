<?php

namespace RDW\Bundle\MenuBundle\Tests\Entity;

use RDW\Bundle\MenuBundle\Entity\Menu;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class MenuTest
 * @package RDW\Bundle\MenuBundle\Tests\Entity
 */
class MenuTest extends WebTestCase
{
    /**
     * @return array
     */
    public function menuTypes()
    {
        return [
            [Menu::TYPE_STATIC, false],
            [Menu::TYPE_SYSTEM, false],
            [Menu::TYPE_URL, false],
            [Menu::TYPE_STATIC_MODAL, true],
            [Menu::TYPE_SUBSCRIPTION_CV_MODAL, true],
            [Menu::TYPE_SUBSCRIPTION_JOB_MODAL, true],
            [Menu::TYPE_NEWSLETTER_MODAL, true],
        ];
    }

    /**
     * @dataProvider menuTypes
     * @test
     */
    public function it_should_return_true_if_menu_is_modal($type, $status)
    {
        $menu = new Menu();
        $menu->setType($type);

        $this->assertEquals($status, $menu->isModal());
    }
}
