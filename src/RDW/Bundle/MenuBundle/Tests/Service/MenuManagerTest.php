<?php

namespace RDW\Bundle\MenuBundle\Tests\Entity;

use RDW\Bundle\MenuBundle\Entity\Menu;
use RDW\Bundle\MenuBundle\Service\MenuManager;
use RDW\Bundle\SlugBundle\Entity\Slug;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class MenuTest
 * @package RDW\Bundle\MenuBundle\Tests\Entity
 */
class MenuManagerTest extends WebTestCase
{
    /**
     * @var MenuManager
     */
    protected $menuManager;

    /**
     * @var string
     */
    protected $homePageUrl;

    /**
     * @var
     */
    protected $menu;

    /**
     * @var \Symfony\Component\DependencyInjection\Container;
     */
    protected $container;

    /**
     * setUp
     */
    public function setUp()
    {
        $client = static::createClient();
        $this->container = $client->getContainer();

        $slugService = $this->getMockBuilder('RDW\Bundle\SlugBundle\Service\SlugService')
            ->disableOriginalConstructor()
            ->getMock();

        $slugService->expects($this->any())
            ->method('getSlug')
            ->will($this->returnValue('slug'));

        $this->container->set('slug_service', $slugService);

        $router = $client->getContainer()->get('router');
        $router->getContext()->setHost('local');
        $router->getContext()->setScheme('http');
        $router->getContext()->setBaseUrl(null);
        $this->container->set('router', $router);

        $this->menu = $this->getMockBuilder('RDW\Bundle\MenuBundle\Entity\Menu')
            ->disableOriginalConstructor()
            ->getMock();

        $this->menu->expects($this->any())
            ->method('getId')
            ->will($this->returnValue(1));

        $text = $this->getMockBuilder('RDW\Bundle\ContentBundle\Entity\Text')
            ->disableOriginalConstructor()
            ->getMock();

        $text->expects($this->any())
            ->method('getId')
            ->will($this->returnValue(1));

        $this->menu->expects($this->any())
            ->method('getText')
            ->will($this->returnValue($text));

        $this->menuManager = $this->container->get('rdw_menu.service.menu_manager');
    }

    /**
     * @return array
     */
    public function menuTypes()
    {
        $container = static::createClient()->getContainer();

        return [
            [Menu::TYPE_STATIC, '//' . $container->getParameter('router.request_context.host') . '/slug'],
            [Menu::TYPE_SYSTEM, '//' . $container->getParameter('router.request_context.host') . '/slug'],
            [Menu::TYPE_URL, '/slug'],
            [Menu::TYPE_STATIC_MODAL, '/modal-text/1'],
            [Menu::TYPE_SUBSCRIPTION_CV_MODAL, '/subscription/subscribe/cv'],
            [Menu::TYPE_SUBSCRIPTION_JOB_MODAL, '/subscription/subscribe/job'],
            [Menu::TYPE_NEWSLETTER_MODAL, '/subscription/newsletter'],
        ];
    }

    /**
     * @dataProvider menuTypes
     * @test
     */
    public function it_should_return_correct_url_by_menu_type($type, $url)
    {
        $this->menu->expects($this->any())
            ->method('getType')
            ->will($this->returnValue($type));

        $this->menu->expects($this->any())
            ->method('getUrl')
            ->will($this->returnValue($url));

        $slug = new Slug();
        $slug->setName('slug');

        $this->menu->expects($this->any())
            ->method('getSlug')
            ->will($this->returnValue($slug));

        $this->assertEquals($url, $this->menuManager->getUrl($this->menu));
    }
}
