<?php

namespace RDW\Bundle\MenuBundle\Twig;

use RDW\Bundle\ContentBundle\Entity\Text;
use RDW\Bundle\MenuBundle\Entity\Menu;
use RDW\Bundle\SlugBundle\Entity\Slug;

/**
 * Class MenuExtension
 *
 * @author Vytautas Beliunas <vytautas@eface.lt>
 */
class MenuExtension extends \Twig_Extension
{
    /**
     * {@inheritDoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('menu_title', [$this, 'getMenuTitle']),
        ];
    }

    /**
     * @param Menu $menu
     *
     * @return null|string
     */
    public function getMenuTitle(Menu $menu)
    {
        if ($menu->isStatic()) {
            if ($menu->getText() instanceof Text) {
                return $menu->getText()->getTitle();
            }
        } else {
            return $menu->getTitle();
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'rdw_menu_extension';
    }
}
