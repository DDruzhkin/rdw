<?php
namespace RDW\Bundle\MenuBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\MenuBundle\Repository\MenuRepository;
use RDW\Bundle\MenuBundle\Entity\Menu;
use RDW\Bundle\SlugBundle\Service\SlugService;
use Symfony\Component\Routing\Router;

/**
 *  Menu service object
 */
class MenuManager
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var SlugService
     */
    protected $slugService;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var string
     */
    protected $host;

    /**
     * @param ObjectManager $objectManager
     * @param SlugService $slugService
     * @param Router $router
     * @param string $host
     */
    public function __construct(ObjectManager $objectManager, SlugService $slugService, \Symfony\Cmf\Component\Routing\ChainRouter $router, $host)
    {
        $this->objectManager = $objectManager;
        $this->slugService = $slugService;
        $this->router = $router;
        $this->host = $host;
    }

    /**
     * @param string $position
     *
     * @return array
     */
    public function getMenuList($position)
    {
        return $this->getRepository()->getMenuByPosition($position);
    }

    /**
     * @param int $id
     *
     * @return Menu
     */
    public function findById($id)
    {
        return $this->getRepository()->find($id);
    }

    /**
     * @param Menu $menu
     * @param bool $andFlush
     *
     * @return bool
     */
    public function update(Menu $menu, $andFlush = true)
    {
        $this->objectManager->persist($menu);

        if ($andFlush) {
            $this->objectManager->flush();
        }

        return true;
    }

    /**
     * @param Menu $menu
     *
     * @return bool
     */
    public function deleteMenu(Menu $menu)
    {
        $this->objectManager->remove($menu);
        $this->objectManager->flush();

        return true;
    }

    /**
     * @param Menu $item
     *
     * @return array
     */
    public function getUrl(Menu $item)
    {
        switch ($item->getType()) {
            case Menu::TYPE_STATIC:
                return '//' . $this->host . '/' . $this->slugService->getSlug($item->getText());
            case Menu::TYPE_STATIC_MODAL:
                return $this->router->generate('rdw_content.content.modal_text', ['id' => $item->getText()->getId()]);
            case Menu::TYPE_SYSTEM:
                return '//' . $this->host . '/' . $item->getSlug()->getName();
            case Menu::TYPE_URL:
                return $item->getUrl();
            case Menu::TYPE_SUBSCRIPTION_CV_MODAL:
                return $this->router->generate('rdw_subscription.subscription.subscribe', ['type' => 'cv']);
            case Menu::TYPE_SUBSCRIPTION_JOB_MODAL:
                return $this->router->generate('rdw_subscription.subscription.subscribe', ['type' => 'job']);
            case Menu::TYPE_NEWSLETTER_MODAL:
                return $this->router->generate('rdw_subscription.subscription.newsletter');
        }
    }

    /**
     * @return MenuRepository
     */
    protected function getRepository()
    {
        return $this->objectManager->getRepository('RDWMenuBundle:Menu');
    }
}