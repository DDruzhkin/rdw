<?php

namespace RDW\Bundle\MenuBundle\Controller;

use RDW\Bundle\MenuBundle\Entity\Menu;
use RDW\Bundle\MenuBundle\Service\MenuManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Menu managing controller
 *
 * @Route("/manage/menu")
 */
class MenuAdminController extends Controller
{
    /**
     * @Route("/list", name="rdw_menu.menu_admin.list")
     * @Method({"GET"})
     * @Template()
     *
     * @return array
     */
    public function listAction()
    {
        $this->isGrantedLocal();

        return [
            'menuHeader' => $this->getMenuManager()->getMenuList(Menu::POSITION_HEADER),
            'menuFooterLeft' => $this->getMenuManager()->getMenuList(Menu::POSITION_FOOTER_LEFT),
            'menuFooterCenter' => $this->getMenuManager()->getMenuList(Menu::POSITION_FOOTER_CENTER),
            'menuFooterRight' => $this->getMenuManager()->getMenuList(Menu::POSITION_FOOTER_RIGHT),
        ];
    }

    /**
     * @Route("/new", name="rdw_menu.menu_admin.new")
     * @Method({"GET"})
     * @Template()
     *
     * @return array
     */
    public function newAction()
    {
        $this->isGrantedLocal();

        $menu = new Menu();
        $form = $this->createForm('rdw_menu', $menu);

        return [
            'form' => $form->createView(),
            'menu' => $menu,
        ];
    }

    /**
     * @param Request $request
     *
     * @Route("/create", name="rdw_menu.menu_admin.create")
     * @Method({"POST"})
     * @Template("RDWMenuBundle:MenuAdmin:new.html.twig")
     *
     * @return array
     */
    public function createAction(Request $request)
    {
        $this->isGrantedLocal();

        $menu = new Menu();
        $form = $this->createForm('rdw_menu', $menu);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getMenuManager()->update($menu);
            $this->get('braincrafted_bootstrap.flash')->success('Menu item created');

            return $this->redirect($this->generateUrl('rdw_menu.menu_admin.list'));
        }

        return [
            'form' => $form->createView(),
            'menu' => $menu
        ];
    }

    /**
     * @param int $id
     *
     * @Route("/edit/{id}", requirements={"id" = "\d+"}, name="rdw_menu.menu_admin.edit")
     * @Method({"GET"})
     * @Template()
     *
     * @return array
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function editAction($id)
    {
        $this->isGrantedLocal();

        $menu = $this->getMenuManager()->findById($id);

        if ( ! $menu) {
            throw new NotFoundHttpException(sprintf('The menu with id "%d" does not exist.', $id));
        }

        $form = $this->createForm('rdw_menu', $menu);

        return [
            'form' => $form->createView(),
            'menu' => $menu,
        ];
    }

    /**
     * @param Request $request
     * @param int     $id
     *
     * @Route("/update/{id}", requirements={"id" = "\d+"}, name="rdw_menu.menu_admin.update")
     * @Method({"POST"})
     * @Template("RDWMenuBundle:MenuAdmin:edit.html.twig")
     *
     * @return array
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function updateAction(Request $request, $id)
    {
        $this->isGrantedLocal();

        $menu = $this->getMenuManager()->findById($id);

        if ( ! $menu) {
            throw new NotFoundHttpException(sprintf('The menu with id "%d" does not exist.', $id));
        }

        $form = $this->createForm('rdw_menu', $menu);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getMenuManager()->update($menu);
            $this->get('braincrafted_bootstrap.flash')->success('Menu item updated');

            return new RedirectResponse($this->generateUrl('rdw_menu.menu_admin.list'));
        }

        return [
            'form' => $form->createView(),
            'menu' => $menu,
        ];
    }

    /**
     * @param int $id
     *
     * @Route("/move-up/{id}", requirements={"id" = "\d+"}, name="rdw_menu.menu_admin.move_up")
     *
     * @return RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function moveUpAction($id)
    {
        $this->isGrantedLocal();

        $menu = $this->getMenuManager()->findById($id);

        if ( ! $menu) {
            throw new NotFoundHttpException(sprintf('The menu with id "%d" does not exist.', $id));
        }

        $menu->moveUp();
        $this->getMenuManager()->update($menu);

        return new RedirectResponse($this->generateUrl('rdw_menu.menu_admin.list'));
    }

    /**
     * @param int $id
     *
     * @Route("/move-down/{id}", requirements={"id" = "\d+"}, name="rdw_menu.menu_admin.move_down")
     *
     * @return RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function moveDownAction($id)
    {
        $this->isGrantedLocal();

        $menu = $this->getMenuManager()->findById($id);

        if ( ! $menu) {
            throw new NotFoundHttpException(sprintf('The menu with id "%d" does not exist.', $id));
        }

        $menu->moveDown();
        $this->getMenuManager()->update($menu);

        return new RedirectResponse($this->generateUrl('rdw_menu.menu_admin.list'));
    }

    /**
     * @param int $id
     *
     * @Route("/delete/{id}", requirements={"id" = "\d+"}, name="rdw_menu.menu_admin.delete")
     *
     * @return RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteAction($id)
    {
        $this->isGrantedLocal();

        $menu = $this->getMenuManager()->findById($id);

        if ( ! $menu) {
            throw new NotFoundHttpException(sprintf('The menu with id "%d" does not exist.', $id));
        }

        $this->getMenuManager()->deleteMenu($menu);

        return new RedirectResponse($this->generateUrl('rdw_menu.menu_admin.list'));
    }

    /**
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    private function isGrantedLocal()
    {
        if ( ! $this->container->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }
    }

    /**
     * @return MenuManager
     */
    protected function getMenuManager()
    {
        return $this->get('rdw_menu.service.menu_manager');
    }

    /**
     * @return \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    protected function getDispatcher()
    {
        return $this->container->get('event_dispatcher');
    }
}