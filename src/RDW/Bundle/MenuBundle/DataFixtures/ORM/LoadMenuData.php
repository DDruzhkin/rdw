<?php

namespace RDW\Bundle\FaqBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\ContactBundle\DataFixtures\ORM\LoadContactSlugData;
use RDW\Bundle\MenuBundle\Entity\Menu;
use Faker\Factory as FakerFactory;
use Faker\Generator;

/**
 * Loads menu data
 *
 * @author Vytautas Beliunas <vytautas@eface.lt>
 */
class LoadMenuData extends AbstractFixture implements DependentFixtureInterface
{
    protected $systemPages = ['faq', 'contact'];

    /**
     * Faker.
     *
     * @var Generator
     */
    protected $faker;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->faker = FakerFactory::create();
    }

    function getDependencies()
    {
        return[
            LoadFaqSlugData::class,
            LoadContactSlugData::class,
        ];
    }

    /**
     * Loads fixture
     *
     * @param \Doctrine\Common\Persistence\ObjectManager $manager Object manager
     *
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->systemPages as $page) {
            $menu = new Menu();
            $menu->setPosition(Menu::POSITION_HEADER);
            $menu->setType(Menu::TYPE_SYSTEM);
            $menu->setSlug($this->getReference($page . '-slug'));
            $manager->persist($menu);
        }

        $manager->flush();
    }
}