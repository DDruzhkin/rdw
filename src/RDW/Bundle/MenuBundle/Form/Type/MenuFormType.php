<?php

namespace RDW\Bundle\MenuBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use RDW\Bundle\ContentBundle\Entity\Text;
use RDW\Bundle\MenuBundle\Entity\Menu;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class MenuFormTypeMenuFormType
 *
 * @author Vytautas Beliunas <vytautas@eface.lt>
 */
class MenuFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', 'choice', [
                'choices'   => Menu::getTypeChoiceList(),
                'empty_value' => 'Choose option',
                'label' => 'Menu type',
                'required' => false,
            ])
            ->add('text', 'entity', [
                'class' => 'RDWContentBundle:Text',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->where('t.status = :status')
                        ->setParameter('status', Text::STATUS_ACTIVE);
                },
                'required' => false,
                'empty_value' => 'Choose option',
                'property' => 'title',
                'label' => 'Text page',
            ])
            ->add('slug', 'entity', [
                'class' => 'RDWSlugBundle:Slug',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->where('s.active = :active')
                        ->andWhere('s.system = :system')
                        ->setParameter('active', true)
                        ->setParameter('system', true);
                },
                'required' => false,
                'empty_value' => 'Choose option',
                'property' => 'name',
                'label' => 'Slug',
            ])
            ->add('title', 'text', [
                    'label' => 'Title',
                    'required' => false,
                ])
            ->add('url', 'text', [
                    'label' => 'URL',
                    'required' => false,
                ])
            ->add('position', 'choice', [
                'choices'   => Menu::getPositionChoiceList(),
                'empty_value' => 'Choose option',
                'label' => 'Menu position',
                'required' => false,
            ])
            ->add('save', 'submit');
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\MenuBundle\Entity\Menu',
            'validation_groups' =>
                function(FormInterface $form) {

                    $groups = ['create', 'edit'];

                    if ($form->getData()->isStatic()) {
                        $groups[] = 'static';
                    } elseif ($form->getData()->isUrl()) {
                        $groups[] = 'url';
                    } elseif ($form->getData()->isSystem()) {
                        $groups[] = 'system';
                    } else {
                        $groups[] = 'other';
                    }

                    return $groups;
                }
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_menu';
    }
}