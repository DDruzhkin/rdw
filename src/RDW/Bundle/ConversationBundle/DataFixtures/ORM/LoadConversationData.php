<?php

namespace RDW\Bundle\ConversationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\ConversationBundle\Entity\Conversation;
use RDW\Bundle\UserBundle\DataFixtures\ORM\LoadUserData;
use RDW\Bundle\UserBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Faker\Factory as FakerFactory;
use Faker\Generator;

/**
 * Test data for conversations
 *
 * @author Vytautas Beliunas <vytautas@eface.lt>
 */
class LoadConversationData extends AbstractFixture implements DependentFixtureInterface
{
    use ContainerAwareTrait;

    const REFERENCE_STRING = 'rdw.conversation.';
    const COUNT = 100;

    /**
     * Faker.
     *
     * @var Generator
     */
    protected $faker;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->faker = FakerFactory::create();
    }

    /**
     * @return array
     */
    function getDependencies()
    {
        return [
            LoadUserData::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $numberOfConversations = self::COUNT;

        for ($i = 1; $i <= $numberOfConversations; $i++) {
            $randomSelectedSender = rand(1, 10);
            $randomSelectedReceiver = rand(11, 20);
            /** @var User $sender */
            $sender = $this->getReference(LoadUserData::REFERENCE_STRING . $randomSelectedSender);
            /** @var User $receiver */
            $receiver = $this->getReference(LoadUserData::REFERENCE_STRING . $randomSelectedReceiver);

            $conversation = new Conversation();
            $conversation->setSender($sender);
            $conversation->setReceiver($receiver);
            $conversation->setTitle($this->faker->word);
            $conversation->setCreatedAt(new \DateTime());
            $conversation->setUpdatedAt(new \DateTime('-' . rand(1, 60) . 'M'));
            $conversation->setReceiverStatus(Conversation::CONVERSATION_STATUS_INBOX);
            $conversation->setSenderStatus(Conversation::CONVERSATION_STATUS_INBOX);

            $this->addReference(self::REFERENCE_STRING . $i, $conversation);
            $manager->persist($conversation);
        }

        $manager->flush();
    }
}
