<?php

namespace RDW\Bundle\ConversationBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\ConversationBundle\Entity\Conversation;
use RDW\Bundle\ConversationBundle\Entity\Message;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Faker\Factory as FakerFactory;
use Faker\Generator;

/**
 * Test data for messages
 *
 * @author Vytautas Beliunas <vytautas@eface.lt>
 */
class LoadMessageData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * Faker.
     *
     * @var Generator
     */
    protected $faker;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->faker = FakerFactory::create();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    function getDependencies()
    {
        return [
            LoadConversationData::class
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $numberOfExistingConversations = 100;

        for ($i = 1; $i <= $numberOfExistingConversations; $i++) {

            $conversation = $this->getReference(LoadConversationData::REFERENCE_STRING . $i);

            $users = [
                0 => $conversation->getSender(),
                1 => $conversation->getReceiver()
            ];

            $randomOfMessageInConversation = rand(1, 10);

            for ($y = 1; $y <= $randomOfMessageInConversation; $y++) {
                $senderNum = rand(0, 1);
                $receiverNum = ($senderNum > 0) ? 0 : 1;

                $sender = $users[$senderNum];
                $receiver = $users[$receiverNum];

                $message = new Message();
                $message->setSender($sender);
                $message->setReceiver($receiver);
                $message->setConversation($conversation);
                $message->setContent($this->faker->text(rand(20, 2000)));
                $message->setCreatedAt(new \DateTime());

                $variable = 'message_' . $i . $y;
                $$variable = $message;

                $manager->persist($$variable);
            }

            $manager->flush();
        }
    }
}
