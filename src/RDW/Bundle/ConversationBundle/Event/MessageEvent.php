<?php

namespace RDW\Bundle\ConversationBundle\Event;

use RDW\Bundle\ConversationBundle\Entity\Conversation;
use RDW\Bundle\ConversationBundle\Entity\Message;

/**
 * Class MessageEvent
 *
 * @package Conversation
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class MessageEvent extends ConversationEvent
{
    private $message;

    /**
     * Constructor
     *
     * @param Conversation $conversation
     * @param Message      $message
     */
    public function __construct(Conversation $conversation, Message $message)
    {
        parent::__construct($conversation);

        $this->message = $message;
    }

    /**
     * @return Message
     */
    public function getMessage()
    {
        return $this->message;
    }
}