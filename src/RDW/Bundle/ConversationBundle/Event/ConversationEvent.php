<?php

namespace RDW\Bundle\ConversationBundle\Event;

use RDW\Bundle\ConversationBundle\Entity\Conversation;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class ConversationEvent
 *
 * @package Conversation
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class ConversationEvent extends Event
{
    private $conversation;

    /**
     * Constructor
     *
     * @param Conversation $conversation
     */
    public function __construct(Conversation $conversation)
    {
        $this->conversation = $conversation;
    }

    /**
     * @return Conversation
     */
    public function getConversation()
    {
        return $this->conversation;
    }
}
