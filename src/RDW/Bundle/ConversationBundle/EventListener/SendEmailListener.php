<?php

namespace RDW\Bundle\ConversationBundle\EventListener;

use RDW\Bundle\ConversationBundle\Event\MessageEvent;
use RDW\Bundle\ConversationBundle\RDWConversationEvents;
use RDW\Bundle\MailerBundle\Service\MailerManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class SendEmailListener
 *
 * @package Conversation
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class SendEmailListener implements EventSubscriberInterface
{
    /**
     * @var \RDW\Bundle\MailerBundle\Service\MailerManager
     */
    private $mailerManager;

    /**
     * @param MailerManager $mailerManager
     */
    public function __construct(MailerManager $mailerManager)
    {
        $this->mailerManager = $mailerManager;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            RDWConversationEvents::SEND_MESSAGE_EMAIL => 'onSendMessageEmail',
        ];
    }

    /**
     * Send new email to employer
     *
     * @param MessageEvent $event
     */
    public function onSendMessageEmail(MessageEvent $event)
    {
        $message = $event->getMessage();

        $this->mailerManager->sendNewMessageEmail($message);
    }
}
