<?php

namespace RDW\Bundle\ConversationBundle\EventListener;

use RDW\Bundle\ConversationBundle\Entity\Conversation;
use RDW\Bundle\ConversationBundle\Event\ConversationEvent;
use RDW\Bundle\ConversationBundle\RDWConversationEvents;
use RDW\Bundle\ConversationBundle\Service\MessageManager;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\SecurityContext;
/**
 * Class ConversationEvent on view conversation
 *
 * @package Conversation
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class ViewListener implements EventSubscriberInterface
{
    /**
     * @var MessageManager
     */
    private $messageManger;

    /**
     * @var SecurityContext
     */
    protected $context;

    /**
     * @param MessageManager  $messageManger
     * @param SecurityContext $context
     */
    public function __construct(MessageManager $messageManger, SecurityContext $context)
    {
        $this->messageManger = $messageManger;
        $this->context = $context;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            RDWConversationEvents::CONVERSATION_VIEW => 'onView',
        ];
    }

    /**
     * View event
     *
     * @param ConversationEvent $event
     *
     * @throws UnexpectedTypeException
     */
    public function onView(ConversationEvent $event)
    {
        $conversation = $event->getConversation();

        if (!$conversation instanceof Conversation) {
            throw new UnexpectedTypeException(
                $conversation,
                'RDW\Bundle\ConversationBundle\Entity\Conversation'
            );
        }

        if ($this->context->getToken() instanceof TokenInterface) {
            if ($this->context->getToken()->getUser() instanceof RegisteredUser) {
                $this->messageManger->markConversationMessagesAsSeen($conversation, $this->context->getToken()->getUser());
            }
        }
    }
}
