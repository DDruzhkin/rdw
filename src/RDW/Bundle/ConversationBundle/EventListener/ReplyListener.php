<?php

namespace RDW\Bundle\ConversationBundle\EventListener;

use RDW\Bundle\ConversationBundle\Entity\Conversation;
use RDW\Bundle\ConversationBundle\Entity\Message;
use RDW\Bundle\ConversationBundle\Event\MessageEvent;
use RDW\Bundle\ConversationBundle\RDWConversationEvents;
use RDW\Bundle\ConversationBundle\Service\ConversationManager;
use RDW\Bundle\HistoryBundle\Entity\History;
use RDW\Bundle\HistoryBundle\RDWHistoryEvents;
use RDW\Bundle\HistoryBundle\Service\HistoryManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;

/**
 * Class ReplyListener replies to conversation
 *
 * @package Conversation
 */
class ReplyListener implements EventSubscriberInterface
{
    /**
     * @var ConversationManager
     */
    private $conversationManager;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var HistoryManager
     */
    private $historyManager;

    /**
     * @param ConversationManager      $conversationManager
     * @param EventDispatcherInterface $eventDispatcher
     * @param HistoryManager           $historyManager
     */
    public function __construct(ConversationManager $conversationManager, EventDispatcherInterface $eventDispatcher, HistoryManager $historyManager)
    {
        $this->conversationManager = $conversationManager;
        $this->eventDispatcher     = $eventDispatcher;
        $this->historyManager      = $historyManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            RDWConversationEvents::MESSAGE_REPLY_SUCCESS => 'onReplySuccess',
        ];
    }

    /**
     * Save replied message
     *
     * @param MessageEvent $event
     *
     * @throws UnexpectedTypeException
     */
    public function onReplySuccess(MessageEvent $event)
    {
        // @todo Need to refactor ReplyListener and WriteListener

        $conversation = $event->getConversation();

        if ( ! $conversation instanceof Conversation) {
            throw new UnexpectedTypeException(
                $conversation,
                'RDW\Bundle\ConversationBundle\Entity\Conversation'
            );
        }

        $message = $event->getMessage();

        if ( ! $message instanceof Message) {
            throw new UnexpectedTypeException(
                $message,
                'RDW\Bundle\ConversationBundle\Entity\Message'
            );
        }

        $receiver = ($conversation->getSender()->getId() == $message->getSender()->getId())
            ? $conversation->getReceiver()
            : $conversation->getSender();

        $message->setConversation($conversation);
        $message->setReceiver($receiver);
        $message->setCreatedAt(new \DateTime());
        $conversation->addMessage($message);
        $conversation->setUpdatedAt(new \DateTime());

        $this->conversationManager->save($conversation);

        $this->eventDispatcher->dispatch(RDWConversationEvents::SEND_MESSAGE_EMAIL, $event);

        $this->historyManager->logEvent($message->getSender(), History::ACTION_REPLY, $conversation, RDWHistoryEvents::REPLY);
    }
}
