<?php

namespace RDW\Bundle\ConversationBundle\EventListener;

use RDW\Bundle\ConversationBundle\Entity\Conversation;
use RDW\Bundle\ConversationBundle\Entity\Message;
use RDW\Bundle\ConversationBundle\Event\MessageEvent;
use RDW\Bundle\ConversationBundle\RDWConversationEvents;
use RDW\Bundle\ConversationBundle\Service\ConversationManager;
use RDW\Bundle\HistoryBundle\Entity\History;
use RDW\Bundle\HistoryBundle\RDWHistoryEvents;
use RDW\Bundle\HistoryBundle\Service\HistoryManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;

/**
 * Class WriteListener
 *
 * @package RDW\Bundle\ConversationBundle\EventListener
 */
class WriteListener implements EventSubscriberInterface
{
    /**
     * @var ConversationManager
     */
    private $conversationManager;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var HistoryManager
     */
    private $historyManager;

    /**
     * @param ConversationManager      $conversationManager
     * @param EventDispatcherInterface $eventDispatcher
     * @param HistoryManager           $historyManager
     */
    public function __construct(ConversationManager $conversationManager, EventDispatcherInterface $eventDispatcher, HistoryManager $historyManager)
    {
        $this->conversationManager = $conversationManager;
        $this->eventDispatcher     = $eventDispatcher;
        $this->historyManager      = $historyManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            RDWConversationEvents::MESSAGE_WRITE_SUCCESS => 'onWriteSuccess',
        ];
    }

    /**
     * @param MessageEvent $event
     *
     * @throws UnexpectedTypeException
     */
    public function onWriteSuccess(MessageEvent $event)
    {
        $conversation = $event->getConversation();

        if ( ! $conversation instanceof Conversation) {
            throw new UnexpectedTypeException(
                $conversation,
                'RDW\Bundle\ConversationBundle\Entity\Conversation'
            );
        }

        $message = $event->getMessage();

        if ( ! $message instanceof Message) {
            throw new UnexpectedTypeException(
                $message,
                'RDW\Bundle\ConversationBundle\Entity\Message'
            );
        }

        $message->setConversation($conversation);
        $message->setCreatedAt(new \DateTime());

        $conversation->setCreatedAt(new \DateTime());
        $conversation->addMessage($message);
        $conversation->setTitle('');

        $this->conversationManager->save($conversation);

        $this->eventDispatcher->dispatch(RDWConversationEvents::SEND_MESSAGE_EMAIL, $event);

        $this->historyManager->logEvent($message->getSender(), History::ACTION_WRITE, $conversation, RDWHistoryEvents::WRITE);
    }
}
