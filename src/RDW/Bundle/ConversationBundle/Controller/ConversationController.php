<?php

namespace RDW\Bundle\ConversationBundle\Controller;

use RDW\Bundle\ConversationBundle\Entity\Conversation;
use RDW\Bundle\ConversationBundle\Entity\Message;
use RDW\Bundle\ConversationBundle\Event\ConversationEvent;
use RDW\Bundle\ConversationBundle\Event\MessageEvent;
use RDW\Bundle\ConversationBundle\Form\Type\ConversationCollectionType;
use RDW\Bundle\ConversationBundle\RDWConversationEvents;
use RDW\Bundle\ConversationBundle\Service\ConversationManager;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Class ConversationController
 *
 * @package RDW\Bundle\ConversationBundle\Controller
 *
 * @Route("/conversation")
 */
class ConversationController extends Controller
{
    /**
     * @param Request $request
     *
     * @Route("/inbox", name="rdw_conversation.conversation.inbox")
     * @Method({"GET"})
     * @Template()
     *
     * @return array
     */
    public function inboxAction(Request $request)
    {
        $pagination = $this->getConversationManager()->getPaginatedConversationList(
            ['user' => $this->getUser(), 'status' => Conversation::CONVERSATION_STATUS_INBOX],
            ['updatedAt' => 'desc'],
            $request->query->get('page', 1),
            $this->getPerPageNumber()
        );

        $form = $this->createForm(new ConversationCollectionType($pagination->getItems()));

        return [
            'pagination' => $pagination,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Template()
     *
     * @return array
     */
    public function unreadConversationsBlockAction()
    {
        $limit = $this->container->getParameter('rdw_conversation.unread_conversations_block_limit');

        $conversations = $this->container
            ->get('doctrine')
            ->getRepository('RDWConversationBundle:Conversation')
            ->getUnreadConversations($this->getUser(), $limit);

        return [
            'conversations' => $conversations,
        ];
    }

    /**
     * @param Request        $request
     * @param RegisteredUser $loggedInUser
     * @param RegisteredUser $otherUser
     *
     * @Route(
     *      "/list/{logged_in_user_id}/{other_user_id}",
     *      requirements={"logged_in_user_id" = "\d+", "other_user_id" = "\d+"},
     *      name="rdw_conversation.conversation.list_by_user"
     * )
     * @ParamConverter("loggedInUser", class="RDWUserBundle:RegisteredUser", options={"id" = "logged_in_user_id"})
     * @ParamConverter("otherUser", class="RDWUserBundle:RegisteredUser", options={"id" = "other_user_id"})
     * @Method({"GET"})
     * @Template()
     *
     * @return array
     */
    public function listByUserAction(Request $request, RegisteredUser $loggedInUser, RegisteredUser $otherUser)
    {
        $sessionUser = $this->getUser();

        // session user must the same loggedInUser or manager of loggedInUser
        if (! $sessionUser instanceof RegisteredUser
            || (
                $sessionUser->getId() != $loggedInUser->getId()
                && ! $loggedInUser->isUserManager($sessionUser)
            )
        ) {
            throw new AccessDeniedHttpException();
        }

        $pagination = $this->getConversationManager()->getPaginatedListBetweenUsers(
            $loggedInUser,
            $otherUser,
            $request->query->get('page', 1),
            $this->container->getParameter('rdw_conversation.conversation.paginator.per_page')
        );

        return [
            'user' => $otherUser,
            'pagination' => $pagination,
            'form' => $this->createForm(new ConversationCollectionType($pagination->getItems()))->createView(),
        ];
    }

    /**
     * @param Cv $cv
     *
     * @Route("/list-by-cv/{id}", requirements={"id" = "\d+"}, name="rdw_conversation.conversation.list_by_cv")
     * @Method({"GET"})
     * @Template()
     *
     * @return array
     *
     * @throws AccessDeniedException
     */
    public function listByCvAction(Cv $cv)
    {
        if (! $cv->isOwner($this->getUser())) {
            throw new AccessDeniedException();
        }

        $pagination = $this->getConversationManager()->getPaginatedConversationList(
            ['user' => $this->getUser(), 'cv' => $cv, 'status' => Conversation::CONVERSATION_STATUS_INBOX],
            ['updatedAt' => 'desc'],
            $this->get('request')->query->get('page', 1),
            $this->container->getParameter('rdw_conversation.conversation.paginator.per_page')
        );

        return [
            'cv' => $cv,
            'pagination' => $pagination,
            'form' => $this->createForm(new ConversationCollectionType($pagination->getItems()))->createView(),
        ];
    }

    /**
     * @param int $id
     *
     * @Route("/view/{id}", requirements={"id" = "\d+"}, name="rdw_conversation.conversation.view")
     * @Method({"GET"})
     * @Template()
     *
     * @return array
     * @throws NotFoundHttpException
     */
    public function viewAction($id)
    {
        $conversation = $this->getConversationManager()->findByIdAndUser($id, $this->getUser());

        if (!$conversation instanceof Conversation) {
            throw $this->createNotFoundException('The conversation does not exist');
        }

        $form = $this->createForm('rdw_message_reply', new Message());

        $event = new ConversationEvent($conversation);
        $this->getDispatcher()->dispatch(RDWConversationEvents::CONVERSATION_VIEW, $event);

        return [
            'form' => $form->createView(),
            'conversation' => $conversation,
        ];
    }

    /**
     * @param int     $id
     * @param Request $request
     *
     * @Route("/reply/{id}", requirements={"id" = "\d+"}, name="rdw_conversation.conversation.reply")
     * @Method({"POST"})
     * @Template("RDWConversationBundle:Conversation:view.html.twig")
     *
     * @return array|RedirectResponse
     * @throws NotFoundHttpException
     */
    public function replyAction($id, Request $request)
    {
        $conversation = $this->getConversationManager()->findByIdAndUser($id, $this->getUser());

        if (! $conversation instanceof Conversation) {
            throw $this->createNotFoundException('The conversation does not exist');
        }

        $message = new Message();
        $message->setSender($this->getUser());
        $form = $this->createForm('rdw_message_reply', $message);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new MessageEvent($conversation, $message);
            $this->getDispatcher()->dispatch(RDWConversationEvents::MESSAGE_REPLY_SUCCESS, $event);

            //redirect to inbox
            return $this->redirect($this->generateUrl('rdw_conversation.conversation.inbox'));
        }

        return [
            'form' => $form->createView(),
            'conversation' => $conversation,
        ];
    }

    /**
     * @param RegisteredUser $receiver
     *
     * @Route("/write-message/{id}", requirements={"id" = "\d+"}, name="rdw_conversation.conversation.write_message")
     * @Method({"GET"})
     * @Template()
     *
     * @return Response
     */
    public function writeMessageAction(RegisteredUser $receiver)
    {
        $message = $this->get('rdw_conversation.service.message_manager')->create($receiver, $this->getUser());
        $form = $this->createForm('rdw_message_write', $message);

        return [
            'form' => $form->createView(),
            'receiver' => $receiver,
        ];
    }

    /**
     * @param Request        $request
     * @param RegisteredUser $receiver
     *
     * @Route("/save-message/{id}", requirements={"id" = "\d+"}, name="rdw_conversation.conversation.save_message")
     * @Method({"POST"})
     *
     * @return JsonResponse
     */
    public function saveMessageAction(Request $request, RegisteredUser $receiver)
    {
        $data['success'] = false;
        $response = new JsonResponse();

        $conversation = $this->get('rdw_conversation.service.conversation_manager')->create($receiver, $this->getUser());
        $message = $this->get('rdw_conversation.service.message_manager')->create($receiver, $this->getUser());

        $form = $this->createForm('rdw_message_write', $message);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new MessageEvent($conversation, $message);
            $this->getDispatcher()->dispatch(RDWConversationEvents::MESSAGE_WRITE_SUCCESS, $event);

            $data['success'] = true;

            return $response->setData($data);
        }

        $data['content'] = $this->renderView(
            'RDWConversationBundle:Conversation:_form.html.twig',
            [
                'form' => $form->createView(),
                'receiver' => $receiver,
            ]
        );

        return $response->setData($data);
    }

    /**
     * @param int $id
     *
     * @Route("/delete/{id}", requirements={"id" = "\d+"}, name="rdw_conversation.conversation.delete")
     * @Method({"GET"})
     *
     * @return RedirectResponse
     * @throws NotFoundHttpException
     */
    public function deleteAction($id)
    {
        $user = $this->getUser();
        $conversation = $this->getConversationManager()->findByIdAndUser($id, $user);

        if (!$conversation instanceof Conversation) {
            throw $this->createNotFoundException('The conversation does not exist');
        }

        $this->getConversationManager()->deleteConversation($conversation, $user);

        return $this->redirect($this->generateUrl('rdw_conversation.conversation.inbox'));
    }

    /**
     * @param Request        $request
     * @param RegisteredUser $user
     *
     * @Route("/multiple-delete/{id}", name="rdw_conversation.conversation.multiple_delete", requirements={"id" = "\d+"})
     *
     * @return RedirectResponse
     * @throws AccessDeniedException
     */
    public function multipleDeleteAction(Request $request, RegisteredUser $user)
    {
        $ids = $request->request->get('conversation_collection', ['id' => []])['id'];
        $form = $this->createForm(new ConversationCollectionType($ids));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getConversationManager()->multipleDeleteByUser($form->get('id')->getData(), $user);

            $this->container->get('braincrafted_bootstrap.flash')->success('Successfully deleted');
        }

        return $this->redirect($this->generateUrl('rdw_conversation.conversation.inbox'));
    }

    /**
     * Get messages count badge block
     *
     * @Template("RDWConversationBundle:Conversation:badge.html.twig")
     *
     * @param bool $showZero
     * @return Response|array
     */
    public function getUnreadMessagesCountAction($showZero = false)
    {
        $user = $this->getUser();

        if (! $user instanceof RegisteredUser) {
            return new Response();
        }

        $newConversationsCount = $this->getConversationManager()->getUnreadConversationsCount($user);

        return [
            'count' => $newConversationsCount,
            'showZero' => $showZero,
        ];
    }

    /**
     * @return ConversationManager
     */
    protected function getConversationManager()
    {
        return $this->get('rdw_conversation.service.conversation_manager');
    }

    /**
     * @return \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    protected function getDispatcher()
    {
        return $this->container->get('event_dispatcher');
    }

    /**
     * Extract per page param from query string
     *
     * @return int
     */
    private function getPerPageNumber()
    {
        $current = $this->get('request')->query->get('per_page');
        $options = $this->container->getParameter('rdw_conversation.list.per_page.options');

        $perPage = ($current && in_array($current, $options)) ? $current : $options[0];

        return $perPage;
    }
}
