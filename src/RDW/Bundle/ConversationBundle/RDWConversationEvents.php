<?php

namespace RDW\Bundle\ConversationBundle;

/**
 * Contains all events thrown in the RDWConversationBundle
 *
 * Class RDWConversationEvents
 *
 * @package Conversation
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
final class RDWConversationEvents
{
    /**
     * The MESSAGE_WRITE_SUCCESS event occurs when the User successfully writes new message to another user
     */
    const MESSAGE_WRITE_SUCCESS = 'rdw_conversation.event.message_write_success';

    /**
     * The MESSAGE_REPLY_SUCCESS event occurs when the User successfully replies to conversation
     */
    const MESSAGE_REPLY_SUCCESS = 'rdw_conversation.event.message_reply_success';

    /**
     * The CONVERSATION_VIEW event occurs when the User views conversation
     */
    const CONVERSATION_VIEW = 'rdw_conversation.event.conversation_view';

    /**
     * The SEND_MESSAGE_EMAIL event occurs when the system successfully save new message
     */
    const SEND_MESSAGE_EMAIL = 'rdw_conversation.event.message_send_email';
}
