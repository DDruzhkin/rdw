<?php

namespace RDW\Bundle\ConversationBundle\Form\Type;

/**
 * Class WriteMessageType
 *
 * @package RDW\Bundle\ConversationBundle\Form\Type
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class WriteMessageType extends ReplyFormType
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_message_write';
    }
}
