<?php

namespace RDW\Bundle\ConversationBundle\Form\Type;

use RDW\Bundle\ConversationBundle\Repository\ConversationRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class ConversationCollectionType
 * @package RDW\Bundle\ConversationBundle\Form\Type
 */
class ConversationCollectionType extends AbstractType
{
    /**
     * @var array
     */
    private $items;

    /**
     * @param array $items
     */
    public function __construct(array $items)
    {
        $this->items = $items;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', 'entity', [
            'required' => false,
            'class' => 'RDWConversationBundle:Conversation',
            'property' => 'id',
            'property_path' => '[id]',
            'multiple' => true,
            'expanded' => true,
            'query_builder' => function (ConversationRepository $repository) {
                return $repository
                    ->createQueryBuilder('c')
                    ->where('c.id IN (:ids)')
                    ->setParameter('ids', $this->items);
            },
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
            'csrf_protection' => false,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'conversation_collection';
    }
}
