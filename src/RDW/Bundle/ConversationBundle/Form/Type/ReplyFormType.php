<?php

namespace RDW\Bundle\ConversationBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Conversation form type class
 */
class ReplyFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', 'textarea', [
                'label' => 'Message',
                'required' => false,
                'attr' => ['placeholder' => 'Message placeholder']
            ])
            ->add('submit', 'submit', [
                'label' => 'Reply'
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\ConversationBundle\Entity\Message',
            'validation_groups' => ['reply']
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_message_reply';
    }
}
