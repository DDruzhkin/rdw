<?php

namespace RDW\Bundle\ConversationBundle\Tests\EventListener;

use RDW\Bundle\ConversationBundle\EventListener\WriteListener;
use RDW\Bundle\ConversationBundle\RDWConversationEvents;
use RDW\Bundle\HistoryBundle\Entity\History;
use RDW\Bundle\HistoryBundle\RDWHistoryEvents;

/**
 * Class WriteListenerTest
 *
 * @package RDW\Bundle\ConversationBundle\Tests\EventListener
 */
class WriteListenerTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $dispatcher;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $messageEvent;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $historyEvent;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $historyManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $conversationManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $message;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $conversation;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $user;

    /**
     * @var WriteListener
     */
    private $listener;

    /**
     * Set up
     */
    public function setUp()
    {
        $this->messageEvent = $this->getMockBuilder('RDW\Bundle\ConversationBundle\Event\MessageEvent')
            ->disableOriginalConstructor()->getMock();

        $this->dispatcher = $this->getMockBuilder('Symfony\Component\EventDispatcher\EventDispatcher')
            ->disableOriginalConstructor()->getMock();

        $this->conversationManager = $this->getMockBuilder('RDW\Bundle\ConversationBundle\Service\ConversationManager')
            ->disableOriginalConstructor()->getMock();

        $this->historyEvent = $this->getMockBuilder('RDW\Bundle\HistoryBundle\Event\HistoryEvent')
            ->disableOriginalConstructor()->getMock();

        $this->historyManager = $this->getMockBuilder('RDW\Bundle\HistoryBundle\Service\HistoryManager')
            ->disableOriginalConstructor()->getMock();

        $this->message = $this->getMock('RDW\Bundle\ConversationBundle\Entity\Message');
        $this->conversation = $this->getMock('RDW\Bundle\ConversationBundle\Entity\Conversation');
        $this->user = $this->getMock('RDW\Bundle\UserBundle\Entity\User');

        $this->listener = new WriteListener($this->conversationManager, $this->dispatcher, $this->historyManager);
    }

    /**
     * @test
     * @expectedException \Symfony\Component\Form\Exception\UnexpectedTypeException
     * @expectedExceptionMessage Expected argument of type "RDW\Bundle\ConversationBundle\Entity\Conversation", "NULL" given
     */
    public function it_should_throw_exception_if_conversation_not_exists()
    {
        $this->messageEvent
            ->expects($this->once())
            ->method('getConversation')
            ->willReturn(null);

        $this->listener->onWriteSuccess($this->messageEvent);
    }

    /**
     * @test
     * @expectedException \Symfony\Component\Form\Exception\UnexpectedTypeException
     * @expectedExceptionMessage Expected argument of type "RDW\Bundle\ConversationBundle\Entity\Message", "NULL" given
     */
    public function it_should_throw_exception_if_message_not_exists()
    {
        $this->messageEvent
            ->expects($this->once())
            ->method('getConversation')
            ->willReturn($this->conversation);

        $this->messageEvent
            ->expects($this->once())
            ->method('getMessage')
            ->willReturn(null);

        $this->listener->onWriteSuccess($this->messageEvent);
    }

    /**
     * @test
     */
    public function it_should_call_send_email_event()
    {
        $this->prepareExpectationsForOnWriteSuccess();

        $this->dispatcher->expects($this->exactly(1))
            ->method('dispatch')
            ->with(RDWConversationEvents::SEND_MESSAGE_EMAIL, $this->messageEvent);

        $this->listener->onWriteSuccess($this->messageEvent);
    }

    /**
     * @test
     */
    public function it_should_call_history_log_event()
    {
        $this->prepareExpectationsForOnWriteSuccess();

        $this->historyManager->expects($this->exactly(1))
            ->method('logEvent')
            ->with($this->user, History::ACTION_WRITE, $this->conversation, RDWHistoryEvents::WRITE);

        $this->listener->onWriteSuccess($this->messageEvent);
    }

    /**
     * Prepare expectations for onWriteSuccess method
     */
    private function prepareExpectationsForOnWriteSuccess()
    {
        $this->messageEvent
            ->expects($this->once())
            ->method('getConversation')
            ->willReturn($this->conversation);

        $this->messageEvent
            ->expects($this->once())
            ->method('getMessage')
            ->willReturn($this->message);

        $this->message
            ->expects($this->once())
            ->method('getSender')
            ->willReturn($this->user);
    }
}
