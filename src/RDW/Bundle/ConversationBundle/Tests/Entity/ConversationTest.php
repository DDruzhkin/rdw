<?php

namespace RDW\Bundle\ConversationBundle\Tests\Entity;

use RDW\Bundle\ConversationBundle\Entity\Conversation;

/**
 * Class ConversationTest
 *
 * @package RDW\Bundle\ConversationBundle\Tests\Entity
 */
class ConversationTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function it_should_have_updated_at_not_null_by_default()
    {
        $conversation = new Conversation();

        $this->assertInstanceOf('\DateTime', $conversation->getUpdatedAt());
    }
}
