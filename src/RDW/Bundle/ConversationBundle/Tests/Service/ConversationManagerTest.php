<?php

namespace RDW\Bundle\ConversationBundle\Tests\Service;

use RDW\Bundle\ConversationBundle\Service\ConversationManager;

/**
 * Class ConversationManagerTest
 *
 * @package RDW\Bundle\ConversationBundle\Tests\Service
 */
class ConversationManagerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ConversationManager
     */
    private $manager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $em;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $paginator;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $sender;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $receiver;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $position;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $job;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $apply;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $conversation;

    public function setup()
    {
        $this->em = $this->getMockBuilder('Doctrine\Common\Persistence\ObjectManager')
            ->disableOriginalConstructor()->getMock();
        $this->paginator = $this->getMock('Knp\Component\Pager\Paginator');
        $this->sender = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');
        $this->receiver = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');
        $this->position = $this->getMock('RDW\Bundle\JobBundle\Entity\Position');
        $this->job = $this->getMock('RDW\Bundle\JobBundle\Entity\Job');
        $this->apply = $this->getMock('RDW\Bundle\JobBundle\Entity\Apply');
        $this->conversation = $this->getMock('RDW\Bundle\ConversationBundle\Entity\Conversation');

        $this->manager = new ConversationManager($this->em, $this->paginator);
    }

    /**
     * @test
     */
    public function it_should_add_job_apply()
    {
        $title = 'Conversation title';
        $this->apply->expects($this->once())->method('getJob')->willReturn($this->job);
        $this->job->expects($this->once())->method('getPosition')->willReturn($this->position);
        $this->position->expects($this->once())->method('getTitle')->willReturn($title);
        $this->conversation->expects($this->once())->method('setTitle')->with($title);
        $this->conversation->expects($this->once())->method('setApply')->with($this->apply);
        $this->conversation->expects($this->once())->method('setCreatedAt');

        $this->manager->addJobApply($this->conversation, $this->apply);
    }
}
