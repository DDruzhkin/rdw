<?php

namespace RDW\Bundle\ConversationBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use RDW\Bundle\JobBundle\Entity\Apply;
use RDW\Bundle\CvBundle\Entity\CvMessage;
use RDW\Bundle\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Main conversation class
 *
 * @ORM\Entity(repositoryClass="RDW\Bundle\ConversationBundle\Repository\ConversationRepository")
 * @ORM\Table("conversations")
 *
 * @package Conversation
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class Conversation
{
    const CONVERSATION_STATUS_INBOX = 1;
    const CONVERSATION_STATUS_DELETED = 2;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=false, name="created_at")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true, name="updated_at")
     */
    protected $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\ConversationBundle\Entity\Message", mappedBy="conversation", cascade={"persist"})
     * @ORM\OrderBy({"createdAt" = "desc"})
     */
    protected $messages;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\User", inversedBy="sentConversations")
     * @ORM\JoinColumn(name="sender_id", referencedColumnName="id", nullable=false)
     */
    protected $sender;

    /**
     * @var Apply
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\JobBundle\Entity\Apply", cascade={"persist"}, inversedBy="conversations")
     * @ORM\JoinColumn(name="apply_id", referencedColumnName="id", nullable=true)
     */
    protected $apply;

    /**
     * @var CvMessage
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\CvBundle\Entity\CvMessage", cascade={"persist"})
     * @ORM\JoinColumn(name="cv_message_id", referencedColumnName="id", nullable=true)
     */
    protected $cvMessage;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\User", inversedBy="receivedConversations")
     * @ORM\JoinColumn(name="receiver_id", referencedColumnName="id", nullable=false)
     */
    protected $receiver;

    /**
     * @var
     *
     * @ORM\Column(name="receiver_status", type="smallint", length=2)
     */
    private $receiverStatus;

    /**
     * @var
     *
     * @ORM\Column(name="sender_status", type="smallint", length=2)
     */
    private $senderStatus;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->messages = new ArrayCollection();
        $this->receiverStatus = self::CONVERSATION_STATUS_INBOX;
        $this->senderStatus = self::CONVERSATION_STATUS_INBOX;
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return self
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @param \DateTime $updatedAt
     *
     * @return self
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param User $sender
     *
     * @return self
     */
    public function setSender(User $sender)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * @param User $receiver
     *
     * @return $this
     */
    public function setReceiver(User $receiver)
    {
        $this->receiver = $receiver;

        return $this;
    }

    /**
     * @return User
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @return User
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * @param Message $message
     *
     * @return $this
     */
    public function addMessage(Message $message)
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * @param Message $message
     *
     * @return $this
     */
    public function removeMessage(Message $message)
    {
        $this->messages->removeElement($message);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Set receiverStatus
     *
     * @param int $receiverStatus
     *
     * @return Conversation
     */
    public function setReceiverStatus($receiverStatus)
    {
        $this->receiverStatus = $receiverStatus;

        return $this;
    }

    /**
     * Get receiverStatus
     *
     * @return int
     */
    public function getReceiverStatus()
    {
        return $this->receiverStatus;
    }

    /**
     * Set senderStatus
     *
     * @param int $senderStatus
     *
     * @return Conversation
     */
    public function setSenderStatus($senderStatus)
    {
        $this->senderStatus = $senderStatus;

        return $this;
    }

    /**
     * Get senderStatus
     *
     * @return int
     */
    public function getSenderStatus()
    {
        return $this->senderStatus;
    }

    /**
     * @return \RDW\Bundle\JobBundle\Entity\Apply
     */
    public function getApply()
    {
        return $this->apply;
    }

    /**
     * @param \RDW\Bundle\JobBundle\Entity\Apply $apply
     *
     * @return $this
     */
    public function setApply($apply)
    {
        $this->apply = $apply;

        return $this;
    }

    /**
     * @return \RDW\Bundle\CvBundle\Entity\CvMessage
     */
    public function getCvMessage()
    {
        return $this->cvMessage;
    }

    /**
     * @param \RDW\Bundle\CvBundle\Entity\CvMessage $cvMessage
     *
     * @return $this
     */
    public function setCvMessage($cvMessage)
    {
        $this->cvMessage = $cvMessage;

        return $this;
    }

    /**
     * @return Message
     */
    public function getLastMessage()
    {
        return $this->getMessages()->first();
    }

    /**
     * @return bool
     */
    public function isOneSender()
    {
        $sender = $this->getMessages()->last()->getSender();

        foreach($this->getMessages() as $message) {
            if($sender != $message->getSender()) {
                return false;
            }
        }

        return true;
    }
}
