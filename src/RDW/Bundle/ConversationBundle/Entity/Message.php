<?php

// src/RDW/ConversationBundle/Entity/Message.php
namespace RDW\Bundle\ConversationBundle\Entity;

use RDW\Bundle\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Main message class
 *
 * @ORM\Entity(repositoryClass="RDW\Bundle\ConversationBundle\Repository\MessageRepository")
 * @ORM\Table("messages")
 *
 * @package Conversation
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class Message
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Conversation
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\ConversationBundle\Entity\Conversation", inversedBy="messages")
     * @ORM\JoinColumn(name="conversation_id", referencedColumnName="id")
     */
    protected $conversation;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\User", inversedBy="sentMessages")
     * @ORM\JoinColumn(name="sender_id", referencedColumnName="id")
     */
    protected $sender;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\User", inversedBy="receivedMessages")
     * @ORM\JoinColumn(name="receiver_id", referencedColumnName="id")
     */
    protected $receiver;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Enter your message", groups={"reply"})
     */
    protected $content;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true, name="seen_at")
     */
    protected $seenAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param User $sender
     *
     * @return self
     */
    public function setSender(User $sender)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * @param User $receiver
     *
     * @return $this
     */
    public function setReceiver(User $receiver)
    {
        $this->receiver = $receiver;

        return $this;
    }

    /**
     * @param Conversation $conversation
     *
     * @return $this
     */
    public function setConversation(Conversation $conversation)
    {
        $this->conversation = $conversation;

        return $this;
    }

    /**
     * @return Conversation
     */
    public function getConversation()
    {
        return $this->conversation;
    }

    /**
     * @return User
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @return User
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return self
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @param string $content
     *
     * @return $this
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param \DateTime $seenAt
     *
     * @return self
     */
    public function setSeenAt(\DateTime $seenAt)
    {
        $this->seenAt = $seenAt;

        return $this;
    }

    /**
     * Seen by receiver?
     *
     * @return \DateTime
     */
    public function getSeenAt()
    {
        return $this->seenAt;
    }
}
