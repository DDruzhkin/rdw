<?php

namespace RDW\Bundle\ConversationBundle\Repository;

use Doctrine\ORM\EntityRepository;
use RDW\Bundle\ConversationBundle\Entity\Conversation;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;

/**
 * Class MessageRepository
 *
 * @package RDW\Bundle\ConversationBundle\Repository
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class MessageRepository extends EntityRepository
{
    /**
     * @param Conversation   $conversation
     * @param RegisteredUser $user
     *
     * @return \Doctrine\ORM\Query
     */
    public function updateMessagesAsSeen(Conversation $conversation, RegisteredUser $user)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $seenAt = new \DateTime('now');

        $queryBuilder
            ->update('RDWConversationBundle:Message', 'm')
            ->set('m.seenAt', "'" . $seenAt->format('Y-m-d H:i:s') . "'")
            ->andWhere('m.conversation = :conversation')
            ->andWhere('m.receiver = :receiver')
            ->andWhere($queryBuilder->expr()->isNull('m.seenAt'))
            ->setParameter('conversation', $conversation)
            ->setParameter('receiver', $user);


        return $queryBuilder->getQuery()->execute();
    }
}
