<?php

namespace RDW\Bundle\ConversationBundle\Repository;

use Doctrine\ORM\EntityRepository;
use RDW\Bundle\ConversationBundle\Entity\Conversation;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\CvBundle\Entity\Cv;

/**
 * Class ConversationRepository
 *
 * @package RDW\Bundle\ConversationBundle\Repository
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class ConversationRepository extends EntityRepository
{
    /**
     * @param array $filters
     * @param array $order
     *
     * @return \Doctrine\ORM\Query
     */
    public function getConversationListQuery(array $filters, array $order)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('c')
            ->from('RDWConversationBundle:Conversation', 'c')
            ->leftJoin('c.sender', 'sender')
            ->leftJoin('c.receiver', 'receiver')

            // only between registered users
            ->andWhere('
                sender INSTANCE OF RDW\Bundle\UserBundle\Entity\RegisteredUser
                AND receiver INSTANCE OF RDW\Bundle\UserBundle\Entity\RegisteredUser
            ');

        // sender or receiver
        if (isset($filters['user'])) {
            $queryBuilder
                ->andWhere('(c.sender = :sender OR c.receiver = :receiver)')
                ->setParameter('sender', $filters['user'])
                ->setParameter('receiver', $filters['user']);
        }

        // sender or receiver
        if (isset($filters['cv'])) {
            $queryBuilder
                ->join('c.cvMessage', 'cm')
                ->andWhere('cm.cv = :cv')
                ->setParameter('cv', $filters['cv']);
        }

        // check by status and user
        if ((isset($filters['user']) && $filters['user'] instanceof RegisteredUser) && isset($filters['status'])) {
            $queryBuilder
                ->andWhere('(CASE WHEN (c.sender = :user) THEN c.senderStatus ELSE c.receiverStatus END) = :status')
                ->setParameter('user', $filters['user'])
                ->setParameter('status', $filters['status']);
        }

        if (! empty($order)) {
            foreach ($order as $orderKey => $value) {
                $queryBuilder->orderBy('c.'.$orderKey, $value);
            }
        }

        $queryBuilder->addOrderBy('c.id', 'desc');

        return $queryBuilder->getQuery();
    }

    /**
     * Count unread conversation
     *
     * @param RegisteredUser $user
     *
     * @return int
     */
    public function countUnreadConversations(RegisteredUser $user)
    {
        $queryBuilder = $this->getUnreadConversationsQueryBuilder($user);

        $queryBuilder
            ->select('COUNT(DISTINCT c.id)');

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    /**
     * @param RegisteredUser $user
     * @param int            $limit
     *
     * @return array
     */
    public function getUnreadConversations(RegisteredUser $user, $limit)
    {
        return $this->getUnreadConversationsQueryBuilder($user)->setMaxResults($limit)->getQuery()->getResult();
    }

    /**
     * @param RegisteredUser $user
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getUnreadConversationsQueryBuilder(RegisteredUser $user)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('c')
            ->from('RDWConversationBundle:Conversation', 'c')
            ->leftJoin('c.sender', 'sender')
            ->leftJoin('c.receiver', 'receiver')

            // only between registered users
            ->andWhere('
                sender INSTANCE OF RDW\Bundle\UserBundle\Entity\RegisteredUser
                AND receiver INSTANCE OF RDW\Bundle\UserBundle\Entity\RegisteredUser
            ')

            ->join('c.messages', 'm')
            ->andWhere('m.receiver = :receiver')
            ->andWhere($queryBuilder->expr()->isNull('m.seenAt'))
            ->andWhere('(CASE WHEN (c.sender = :user) THEN c.senderStatus ELSE c.receiverStatus END) = :status')
            ->setParameter('user', $user)
            ->setParameter('status', Conversation::CONVERSATION_STATUS_INBOX)
            ->setParameter('receiver', $user);

        return $queryBuilder;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getJobAppliesListQuery()
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('c', 'j')
            ->from('RDWConversationBundle:Conversation', 'c')
            ->join('c.job', 'j')
            ->leftJoin('c.sender', 'sender')
            ->leftJoin('c.receiver', 'receiver')

            // only between registered users
            ->andWhere('
                sender INSTANCE OF RDW\Bundle\UserBundle\Entity\RegisteredUser
                AND receiver INSTANCE OF RDW\Bundle\UserBundle\Entity\RegisteredUser
            ');

        return $queryBuilder->getQuery();
    }

    /**
     * @param int            $id
     * @param RegisteredUser $user
     *
     * @return \RDW\Bundle\ConversationBundle\Entity\Conversation|null
     */
    public function getConversationByIdAndUser($id, RegisteredUser $user)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('c', 'a')
            ->from('RDWConversationBundle:Conversation', 'c')
            ->leftJoin('c.apply', 'a')
            ->where('c.id = :id')
            ->andWhere('c.sender = :sender OR c.receiver = :receiver')
            ->andWhere('(CASE WHEN (c.sender = :user) THEN c.senderStatus ELSE c.receiverStatus END) = :status')
            ->setParameter('sender', $user)
            ->setParameter('id', $id)
            ->setParameter('receiver', $user)
            ->setParameter('user', $user)
            ->setParameter('status', Conversation::CONVERSATION_STATUS_INBOX)
            ->setMaxResults(1);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    /**
     * @param RegisteredUser $sender
     * @param RegisteredUser $receiver
     *
     * @return array
     */
    public function getConversationsBetweenUsersQuery(RegisteredUser $sender, RegisteredUser $receiver)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('c')
            ->from('RDWConversationBundle:Conversation', 'c')
            ->leftJoin('c.sender', 'sender')
            ->leftJoin('c.receiver', 'receiver')

            // only between registered users
            ->andWhere('
                sender INSTANCE OF RDW\Bundle\UserBundle\Entity\RegisteredUser
                or receiver INSTANCE OF RDW\Bundle\UserBundle\Entity\RegisteredUser
            ')

            ->andWhere($queryBuilder->expr()->orX(
                $queryBuilder->expr()->eq('c.sender', ':sender'),
                $queryBuilder->expr()->eq('c.sender', ':receiver')
            ))
            ->andWhere($queryBuilder->expr()->orX(
                $queryBuilder->expr()->eq('c.receiver', ':receiver'),
                $queryBuilder->expr()->eq('c.receiver', ':sender')
            ))
            ->setParameter('sender', $sender)
            ->setParameter('receiver', $receiver);

        return $queryBuilder->getQuery();
    }

    /**
     * @param Job            $job
     * @param RegisteredUser $user
     *
     * @return \Doctrine\ORM\Query
     */
    public function getJobConversation(Job $job, RegisteredUser $user)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('c')
            ->from('RDWConversationBundle:Conversation', 'c')
            ->leftJoin('c.apply', 'apply')
            ->where('apply.job = :job')
            ->andWhere('apply.user = :user')
            ->setParameter('job', $job)
            ->setParameter('user', $user)
            ->orderBy('c.createdAt', 'DESC')
            ->setMaxResults(1);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    /**
     * @param Cv             $cv
     * @param RegisteredUser $user
     *
     * @return \Doctrine\ORM\Query
     */
    public function getCvConversation(Cv $cv, RegisteredUser $user)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('c')
            ->from('RDWConversationBundle:Conversation', 'c')
            ->leftJoin('c.cvMessage', 'cv')
            ->where('cv.cv = :cv')
            ->andWhere('c.sender = :user')
            ->setParameter('cv', $cv)
            ->setParameter('user', $user)
            ->orderBy('c.createdAt', 'DESC')
            ->setMaxResults(1);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
