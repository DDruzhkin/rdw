<?php

namespace RDW\Bundle\ConversationBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\ConversationBundle\Entity\Conversation;
use RDW\Bundle\ConversationBundle\Entity\Message;
use RDW\Bundle\JobBundle\Entity\Apply;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\User;

/**
 * Class MessageManager
 *
 * @package RDW\Bundle\ConversationBundle\Service
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class MessageManager
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $entityManager;

    /**
     * @param ObjectManager  $entityManager
     */
    public function __construct(ObjectManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Mark all conversation messages as seen by receiver
     *
     * @param Conversation   $conversation
     * @param RegisteredUser $user
     *
     * @return bool
     */
    public function markConversationMessagesAsSeen(Conversation $conversation, RegisteredUser $user)
    {
        $this->getRepository()->updateMessagesAsSeen($conversation, $user);

        return true;
    }

    /**
     * @param User $receiver
     * @param User $sender
     *
     * @return Message
     */
    public function create(User $receiver, User $sender)
    {
        $message = new Message();
        $message->setReceiver($receiver);
        $message->setSender($sender);
        $message->setCreatedAt(new \DateTime());

        return $message;
    }

    /**
     * @param Apply $apply
     *
     * @return Message
     */
    public function createFromJobApply(Apply $apply)
    {
        $message = $this->create($apply->getJob()->getUser(), $apply->getUser());
        $message->setContent($apply->getMessage());

        return $message;
    }

    /**
     * @return \RDW\Bundle\ConversationBundle\Repository\MessageRepository
     */
    private function getRepository()
    {
        return $this->entityManager->getRepository('RDWConversationBundle:Message');
    }
}
