<?php

namespace RDW\Bundle\ConversationBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Knp\Component\Pager\Paginator;
use RDW\Bundle\ConversationBundle\Entity\Conversation;
use RDW\Bundle\ConversationBundle\Entity\Message;
use RDW\Bundle\JobBundle\Entity\Apply;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\User;

/**
 * Class ConversationManager
 *
 * @package RDW\Bundle\ConversationBundle\Service
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class ConversationManager
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $entityManager;

    /**
     * @var \Knp\Component\Pager\Paginator
     */
    private $paginator;

    /**
     * @param ObjectManager $entityManager
     * @param Paginator     $paginator
     */
    public function __construct(ObjectManager $entityManager, Paginator $paginator)
    {
        $this->entityManager = $entityManager;
        $this->paginator = $paginator;
    }

    /**
     * @param User $receiver
     * @param User $sender
     *
     * @return Conversation
     */
    public function create(User $receiver, User $sender)
    {
        $conversation = new Conversation();
        $conversation->setReceiver($receiver);
        $conversation->setSender($sender);

        return $conversation;
    }

    /**
     * @param Conversation $conversation
     * @param Apply        $apply
     *
     * @return Conversation
     */
    public function addJobApply(Conversation $conversation, Apply $apply)
    {
        $conversation->setTitle($apply->getJob()->getPosition()->getTitle());
        $conversation->setApply($apply);
        $conversation->setCreatedAt(new \DateTime());

        return $conversation;
    }

    /**
     * @param Conversation $conversation
     * @param Message      $message
     *
     * @return Conversation
     */
    public function addMessage(Conversation $conversation, Message $message)
    {
        $message->setConversation($conversation);
        $conversation->addMessage($message);
        $conversation->setUpdatedAt(new \DateTime());

        return $conversation;
    }

    /**
     * @param array $filters
     * @param array $order
     * @param int   $page
     * @param int   $limit
     *
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function getPaginatedConversationList(array $filters, array $order, $page, $limit)
    {
        $query = $this->getRepository()->getConversationListQuery($filters, $order);

        $pagination = $this->paginator->paginate($query, $page, $limit);

        return $pagination;
    }

    /**
     * @param RegisteredUser $loggedInUser
     * @param RegisteredUser $otherUser
     * @param int            $page
     * @param int            $limit
     *
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function getPaginatedListBetweenUsers(RegisteredUser $loggedInUser, RegisteredUser $otherUser, $page, $limit)
    {
        $query = $this->getRepository()->getConversationsBetweenUsersQuery($loggedInUser, $otherUser);

        $pagination = $this->paginator->paginate($query, $page, $limit);

        return $pagination;
    }


    /**
     * @param int            $id
     * @param RegisteredUser $user
     *
     * @return \RDW\Bundle\ConversationBundle\Entity\Conversation|null
     */
    public function findByIdAndUser($id, RegisteredUser $user)
    {
        return $this->getRepository()->getConversationByIdAndUser($id, $user);
    }

    /**
     * @param Conversation   $conversation
     *
     * @return bool
     */
    public function save(Conversation $conversation)
    {
        if (Conversation::CONVERSATION_STATUS_DELETED == $conversation->getSenderStatus()) {
            $conversation->setSenderStatus(Conversation::CONVERSATION_STATUS_INBOX);
        }

        if (Conversation::CONVERSATION_STATUS_DELETED == $conversation->getReceiverStatus()) {
            $conversation->setReceiverStatus(Conversation::CONVERSATION_STATUS_INBOX);
        }

        $this->update($conversation);

        return true;
    }

    /**
     * @param Conversation $conversation
     * @param bool         $andFlush
     *
     * @return bool
     */
    public function update(Conversation $conversation, $andFlush = true)
    {
        $this->entityManager->persist($conversation);

        if ($andFlush) {
            $this->entityManager->flush();
        }

        return true;
    }

    /**
     * @param RegisteredUser $user
     *
     * @return int
     */
    public function getUnreadConversationsCount(RegisteredUser $user)
    {
        return $this->getRepository()->countUnreadConversations($user);
    }

    /**
     * @param Apply $apply
     *
     * @return null|object
     */
    public function findByApply(Apply $apply)
    {
        return $this->getRepository()->findOneBy([
            'apply' => $apply,
        ]);
    }

    /**
     * @param Conversation   $conversation
     * @param RegisteredUser $user
     *
     * @return bool
     */
    public function deleteConversation(Conversation $conversation, RegisteredUser $user)
    {
        if ($conversation->getSender()->getId() == $user->getId()) {
            $conversation->setSenderStatus(Conversation::CONVERSATION_STATUS_DELETED);
        } else {
            $conversation->setReceiverStatus(Conversation::CONVERSATION_STATUS_DELETED);
        }

        $this->update($conversation);

        return true;
    }

    /**
     * @param ArrayCollection $conversations
     * @param RegisteredUser  $user
     */
    public function multipleDeleteByUser(ArrayCollection $conversations, RegisteredUser $user)
    {
        foreach ($conversations as $conversation) {
            $this->deleteConversation($conversation, $user);
        }
    }

    /**
     * @return \RDW\Bundle\ConversationBundle\Repository\ConversationRepository
     */
    private function getRepository()
    {
        return $this->entityManager->getRepository('RDWConversationBundle:Conversation');
    }
}
