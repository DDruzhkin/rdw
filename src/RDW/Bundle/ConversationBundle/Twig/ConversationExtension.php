<?php

namespace RDW\Bundle\ConversationBundle\Twig;

use RDW\Bundle\ConversationBundle\Entity\Conversation;
use RDW\Bundle\ConversationBundle\Repository\ConversationRepository;
use RDW\Bundle\ConversationBundle\Entity\Message;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Class ConversationExtension
 *
 * @author Vytautas Beliunas <vytautas@eface.lt>
 */
class ConversationExtension extends \Twig_Extension implements ContainerAwareInterface
{
    /**
     * Container
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('is_conversation_new', [$this, 'isConversationNew']),
            new \Twig_SimpleFunction('did_send_last', [$this, 'didSendLast']),
            new \Twig_SimpleFunction('is_replied', [$this, 'isReplied']),
            new \Twig_SimpleFunction('conversationPanel', [$this, 'getConversationPanel'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * @param Conversation   $conversation
     * @param RegisteredUser $receiver
     *
     * @return bool
     */
    public function isConversationNew(Conversation $conversation, RegisteredUser $receiver)
    {
        /** @var Message $lastMessage */
        $lastMessage = $conversation->getMessages()->first();

        if ($lastMessage->getSender()->getId() != $receiver->getId() && $lastMessage->getSeenAt() === null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param Conversation   $conversation
     * @param RegisteredUser $sender
     *
     * @return bool
     */
    public function didSendLast(Conversation $conversation, RegisteredUser $sender)
    {
        $lastMessage = $conversation->getMessages()->first();

        if ($lastMessage->getSender()->getId() == $sender->getId()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param Conversation   $conversation
     * @param RegisteredUser $sender
     *
     * @return bool
     */
    public function isReplied(Conversation $conversation, RegisteredUser $sender)
    {
        $lastMessage = $conversation->getMessages()->first();

        if (! $conversation->isOneSender() && $lastMessage->getSender()->getId() == $sender->getId()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string $type
     *
     * @return int
     */
    public function getConversationPanel($entity)
    {
        /** @var $token TokenInterface */
        $token = $this->container->get('security.context')->getToken();
        /** @var RegisteredUser $user */
        $user = $token->getUser();

        return ($user->isTypeEmployee()) ? $this->getJobConversation($user, $entity) : $this->getCvConversation($user, $entity);
    }

    /**
     * @param RegisteredUser $registeredUser
     * @param Job  $job
     * @return int
     */
    private function getJobConversation(RegisteredUser $registeredUser, Job $job)
    {
        /** @var $repository ConversationRepository */
        $repository = $this->container
            ->get('doctrine.orm.default_entity_manager')
            ->getRepository('RDWConversationBundle:Conversation');

        return $this->container->get('templating')->render('RDWConversationBundle:Conversation:_job_panel.html.twig', ['conversation' => $repository->getJobConversation($job, $registeredUser)]);
    }

    /**
     * @param RegisteredUser $registeredUser
     * @param Cv             $cv
     * @return int
     */
    private function getCvConversation(RegisteredUser $registeredUser, Cv $cv)
    {
        /** @var $repository ConversationRepository */
        $repository = $this->container
            ->get('doctrine.orm.default_entity_manager')
            ->getRepository('RDWConversationBundle:Conversation');

        return $this->container->get('templating')->render('RDWConversationBundle:Conversation:_job_panel.html.twig', ['conversation' => $repository->getCvConversation($cv, $registeredUser)]);
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'rdw_conversation_extension';
    }
}
