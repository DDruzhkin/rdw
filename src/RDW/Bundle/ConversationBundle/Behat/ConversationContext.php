<?php

namespace RDW\Bundle\ConversationBundle\Behat;

use Behat\Gherkin\Node\TableNode;
use RDW\Bundle\AppBundle\Behat\DefaultContext;
use RDW\Bundle\ConversationBundle\Entity\Conversation;
use RDW\Bundle\ConversationBundle\Entity\Message;
use RDW\Bundle\UserBundle\Entity\User;

/**
 * Class ConversationsContext. Context related with conversations
 */
class ConversationContext extends DefaultContext
{
    /**
     * @param TableNode $table
     *
     * @Given /^there are following conversations:$/
     */
    public function thereAreFollowingConversations(TableNode $table)
    {
        foreach ($table->getHash() as $data) {
            $this->thereIsConversation(
                $data['title'],
                $data['receiver'],
                $data['sender'],
                $data['updatedAt']
            );
        }

        $this->getEntityManager()->flush();
    }

    /**
     * @Given /^I should be on inbox page$/
     */
    public function iShouldBeOnInboxPage()
    {
        $this->assertSession()->addressEquals($this->generateUrl('rdw_conversation.conversation.inbox'));
    }

    /**
     * @param string $title
     *
     * @throws \InvalidArgumentException
     *
     * @Given /^I should be on view conversation page with title "([^"]*)"$/
     */
    public function iShouldBeOnViewConversationPage($title)
    {
        $conversation = $this->findConversationByTitle($title);

        if ($conversation instanceof Conversation) {
            $this->assertSession()->addressEquals($this->generateUrl('rdw_conversation.conversation.view', ['id' => $conversation->getId()]));
        } else {
            throw new \InvalidArgumentException(sprintf('Cannot find conversation with title: "%s"', $title));
        }
    }

    /**
     * @param string $title
     *
     * @throws \InvalidArgumentException
     *
     * @Given /^I should be on reply conversation page with title "([^"]*)"$/
     */
    public function iShouldBeOnReplyConversationPage($title)
    {
        $conversation = $this->findConversationByTitle($title);

        if ($conversation instanceof Conversation) {
            $this->assertSession()->addressEquals($this->generateUrl('rdw_conversation.conversation.reply', ['id' => $conversation->getId()]));
        } else {
            throw new \InvalidArgumentException(sprintf('Cannot find conversation with title: "%s"', $title));
        }
    }

    /**
     * @param string $title
     * @param string $receiverEmail
     * @param string $senderEmail
     * @param string $updatedAt
     * @param bool   $flush
     *
     * @return Conversation
     */
    public function thereIsConversation($title, $receiverEmail, $senderEmail, $updatedAt = 'now', $flush = true)
    {
        $conversation = new Conversation();
        $conversation->setTitle($title);
        $conversation->setCreatedAt(new \DateTime());
        $conversation->setUpdatedAt(new \DateTime($updatedAt));
        $conversation->setReceiverStatus(Conversation::CONVERSATION_STATUS_INBOX);
        $conversation->setSenderStatus(Conversation::CONVERSATION_STATUS_INBOX);

        $receiver = $this->getUserManager()->findUserByEmail($receiverEmail);
        $sender = $this->getUserManager()->findUserByEmail($senderEmail);

        if ($receiver instanceof User and $sender instanceof User) {
            $conversation->setReceiver($receiver);
            $conversation->setSender($sender);

            //add some messages
            $numOfMessagePerConversation = rand(1, 5);
            for ($y = 1; $y <= $numOfMessagePerConversation; $y++) {
                $message = new Message();

                $message->setSender($receiver);
                $message->setReceiver($sender);

                $message->setConversation($conversation);
                $message->setContent($this->faker->text(rand(20, 2000)));
                $message->setCreatedAt(new \DateTime());

                $this->getEntityManager()->persist($message);
            }

            $this->getEntityManager()->persist($conversation);

            if ($flush) {
                $this->getEntityManager()->flush();
            }
        }

        return $conversation;
    }

    /**
     * @return \RDW\Bundle\UserBundle\Service\UserManager
     */
    protected function getUserManager()
    {
        return $this->getService('rdw_user.service.user_manager');
    }

    /**
     * @param string $title
     *
     * @return Conversation|null
     */
    protected function findConversationByTitle($title)
    {
        return $this->getEntityManager()->getRepository('RDWConversationBundle:Conversation')->findOneBy(['title' => $title]);
    }
}
