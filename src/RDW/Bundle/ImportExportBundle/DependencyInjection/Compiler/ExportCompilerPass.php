<?php

namespace RDW\Bundle\ImportExportBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

class ExportCompilerPass implements CompilerPassInterface
{
    use CompilerPassTrait;

    /**
     * @return string
     */
    public function getRegistryName()
    {
        return 'rdw_import_export.registry';
    }

    /**
     * @return string
     */
    public function getServiceTag()
    {
        return 'rdw_export.processor';
    }

    /**
     * @return string
     */
    public function getServiceMethod()
    {
        return 'addExportProcessor';
    }
}
