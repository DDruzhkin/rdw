<?php

namespace RDW\Bundle\ImportExportBundle;

use RDW\Bundle\ImportExportBundle\DependencyInjection\Compiler\ExportCompilerPass;
use RDW\Bundle\ImportExportBundle\DependencyInjection\Compiler\ImportCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class RDWImportExportBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new ImportCompilerPass());
        $container->addCompilerPass(new ExportCompilerPass());
    }
}
