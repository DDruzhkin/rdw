<?php

namespace RDW\Bundle\ImportExportBundle\Registry;

interface ProcessorInterface
{
    /**
     * @return string
     */
    public function getDescription();
}
