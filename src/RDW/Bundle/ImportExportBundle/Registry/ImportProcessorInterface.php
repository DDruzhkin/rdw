<?php

namespace RDW\Bundle\ImportExportBundle\Registry;

use Psr\Log\LoggerAwareInterface;
use RDW\Bundle\ImportExportBundle\Model\ImportModelInterface;

interface ImportProcessorInterface extends ProcessorInterface, LoggerAwareInterface
{
    /**
     * Returns an array of models to import
     *
     * @param mixed $source
     *
     * @return ImportModelInterface[]
     */
    public function processSource($source);

    /**
     * Returns Entity
     *
     * @param ImportModelInterface $record
     *
     * @return mixed
     */
    public function processRecord(ImportModelInterface $record);
}
