<?php

namespace RDW\Bundle\SlugBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Class SlugRepository
 *
 * @package RDW\Bundle\SlugBundle\Entity
 */
class SlugRepository extends EntityRepository
{
    /**
     * @param string $slug
     *
     * @return null|object
     */
    public function getBySlug($slug)
    {
        return $this->findOneBy(['name' => $slug]);
    }

    /**
     * @param string $slug
     *
     * @return bool
     */
    public function slugExists($slug)
    {
        $slugEnt = $this->getBySlug($slug);

        return (empty($slugEnt)) ? false : true;
    }

    /**
     * @param int    $id
     * @param string $type
     *
     * @return null|object
     */
    public function getByIdAndType($id, $type)
    {
        $slug = $this->findOneBy(['itemId' => (int) $id, 'type' => $type, 'active' => 1]);

        return ( ! empty($slug)) ? $slug : null;
    }
}
