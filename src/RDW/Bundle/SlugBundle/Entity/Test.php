<?php

namespace RDW\Bundle\SlugBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\SlugBundle\Annotations as SlugAno;

/**
 * @ORM\Table(name="test_listeners")
 * @ORM\Entity()
 * @SlugAno\Slug(fields={"name", "id"})
 */
class Test
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     * @ORM\Column(name="name", type="string")
     */
    private $name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Test
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}
