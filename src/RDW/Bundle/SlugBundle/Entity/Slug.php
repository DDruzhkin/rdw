<?php

namespace RDW\Bundle\SlugBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Slug
 *
 * @package RDW\Bundle\SlugBundle\Entity
 *
 * @ORM\Table(
 *      name="slugs",
 *      indexes={
 *          @ORM\Index(name="item_id_idx", columns={"item_id"}),
 *          @ORM\Index(name="item_id_type_idx", columns={"item_id", "type"}),
 *          @ORM\Index(name="name_idx", columns={"name"})
 *      },
 *      uniqueConstraints={
 *          @ORM\UniqueConstraint(name="type_name_unq", columns={"type", "name", "is_active"})
 *      }
 * )
 * @ORM\Entity(repositoryClass="RDW\Bundle\SlugBundle\Entity\SlugRepository")
 * @UniqueEntity(
 *      fields={"name", "type", "active"},
 *      errorPath="name",
 *      message="This url slug is not available",
 *      groups={"personalize"}
 * )
 */
class Slug
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer $itemId
     *
     * @ORM\Column(name="item_id", type="integer", nullable=true)
     */
    private $itemId;

    /**
     * @var string $type
     *
     * @ORM\Column(name="type", type="string", length=100, nullable=true)
     */
    private $type;

    /**
     * @var string $controller
     *
     * @ORM\Column(name="controller", type="string", length=100, nullable=true)
     */
    private $controller;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     * @Assert\NotBlank(message="Slug cannot be empty", groups={"personalize"})
     */
    private $name;

    /**
     * @var string $name
     *
     * @ORM\Column(name="orig_name", type="string", length=255)
     */
    private $origName;

    /**
     * @var boolean $active
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $active = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_system", type="boolean")
     */
    private $system;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->system = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set itemId
     *
     * @param integer $itemId
     *
     * @return $this
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;

        return $this;
    }

    /**
     * Get itemId
     *
     * @return integer
     */
    public function getItemId()
    {
        return $this->itemId;
    }


    /**
     * Set type
     *
     * @param string $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set is_active
     *
     * @param boolean $isActive
     *
     * @return $this
     */
    public function setActive($isActive)
    {
        $this->active = $isActive;

        return $this;
    }

    /**
     * Get is_active
     *
     * @return boolean
     */
    public function isActive()
    {
        return $this->getActive();
    }

    /**
     * Set origName
     *
     * @param string $origName
     *
     * @return $this
     */
    public function setOrigName($origName)
    {
        $this->origName = $origName;

        return $this;
    }

    /**
     * Get origName
     *
     * @return string
     */
    public function getOrigName()
    {
        return $this->origName;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Get is system
     *
     * @return boolean
     */
    public function getSystem()
    {
        return $this->system;
    }

    /**
     * @param bool $system
     *
     * @return $this
     */
    public function setSystem($system)
    {
        $this->system = $system;

        return $this;
    }

    /**
     * Set controller
     *
     * @param string $controller
     *
     * @return Slug
     */
    public function setController($controller)
    {
        $this->controller = $controller;

        return $this;
    }

    /**
     * Get controller
     *
     * @return string
     */
    public function getController()
    {
        return $this->controller;
    }
}
