<?php

namespace RDW\Bundle\SlugBundle\Annotations;

use Doctrine\Common\Annotations\Annotation;

/**
 * Class Slug
 *
 * @Annotation
 *
 * @package RDW\Bundle\SlugBundle\Annotations
 */
class Slug extends Annotation
{
    public $fields = array();
}
