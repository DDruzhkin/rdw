<?php

namespace RDW\Bundle\SlugBundle\Annotations\Driver;

use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerAware;

/**
 * Class SlugDriver
 *
 * @package RDW\Bundle\SlugBundle\Annotations\Driver
 */
class SlugDriver extends ContainerAware
{
    /**
     * @var Reader
     */
    private $reader;

    /**
     * @param Reader $reader
     */
    public function __construct(Reader $reader)
    {
        // initialise Doctrine Reader
        $this->reader = $reader;
    }

    /**
     * @param LifecycleEventArgs $event
     */
    public function postPersist(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();

        // get Reflection of Class
        $reflectionProperties = new \ReflectionClass($entity);

        $classProperties = $this->reader->getClassAnnotation(
            $reflectionProperties,
            "RDW\\Bundle\\SlugBundle\\Annotations\\Slug"
        );

        $retValue = [];

        if ( ! empty($classProperties->fields)) {
            foreach ($classProperties->fields as $fieldName) {
                $getter = "get" . ucfirst($fieldName);
                $fieldValue = $entity->{$getter}();

                if ( ! empty($fieldValue)) {
                    $retValue[] = $fieldValue;
                }
            }
        } else {
            return;
        }

        $retString = implode(" ", $retValue);

        $this->container->get('slug_service')->saveSlug($entity->getId(), get_class($entity), $retString, false);
    }

    /**
     * @param LifecycleEventArgs $event
     */
    public function postUpdate(LifecycleEventArgs $event)
    {
        $this->postPersist($event);
    }
}
