<?php

namespace RDW\Bundle\SlugBundle\Tests\Entity;

use RDW\Bundle\SlugBundle\Service\SlugService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SlugTest extends WebTestCase
{

    public function testSlugify()
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $equal = "pivo-vodka-dizel-benzin-testovyy-avtomobil-voyna";
        $source = "пиво водка дизель бензин тестовый автомобиль война";
        $result = $container->get('slug_service')->slugify($source);

        $this->assertEquals($equal, $result);
    }
}
