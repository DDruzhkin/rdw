<?php

namespace RDW\Bundle\SlugBundle\Service;

use Cocur\Slugify\Slugify;
use Doctrine\Common\Util\ClassUtils;
use RDW\Bundle\SlugBundle\Entity\Slug;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;
use RDW\Bundle\AppBundle\Entity\Transliterator;

/**
 * Class SlugService
 *
 * @package RDW\Bundle\SlugBundle\Service
 */
class SlugService extends ContainerAware
{
    /**
     * @return \Symfony\Component\DependencyInjection\ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param string $text
     * @param string $separator
     *
     * @return string
     */
    public function slugify($text, $separator = "-")
    {
        return Transliterator::transliterate($text, $separator);
    }

    /**
     * @param int    $objectId
     * @param string $objectType
     * @param string $slugText
     */
    public function saveSlug($objectId, $objectType, $slugText, $flush = true)
    {
        $slugText = $this->slugify($slugText);
        $slugOrigText = $slugText;
        $noChanges = false;

        $slugInfo = $this->getContainer()->get('doctrine')
            ->getRepository('RDWSlugBundle:Slug')
            ->findOneBy(['name' => $slugText]);

        if ($slugInfo && ($slugInfo->getItemId() != $objectId || $slugInfo->getType() != $objectType)) {
            $slugText = $this->checkAndFixSlug($slugText, $objectId, $objectType);
        } elseif ($slugInfo && $slugInfo->getItemId() == $objectId && $slugInfo->getType() == $objectType) {
            $noChanges = true;
        }

        if ( ! $noChanges) {
            $this->deactivateOlderSlugs($objectId, $objectType, $slugText, $flush);

            $slugEnt = new Slug();
            $slugEnt->setName($slugText)
                ->setOrigName($slugOrigText)
                ->setType($objectType)
                ->setActive(true)
                ->setItemId($objectId);

            $em = $this->getContainer()->get('doctrine')->getManager();
            $em->persist($slugEnt);
            if ($flush) {
                $em->flush();
            }
        }
    }

    /**
     * Older slugs must be either deactivated - or if they are the same - left alone :D
     *
     * @param int    $objectId
     * @param string $objectType
     * @param string $slugText
     */
    private function deactivateOlderSlugs($objectId, $objectType, $slugText = "", $flush = true)
    {
        $item = $this->getContainer()->get('doctrine')
            ->getRepository('RDWSlugBundle:Slug')
            ->getByIdAndType($objectId, $objectType);

        if ($item && $item->getName() != $slugText) {
            $item->setActive(false);
            if ($flush) {
                $this->getContainer()->get('doctrine')->getManager()->flush();
            }
        }
    }

    /**
     * @param string $slug
     * @param int    $objectId
     * @param string $objectType
     *
     * @return string
     */
    private function checkAndFixSlug($slug, $objectId, $objectType)
    {
        $newSlug = $slug;
        $i = 0;

        while ($this->checkIfExists($newSlug)) {

            if ($i > 0) {
                $newSlug = $slug . '-' . $i;
            }

            $slugInfo = $this->getContainer()->get('doctrine')
                ->getRepository('RDWSlugBundle:Slug')
                ->findOneBy(['name' => $slug]);

            $i++;

            if ($slugInfo->getItemId() == $objectId && $slugInfo->getType() == $objectType) {
                break;
            }
        }

        return $newSlug;
    }

    /**
     * Slug exists. Just slug :)
     *
     * @param string $slug
     *
     * @return bool
     */
    private function checkIfExists($slug)
    {
        return $this->getContainer()->get('doctrine')->getRepository('RDWSlugBundle:Slug')->slugExists($slug);
    }

    /**
     * Get slug related information
     *
     * @param string $slug
     *
     * @return Slug
     */
    public function getSlugInfo($slug)
    {
        return $this->getContainer()->get('doctrine')->getRepository('RDWSlugBundle:Slug')->getBySlug($slug);
    }

    /**
     * @param object $object
     * @param bool   $returnAsObject
     *
     * @return null|Slug|string
     */
    public function getSlug($object, $returnAsObject = false)
    {
        $slug = $this->getContainer()->get('doctrine')
            ->getRepository('RDWSlugBundle:Slug')
            ->getByIdAndType($object->getId(), ClassUtils::getClass($object));

        if ( ! $slug instanceof Slug) {

            return null;
        }

        return ($returnAsObject) ? $slug : $slug->getName();
    }

    /**
     * Get slug from pathInfo
     *
     * @param string $pathInfo
     *
     * @return mixed|string
     */
    public function getSlugPartFromPathInfo($pathInfo)
    {
        $urlParts = explode("/", $pathInfo);
        $lastPart = end($urlParts);

        if (sizeof($urlParts) > 2) {
            if ( ! empty($lastPart)) {
                $slug = $urlParts[2];
            } else {
                $slug = $urlParts[1];
            }
        } else {
            $slug = $lastPart;
        }

        return $slug;
    }

    /**
     * @param Slug $slugRow
     *
     * @return string
     * @throws ParameterNotFoundException
     */
    public function getController(Slug $slugRow)
    {
        if ($slugRow->getController()) {
            return $slugRow->getController();
        }

        if ( ! $this->getContainer()->hasParameter($slugRow->getType())) {
            throw new ParameterNotFoundException($slugRow->getType()." has no Controller specified");
        }

        return $this->getContainer()->getParameter($slugRow->getType());
    }
}
