<?php

namespace RDW\Bundle\SlugBundle\Twig;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Routing\Router;

/**
 * Class SlugExtension
 *
 * @package RDW\Bundle\SlugBundle\Twig
 */
class SlugExtension extends \Twig_Extension
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var Router
     */
    private $router;

    /**
     * @param Container $container
     */
    public function setContainer(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @param Router $router
     */
    public function setRouter(\Symfony\Cmf\Component\Routing\ChainRouter $router)
    {
        $this->router = $router;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('slug_url', array($this, 'slugUrlFilter')),
            new \Twig_SimpleFilter('slug', array($this, 'slugFilter'))
        );
    }

    /**
     * @param object $object
     *
     * @return string
     */
    public function slugUrlFilter($object)
    {
        return $this->router->generate(
            'rdw_slugs.slugs.slug', [
                'slug' => $this->container->get('slug_service')->getSlug($object)
            ]
        );
    }

    /**
     * @param object $object
     *
     * @return null|\RDW\Bundle\SlugBundle\Entity\Slug|string
     */
    public function slugFilter($object)
    {
        return $this->container->get('slug_service')->getSlug($object);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'slug_extension';
    }
}
