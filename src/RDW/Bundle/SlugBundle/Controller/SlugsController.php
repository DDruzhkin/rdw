<?php

namespace RDW\Bundle\SlugBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class SlugsController
 *
 * @package RDW\Bundle\SlugBundle\Controller
 */
class SlugsController extends Controller
{
    /**
     * @param string $slug
     *
     * @Route("/{slug}", name="rdw_slugs.slugs.slug")
     *
     * @return Response
     */
    public function slugAction($slug)
    {
        $slugRow = $this->get('slug_service')->getSlugInfo($slug);

        if (empty($slugRow)) {
            throw new NotFoundHttpException();
        }

        $controller = $this->container->get('slug_service')->getController($slugRow);

        return $this->forward($controller, ['id' => $slugRow->getItemId(), 'slug' => $slugRow->getName()]);
    }
}
