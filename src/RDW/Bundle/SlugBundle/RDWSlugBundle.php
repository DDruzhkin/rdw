<?php

namespace RDW\Bundle\SlugBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class RDWSlugBundle
 *
 * @package RDW\Bundle\SlugBundle
 */
class RDWSlugBundle extends Bundle
{

}
