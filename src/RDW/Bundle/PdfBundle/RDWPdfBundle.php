<?php

namespace RDW\Bundle\PdfBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class RDWPdfBundle
 *
 * @package RDW\Bundle\PdfBundle
 */
class RDWPdfBundle extends Bundle
{
}
