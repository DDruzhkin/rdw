<?php

namespace RDW\Bundle\PdfBundle\Generator;

use Knp\Bundle\SnappyBundle\Snappy\LoggableGenerator;
use Knp\Snappy\GeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Log\LoggerInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class PdfGenerator
 *
 * @package RDW\Bundle\PdfBundle\Generator
 * @author  Giedrius Sabaliauskas <gs@nebijokit.lt>
 */
class PdfGenerator extends LoggableGenerator
{
    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param GeneratorInterface  $generator
     * @param LoggerInterface     $logger
     * @param EngineInterface     $templating
     * @param TranslatorInterface $translator
     */
    public function __construct(
        GeneratorInterface $generator,
        LoggerInterface $logger = null,
        EngineInterface $templating,
        TranslatorInterface $translator
    )
    {
        $this->templating = $templating;
        $this->translator = $translator;

        parent::__construct($generator, $logger);
    }

    /**
     * @param string $view
     * @param array  $data
     *
     * @return Response
     */
    public function getResponse($view, $data)
    {
        $this->setOption('footer-center', $this->translator->trans('Page') . ' [page]');

        $html = $this->templating->render($view, $data);

        return new Response($this->getOutputFromHtml($html), 200, [
            'Content-Type' => 'application/pdf',
        ]);
    }
}
