<?php

namespace RDW\Bundle\PdfBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class UrlExtension
 *
 * @package RDW\Bundle\PdfBundle\Twig
 */
class UrlExtension extends \Twig_Extension implements ContainerAwareInterface
{
    /**
     * Container
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('printable_url', [$this, 'getPrintableUrl']),
        ];
    }

    /**
     * @param string $route
     * @param array  $params
     *
     * @return bool
     */
    public function getPrintableUrl($route, array $params = [])
    {
        return sprintf('/pdf/export.php?slug=%s', urlencode($this->container->get('router')->generate($route, $params)));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'printable_url_extension';
    }
}
