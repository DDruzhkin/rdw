<?php

namespace RDW\Bundle\PdfBundle\EventListener;

use Knp\Snappy\GeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ControllerReference;
use Symfony\Component\HttpKernel\Fragment\FragmentHandler;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Knp\Snappy\Pdf;
use Symfony\Component\Process\Exception\ProcessTimedOutException;

/**
 * Class PdfListener
 *
 * @package RDW\Bundle\PdfBundle\EventListener
 */
class PdfListener
{
    /**
     * @var GeneratorInterface
     */
    private $generator;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var FragmentHandler
     */
    private $fragmentHandler;

    /**
     * @var string
     */
    private $urlParamKey;

    /**
     * @var string
     */
    private $urlParamValue;

    /**
     * @param GeneratorInterface $generator
     * @param Router             $router
     * @param FragmentHandler    $fragmentHandler
     * @param string             $urlParamKey
     * @param string             $urlParamValue
     */
    public function __construct(GeneratorInterface $generator, Router $router, FragmentHandler $fragmentHandler, $urlParamKey, $urlParamValue)
    {
        $this->generator = $generator;
        $this->router = $router;
        $this->fragmentHandler = $fragmentHandler;
        $this->urlParamKey = $urlParamKey;
        $this->urlParamValue = $urlParamValue;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $this->before($event);
    }

    /**
     * @param GetResponseEvent $event
     *
     * @return null|void
     */
    public function before(GetResponseEvent $event)
    {
        if (HttpKernel::MASTER_REQUEST != $event->getRequestType()) {
            // don't do anything if it's not the master request
            return null;
        }

        $request = $event->getRequest();

        if ($this->urlParamValue != $request->query->get($this->urlParamKey)) {
            return null;
        }

        $pdf = new Pdf('/usr/local/bin/wkhtmltopdf-amd64');
        $pdf->setOption('print-media-type', true);

        $key = implode("=", [$this->urlParamKey, $this->urlParamValue]);

        $uri = str_replace($key, '', $event->getRequest()->getUri());

        // $pdfOptions = [
        //     // 'viewport-size' => '1280x1024',
        //     // 'load-error-handling' => 'ignore',
        //     'disable-internal-links' => true
        // ];

        /* if user has session - pass it */
        if ($event->getRequest()->hasSession() && $event->getRequest()->getSession()->getId()) {
            $session = $event->getRequest()->getSession();
            $pdfOptions['cookie'] = [$session->getName() => $session->getId()];
        }

        $pdf->setTimeout(30);

        $response = new Response(
                $pdf->getOutput($uri, $pdfOptions),
                200,
                ['Content-Type' => 'application/pdf']
        );

        return $event->setResponse($response);
    }
}
