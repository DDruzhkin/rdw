<?php

namespace RDW\Bundle\SubscriptionBundle\Controller;

use RDW\Bundle\AppBundle\Event\GenericGuestEvent;
use RDW\Bundle\SubscriptionBundle\Form\Type\NewsletterFormType;
use RDW\Bundle\SubscriptionBundle\Entity\Subscription;
use RDW\Bundle\SubscriptionBundle\Form\Type\NewsletterSubscriptionFormType;
use RDW\Bundle\SubscriptionBundle\Form\Type\SubscriptionCollectionType;
use RDW\Bundle\SubscriptionBundle\Form\Type\SubscriptionFormType;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


/**
 * Class SubscriptionController
 *
 * @package RDW\Bundle\SubscriptionBundle\Controller
 *
 * @Route("/subscription")
 */
class SubscriptionController extends Controller
{
    /**
     * @Route("/list", name="rdw_subscription.subscription.list")
     * @Template()
     * @Security("has_role('ROLE_REGISTERED')")
     *
     * @return array
     */
    public function listAction()
    {
        $user = $this->getUser();

        $newsletter = $this->get('rdw_subscription.service.newsletter_manager')->getNewsletterByEmail($user->getEmail());
        $newsletterForm = $this->createForm(new NewsletterSubscriptionFormType(), null, ['newsletter' => $newsletter]);

        $subscriptions = $this
            ->getDoctrine()->getRepository('RDWSubscriptionBundle:Subscription')
            ->findBy(['user' => $user], ['subscribedAt' => 'DESC']);

        $form = $this->createForm(new SubscriptionCollectionType($subscriptions));

        return [
            'newsletterForm' => $newsletterForm->createView(),
            'form' => $form->createView(),
            'subscriptions' => $subscriptions,
        ];
    }

    /**
     * @param Request $request
     *
     * @Route("/change-subscription", name="rdw_subscription.subscription.change_subscription")
     * @Method({"POST"})
     * @Template()
     *
     * @return RedirectResponse
     */
    public function changeSubscriptionAction(Request $request)
    {
        $user = $this->getUser();
        $newsletter = $this->get('rdw_subscription.service.newsletter_manager')->getNewsletterByEmail($user->getEmail());
        $subscriptionForm = $this->createForm(new NewsletterSubscriptionFormType(), null, ['newsletter' => $newsletter]);

        $subscriptionForm->handleRequest($request);

        if ($subscriptionForm->isSubmitted() && $subscriptionForm->isValid()) {
            $subscription = $subscriptionForm->get('subscribe')->getData();
            $this->get('rdw_subscription.service.newsletter_manager')->changeSubscription($user, $subscription, $newsletter);
        }

        return $this->redirect($this->generateUrl('rdw_subscription.subscription.list'));
    }

    /**
     * @param Request $request
     * @param User    $user
     * @param string  $type
     * @param int     $objectId
     *
     * @Route(
     *      "/subscribe/{type}/{user_id}/{objectId}",
     *      name="rdw_subscription.subscription.subscribe",
     *      defaults={"objectId"="null", "user_id"=null},
     *      requirements={"type" = "job|cv", "user_id" = "\d+"}
     * )
     * @ParamConverter("user", class="RDWUserBundle:User", options={"id" = "user_id"})
     * @Method({"GET"})
     * @Template()
     *
     * @return RedirectResponse
     */
    public function subscribeAction(Request $request, $type, User $user = null, $objectId = null)
    {
        if (! $user) {
            $user = $this->getUser();
        }

        $filterKey = ($type == Subscription::TYPE_CV ? '_subscription_cv_filter' : 'job_filter');
        $filter = $request->getSession()->get($filterKey);
        $subscriptionManager = $this->get('rdw_subscription.service.subscription_manager');

        $subscription = $subscriptionManager->create($type, $user);
        $subscription = $this->mapSubscription($subscription, $filter, $objectId);

        $form = $this->createForm(new SubscriptionFormType(), $subscription);

        $data = [
            'form' => $form->createView(),
            'type' => $type,
            'objectId' => $objectId,
            'userType' => ($type == Subscription::TYPE_CV) ? RegisteredUser::USER_TYPE_EMPLOYER : RegisteredUser::USER_TYPE_EMPLOYEE,
        ];

        return $data;
    }

    /**
     * @param string $token
     *
     * @Route("/unsubscribe/{token}", name="rdw_subscription.subscription.unsubscribe")
     * @Method({"GET"})
     *
     * @return RedirectResponse
     */
    public function unsubscribeAction($token)
    {
        $subscription = $this->getDoctrine()
            ->getRepository('RDWSubscriptionBundle:Subscription')
            ->findOneBy(['token' => $token]);

        if ($subscription) {
            $this->get('rdw_subscription.service.subscription_manager')->removeSubscription($subscription);
            $this->get('braincrafted_bootstrap.flash')->success('Your subscription was successfully canceled');
        }

        return $this->redirect($this->generateUrl('rdw_app.default.index'));
    }

    /**
     * @param Request $request
     *
     * @Route("/newsletter", name="rdw_subscription.subscription.newsletter")
     * @Method({"GET", "POST"})
     * @Template()
     *
     * @return Response
     */
    public function newsletterAction(Request $request)
    {
        $user = $this->getUser();
        $newsletterManager = $this->get('rdw_subscription.service.newsletter_manager');
        $newsletter = $newsletterManager->createNewsletterFromUser($user);
        $form = $this->createForm(new NewsletterFormType(), $newsletter);

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @param Request $request
     *
     * @Route("/newsletter/save", name="rdw_subscription.subscription.save_newsletter")
     * @Method({"GET", "POST"})
     * @Template()
     *
     * @return Response
     */
    public function saveNewsletterAction(Request $request)
    {
        $data = [];
        $data['success'] = false;

        $user = $this->getUser();
        $newsletterManager = $this->get('rdw_subscription.service.newsletter_manager');
        $newsletter = $newsletterManager->createNewsletterFromUser($user);
        $form = $this->createForm(new NewsletterFormType(), $newsletter);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data['updated'] = $newsletterManager->update($newsletter);
            $this->get('braincrafted_bootstrap.flash')->success('You successfully subscribe newsletter');
            $data['success'] = true;
        }


        $data['content'] = $this->renderView('RDWSubscriptionBundle:Subscription:_newsletter_form.html.twig', [
            'form' => $form->createView(),
        ]);

        $response = new JsonResponse();
        $response->setData($data);

        return $response;

    }

    /**
     * @param Request $request
     * @param string  $type
     * @param int     $objectId
     *
     * @Route(
     *      "/subscribe/{type}/save/{objectId}",
     *      name="rdw_subscription.subscription.save_subscription",
     *      requirements={"type" = "job|cv"}
     * )
     * @Method({"POST"})
     * @Template("RDWSubscriptionBundle:Subscription:subscribe.html.twig")
     *
     * @return JsonResponse
     */
    public function saveSubscriptionAction(Request $request, $type, $objectId = null)
    {
        // $this->isGranted($user);

        $data = [];
        $data['success'] = true;

        $filterKey = ($type == Subscription::TYPE_CV ? '_subscription_cv_filter' : 'job_filter');
        $filter = $request->getSession()->get($filterKey);
        $subscriptionManager = $this->get('rdw_subscription.service.subscription_manager');

        $subscription = $subscriptionManager->create($type, $this->getUser());
        $subscription = $this->mapSubscription($subscription, $filter, $objectId);

        $form = $this->createForm(new SubscriptionFormType(), $subscription);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $request->getSession()->set($filterKey, $filter);

            $event = new GenericGuestEvent($request, $subscription);
            $this->container->get('event_dispatcher')->dispatch('rdw_app.subscription.pre_create', $event);

            if ($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
                $subscription->setUser($this->getUser());

                $data['updated'] = $subscriptionManager->update($subscription);

                $this->get('braincrafted_bootstrap.flash')->success('You successfully subscribe new subscription');
            }

            $data['success'] = true;
        } else {
            $data['content'] = $this->renderView('RDWSubscriptionBundle:Subscription:_subscribe_form.html.twig', [
                'form' => $form->createView(),
                'action' => $this->generateUrl('rdw_subscription.subscription.save_subscription', ['type' => $type, 'objectId' => $objectId]),
                'method' => 'POST',
            ]);

            $data['success'] = false;
        }

        $response = new JsonResponse();
        $response->setData($data);

        return $response;
    }

    protected function mapSubscription(Subscription $subscription, $filter, $objectId)
    {
        $subscriptionFieldManager = $this->get('rdw_subscription.service.subscription_field_mapper');

        if ($objectId) {
            $subscription = $subscriptionFieldManager->mapSubscriptionWithObject($subscription, $objectId);
        } else {
            $subscription = $subscriptionFieldManager->mapSubscriptionWithFilterValues($subscription, $filter, $objectId);
        }

        return $subscription;
    }

    /**
     * @param Subscription $subscription
     *
     * @Route(
     *      "/subscribtion/edit/{id}",
     *      name="rdw_subscription.subscription.edit",
     *      requirements={"id" = "\d+"}
     * )
     * @ParamConverter("subscription", class="RDWSubscriptionBundle:Subscription")
     * @Method({"GET"})
     * @Template("RDWSubscriptionBundle:Subscription:edit.html.twig")
     *
     * @return array
     */
    public function editSubscriptionAction(Subscription $subscription)
    {
        $this->isGrantedLocal($subscription->getUser());

        $form = $this->createForm(new SubscriptionFormType(), $subscription);

        $data = [
            'subscription' => $subscription,
            'form' => $form->createView(),
        ];

        return $data;
    }

    /**
     * @param Request $request
     * @param Subscription $subscription
     * @Route(
     *      "/subscribtion/update/{id}",
     *      name="rdw_subscription.subscription.update_subscription",
     *      requirements={"id" = "\d+"}
     * )
     * @ParamConverter("subscription", class="RDWSubscriptionBundle:Subscription")
     * @Method({"POST"})
     * @Template("RDWSubscriptionBundle:Subscription:edit.html.twig")
     *
     * @return JsonResponse
     */
    public function updateSubscriptionAction(Request $request, Subscription $subscription)
    {
        $this->isGrantedLocal($subscription->getUser());

        $data = [];
        $data['success'] = false;

        $form = $this->createForm(new SubscriptionFormType(), $subscription);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data['updated'] = $this->get('rdw_subscription.service.subscription_manager')->update($subscription);

            $this->get('braincrafted_bootstrap.flash')->success('You successfully subscribe new subscription');
            $data['success'] = true;
        }

        $data['content'] = $this->renderView('RDWSubscriptionBundle:Subscription:_subscribe_form.html.twig', [
            'form' => $form->createView(),
            'action' => $this->generateUrl('rdw_subscription.subscription.update_subscription', ['id' => $subscription->getId()]),
            'method' => 'POST',
        ]);

        $response = new JsonResponse();
        $response->setData($data);

        return $response;
    }

    /**
     * @param Request        $request
     * @param RegisteredUser $user
     *
     * @Route("/multiple-delete/{id}", name="rdw_subscription.subscription.multiple_delete", requirements={"id" = "\d+"})
     * @Security("has_role('ROLE_REGISTERED')")
     *
     * @return RedirectResponse
     * @throws AccessDeniedException
     */
    public function multipleDeleteAction(Request $request, RegisteredUser $user)
    {
        $ids = $request->request->get('subscription_collection', ['id' => []])['id'];
        $form = $this->createForm(new SubscriptionCollectionType($ids));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this
                ->get('rdw_subscription.service.subscription_manager')
                ->multipleRemoveByUser($form->get('id')->getData(), $user);

            $this->container->get('braincrafted_bootstrap.flash')->success('Successfully deleted');
        }

        return $this->redirect($this->generateUrl('rdw_subscription.subscription.list'));
    }

    /**
     * @Template()
     *
     * @return array
     */
    public function newSubscriptionsBlockAction()
    {
        $limit = $this->container->getParameter('rdw_subscriptions.new_subscriptions_block_limit');

        $subscriptions = $this->container
            ->get('doctrine')
            ->getRepository('RDWSubscriptionBundle:Subscription')
            ->getNewSubscriptions($this->getUser(), $limit);

        return [
            'subscriptions' => $subscriptions,
        ];
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    private function isGrantedLocal(User $user)
    {
        $sessionUser = $this->getUser();

        if ($user->getId() == $sessionUser->getId()
            || ($user instanceof RegisteredUser
                && $sessionUser instanceof RegisteredUser
                && $user->isUserManager($sessionUser))
        ) {
            return true;
        } else {
            throw new AccessDeniedException();
        }
    }
}
