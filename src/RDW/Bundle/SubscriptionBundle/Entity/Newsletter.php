<?php

namespace RDW\Bundle\SubscriptionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class Newsletter
 *
 * @package RDW\Bundle\SubscriptionBundle\Entity
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 *
 * @ORM\Entity
 * @ORM\Table(name="newsletters")
 * @ORM\HasLifecycleCallbacks()
 *
 * @UniqueEntity(fields="email", message="Newsletter is already subscribed for this email", groups={"subscribe"})
 */
class Newsletter
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false, name="token")
     */
    protected $token;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false, unique=true)
     *
     * @Assert\NotBlank(message="Email address cannot be empty", groups={"subscribe"})
     * @Assert\Email(message="Enter valid email address", groups={"subscribe"})
     */
    protected $email;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     *
     * @Assert\NotBlank(message="Name cannot be empty", groups={"subscribe"})
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\NotBlank(message="Surname cannot be empty", groups={"subscribe"})
     */
    protected $surname;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true, name="subscribed_at")
     *
     * @Gedmo\Timestampable(on="create")
     */
    protected $subscribedAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     *
     * @return $this
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getSubscribedAt()
    {
        return $this->subscribedAt;
    }

    /**
     * @param \DateTime $subscribedAt
     *
     * @return $this
     */
    public function setSubscribedAt($subscribedAt)
    {
        $this->subscribedAt = $subscribedAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     *
     * @return $this
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }
}
