<?php

namespace RDW\Bundle\SubscriptionBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\JobBundle\Entity\Position;
use RDW\Bundle\JobBundle\Entity\WorkScheduleType;
use RDW\Bundle\JobBundle\Entity\ProfessionalArea;
use RDW\Bundle\LocationBundle\Entity\City;
use RDW\Bundle\UserBundle\Entity\Scope;
use RDW\Bundle\UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class Subscription
 *
 * @package RDW\Bundle\SubscriptionBundle\Entity
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 *
 * @ORM\Entity(repositoryClass="RDW\Bundle\SubscriptionBundle\Repository\SubscriptionRepository")
 * @ORM\Table(name="subscriptions")
 * @ORM\HasLifecycleCallbacks()
 */
class Subscription
{
    const TYPE_CV = 'cv';
    const TYPE_JOB = 'job';

    const FREQUENCY_DAILY = 1;
    const FREQUENCY_WEEKLY = 7;
    const FREQUENCY_TWO_WEEKS = 14;

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false, name="token")
     */
    protected $token;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\User", inversedBy="subscriptions")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true, name="subscribed_at")
     *
     * @Gedmo\Timestampable(on="create")
     */
    protected $subscribedAt;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false, name="type")
     */
    protected $type;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false, name="email")
     */
    protected $email;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="company_title")
     */
    protected $companyTitle;

    /**
     * @var int
     * @ORM\Column(type="smallint", nullable=false, name="frequency")
     */
    protected $frequency;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="RDW\Bundle\LocationBundle\Entity\City", inversedBy="subscriptions")
     */
    private $cities;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="RDW\Bundle\JobBundle\Entity\Position", inversedBy="subscriptions")
     */
    private $positions;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="RDW\Bundle\JobBundle\Entity\WorkScheduleType", inversedBy="subscriptions")
     * @ORM\JoinTable(name="subscription_work_schedule_type")
     */
    private $workSchedules;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="RDW\Bundle\UserBundle\Entity\Scope", inversedBy="subscriptions")
     */
    private $scopes;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="RDW\Bundle\JobBundle\Entity\ProfessionalArea", inversedBy="subscriptions")
     */
    private $professionalAreas;

    /**
     * @var string
     * @ORM\Column(type="string", length=20, nullable=true, name="status")
     * @Assert\NotBlank(message="Status cannot be empty")
     */
    private $status;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cities = new ArrayCollection();
        $this->positions = new ArrayCollection();
        $this->scopes = new ArrayCollection();
        $this->workSchedules = new ArrayCollection();
        $this->professionalAreas = new ArrayCollection();
        $this->status = self::STATUS_ACTIVE;
    }

    /**
     * @return array only specialization
     */
    public function getOnlySpecializationPA()
    {
        $professionalAreasNotParent = [];

        foreach ($this->professionalAreas as $value) {
            if (!$value->isParent()) {
                $professionalAreasNotParent[] = $value;
            }
        }

        return $professionalAreasNotParent;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     *
     * @return $this
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getSubscribedAt()
    {
        return $this->subscribedAt;
    }

    /**
     * @param \DateTime $subscribedAt
     *
     * @return $this
     */
    public function setSubscribedAt($subscribedAt)
    {
        $this->subscribedAt = $subscribedAt;

        return $this;
    }

    /**
     * @return \RDW\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param \RDW\Bundle\UserBundle\Entity\User $user
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return int
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * @param int $frequency
     *
     * @return $this
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @param ArrayCollection $cities
     *
     * @return $this
     */
    public function setCities(ArrayCollection $cities)
    {
        $this->cities = $cities;

        return $this;
    }

    /**
     * @param ArrayCollection $positions
     *
     * @return $this
     */
    public function setPositions(ArrayCollection $positions)
    {
        $this->positions = $positions;

        return $this;
    }

    /**
     * @param ArrayCollection $scopes
     *
     * @return $this
     */
    public function setScopes(ArrayCollection $scopes)
    {
        $this->scopes = $scopes;

        return $this;
    }

    /**
     * @param City $city
     *
     * @return $this
     */
    public function addCity($city)
    {
        if ($city) {
            $this->cities->add($city);
        }

        return $this;
    }

    /**
     * Remove duplicates entries
     *
     * @ORM\PrePersist
     */
    public function clearCities()
    {
        $cities = new ArrayCollection();

        foreach ($this->getCities() as $city) {
            if (!$cities->contains($city)) {
                $cities->add($city);
            }
        }

        $this->cities = $cities;
    }

    /**
     * Remove duplicates entries
     *
     * @ORM\PrePersist
     */
    public function clearPositions()
    {
        $positions = new ArrayCollection();

        foreach ($this->getPositions() as $position) {
            if (!$positions->contains($position)) {
                $positions->add($position);
            }
        }

        $this->positions = $positions;
    }

    /**
     * Remove duplicates entries
     *
     * @ORM\PrePersist
     */
    public function clearWorkScheduleTypes()
    {
        $workSchedules = new ArrayCollection();

        foreach ($this->getWorkSchedules() as $schedule) {
            if (!$workSchedules->contains($schedule)) {
                $workSchedules->add($schedule);
            }
        }

        $this->workSchedules = $workSchedules;
    }

    /**
     * Remove duplicates entries
     *
     * @ORM\PrePersist
     */
    public function clearScopes()
    {
        $scopes = new ArrayCollection();

        foreach ($this->getScopes() as $scope) {
            if (!$scopes->contains($scope)) {
                $scopes->add($scope);
            }
        }

        $this->scopes = $scopes;
    }

    /**
     * @param City $city
     *
     * @return $this
     */
    public function removeCity(City $city)
    {
        $this->cities->removeElement($city);

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getPositions()
    {
        return $this->positions;
    }

    /**
     * @param Position $position
     *
     * @return $this
     */
    public function addPosition($position)
    {
        if ($position) {
            $this->positions->add($position);
        }

        return $this;
    }

    /**
     * @param Position $position
     *
     * @return $this
     */
    public function removePosition(Position $position)
    {
        $this->positions->removeElement($position);

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getScopes()
    {
        return $this->scopes;
    }

    /**
     * @param Scope $scope
     *
     * @return $this
     */
    public function addScope($scope)
    {
        if ($scope) {
            $this->scopes->add($scope);
        }

        return $this;
    }

    /**
     * @param Scope $scope
     *
     * @return $this
     */
    public function removeScope(Scope $scope)
    {
        $this->scopes->removeElement($scope);

        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getWorkSchedules()
    {
        return $this->workSchedules;
    }

    /**
     * @param WorkScheduleType $workScheduleType
     *
     * @return $this
     */
    public function addWorkSchedule($workScheduleType)
    {
        if ($workScheduleType) {
            $this->workSchedules->add($workScheduleType);
        }

        return $this;
    }

    /**
     * @param WorkScheduleType $workScheduleType
     *
     * @return $this
     */
    public function removeWorkSchedule(WorkScheduleType $workScheduleType)
    {
        $this->workSchedules->removeElement($workScheduleType);

        return $this;
    }

    /**
     * @return array
     */
    public static function getFrequencyChoiceList()
    {
        return [
            self::FREQUENCY_DAILY => 'Daily',
            self::FREQUENCY_WEEKLY => 'Weekly',
            self::FREQUENCY_TWO_WEEKS => 'Once per two weeks',
        ];
    }

    /**
     * @return string
     */
    public function getCompanyTitle()
    {
        return $this->companyTitle;
    }

    /**
     * @param string $companyTitle
     *
     * @return $this
     */
    public function setCompanyTitle($companyTitle)
    {
        $this->companyTitle = $companyTitle;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return array
     */
    public static function getStatusesChoiceList()
    {
        return [
            self::STATUS_INACTIVE => 'not subscribed',
            self::STATUS_ACTIVE => 'subscribed',
        ];
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return ($this->getStatus() == self::STATUS_ACTIVE);
    }

    /**
     * @return bool
     */
    public function isTypeJob()
    {
        return (self::TYPE_JOB == $this->getType());
    }

    /**
     * Add professionalAreas
     *
     * @param ProfessionalArea $professionalAreas
     *
     * @return Subscription
     */
    public function addProfessionalArea(ProfessionalArea $professionalAreas)
    {
        $this->professionalAreas[] = $professionalAreas;

        return $this;
    }

    /**
     * Remove professionalAreas
     *
     * @param ProfessionalArea $professionalAreas
     */
    public function removeProfessionalArea(ProfessionalArea $professionalAreas)
    {
        $this->professionalAreas->removeElement($professionalAreas);
    }

    /**
     * Get professionalAreas
     *
     * @return ArrayCollection
     */
    public function getProfessionalAreas()
    {
        return $this->professionalAreas;
    }

    /**
     * @param ArrayCollection $pa
     *
     * @return $this
     */
    public function setProfessionalAreas(ArrayCollection $pa)
    {
        $this->professionalAreas = $pa;

        return $this;
    }
}
