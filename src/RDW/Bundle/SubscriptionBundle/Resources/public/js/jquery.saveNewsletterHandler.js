(function( $ ){
    $.fn.saveNewsletterHandler = function(options) {
        var settings = $.extend({
            contentHolder: "#modal_container"
        }, options);

        $(this).on('click', function(e) {
            e.preventDefault();

            var form = $(this).parents('form');
            var $this = $(this);

            var options = {
                success:   showResponse,
                type:      'post',
                dataType:  'json',
                resetForm: false
            };

            $(document).loaderStart();

            form.ajaxForm(options);
            form.submit();

            // post-submit callback
            function showResponse(data)  {
                if (data.success) {
                    window.location.reload();
                } else {
                    $('#' +$this.attr('data-content-holder')).html(data.content);
                    $("[data-handler='saveNewsletter']").saveNewsletterHandler();
                    $("[data-handler='form-collection']").formCollection();
                    $('select:not(.search-form-select select)').selecter();
                }

                $(document).loaderStop();

                return false;
            }
        });

    };
})(jQuery);