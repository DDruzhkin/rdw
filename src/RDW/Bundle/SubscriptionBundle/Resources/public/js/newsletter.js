$(function () {
	$("[data-handler='saveNewsletter']").saveNewsletterHandler();
	$("[data-handler='form-collection']").formCollection();
	$('select:not(.search-form-select select)').selecter();
	$('#modal_container').on('click', function () {
		$('.select2-container').each(function(i,el){$(el).data('select2').close()})
	});
});
