<?php

namespace RDW\Bundle\SubscriptionBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Faker\Generator;
use Faker\Factory as FakerFactory;
use RDW\Bundle\JobBundle\DataFixtures\ORM\LoadJobData;
use RDW\Bundle\JobBundle\DataFixtures\ORM\LoadPositionData;
use RDW\Bundle\JobBundle\DataFixtures\ORM\LoadWorkScheduleTypeData;
use RDW\Bundle\SubscriptionBundle\Entity\Subscription;
use RDW\Bundle\UserBundle\DataFixtures\ORM\LoadScopeData;
use RDW\Bundle\UserBundle\DataFixtures\ORM\LoadUserData;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;

/**
 * Class LoadSubscriptionsData
 *
 * @package RDW\Bundle\SubscriptionBundle\DataFixtures\ORM
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class LoadSubscriptionsData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * Faker.
     *
     * @var Generator
     */
    protected $faker;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->faker = FakerFactory::create();
    }

    function getDependencies()
    {
        return [
            LoadUserData::class,
            LoadPositionData::class,
            LoadScopeData::class,
            LoadWorkScheduleTypeData::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $entityManager)
    {
        $frequency = Subscription::getFrequencyChoiceList();

        for ($i = 1; $i <= 50; $i++) {
            /** @var $user RegisteredUser */
            $user = $this->getReference(LoadUserData::REFERENCE_STRING . rand(1, LoadUserData::NUMBER_OF_EMPLOYEES));

            $subscription = new Subscription();
            $subscription->setEmail('vytautas+' . $i . '@eface.lt');
            $subscription->setToken(md5($i));
            $subscription->setUser($user);
            $subscription->setCompanyTitle($this->faker->word);
            $type = $i % 3 == 0 ? Subscription::TYPE_JOB : Subscription::TYPE_CV;
            $subscription->setType($type);
            $subscription->setFrequency(array_rand($frequency));

            if ($i % 4 == 0) {
                $this->addWorkSchedule($subscription);
            }

            if ($i % 5 == 0) {
                $this->addScope($subscription);
            }

            if ($i % 6 == 0) {
                $this->addPosition($subscription);
            }

            if ($i % 3 == 0) {
                $this->addCities($subscription);
            }

            $entityManager->persist($subscription);
        }

        $entityManager->flush();
    }

    /**
     * @param Subscription $subscription
     *
     * @return Subscription
     */
    protected function addCities(Subscription $subscription)
    {
        foreach ($subscription->getUser()->getCities() as $city) {
            $subscription->addCity($city);
        }

        return $subscription;
    }

    /**
     * @param Subscription $subscription
     *
     * @return Subscription
     */
    protected function addPosition(Subscription $subscription)
    {
        $position = $this->getReference(LoadPositionData::REFERENCE_STRING . rand(1, LoadPositionData::COUNT));
        $subscription->addPosition($position);

        return $subscription;
    }

    /**
     * @param Subscription $subscription
     *
     * @return Subscription
     */
    protected function addScope(Subscription $subscription)
    {
        $scope = $this->getReference(LoadScopeData::REFERENCE_STRING . rand(1, LoadScopeData::COUNT));
        $subscription->addScope($scope);

        return $subscription;
    }

    /**
     * @param Subscription $subscription
     *
     * @return Subscription
     */
    protected function addWorkSchedule(Subscription $subscription)
    {
        $workSchedule = $this->getReference(
            LoadWorkScheduleTypeData::REFERENCE_STRING . rand(1, LoadWorkScheduleTypeData::COUNT)
        );
        $subscription->addWorkSchedule($workSchedule);

        return $subscription;
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 9;
    }
}
