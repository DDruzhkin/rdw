<?php

namespace RDW\Bundle\SubscriptionBundle\Service;

use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\SubscriptionBundle\Entity\Subscription;
use Symfony\Component\DependencyInjection\ContainerAware;

/**
 * Class SubscriptionService
 *
 * @package RDW\Bundle\SubscriptionBundle\Service
 */
class SubscriptionService extends ContainerAware
{
    /**
     * @param Subscription $subscription
     *
     * @return array
     */
    public function getObjectsForSubscription(Subscription $subscription)
    {
        $currentDate = new \DateTime('-' . $subscription->getFrequency() . ' days');

        $em = $this->container->get('doctrine')->getManager();

        /** @var \Doctrine\ORM\QueryBuilder $qb */
        $qb = $em->createQueryBuilder();
        $qb
            ->select('o', 'workingPlaceCity', 'position', 'scope', 'professionalArea', 'user', 'ws');

        if ($subscription->getType() == Subscription::TYPE_CV) {
            $qb->from('RDWCvBundle:Cv', 'o');
        } else {
            $qb->from('RDWJobBundle:Job', 'o');
        }

        $qb
            ->leftJoin('o.workScheduleType', 'ws')
            ->leftJoin('o.workingPlaceCity', 'workingPlaceCity')
            ->leftJoin('o.position', 'position')
            ->leftJoin('o.scope', 'scope')
            ->leftJoin('o.professionalArea', 'professionalArea')
            ->join('o.user', 'user')
            ->where('o.status IN (:status)')
            ->andWhere('o.updatedAt > :updatedAtToCheck')
            ->setParameter('updatedAtToCheck', $currentDate)
            ->setParameter('status', Job::STATUS_ACTIVE)
            ->orderBy('o.updatedAt', 'DESC');

        if ($subscription->getCities()->count() > 0) {
            $qb
                ->andWhere('o.workingPlaceCity IN (:cities)')
                ->setParameter('cities', $subscription->getCities()->getValues());
        }

        if ($subscription->getWorkSchedules()->count() > 0) {
            $qb
                ->andWhere('ws IN (:ws)')
                ->setParameter('ws', $subscription->getWorkSchedules()->getValues());
        }

        if ($subscription->getScopes()->count() > 0) {
            $qb
                ->andWhere('scope IN (:scope)')
                ->setParameter('scope', $subscription->getScopes()->getValues());
        }

        if ($subscription->getPositions()->count() > 0) {
            $qb
                ->andWhere('position IN (:position)')
                ->setParameter('position', $subscription->getPositions()->getValues());
        }

        if ($subscription->getProfessionalAreas()->count() > 0) {
            $qb
                ->andWhere('professionalArea IN (:pa)')
                ->setParameter('pa', $subscription->getOnlySpecializationPA());
        }

        return $qb->getQuery()->getResult();
    }
}
