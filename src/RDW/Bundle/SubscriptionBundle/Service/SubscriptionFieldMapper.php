<?php

namespace RDW\Bundle\SubscriptionBundle\Service;


use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\CvBundle\Service\CvManager;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\JobBundle\Model\JobFilter;
use RDW\Bundle\JobBundle\Service\JobManager;
use RDW\Bundle\JobBundle\Twig\JobExtension;
use RDW\Bundle\SubscriptionBundle\Entity\Subscription;

/**
 * Class SubscriptionFieldMapper
 *
 * @package RDW\Bundle\SubscriptionBundle\Service
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class SubscriptionFieldMapper
{
    /**
     * @var \RDW\Bundle\JobBundle\Service\JobManager
     */
    private $jobManager;

    /**
     * @var \RDW\Bundle\CvBundle\Service\CvManager
     */
    private $cvManager;

    /**
     * @var \RDW\Bundle\JobBundle\Twig\JobExtension
     */
    private $jobExtension;

    /**
     * @var ObjectManager
     */
    private $entityManager;

    /**
     * @param JobManager    $jobManager
     * @param CvManager     $cvManager
     * @param JobExtension  $jobExtension
     * @param ObjectManager $entityManager
     */
    public function __construct(
        JobManager $jobManager,
        CvManager $cvManager,
        JobExtension $jobExtension,
        ObjectManager $entityManager
    )
    {
        $this->jobManager   = $jobManager;
        $this->cvManager    = $cvManager;
        $this->jobExtension = $jobExtension;
        $this->entityManager = $entityManager;
    }

    /**
     * @param Subscription $subscription
     * @param object $filter
     * @param int $jobId
     *
     * @return Subscription
     */
    public function mapSubscriptionWithFilterValues(Subscription $subscription, $filter = null, $jobId = null)
    {
        if ( ! $filter) {
            return $subscription;
        }

        if ($filter->getCities()) {
            foreach ($filter->getCities() as $city) {
                $city = $this->entityManager->merge($city);
                $subscription->addCity($city);
            }
        }

        if ($filter->getWorkScheduleType()) {
            $workScheduleType = $this->entityManager->merge($filter->getWorkScheduleType());
            $subscription->addWorkSchedule($workScheduleType);
        }

        if ($filter instanceof JobFilter) {
            if ($filter->getScope()) {
                $scope = $this->entityManager->merge($filter->getScope());
                $subscription->addScope($scope);
            }

            if ($filter->getPosition()) {
                $position = $this->entityManager->merge($filter->getPosition());
                $subscription->addPosition($position);
            }

            if ($jobId) {
                $this->mapCompanyTitleFromJob($subscription, $jobId);
            }
        } else {
            if ($filter->getScopes()) {
                foreach ($filter->getScopes() as $scope) {
                    $scope = $this->entityManager->merge($scope);
                    $subscription->addScope($scope);
                }
            }
            if ($filter->getPositions()) {
                foreach ($filter->getPositions() as $position) {
                    $position = $this->entityManager->merge($position);
                    $subscription->addPosition($position);
                }
            }
        }

        return $subscription;
    }

    /**
     * @param Subscription $subscription
     * @param int          $objectId
     *
     * @return Subscription
     */
    public function mapSubscriptionWithObject(Subscription $subscription, $objectId)
    {
        if ($subscription->getType() == Subscription::TYPE_CV) {
            $object = $this->cvManager->findCvById($objectId);
        } else {
            $object = $this->jobManager->getById($objectId);
        }

        if ($object) {
            foreach ($object->getCities() as $city) {
                $city = $this->entityManager->merge($city);
                $subscription->addCity($city);
            }

            if ($object->getWorkScheduleType()) {
                $workScheduleType = $this->entityManager->merge($object->getWorkScheduleType());
                $subscription->addWorkSchedule($workScheduleType);
            }

            if ($object->getScope()) {
                $scope = $this->entityManager->merge($object->getScope());
                $subscription->addScope($scope);
            }

            if ($object->getPosition()) {
                $position = $this->entityManager->merge($object->getPosition());
                $subscription->addPosition($position);
            }

            if ($object instanceof Job) {
                $this->mapCompanyTitleFromJob($subscription, $objectId);
            }
        }

        return $subscription;
    }

    /**
     * @param Subscription $subscription
     * @param int          $jobId
     */
    private function mapCompanyTitleFromJob(Subscription $subscription, $jobId)
    {
        $job = $this->jobManager->getById($jobId);

        if ($job instanceof Job) {
            // only if company title is allowed to view
            if ($this->jobExtension->getSecretField($job, 'companyTitle', true)) {
                $subscription->setCompanyTitle($job->getCompanyTitle());
            }
        }
    }
}
