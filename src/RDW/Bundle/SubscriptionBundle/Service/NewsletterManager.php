<?php

namespace RDW\Bundle\SubscriptionBundle\Service;

use Doctrine\ORM\EntityRepository;
use RDW\Bundle\SubscriptionBundle\Entity\Newsletter;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class NewsletterManager
 *
 * @package RDW\Bundle\SubscriptionBundle\Service
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class NewsletterManager
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $entityManager;

    /**
     * @var TokenGeneratorInterface
     */
    private $tokenGenerator;

    /**
     * @param ObjectManager           $entityManager
     * @param TokenGeneratorInterface $tokenGenerator
     */
    public function __construct(ObjectManager $entityManager, TokenGeneratorInterface $tokenGenerator)
    {
        $this->entityManager = $entityManager;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * @param object $user
     *
     * @return Newsletter
     */
    public function createNewsletterFromUser($user = null)
    {
        $newsletter = new Newsletter();

        if ($user instanceof RegisteredUser) {
            $newsletter->setEmail($user->getEmail());

            if ($user->isTypeEmployee()) {
                $newsletter->setName($user->getName());
                $newsletter->setSurname($user->getSurname());
            }

            if ($user->isTypeEmployer()) {
                $newsletter->setName($user->getTitle());
            }
        }

        $newsletter->setToken($this->tokenGenerator->generateToken());

        return $newsletter;
    }

    /**
     * @param RegisteredUser $user
     * @param bool           $subscription
     * @param Newsletter     $newsletter
     *
     * @return boolean
     */
    public function changeSubscription(RegisteredUser $user, $subscription, Newsletter $newsletter = null)
    {
        if ($user instanceof RegisteredUser) {
            if (! $newsletter && $subscription) {
                $this->update($this->createNewsletterFromUser($user));
            }

            if ($newsletter && ! $subscription) {
                $this->removeSubscription($newsletter);
            }

            return true;
        }

        return false;
    }

    /**
     * @param Newsletter $newsletter
     * @param bool       $andFlush
     *
     * @return bool
     */
    public function update(Newsletter $newsletter, $andFlush = true)
    {
        $this->entityManager->persist($newsletter);

        if ($andFlush) {
            $this->entityManager->flush();
        }

        return true;
    }

    /**
     * @param $email
     * @return Newsletter|null
     */
    public function getNewsletterByEmail($email)
    {
        return $this->getRepository()->findOneBy(['email' => $email]);
    }

    /**
     * @param Newsletter $newsletter
     *
     * @return bool
     */
    public function removeSubscription(Newsletter $newsletter)
    {
        $this->entityManager->remove($newsletter);
        $this->entityManager->flush();

        return true;
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    protected function getRepository()
    {
        return $this->entityManager->getRepository('RDWSubscriptionBundle:Newsletter');
    }
}
