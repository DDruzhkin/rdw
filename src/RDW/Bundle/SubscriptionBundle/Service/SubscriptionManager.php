<?php

namespace RDW\Bundle\SubscriptionBundle\Service;

use FOS\UserBundle\Util\TokenGeneratorInterface;
use RDW\Bundle\JobBundle\Entity\ProfessionalArea;
use RDW\Bundle\LocationBundle\Entity\City;
use RDW\Bundle\SubscriptionBundle\Entity\Newsletter;
use RDW\Bundle\SubscriptionBundle\Entity\Subscription;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\VarDumper\VarDumper;

/**
 * Class SubscriptionManager
 *
 * @package RDW\Bundle\SubscriptionBundle\Service
 */
class SubscriptionManager
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $entityManager;

    /**
     * @var TokenGeneratorInterface
     */
    private $tokenGenerator;

    /**
     * @param ObjectManager $entityManager
     * @param TokenGeneratorInterface $tokenGenerator
     */
    public function __construct(
        ObjectManager $entityManager,
        TokenGeneratorInterface $tokenGenerator
    ) {
        $this->entityManager = $entityManager;
        $this->tokenGenerator = $tokenGenerator;
    }

    public function create($type, User $user = null)
    {
        $subscription = new Subscription();
        $subscription->setToken($this->tokenGenerator->generateToken());
        $subscription->setType($type);

        if (null !== $user) {
            $subscription->setUser($user);
            $subscription->setEmail($user->getEmail());
        }

        $subscription->setUser($user);

        return $subscription;
    }

    /**
     * @param object $user
     *
     * @return Newsletter
     */
    public function createNewsletterFromUser($user = null)
    {
        $newsletter = new Newsletter();

        if ($user instanceof User) {
            $newsletter->setEmail($user->getEmail());
        }

        return $newsletter;
    }

    /**
     * @param Subscription $subscription
     * @param bool $andFlush
     *
     * @return bool
     */
    public function update(Subscription $subscription, $andFlush = true)
    {
        $this->hydrateCities($subscription);
        $this->hydratePa($subscription);

        $this->entityManager->persist($subscription);

        if ($andFlush) {
            $this->entityManager->flush();
        }

        return true;
    }

    /**
     * @param Subscription $subscription
     */
    private function hydrateCities(Subscription $subscription)
    {
        $items = $subscription->getCities();
        $merged = new ArrayCollection();
        /** @var City $item */
        foreach ($items->toArray() as $item) {
            try {
                $merged->add($this->entityManager->merge($item));
            } catch (\Exception $e){

            }
        }
        $subscription->setCities($merged);
    }

    private function hydratePa(Subscription $subscription)
    {
        $items = $subscription->getProfessionalAreas();
        $merged = new ArrayCollection();
        /** @var ProfessionalArea $item */
        foreach ($items->toArray() as $item) {
            try {
                $merged->add($this->entityManager->merge($item));
            } catch (\Exception $e){

            }
        }
        $subscription->setProfessionalAreas($merged);
    }

    /**
     * @param string $email
     * @param string $type
     *
     * @return bool
     */
    public function deleteOldSubscriptionByEmailAndType($email, $type)
    {
        $oldSubscription = $this->getRepository()->findOneBy([
            'email' => $email,
            'type' => $type
        ]);

        if ($oldSubscription instanceof Subscription) {
            return $this->removeSubscription($oldSubscription);
        }

        return false;
    }

    /**
     * @param Subscription $subscription
     *
     * @return bool
     */
    public function removeSubscription(Subscription $subscription)
    {
        $this->entityManager->remove($subscription);
        $this->entityManager->flush();

        return true;
    }

    /**
     * @param ArrayCollection $subscriptions
     * @param RegisteredUser $user
     */
    public function multipleRemoveByUser(ArrayCollection $subscriptions, RegisteredUser $user)
    {
        foreach ($subscriptions as $subscription) {
            if ($subscription->getUser()->getId() == $user->getId()) {
                $this->entityManager->remove($subscription);
            }
        }

        $this->entityManager->flush();
    }

    /**
     * @return \RDW\Bundle\JobBundle\Repository\ApplyRepository
     */
    protected function getRepository()
    {
        return $this->entityManager->getRepository('RDWSubscriptionBundle:Subscription');
    }
}
