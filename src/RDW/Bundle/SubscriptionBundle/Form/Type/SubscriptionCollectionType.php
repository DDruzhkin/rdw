<?php

namespace RDW\Bundle\SubscriptionBundle\Form\Type;

use RDW\Bundle\SubscriptionBundle\Repository\SubscriptionRepository;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\UserInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class SubscriptionCollectionType
 * @package RDW\Bundle\SubscriptionBundle\Form\Type
 */
class SubscriptionCollectionType extends AbstractType
{
    private $subscriptions;

    public function __construct(array $subscriptions)
    {
        $this->subscriptions = $subscriptions;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', 'entity', [
            'required' => false,
            'class' => 'RDWSubscriptionBundle:Subscription',
            'property' => 'id',
            'property_path' => '[id]',
            'multiple' => true,
            'expanded' => true,
            'query_builder' => function (SubscriptionRepository $repository) {
                return $repository
                    ->createQueryBuilder('s')
                    ->where('s.id IN (:ids)')
                    ->setParameter('ids', $this->subscriptions);
            },
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
            'csrf_protection' => false,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'subscription_collection';
    }
}
