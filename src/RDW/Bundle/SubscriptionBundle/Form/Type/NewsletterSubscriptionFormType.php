<?php

namespace RDW\Bundle\SubscriptionBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class NewsletterSubscriptionFormType
 * @package RDW\Bundle\SubscriptionBundle\Form\Type
 */
class NewsletterSubscriptionFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('subscribe', 'checkbox', [
                'label' => 'I want to receive newsletter',
                'required' => false,
                'data' => (isset($options['newsletter']) && $options['newsletter'])
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'newsletter' => false
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_newsletter_subscription';
    }
}
