<?php

namespace RDW\Bundle\SubscriptionBundle\Form\Type;

use RDW\Bundle\SubscriptionBundle\Entity\Newsletter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class NewsletterFormType
 *
 * @package RDW\Bundle\SubscriptionBundle\Form\Type
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class NewsletterFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email', [
                'label' => 'Email',
                'required' => true,
                'attr' => ['placeholder' => 'Email placeholder']
            ])
            ->add('name', 'text', [
                'label' => 'Name',
                'required' => true,
                'attr' => ['placeholder' => 'Name placeholder']
            ])
            ->add('surname', 'text', [
                'label' => 'Surname',
                'required' => true,
                'attr' => ['placeholder' => 'Surname placeholder']
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\SubscriptionBundle\Entity\Newsletter',
            'validation_groups' => ['subscribe']
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_newsletter';
    }
}
