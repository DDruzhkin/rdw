<?php

namespace RDW\Bundle\SubscriptionBundle\Form\Type;

use RDW\Bundle\SubscriptionBundle\Entity\Subscription;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormError;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class SubscriptionFormType
 *
 * @package RDW\Bundle\SubscriptionBundle\Form\Type
 */
class SubscriptionFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Subscription $subscription */
        $subscription = $builder->getForm()->getData();

        if ($subscription->isTypeJob()) {
            $builder
                ->add('companyTitle', 'text', [
                        'label' => 'Company title',
                        'required' => false,
                        'attr' => ['placeholder' => 'Company title placeholder'],
                    ]);
        }

        $builder
            ->add('professionalAreas', 'professional_area_collection_popup', [
                    'label' => 'Professional Area',
                ])
            ->add('cities', 'city_collection', [
                    'label' => 'Cities',
                ])
            ->add('workSchedules', 'work_schedule_collection', [
                    'label' => 'Work schedules',
                ])
            ->add('frequency', 'choice', [
                    'choices'   => Subscription::getFrequencyChoiceList(),
                    'label' => 'Email frequency',
                    'required' => true,
                ]);

        if ($subscription->getUser() instanceof RegisteredUser) {
            $builder
                ->add('submit', 'submit', [
                'label' => 'save subscription',
            ]);
        }

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
                $entity = $event->getData();
                $form = $event->getForm();

                if ($entity && null !== $entity->getId()) {
                    $form->add('status', 'choice', [
                            'choices' => Subscription::getStatusesChoiceList(),
                            'empty_value' => 'Choose option',
                        ]);
                }
            });
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
                'data_class' => 'RDW\Bundle\SubscriptionBundle\Entity\Subscription',
                'validation_groups' => ['subscribe'],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_subscription';
    }
}