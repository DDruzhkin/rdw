<?php

namespace RDW\Bundle\SubscriptionBundle\Model;

use RDW\Bundle\SubscriptionBundle\Entity\Subscription;

/**
 * Interface FilterInterface
 *
 * @package RDW\Bundle\SubscriptionBundle\Model
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
interface FilterInterface
{
    /**
     * @param Subscription $subscription
     *
     * @return $this
     */
    public function update(Subscription $subscription);
}
