<?php
namespace RDW\Bundle\SubscriptionBundle\Command;

use RDW\Bundle\SubscriptionBundle\Entity\Subscription;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SubscriptionCommand
 *
 * @package RDW\Bundle\SubscriptionBundle\Command
 */
class SubscriptionCommand extends ContainerAwareCommand
{
    /**
     * Configure
     */
    protected function configure()
    {
        $this
            ->setName('rdw:subscription:send')
            ->setDescription('Send subscriptions for new subscribers')
            ->addOption(
                'frequency',
                'f',
                InputOption::VALUE_REQUIRED,
                'How often should be subsriptions sent'
            )
            ->addOption(
                'type',
                't',
                InputOption::VALUE_REQUIRED,
                'Type of subscription. Posible values job or cv'
            )
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $frequency = $this->getFrequencyFromOption($input, $output);
        $type = $input->getOption('type');

        if (null === $frequency || null === $type) {
            $output->writeln('<error>Please specify frequancy and type arguments</error>');
            return;
        }

        $output->writeln('<info>Sending subscriptions</info>');

        $subscriptions = $this
            ->getContainer()
            ->get('rdw_subscription.subscription.repository')
            ->getSubscribers($frequency, $type);

        $output->writeln(sprintf(
            '<comment>Frequency: %d, object type: "%s", total subscriptions found: %d</comment>',
            $frequency, $type, sizeof($subscriptions)
        ));
        $output->writeln('');

        foreach ($subscriptions as $subscription) {
            if (! $subscription instanceof Subscription) {
                continue;
            }

            $objects = $this->getContainer()
                ->get('rdw_subscription.service.subscription_service')
                ->getObjectsForSubscription($subscription);

            $this->sendSubscription($output, $subscription, $objects);
        }
    }

    /**
     * @param OutputInterface $output
     * @param Subscription    $subscription
     * @param array           $objects
     */
    protected function sendSubscription(OutputInterface $output, Subscription $subscription, array $objects)
    {
        if (empty($objects)) {
            $output->writeln(sprintf('<error>No objects found for %d</error>', $subscription->getId()));
            return;
        }

        if ($subscription->getType() == Subscription::TYPE_CV) {
            $this->getContainer()->get('rdw_mailer.service.mailer_manager')->sendNewSubscribedCvsEmail($subscription, $objects);
        } else {
            $this->getContainer()->get('rdw_mailer.service.mailer_manager')->sendNewSubscribedJobsEmail($subscription, $objects);
        }

        $output->write(sprintf('<comment>Sent for %d with %d objects</comment>', $subscription->getId(), sizeof($objects)));
        $output->write('<info> ✓</info>');
        $output->writeln('');
    }

    /**
     * @param InputInterface $input
     *
     * @return int
     */
    protected function getFrequencyFromOption(InputInterface $input, OutputInterface $output)
    {
        $frequency = $input->getOption('frequency');

        switch ($frequency) {
            case 'daily':
                return 1;

            case 'weekly':
                return 7;

            case 'biweekly':
                return 14;

            default:
                $output->writeln(sprintf('<error>Wrong frequency specified, expected daily, weekly, biweekly, but got "%s"</error>', $frequency));
                exit;
        }
    }
}
