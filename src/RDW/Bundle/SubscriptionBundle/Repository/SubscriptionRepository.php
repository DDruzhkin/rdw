<?php

namespace RDW\Bundle\SubscriptionBundle\Repository;

use Doctrine\ORM\EntityRepository;
use RDW\Bundle\SubscriptionBundle\Entity\Subscription;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;

/**
 * Class SubscriptionRepository
 *
 * @package RDW\Bundle\SubscriptionBundle\Repository
 */
class SubscriptionRepository extends EntityRepository
{
    /**
     * @param string $frequency
     * @param string $type
     *
     * @return array
     */
    public function getSubscribers($frequency, $type)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('s', 'cities', 'positions', 'user')
            ->from('RDWSubscriptionBundle:Subscription', 's')
            ->join('s.user', 'user')
            ->leftJoin('s.cities', 'cities')
            ->leftJoin('s.positions', 'positions')
            ->leftJoin('s.scopes', 'scopes')
            ->leftJoin('s.workSchedules', 'work_schedules')
            ->where('s.frequency = :frequency')
            ->andWhere('user INSTANCE OF RDW\Bundle\UserBundle\Entity\RegisteredUser')
            ->andWhere('s.type = :type')
            ->andWhere('s.status = :status')
            ->setParameter('type', $type)
            ->setParameter('frequency', $frequency)
            ->setParameter('status', Subscription::STATUS_ACTIVE)
           ;

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param RegisteredUser $user
     * @param int  $limit
     *
     * @return array
     */
    public function getNewSubscriptions(RegisteredUser $user, $limit = 3)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('s', 'cities', 'positions')
            ->from('RDWSubscriptionBundle:Subscription', 's')
            ->leftJoin('s.cities', 'cities')
            ->leftJoin('s.positions', 'positions')
            ->where('s.user = :user')
            ->andWhere('s.status = :status')
            ->setParameter('user', $user)
            ->setParameter('status', Subscription::STATUS_ACTIVE)
            ->orderBy('s.subscribedAt', 'DESC')
            ->setMaxResults($limit)
        ;

        return $queryBuilder->getQuery()->getResult();
    }
}
