<?php

namespace RDW\Bundle\SubscriptionBundle\Tests\Service;

use RDW\Bundle\SubscriptionBundle\Entity\Newsletter;
use RDW\Bundle\SubscriptionBundle\Entity\Subscription;
use RDW\Bundle\SubscriptionBundle\Service\NewsletterManager;
use RDW\Bundle\SubscriptionBundle\Service\SubscriptionManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class NewsletterManagerTest
 *
 * @package RDW\Bundle\SubscriptionBundle\Tests\Service
 */
class NewsletterManagerTest extends WebTestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $em;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $userMock;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $registeredUserMock;

    /**
     * @var NewsletterManager
     */
    private $newsletterManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $repo;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $tokenGenerator;

    /**
     * set up
     */
    public function setUp()
    {
        $this->tokenGenerator = $this->getMockBuilder('FOS\UserBundle\Util\TokenGenerator')->disableOriginalConstructor()->getMock();
        $this->repo = $this->getMock('Doctrine\Common\Persistence\ObjectRepository');
        $this->registeredUserMock = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');
        $this->userMock = $this->getMock('RDW\Bundle\UserBundle\Entity\User');

        $this->em = $this->getMockBuilder('Doctrine\ORM\EntityManager')
            ->disableOriginalConstructor()->getMock();

        $this->em->expects($this->any())
            ->method('getRepository')
            ->with('RDWSubscriptionBundle:Newsletter')
            ->will($this->returnValue($this->repo));

        $this->newsletterManager = new NewsletterManager($this->em, $this->tokenGenerator);
    }

    /**
     * @test
     */
    public function it_should_create_new_newsletter_from_user()
    {
        $this->tokenGenerator
            ->expects($this->once())
            ->method('generateToken')
            ->willReturn('newToken');

        $newsletter = new Newsletter();
        $newsletter->setToken('newToken');

        $this->assertEquals(
            $newsletter,
            $this->newsletterManager->createNewsletterFromUser($this->userMock)
        );
    }

    /**
     * @test
     */
    public function it_should_create_new_newsletter_from_registered_user()
    {
        $this->registeredUserMock
            ->expects($this->once())
            ->method('getEmail')
            ->willReturn('user@example.com');

        $this->tokenGenerator
            ->expects($this->once())
            ->method('generateToken')
            ->willReturn('newToken');

        $newsletter = new Newsletter();
        $newsletter->setEmail('user@example.com');
        $newsletter->setToken('newToken');

        $this->assertEquals(
            $newsletter,
            $this->newsletterManager->createNewsletterFromUser($this->registeredUserMock)
        );
    }

    /**
     * @test
     */
    public function it_should_only_create_newsletter_object_when_user_not_logged_in()
    {
        $this->userMock
            ->expects($this->never())
            ->method('getEmail');

        $newsletter = new Newsletter();

        $this->tokenGenerator
            ->expects($this->once())
            ->method('generateToken')
            ->willReturn('newToken');

        $newsletter->setToken('newToken');

        $this->assertEquals(
            $newsletter,
            $this->newsletterManager->createNewsletterFromUser(null)
        );
    }

    /**
     * @test
     */
    public function it_should_update_and_flush_object()
    {
        $object = new Newsletter();

        $this->em
            ->expects($this->once())
            ->method('persist')
            ->will($this->returnValue(true));

        $this->em
            ->expects($this->once())
            ->method('flush')
            ->will($this->returnValue(true));

        $result = $this->newsletterManager->update($object);
        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function it_should_persist_and_not_flush()
    {
        $object = new Newsletter();

        $this->em
            ->expects($this->once())
            ->method('persist')
            ->will($this->returnValue(true));

        $this->em
            ->expects($this->never())
            ->method('flush')
            ->will($this->returnValue(true));

        $result = $this->newsletterManager->update($object, false);
        $this->assertTrue($result);
    }
}

