<?php

namespace RDW\Bundle\SubscriptionBundle\Tests\Service;

use Doctrine\Common\Collections\ArrayCollection;
use RDW\Bundle\CvBundle\Model\CvFilter;
use RDW\Bundle\JobBundle\Entity\Position;
use RDW\Bundle\JobBundle\Entity\WorkScheduleType;
use RDW\Bundle\LocationBundle\Entity\City;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\Scope;
use RDW\Bundle\UserBundle\Entity\User;
use RDW\Bundle\SubscriptionBundle\Entity\Subscription;
use RDW\Bundle\SubscriptionBundle\Service\SubscriptionManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SubscriptionManagerTest extends WebTestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $em;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $repo;

    /**
     * @var SubscriptionManager
     */
    private $subscriptionManager;

    /**
     * @var Subscription
     */
    private $subscription;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $tokenGenerator;

    /**
     * set up
     */
    public function setUp()
    {
        $this->subscription = $this->getMock('\RDW\Bundle\SubscriptionBundle\Entity\Subscription');
        $this->tokenGenerator = $this->getMock('\FOS\UserBundle\Util\TokenGenerator');
        $this->tokenGenerator = $this->getMock('\FOS\UserBundle\Util\TokenGenerator');
        $this->repo = $this->getMock('Doctrine\Common\Persistence\ObjectRepository');
        $this->em = $this->getMockBuilder('Doctrine\ORM\EntityManager')
            ->disableOriginalConstructor()->getMock();
        $this->em->expects($this->any())
            ->method('getRepository')
            ->with('RDWSubscriptionBundle:Subscription')
            ->will($this->returnValue($this->repo));

        $this->subscriptionManager = new SubscriptionManager($this->em, $this->tokenGenerator);
    }

    /**
     * @test
     */
    public function it_should_create_new_subscription_object_from_user_and_type()
    {
        $user = new RegisteredUser();
        $user->setEmail('jonas@test.com');

        $subscription = new Subscription();
        $subscription->setEmail('jonas@test.com');
        $subscription->setUser($user);
        $subscription->setType(Subscription::TYPE_CV);

        $this->assertEquals(
            $subscription,
            $this->subscriptionManager->create(Subscription::TYPE_CV, $user)
        );
    }

    /**
     * @test
     */
    public function it_should_remove_subscription()
    {
        $this->em
            ->expects($this->once())
            ->method('remove')
            ->with($this->equalTo(
                $this->subscription
            ))
            ->will($this->returnValue(true));

        $this->em
            ->expects($this->once())
            ->method('flush')
            ->will($this->returnValue(true));

        $this->assertTrue($this->subscriptionManager->removeSubscription($this->subscription));
    }

    /**
     * @test
     */
    public function it_should_update_and_flush_object()
    {
        $object = new Subscription();

        $this->em
            ->expects($this->once())
            ->method('persist')
            ->will($this->returnValue(true));

        $this->em
            ->expects($this->once())
            ->method('flush')
            ->will($this->returnValue(true));

        $result = $this->subscriptionManager->update($object);
        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function it_should_persist_and_not_flush()
    {
        $object = new Subscription();

        $this->em
            ->expects($this->once())
            ->method('persist')
            ->will($this->returnValue(true));

        $this->em
            ->expects($this->never())
            ->method('flush')
            ->will($this->returnValue(true));

        $result = $this->subscriptionManager->update($object, false);
        $this->assertTrue($result);
    }
}
