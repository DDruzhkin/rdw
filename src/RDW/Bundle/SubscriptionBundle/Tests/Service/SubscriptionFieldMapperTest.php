<?php

namespace RDW\Bundle\SubscriptionBundle\Tests\Service;


use Doctrine\Common\Collections\ArrayCollection;
use RDW\Bundle\CvBundle\Model\CvFilter;
use RDW\Bundle\JobBundle\Entity\Position;
use RDW\Bundle\JobBundle\Entity\WorkScheduleType;
use RDW\Bundle\LocationBundle\Entity\City;
use RDW\Bundle\SubscriptionBundle\Entity\Subscription;
use RDW\Bundle\SubscriptionBundle\Service\SubscriptionFieldMapper;
use RDW\Bundle\UserBundle\Entity\Scope;

class SubscriptionFieldMapperTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $jobManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $cvManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $jobExtension;

    /**
     * @var SubscriptionFieldMapper
     */
    private $subscriptionFieldMapper;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $entityManager;

    /**
     * set up
     */
    public function setUp()
    {
        $this->jobManager = $this->getMockBuilder('RDW\Bundle\JobBundle\Service\JobManager')->disableOriginalConstructor()->getMock();
        $this->cvManager = $this->getMockBuilder('RDW\Bundle\CvBundle\Service\CvManager')->disableOriginalConstructor()->getMock();
        $this->jobExtension = $this->getMockBuilder('RDW\Bundle\JobBundle\Twig\JobExtension')->disableOriginalConstructor()->getMock();
        $this->entityManager = $this->getMockBuilder('Doctrine\Common\Persistence\ObjectManager')->disableOriginalConstructor()->getMock();

        $this->subscriptionFieldMapper = new SubscriptionFieldMapper($this->jobManager, $this->cvManager, $this->jobExtension, $this->entityManager);
    }

    /**
     * @test
     */
    public function it_should_map_subscription_with_filter_values()
    {
        $subscription = new Subscription();
        $filter = new CvFilter();

        $cities = new ArrayCollection();
        $city1 = new City();
        $cities->add($city1);
        $city2 = new City();
        $cities->add($city2);
        $filter->setCities($cities);
        $this->entityManager->expects($this->at(0))->method('merge')->with($city1)->will($this->returnValue($city1));
        $this->entityManager->expects($this->at(1))->method('merge')->with($city2)->will($this->returnValue($city2));

        $workScheduleType = new WorkScheduleType();
        $filter->setWorkScheduleType($workScheduleType);
        $this->entityManager->expects($this->at(2))->method('merge')->with($workScheduleType)->will($this->returnValue($workScheduleType));

        $scopes = new ArrayCollection();
        $scope = new Scope();
        $scopes->add($scope);
        $filter->setScopes($scopes);
        $this->entityManager->expects($this->at(3))->method('merge')->with($scope)->will($this->returnValue($scope));

        $positions = new ArrayCollection();
        $position = new Position();
        $positions->add($position);
        $filter->setPositions($positions);
        $this->entityManager->expects($this->at(4))->method('merge')->with($position)->will($this->returnValue($position));

        $this->assertCount(0, $subscription->getPositions());
        $this->assertCount(0, $subscription->getCities());
        $this->assertCount(0, $subscription->getWorkSchedules());
        $this->assertCount(0, $subscription->getScopes());

        $result = $this->subscriptionFieldMapper->mapSubscriptionWithFilterValues($subscription, $filter);

        $this->assertCount(1, $result->getPositions());
        $this->assertCount(2, $result->getCities());
        $this->assertCount(1, $result->getWorkSchedules());
        $this->assertCount(1, $result->getScopes());
    }

}
