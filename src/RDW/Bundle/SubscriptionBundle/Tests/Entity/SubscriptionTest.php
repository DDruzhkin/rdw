<?php

namespace RDW\Bundle\SubscriptionBundle\Tests\Entity;

use RDW\Bundle\JobBundle\Entity\Position;
use RDW\Bundle\JobBundle\Entity\WorkScheduleType;
use RDW\Bundle\LocationBundle\Entity\City;
use RDW\Bundle\SubscriptionBundle\Entity\Subscription;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\Scope;

/**
 * Class SubscriptionTest
 *
 * @package RDW\Bundle\SubscriptionBundle\Tests\Entity
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class SubscriptionTest extends \PHPUnit_Framework_TestCase
{
    const CLASS_NAME = 'RDW\Bundle\SubscriptionBundle\Entity\Subscription';

    /**
     * @test
     */
    public function it_should_set_type_properly()
    {
        $type = Subscription::TYPE_JOB;

        $subscription = new Subscription();
        $result = $subscription->setType($type);
        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals($type, $result->getType());
    }

    /**
     * @test
     */
    public function it_should_set_frequency_properly()
    {
        $frequency = Subscription::FREQUENCY_TWO_WEEKS;

        $subscription = new Subscription();
        $result = $subscription->setFrequency($frequency);
        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals($frequency, $result->getFrequency());
    }

    /**
     * @test
     */
    public function it_should_set_subscribed_at_properly()
    {
        $date = new \DateTime('-2DAYS');
        $subscription = new Subscription();
        $subscription->setSubscribedAt($date);

        $this->assertInstanceOf('\DateTime', $subscription->getSubscribedAt());
        $this->assertEquals($date, $subscription->getSubscribedAt());
    }

    /**
     * @test
     */
    public function it_should_add_city_properly()
    {
        $city = new City();
        $subscription = new Subscription();
        $this->assertCount(0, $subscription->getCities());

        $result = $subscription->addCity($city);
        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertCount(1, $result->getCities());
    }

    /**
     * @test
     */
    public function it_should_add_position_properly()
    {
        $position = new Position();
        $subscription = new Subscription();
        $this->assertCount(0, $subscription->getPositions());

        $result = $subscription->addPosition($position);
        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertCount(1, $result->getPositions());
    }

    /**
     * @test
     */
    public function it_should_add_scope_properly()
    {
        $scope = new Scope();
        $subscription = new Subscription();
        $this->assertCount(0, $subscription->getScopes());

        $result = $subscription->addScope($scope);
        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertCount(1, $result->getScopes());
    }

    /**
     * @test
     */
    public function it_should_add_work_schedule_properly()
    {
        $workSchedule = new WorkScheduleType();
        $subscription = new Subscription();
        $this->assertCount(0, $subscription->getWorkSchedules());

        $result = $subscription->addWorkSchedule($workSchedule);
        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertCount(1, $result->getWorkSchedules());
    }

    /**
     * @test
     */
    public function it_should_remove_city_properly()
    {
        $city = new City();
        $subscription = new Subscription();
        $subscription->addCity($city);

        $this->assertCount(1, $subscription->getCities());

        $subscription->removeCity($city);
        $this->assertCount(0, $subscription->getCities());
    }

    /**
     * @test
     */
    public function it_should_remove_duplicated_cities()
    {
        $subscription = new Subscription();

        $city = new City();
        $city2 = $city;

        $subscription->addCity($city);
        $subscription->addCity($city2);

        $this->assertCount(2, $subscription->getCities());

        $subscription->clearCities();
        $this->assertCount(1, $subscription->getCities());
    }

    /**
     * @test
     */
    public function it_should_remove_duplicated_work_schedules()
    {
        $subscription = new Subscription();

        $schedule = new WorkScheduleType();
        $schedule2 = $schedule;

        $subscription->addWorkSchedule($schedule);
        $subscription->addWorkSchedule($schedule2);

        $this->assertCount(2, $subscription->getWorkSchedules());

        $subscription->clearWorkScheduleTypes();
        $this->assertCount(1, $subscription->getWorkSchedules());
    }

    /**
     * @test
     */
    public function it_should_remove_duplicated_positions()
    {
        $subscription = new Subscription();

        $position = new Position();
        $position2 = $position;

        $subscription->addPosition($position);
        $subscription->addPosition($position2);

        $this->assertCount(2, $subscription->getPositions());

        $subscription->clearPositions();
        $this->assertCount(1, $subscription->getPositions());
    }

    /**
     * @test
     */
    public function it_should_remove_duplicated_scopes()
    {
        $subscription = new Subscription();

        $scope = new Scope();
        $scope2 = $scope;

        $subscription->addScope($scope);
        $subscription->addScope($scope2);

        $this->assertCount(2, $subscription->getScopes());

        $subscription->clearScopes();
        $this->assertCount(1, $subscription->getScopes());
    }

    /**
     * @test
     */
    public function it_should_remove_work_schedule_properly()
    {
        $workSchedule = new WorkScheduleType();
        $subscription = new Subscription();
        $subscription->addWorkSchedule($workSchedule);

        $this->assertCount(1, $subscription->getWorkSchedules());

        $subscription->removeWorkSchedule($workSchedule);
        $this->assertCount(0, $subscription->getWorkSchedules());
    }

    /**
     * @test
     */
    public function it_should_remove_scope_properly()
    {
        $scope = new Scope();
        $subscription = new Subscription();
        $subscription->addScope($scope);

        $this->assertCount(1, $subscription->getScopes());

        $subscription->removeScope($scope);
        $this->assertCount(0, $subscription->getScopes());
    }

    /**
     * @test
     */
    public function it_should_remove_position_properly()
    {
        $position = new Position();
        $subscription = new Subscription();
        $subscription->addPosition($position);

        $this->assertCount(1, $subscription->getPositions());

        $subscription->removePosition($position);
        $this->assertCount(0, $subscription->getPositions());
    }

    /**
     * @test
     */
    public function it_should_set_email_properly()
    {
        $email = 'vytautas@eface.lt';

        $subscription = new Subscription();
        $result = $subscription->setEmail($email);
        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals($email, $result->getEmail());
    }

    /**
     * @test
     */
    public function it_should_set_user_properly()
    {
        $user = new RegisteredUser();

        $subscription = new Subscription();
        $result = $subscription->setUser($user);
        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertInstanceOf('RDW\Bundle\UserBundle\Entity\RegisteredUser', $result->getUser());
    }

    /**
     * @test
     */
    public function it_should_set_token_properly()
    {
        $token = 'my_secret_token';
        $subscription = new Subscription();
        $subscription->setToken($token);

        $this->assertEquals($token, $subscription->getToken());
    }

    /**
     * @test
     */
    public function it_has_no_id_by_default()
    {
        $subscription = new Subscription();

        $this->assertNull($subscription->getId());
    }

    /**
     * @test
     */
    public function it_should_return_frequency_choice_list()
    {
        $expectedResult =  [
            Subscription::FREQUENCY_DAILY => 'Daily',
            Subscription::FREQUENCY_WEEKLY => 'Weekly',
            Subscription::FREQUENCY_TWO_WEEKS => 'Once per two weeks',
        ];

        $subscription = new Subscription();

        $this->assertEquals($expectedResult, $subscription->getFrequencyChoiceList());
    }
}
