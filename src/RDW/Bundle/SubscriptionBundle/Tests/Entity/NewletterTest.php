<?php

namespace RDW\Bundle\SubscriptionBundle\Tests\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use RDW\Bundle\JobBundle\Entity\Position;
use RDW\Bundle\JobBundle\Entity\WorkScheduleType;
use RDW\Bundle\LocationBundle\Entity\City;
use RDW\Bundle\SubscriptionBundle\Entity\Newsletter;
use RDW\Bundle\SubscriptionBundle\Entity\Subscription;
use RDW\Bundle\UserBundle\Entity\Scope;
use RDW\Bundle\UserBundle\Entity\User;

/**
 * Class NewsletterTest
 *
 * @package RDW\Bundle\SubscriptionBundle\Tests\Entity
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class NewsletterTest extends \PHPUnit_Framework_TestCase
{
    const CLASS_NAME = 'RDW\Bundle\SubscriptionBundle\Entity\Newsletter';

    /**
     * @test
     */
    public function it_should_set_email_properly()
    {
        $email = 'test@example.com';

        $newsletter = new Newsletter();
        $result = $newsletter->setEmail($email);
        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals($email, $result->getEmail());
    }

    /**
     * @test
     */
    public function it_should_set_name_properly()
    {
        $name = 'test-name';

        $newsletter = new Newsletter();
        $result = $newsletter->setName($name);
        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals($name, $result->getName());
    }

    /**
     * @test
     */
    public function it_should_set_surname_properly()
    {
        $surname = 'test-surname';

        $newsletter = new Newsletter();
        $result = $newsletter->setSurname($surname);
        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals($surname, $result->getSurname());
    }

    /**
     * @test
     */
    public function it_should_set_token_properly()
    {
        $token = 'token';

        $newsletter = new Newsletter();
        $result = $newsletter->setToken($token);
        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals($token, $result->getToken());
    }

    /**
     * @test
     */
    public function it_should_set_subscribed_at_properly()
    {
        $date = new \DateTime('-2DAYS');
        $newsletter = new Newsletter();
        $newsletter->setSubscribedAt($date);

        $this->assertInstanceOf('\DateTime', $newsletter->getSubscribedAt());
        $this->assertEquals($date, $newsletter->getSubscribedAt());
    }

    /**
     * @test
     */
    public function its_should_not_id_by_default()
    {
        $newsletter = new Newsletter();
        $this->assertNull($newsletter->getId());
    }
}
