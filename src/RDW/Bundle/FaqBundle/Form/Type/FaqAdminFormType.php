<?php

namespace RDW\Bundle\FaqBundle\Form\Type;

use RDW\Bundle\FaqBundle\Entity\Faq;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class FaqAdminFormType
 *
 * @author Vytautas Beliunas <vytautas@eface.lt>
 */
class FaqAdminFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $faq = $builder->getForm()->getData();

        $builder
            ->add('name', 'text', [
                'required' => true,
                'mapped' => false,
                'data' => $faq->getName(),
                'read_only' => true,
                'label' => 'Name',
            ])
            ->add('email', 'email', [
                'required' => true,
                'mapped' => false,
                'data' => $faq->getEmail(),
                'read_only' => true,
                'label' => 'Email',
            ])
            ->add('question', 'text', [
                'required' => true,
                'label' => 'Question',
            ])
            ->add('answer', 'ckeditor', [
                'required' => true,
                'label' => 'Answer',
            ])
            ->add('status', 'choice', [
                'choices'   => Faq::getStatuses(),
                'required' => true,
                'label' => 'Status',
            ])
            ->add('reply', 'submit', [
                'label' => 'Reply'
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\FaqBundle\Entity\Faq',
            'validation_groups' => [],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_faq_admin';
    }
}