<?php

namespace RDW\Bundle\FaqBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class FaqFormType
 *
 * @author Vytautas Beliunas <vytautas@eface.lt>
 */
class FaqFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', [
                'required' => true,
                'label' => 'Name',
                'attr' => [
                    'placeholder' => 'Name placeholder'
                ]
            ])
            ->add('email', 'email', [
                'required' => true,
                'label' => 'Email',
                'attr' => [
                    'placeholder' => 'Email placeholder'
                ]
            ])
            ->add('question', 'textarea', [
                'required' => true,
                'label' => 'Question',
                'attr' => [
                    'placeholder' => 'Question placeholder'
                ]
            ])
            ->add('sumCaptcha', 'sum_captcha', [
                /** @Ignore */
                'label' => false
            ])
            ->add('ask', 'submit', [
                'label' => 'Ask'
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\FaqBundle\Entity\Faq',
            'validation_groups' => ['ask'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_faq';
    }
}