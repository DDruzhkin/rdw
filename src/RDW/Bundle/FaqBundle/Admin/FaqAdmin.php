<?php

namespace RDW\Bundle\FaqBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use RDW\Bundle\FaqBundle\Entity\Faq;

class FaqAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('name', 'text', [
                'required' => true,
                //'mapped' => false,
                'read_only' => true,
                'label' => 'Name',
            ])
            ->add('email', 'email', [
                'required' => true,
                //'mapped' => false,
                'read_only' => true,
                'label' => 'Email',
            ])
            ->add('question', 'text', [
                'required' => true,
                'label' => 'Question',
            ])
            ->add('answer', 'ckeditor', [
                'required' => true,
                'label' => 'Answer',
            ])
            ->add('status', 'choice', [
                'choices'   => Faq::getStatuses(),
                'required' => true,
                'label' => 'Status',
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        //$datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name')
            ->add('email')
            ->add('question')
            ;

        $listMapper->add('_action', 'actions', array('actions' => array(
            'edit'       => array(),
            'delete'     => array('label' => 'delete')
        )));
    }
}