<?php
namespace RDW\Bundle\FaqBundle\Controller;

use RDW\Bundle\FaqBundle\Entity\Faq;
use RDW\Bundle\FaqBundle\Event\FaqEvent;
use RDW\Bundle\FaqBundle\RDWFaqEvents;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use RDW\Bundle\FaqBundle\Service\FaqManager;

/**
 * FAQ controller
 *
 * @Route("/faq")
 */
class FaqController extends Controller
{
    /**
     * @Route("/", name="rdw_faq.faq.index")
     * @Method({"GET"})
     * @Template()
     *
     * @return array
     */
    public function indexAction()
    {
        $form = $this->createForm('rdw_faq', new Faq());

        return [
            'form' => $form->createView(),
            'list' => $this->get('rdw.faq.repository')->findActive()
        ];
    }

    /**
     * @Route("/new", name="rdw_faq.faq.new")
     * @Method({"GET"})
     * @Template()
     */
    public function newAction()
    {
        $faq = new Faq();
        $form = $this->createForm('rdw_faq', $faq);

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @param Request $request
     *
     * @Route("/ask", name="rdw_faq.faq.ask")
     * @Method({"POST"})
     *
     * @return array
     */
    public function askAction(Request $request)
    {
        $data['success'] = false;
        $response = new JsonResponse();

        $faq = new Faq();
        $form = $this->createForm('rdw_faq', $faq);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getFaqManager()->update($faq);
            $this->get('braincrafted_bootstrap.flash')->success('Faq successfully sent');

            $event = new FaqEvent($faq);
            $this->getDispatcher()->dispatch(RDWFaqEvents::ASK_QUESTION_SUCCESS, $event);

            $data['success'] = true;
        } else {
            $this->get('session')->set('_digit1', null);
            $this->get('session')->set('_digit2', null);
        }

        $data['content'] = $this->renderView('RDWFaqBundle:Faq:_form.html.twig', [
            'form' => $form->createView()
        ]);

        $response->setData($data);

        return $response;
    }

    /**
     * @return FaqManager
     */
    protected function getFaqManager()
    {
        return $this->get('rdw_faq.service.faq_manager');
    }

    /**
     * @return \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    protected function getDispatcher()
    {
        return $this->container->get('event_dispatcher');
    }
}