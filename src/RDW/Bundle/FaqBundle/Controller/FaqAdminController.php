<?php
namespace RDW\Bundle\FaqBundle\Controller;

use RDW\Bundle\FaqBundle\RDWFaqEvents;
use RDW\Bundle\FaqBundle\Event\FaqEvent;
use RDW\Bundle\FaqBundle\Service\FaqManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * FAQ list managing controller
 *
 * @Route("/manage/faq")
 */
class FaqAdminController extends Controller
{
    /**
     * @Route("/list", name="rdw_faq.faq_admin.list")
     * @Method({"GET"})
     * @Template()
     *
     * @return array
     */
    public function listAction()
    {
        return [
            'items' => $this->getFaqManager()->getFaqsList(),
        ];
    }

    /**
     * @param int $id
     *
     * @Route("/edit/{id}", requirements={"id" = "\d+"}, name="rdw_faq.faq_admin.edit")
     * @Method({"GET"})
     * @Template()
     *
     * @return array
     * @throws NotFoundHttpException
     */
    public function editAction($id)
    {
        $this->isGrantedLocal();

        $faq = $this->getFaqManager()->findById($id);

        if ( ! $faq) {
            throw new NotFoundHttpException(sprintf('The faq with id "%d" does not exist.', $id));
        }

        $form = $this->createForm('rdw_faq_admin', $faq);

        return [
            'form' => $form->createView(),
            'faq' => $faq,
        ];
    }

    /**
     * @param Request $request
     * @param int     $id
     *
     * @Route("/update/{id}", requirements={"id" = "\d+"}, name="rdw_faq.faq_admin.update")
     * @Method({"POST"})
     * @Template("RDWFaqBundle:FaqAdmin:edit.html.twig")
     *
     * @return array|Response
     * @throws NotFoundHttpException
     */
    public function updateAction(Request $request, $id)
    {
        $this->isGrantedLocal();

        $faq = $this->getFaqManager()->findById($id);

        if ( ! $faq) {
            throw new NotFoundHttpException(sprintf('The faq with id "%d" does not exist.', $id));
        }

        $form = $this->createForm('rdw_faq_admin', $faq);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getFaqManager()->update($faq);

            $event = new FaqEvent($faq);
            $this->getDispatcher()->dispatch(RDWFaqEvents::FAQ_REPLY_SUCCESS, $event);

            return new RedirectResponse($this->generateUrl('rdw_faq.faq_admin.list'));
        }

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @param int $id
     *
     * @Route("/delete/{id}", requirements={"id" = "\d+"}, name="rdw_faq.faq_admin.delete")
     *
     * @return RedirectResponse
     * @throws NotFoundHttpException
     */
    public function deleteAction($id)
    {
        $this->isGrantedLocal();

        $faq = $this->getFaqManager()->findById($id);

        if ( ! $faq) {
            throw new NotFoundHttpException(sprintf('The faq with id "%d" does not exist.', $id));
        }

        $this->getFaqManager()->deleteFaq($faq);

        return new RedirectResponse($this->generateUrl('rdw_faq.faq_admin.list'));
    }

    /**
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    private function isGrantedLocal()
    {
        if ( ! $this->container->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }
    }

    /**
     * @return FaqManager
     */
    protected function getFaqManager()
    {
        return $this->get('rdw_faq.service.faq_manager');
    }

    /**
     * @return \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    protected function getDispatcher()
    {
        return $this->container->get('event_dispatcher');
    }
}