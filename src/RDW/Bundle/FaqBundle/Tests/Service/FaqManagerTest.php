<?php
/**
 * Created by PhpStorm.
 * User: vb
 * Date: 12/12/14
 * Time: 3:39 PM
 */

namespace RDW\Bundle\FaqBundle\Tests\Service;

use RDW\Bundle\FaqBundle\Entity\Faq;
use RDW\Bundle\FaqBundle\Service\FaqManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FaqManagerTest extends WebTestCase {

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $em;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $repo;

    /**
     * @var FaqManager
     */
    private $faqManager;

    private $faq;

    /**
     * set up
     */
    public function setUp()
    {
        $client = static::createClient();
        $container = $client->getContainer();

        $this->em = $this->getMockBuilder('\Doctrine\Common\Persistence\ObjectManager')->getMock();
        $this->repo = $this->getMockBuilder('Doctrine\ORM\EntityRepository')->disableOriginalConstructor()->getMock();
        $this->faq = $this->getMock('RDW\Bundle\FaqBundle\Entity\Faq');
        $this->faqManager = new FaqManager($this->em);

        $this->em
            ->expects($this->any())
            ->method('getRepository')
            ->with($this->equalTo('RDWFaqBundle:Faq'))
            ->willReturn($this->repo);
    }

    /**
     * @test
     */
    public function it_should_find_by_id()
    {
        $this->repo
            ->expects($this->once())
            ->method('find')
            ->with($this->equalTo(11))
            ->will($this->returnValue($this->faq));

        $this->assertEquals($this->faq, $this->faqManager->findById(11));
    }

    /**
     * @test
     */
    public function it_should_persist_and_not_flush()
    {
        $object = new Faq();

        $this->em
            ->expects($this->once())
            ->method('persist')
            ->will($this->returnValue(true));

        $this->em
            ->expects($this->never())
            ->method('flush')
            ->will($this->returnValue(true));

        $result = $this->faqManager->update($object, false);
        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function it_should_delete_faq()
    {
        $object = new Faq();

        $this->em
            ->expects($this->once())
            ->method('remove')
            ->with($this->equalTo($object))
            ->will($this->returnValue(true));

        $this->em
            ->expects($this->once())
            ->method('flush')
            ->will($this->returnValue(true));

        $result = $this->faqManager->deleteFaq($object);
        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function it_should_get_faq_list()
    {
        $result = ['0' => 'result'];

        $this->repo
            ->expects($this->once())
            ->method('findBy')
            ->with($this->equalTo([]), $this->equalTo(['id' => 'desc']))
            ->will($this->returnValue($result));

        $this->assertEquals($result, $this->faqManager->getFaqsList());
    }

    /**
     * @test
     */
    public function it_should_persist_and_flush()
    {
        $object = new Faq();

        $this->em
            ->expects($this->once())
            ->method('persist')
            ->will($this->returnValue(true));

        $this->em
            ->expects($this->once())
            ->method('flush')
            ->will($this->returnValue(true));

        $result = $this->faqManager->update($object, true);
        $this->assertTrue($result);
    }
}
