<?php
/**
 * Created by PhpStorm.
 * User: vb
 * Date: 12/12/14
 * Time: 12:25 PM
 */

namespace RDW\Bundle\FaqBundle\Tests\Entity;

use RDW\Bundle\FaqBundle\Entity\Faq;

class FaqTest extends \PHPUnit_Framework_TestCase {

    const CLASS_NAME = 'RDW\Bundle\FaqBundle\Entity\Faq';

    /**
     * @var Faq
     */
    private $faq;

    /**
     * set up
     */
    public function setUp()
    {
        $this->faq = new Faq();
    }

    /**
     * @test
     */
    public function it_should_no_id_by_default()
    {
        $this->assertNull($this->faq->getId());
    }

    /**
     * @test
     */
    public function it_should_set_name_properly()
    {
        $result = $this->faq->setName('name');

        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals('name', $this->faq->getName());
    }

    /**
     * @test
     */
    public function it_should_set_email_properly()
    {
        $result = $this->faq->setEmail('test@example.com');

        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals('test@example.com', $this->faq->getEmail());
    }

    /**
     * @test
     */
    public function it_should_set_question_properly()
    {
        $result = $this->faq->setQuestion('question');

        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals('question', $this->faq->getQuestion());
    }

    /**
     * @test
     */
    public function it_should_set_answer_properly()
    {
        $result = $this->faq->setAnswer('answer');

        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals('answer', $this->faq->getAnswer());
    }

    /**
     * @test
     */
    public function it_should_set_status_properly()
    {
        $result = $this->faq->setStatus(Faq::STATUS_ACTIVE);

        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals(Faq::STATUS_ACTIVE, $this->faq->getStatus());
    }

    /**
     * @test
     */
    public function it_should_allow_send_email_if_replied_and_activated()
    {
        $this->faq->setAnswer('not null answer');
        $this->faq->setStatus(Faq::STATUS_ACTIVE);

        $this->assertTrue($this->faq->isSendReplyEmail());
    }

    /**
     * @test
     */
    public function it_should_not_allow_send_email_if_not_replied()
    {
        $this->assertFalse($this->faq->isSendReplyEmail());
    }

    /**
     * @test
     */
    public function it_should_not_allow_send_email_if_not_active()
    {
        $this->assertFalse($this->faq->isSendReplyEmail());
    }

    /**
     * @test
     */
    public function it_should_return_statuses_array()
    {
        $statuses = [
            Faq::STATUS_ACTIVE => 'Active',
            Faq::STATUS_HIDDEN => 'Hidden',
        ];

        $this->assertCount(2, $this->faq->getStatuses());
        $this->assertEquals($statuses, $this->faq->getStatuses());
    }

    /**
     * @test
     */
    public function it_should_non_active_by_default()
    {
        $this->assertFalse($this->faq->isActive());
    }

    /**
     * @test
     */
    public function it_should_check_is_active()
    {
        $this->faq->setStatus(Faq::STATUS_ACTIVE);
        $this->assertTrue($this->faq->isActive());
    }

    /**
     * @test
     */
    public function it_should_set_deleted_at_properly()
    {
        $date = new \DateTime('-2DAYS');
        $result = $this->faq->setDeletedAt($date);

        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertInstanceOf('\DateTime', $this->faq->getDeletedAt());
        $this->assertEquals($date, $this->faq->getDeletedAt());
    }
}
