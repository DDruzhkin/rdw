<?php
/**
 * Created by PhpStorm.
 * User: vb
 * Date: 12/12/14
 * Time: 12:44 PM
 */

namespace RDW\Bundle\FaqBundle\Tests\EventListener;

use RDW\Bundle\FaqBundle\EventListener\ReplyListener;
use RDW\Bundle\FaqBundle\RDWFaqEvents;

class ReplyListenerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $mailerManager;

    /**
     * @var ReplyListener
     */
    protected $replyListener;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $faqEvent;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $faq;

    /**
     * Set up
     */
    public function setUp()
    {
        $this->mailerManager = $this->getMockBuilder('RDW\Bundle\MailerBundle\Service\MailerManager')->disableOriginalConstructor()->getMock();
        $this->faq = $this->getMock('RDW\Bundle\FaqBundle\Entity\Faq');
        $this->faqEvent = $this->getMockBuilder('RDW\Bundle\FaqBundle\Event\FaqEvent')->disableOriginalConstructor()->getMock();

        $this->replyListener = new ReplyListener($this->mailerManager);
    }

    /**
     * @test
     */
    public function it_should_send_email_for_author()
    {
        $this->faqEvent
            ->expects($this->once())
            ->method('getFaq')
            ->willReturn($this->faq);

        $this->mailerManager
            ->expects($this->once())
            ->method('sendFaqReplyEmail')
            ->with($this->equalTo($this->faq));

        $this->faq
            ->expects($this->once())
            ->method('isSendReplyEmail')
            ->willReturn(true);

        $this->assertTrue($this->replyListener->onReplySuccess($this->faqEvent));
    }

    /**
     * @test
     */
    public function it_should_not_send_email_for_author_is_faq_is_not_ready()
    {
        $this->faqEvent
            ->expects($this->once())
            ->method('getFaq')
            ->willReturn($this->faq);

        $this->mailerManager
            ->expects($this->never())
            ->method('sendFaqReplyEmail')
            ->with($this->equalTo($this->faq));

        $this->faq
            ->expects($this->once())
            ->method('isSendReplyEmail')
            ->willReturn(false);

        $this->assertTrue($this->replyListener->onReplySuccess($this->faqEvent));
    }

    /**
     * @test
     * @expectedException \Symfony\Component\Form\Exception\UnexpectedTypeException
     */
    public function it_should_throw_exception_during_send_email_for_admin()
    {
        $this->faqEvent
            ->expects($this->once())
            ->method('getFaq')
            ->willReturn(null);

        $this->replyListener->onReplySuccess($this->faqEvent);
    }

    /**
     * @test
     */
    public function it_should_return_subscribed_events()
    {
        $subscribedEvents =  [
            RDWFaqEvents::FAQ_REPLY_SUCCESS => 'onReplySuccess',
        ];

        $this->assertEquals($subscribedEvents, $this->replyListener->getSubscribedEvents());
    }
}
