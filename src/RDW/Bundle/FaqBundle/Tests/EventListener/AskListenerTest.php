<?php
/**
 * Created by PhpStorm.
 * User: vb
 * Date: 12/12/14
 * Time: 12:44 PM
 */

namespace RDW\Bundle\FaqBundle\Tests\EventListener;


use RDW\Bundle\FaqBundle\EventListener\AskListener;
use RDW\Bundle\FaqBundle\RDWFaqEvents;

class AskListenerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $mailerManager;

    /**
     * @var AskListener
     */
    protected $askListener;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $faqEvent;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $faq;

    /**
     * Set up
     */
    public function setUp()
    {
        $this->mailerManager = $this->getMockBuilder('RDW\Bundle\MailerBundle\Service\MailerManager')->disableOriginalConstructor()->getMock();
        $this->faq = $this->getMock('RDW\Bundle\FaqBundle\Entity\Faq');
        $this->faqEvent = $this->getMockBuilder('RDW\Bundle\FaqBundle\Event\FaqEvent')->disableOriginalConstructor()->getMock();

        $this->askListener = new AskListener($this->mailerManager);
    }

    /**
     * @test
     */
    public function it_should_send_email_for_admin()
    {
        $this->faqEvent
            ->expects($this->once())
            ->method('getFaq')
            ->willReturn($this->faq);

        $this->mailerManager
            ->expects($this->once())
            ->method('sendAskNewFaqForAdminEmail')
            ->with($this->equalTo($this->faq));

        $this->assertTrue($this->askListener->onAskSuccess($this->faqEvent));
    }

    /**
     * @test
     * @expectedException \Symfony\Component\Form\Exception\UnexpectedTypeException
     */
    public function it_should_throw_exception_during_send_email_for_admin()
    {
        $this->faqEvent
            ->expects($this->once())
            ->method('getFaq')
            ->willReturn(null);

        $this->askListener->onAskSuccess($this->faqEvent);
    }

    /**
     * @test
     */
    public function it_should_return_subscribed_events()
    {
        $subscribedEvents =  [
            RDWFaqEvents::ASK_QUESTION_SUCCESS => 'onAskSuccess',
        ];

        $this->assertEquals($subscribedEvents, $this->askListener->getSubscribedEvents());
    }
}
