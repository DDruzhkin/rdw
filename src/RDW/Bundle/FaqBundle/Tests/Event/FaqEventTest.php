<?php
/**
 * Created by PhpStorm.
 * User: vb
 * Date: 12/12/14
 * Time: 12:40 PM
 */

namespace RDW\Bundle\FaqBundle\Tests\Event;

use RDW\Bundle\FaqBundle\Entity\Faq;
use RDW\Bundle\FaqBundle\Event\FaqEvent;

class FaqEventTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var FaqEvent
     */
    private $faqEvent;

    /**
     * set up
     */
    public function setUp()
    {
        $this->faqEvent = new FaqEvent(new Faq());
    }

    /**
     * @test
     */
    public function it_should_be_constructed_with_faq_object_by_default()
    {
        $this->assertInstanceOf('RDW\Bundle\FaqBundle\Entity\Faq', $this->faqEvent->getFaq());
    }
}
