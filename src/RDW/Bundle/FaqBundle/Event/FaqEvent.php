<?php

namespace RDW\Bundle\FaqBundle\Event;

use RDW\Bundle\FaqBundle\Entity\Faq;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class FaqEvent
 *
 * @package Faq
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class FaqEvent extends Event
{
    private $faq;

    /**
     * Constructor
     *
     * @param Faq $faq
     */
    public function __construct(Faq $faq)
    {
        $this->faq = $faq;
    }

    /**
     * @return Faq
     */
    public function getFaq()
    {
        return $this->faq;
    }
}