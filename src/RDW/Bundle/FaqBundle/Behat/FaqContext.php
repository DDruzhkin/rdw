<?php

namespace RDW\Bundle\FaqBundle\Behat;

use Behat\Gherkin\Node\TableNode;
use RDW\Bundle\AppBundle\Behat\DefaultContext;
use RDW\Bundle\FaqBundle\Entity\Faq;

/**
 * Class FaqContext. Context related with faqs
 */
class FaqContext extends DefaultContext
{
    /**
     * @param TableNode $table
     *
     * @Given /^there are following faqs:$/
     */
    public function thereAreFollowingCities(TableNode $table)
    {
        foreach ($table->getHash() as $data) {
            $this->thereIsFaq(
                $data['name'],
                $data['email'],
                $data['question'],
                $data['answer'],
                $data['status']
            );
        }

        $this->getEntityManager()->flush();
    }

    /**
     * @Given /^I should be on the manage faq list page$/
     */
    public function iShouldBeTheOnManageFaqListPage()
    {
        $this->assertSession()->addressEquals($this->generateUrl('rdw_faq.faq_admin.list'));
    }

    /**
     * @Given /^I should be on the ask faq page$/
     */
    public function iShouldBeTheOnAskFaqPage()
    {
        $this->assertSession()->addressEquals($this->generateUrl('rdw_faq.faq.new'));
    }

    /**
     * @Given /^I am on the manage faq list page$/
     */
    public function iAmOnTheManageFaqListPage()
    {
        $this->getSession()->visit($this->generateUrl('rdw_faq.faq_admin.list'));
    }

    /**
     * @Given /^I am on the ask faq page$/
     */
    public function iAmOnTheAskFaqPage()
    {
        $this->getSession()->visit($this->generateUrl('rdw_faq.faq.new'));
    }

    /**
     * @param string $name
     *
     * @throws \InvalidArgumentException
     *
     * @Given /^I should be on reply faq page with name "([^"]*)"$/
     */
    public function iShouldBeOnReplyFaqPage($name)
    {
        $faq = $this->findFaqByName($name);

        if ($faq instanceof Faq) {
            $this->assertSession()->addressEquals($this->generateUrl('rdw_faq.faq_admin.edit', ['id' => $faq->getId()]));
        } else {
            throw new \InvalidArgumentException(sprintf('Cannot find faq with name: "%s"', $name));
        }
    }

    /**
     * @param string $name
     * @param string $email
     * @param string $question
     * @param string $answer
     * @param string $status
     * @param bool   $flush
     *
     * @return Faq
     */
    public function thereIsFaq($name, $email, $question, $answer, $status, $flush = true)
    {
        $faq = new Faq();
        $faq->setName($name);
        $faq->setEmail($email);
        $faq->setQuestion($question);
        $faq->setAnswer($answer);

        if ($status == 'active') {
            $faq->setStatus(Faq::STATUS_ACTIVE);
        } else {
            $faq->setStatus(Faq::STATUS_HIDDEN);
        }

        $this->getEntityManager()->persist($faq);

        if ($flush) {
            $this->getEntityManager()->flush();
        }

        return $faq;
    }

    /**
     * @param string $name
     *
     * @return Faq|null
     */
    protected function findFaqByName($name)
    {
        return $this->getEntityManager()->getRepository('RDWFaqBundle:Faq')->findOneBy(['name' => $name]);
    }
}
