<?php

namespace RDW\Bundle\FaqBundle;

/**
 * Contains all events thrown in the RDWFaqBundle
 *
 * Class RDWFaqEvents
 *
 * @package Faq
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
final class RDWFaqEvents
{
    /**
     * The FAQ_REPLY_SUCCESS event occurs when the system Administrator successfully replies to asked question
     */
    const FAQ_REPLY_SUCCESS = 'rdw_faq.event.faq_reply_success';

    /**
     * The ASK_QUESTION_SUCCESS event occurs when the site visitor successfully ask question in FAQ's page
     */
    const ASK_QUESTION_SUCCESS = 'rdw_faq.event.faq_ask_success';
}