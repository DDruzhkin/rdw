<?php

namespace RDW\Bundle\FaqBundle\EventListener;

use RDW\Bundle\FaqBundle\RDWFaqEvents;
use RDW\Bundle\FaqBundle\Entity\Faq;
use RDW\Bundle\FaqBundle\Event\FaqEvent;
use RDW\Bundle\MailerBundle\Service\MailerManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;

/**
 * Class AskListener
 *
 * @package Faq
 */
class AskListener implements EventSubscriberInterface
{
    /**
     * @var MailerManager
     */
    protected $mailerManager;

    /**
     * @param MailerManager $mailerManager
     */
    public function __construct(MailerManager $mailerManager)
    {
        $this->mailerManager = $mailerManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            RDWFaqEvents::ASK_QUESTION_SUCCESS => 'onAskSuccess',
        ];
    }

    /**
     * Send email for admin after new question is asked in FAQ
     *
     * @param FaqEvent $event
     *
     * @return true;
     *
     * @throws UnexpectedTypeException
     */
    public function onAskSuccess(FaqEvent $event)
    {
        $faq = $event->getFaq();

        if (!$faq instanceof Faq) {
            throw new UnexpectedTypeException(
                $faq,
                'RDW\Bundle\FaqBundle\Entity\Faq'
            );
        }

        $this->mailerManager->sendAskNewFaqForAdminEmail($faq);

        return true;
    }
}
