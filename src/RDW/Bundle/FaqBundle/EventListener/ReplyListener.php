<?php

namespace RDW\Bundle\FaqBundle\EventListener;

use RDW\Bundle\FaqBundle\RDWFaqEvents;
use RDW\Bundle\FaqBundle\Entity\Faq;
use RDW\Bundle\FaqBundle\Event\FaqEvent;
use RDW\Bundle\MailerBundle\Service\MailerManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;

/**
 * Class ReplyListener replies to asked question in FAQ
 *
 * @package Faq
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class ReplyListener implements EventSubscriberInterface
{
    /**
     * @var MailerManager
     */
    protected $mailerManager;

    /**
     * @param MailerManager $mailerManager
     */
    public function __construct(MailerManager $mailerManager)
    {
        $this->mailerManager = $mailerManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            RDWFaqEvents::FAQ_REPLY_SUCCESS => 'onReplySuccess',
        ];
    }

    /**
     * Send email for person who asked question when admin replies
     *
     * @param FaqEvent $event
     *
     * @throws UnexpectedTypeException
     */
    public function onReplySuccess(FaqEvent $event)
    {
        $faq = $event->getFaq();

        if (!$faq instanceof Faq) {
            throw new UnexpectedTypeException(
                $faq,
                'RDW\Bundle\FaqBundle\Entity\Faq'
            );
        }

        if ($faq->isSendReplyEmail()) {
            $this->mailerManager->sendFaqReplyEmail($faq);
        }

        return true;
    }
}
