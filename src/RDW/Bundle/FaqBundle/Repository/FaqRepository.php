<?php

namespace RDW\Bundle\FaqBundle\Repository;

use Doctrine\ORM\EntityRepository;
use RDW\Bundle\FaqBundle\Entity\Faq;

/**
 * Class FaqRepository
 *
 * @package RDW\Bundle\FaqBundle\Repository
 */
class FaqRepository extends EntityRepository
{
    /**
     * Find active list
     *
     * @return array
     */
    public function findActive()
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('f')
            ->from('RDWFaqBundle:Faq', 'f')
            ->orderBy('f.id', 'DESC')
            ->andWhere('f.status = :status')
            ->setParameter('status', Faq::STATUS_ACTIVE);

        return $queryBuilder->getQuery()->getResult();
    }
}
