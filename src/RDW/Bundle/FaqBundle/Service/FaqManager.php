<?php
namespace RDW\Bundle\FaqBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\FaqBundle\Entity\Faq;

/**
 *  Faq service object
 */
class FaqManager
{
    protected $objectManager;

    /**
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @return array
     */
    public function getFaqsList()
    {
        return $this->objectManager->getRepository('RDWFaqBundle:Faq')->findBy([], ['id' => 'desc']);
    }

    /**
     * @param int $id
     *
     * @return Faq
     */
    public function findById($id)
    {
        return $this->getRepository()->find($id);
    }

    /**
     * @param Faq  $faq
     * @param bool $andFlush
     *
     * @return bool
     */
    public function update(Faq $faq, $andFlush = true)
    {
        $this->objectManager->persist($faq);

        if ($andFlush) {
            $this->objectManager->flush();
        }

        return true;
    }

    /**
     * @param Faq $faq
     *
     * @return bool
     */
    public function deleteFaq(Faq $faq)
    {
        $this->objectManager->remove($faq);
        $this->objectManager->flush();

        return true;
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    protected function getRepository()
    {
        return $this->objectManager->getRepository('RDWFaqBundle:Faq');
    }
}