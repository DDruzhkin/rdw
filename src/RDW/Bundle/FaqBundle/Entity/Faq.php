<?php

namespace RDW\Bundle\FaqBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Main faq entity class
 *
 * @ORM\Entity(repositoryClass="RDW\Bundle\FaqBundle\Repository\FaqRepository")
 * @ORM\Table(name="faqs")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Faq
{
    const STATUS_ACTIVE = 1;
    const STATUS_HIDDEN = 2;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Name cannot be empty", groups={"ask"})
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Email cannot be empty", groups={"ask"})
     * @Assert\Email(message="Email must be a valid email address", groups={"ask"})
     */
    protected $email;

    /**
     * @var string
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank(message="Question cannot be empty", groups={"ask"})
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "Your question cannot be longer than {{ limit }} characters",
     *      groups={"ask"}
     * )
     */
    protected $question;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $answer;

    /**
     * @var int
     * @ORM\Column(type="smallint")
     */
    protected $status;

    /**
     * @var \DateTime
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * constructor
     */
    public function __construct()
    {
        $this->status = self::STATUS_HIDDEN;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $question
     *
     * @return self
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param string $answer
     *
     * @return self
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return ($this->getStatus() == self::STATUS_ACTIVE) ? true : false;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     */
    public function setDeletedAt(\DateTime $deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return array(
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_HIDDEN => 'Hidden',
        );
    }

    /**
     * @return bool
     */
    public function isSendReplyEmail()
    {
        return ($this->getAnswer() && $this->isActive() ? true : false);
    }
}
