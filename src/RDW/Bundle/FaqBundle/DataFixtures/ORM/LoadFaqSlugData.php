<?php

namespace RDW\Bundle\FaqBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\SlugBundle\Entity\Slug;

/**
 * Class LoadFaqSlugData
 *
 * @package RDW\Bundle\FaqBundle\DataFixtures\ORM
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class LoadFaqSlugData extends AbstractFixture
{
    const REFERENCE_STRING = 'faq-slug';

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $entityManager)
    {
        $slug = new Slug();
        $slug->setController('RDWFaqBundle:Faq:index');
        $slug->setName('faq');
        $slug->setOrigName('faq');
        $slug->setSystem(true);

        $this->addReference('faq-slug', $slug);

        $entityManager->persist($slug);
        $entityManager->flush();
    }
}