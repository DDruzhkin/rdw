<?php

namespace RDW\Bundle\FaqBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\FaqBundle\Entity\Faq;
use RDW\Bundle\LocationBundle\Entity\City;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Faker\Factory as FakerFactory;
use Faker\Generator;

/**
 * Loads faq data
 *
 * @author Vytautas Beliunas <vytautas@eface.lt>
 */
class LoadFaqData extends AbstractFixture
{
    /**
     * Faker.
     *
     * @var Generator
     */
    protected $faker;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->faker = FakerFactory::create();
    }

    /**
     * Loads fixture
     *
     * @param ObjectManager $manager Object manager
     *
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $totalOfFaqs = 20;

        for ($i = 0; $i <= $totalOfFaqs; $i++) {
            $faq = new Faq();
            $faq->setName($this->faker->name);
            $faq->setEmail('test' . $i . '@example.com');
            $faq->setAnswer($this->faker->text(500));
            $faq->setQuestion($this->faker->sentence());
            $faq->setStatus(Faq::STATUS_ACTIVE);

            $manager->persist($faq);
        }

        $manager->flush();
    }
}