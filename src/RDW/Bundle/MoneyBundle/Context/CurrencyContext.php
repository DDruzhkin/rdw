<?php

namespace RDW\Bundle\MoneyBundle\Context;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class CurrencyContext
 *
 * @package RDW\Bundle\MoneyBundle\Context
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class CurrencyContext
{
    protected $session;
    protected $defaultCurrency;

    /**
     * @param SessionInterface $session
     * @param string           $defaultCurrency
     */
    public function __construct(SessionInterface $session, $defaultCurrency)
    {
        $this->session = $session;
        $this->defaultCurrency = $defaultCurrency;
    }

    /**
     * @return string
     */
    public function getDefaultCurrency()
    {
        return $this->defaultCurrency;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->session->get('currency', $this->defaultCurrency);
    }

    /**
     * @param string $currency
     *
     * @return mixed
     */
    public function setCurrency($currency)
    {
        return $this->session->set('currency', $currency);
    }
}
