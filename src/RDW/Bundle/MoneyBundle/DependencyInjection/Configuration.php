<?php

namespace RDW\Bundle\MoneyBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('rdw_money');

        $rootNode
            ->children()
            ->scalarNode('locale')->defaultValue('ru')->cannotBeEmpty()->end()
            ->scalarNode('currency')->defaultValue('byr')->cannotBeEmpty()->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
