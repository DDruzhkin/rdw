<?php

namespace RDW\Bundle\MoneyBundle\Templating\Helper;

use RDW\Bundle\MoneyBundle\Context\CurrencyContext;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Templating\Helper\Helper;

/**
 * Class MoneyHelper
 *
 * @package Money
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class MoneyHelper extends Helper
{
    /**
     * @var CurrencyContext
     */
    private $currencyContext;

    /**
     * @var \NumberFormatter
     */
    private $formatter;

    /**
     * @var string
     */
    private $pattern = "@######## Â¤;-@######## Â¤";

    /**
     * @param CurrencyContext $currencyContext
     * @param string          $locale
     */
    public function __construct(CurrencyContext $currencyContext, $locale)
    {
        $locale = 'ru';
        $this->currencyContext = $currencyContext;

        $this->formatter       = new \NumberFormatter($locale ?: \Locale::ACTUAL_LOCALE, \NumberFormatter::PATTERN_DECIMAL);

        $this->formatter->setPattern($this->pattern);
    }

    /**
     * Format the money amount to nice display form.
     *
     * @param integer     $amount
     * @param string|null $currency
     *
     * @return string
     * @throws \InvalidArgumentException
     */
    public function formatMoney($amount, $currency = null)
    {
        $currency = $currency ?: $this->currencyContext->getDefaultCurrency();

        $result = sprintf('%s %s', $this->formatNumber($amount, true), $currency);

        /*$result   = $this->formatter->formatCurrency($amount, $currency);

        if (false === $result) {
            throw new \InvalidArgumentException(sprintf('The amount "%s" of type %s cannot be formatted to currency "%s".', $amount, gettype($amount), $currency));
        }*/

        return $result;
    }

    /**
     * Convert price between currencies and format the amount to nice display form.
     *
     * @param integer     $amount
     * @param string|null $currency
     *
     * @return string
     */
    public function formatPrice($amount, $currency = null)
    {
        $currency = $currency ?: $this->currencyContext->getCurrency();

        return $this->formatMoney($amount, $currency);
    }

    /**
     * Add thousands separator to number
     *
     * @param float $amount
     * @param bool  $withDecimals
     *
     * @return string
     */
    public function formatNumber($amount, $withDecimals = false)
    {
        $decimals = ($withDecimals) ? 2 : 0;

        return number_format($amount, $decimals, '.', ' ');
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_money';
    }
}
