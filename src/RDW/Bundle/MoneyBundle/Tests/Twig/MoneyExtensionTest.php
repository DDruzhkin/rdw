<?php
/**
 * Created by PhpStorm.
 * User: vb
 * Date: 1/20/15
 * Time: 1:51 PM
 */

namespace RDW\Bundle\MoneyBundle\Tests\Twig;

use RDW\Bundle\MoneyBundle\Twig\MoneyExtension;
use RDW\Bundle\MoneyBundle\Templating\Helper\MoneyHelper;

class MoneyExtensionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var MoneyHelper
     */
    protected $moneyHelper;

    /**
     * @var MoneyExtension
     */
    protected $moneyExtension;

    /**
     * set up
     */
    public function setUp()
    {
        $this->moneyHelper = $this->getMockBuilder('\RDW\Bundle\MoneyBundle\Templating\Helper\MoneyHelper')->disableOriginalConstructor()->getMock();
        $this->moneyExtension = new MoneyExtension($this->moneyHelper);
    }

    /**
     * @test
     */
    function it_should_return_proper_functions()
    {
        $filters = [
            new \Twig_SimpleFilter('price', [$this->moneyExtension, 'formatPrice']),
            new \Twig_SimpleFilter('priceWithoutCurrency', [$this->moneyExtension, 'formatPriceWithoutCurrency']),
        ];

        $this->assertEquals($filters, $this->moneyExtension->getFilters());
    }

    /**
     * @test
     */
    function it_should_format_price()
    {
        $this->moneyHelper
            ->expects($this->once())
            ->method('formatPrice')
            ->with($this->equalTo(15), $this->equalTo('USD'))
            ->willReturn('15 $');

        $this->assertEquals('15 $',  $this->moneyExtension->formatPrice(15, 'USD'));
    }

    /**
     * @test
     */
    function it_should_return_proper_name()
    {
        $this->assertEquals('rdw_money', $this->moneyExtension->getName());
    }
}
