<?php
/**
 * Created by PhpStorm.
 * User: vb
 * Date: 1/20/15
 * Time: 2:22 PM
 */

namespace RDW\Bundle\MoneyBundle\Tests\Context;


use RDW\Bundle\MoneyBundle\Context\CurrencyContext;
use Symfony\Component\HttpFoundation\Session\Session;

class CurrencyContextTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * @var CurrencyContext
     */
    protected $currencyContext;

    /**
     * set up
     */
    public function setUp()
    {
        $this->session = $this->getMockBuilder('\Symfony\Component\HttpFoundation\Session\Session')->disableOriginalConstructor()->getMock();
        $this->currencyContext = new CurrencyContext($this->session, 'EUR');
    }

    /**
     * @test
     */
    function it_should_return_default_currency()
    {
        $this->assertEquals('EUR', $this->currencyContext->getDefaultCurrency());
    }

    /**
     * @test
     */
    function it_should_set_currency_properly()
    {
        $this->session
            ->expects($this->once())
            ->method('set')
            ->with($this->equalTo('currency'), 'LTL');

        $this->currencyContext->setCurrency('LTL');
    }

    /**
     * @test
     */
    function it_should_get_currency_properly()
    {
        $this->session
            ->expects($this->once())
            ->method('get')
            ->with($this->equalTo('currency'), 'EUR')
            ->willReturn('USD');

        $this->assertEquals('USD', $this->currencyContext->getCurrency());
    }
}
