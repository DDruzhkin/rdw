<?php
/**
 * Created by PhpStorm.
 * User: vb
 * Date: 1/20/15
 * Time: 11:54 AM
 */

namespace RDW\Bundle\MoneyBundle\Tests\Templating\Helper;


use RDW\Bundle\MoneyBundle\Templating\Helper\MoneyHelper;

class MoneyHelperTest extends \PHPUnit_Framework_TestCase
{

    private $context;

    /**
     * @var MoneyHelper
     */
    private $moneyHelper;

    /**
     * @var \NumberFormatter
     */
    private $formatter;

    /**
     * set up
     */
    public function setUp()
    {
        $this->context = $this->getMockBuilder('\RDW\Bundle\MoneyBundle\Context\CurrencyContext')->disableOriginalConstructor()->getMock();
        $this->moneyHelper = new MoneyHelper($this->context, 'LT');
        $this->formatter = new \NumberFormatter('LT' ?: \Locale::ACTUAL_LOCALE, \NumberFormatter::PATTERN_DECIMAL);//$this->getMockBuilder('\NumberFormatter')->disableOriginalConstructor()->getMock();
    }

    /**
     * @dataProvider moneyProvider
     * @test
     */
    function it_formats_the_integer_amounts_into_string_representation($amount, $expected)
    {
        $this->context
            ->expects($this->any())
            ->method('getDefaultCurrency')
            ->willReturn('Br');

        $this->assertEquals($expected, $this->moneyHelper->formatPrice($amount));
    }

    /**
     * @test
     */
    function it_allows_to_format_money_in_different_currencies()
    {
        $this->context
            ->expects($this->any())
            ->method('getDefaultCurrency')
            ->willReturn('Br');

        $this->assertEquals('15 Br', $this->moneyHelper->formatPrice(15));
    }

    /*
     * no exception will be throw, because no such currency Br which is now used by system
     *
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage The amount "10" of type integer cannot be formatted to currency "NON_EXIST".
     * @test
     *
    function it_throw_exception_with_non_existing_currency()
    {
        $this->context
            ->expects($this->any())
            ->method('getDefaultCurrency')
            ->willReturn('USD');

        $this->moneyHelper->formatPrice(10, 'NON_EXIST');
    }*/

    /**
     * @test
     */
    function it_should_return_proper_name()
    {
        $this->assertEquals('rdw_money', $this->moneyHelper->getName());
    }

    /**
     * @return array
     */
    public function moneyProvider()
    {
        return [
            [15, '15 Br'],
            [500, '500 Br'],
        ];
    }
}
