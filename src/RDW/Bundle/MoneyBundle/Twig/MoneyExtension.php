<?php

namespace RDW\Bundle\MoneyBundle\Twig;

use RDW\Bundle\MoneyBundle\Templating\Helper\MoneyHelper;

/**
 * Money Twig helper.
 *
 * @author Vytautas Beliunas <vytautas@eface.lt>
 */
class MoneyExtension extends \Twig_Extension
{
    /**
     * @var MoneyHelper
     */
    protected $helper;

    /**
     * @param MoneyHelper $helper
     */
    public function __construct(MoneyHelper $helper)
    {
        $this->helper = $helper;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('price', [$this, 'formatPrice']),
            new \Twig_SimpleFilter('priceWithoutCurrency', [$this, 'formatPriceWithoutCurrency']),
        ];
    }

    /**
     * Convert price between currencies and format the amount to nice display form.
     *
     * @param integer     $amount
     * @param string|null $currency
     *
     * @return string
     */
    public function formatPrice($amount, $currency = null)
    {
        return $this->helper->formatPrice($amount, $currency);
    }

    /**
     * format the amount to nice display form.
     *
     * @param float $amount
     * @param bool  $withDecimals
     *
     * @return string
     */
    public function formatPriceWithoutCurrency($amount, $withDecimals = false)
    {
        return $this->helper->formatNumber($amount, $withDecimals);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_money';
    }
}
