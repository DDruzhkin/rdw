<?php

namespace RDW\Bundle\ContactBundle\Model;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Message
 *
 * @package RDW\Bundle\ContactBundle\Model
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class Message
{
    /**
     * @var string
     *
     * @Assert\NotBlank(message="The name cannot be empty")
     */
    protected $name;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="The email cannot be empty")
     * @Assert\Email(message="The email must be a valid email address")
     */
    protected $email;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="The message body cannot be empty")
     */
    protected $body;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     *
     * @return $this
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }
}
