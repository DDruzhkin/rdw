<?php

namespace RDW\Bundle\ContactBundle\Service;

use RDW\Bundle\ContactBundle\Model\Message;

/**
 * Class MessageManager
 *
 * @package RDW\Bundle\ContactBundle\Service
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class MessageManager
{
    /**
     * @return Message
     */
    public function create()
    {
        return new Message();
    }
}