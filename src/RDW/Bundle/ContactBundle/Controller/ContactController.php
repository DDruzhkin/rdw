<?php

namespace RDW\Bundle\ContactBundle\Controller;

use RDW\Bundle\ContactBundle\Form\Type\MessageType;
use RDW\Bundle\ContactBundle\Service\MessageManager;
use RDW\Bundle\MailerBundle\Service\MailerManager;
use RDW\Bundle\ContentBundle\Service\TextManager;
use RDW\Bundle\ContentBundle\Entity\Text;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ContactController
 *
 * @package RDW\Bundle\ContactBundle\Controller
 */
class ContactController extends Controller
{
    /**
     * @param Request $request
     *
     * @Route("/contact", name="rdw_contact.contact.index")
     * @Template()
     *
     * @return array|Response
     */
    public function indexAction(Request $request)
    {
        $message = $this->getMessageManager()->create();

        $form = $this->createForm(new MessageType(), $message);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getMailerManager()->sendContactFormEmail($form->getData());

            $this->get('braincrafted_bootstrap.flash')->success('The message was successfully sent');

            return $this->redirect($this->get('router')->generate('rdw_contact.contact.index'));
        } else {
            $this->get('session')->set('_digit1', null);
            $this->get('session')->set('_digit2', null);
        }

        $text = $this->getTextManager()->getActiveTextBy(['type' => Text::TYPE_CONTACT]);

        return [
            'text' => $text,
            'form' => $form->createView(),
        ];
    }

    /**
     * @return MessageManager
     */
    private function getMessageManager()
    {
        return $this->get('rdw_contact.service.message_manager');
    }

    /**
     * @return MailerManager
     */
    private function getMailerManager()
    {
        return $this->get('rdw_mailer.service.mailer_manager');
    }

    /**
     * @return TextManager
     */
    private function getTextManager()
    {
        return $this->get('rdw_content.service.text_manager');
    }
}
