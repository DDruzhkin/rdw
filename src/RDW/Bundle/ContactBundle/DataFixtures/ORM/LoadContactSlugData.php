<?php

namespace RDW\Bundle\ContactBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\SlugBundle\Entity\Slug;
use Doctrine\Common\DataFixtures\AbstractFixture;

/**
 * Class LoadFaqSlugData
 *
 * @package RDW\Bundle\ContactBundle\DataFixtures\ORM
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class LoadContactSlugData extends AbstractFixture
{
    const REFERENCE_STRING = 'contact-slug';

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $entityManager)
    {
        $slug = new Slug();
        $slug->setController('RDWContactBundle:Contact:index');
        $slug->setName('contacts');
        $slug->setOrigName('contacts');
        $slug->setSystem(true);

        $this->addReference(self::REFERENCE_STRING, $slug);

        $entityManager->persist($slug);
        $entityManager->flush();
    }
}