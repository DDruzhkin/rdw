<?php

namespace RDW\Bundle\ContactBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class MessageType
 *
 * @package RDW\Bundle\ContactBundle\Form\Type
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class MessageType extends AbstractType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', [
                'required' => false,
                'label' => 'Name',
                'attr' => ['placeholder' => 'Name placeholder']
            ])
            ->add('email', 'email', [
                'required' => false,
                'label' => 'Email',
                'attr' => ['placeholder' => 'Email placeholder']
            ])
            ->add('body', 'textarea', [
                'required' => false,
                'label' => 'Message',
                'attr' => ['placeholder' => 'Message placeholder']
            ])
            ->add('sumCaptcha', 'sum_captcha', [
                /** @Ignore */
                'label' => false,
            ])
            ->add('submit', 'submit', array(
                'label' => 'Send'
            ));
    }

    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RDW\Bundle\ContactBundle\Model\Message',
        ));
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'rdw_contact_message';
    }
}