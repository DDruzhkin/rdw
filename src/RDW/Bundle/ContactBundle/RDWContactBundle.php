<?php

namespace RDW\Bundle\ContactBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class RDWContactBundle
 *
 * @package RDW\Bundle\ContactBundle
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class RDWContactBundle extends Bundle
{
}
