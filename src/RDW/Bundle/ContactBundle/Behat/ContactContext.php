<?php

namespace RDW\Bundle\ContactBundle\Behat;

use RDW\Bundle\AppBundle\Behat\DefaultContext;

/**
 * Class ContactContext
 *
 * @package RDW\Bundle\ContactBundle\Behat
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class ContactContext extends DefaultContext
{
    /**
     * @Given /^I am on the contact form page$/
     */
    public function iAmOnTheRegistrationPage()
    {
        $this->getSession()->visit($this->generatePageUrl('rdw_contact.contact.index'));
    }

    /**
     * @Given /^I should be on the contact form page$/
     */
    public function iShouldBeOnContactFormPage()
    {
        $this->assertSession()->addressEquals($this->generateUrl('rdw_contact.contact.index'));
    }
}