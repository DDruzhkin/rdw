<?php

namespace RDW\Bundle\HistoryBundle\Repository;

use Doctrine\ORM\EntityRepository;
use RDW\Bundle\HistoryBundle\Model\HistoryFilter;

/**
 * Class HistoryRepository
 *
 * @package RDW\Bundle\HistoryBundle\Repository
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class HistoryRepository extends EntityRepository
{
    /**
     * @param HistoryFilter $filter
     *
     * @return \Doctrine\ORM\Query
     */
    public function getHistoryListQuery(HistoryFilter $filter)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('h')
            ->from('RDWHistoryBundle:History', 'h')
            ->orderBy('h.createdAt', 'DESC');

        if ($filter->getUser()) {
            $queryBuilder
                ->andWhere('h.user = :user')
                ->setParameter('user', $filter->getUser());
        }

        return $queryBuilder->getQuery();
    }
}
