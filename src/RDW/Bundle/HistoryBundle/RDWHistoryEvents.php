<?php

namespace RDW\Bundle\HistoryBundle;

/**
 * Contains all events thrown in the RDWHistoryBundle
 *
 * Class RDWHistoryEvents
 *
 * @package History
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
final class RDWHistoryEvents
{
    /**
     * The LOGIN_SUCCESS event occurs when the someone successfully login
     */
    const LOGIN_SUCCESS = 'rdw_history.event.login_success';

    /**
     * The LOGOUT_SUCCESS event occurs when the someone successfully logout
     */
    const LOGOUT_SUCCESS = 'rdw_history.event.logout_success';

    /**
     * The EDIT_OBJECT event occurs when the someone successfully edit object
     */
    const EDIT_OBJECT = 'rdw_history.event.edit_object';

    /**
     * The CREATE_OBJECT event occurs when the someone successfully creates new object
     */
    const CREATE_OBJECT = 'rdw_history.event.create_object';

    /**
     * The CREATE_OBJECT event occurs when the someone successfully creates new object
     */
    const DELETE_OBJECT = 'rdw_history.event.delete_object';

    /**
     * The ABUSE_OBJECT event occurs when the someone successfully abuse object
     */
    const ABUSE_OBJECT = 'rdw_history.event.abuse_object';

    /**
     * The VIEW_OBJECT event occurs when the someone successfully views object
     */
    const VIEW_OBJECT = 'rdw_history.event.view_object';

    /**
     * The APPLY_JOB event occurs when employee applies for job
     */
    const APPLY_JOB = 'rdw_history.event.apply_job';

    /**
     * The WRITE event occurs when user starts new conversation with another user
     */
    const WRITE = 'rdw_history.event.write';

    /**
     * The REPLY event occurs when user replies for cv or in conversation
     */
    const REPLY = 'rdw_history.event.reply';
}
