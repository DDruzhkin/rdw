<?php

namespace RDW\Bundle\HistoryBundle\Twig;

use RDW\Bundle\ConversationBundle\Entity\Conversation;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\HistoryBundle\Entity\History;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class HistoryExtension
 *
 * @author Vytautas Beliunas <vytautas@eface.lt>
 */
class HistoryExtension extends \Twig_Extension
{
    /**
     * @var Router
     */
    private $router;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var array
     */
    private $cvActions = [
        History::ACTION_CREATE,
        History::ACTION_EDIT,
        History::ACTION_VIEW,
        History::ACTION_DELETE,
        History::ACTION_ABUSE,
        History::ACTION_REPLY
    ];

    /**
     * @var array
     */
    private $jobActions = [
        History::ACTION_CREATE,
        History::ACTION_EDIT,
        History::ACTION_VIEW,
        History::ACTION_DELETE,
        History::ACTION_APPLY,
        History::ACTION_ABUSE,
    ];

    const RETURN_TYPE_URL = 'url';
    const RETURN_TYPE_TITLE = 'title';

    /**
     * @param Router $router
     * @param TranslatorInterface $translator
     */
    public function __construct(\Symfony\Cmf\Component\Routing\ChainRouter $router, TranslatorInterface $translator)
    {
        $this->router = $router;
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('historyActionUrl', [$this, 'getHistoryActionUrl']),
            new \Twig_SimpleFunction('historyActionTitle', [$this, 'getHistoryActionTitle']),
        ];
    }

    /**
     * @param History $history
     *
     * @return string|null
     */
    public function getHistoryActionUrl(History $history)
    {
        return $this->getHistoryActionUrlOrTitle($history, self::RETURN_TYPE_URL);
    }

    /**
     * @param History $history
     *
     * @return string|null
     */
    public function getHistoryActionTitle(History $history)
    {
        /** @Ignore */
        $title = $this->translator->trans($history->getAction());

        if ($history->getObjectClass()) {
            $title .= ' ' . $this->getHistoryActionUrlOrTitle($history, self::RETURN_TYPE_TITLE);
        }

        return $title;
    }

    /**
     * @param History $history
     * @param string $returnType
     *
     * @return string
     */
    private function getHistoryActionUrlOrTitle(History $history, $returnType)
    {
        $objectClassBaseName = basename($history->getObjectClass());
        $object = new $objectClassBaseName;
        $action = $history->getAction();
        $objectId = $history->getObjectId();

        $returnString = null;

        // job view
        if ($object instanceof Job && in_array($action, $this->jobActions)) {
            if ($returnType == self::RETURN_TYPE_URL) {
                $returnString = $this->router->generate(
                    'rdw_job.job.view', ['id' => $objectId, 'slug' => $object->getSlug()], true
                );
            } else {
                $returnString = $this->translator->trans('job id %id%', ['%id%' => $objectId]);
            }
        }

        // cv create, edit, view
        if ($object instanceof Cv && in_array($action, $this->cvActions)) {
            if ($returnType == self::RETURN_TYPE_URL) {
                if ($action == History::ACTION_DELETE) {
                    $returnString = '#';
                } else {
                    $returnString = $this->router->generate(
                        'rdw_cv.cv.view', ['id' => $objectId, 'slug' => $object->getSlug()], true
                    );
                }
            } else {
                $returnString = $this->translator->trans('cv id %id%', ['%id%' => $objectId]);
            }
        }

        // conversation reply
        if ($object instanceof Conversation && $action == History::ACTION_REPLY) {
            if ($returnType == self::RETURN_TYPE_URL) {
                $returnString = "#";
            } else {
                $returnString = $this->translator->trans('conversation');
            }
        }

        return $returnString;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_history_twig_extension';
    }
}
