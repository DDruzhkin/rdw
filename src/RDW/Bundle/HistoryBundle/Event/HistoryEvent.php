<?php

namespace RDW\Bundle\HistoryBundle\Event;

use RDW\Bundle\UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class HistoryEvent
 *
 * @package History
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class HistoryEvent extends Event
{
    /**
     * @var string
     */
    private $type;

    /**
     * @var object
     */
    private $object;

    /**
     * @var object
     */
    private $user;

    /**
     * @param User   $user
     * @param string $type
     * @param object $object
     */
    public function __construct(User $user, $type, $object = null)
    {
        $this->user = $user;
        $this->type = $type;
        $this->object = $object;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return object
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @return object
     */
    public function getUser()
    {
        return $this->user;
    }
}
