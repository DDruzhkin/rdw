<?php

namespace RDW\Bundle\HistoryBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use RDW\Bundle\HistoryBundle\Entity\History;
use Knp\Component\Pager\Paginator;
use RDW\Bundle\HistoryBundle\Event\HistoryEvent;
use RDW\Bundle\HistoryBundle\Model\HistoryFilter;

/**
 * Class HistoryManager
 *
 * @package RDW\Bundle\HistoryBundle\Service
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class HistoryManager
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $entityManager;

    /**
     * @var \Knp\Component\Pager\Paginator
     */
    private $paginator;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param ObjectManager    $entityManager
     * @param Paginator        $paginator
     * @param EventDispatcherInterface  $eventDispatcher
     */
    public function __construct(ObjectManager $entityManager, Paginator $paginator, EventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->paginator = $paginator;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param HistoryFilter $filter
     * @param int           $page
     * @param int           $limit
     *
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function getPaginatedHistoryList(HistoryFilter $filter, $page, $limit)
    {
        $query = $this->getRepository()->getHistoryListQuery($filter);

        $pagination = $this->paginator->paginate($query, $page, $limit);

        return $pagination;
    }

    /**
     * @param History $history
     * @param bool    $andFlush
     *
     * @return bool
     */
    public function update(History $history, $andFlush = true)
    {
        $this->entityManager->persist($history);

        if ($andFlush) {
            $this->entityManager->flush();
        }

        return true;
    }

    /**
     * @param User        $user
     * @param string      $action
     * @param object|null $object
     * @param string      $event
     */
    public function logEvent(User $user, $action, $object, $event)
    {
        $historyEvent = new HistoryEvent($user, $action, $object);
        $this->eventDispatcher->dispatch($event, $historyEvent);
    }



    /**
     * @return \RDW\Bundle\HistoryBundle\Repository\HistoryRepository
     */
    protected function getRepository()
    {
        return $this->entityManager->getRepository('RDWHistoryBundle:History');
    }
}
