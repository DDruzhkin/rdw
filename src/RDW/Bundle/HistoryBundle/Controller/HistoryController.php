<?php

namespace RDW\Bundle\HistoryBundle\Controller;

use RDW\Bundle\HistoryBundle\Model\HistoryFilter;
use RDW\Bundle\UserBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class CvController
 *
 * @Route("/")
 *
 * @package RDW\Bundle\HistoryBundle\Controller
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class HistoryController extends Controller
{
    /**
     * @param User $user
     *
     * @Route("/show/{id}", requirements={"id" = "\d+"}, name="rdw_history.history.show")
     * @Template()
     *
     * @return array|RedirectResponse
     * @throws NotFoundHttpException
     */
    public function showAction(User $user)
    {
        $this->isGrantedLocal();

        if ( ! $user) {
            throw new NotFoundHttpException("User was not found.");
        }

        $historyManager = $this->get('rdw_history.service.history_manager');
        $filter = new HistoryFilter();
        $filter->setUser($user);

        $history = $historyManager->getPaginatedHistoryList(
            $filter,
            $this->get('request')->query->get('page', 1),
            $this->container->getParameter('rdw_history.history.paginator.per_page')
        );

        return [
            'user' => $user,
            'historyPagination' => $history
        ];
    }

    /**
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    private function isGrantedLocal()
    {
        if ( ! $this->container->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }
    }
}
