<?php

namespace RDW\Bundle\HistoryBundle\Tests\Event;

use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\HistoryBundle\Entity\History;
use RDW\Bundle\HistoryBundle\Event\HistoryEvent;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\User;

/**
 * Class HistoryEvenyTest
 *
 * @package RDW\Bundle\HistoryBundle\Tests\Event
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class HistoryEventTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function it_should_return_user_properly()
    {
        $user = new RegisteredUser();
        $historyEvent = new HistoryEvent($user, History::ACTION_LOGIN);

        $this->assertInstanceOf('RDW\Bundle\UserBundle\Entity\RegisteredUser', $historyEvent->getUser());
    }

    /**
     * @test
     */
    public function it_should_return_type_properly()
    {
        $user = new RegisteredUser();
        $historyEvent = new HistoryEvent($user, History::ACTION_LOGIN);

        $this->assertEquals(History::ACTION_LOGIN, $historyEvent->getType());
    }

    /**
     * @test
     */
    public function it_should_return_object_properly()
    {
        $user = new RegisteredUser();
        $object = new Cv();
        $historyEvent = new HistoryEvent($user, History::ACTION_LOGIN, $object);

        $this->assertInstanceOf('RDW\Bundle\CvBundle\Entity\Cv', $historyEvent->getObject());
    }
}
