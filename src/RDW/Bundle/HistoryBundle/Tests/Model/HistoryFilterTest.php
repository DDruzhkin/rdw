<?php
/**
 * Created by PhpStorm.
 * User: vb
 * Date: 11/22/14
 * Time: 2:21 PM
 */

namespace RDW\Bundle\HistoryBundle\Tests\Model;

use RDW\Bundle\HistoryBundle\Model\HistoryFilter;

class HistoryFilterTest extends \PHPUnit_Framework_TestCase
{
    private $user;

    /**
     * set up
     */
    public function setUp()
    {
        $this->user = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');
    }

    /**
     * @test
     */
    public function it_should_set_user_properly()
    {
        $historyFilter = new HistoryFilter();
        $historyFilter->setUser($this->user);

        $this->assertEquals($this->user, $historyFilter->getUser());
    }
}
