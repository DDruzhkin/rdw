<?php
/**
 * Created by PhpStorm.
 * User: vb
 * Date: 11/22/14
 * Time: 2:02 PM
 */

namespace RDW\Bundle\HistoryBundle\Tests\EventListener;


use RDW\Bundle\HistoryBundle\Entity\History;
use RDW\Bundle\HistoryBundle\Event\HistoryEvent;
use RDW\Bundle\HistoryBundle\EventListener\HistoryListener;
use RDW\Bundle\HistoryBundle\Service\HistoryManager;

class HistoryListenerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $em;

    /**
     * @var HistoryManager
     */
    private $historyManager;

    /**
     * @var HistoryListener
     */
    private $listener;

    private $user;

    private $eventObject;

    /**
     * set up
     */
    public function setUp()
    {
        $this->em = $this->getMockBuilder('\Doctrine\Common\Persistence\ObjectManager')->getMock();
        $this->eventObject = $this->getMock('RDW\Bundle\CvBundle\Entity\Cv');
        $this->user = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');
        $this->historyManager = $this->getMockBuilder('RDW\Bundle\HistoryBundle\Service\HistoryManager')
            ->disableOriginalConstructor()
            ->getMock();
        $this->listener = new HistoryListener($this->historyManager);
    }

    /**
     * @test
     */
    public function it_should_save_new_history_object_and_set_object()
    {
        $historyEvent = new HistoryEvent($this->user, History::ACTION_REPLY, $this->eventObject);

        $this->eventObject
            ->expects($this->once())
            ->method('getId')
            ->will($this->returnValue(5));

        $this->historyManager
            ->expects($this->once())
            ->method('update')
            ->will($this->returnValue(true));

        $this->assertTrue($this->listener->saveNewHistory($historyEvent));
    }

    /**
     * @test
     */
    public function it_should_save_new_history_object_but_not_set_object()
    {
        $historyEvent = new HistoryEvent($this->user, History::ACTION_REPLY);

        $this->eventObject
            ->expects($this->never())
            ->method('getId');

        $this->historyManager
            ->expects($this->once())
            ->method('update')
            ->will($this->returnValue(true));

        $this->assertTrue($this->listener->saveNewHistory($historyEvent));
    }
}
