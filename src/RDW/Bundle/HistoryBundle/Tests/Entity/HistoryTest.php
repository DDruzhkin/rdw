<?php

namespace RDW\Bundle\HistoryBundle\Tests\Entity;

use RDW\Bundle\HistoryBundle\Entity\History;
use RDW\Bundle\ManageBundle\Entity\User;

class HistoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function it_should_set_action_properly()
    {
        $history = new History();
        $history->setAction(History::ACTION_EDIT);

        $this->assertEquals(History::ACTION_EDIT, $history->getAction());
    }

    /**
     * @test
     */
    public function it_should_set_user_properly()
    {
        $history = new History();
        $history->setUser(new User());

        $this->assertInstanceOf('RDW\Bundle\ManageBundle\Entity\User', $history->getUser());
    }

    /**
     * @test
     */
    public function it_should_set_object_id_properly()
    {
        $history = new History();
        $history->setObjectId(1);

        $this->assertEquals(1, $history->getObjectId());
    }

    /**
     * @test
     */
    public function it_should_set_object_class_properly()
    {
        $history = new History();
        $history->setObjectClass('RDW\Bundle\ManageBundle\Entity\User');

        $this->assertEquals('RDW\Bundle\ManageBundle\Entity\User', $history->getObjectClass());
    }

    /**
     * @test
     */
    public function it_should_set_created_at_properly()
    {
        $date = new \DateTime('-2DAYS');
        $history = new History();
        $history->setCreatedAt($date);

        $this->assertInstanceOf('\DateTime', $history->getCreatedAt());
        $this->assertEquals($date, $history->getCreatedAt());
    }
}
