<?php

namespace RDW\Bundle\HistoryBundle\Tests\Service;

use RDW\Bundle\HistoryBundle\Entity\History;
use RDW\Bundle\HistoryBundle\Service\HistoryManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class HistoryManagerTest
 *
 * @package RDW\Bundle\HistoryBundle\Tests\Service
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class HistoryManagerTest extends WebTestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $em;

    /**
     * @var HistoryManager
     */
    private $historyManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $paginator;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $eventDispatcher;

    /**
     * set up
     */
    public function setUp()
    {
        $client = static::createClient();
        $container = $client->getContainer();

        $this->em = $this->getMockBuilder('\Doctrine\Common\Persistence\ObjectManager')->getMock();
        $this->paginator = $this->getMock('Knp\Component\Pager\Paginator');
        $this->eventDispatcher = $this->getMock('Symfony\Component\EventDispatcher\EventDispatcher');

        $this->historyManager = new HistoryManager($this->em, $this->paginator, $this->eventDispatcher);

        $this->repo = $container
            ->get('doctrine.orm.entity_manager')
            ->getRepository('RDWAppBundle:JobAbuse');
    }

    /**
     * @test
     */
    public function it_should_persist_and_not_flush()
    {
        $object = new History();

        $this->em
            ->expects($this->once())
            ->method('persist')
            ->will($this->returnValue(true));

        $this->em
            ->expects($this->never())
            ->method('flush')
            ->will($this->returnValue(true));

        $result = $this->historyManager->update($object, false);
        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function it_should_persist_and_flush()
    {
        $object = new History();

        $this->em
            ->expects($this->once())
            ->method('persist')
            ->will($this->returnValue(true));

        $this->em
            ->expects($this->once())
            ->method('flush')
            ->will($this->returnValue(true));

        $result = $this->historyManager->update($object, true);
        $this->assertTrue($result);
    }

    public function it_should_set_up_repository()
    {

    }
}
