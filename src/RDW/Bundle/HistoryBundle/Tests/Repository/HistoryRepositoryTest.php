<?php
/**
 * Created by PhpStorm.
 * User: vb
 * Date: 11/22/14
 * Time: 2:24 PM
 */

namespace RDW\Bundle\HistoryBundle\Tests\Repository;

use RDW\Bundle\HistoryBundle\Model\HistoryFilter;
use RDW\Bundle\HistoryBundle\Repository\HistoryRepository;

class HistoryRepositoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $entityRepository;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $entityManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $queryBuilder;

    /**
     * @var HistoryRepository
     */
    private $historyRepository;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $class;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $query;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $user;

    /**
     * set up
     */
    public function setUp()
    {
        $this->entityManager = $this->getMockBuilder('Doctrine\ORM\EntityManager')
            ->disableOriginalConstructor()
            ->getMock();

        $this->query = $this->getMockBuilder('Doctrine\ORM\AbstractQuery')
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();
        $this->query->expects($this->any())
            ->method('_doExecute')
            ->will($this->returnValue(true));

        $this->class = $this->getMockBuilder('\Doctrine\ORM\Mapping\ClassMetadata')
            ->disableOriginalConstructor()
            ->getMock();

        $this->queryBuilder = $this->getMockBuilder('Doctrine\ORM\QueryBuilder')
            ->disableOriginalConstructor()
            ->getMock();

        $this->entityManager
            ->expects($this->once())
            ->method('createQueryBuilder')
            ->will($this->returnValue($this->queryBuilder));

        $this->historyRepository = new HistoryRepository($this->entityManager, $this->class);
        $this->user = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');
    }

    /**
     * @test
     */
    public function it_should_return_query_ant_apply_filters()
    {
        $historyFilter = new HistoryFilter();
        $historyFilter->setUser($this->user);

        $this->queryBuilder
            ->expects($this->once())
            ->method('select')
            ->will($this->returnValue($this->queryBuilder));

        $this->queryBuilder
            ->expects($this->once())
            ->method('from')
            ->with($this->equalTo('RDWHistoryBundle:History'), $this->equalTo('h'))
            ->will($this->returnValue($this->queryBuilder));

        $this->queryBuilder
            ->expects($this->once())
            ->method('andWhere')
            ->with($this->equalTo('h.user = :user'))
            ->will($this->returnValue($this->queryBuilder));

        $this->queryBuilder
            ->expects($this->once())
            ->method('setParameter')
            ->with($this->equalTo('user'), $this->equalTo($this->user))
            ->will($this->returnValue($this->queryBuilder));

        $this->queryBuilder
            ->expects($this->once())
            ->method('getQuery')
            ->will($this->returnValue($this->query));

        $this->assertEquals($this->query, $this->historyRepository->getHistoryListQuery($historyFilter));
    }
}
