<?php

namespace RDW\Bundle\HistoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class History
 *
 * @package RDW\Bundle\HistoryBundle\Entity
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 *
 * @ORM\Table(name="history")
 * @ORM\Entity(repositoryClass="RDW\Bundle\HistoryBundle\Repository\HistoryRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class History
{
    const ACTION_EDIT = 'edit';
    const ACTION_VIEW = 'view';
    const ACTION_LOGOUT = 'logout';
    const ACTION_LOGIN = 'login';
    const ACTION_CREATE = 'create';
    const ACTION_DELETE = 'delete';
    const ACTION_ABUSE = 'abuse';
    const ACTION_APPLY = 'apply';
    const ACTION_REPLY = 'reply';
    const ACTION_WRITE = 'write';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\RegisteredUser", inversedBy="history")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true, name="updated_at")
     *
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false, name="action")
     */
    protected $action;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true, name="object_id")
     */
    protected $objectId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true, name="object_class")
     */
    protected $objectClass;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return int
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * @param int $objectId
     *
     * @return $this
     */
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;

        return $this;
    }

    /**
     * @return string
     */
    public function getObjectClass()
    {
        return $this->objectClass;
    }

    /**
     * @param string $objectClass
     *
     * @return $this
     */
    public function setObjectClass($objectClass)
    {
        $this->objectClass = $objectClass;

        return $this;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     *
     * @return $this
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEditAction()
    {
        return ($this->getAction() == self::ACTION_EDIT) ? true : false;
    }
}
