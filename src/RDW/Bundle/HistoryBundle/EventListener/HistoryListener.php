<?php

namespace RDW\Bundle\HistoryBundle\EventListener;

use RDW\Bundle\HistoryBundle\Entity\History;
use RDW\Bundle\HistoryBundle\Event\HistoryEvent;
use RDW\Bundle\HistoryBundle\RDWHistoryEvents;
use RDW\Bundle\HistoryBundle\Service\HistoryManager;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class HistoryListener
 *
 * @package History
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class HistoryListener implements EventSubscriberInterface
{
    /**
     * @var HistoryManager
     */
    private $historyManager;

    /**
     * @param HistoryManager $historyManager
     */
    public function __construct(HistoryManager $historyManager)
    {
        $this->historyManager = $historyManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            RDWHistoryEvents::LOGIN_SUCCESS  => 'saveNewHistory',
            RDWHistoryEvents::LOGOUT_SUCCESS => 'saveNewHistory',
            RDWHistoryEvents::EDIT_OBJECT    => 'saveNewHistory',
            RDWHistoryEvents::VIEW_OBJECT    => 'saveNewHistory',
            RDWHistoryEvents::DELETE_OBJECT  => 'saveNewHistory',
            RDWHistoryEvents::CREATE_OBJECT  => 'saveNewHistory',
            RDWHistoryEvents::APPLY_JOB      => 'saveNewHistory',
            RDWHistoryEvents::ABUSE_OBJECT   => 'saveNewHistory',
            RDWHistoryEvents::REPLY          => 'saveNewHistory',
            RDWHistoryEvents::WRITE          => 'saveNewHistory',
        ];
    }

    /**
     * @param HistoryEvent $event
     *
     * @return bool
     */
    public function saveNewHistory(HistoryEvent $event)
    {
        if (! $event->getUser() instanceof RegisteredUser) {
            return false;
        }

        $history = new History();
        $history->setAction($event->getType());
        $history->setUser($event->getUser());

        $object = $event->getObject();

        if ($object) {
            $history->setObjectClass(get_class($object));
            $history->setObjectId($object->getId());
        }

        $this->historyManager->update($history);

        return true;
    }
}
