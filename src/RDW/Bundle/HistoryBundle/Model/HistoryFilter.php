<?php

namespace RDW\Bundle\HistoryBundle\Model;

use \RDW\Bundle\UserBundle\Entity\User;

/**
 * Class HistoryFilter
 *
 * @package RDW\Bundle\HistoryBundle\Model
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class HistoryFilter
{
    /**
     * @var User
     */
    private $user;

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }
}
