<?php

namespace RDW\Bundle\UserBundle\Security\Provider;

use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\Exception\AccountNotLinkedException;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthAwareUserProviderInterface;
use RDW\Bundle\UserBundle\Entity\User;
use RDW\Bundle\UserBundle\Service\SocialManager;

/**
 * Class OAuthUserProvider
 *
 * @package RDw\Bundle\UserBundle\Security
 */
class OAuthUserProvider implements OAuthAwareUserProviderInterface
{
    /**
     * @var \RDW\Bundle\UserBundle\Service\SocialManager
     */
    private $socialManager;

    /**
     * @param SocialManager $socialManager
     */
    public function __construct(SocialManager $socialManager)
    {
        $this->socialManager = $socialManager;
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        //var_dump($response);die();

        // get provider
        $provider = $response->getResourceOwner()->getName();
        $accountId = $response->getUsername();

        $existingUser = $this->socialManager->findUserBySocialProvider($provider, $accountId);

        if ($existingUser) {
            return $existingUser;
        } else {

            // if email in response when full user
            if (!$response->getEmail()) {
                throw new AccountNotLinkedException(sprintf("Account does not have an email."));
            }

            // find by email
            $oldUser = $this->socialManager->findUserByEmail($response->getEmail());

            if ($oldUser instanceof User) {
                return $oldUser;
            } else {
                return $this->socialManager->createNewUserFromOAuthResponse($response);
            }
        }

    }
}
