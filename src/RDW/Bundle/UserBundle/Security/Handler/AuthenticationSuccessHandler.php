<?php

namespace RDW\Bundle\UserBundle\Security\Handler;

use RDW\Bundle\HistoryBundle\Entity\History;
use RDW\Bundle\HistoryBundle\RDWHistoryEvents;
use RDW\Bundle\HistoryBundle\Service\HistoryManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use Symfony\Component\Security\Http\HttpUtils;
use Symfony\Component\HttpFoundation\JsonResponse;

class AuthenticationSuccessHandler extends DefaultAuthenticationSuccessHandler
{
    /**
     * @var HistoryManager
     */
    private $historyManager;

    /**
     * @param HttpUtils                $httpUtils
     * @param array                    $options
     * @param HistoryManager           $historyManager
     */
    public function __construct(HttpUtils $httpUtils, array $options, HistoryManager $historyManager)
    {
        parent::__construct($httpUtils, $options);

        $this->historyManager  = $historyManager;
    }

    /**
     * @param Request        $request
     * @param TokenInterface $token
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $this->historyManager->logEvent($token->getUser(), History::ACTION_LOGIN, null, RDWHistoryEvents::LOGIN_SUCCESS);

        if ($request->isXmlHttpRequest()) {
            $data = [
                'success' => true,
                'redirect' => $this->determineTargetUrl($request),
            ];

            $response = new JsonResponse($data);
        } else {
            $response = parent::onAuthenticationSuccess($request, $token);
        }

        return $response;
    }
}
