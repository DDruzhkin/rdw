<?php

namespace RDW\Bundle\UserBundle\Security\Handler;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Csrf\CsrfProvider\CsrfProviderInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationFailureHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\HttpUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Templating\EngineInterface;
use Symfony\Component\Translation\TranslatorInterface;

class AuthenticationFailureHandler extends DefaultAuthenticationFailureHandler
{
    /**
     * @var Template
     */
    private $templating;

    /**
     * @var CsrfProviderInterface
     */
    private $csrfProvider;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var Session
     */
    private $session;

    /**
     * AuthenticationFailureHandler constructor.
     *
     * @param HttpKernel $httpKernel
     * @param HttpUtils $httpUtils
     * @param array $options
     * @param EngineInterface $templating
     * @param CsrfProviderInterface $csrfProvider
     * @param TranslatorInterface $translator
     * @param Session $session
     */
    public function __construct(
        HttpKernel $httpKernel,
        HttpUtils $httpUtils,
        array $options,
        EngineInterface $templating,
        CsrfProviderInterface $csrfProvider,
        TranslatorInterface $translator,
        Session $session
    ) {
        parent::__construct($httpKernel, $httpUtils, $options);

        $this->templating = $templating;
        $this->csrfProvider = $csrfProvider;
        $this->translator = $translator;
        $this->session = $session;
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     *
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        if ($request->isXmlHttpRequest()) {

            $lastUsername = (null === $this->session) ? '' : $this->session->get(SecurityContextInterface::LAST_USERNAME);

            $data = [
                'success' => false,
                'content' => $this->templating->render(
                    'RDWUserBundle:Registration:_login_form.html.twig',
                    [
                        'error' => /** @Ignore */
                            $this->translator->trans($exception->getMessage()),
                        'last_username' => $lastUsername,
                        'csrf_token' => $this->csrfProvider->generateCsrfToken('authenticate'),
                        'redirect_url' => $this->session->has('referer') ? $this->session->get('referer') : null,
                    ]
                )
            ];

            return new JsonResponse($data);
        } else {
            return parent::onAuthenticationFailure($request, $exception);
        }
    }
}
