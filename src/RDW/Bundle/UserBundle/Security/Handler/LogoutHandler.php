<?php

namespace RDW\Bundle\UserBundle\Security\Handler;

use RDW\Bundle\HistoryBundle\Entity\History;
use RDW\Bundle\HistoryBundle\Event\HistoryEvent;
use RDW\Bundle\HistoryBundle\RDWHistoryEvents;
use RDW\Bundle\HistoryBundle\Service\HistoryManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Logout\LogoutHandlerInterface;

/**
 * Class LogoutHandler
 *
 * @package RDW\Bundle\UserBundle\Security\Handler
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class LogoutHandler implements LogoutHandlerInterface
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var HistoryManager
     */
    private $historyManager;

    /**
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EventDispatcherInterface $dispatcher, HistoryManager $historyManager)
    {
        $this->eventDispatcher = $dispatcher;
        $this->historyManager  = $historyManager;
    }

    /**
     * @param Request        $request
     * @param Response       $response
     * @param TokenInterface $token
     */
    public function logout(Request $request, Response $response, TokenInterface $token)
    {
        $this->historyManager->logEvent($token->getUser(), History::ACTION_LOGOUT, null, RDWHistoryEvents::LOGOUT_SUCCESS);
    }
}
