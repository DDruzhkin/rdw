<?php

namespace RDW\Bundle\UserBundle\Twig;

use RDW\Bundle\AppBundle\Entity\AbstractAd;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\UserBundle\Entity\CvFavorite;
use RDW\Bundle\UserBundle\Entity\JobFavorite;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\UserInterface;
use RDW\Bundle\UserBundle\Service\FavoriteManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BaseExtension
 *
 * @package RDW\Bundle\UserBundle\Twig
 * @author Vytautas Beliunas <vytautas@eface.lt>
 */
class BaseExtension extends \Twig_Extension implements ContainerAwareInterface
{
    /**
     * Container
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('youtube_id', [$this, 'getYoutubeIdFromUrl']),
            new \Twig_SimpleFilter('age', [$this, 'getAgeCountFromDate']),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('gender', [$this, 'getTranslatedGender']),
            new \Twig_SimpleFunction('hasFavorite', [$this, 'hasFavorite']),
            new \Twig_SimpleFunction('imageUrl', [$this, 'getImageUrl']),
        ];
    }

    /**
     * @param \DateTime $date
     *
     * @return int|string
     */
    public function getAgeCountFromDate(\DateTime $date = null)
    {
        if (null === $date) {
            return '';
        }

        $oDateNow = new \DateTime();
        $oDateBirth = $date;
        $oDateInterval = $oDateNow->diff($oDateBirth);

        /* @var $oDateInterval \DateInterval */
        return (int) $oDateInterval->y;
    }

    /**
     * @param UserInterface $user   User
     * @param AbstractAd    $entity Job or CV
     *
     * @return bool
     */
    public function hasFavorite(UserInterface $user = null, AbstractAd $entity)
    {
        $hasFavorite = false;

        if ( ! $user) {
            return $hasFavorite;
        }

        $oldFavorite = $this->getFavoriteManager()->findByUser($entity, $user);

        if ($oldFavorite) {
            $hasFavorite = true;
        }

        return $hasFavorite;
    }

    /**
     * @param RegisteredUser $user
     *
     * @return string
     */
    public function getUserName(RegisteredUser $user)
    {
        return $user->getTitle();
    }

    /**
     * @param string $youtubeUrl
     *
     * @return string|bool
     */
    public function getYoutubeIdFromUrl($youtubeUrl)
    {
        preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $youtubeUrl, $matches);

        if (isset($matches[0])) {
            return $matches[0];
        }

        return false;
    }

    /**
     * @param string $gender
     *
     * @return null|string
     */
    public function getTranslatedGender($gender)
    {
        switch ($gender) {
            case 'f':
                return $this->container->get('translator')->trans('Female');
                break;

            case 'm':
                return $this->container->get('translator')->trans('Male');
                break;

            default:
                return null;
        }
    }

    /**
     * @param RegisteredUser $user
     * @param string         $filter
     * @param bool           $absolute
     *
     * @return mixed|string
     */
    public function getImageUrl(RegisteredUser $user, $filter = 'company_thumb', $absolute = true)
    {
        return ($photo = $user->getPhoto())
            ? $this->container->get('imagine.cache.path.resolver')->getBrowserPath($photo, $filter, $absolute)
            : $this->container->getParameter('rdw_app.user_profile_no_logo');
    }

    /**
     * @return FavoriteManager
     */
    protected function getFavoriteManager()
    {
        return $this->container->get('rdw_user.service.favorite_manager');
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_user_twig_extension';
    }
}
