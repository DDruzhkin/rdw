<?php

namespace RDW\Bundle\UserBundle\Twig;

use RDW\Bundle\UserBundle\Repository\FavoriteRepository;
use RDW\Bundle\UserBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Class FavoritesExtension
 * @package RDW\Bundle\UserBundle\Twig
 *
 * @author Paulius Aleliūnas <paulius@eface.lt>
 */
class FavoritesExtension extends \Twig_Extension implements ContainerAwareInterface
{
    /**
     * Container
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('countFavorites', [$this, 'getCountFavorites']),
        ];
    }

    /**
     * @param string $type
     *
     * @return int
     */
    public function getCountFavorites($type)
    {
        /** @var $token TokenInterface */
        $token = $this->container->get('security.context')->getToken();
        $user = $token->getUser();

        return ('jobs' == $type) ? $this->countFavoriteJobs($user) : $this->countFavoriteCvs($user);
    }

    /**
     * @param User $registeredUser
     * @return int
     */
    private function countFavoriteJobs(User $registeredUser)
    {
        /** @var $repository FavoriteRepository */
        $repository = $this->container
            ->get('doctrine.orm.default_entity_manager')
            ->getRepository('RDWUserBundle:JobFavorite');

        return $repository->countUserFavoriteJobs($registeredUser);
    }

    /**
     * @param User $registeredUser
     * @return int
     */
    private function countFavoriteCvs(User $registeredUser)
    {
        /** @var $repository FavoriteRepository */
        $repository = $this->container
            ->get('doctrine.orm.default_entity_manager')
            ->getRepository('RDWUserBundle:CvFavorite');

        return $repository->countUserFavoriteCvs($registeredUser);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_favorite_extension';
    }
}
