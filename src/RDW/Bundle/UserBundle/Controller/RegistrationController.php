<?php

namespace RDW\Bundle\UserBundle\Controller;

use FOS\UserBundle\Event\UserEvent;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\User;
use RDW\Bundle\UserBundle\RDWUserEvents;
use RDW\Bundle\UserBundle\Event\SingleUserEvent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\UserBundle\Controller\RegistrationController as FOSRegistrationController;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\SecurityContextInterface;

/**
 * Registration handling controller
 *
 * @Route("/user")
 */
class RegistrationController extends FOSRegistrationController
{
    /**
     * @param Request $request
     * @param string  $type
     *
     * @Route("/register", defaults={"type" = ""}, name="rdw_user.registration.index")
     * @Route("/register/{type}", requirements={"type" = "employee|employer"}, name="rdw_user.registration.index")
     * @Method({"GET"})
     * @Template("RDWUserBundle:Registration:register.html.twig")
     *
     * @return Response
     */
    public function indexAction(Request $request, $type = '')
    {
        $user = $this->createUser($type);
        $form = $this->createRegistrationForm($request, $user);

        // save registration type to session, later its will be user for social networks registration
        if ($type) {
            $request->getSession()->set('_user_type', $type);
        }

        return [
            'type' => $type,
            'form' => $form->createView(),
        ];
    }

    /**
     * @param Request $request
     * @param string  $type
     *
     * @Route("/register/{type}", requirements={"type" = "employee|employer"}, name="rdw_user.registration.register")
     * @Method({"POST"})
     * @Template("RDWUserBundle:Registration:register.html.twig")
     *
     * @return Response
     */
    public function registerAction(Request $request, $type = '')
    {
        $dispatcher = $this->container->get('event_dispatcher');

        $user = $this->createUser($type);
        $form = $this->createRegistrationForm($request, $user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(RDWUserEvents::REGISTRATION_SUCCESS, $event);

            $this->container->get('rdw_user.service.user_manager')->registerNewUser($user);

            if (null === $response = $event->getResponse()) {
                $url = $this->container->get('router')->generate('fos_user_registration_confirmed');
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
            $dispatcher->dispatch(RDWUserEvents::UPDATE_USER_PROFILE_PERCENT, new SingleUserEvent($user));

            if ($request->isXmlHttpRequest()) {
                $data = [];
                $data['success'] = true;
                $data['redirect'] = $response->getTargetUrl();

                return new JsonResponse($data);
            } else {
                return $response;
            }
        } else {
            $this->container->get('session')->set('_digit1', null);
            $this->container->get('session')->set('_digit2', null);

            if ($request->isXmlHttpRequest()) {
                $data = [];
                $data['success'] = false;
                $data['content'] = $this->renderView(
                    sprintf('RDWUserBundle:Registration:form_%s.html.twig', $type),
                    [
                        'last_username' => (null === $request->getSession())
                            ? ''
                            : $request->getSession()->get(SecurityContextInterface::LAST_USERNAME),
                        'csrf_token' => $this->container->has('form.csrf_provider')
                            ? $this->container->get('form.csrf_provider')->generateCsrfToken('authenticate')
                            : null,
                        'form' => $form->createView(),
                        'type' => $type,
                        'popup' => true,
                    ]
                );

                return new JsonResponse($data);
            }
        }

        return [
            'type' => $type,
            'form' => $form->createView(),
        ];
    }

    private function createRegistrationForm(Request $request, $user)
    {
        $formFactory = $this->container->get('form.factory');
        $dispatcher = $this->container->get('event_dispatcher');

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->create('rdw_user_registration', $user);

        return $form;
    }

    private function createUser($type)
    {
        $user = $this->container->get('rdw_user.service.user_manager')->createUser();
        $user->setEnabled(true);
        $user->setType($type);

        return $user;
    }

    /**
     * Receive the confirmation token from user email provider, login the user
     *
     * @param Request $request
     * @param string  $token
     *
     * @return RedirectResponse|mixed
     *
     * @throws NotFoundHttpException
     */
    public function confirmAction(Request $request, $token)
    {
        /** @var RegisteredUser $user */
        $user = $this->container->get('rdw_user.service.user_manager')->findUserByConfirmationToken($token);

        if (null === $user) {
            return new RedirectResponse($this->container->get('router')->generate('fos_user_security_login'));
        }

        $dispatcher = $this->getEventDispatcher();

        $user->setConfirmationToken(null);
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRM, $event);

        $this->container->get('rdw_user.service.user_manager')->updateUser($user);

        if (null === $response = $event->getResponse()) {
            $url = $this->container->get('router')->generate('rdw_user.user.dashboard');
            $response = new RedirectResponse($url);
        }

        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRMED, new FilterUserResponseEvent($user, $request, $response));

        if ($targetUrl = $request->get('_target_path')) {
            $response = new RedirectResponse($targetUrl);
        }

        return $response;
    }

    /**
     * @param Request $request
     * @param string  $token
     *
     * @Route("/confirm-social/{token}", name="rdw_user.registration.confirm_social")
     * @Method({"GET"})
     * @Template("RDWUserBundle:Registration:register.html.twig")
     *
     * @return Response
     */
    public function confirmSocialAction(Request $request, $token)
    {
        throw new \RuntimeException('Refactor');

        $user = $this
            ->get('doctrine.orm.default_entity_manager')
            ->getRepository('RDWUserBundle:AnonymousUser')
            ->findOneBy(['confirmationToken' => $token]);

        if ($user instanceof AnonymousUser) {

            $url = $this->container->get('router')->generate('rdw_user.profile.edit');
            $response = new RedirectResponse($url);

            // confirm and finish user registration
            $user = $this->get('rdw_user.service.social_manager')->finishSocialRegistration($user);
            $dispatcher = $this->get('event_dispatcher');
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
            $dispatcher->dispatch(RDWUserEvents::UPDATE_USER_PROFILE_PERCENT, new SingleUserEvent($user));

            return $response;
        } else {
            throw $this->createNotFoundException('Confirmation token is not valid');
        }
    }

    /**
     * @Route("/registration-popup/{type}", name="rdw_user.registration.popup")
     * @Method({"GET"})
     *
     * @return JsonResponse
     */
    public function registrationPopupAction(Request $request, $type = RegisteredUser::USER_TYPE_EMPLOYER)
    {
        $data = [];
        $data['popup'] = $this->getRegistrationForm(
            'RDWUserBundle:Registration:popup.html.twig',
            $request,
            $type
        );

        return new JsonResponse($data);
    }

    /**
     * @return string
     */
    public function getRegistrationFormAction(Request $request, $type = RegisteredUser::USER_TYPE_EMPLOYER)
    {
        return new Response(
            $this->getRegistrationForm('RDWUserBundle:Registration:registration_form.html.twig',
                $request,
                $type
            )
        );
    }

    /**
     * @param $template
     * @param Request $request
     * @param $type
     * @return string
     */
    protected function getRegistrationForm($template, Request $request, $type, $redirectUrl = null)
    {
        $user = $this->createUser($type);
        $form = $this->createRegistrationForm($request, $user);

        return $this->renderView(
            $template,
            [
                'last_username' => (null === $request->getSession())
                    ? ''
                    : $request->getSession()->get(SecurityContextInterface::LAST_USERNAME),
                'csrf_token' => $this->container->has('form.csrf_provider')
                    ? $this->container->get('form.csrf_provider')->generateCsrfToken('authenticate')
                    : null,
                'form' => $form->createView(),
                'type' => $type,
                'redirect_url' => $this->get('session')->has('referer') ? $this->get('session')->get('referer') : null
            ]
        );
    }


    /**
     * @return \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    protected function getEventDispatcher()
    {
        return $this->container->get('event_dispatcher');
    }
}
