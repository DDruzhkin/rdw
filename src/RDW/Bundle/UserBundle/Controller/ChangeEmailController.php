<?php

namespace RDW\Bundle\UserBundle\Controller;

use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\UserEvent;
use RDW\Bundle\UserBundle\RDWUserEvents;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Change email controller class
 * Handles change User email logic
 *
 * Class ChangeEmailController
 *
 * @Route("/user/email")
 *
 * @package User
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class ChangeEmailController extends Controller
{
    /**
     * Change email address
     *
     * @param Request $request
     *
     * @Route("/change", name="rdw_user.change_email.change")
     * @Method({"GET", "POST"})
     * @Template()
     *
     * @return array
     */
    public function changeAction(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createForm('rdw_user_change_email', $user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new FormEvent($form, $request);
            $this->getEventDispatcher()->dispatch(RDWUserEvents::CHANGE_EMAIL_SUCCESS, $event);

            $this->getUserManager()->updateUser($user);
            $this->get('braincrafted_bootstrap.flash')->success('Elektroninio pašto keitimo nuoroda išsiųsta jūsų naujuoju elektroniniu pašto adresu.');

            return $this->redirect($this->generateUrl('rdw_user.change_email.change'));
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * Confirm changed email address
     *
     * @param string  $token
     * @param Request $request
     *
     * @Route("/confirm/{token}", name="rdw_user.change_email.confirm")
     * @Method({"GET"})
     *
     * @return array
     * @throws NotFoundHttpException
     */
    public function confirmAction($token, Request $request)
    {
        $user = $this->getUserManager()->findUserByConfirmationToken($token);

        if (!$user) {
            throw $this->createNotFoundException('The user with confirmation tokens does not exist');
        }

        $event = new UserEvent($user, $request);
        $this->getEventDispatcher()->dispatch(RDWUserEvents::CONFIRM_CHANGED_EMAIL_SUCCESS, $event);

        $this->getUserManager()->updateUser($user);
        $this->get('braincrafted_bootstrap.flash')->success(
            $this->get('translator')->trans('Your new email address changed to: %email%.', ['%email%' => $user->getEmail()])
        );

        return $this->redirect($this->generateUrl('rdw_user.profile.edit'));
    }

    /**
     * @return \RDW\Bundle\UserBundle\Service\UserManager
     */
    protected function getUserManager()
    {
        return $this->container->get('rdw_user.service.user_manager');
    }

    /**
     * @return \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    protected function getEventDispatcher()
    {
        return $this->container->get('event_dispatcher');
    }
}
