<?php

namespace RDW\Bundle\UserBundle\Controller;

use RDW\Bundle\AppBundle\Entity\AdInterface;
use RDW\Bundle\AppBundle\Event\GenericGuestEvent;
use RDW\Bundle\UserBundle\Entity\CvFavorite;
use RDW\Bundle\UserBundle\Entity\JobFavorite;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\UserInterface;
use RDW\Bundle\UserBundle\Form\Type\FavoriteCollectionType;
use RDW\Bundle\UserBundle\Model\FavoriteFilter;
use RDW\Bundle\UserBundle\Repository\FavoriteRepository;
use RDW\Bundle\UserBundle\Service\FavoriteManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


/**
 * Class FavoriteController
 *
 * @package RDW\Bundle\UserBundle\Controller
 *
 * @Route("/user/favorite")
 */
class FavoriteController extends Controller
{
    /**
     * @param Request $request
     * @param string $type
     * @param int $adId
     *
     * @return JsonResponse
     * @Route("/add/{type}/{adId}", name="rdw_user.favorite.add", requirements={"adId" = "\d+", "type" = "cv|job"})
     * @Method({"GET"})
     *
     */
    public function addAction(Request $request, $type, $adId)
    {
        $type = ucfirst($type);
        /** @var AdInterface $ad */
        $ad = $this->getDoctrine()->getRepository(sprintf('RDW%sBundle:%s', $type, $type))->find($adId);

        $user = $this->getUser();
        $favorite = $this->getFavoriteManager()->create($ad, $user);
        $event = new GenericGuestEvent($request, $favorite);
        $this->container->get('event_dispatcher')->dispatch('rdw_app.favorite.pre_create', $event);

        $return['success'] = true;

        if ($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $return['success'] = $this->getFavoriteManager()->insert($favorite);

            $return['success_message'] = ('job' === $type)
                ? $this->get('translator')->trans('Job was added to your dashboard')
                : $this->get('translator')->trans('Cv was added to your dashboard');

            $method = sprintf('countUserFavorite%ss', $type);
            $return['favorite_number'] = $this->getFavoriteManager()->getRepository($favorite)->$method($user);
            $return['url'] = $this->generateUrl('rdw_user.favorite.remove', ['adId' => $ad->getId(), 'type' => strtolower($type)], true);
        } else {
            $registrationType = ('cv' == strtolower($type)) ? RegisteredUser::USER_TYPE_EMPLOYER : RegisteredUser::USER_TYPE_EMPLOYEE;
            $return['needRegister'] = true;
            $return['needRegisterUrl'] = $this->generateUrl('rdw_user.registration.popup', ['type' => $registrationType], true);
        }

        return new JsonResponse($return);
    }

    /**
     * @param string $type
     * @param int $adId
     *
     * @Route("/remove/{type}/{adId}", name="rdw_user.favorite.remove", requirements={"adId" = "\d+", "type" =
     *     "cv|job"})
     * @Method({"GET"})
     *
     * @return JsonResponse
     */
    public function removeAction($type, $adId)
    {
        $user = $this->getUser();

        $type = ucfirst($type);
        $ad = $this->getDoctrine()->getRepository(sprintf('RDW%sBundle:%s', $type, $type))->find($adId);
        $favorite = $this->getFavoriteManager()->findByUser($ad, $user);

        $return['success'] = false;

        if ($user instanceof UserInterface && $ad instanceof AdInterface) {
            $method = sprintf('remove%sFavoriteBy%sAndUser', $type, $type);
            $return['success'] = $this->getFavoriteManager()->$method($ad, $user);
            $return['success_message'] = $this->get('translator')->trans('Job was removed from your dashboard');

            $method = sprintf('countUserFavorite%ss', $type);
            $return['favorite_number'] = $this->getFavoriteManager()->getRepository($favorite)->$method($user);
            $return['url'] = $this->generateUrl('rdw_user.favorite.add', ['adId' => $ad->getId(), 'type' => strtolower($type)], true);
        }

        return new JsonResponse($return);
    }

    /**
     * @param JobFavorite $favorite
     *
     * @Route("/job-edit/{id}", requirements={"id" = "\d+"}, name="rdw_user.favorite.job_edit")
     * @Template()
     * @Method({"GET", "POST"})
     *
     * @return array|RedirectResponse
     * @throws AccessDeniedException
     */
    public function jobEditAction(JobFavorite $favorite)
    {
        $this->isGrantedLocal();

        if (!$favorite->isOwner($this->getUser())) {
            throw new AccessDeniedException();
        }

        $form = $this->createForm('rdw_user_job_favorite', $favorite);

        return [
            'form' => $form->createView(),
            'id' => $favorite->getId(),
        ];
    }

    /**
     * @param Request $request
     * @param JobFavorite $favorite
     *
     * @Route("/save-job/{id}",name="rdw_user.favorite.save_job_favorite", requirements={"id" = "\d+"})
     * @Method({"POST"})
     *
     * @return JsonResponse
     * @throws AccessDeniedException
     */
    public function saveJobFavoriteAction(Request $request, JobFavorite $favorite)
    {
        $this->isGrantedLocal();

        if (!$favorite->isOwner($this->getUser())) {
            throw new AccessDeniedException();
        }

        $data['success'] = false;
        $response = new JsonResponse();

        $form = $this->createForm('rdw_user_job_favorite', $favorite);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getFavoriteManager()->save($favorite);
            $data['success'] = true;
        }

        $data['content'] = $this->renderView('RDWUserBundle:Favorite:_form_job.html.twig', [
            'form' => $form->createView(),
            'id' => $favorite->getId(),
        ]);

        $response->setData($data);

        return $response;
    }

    /**
     * @param CvFavorite $favorite
     *
     * @Route("/cv-edit/{id}", requirements={"id" = "\d+"}, name="rdw_user.favorite.cv_edit")
     * @Template()
     * @Method({"GET"})
     *
     * @return array|RedirectResponse
     * @throws AccessDeniedException
     */
    public function cvEditAction(CvFavorite $favorite)
    {
        $this->isGrantedLocal();

        if (!$favorite->isOwner($this->getUser())) {
            throw new AccessDeniedException();
        }

        $form = $this->createForm('rdw_user_cv_favorite', $favorite);

        return [
            'form' => $form->createView(),
            'favorite' => $favorite,
        ];
    }

    /**
     * @param Request $request
     * @param CvFavorite $favorite
     *
     * @Route("/save-cv/{id}", name="rdw_user.favorite.save_cv_favorite", requirements={"id" = "\d+"})
     * @Method({"POST"})
     *
     * @return JsonResponse
     * @throws AccessDeniedException
     */
    public function saveCvFavoriteAction(Request $request, CvFavorite $favorite)
    {
        $this->isGrantedLocal();

        if (!$favorite->isOwner($this->getUser())) {
            throw new AccessDeniedException();
        }

        $data['success'] = false;

        $form = $this->createForm('rdw_user_cv_favorite', $favorite);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getFavoriteManager()->save($favorite);

            $data['success'] = true;
        }

        $data['content'] = $this->renderView('RDWUserBundle:Favorite:_form_cv.html.twig', [
            'form' => $form->createView(),
            'favorite' => $favorite,
        ]);

        $response = new JsonResponse();
        $response->setData($data);

        return $response;
    }

    /**
     * @param Request $request
     * @param string $type
     *
     * @Route("/{type}-list", name="rdw_user.favorite.list", requirements={"type": "cv|job"})
     * @Route("/{type}-list/archived", name="rdw_user.favorite.list_archived", requirements={"type": "cv|job"})
     * @Method({"GET"})
     *
     * @return Response
     */
    public function listAction(Request $request, $type)
    {
        $this->isGrantedLocal();
        $user = $this->getUser();

        $isArchivedVisible = ('rdw_user.favorite.list_archived' == $request->get('_route')) ? true : false;

        $filter = new FavoriteFilter();
        $filter->setUser($user);
        $filter->setType($type);
        $filter->setArchivedVisible($isArchivedVisible);

        $favoriteManager = $this->container->get('rdw_user.service.favorite_manager');
        $favorites = $favoriteManager->getPaginatedList(
            $filter,
            $request->query->get('page', 1),
            $this->getPerPageNumber()
        );

        $form = $this->createForm(new FavoriteCollectionType($favorites->getItems(), $filter->getType()));

        $data = [
            'pagination' => $favorites,
            'isArchivedVisible' => $isArchivedVisible,
            'form' => $form->createView(),
            'type' => $type,
        ];

        return $this->render('RDWUserBundle:Favorite:list.html.twig', $data);
    }

    /**
     * @param int $id
     *
     * @Route("/cv-archive/{id}", name="rdw_user.favorite.cv_archive", requirements={"id" = "\d+"})
     * @Method({"GET"})
     *
     * @return JsonResponse
     */
    public function cvArchiveAction($id)
    {
        $this->isGrantedLocal();

        $return['success'] = false;

        $favorite = $this->getFavoriteManager()->getCvFavoriteById($id);

        if ($this->getFavoriteManager()->isAllowedToArchive($favorite, $this->getUser())) {
            $this->getFavoriteManager()->archive($favorite);
            $return['success'] = true;
        }

        $response = new JsonResponse();
        $response->setData($return);

        return $response;
    }

    /**
     * @param int $id
     *
     * @Route("/job-archive/{id}", name="rdw_user.favorite.job_archive", requirements={"id" = "\d+"})
     * @Method({"GET"})
     *
     * @return JsonResponse
     */
    public function jobArchiveAction($id)
    {
        $this->isGrantedLocal();

        $return['success'] = false;

        $favorite = $this->getFavoriteManager()->getJobFavoriteById($id);

        if ($this->getFavoriteManager()->isAllowedToArchive($favorite, $this->getUser())) {
            $this->getFavoriteManager()->archive($favorite);
            $return['success'] = true;
        }

        $response = new JsonResponse();
        $response->setData($return);

        return $response;
    }

    /**
     * @param Request $request
     * @param string $type
     *
     * @Route("/multiple-delete/{type}", name="rdw_user.favorite.multiple_delete")
     * @Method({"POST"})
     *
     * @return RedirectResponse
     */
    public function multipleDeleteAction(Request $request, $type)
    {
        $user = $this->getUser();
        $ids = $request->request->get('favorite_collection', ['id' => []])['id'];
        $form = $this->createForm(new FavoriteCollectionType($ids, $type));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getFavoriteManager()->multipleRemoveByUser($form->get('id')->getData(), $user);

            $this->container->get('braincrafted_bootstrap.flash')->success('Successfully deleted');
        }

        return $this->redirect($this->generateUrl('rdw_user.favorite.list', ['type' => $type]));
    }

    /**
     * @param Request $request
     * @param string $type
     *
     * @Route(
     *      "/multiple-move-to-archive/{type}",
     *      name="rdw_user.favorite.multiple_move_to_archive",
     *      requirements={"type": "cv|job"}
     * )
     * @Method({"POST"})
     *
     * @return RedirectResponse
     */
    public function multipleMoveToArchiveAction(Request $request, $type)
    {
        $user = $this->getUser();
        $ids = $request->request->get('favorite_collection', ['id' => []])['id'];
        $form = $this->createForm(new FavoriteCollectionType($ids, $type));
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getFavoriteManager()->multipleMoveToArchiveByUser($form->get('id')->getData(), $user);

            $this->container->get('braincrafted_bootstrap.flash')->success('Successfully archived ' . $type);
        }

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    private function isGrantedLocal($role = 'ROLE_REGISTERED')
    {
        if (!$this->container->get('security.context')->isGranted($role)) {
            throw new AccessDeniedException();
        }
    }

    /**
     * @return FavoriteManager
     */
    private function getFavoriteManager()
    {
        return $this->get('rdw_user.service.favorite_manager');
    }

    /**
     * Extract per page param from query string
     *
     * @return int
     */
    private function getPerPageNumber()
    {
        $current = $this->get('request')->query->get('per_page');
        $options = $this->container->getParameter('rdw_user.favorite_list.per_page.options');

        $perPage = ($current && in_array($current, $options)) ? $current : $options[0];

        return $perPage;
    }

    /**
     * @param Request $request
     * @param CvFavorite $favorite
     *
     * @Route("/save-cv-time/{id}", name="rdw_user.favorite.save_cv_favorite_time", requirements={"id" = "\d+"})
     * @Method({"POST"})
     *
     * @return JsonResponse
     * @throws AccessDeniedException
     */
    public function saveCvFavoriteTimeAction(Request $request, CvFavorite $favorite)
    {
        $this->isGrantedLocal();

        if (!$favorite->isOwner($this->getUser())) {
            throw new AccessDeniedException();
        }

        $data = ['success' => false];
        $response = new JsonResponse();

        if ($meetTime = $request->get('meetTime')) {
            $favorite->setMeetTime(new \DateTime($meetTime));
            $this->getFavoriteManager()->update($favorite);

            $data['meetTime'] = $meetTime;
            $data['success'] = true;
        }

        $response->setData($data);

        return $response;
    }

    /**
     * @param Request $request
     * @param JobFavorite $favorite
     *
     * @Route("/save-job-time/{id}", name="rdw_user.favorite.save_job_favorite_time", requirements={"id" = "\d+"})
     * @Method({"POST"})
     *
     * @return JsonResponse
     * @throws AccessDeniedException
     */
    public function saveJobFavoriteTimeAction(Request $request, JobFavorite $favorite)
    {
        $this->isGrantedLocal();

        if (!$favorite->isOwner($this->getUser())) {
            throw new AccessDeniedException();
        }

        $data = ['success' => false];
        $response = new JsonResponse();

        if ($meetTime = $request->get('meetTime')) {
            $favorite->setMeetTime(new \DateTime($meetTime));
            $this->getFavoriteManager()->update($favorite);

            $data['meetTime'] = $meetTime;
            $data['success'] = true;
        }

        $response->setData($data);

        return $response;
    }

    /**
     * @return FavoriteRepository
     */
    private function getCvFavoriteRepository()
    {
        return $this->getDoctrine()->getRepository('RDWUserBundle:CvFavorite');
    }

    /**
     * @return FavoriteRepository
     */
    private function getJobFavoriteRepository()
    {
        return $this->getDoctrine()->getRepository('RDWUserBundle:JobFavorite');
    }
}
