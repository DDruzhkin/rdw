<?php

namespace RDW\Bundle\UserBundle\Controller;

use FOS\UserBundle\Event\UserEvent;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Event\SingleUserEvent;
use RDW\Bundle\UserBundle\Form\Type\ClientSearchType;
use RDW\Bundle\UserBundle\Form\Type\EmployerNoteFormType;
use RDW\Bundle\UserBundle\Model\WebUserFilter;
use RDW\Bundle\UserBundle\RDWUserEvents;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class UserController
 *
 * @package RDW\Bundle\UserBundle\Controller
 *
 * @Route("/user")
 *
 */
class UserController extends Controller
{
    /**
     * @Route("/dashboard", name="rdw_user.user.dashboard")
     * @Template()
     *
     * @return array
     */
    public function dashboardAction()
    {
        return [];
    }

    /**
     * @param Request $request
     *
     * @Route("/create-employer", name="rdw_user.user.create_employer")
     * @Template()
     *
     * @return array|RedirectResponse
     */
    public function createEmployerAction(Request $request)
    {
        if (! $this->container->get('security.context')->isGranted('ROLE_MANAGER')) {
            throw new AccessDeniedHttpException();
        }

        /** @var RegisteredUser $user */
        $user = $this->container->get('rdw_user.service.user_manager')->createUser();
        $user->setType(RegisteredUser::USER_TYPE_EMPLOYER);
        $user->setManager($this->getUser());

        $form = $this->createForm('rdw_employer', $user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new SingleUserEvent($form->getData());

            $this->container->get('event_dispatcher')->dispatch(RDWUserEvents::PRE_CREATE, $event);

            $this->container->get('rdw_user.service.user_manager')->updateProfile($user);

            // dispatch event to send new password email and email for administrator
            $this->container->get('event_dispatcher')->dispatch(RDWUserEvents::POST_CREATE, $event);

            $this->get('braincrafted_bootstrap.flash')->success('You successfully created employer');

            return $this->redirect($this->get('router')->generate('rdw_user.user.employer_list'));
        }

        return [
            'form' => $form->createView(),
            'user' => $user,
        ];
    }

    /**
     * @param Request        $request
     * @param RegisteredUser $user
     *
     * @Route("/edit-employer/{id}", requirements={"id" = "\d+"}, name="rdw_user.user.edit_employer")
     * @Template()
     *
     * @return array|RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function editEmployerAction(Request $request, RegisteredUser $user)
    {
        if (! $this->container->get('security.context')->isGranted('ROLE_MANAGER')) {
            throw new AccessDeniedHttpException();
        }

        $user->setOldEmail($user->getEmail());

        $form = $this->createForm('rdw_employer', $user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->container->get('rdw_user.service.user_manager')->updateProfile($user);
            $this->container->get('event_dispatcher')->dispatch(RDWUserEvents::UPDATE_USER_PROFILE_PERCENT, new SingleUserEvent($user));

            $this->get('braincrafted_bootstrap.flash')->success('User successfully updated');

            if ($user->getNewEmail()) {
                $event = new FormEvent($form, $request);
                $this->container->get('event_dispatcher')->dispatch(RDWUserEvents::CHANGE_EMAIL_SUCCESS, $event);

                $this->container->get('rdw_user.service.user_manager')->updateUser($user);
                $this->get('braincrafted_bootstrap.flash')->alert(
                    $this->get('translator')->trans('Email confirmation link has been sent to your new email address')
                );
            }

            return $this->redirect($this->get('router')->generate('rdw_user.user.employer_list'));
        }

        return [
            'form' => $form->createView(),
            'user' => $user,
        ];
    }

    /**
     * @param Request        $request
     * @param RegisteredUser $user
     *
     * @Route("/generate-password/{id}", requirements={"id" = "\d+"}, name="rdw_user.user.generate_password")
     *
     * @return RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function generatePasswordAction(Request $request, RegisteredUser $user)
    {
        if (! $this->container->get('security.context')->isGranted('ROLE_MANAGER')) {
            throw new AccessDeniedHttpException();
        }

        $event = new UserEvent($user, $request);
        $this->container->get('event_dispatcher')->dispatch(RDWUserEvents::GENERATE_NEW_PASSWORD, $event);

        $this->container->get('rdw_user.service.user_manager')->unblockUser($user);

        $this->get('braincrafted_bootstrap.flash')->success('New password was generated successfully');

        return new RedirectResponse($this->get('router')->generate('rdw_user.user.edit_employer', ['id' => $user->getId()]));
    }

    /**
     * @param Request $request
     *
     * @Route("/employer-list", name="rdw_user.user.employer_list")
     * @Template()
     *
     * @return array
     * @throws AccessDeniedHttpException
     */
    public function employerListAction(Request $request)
    {
        if (! $this->container->get('security.context')->isGranted('ROLE_MANAGER')) {
            throw new AccessDeniedHttpException();
        }

        $filter = new WebUserFilter();
        $filter->setType(RegisteredUser::USER_TYPE_EMPLOYER);
        $filter->setManager($this->getUser());

        $form = $this->createForm(new ClientSearchType());
        $form->handleRequest($request);

        if ($form->isValid()) {
            $filter->setCompanyTitle($form->get('companyTitle')->getData());
        }

        $pagination = $this->container->get('rdw_user.service.user_manager')->getPaginatedWebUserList(
            $filter,
            $request->query->get('page', 1),
            $this->container->getParameter('rdw_user.web_user.paginator.per_page')
        );

        return [
            'pagination' => $pagination,
        ];
    }

    /**
     * @Cache(smaxage="600")
     *
     * @return Response
     */
    public function topEmployersAction($withCount = null)
    {
        $limit = $this->container->getParameter('rdw_user.top_employers_limit');
        $topEmployers = $this->getDoctrine()->getRepository('RDWOrderBundle:OrderItemJob')->findAllTopEmployers($limit);

        if ($withCount) {
            $employerInfo = [];

            foreach ($topEmployers as $key => $value) {
                $employerInfo[$value->getId()]['company'] = $value;
                $employerInfo[$value->getId()]['countActiveJobs'] = $this->get('rdw_job.service.job_manager')->getCountUserActiveJobs($value->getOrder()->getUser());
            }
            
            $topEmployers = $employerInfo;
        }


        return $this->render('RDWUserBundle:User:topEmployers.html.twig', [
            'topEmployers' => $topEmployers,
        ]);
    }

    /**
     * @Template()
     *
     * @return array
     */
    public function clientsHavingWaitingJobsBlockAction()
    {
        $limit = $this->container->getParameter('rdw_user.clients_having_waiting_jobs_limit');
        $clients = $this->container->get('rdw_user.service.user_manager')->getWithWaitingJobs($this->getUser(), $limit);

        return [
            'clients' => $clients,
        ];
    }

    /**
     * @return Response
     */
    public function clientsWaitingJobsCountBlockAction()
    {
        $period = $this->container->getParameter('rdw_user.manager_dashboard.waiting_jobs_count_period');
        $waitingJobsCount = $this->getDoctrine()
            ->getRepository('RDWUserBundle:RegisteredUser')
            ->getNewestWaitingJobsCountByManager($this->getUser(), $period);

        return new Response($waitingJobsCount);
    }

    /**
     * @param Request $request
     *
     * @Template()
     *
     * @return array
     */
    public function clientSearchFormBlockAction(Request $request)
    {
        $form = $this->createForm(new ClientSearchType());
        $form->handleRequest($request);

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @param RegisteredUser $registeredUser
     *
     * @Route("/note/{id}", requirements={"id" = "\d+"}, name="rdw_user.user.employer_note")
     * @Template()
     *
     * @return array|RedirectResponse
     * @throws AccessDeniedException
     */
    public function noteEditAction(RegisteredUser $registeredUser)
    {
        if (! $this->container->get('security.context')->isGranted('ROLE_MANAGER')) {
            throw new AccessDeniedHttpException();
        }

        $form = $this->createForm(new EmployerNoteFormType(), $registeredUser);

        return [
            'form' => $form->createView(),
            'user' => $registeredUser,
        ];
    }

    /**
     * @param Request        $request
     * @param RegisteredUser $registeredUser
     *
     * @Route("/save-cv/{id}", name="rdw_user.user.employer_note_save", requirements={"id" = "\d+"})
     * @Method({"POST"})
     *
     * @return JsonResponse
     * @throws AccessDeniedException
     */
    public function noteSaveAction(Request $request, RegisteredUser $registeredUser)
    {
        if (! $this->container->get('security.context')->isGranted('ROLE_MANAGER')) {
            throw new AccessDeniedHttpException();
        }

        $data['success'] = false;

        $form = $this->createForm(new EmployerNoteFormType(), $registeredUser);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->container->get('doctrine')->getManager()->persist($registeredUser);
            $this->container->get('doctrine')->getManager()->flush();

            $data['success'] = true;
        }

        $data['content'] = $this->renderView('RDWUserBundle:User:_employer_note_form.html.twig', [
                'form' => $form->createView(),
                'user' => $registeredUser,
            ]);

        $response = new JsonResponse();
        $response->setData($data);

        return $response;
    }
}
