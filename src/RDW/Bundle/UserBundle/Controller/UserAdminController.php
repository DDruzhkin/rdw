<?php

namespace RDW\Bundle\UserBundle\Controller;

use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\UserEvent;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Model\WebUserFilter;
use RDW\Bundle\UserBundle\RDWUserEvents;
use RDW\Bundle\UserBundle\Event\SingleUserEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class UserAdminController
 *
 * @package RDW\Bundle\UserBundle\Controller
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 *
 * @Route("/manage/web-user")
 */
class UserAdminController extends Controller
{
    /**
     * @param Request $request
     *
     * @Route("/list", name="rdw_user.user_admin.list")
     * @Template("RDWUserBundle:UserAdmin:list.html.twig")
     *
     * @return array
     */
    public function listAction(Request $request)
    {
        $this->isGrantedLocal();

        $filter = new WebUserFilter();

        $filterForm = $this->createForm('user_filter', $filter);
        $filterForm->handleRequest($request);

        if ($filterForm->get('clear')->isClicked()) {
            return $this->redirect($this->get('router')->generate('rdw_user.user_admin.list'));
        }

        $pagination = $this->getUserManager()->getPaginatedWebUserList(
            $filter,
            $request->query->get('page', 1),
            $this->container->getParameter('rdw_user.web_user.paginator.per_page')
        );

        return [
            'filterForm' => $filterForm->createView(),
            'pagination' => $pagination,
        ];
    }

    /**
     * @param Request $request
     * @param string  $type
     *
     * @Route("/create/{type}", name="rdw_user.user_admin.create", requirements={"type" = "employee|employer"})
     * @Template()
     *
     * @return array|RedirectResponse
     */
    public function createAction(Request $request, $type = '')
    {
        $this->isGrantedLocal();

        $user = $this->getUserManager()->createUserForAdmin($type);

        $formType = ($user->isTypeEmployer()) ? 'rdw_manage_employer' : 'rdw_manage_employee';

        $form = $this->createForm($formType, $user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new SingleUserEvent($form->getData());

            $this->getEventDispatcher()->dispatch(RDWUserEvents::PRE_CREATE, $event);

            $this->getUserManager()->updateProfile($user);

            // dispatch event to send new password email and email for administrator
            $this->getEventDispatcher()->dispatch(RDWUserEvents::POST_CREATE, $event);

            $this->get('braincrafted_bootstrap.flash')->success('You successfully created employer');

            $url = $request->query->get('redirectUrl')
                ? $request->query->get('redirectUrl')
                : $this->generateUrl('rdw_user.user_admin.list');

            return new RedirectResponse($url);
        }

        return [
            'form' => $form->createView(),
            'user' => $user,
        ];
    }

    /**
     * @param RegisteredUser $user
     *
     * @Route("/delete/{id}", requirements={"id" = "\d+"}, name="rdw_user.user_admin.delete")
     *
     * @return RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteAction(RegisteredUser $user)
    {
        $this->isGrantedLocal();

        $this->getEventDispatcher()->dispatch(RDWUserEvents::PRE_REMOVE, new SingleUserEvent($user));
        $this->getUserManager()->deleteUser($user);

        return new RedirectResponse($this->get('router')->generate('rdw_user.user_admin.list'));
    }

    /**
     * @param Request        $request
     * @param RegisteredUser $user
     *
     * @Route("/edit/{id}", requirements={"id" = "\d+"}, name="rdw_user.user_admin.edit")
     * @Template()
     *
     * @return array|RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function editAction(Request $request, RegisteredUser $user)
    {
        $this->isGrantedLocal();

        $oldUserManager = $user->getManager();

        $formType = ($user->isTypeEmployer()) ? 'rdw_manage_employer' : 'rdw_manage_employee';

        $form = $this->createForm($formType, $user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getUserManager()->updateProfile($user);

            $dispatcher = $this->getEventDispatcher();
            $dispatcher->dispatch(RDWUserEvents::UPDATE_USER_PROFILE_PERCENT, new SingleUserEvent($user));

            // if manager has been changed inform him about new client
            if (
                $user->getManager() // if has assigned manager
                && (
                    ! $oldUserManager // and has not has it before
                    || ($oldUserManager && $oldUserManager->getId() != $user->getManager()->getId()) // or new manager is other than before
                )
            ) {
                $mailerManager = $this->container->get('rdw_mailer.service.mailer_manager');

                $mailerManager->sendEmailForManagerAboutNewUser($user);

                // also for user
                $user->getManager()->setEmail('giedrius+testas@eface.lt');
                $mailerManager->sendEmailForUserAboutAssignedManager($user);
            }

            $this->get('braincrafted_bootstrap.flash')->success('User successfully updated');

            $url = $request->query->get('redirectUrl')
                ? $request->query->get('redirectUrl')
                : $this->generateUrl('rdw_user.user_admin.list');

            return new RedirectResponse($url);
        }

        return [
            'form' => $form->createView(),
            'user' => $user
        ];
    }

    /**
     * @param Request        $request
     * @param RegisteredUser $user
     *
     * @Route("/block/{id}", requirements={"id" = "\d+"}, name="rdw_user.user_admin.block")
     * @Template()
     *
     * @return array|RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function blockAction(Request $request, RegisteredUser $user)
    {
        $this->isGrantedLocal();

        $form = $this->createForm('rdw_user_block', $user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new FormEvent($form, $request);
            $this->getEventDispatcher()->dispatch(RDWUserEvents::BLOCK_USER, $event);

            $this->getUserManager()->blockUser($user);

            $this->get('braincrafted_bootstrap.flash')->success('User blocked');

            return $this->redirect($this->get('router')->generate('rdw_user.user_admin.list'));
        }

        return [
            'form' => $form->createView(),
            'user' => $user
        ];
    }

    /**
     * @param Request $request
     * @param int     $userId
     *
     * @Route("/personalize/{userId}", requirements={"userId" = "\d+"}, name="rdw_user.user_admin.personalize")
     * @Template()
     *
     * @return array|RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function personalizeAction(Request $request, $userId)
    {
        $this->isGrantedLocal();

        $user = $this->getUserManager()->findUserByIdForAdmin($userId);

        if ( ! $user) {
            throw new NotFoundHttpException(sprintf('The user with id "%d" does not exist.', $userId));
        }

        $form = $this->createForm('rdw_user_personalize', $user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getUserManager()->updateUser($user);

            $this->get('braincrafted_bootstrap.flash')->success('User profile was personalized');

            return $this->redirect($this->get('router')->generate('rdw_user.user_admin.list'));
        }

        return [
            'form' => $form->createView(),
            'user' => $user
        ];
    }

    /**
     * @param RegisteredUser $user
     *
     * @Route("/unblock/{id}", requirements={"id" = "\d+"}, name="rdw_user.user_admin.unblock")
     *
     * @return RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function unblockAction(RegisteredUser $user)
    {
        $this->isGrantedLocal();

        $this->getUserManager()->unblockUser($user);
        $this->container->get('rdw_mailer.service.mailer_manager')->sendEmailForUnblockedUser($user);

        return new RedirectResponse($this->get('router')->generate('rdw_user.user_admin.edit', ['id' => $user->getId()]));
    }

    /**
     * @param Request        $request
     * @param RegisteredUser $user
     *
     * @Route("/generate-password/{id}", requirements={"id" = "\d+"}, name="rdw_user.user_admin.generate_password")
     *
     * @return RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function generatePasswordAction(Request $request, RegisteredUser $user)
    {
        $this->isGrantedLocal();

        $event = new UserEvent($user, $request);
        $this->getEventDispatcher()->dispatch(RDWUserEvents::GENERATE_NEW_PASSWORD, $event);

        // save data
        $this->getUserManager()->unblockUser($user);

        $this->get('braincrafted_bootstrap.flash')->success('New password was generated successfully');

        return new RedirectResponse($this->get('router')->generate('rdw_user.user_admin.edit', ['id' => $user->getId()]));
    }

    /**
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    private function isGrantedLocal()
    {
        if ( ! $this->container->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }
    }

    /**
     * @return \RDW\Bundle\UserBundle\Service\UserManager
     */
    protected function getUserManager()
    {
        return $this->container->get('rdw_user.service.user_manager');
    }

    /**
     * @return \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    protected function getEventDispatcher()
    {
        return $this->container->get('event_dispatcher');
    }
}
