<?php

namespace RDW\Bundle\UserBundle\Controller;

use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\FOSUserEvents;
use RDW\Bundle\LocationBundle\Entity\City;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use RDW\Bundle\UserBundle\Event\SingleUserEvent;
use RDW\Bundle\UserBundle\RDWUserEvents;

class ProfileController extends Controller
{
    /**
     * @Route("/profile/")
     * @Method({"GET"})
     *
     * @return RedirectResponse
     */
    public function profileAction()
    {
        return $this->redirect($this->container->get('router')->generate('rdw_app.default.index'));
    }

    /**
     * @Route("/user/profile/edit", name="rdw_user.profile.edit")
     * @Method({"GET"})
     * @Template()
     *
     * @return array
     */
    public function editAction()
    {
        $this->isGrantedLocal();

        /** @var RegisteredUser $user */
        $user = $this->getUser();

        if ($user->getCities()->count() == 0) {
            $locationManager = $this->get('rdw_location.service.location_manager');
            $defaultCity = $locationManager->getDefaultCity();

            if ($defaultCity instanceof City) {
                $user->addCity($defaultCity);
            }
        }

        $user->setOldEmail($user->getEmail());

        $form = $this->createForm($user->getFormNameByType(), $user);

        return [
            'form' => $form->createView(),
            'user' => $user,
        ];
    }

    /**
     * @param Request $request
     *
     * @Route("/user/profile/finish", name="rdw_user.profile.finish")
     * @Method({"GET", "POST"})
     * @Template()
     *
     * @return array
     */
    public function finishAction(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createForm('rdw_user_o_auth_registration', $user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new UserEvent($user, $request);
            $this->get('event_dispatcher')->dispatch(RDWUserEvents::FINISH_O_AUTH_SUCCESS, $event);

            $this->get('rdw_user.service.user_manager')->updateUser($user);

            return $this->render('RDWUserBundle:Profile:finish_confirm.html.twig');
        }

        $response = $this->render('RDWUserBundle:Profile:finish.html.twig', [
            'form' => $form->createView(),
        ]);

        return $response;
    }

    /**
     * @param Request $request
     *
     * @Route("/user/profile/update", name="rdw_user.profile.update")
     * @Method({"POST"})
     * @Template("RDWUserBundle:Profile:edit.html.twig")
     *
     * @return array|RedirectResponse
     */
    public function updateAction(Request $request)
    {
        $this->isGrantedLocal();

        $user = $this->getUser();
        $form = $this->createForm($user->getFormNameByType(), $user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $userManager = $this->getUserManager();
            $dispatcher = $this->getEventDispatcher();

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_SUCCESS, $event);

            $userManager->updateProfile($user);

            if($user->getNewEmail()) {
                $event = new FormEvent($form, $request);
                $this->getEventDispatcher()->dispatch(RDWUserEvents::CHANGE_EMAIL_SUCCESS, $event);

                $this->getUserManager()->updateUser($user);
                $this->get('braincrafted_bootstrap.flash')->alert(
                    $this->get('translator')->trans('Email confirmation link has been sent to your new email address')
                );
            }

            if (null === $response = $event->getResponse()) {
                $url = $this->container->get('router')->generate('rdw_user.profile.edit');
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(FOSUserEvents::PROFILE_EDIT_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
            $dispatcher->dispatch(RDWUserEvents::UPDATE_USER_PROFILE_PERCENT, new SingleUserEvent($user));

            return $response;
        }

        return [
            'form' => $form->createView(),
            'user' => $user,
        ];
    }

    /**
     * View user profile info
     *
     * @param RegisteredUser $user
     *
     * @Route("/user/profile/view/{id}", name="rdw_user.profile.view", requirements={"id" = "\d+"})
     * @Method({"GET"})
     * @Template()
     *
     * @return array|RedirectResponse
     *
     * @throws NotFoundHttpException
     */
    public function viewAction(RegisteredUser $user)
    {
        if ($user->isTypeEmployee() || $user->isManager()) {
            throw new NotFoundHttpException();
        }

        return [
            'user' => $user,
            'sortedCompanyJobs' => $this->get('rdw_job.service.job_manager')->getOrderedUserActiveJobs($user),
        ];
    }

    /**
     * Delete user profile
     *
     * @Route("/user/profile/delete", name="rdw_user.profile.delete")
     * @Method({"GET"})
     *
     * @return array|RedirectResponse
     *
     * @throws NotFoundHttpException
     */
    public function deleteAction()
    {
        $this->isGrantedLocal();

        $user = $this->getUser();

        $this->getEventDispatcher()->dispatch(RDWUserEvents::PRE_REMOVE, new SingleUserEvent($user));

        try {
            $this->getUserManager()->deleteProfile($user);
        } catch (\Exception $e) {
            throw new NotFoundHttpException('User does not exist or is already deleted');
        }

        $this->getMailerManager()->sendDeletedUserEmail($user);

        $this->get('session')->invalidate();

        return $this->redirect($this->container->get('router')->generate('rdw_app.default.index'), 302);
    }

    /**
     * @Route("/user/profile/assigned-manager", name="rdw_user.profile.assigned_manager")
     * @Method({"GET"})
     * @Template()
     *
     * @return array
     */
    public function assignedManagerAction()
    {
        $this->isGrantedLocal();

        /** @var RegisteredUser $user */
        $user = $this->getUser();

        return [
            'manager' => $user->getManager()
        ];
    }

    /**
     * @Template()
     *
     * @return array
     */
    public function profileCompletenessBlockAction()
    {
        return [];
    }

    /**
     * @throws AccessDeniedException
     */
    private function isGrantedLocal()
    {
        if ( ! $this->container->get('security.context')->isGranted('ROLE_REGISTERED')) {
            throw new AccessDeniedException();
        }
    }

    /**
     * @return \RDW\Bundle\UserBundle\Service\UserManager
     */
    protected function getUserManager()
    {
        return $this->container->get('rdw_user.service.user_manager');
    }

    /**
     * @return \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    protected function getEventDispatcher()
    {
        return $this->container->get('event_dispatcher');
    }

    /**
     * @return \RDW\Bundle\MailerBundle\Service\MailerManager
     */
    protected function getMailerManager()
    {
        return $this->container->get('rdw_mailer.service.mailer_manager');
    }
}
