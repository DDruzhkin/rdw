<?php

namespace RDW\Bundle\UserBundle\EventListener;

use FOS\UserBundle\Event\FormEvent;
use RDW\Bundle\MailerBundle\Service\MailerManager;
use RDW\Bundle\UserBundle\RDWUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class SendEmailForAdminListener
 *
 * @package RDW\Bundle\UserBundle\EventListener
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class BlockUserListener implements EventSubscriberInterface
{
    /**
     * @var MailerManager
     */
    private $mailerManager;


    /**
     * @param MailerManager $mailerManager
     */
    public function __construct(MailerManager $mailerManager)
    {
        $this->mailerManager = $mailerManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            RDWUserEvents::BLOCK_USER => 'onBlockUser',
        );
    }

    /**
     * @param FormEvent $event
     */
    public function onBlockUser(FormEvent $event)
    {
        /** @var $user \FOS\UserBundle\Model\UserInterface */
        $user = $event->getForm()->getData();

        $this->mailerManager->sendEmailForBlockedUser($user);
    }
}
