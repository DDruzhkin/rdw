<?php
namespace RDW\Bundle\UserBundle\EventListener;

use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Util\TokenGenerator;
use RDW\Bundle\MailerBundle\Service\MailerManager;
use Symfony\Component\Form\Exception\UnexpectedTypeException;

/**
 * Class OAuthEmailConfirmationListener
 *
 * @package RDW\Bundle\UserBundle\EventListener
 */
class OAuthEmailConfirmationListener
{
    /**
     * @var MailerManager
     */
    private $mailer;

    /**
     * @var TokenGenerator
     */
    private $tokenGenerator;

    /**
     * @param MailerManager  $mailer
     * @param TokenGenerator $tokenGenerator
     */
    public function __construct(MailerManager $mailer, TokenGenerator $tokenGenerator)
    {
        $this->mailer = $mailer;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * Event occurs on o auth registration success
     *
     * @param UserEvent $event
     *
     * @throws UnexpectedTypeException
     */
    public function onRegistrationSuccess(UserEvent $event)
    {
        $user = $event->getUser();

        if (null === $user->getConfirmationToken()) {
            $user->setConfirmationToken($this->tokenGenerator->generateToken());
        }

        $this->mailer->sendSocialRegistrationConfirmEmailMessage($user);
    }
}
