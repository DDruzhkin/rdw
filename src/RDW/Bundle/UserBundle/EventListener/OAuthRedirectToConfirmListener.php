<?php

namespace RDW\Bundle\UserBundle\EventListener;

use RDW\Bundle\UserBundle\Entity\AnonymousUser;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * Class AnonymousUserListener
 *
 * @package RDW\Bundle\UserBundle\EventListener
 */
class OAuthRedirectToConfirmListener
{
    private $securityContext;

    /**
     * @var Router
     */
    private $router;

    /**
     * @param SecurityContext $context
     * @param Router          $router
     */
    public function __construct(SecurityContext $context, \Symfony\Cmf\Component\Routing\ChainRouter $router)
    {
        $this->router = $router;
        $this->securityContext = $context;
    }

    /**
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        // only for master requests
        if (HttpKernelInterface::MASTER_REQUEST != $event->getRequestType()) {
            return null;
        }

        $request = $event->getRequest();
        $token = $this->securityContext->getToken();
        if ($token) {
            $user = $token->getUser();
            // id user with role incomplete
            if ($this->securityContext->isGranted("ROLE_INCOMPLETE")) {

                if ($request->cookies->get('_o_auth_confirm') != $user->getOAuthId()) {

                    $cookie = new Cookie('_o_auth_confirm', $user->getOAuthId(), new \DateTime('+1hour'), '/', null, false, false);

                    $response = new RedirectResponse($this->router->generate('rdw_user.profile.finish'), 302);
                    $response->headers->setCookie($cookie);

                    $response->send();
                }
            }
        }
    }

}
