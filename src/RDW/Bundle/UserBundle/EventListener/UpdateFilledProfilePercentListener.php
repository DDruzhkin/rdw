<?php

namespace RDW\Bundle\UserBundle\EventListener;

use RDW\Bundle\UserBundle\Event\SingleUserEvent;
use RDW\Bundle\UserBundle\RDWUserEvents;
use RDW\Bundle\UserBundle\Service\ProfileCounter;
use RDW\Bundle\UserBundle\Service\UserManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UpdateFilledProfilePercentListener
 *
 * @package RDW\Bundle\UserBundle\EventListener
 * @author Paulius Aleliūnas <paulius@eface.lt>
 */
class UpdateFilledProfilePercentListener implements EventSubscriberInterface
{
    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var ProfileCounter
     */
    private $profileCounter;

    /**
     * Default constructor
     *
     * @param UserManager $userManager
     * @param ProfileCounter $profileCounter
     */
    public function __construct(UserManager $userManager, ProfileCounter $profileCounter)
    {
        $this->userManager = $userManager;
        $this->profileCounter = $profileCounter;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            RDWUserEvents::UPDATE_USER_PROFILE_PERCENT => 'onUpdateUserProfilePercent',
        );
    }

    /**
     * @param SingleUserEvent $event
     */
    public function onUpdateUserProfilePercent(SingleUserEvent $event)
    {
        $user = $event->getUser();
        $user->setProfilePercent($this->profileCounter->getUserProfilePercent($user));

        $this->userManager->updateUser($user);
    }
}
