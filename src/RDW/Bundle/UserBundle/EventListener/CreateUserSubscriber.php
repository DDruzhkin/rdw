<?php

namespace RDW\Bundle\UserBundle\EventListener;

use FOS\UserBundle\Event\UserEvent;
use RDW\Bundle\MailerBundle\Service\MailerManager;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Event\SingleUserEvent;
use RDW\Bundle\UserBundle\Util\PasswordGenerator;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use RDW\Bundle\UserBundle\RDWUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class CreateUserSubscriber
 *
 * @package RDW\Bundle\UserBundle\EventListener
 */
class CreateUserSubscriber implements EventSubscriberInterface
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var EncoderFactory
     */
    private $encoderFactory;

    /**
     * @var PasswordGenerator
     */
    private $passwordGenerator;

    /**
     * @var MailerManager
     */
    private $mailerManager;

    /**
     * @param EventDispatcherInterface $eventDispatcher
     * @param EncoderFactory           $encoderFactory
     * @param PasswordGenerator        $passwordGenerator
     * @param MailerManager            $mailerManager
     */
    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        EncoderFactory $encoderFactory,
        PasswordGenerator $passwordGenerator,
        MailerManager $mailerManager
    )
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->encoderFactory = $encoderFactory;
        $this->passwordGenerator = $passwordGenerator;
        $this->mailerManager = $mailerManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            RDWUserEvents::PRE_CREATE => 'onPreCreate',
            RDWUserEvents::POST_CREATE => 'onPostCreate',
            RDWUserEvents::GENERATE_NEW_PASSWORD => 'onGenerateNewPassword',
        );
    }

    /**
     * @param SingleUserEvent $event
     */
    public function onPreCreate(SingleUserEvent $event)
    {
        $user = $event->getUser();

        $this->setNewPassword($user);
        $user->addRole($user->getMainRole());
    }

    /**
     * @param SingleUserEvent $event
     */
    public function onPostCreate(SingleUserEvent $event)
    {
        $user = $event->getUser();

        $this->mailerManager->sendEmailForAdminAboutCreatedNewUser($user);

        // if user is employer, send email for manager
        if ($user->isTypeEmployer() && $user->getManager()) {
            $this->mailerManager->sendEmailForManagerAboutNewUser($user);

            // also send email for user
            $this->mailerManager->sendEmailForUserAboutAssignedManager($user);
        }

        $this->eventDispatcher->dispatch(RDWUserEvents::UPDATE_USER_PROFILE_PERCENT, $event);
    }

    /**
     * @param UserEvent $event
     */
    public function onGenerateNewPassword(UserEvent $event)
    {
        $user = $event->getUser();

        // password
        $this->setNewPassword($user);

        // dispatch event to send new password email and email for administrator
        $this->eventDispatcher->dispatch(RDWUserEvents::GENERATE_NEW_PASSWORD_SUCCESS, $event);
    }

    /**
     * @param RegisteredUser $user
     *
     * @return RegisteredUser
     */
    protected function setNewPassword(RegisteredUser $user)
    {
        $encoder = $this->getSecurityEncoder($user);
        $user->setPlainPassword($this->passwordGenerator->generatePassword());
        $encodedPassword = $encoder->encodePassword($user->getPlainPassword(), $user->getSalt());
        $user->setPassword($encodedPassword);

        return $user;
    }

    /**
     * @param RegisteredUser $user
     *
     * @return PasswordEncoderInterface
     */
    protected function getSecurityEncoder(RegisteredUser $user)
    {
        return $this->encoderFactory->getEncoder($user);
    }
}
