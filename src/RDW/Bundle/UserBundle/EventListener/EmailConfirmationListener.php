<?php

namespace RDW\Bundle\UserBundle\EventListener;

use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use FOS\UserBundle\EventListener\EmailConfirmationListener as FOSEmailConfirmationListener;
use RDW\Bundle\MailerBundle\Service\MailerManager;
use RDW\Bundle\UserBundle\RDWUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class EmailConfirmationListener
 *
 * @package RDW\Bundle\UserBundle\EventListener
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class EmailConfirmationListener extends FOSEmailConfirmationListener
{
    /**
     * @var TokenGeneratorInterface
     */
    private $tokenGenerator;

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var MailerManager
     */
    private $mailerManager;

    /**
     * @param TokenGeneratorInterface $tokenGenerator
     * @param UrlGeneratorInterface   $router
     * @param SessionInterface        $session
     * @param MailerManager           $mailerManager
     */
    public function __construct(
        TokenGeneratorInterface $tokenGenerator,
        UrlGeneratorInterface $router,
        SessionInterface $session,
        MailerManager $mailerManager
    )
    {
        $this->tokenGenerator = $tokenGenerator;
        $this->router = $router;
        $this->session = $session;
        $this->mailerManager = $mailerManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            RDWUserEvents::REGISTRATION_SUCCESS => 'onRegistrationSuccess',
        );
    }

    /**
     * @param FormEvent $event
     */
    public function onRegistrationSuccess(FormEvent $event)
    {
        /** @var $user \FOS\UserBundle\Model\UserInterface */
        $user = $event->getForm()->getData();

        $user->setEnabled(false);
        if (null === $user->getConfirmationToken()) {
            $user->setConfirmationToken($this->tokenGenerator->generateToken());
        }

        $this->mailerManager->sendRegistrationConfirmEmail($user);

        $this->session->set('fos_user_send_confirmation_email/email', $user->getEmail());

        $url = $this->router->generate('fos_user_registration_check_email');
        $event->setResponse(new RedirectResponse($url));
    }
}
