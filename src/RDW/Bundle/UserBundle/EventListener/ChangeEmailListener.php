<?php

namespace RDW\Bundle\UserBundle\EventListener;

use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use RDW\Bundle\MailerBundle\Service\MailerManager;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\RDWUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ChangeEmailListener
 *
 * @package RDW\Bundle\UserBundle\EventListener
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class ChangeEmailListener implements EventSubscriberInterface
{
    /**
     * @var MailerManager
     */
    private $mailerManager;

    /**
     * @var \FOS\UserBundle\Util\TokenGeneratorInterface
     */
    private $tokenGenerator;

    /**
     * @param MailerManager           $mailerManager
     * @param TokenGeneratorInterface $tokenGenerator
     */
    public function __construct(MailerManager $mailerManager, TokenGeneratorInterface $tokenGenerator)
    {
        $this->mailerManager = $mailerManager;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            RDWUserEvents::CHANGE_EMAIL_SUCCESS => 'onChangeSuccess',
            RDWUserEvents::CONFIRM_CHANGED_EMAIL_SUCCESS => 'onConfirmSuccess',
        );
    }

    /**
     * @param FormEvent $event
     */
    public function onChangeSuccess(FormEvent $event)
    {
        /** @var $user \FOS\UserBundle\Model\UserInterface */
        $user = $event->getForm()->getData();

        if (null === $user->getConfirmationToken()) {
            $user->setConfirmationToken($this->tokenGenerator->generateToken());
        }

        $this->mailerManager->sendChangeEmailConfirmationEmail($user);
    }

    /**
     * @param UserEvent $event
     */
    public function onConfirmSuccess(UserEvent $event)
    {
        /** @var $user RegisteredUser */
        $user = $event->getUser();

        if($user->getNewEmail()) {
            $user->setEmail($user->getNewEmail());
            $user->setUsername($user->getNewEmail());
        }
        $user->setConfirmationToken(null);
        $user->setNewEmail(null);
    }
}
