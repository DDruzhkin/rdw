<?php

namespace RDW\Bundle\UserBundle\EventListener;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Event\SingleUserEvent;
use RDW\Bundle\UserBundle\RDWUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Security\Core\SecurityContextInterface;

/**
 * Class DeleteUserSubscriber
 * @package RDW\Bundle\UserBundle\EventListener
 */
class DeleteUserSubscriber implements EventSubscriberInterface
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            RDWUserEvents::PRE_REMOVE => 'onPreRemove',
        );
    }

    /**
     * @param SingleUserEvent $event
     */
    public function onPreRemove(SingleUserEvent $event)
    {
        $entity = $event->getUser();

        if ($entity instanceof RegisteredUser) {
            foreach ($entity->getAssignedUsers() as $user) {
                /** @var RegisteredUser $user */
                $user->setManager(null);
                $this->objectManager->persist($user);
                $this->objectManager->flush();
            }
        }
    }
}
