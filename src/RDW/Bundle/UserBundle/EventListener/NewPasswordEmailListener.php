<?php

namespace RDW\Bundle\UserBundle\EventListener;

use FOS\UserBundle\Event\UserEvent;
use RDW\Bundle\MailerBundle\Service\MailerManager;
use RDW\Bundle\UserBundle\Event\SingleUserEvent;
use RDW\Bundle\UserBundle\RDWUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class NewPasswordEmailListener
 *
 * @package RDW\Bundle\UserBundle\EventListener
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class NewPasswordEmailListener implements EventSubscriberInterface
{
    /**
     * @var MailerManager
     */
    private $mailerManager;


    /**
     * @param MailerManager $mailerManager
     */
    public function __construct(MailerManager $mailerManager)
    {
        $this->mailerManager = $mailerManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            RDWUserEvents::PRE_CREATE => 'onPreCreate',
            RDWUserEvents::GENERATE_NEW_PASSWORD_SUCCESS => 'onGenerateSuccess',
        );
    }

    /**
     * @param SingleUserEvent $event
     */
    public function onPreCreate(SingleUserEvent $event)
    {
        $user = $event->getUser();

        $this->mailerManager->sendNewPasswordEmail($user);
    }

    /**
     * @param UserEvent $event
     */
    public function onGenerateSuccess(UserEvent $event)
    {
        /** @var $user \FOS\UserBundle\Model\UserInterface */
        $user = $event->getUser();

        $this->mailerManager->sendNewPasswordEmail($user);
    }
}
