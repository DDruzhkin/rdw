<?php

namespace RDW\Bundle\UserBundle\Event;

use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class SingleUserEvent
 *
 * @package RDW\Bundle\UserBundle\Event
 */
class SingleUserEvent extends Event
{
    /**
     * @var RegisteredUser
     */
    private $user;

    /**
     * @param RegisteredUser $user
     */
    public function __construct(RegisteredUser $user)
    {
        $this->user = $user;
    }

    /**
     * @return RegisteredUser
     */
    public function getUser()
    {
        return $this->user;
    }
}
