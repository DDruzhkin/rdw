<?php

namespace RDW\Bundle\UserBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Form\Element\EventListener\UserStatusHandleSubscriber;
use RDW\Bundle\UserBundle\Form\EventListener\AddEmailFieldListener;
use RDW\Bundle\UserBundle\Form\EventListener\EmployerCompanyProfileSubscriber;
use RDW\Bundle\UserBundle\Form\EventListener\EmployerPersonProfileSubscriber;
use RDW\Bundle\AppBundle\Form\EventListener\BuildContactPersonPhotoUploadFormListener;

class UserAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $object = $this->getSubject();

        $formMapper
            ->add('cities', 'entity', array(
                'required' => true,
                'class' => 'RDW\Bundle\LocationBundle\Entity\City',
                'multiple' => true,
                'label' => 'Cities',
            ))
            ->add('phone', 'text', [
                'label' => 'Phone',
                'required' => true,
                'attr' => [
                    'placeholder' => '+375 29 100 000 00',
                ],
            ])
            ->add('youtubeVideo', 'text', [
                'label' => 'Video',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Video placeholder',
                    'help_text' => 'Link to youtube video',
                ],
            ])
            ->add('fbContact', 'text', [
                'label' => 'Facebook contact',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Facebook contact placeholder',
                ],
            ])
            ->add('vkContact', 'text', [
                'label' => 'VK contact',
                'required' => false,
                'attr' => [
                    'placeholder' => 'VK contact placeholder',
                ],
            ])
            ->add('okContact', 'text', [
                'label' => 'Odnoklassniki contact',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Odnoklassniki contact placeholder',
                ],
            ])
            ->add('gPlusContact', 'text', [
                'label' => 'Google plus contact',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Google pluse contact placeholder',
                ],
            ])
            ->add('twitterContact', 'text', [
                'label' => 'Twitter contact',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Twitter contact placeholder',
                ],
            ])
            ->add('linkedInContact', 'text', [
                'label' => 'LinkedId contact',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Linkedin contact placeholder',
                ],
            ])
            ->add('skypeContact', 'text', [
                'label' => 'Skype contact',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Skype contact placeholder',
                ],
            ])
        ;


        if($object->getType() == "employer") {

            $formMapper
                ->add('address', 'text', [
                    'label' => 'Address',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'Address placeholder'
                    ]
                ])
                ->add('fax', 'text', [
                    'label' => 'Fax',
                    'required' => false,
                    'attr' => [
                        'placeholder' => '+375 29 100 000 00'
                    ]
                ])
                ->add('scope', 'entity', array(
                    'class' => 'RDWUserBundle:Scope',
                    'empty_value' => 'Choose option',
                    'property' => 'title',
                    'required' => true,
                ))
                ->add('companyInfo', 'textarea', [
                    'label' => 'Information about company',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'Information about company placeholder',
                        'help_text' => 'Text from 50 to 500 symbols'
                    ]
                ])
                ->add('bankAccount', 'text', [
                    'label' => 'Bank account',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'Bank account placeholder'
                    ]
                ])
                ->add('bankName', 'text', [
                    'label' => 'Bank name account',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'Bank name placeholder'
                    ]
                ])
                ->add('bankCode', 'text', [
                    'label' => 'Bank code',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'Bank code placeholder',
                    ],
                ])
                ->add('bankAddress', 'text', [
                    'label' => 'Bank address',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'Bank address placeholder',
                    ],
                ])
                ->add('status', 'choice', [
                    'choices'   => RegisteredUser::getUserStatusesChoiceListForAdmin(),
                    'label' => 'User status',
                    'required' => true,
                ])
                ->add('category', 'choice', [
                    'choices'   => RegisteredUser::getUserCategoriesChoiceListForAdmin(),
                    'label' => 'Category',
                    'required' => false,
                ])
                ->add('adminComment', 'textarea', [
                    'label' => 'Comment',
                    'required' => false,
                    'attr' => ['placeholder' => 'Comment placeholder']
                ])
                ->add('managerComment', 'textarea', [
                    'label' => 'Manager note',
                    'required' => false,
                    'attr' => ['placeholder' => 'Manager note placeholder'],
                ])
                ->add('employerType', 'choice', [
                    'choices'   => RegisteredUser::getEmployerTypeChoiceList(),
                    'label' => 'Employer type',
                    'empty_value' => 'Choose type',
                    'required' => false,
                    'attr' => ['data-handler' => 'employerType'],
                ])
                ->add('isManager', 'checkbox', [
                    'label' => 'User is manager',
                    'required' => false,
                    'mapped' => false,
                    'attr' => ['data-handler' => 'isManager'],
                ])
                ->add('supportingNewClients', 'checkbox', [
                    'label' => 'Supporting new clients',
                    'required' => false,
                    'attr' => ['data-hide' => 'nonManager'],
                ])
                ->add('oldEmail', 'email', [
                    'label' => 'Old email',
                    'required' => false,
                    'attr' => ['placeholder' => 'Old email placeholder', 'readonly' => 'readonly']
                ])
                ->add('newEmail', 'email', [
                    'label' => 'New email',
                    'required' => false,
                    'disabled' => false,
                    'attr' => [
                        'placeholder' => 'New email placeholder',
                    ]
                ])
                ->add('urlAddress', 'url', [
                    'label' => 'Company url address',
                    'required' => false,
                    'attr' => ['placeholder' => 'Company url address placeholder'],
                ])
                ->add('contactPerson', 'text', [
                    'label' => 'Contact person name',
                    'required' => true,
                    'attr' => ['placeholder' => 'Contact person name placeholder'],
                ])
                ->add('contactPersonPatronymic', 'text', [
                    'label' => 'Patronymic',
                    'required' => false,
                    'attr' => ['placeholder' => 'Contact person patronymic placeholder'],
                ])
                ->add('contactPersonSurname', 'text', [
                    'label' => 'Surname',
                    'required' => false,
                    'attr' => ['placeholder' => 'Contact person surname placeholder'],
                ])
                ->add('contactPersonPhone', 'text', [
                    'label' => 'Phone',
                    'required' => false,
                    'attr' => ['placeholder' => '+375 29 100 000 00'],
                ])
                ->add('contactPersonEmail', 'text', [
                    'label' => 'Email',
                    'required' => false,
                    'attr' => ['placeholder' => 'Contact person email placeholder'],
                ])
                ->add('companyType', 'entity', [
                    'empty_value' => 'Choose company type',
                    'class' => 'RDWUserBundle:CompanyType',
                    'property' => 'title',
                    'label' => 'Company type',
                    'required' => true,
                    'attr' => ['data-hide' => ''],
                ])
                ->add('companyTitle', 'text', [
                    'label' => 'Company title',
                    'required' => true,
                    'attr' => [
                        'placeholder' => 'Company title placeholder',
                        'data-hide' => '',
                    ],
                ])
                ->add('companyTradeMark', 'text', [
                    'label' => 'Company trade mark',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'Please fill company trade mark',
                        'data-hide' => '',
                    ],
                ])
                ->add('unpCode', 'text', [
                    'label' => 'UNP code',
                    'required' => true,
                    'attr' => [
                        'placeholder' => 'UNP code placeholder',
                        'data-hide' => '',
                    ],
                ])
            ;
        } else {

            $formMapper
                ->add('name', 'text', [
                    'label' => 'Name',
                    'required' => true,
                    'attr' => ['placeholder' => 'Name placeholder']
                ])
                ->add('patronymic', 'text', [
                    'label' => 'Patronymic',
                    'required' => false,
                    'attr' => ['placeholder' => 'Patronymic placeholder']
                ])
                ->add('surname', 'text', [
                    'label' => 'Surname',
                    'required' => true,
                    'attr' => ['placeholder' => 'Surname placeholder']
                ])
                ->add('gender', 'choice', [
                    'label' => 'Gender',
                    'expanded' => true,
                    'choices'   => RegisteredUser::getGenderChoices(),
                ])
                ->add('familyStatus', 'entity', [
                    'label' => 'Family status',
                    'class' => 'RDWUserBundle:MaritalStatus',
                    'required' => false,
                    'property' => 'title',
                    'empty_value' => 'Choose option',
                ])
                ->add('children', 'integer', [
                    'label' => 'Children',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'Children placeholder',
                        'min' => 0
                    ]
                ])
                ->add('birthday', 'birthday', [
                    'label' => 'Birthday',
                    'years' => range(1925, date('Y')),
                ])
                ->add('email', 'email', [
                    'label' => 'Email',
                    'mapped' => false,
                    'data' => $object->getEmail(),
                    'read_only' => true,
                    'required' => false,
                ])
                ->add('status', 'choice', [
                    'choices'   => RegisteredUser::getUserStatusesChoiceListForAdmin(),
                    'label' => 'User status',
                    'required' => true,
                ])
                ->add('category', 'choice', [
                    'choices'   => RegisteredUser::getUserCategoriesChoiceListForAdmin(),
                    'label' => 'Category',
                    'required' => false,
                ])
                ->add('adminComment', 'textarea', [
                    'label' => 'Comment',
                    'required' => false,
                ])
            ;
        }

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('username');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('name')
            ->add("surname")
            ->add("email")
            ->add("status", 'choice', array(
                'choices' => array('inactive' => 'Не размещенa', 'active' => 'Размещена', 'blocked' => 'Заблокированные'),
            ));

        $listMapper->add('_action', 'actions', array('actions' => array(
            'edit'       => array(),
            'delete'     => array('label' => 'delete')
        )));
    }
}