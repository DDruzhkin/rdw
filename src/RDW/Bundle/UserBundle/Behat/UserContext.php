<?php

namespace RDW\Bundle\UserBundle\Behat;

use Behat\Gherkin\Node\TableNode;
use RDW\Bundle\AppBundle\Behat\DefaultContext;
use RDW\Bundle\UserBundle\Entity\Scope;
use RDW\Bundle\UserBundle\Entity\User;

/**
 * Class UserContext. Context related with users
 */
class UserContext extends DefaultContext
{
    const EMPLOYEE_PROFILE_FORM_NAME = 'rdw_employee_profile';
    const EMPLOYER_PROFILE_FORM_NAME = 'rdw_employer_profile';

    /**
     * @Given /^I am on the registration page$/
     */
    public function iAmOnTheRegistrationPage()
    {
        $this->getSession()->visit($this->generatePageUrl('rdw_user.registration.index'));
    }

    /**
     * @Given /^I am on the login page$/
     */
    public function iAmOnTheLoginPage()
    {
        $this->getSession()->visit($this->generatePageUrl('fos_user_security_login'));
    }

    /**
     * @Given /^I should be on login page$/
     */
    public function iShouldBeOnLoginPage()
    {
        $this->assertSession()->addressEquals($this->generateUrl('fos_user_security_login'));
    }

    /**
     * @param string $birthday
     *
     * @Given /^I fill in birthday with "([^"]*)"$/
     * @throws \InvalidArgumentException
     */
    public function iFillInBirthday($birthday)
    {
        $dates = explode('-', $birthday);

        if (count($dates) == 3) {
            $this->fillField(self::EMPLOYEE_PROFILE_FORM_NAME.'[birthday][month]', $dates[1]);
            $this->fillField(self::EMPLOYEE_PROFILE_FORM_NAME.'[birthday][day]', $dates[2]);
            $this->fillField(self::EMPLOYEE_PROFILE_FORM_NAME.'[birthday][year]', $dates[0]);
        } else {
            throw new \InvalidArgumentException(sprintf('Invalid date format: "%s"', $birthday));
        }
    }

    /**
     * @param int $symbolsLength
     *
     * @Given /^I fill in company info with "([^"]*)" symbols$/
     */
    public function iFillInFieldWithSymbols($symbolsLength)
    {
        $this->fillField(self::EMPLOYER_PROFILE_FORM_NAME.'[companyInfo]', $this->getRandomString($symbolsLength));
    }

    /**
     * @Given /^I should be on profile edit page$/
     */
    public function iShouldBeOnProfileEditPage()
    {
        $this->assertSession()->addressEquals($this->generateUrl('rdw_user.profile.edit'));
    }

    /**
     * @param TableNode $table
     *
     * @Given /^there are following users:$/
     */
    public function thereAreFollowingUsers(TableNode $table)
    {
        foreach ($table->getHash() as $data) {
            $this->thereIsUser(
                $data['email'],
                isset($data['password']) ? $data['password'] : $this->faker->word,
                isset($data['type']) ? $data['type'] : User::USER_TYPE_EMPLOYEE,
                isset($data['role']) ? $data['role'] : User::ROLE_EMPLOYEE,
                isset($data['enabled']) ? $data['enabled'] : true,
                isset($data['employerType']) ? $data['employerType'] : ''
            );
        }

        $this->getEntityManager()->flush();
    }

    /**
     * @Given /^there are following company types:$/
     */
    public function thereAreFollowingCompanyTypes(TableNode $table)
    {
        foreach ($table->getHash() as $row) {
            $companyType = new \RDW\Bundle\UserBundle\Entity\CompanyType;
            $companyType
                ->setTitle($row['title'])
                ->setPosition((int)$row['position'])
            ;

            $this->getEntityManager()->persist($companyType);
        }

        $this->getEntityManager()->flush();
    }

    /**
     * @param string $email
     * @param string $password
     * @param string $type
     * @param null   $role
     * @param string $enabled
     * @param string $employerType
     *
     * @return \FOS\UserBundle\Model\UserInterface|User
     */
    public function thereIsUser($email, $password, $type, $role = null, $enabled = 'yes', $employerType = '')
    {
        if (null === $user = $this->getUserManager()->findUserByEmail($email)) {

            $userManager = $this->getUserManager();
            /* @var $user User */
            $user = $userManager->createUser();
            $user->setEmail($email);
            $user->setEnabled($enabled === 'yes' ? true : false);
            $user->setUsername($email);
            $user->setPlainPassword($password);
            $user->setType($type);

            if ($employerType != '') {
                $user->setEmployerType($employerType);
            }

            if (null !== $role) {
                $user->addRole($role);
            }

            $userManager->updateUser($user);
        }

        return $user;
    }

    /**
     * @return \RDW\Bundle\UserBundle\Service\UserManager
     */
    protected function getUserManager()
    {
        return $this->getService('rdw_user.service.user_manager');
    }

    /**
     * @param TableNode $table
     *
     * @Given /^there are following scopes:$/
     */
    public function thereAreFollowingScopes(TableNode $table)
    {
        foreach ($table->getHash() as $data) {
            $this->thereIsScope($data['title']);
        }

        $this->getEntityManager()->flush();
    }

    /**
     * @param string $title
     * @param bool   $flush
     *
     * @return Scope
     */
    public function thereIsScope($title, $flush = true)
    {
        $scope = $this->getEntityManager()->getRepository('RDWUserBundle:Scope')->findOneBy(['title' => $title]);

        if (null === $scope) {
            $scope = new Scope();
            $scope->setTitle($title);

            $this->getEntityManager()->persist($scope);

            if ($flush) {
                $this->getEntityManager()->flush();
            }
        }

        return $scope;
    }

    /**
     * @param int $length
     *
     * @return string
     */
    protected function getRandomString($length = 8)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = '';

        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }
}
