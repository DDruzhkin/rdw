<?php

namespace RDW\Bundle\UserBundle\Repository;

use Doctrine\ORM\EntityRepository;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Model\WebUserFilter;

/**
 * Class UserRepository
 *
 * @package RDW\Bundle\UserBundle\Repository
 */
class UserRepository extends EntityRepository
{
    /**
     * @param WebUserFilter $filter
     *
     * @return \Doctrine\ORM\Query
     */
    public function getWebUserListQuery(WebUserFilter $filter)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('u')
            ->from('RDWUserBundle:RegisteredUser', 'u')
            ->orderBy('u.id', 'DESC');

        // User id
        if ($id = $filter->getId()) {
            $queryBuilder
                ->andWhere('u.id = :id')
                ->setParameter('id', $filter->getId());
        }

        // user type
        if (null != $filter->getType()) {
            $queryBuilder
                ->andWhere('u.type = :type')
                ->setParameter('type', $filter->getType());
        }

        if ($foreignId = $filter->getForeignId()) {
            $queryBuilder
                ->andWhere('u.foreignId = :foreignId')
                ->setParameter('foreignId', $filter->getForeignId());
        }

        // manager
        if ($filter->getManager() instanceof RegisteredUser) {
            $queryBuilder
                ->andWhere('u.manager = :manager')
                ->setParameter('manager', $filter->getManager());
        }

        if ($companyTitle = $filter->getCompanyTitle()) {
            $queryBuilder
                ->andWhere('u.companyTitle LIKE :companyTitle')
                ->setParameter('companyTitle', '%'.$companyTitle.'%');
        }

        if ($email = $filter->getEmail()) {
            $queryBuilder
                ->andWhere('u.email LIKE :email')
                ->setParameter('email', '%'.$email.'%');
        }

        if ($phone = $filter->getPhone()) {
            $queryBuilder
                ->andWhere('u.phone LIKE :phone')
                ->setParameter('phone', '%'.$phone.'%');
        }

        if ($title = $filter->getTitle()) {
            $queryBuilder
                ->andWhere('(u.companyTitle LIKE :title OR CONCAT(u.contactPerson, \' \', u.contactPersonSurname) LIKE :title OR CONCAT(u.name, \' \', u.surname) LIKE :title)')
                ->setParameter('title', '%'.$title.'%');
        }

        /*
        if ($companyTitle = $filter->getCompanyTitle()) {
            $queryBuilder
                ->andWhere('u.companyTitle LIKE :companyTitle')
                ->setParameter('companyTitle', '%'.$companyTitle.'%');
        } */

        $queryBuilder
            ->orderBy('u.updatedAt', 'desc');

        return $queryBuilder->getQuery();
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findManagerForSupportingNewUsers()
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('u')
            ->addSelect('COUNT(assignedUsers) as assignedUsersCount')
            ->from('RDWUserBundle:RegisteredUser', 'u')
            ->leftJoin('u.assignedUsers', 'assignedUsers')
            ->andWhere('u.enabled = 1')
            ->andWhere('u.type = :type')
            ->andWhere('u.supportingNewClients = :supportingNewClients')
            ->andWhere('u.roles LIKE :role')
            ->setParameter('type', RegisteredUser::USER_TYPE_EMPLOYER)
            ->setParameter('supportingNewClients', true)
            ->setParameter('role', '%'.RegisteredUser::ROLE_MANAGER.'%')
            ->addGroupBy('u.id')
            ->orderBy('assignedUsersCount', 'ASC')
            ->setMaxResults(1);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    /**
     * @param RegisteredUser $user
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findAllManagersQueryBuilder(RegisteredUser $user = null)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('u')
            ->from('RDWUserBundle:RegisteredUser', 'u')
            ->andWhere('u.enabled = 1')
            ->andWhere('u.type = :type')
            ->andWhere('u.roles LIKE :role')
            ->setParameter('type', RegisteredUser::USER_TYPE_EMPLOYER)
            ->setParameter('role', '%'.RegisteredUser::ROLE_MANAGER.'%')
            ->orderBy('u.name', 'ASC')
            ->orderBy('u.surname', 'ASC');

        if ($user) {
            $queryBuilder
                ->andWhere('u != :user')
                ->setParameter('user', $user);
        }

        return $queryBuilder;
    }

    /**
     * @param RegisteredUser $manager
     * @param int            $limit
     *
     * @return array|RegisteredUser[]
     */
    public function findHavingWaitingJobs(RegisteredUser $manager, $limit)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('u', 'COUNT(job) as waitingJobsCount')
            ->from('RDWUserBundle:RegisteredUser', 'u')
            ->leftJoin('u.jobs', 'job')
            ->leftJoin('u.manager', 'manager')
            ->andWhere('job.status = :statusWaiting')
            ->andWhere('manager = :manager')
            ->setParameter('statusWaiting', Job::STATUS_WAITING)
            ->setParameter('manager', $manager)
            ->groupBy('u.id')
            ->having('waitingJobsCount > 0')
            ->orderBy('waitingJobsCount', 'DESC')
            ->setMaxResults($limit);

        $results = [];

        foreach ($queryBuilder->getQuery()->getResult() as $item) {
            /** @var RegisteredUser $job */
            $user = $item[0];

            $user->setWaitingJobsCount($item['waitingJobsCount']);
            $results[] = $user;
        }

        return $results;
    }

    /**
     * @param RegisteredUser $manager
     * @param string         $period
     *
     * @return int
     */
    public function getNewestWaitingJobsCountByManager(RegisteredUser $manager, $period)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('COUNT(job)')
            ->from('RDWJobBundle:Job', 'job')
            ->join('job.user', 'user')
            ->join('user.manager', 'manager')
            ->andWhere('manager = :manager')
            ->andWhere('job.status = :statusWaiting')
            ->andWhere('job.updatedAt >= :updatedFrom')
            ->setParameter('manager', $manager)
            ->setParameter('statusWaiting', Job::STATUS_WAITING)
            ->setParameter('updatedFrom', new \DateTime($period));

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }


    public function getLastShowContacts(RegisteredUser $user, Job $job)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('s')
            ->from('RDWJobBundle:JobShowContacts', 's')
            ->join('s.user', 'user')
            ->join('s.job', 'j')
            ->andWhere('user = :user')
            ->setParameter('user', $user)
            ->andWhere('j = :job')
            ->setParameter('job', $job)
            ->orderBy('s.updatedAt', 'DESC')
            ->setMaxResults(1);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
