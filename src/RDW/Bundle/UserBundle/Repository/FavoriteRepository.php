<?php

namespace RDW\Bundle\UserBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\UserBundle\Entity\User;
use RDW\Bundle\UserBundle\Model\FavoriteFilter;

/**
 * Class FavoriteRepository
 *
 * @package RDW\Bundle\UserBundle\Repository
 */
class FavoriteRepository extends EntityRepository
{
    /**
     * @param User           $user
     * @param FavoriteFilter $filter
     *
     * @return QueryBuilder
     */
    public function getUserFavoriteJobsQuery(User $user, FavoriteFilter $filter = null)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('f', 'job')
            ->from('RDWUserBundle:JobFavorite', 'f')
            ->join('f.job', 'job')
            ->leftJoin('RDWJobBundle:Apply', 'apply', 'WITH', 'f = apply.job')
            ->leftJoin('RDWConversationBundle:Conversation', 'conversation', 'WITH', 'apply = conversation.apply')
            ->where('f.user = :user')
            ->andWhere('f.archivedAt is NULL')
            ->andWhere('job.status = :status')
            ->setParameter('user', $user)
            ->setParameter('status', Job::STATUS_ACTIVE)
            ->orderBy('f.id', 'DESC');

        if ($filter) {
            $queryBuilder = $this->applyFilters($queryBuilder, $filter);
        }

        return $queryBuilder;
    }

    /**
     * @param User $user
     * @param FavoriteFilter $filter
     *
     * @return \Doctrine\ORM\Query
     */
    public function getUserFavoriteJobs(User $user, FavoriteFilter $filter = null)
    {
        return $this->getUserFavoriteJobsQuery($user, $filter)->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @param FavoriteFilter $filter
     *
     * @return int
     */
    public function countUserFavoriteJobs(User $user, FavoriteFilter $filter = null)
    {
        $queryBuilder = $this->getUserFavoriteJobsQuery($user, $filter);

        $queryBuilder
            ->select('COUNT(DISTINCT f.id)');

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    /**
     * @param FavoriteFilter $filter
     *
     * @return \Doctrine\ORM\Query
     */
    public function getJobsListQuery(FavoriteFilter $filter)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder
            ->select('favorite', 'job', 'workingPlaceCity', 'workScheduleType')
            ->from('RDWUserBundle:JobFavorite', 'favorite')
            ->join('favorite.job', 'job')
            ->leftJoin('RDWJobBundle:Apply', 'apply', 'WITH', 'favorite = apply.job')
            ->leftJoin('RDWConversationBundle:Conversation', 'conversation', 'WITH', 'apply = conversation.apply')
            ->leftJoin('job.workingPlaceCity', 'workingPlaceCity')
            ->leftJoin('job.workScheduleType', 'workScheduleType')
            ->andWhere('job.status = :status')
            ->setParameter('status', Job::STATUS_ACTIVE)
            ->orderBy('favorite.id', 'DESC');

        $this->applyFilters($queryBuilder, $filter);

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param User $user
     *
     * @param FavoriteFilter $filter
     * @return QueryBuilder
     */
    public function getUserFavoriteCvsQuery(User $user, FavoriteFilter $filter = null)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $subQuerySamePosition =
            '(SELECT COUNT(j.id) FROM RDWJobBundle:Job j WHERE j.position = cv.position AND j.status = \'active\')';
        $queryBuilder
            ->select(['f', 'cv', 'conversation.updatedAt', 'workingPlaceCity'])
            ->addSelect($subQuerySamePosition.' as relatedJobsCount')
            ->from('RDWUserBundle:CvFavorite', 'f')
            ->join('f.cv', 'cv')
            ->leftJoin('RDWCvBundle:CvMessage', 'cv_message', 'WITH', 'f = cv_message.cv')
            ->leftJoin('RDWConversationBundle:Conversation', 'conversation', 'WITH', 'cv_message = conversation.cvMessage')
            ->leftJoin('cv.workingPlaceCity', 'workingPlaceCity')
            ->andWhere('f.archivedAt is NULL')
            ->andWhere('f.user = :user')
            ->andWhere('cv.status IN (:status)')
            ->setParameter('status', [Cv::STATUS_ACTIVE])
            ->setParameter('user', $user)
            ->orderBy('f.id', 'DESC');

        if ($filter) {
            $queryBuilder = $this->applyFilters($queryBuilder, $filter);
        }

        return $queryBuilder;
    }

    /**
     * @param User $user
     * @param FavoriteFilter $filter
     *
     * @return \Doctrine\ORM\Query
     */
    public function getUserFavoriteCvs(User $user, FavoriteFilter $filter = null)
    {
        return $this->getUserFavoriteCvsQuery($user, $filter)->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @param FavoriteFilter $filter
     *
     * @return int
     */
    public function countUserFavoriteCvs(User $user, FavoriteFilter $filter = null)
    {
        $queryBuilder = $this->getUserFavoriteCvsQuery($user, $filter);

        $queryBuilder
            ->select('COUNT(DISTINCT f.id)');

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    /**
     * @param FavoriteFilter $filter
     *
     * @return \Doctrine\ORM\Query
     */
    public function getCvsListQuery(FavoriteFilter $filter)
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $queryBuilder
            ->select(['favorite', 'cv', 'conversation.updatedAt'])
            ->from('RDWUserBundle:CvFavorite', 'favorite')
            ->join('favorite.cv', 'cv')
            ->leftJoin('RDWCvBundle:CvMessage', 'cv_message', 'WITH', 'favorite = cv_message.cv')
            ->leftJoin('RDWConversationBundle:Conversation', 'conversation', 'WITH', 'cv_message = conversation.cvMessage')
            ->orderBy('favorite.id', 'DESC');

        if ($filter) {
            $queryBuilder = $this->applyFilters($queryBuilder, $filter);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param QueryBuilder   $queryBuilder
     * @param FavoriteFilter $filter
     *
     * @return QueryBuilder
     */
    protected function applyFilters(QueryBuilder $queryBuilder, FavoriteFilter $filter)
    {
        $user = $user = $filter->getUser();

        if ($user instanceof User) {
            $queryBuilder
                ->andWhere('favorite.user = :user')
                ->setParameter('user', $user);
        }

        if ($filter->isArchivedVisible()) {
            $queryBuilder->andWhere('favorite.archivedAt is NOT NULL');
        } else {
            $queryBuilder->andWhere('favorite.archivedAt is NULL');
        }

        // for RegisteredUser cvs with status = limited also should be visible
        if (FavoriteFilter::TYPE_CV == $filter->getType()) {
            $queryBuilder
                ->andWhere('cv.status IN (:status)')
                ->setParameter('status', [Cv::STATUS_ACTIVE]);
        }

        return $queryBuilder;
    }
}
