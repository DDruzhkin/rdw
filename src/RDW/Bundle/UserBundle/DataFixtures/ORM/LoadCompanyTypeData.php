<?php

namespace RDW\Bundle\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\UserBundle\Entity\CompanyType;

/**
 * Loads scope data
 *
 * @author Vytautas Beliunas <vytautas@eface.lt>
 */
class LoadCompanyTypeData extends AbstractFixture
{
    const REFERENCE_STRING = 'rdw.company_type.';
    const COUNT = 3;

    /**
     * @var array
     */
    private $companyTypes = [
        1 => 'ODO',
        2 => 'OHO',
        3 => 'REA'
    ];

    /**
     * Loads fixture
     *
     * @param ObjectManager $manager Object manager
     *
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->companyTypes as $key => $title) {
            $companyType = new CompanyType();
            $companyType->setTitle($title);
            $manager->persist($companyType);
            $this->addReference(self::REFERENCE_STRING . $key, $companyType);
        }

        $manager->flush();
    }
}
