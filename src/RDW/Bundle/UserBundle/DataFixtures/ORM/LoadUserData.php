<?php

namespace RDW\Bundle\UserBundle\DataFixtures\ORM;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\LocationBundle\DataFixtures\ORM\LoadCityData;
use RDW\Bundle\LocationBundle\Entity\City;
use RDW\Bundle\UserBundle\Entity\CompanyType;
use RDW\Bundle\WalletBundle\Entity\Wallet;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Faker\Factory as FakerFactory;
use Faker\Generator;

/**
 * Class LoadUserData
 *
 * @package RDW\Bundle\UserBundle\DataFixtures\ORM
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class LoadUserData extends AbstractFixture implements DependentFixtureInterface, ContainerAwareInterface
{
    const NUMBER_OF_EMPLOYEES = 20;
    const NUMBER_OF_EMPLOYERS = 20;
    const REFERENCE_STRING = 'rdw.user.';

    use ContainerAwareTrait;

    /**
     * Faker.
     *
     * @var Generator
     */
    protected $faker;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->faker = FakerFactory::create();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    function getDependencies()
    {
        return [
            LoadCityData::class,
            LoadCompanyTypeData::class,
        ];
    }

    /**
     * Loads fixture
     *
     * @param \Doctrine\Common\Persistence\ObjectManager $manager Object manager
     *
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $numberOfEmployees = self::NUMBER_OF_EMPLOYEES;
        $numberOfEmployers = self::NUMBER_OF_EMPLOYERS;
        $numberOfUsers = $numberOfEmployees + $numberOfEmployers;

        $genders = ['m', 'f'];

        for ($i = 1; $i <= $numberOfUsers; $i++) {
            $rand = rand(0, 1);
            $gender = $genders[$rand];

            $user = new RegisteredUser();
            $user->setEnabled(true);
            $user->setGender($gender);
            $user->setEmail('test+' . $i . '@nebijokit.lt');
            $user->setEmailCanonical('test+' . $i . '@nebijokit.lt');
            $user->setUsername($this->faker->word . '_' . $i);
            $user->setUsernameCanonical($user->getUsername());
            $user->setCities($this->getRandomCities());
            $user->setIsUnlimited(false);

            if ($i <= $numberOfEmployees) {
                $user->setType(RegisteredUser::USER_TYPE_EMPLOYEE);
                $user->addRole(RegisteredUser::ROLE_EMPLOYEE);
            } else {
                $user->setType(RegisteredUser::USER_TYPE_EMPLOYER);
                $user->addRole(RegisteredUser::ROLE_EMPLOYER);

                /** @var $companyType CompanyType */
                $companyType = $this->getReference(
                    LoadCompanyTypeData::REFERENCE_STRING . rand(1, LoadCompanyTypeData::COUNT)
                );
                $user->setCompanyType($companyType);

                $user->setCompanyTitle($this->faker->word);
                $user->setCompanyTradeMark($this->faker->word);
                $user->setCompanyInfo($this->faker->text(300));
                $user->setEmployerType(RegisteredUser::EMPLOYER_TYPE_COMPANY);
                $user->setContactPerson($this->faker->firstName($gender));
                $user->setContactPersonSurname($this->faker->lastName());
                $user->setContactPersonPosition($this->faker->word);
                $user->setUnpCode($this->faker->randomNumber(9));
                $user->setAddress($this->faker->address);
                $user->setPhone($this->faker->phoneNumber);
            }

            $user->setScope(
                $this->getReference(LoadScopeData::REFERENCE_STRING . rand(1, LoadScopeData::COUNT))
            );

            $user->setName($this->faker->firstName);
            $user->setSurname($this->faker->lastName);

            $birthday = new \DateTime($this->faker->date($format = 'Y-m-d', $max = '-18year'));
            $user->setBirthday($birthday);
            $encoder = $this->getSecurityEncoder($user);

            $encodedPassword = $encoder->encodePassword('rdwrdw' . $i, $user->getSalt());
            $user->setPassword($encodedPassword);

            // set wallet
            $wallet = new Wallet();
            $wallet->setUser($user);
            $user->setWallet($wallet);

            $this->addReference(self::REFERENCE_STRING . $i, $user);
            $manager->persist($user);
        }

        $manager->flush();
    }

    /**
     * @return ArrayCollection
     */
    protected function getRandomCities()
    {
        $cities = new ArrayCollection();

        for ($i = 1; $i <= rand(1, LoadCityData::COUNT); $i++) {
            /** @var $city City */
            $cities->add($this->getReference(LoadCityData::REFERENCE_STRING . $i));
        }

        return $cities;
    }

    /**
     * @param RegisteredUser $user
     *
     * @return PasswordEncoderInterface
     */
    protected function getSecurityEncoder(RegisteredUser $user)
    {
        return $this->container->get('security.encoder_factory')->getEncoder($user);
    }
}
