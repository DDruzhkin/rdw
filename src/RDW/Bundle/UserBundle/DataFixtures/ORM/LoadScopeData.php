<?php

namespace RDW\Bundle\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\UserBundle\Entity\Scope;

/**
 * Loads scope data
 *
 * @author Vytautas Beliunas <vytautas@eface.lt>
 */
class LoadScopeData extends AbstractFixture
{
    const REFERENCE_STRING = 'rdw.scope.';
    const COUNT = 2;

    /**
     * @var array
     */
    private $scopes = [
        1 => 'Telecommunications',
        2 => 'IT'
    ];

    /**
     * Loads fixture
     *
     * @param ObjectManager $manager Object manager
     *
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->scopes as $key => $scopeTitle) {
            $scope = new Scope();
            $scope->setTitle($scopeTitle);
            $manager->persist($scope);
            $this->addReference(self::REFERENCE_STRING . $key, $scope);
        }

        $manager->flush();
    }
}
