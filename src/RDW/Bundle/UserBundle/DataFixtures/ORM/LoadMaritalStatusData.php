<?php

namespace RDW\Bundle\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\UserBundle\Entity\MaritalStatus;

/**
 * Loads marital statuses data
 *
 * @author Vytautas Beliunas <vytautas@eface.lt>
 */
class LoadMaritalStatusData extends AbstractFixture
{
    const REFERENCE_STRING = 'rdw.marital_status.';
    const COUNT = 4;

    /**
     * @var array
     */
    private $maritalStatuses = [
        1 => 'Single',
        2 => 'Married',
        3 => 'Divorced',
        4 => 'Widow'
    ];

    /**
     * Loads fixture
     *
     * @param \Doctrine\Common\Persistence\ObjectManager $manager Object manager
     *
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->maritalStatuses as $key => $title) {
            $maritalStatus = new MaritalStatus();
            $maritalStatus->setTitle($title);

            $this->addReference(self::REFERENCE_STRING . $key, $maritalStatus);

            $manager->persist($maritalStatus);
        }

        $manager->flush();
    }
}
