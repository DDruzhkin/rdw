(function( $ ){
    $.fn.submitLoginModalForm = function(options) {
        var settings = $.extend({
            contentHolder: "#modal_container"
        }, options);

        $(this).on('click', function(e) {
            e.preventDefault();

            $(document).loaderStart();

            $('.error_txt').remove();
            $('.error').removeClass('error');

            var $this = $(this);
            var mainForm = $(settings.contentHolder).find('#main-form');

            if (mainForm.length > 0) {
                submitForm(mainForm, showFormResponse);
            } else if ('undefined' !== typeof ($(this).data('form'))) {
                var formId = $this.data('form');
                var loginForm = $(settings.contentHolder).find('#' + formId);

                submitForm(loginForm, showLoginFormResponse);
            }

            $(document).loaderStop();

            function submitForm(form, response)
            {
                var options = {
                    success:   response,
                    type:      'post',
                    dataType:  'json',
                    resetForm: false
                };

                form.ajaxForm(options);
                form.submit();
            }

            // post-submit callback
            function showFormResponse(data)  {
                if (data.success && typeof data.updated != 'undefined' && data.updated) {
                    window.location.reload();
                } else if (data.success) {
                    var formId = $this.data('form');
                    var form = $(settings.contentHolder).find('#' + formId);
                    submitForm(form, showLoginFormResponse);
                } else {
                    $('#main-form-holder').html(data.content);
                    initHandler();
                }

                return false;
            }

            function showLoginFormResponse(data)  {
                if (data.success) {
                    if (typeof(data.popup) != 'undefined') {
                        $('#' + $this.data('content-holder')).modal('hide').html(data.popup).modal('show');
                    } else if ((typeof(data.redirect) != 'undefined') && (data.redirect.length > 0)) {
                        window.location = data.redirect;
                    } else {
                        window.location.reload();
                    }
                } else {
                    $('#' +$this.data('content-holder')).html(data.content);
                    initHandler();
                }

                return false;
            }

            function initHandler()
            {
                $("[data-handler='submitLoginModalForm']").submitLoginModalForm();
                $("[data-handler='form-collection']").formCollection();
                $('select:not(.search-form-select select)').selecter();
                $("[data-handler='modalTabs']").modalTabsHandler({ 'modal': '#modal_container' });

                if ($.fn.iCheck) {
                    $("input").iCheck({
                        checkboxClass: "icheckbox_minimal checkbox"
                    });
                }
            }
        });
    };
})(jQuery);

$(function() {
    $("[data-handler='submitLoginModalForm']").submitLoginModalForm();
    $("[data-handler='form-collection']").formCollection();
    $('select:not(.search-form-select select)').selecter();
});

