$(document).ready(function() {
    var form = $("[data-handler='createEmployerForm']");

    form.manageUserHandling({
        'personTypeVal' : form.attr('data-employer-type'),
        'contactPersonFieldLabelAsPerson' : form.attr('data-name-label'),
        'contactPersonSurnameFieldLabelAsPerson' : form.attr('data-surname-label'),
        'infoFieldLabelAsPerson' : form.attr('data-info-label'),
        'urlFieldLabelAsPerson' : form.attr('data-url-label'),
        'contactPersonFieldLabelDefault' : form.attr('data-contact-person-label'),
        'contactPersonSurnameFieldLabelDefault' : form.attr('data-contact-person-surname-label'),
        'infoFieldLabelDefault' : form.attr('data-info-label-default'),
        'urlFieldLabelDefault' : form.attr('data-url-label-default'),
        'noAvailableManagers' : form.attr('data-no-available-managers')
    });
});
