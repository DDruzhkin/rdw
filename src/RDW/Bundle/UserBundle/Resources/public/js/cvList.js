$(document).ready(function() {
    $("[data-handler='removeFavorite']").removeFavoriteHandler();
    $("[data-handler='archiveFavorite']").archiveFavoriteHandler();
    $("[data-handler='cvFavoriteList']").favoriteListHandler();
    $("[data-handler='multipleReply']").multipleReplyHandler();
    $("[data-handler='meetTime']").meetTimeHandler();
});
