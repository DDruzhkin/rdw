(function( $ ){
    $.fn.saveFavoriteJobHandler = function(options) {
        var settings = $.extend({}, options);

        $(this).on('click', function() {

            var form = $(this).parents('form');
            var $this = $(this);

            var options = {
                success:       showResponse,
                type:      'post',
                dataType:  'json',
                resetForm: false
            };

            form.ajaxForm(options);
            form.submit();

            // post-submit callback
            function showResponse(data)  {
                if (data.success) {
                    window.location.reload();
                } else {
                    $('#' +$this.attr('data-content-holder')).html(data.content);
                    $("[data-handler='editJobFavorite']").saveFavoriteJobHandler();
                }
                return false;
            }
        });

    };
})(jQuery);