/**
 * Registration form handling
 */
(function( $ ){
    $.fn.registrationHandling = function(options) {

        var settings = $.extend({
            agreeTermsHandler: 'agreeTerms',
            personTypeVal: 3,
            employerTypeHandler: 'employerType'
        }, options );

        var form = $(this);
        agreeWithTerms();
        changeEmployerType();

        function agreeWithTerms()
        {
            var $agreeLink = form.find('[data-agree-handler="' + settings.agreeTermsHandler + '"]');

            if ($agreeLink) {
                var $submitButton = form.find("input[type=submit]");
                var $checkbox = $agreeLink.prev().find('input[type="checkbox"]');

                if ($checkbox.prop('checked')) {
                    $submitButton.removeAttr('disabled');
                }

                // ifChecked, ifUnchecked is iCheck callbacks: http://fronteed.com/iCheck/
                $checkbox.on('ifChecked', function() {
                    $submitButton.removeAttr('disabled');
                });
                $checkbox.on('ifUnchecked', function() {
                    $submitButton.attr('disabled', 'disabled');
                });
            }
        }

        function changeEmployerType()
        {
            if (form.find('[data-handler="' + settings.employerTypeHandler + '"]')) {

                var employerTypeSelect = form.find('[data-handler="' + settings.employerTypeHandler + '"]');

                if (employerTypeSelect.val() == settings.personTypeVal) {
                    $('[data-hide]').each(function() {
                        $( this ).parent().hide();
                    });
                }

                employerTypeSelect.on('change', function() {

                    if (employerTypeSelect.val() == settings.personTypeVal) {
                        $('label[for=rdw_user_registration_contactPerson]').html(settings.contactPersonFieldLabelAsPerson);
                        $('label[for=rdw_create_employer_contactPerson]').html(settings.contactPersonFieldLabelAsPerson);
                        $('label[for=rdw_manage_employer_contactPerson]').html(settings.contactPersonFieldLabelAsPerson);
                        $('[data-hide]').each(function() {
                            $(this).closest('li').hide();
                        });
                    } else {
                        $('label[for=rdw_user_registration_contactPerson]').html(settings.contactPersonFieldLabelDefault);
                        $('label[for=rdw_manage_employer_contactPerson]').html(settings.contactPersonFieldLabelDefault);
                        $('[data-hide]').each(function() {
                            $(this).closest('li').show();
                        });
                    }
                })
            }
        }
    };
})(jQuery);
