$(document).ready(function() {
    $("[data-handler='editJobFavorite']").saveFavoriteJobHandler();

    if ($.fn.selecter) {
        $("select.el_84").selecter({
            mobile: true,
            customClass: 'el_84'
        });
        $("select.el_123").selecter({
            mobile: true,
            customClass: 'el_123'
        });
        $("select.el_133").selecter({
            mobile: true,
            customClass: 'el_133'
        });
    }
});