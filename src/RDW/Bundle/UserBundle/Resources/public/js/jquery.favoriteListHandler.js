(function($) {
    $.fn.favoriteListHandler = function(options) {
        $(this).on('click', function(event) {
            var $this = $(event.target),
                $listForm = $('#' + $this.data('form')),
                $submitButton = $listForm.find('button[type="submit"]'),
                checkedRowsCount = $listForm.find('input[type="checkbox"]:checked').length;

                if (checkedRowsCount) {
                    $submitButton.attr('disabled', false);
                } else {
                    $submitButton.attr('disabled', true);
                }
        });
    };
})(jQuery);
