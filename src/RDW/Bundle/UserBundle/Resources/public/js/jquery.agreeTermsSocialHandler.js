(function( $ ){
    $.fn.agreeTermsSocial = function(options) {

        var settings = $.extend({
            agreeTermsHolder: 'agree-terms-holder',
            socialErrorHolder: 'social_error'
        }, options );

        $(this).on('click', function(e) {
            e.preventDefault();

            var $agreeHolder = $('.' + settings.agreeTermsHolder);
            var $socialErroHolder = $('.' + settings.socialErrorHolder);
            var $checkbox = $agreeHolder.find('input[type="checkbox"]');

            $agreeHolder.find('.error_txt').hide();

            if ($checkbox.prop('checked')) {
                window.location = $(this).attr('href');
            } else {
                $socialErroHolder.show();
            }
        });
    };
})(jQuery);

$(function() {
    $("[data-handler='agreeTermsSocial']").agreeTermsSocial();
});
