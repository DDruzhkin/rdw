(function($) {
    $.fn.multipleReplyHandler = function(options) {

        var settings = $.extend({
            contentHolder: '#modal_container',
            form: 'form[data-multiple-messages]'
        }, options);

        $(this).on('click', function() {
            var $this = $(this),
                $form = $(settings.form);

            $form.attr('action', $this.data('action'));

            var options = {
                success:    showResponse,
                type:       'post',
                dataType:   'json',
                resetForm:  false
            };

            $form.ajaxForm(options);
            $form.submit();

            function showResponse(data)  {
                if (data.success) {
                    $(settings.contentHolder).html(data.content).modal();
                }
            }

            return false;
        });
    };
})(jQuery);
