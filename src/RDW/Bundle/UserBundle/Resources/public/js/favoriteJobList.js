$(document).ready(function() {
    $("[data-handler='removeFavorite']").removeFavoriteHandler();
    $("[data-handler='archiveFavorite']").archiveFavoriteHandler();
    $("[data-handler='jobFavoriteList']").favoriteListHandler();
});
