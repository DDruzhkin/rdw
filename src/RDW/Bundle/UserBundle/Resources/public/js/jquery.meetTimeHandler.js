(function($) {
    $.fn.meetTimeHandler = function(options) {

        $(this).on('meetTime', function(event) {

            var $el = $(this);
            if ($el.data('clicked')){
                // Previously clicked, stop actions
                event.preventDefault();
                event.stopPropagation();
            } else {
                // Mark to ignore next click
                $el.data('clicked', true);

                if ($(event.target).attr('data-url') !== undefined) {
                    var requestUrl = $(event.target).attr('data-url');
                    var $dateField  = $('#' + $(event.target).attr('data-field'));

                    $.ajax({
                        url: requestUrl,
                        method: 'POST',
                        dataType: 'json',
                        data: { 'meetTime' : $el.data('value') },
                        success: function(data) {
                            if(data.success) {
                                $dateField.find(".ui_date").text($el.data('value'));
                                $dateField.show();
                                $el.parent().hide();
                            }
                        },
                        complete: function () {
                            window.setTimeout(function(){
                                $el.removeData('clicked');
                            }, 1000)

                        }
                    });
                }
            }

            return false;
        });
    };
})(jQuery);


$(function() {
    $('.datepicker').each(function(e) {
        $(this).datepicker({
            onSelect : function(dateText, instance) {
                if( dateText !== instance.lastVal ) {
                    $(this).data('value', dateText);
                    $(this).trigger('meetTime');
                }
            }
        });
    });
});