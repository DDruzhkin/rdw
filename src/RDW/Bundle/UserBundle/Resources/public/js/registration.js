$(document).ready(function() {
    var form = $("[data-handler='registrationForm']");

    form.registrationHandling({
        'personTypeVal' : form.attr('data-employer-type'),
        'contactPersonFieldLabelAsPerson' : form.attr('data-name-label'),
        'contactPersonFieldLabelDefault' : form.attr('data-contact-person-label')
    });
});
