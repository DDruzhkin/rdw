/**
 * Registration form handling
 */
(function( $ ){
    $.fn.manageUserHandling = function(options) {

        var settings = $.extend({
            personTypeVal: 3,
            employerTypeHandler: 'employerType',
            isManagerHandler: 'isManager',
            hasManagerHandler: 'hasManagers'
        }, options );

        var form = $(this);

        changeEmployerType();
        changeIsManager();
        hideEmptyManagersList();

        function changeEmployerType()
        {
            if (form.find("[data-handler='"+settings.employerTypeHandler+"']")) {

                var employerTypeSelect = form.find("[data-handler='"+settings.employerTypeHandler+"']");

                // update on load
                updateLabels(employerTypeSelect.val());

                // on change
                employerTypeSelect.on('change', function() {
                    updateLabels(employerTypeSelect.val());
                })
            }
        }

        function updateLabels(employerTypeValue)
        {
            if (employerTypeValue == settings.personTypeVal) {
                $('label[for=rdw_manage_employer_contactPerson]').html(settings.contactPersonFieldLabelAsPerson);
                $('label[for=rdw_manage_employer_contactPersonSurname]').html(settings.contactPersonSurnameFieldLabelAsPerson);
                $('label[for=rdw_manage_employer_companyInfo]').html(settings.infoFieldLabelAsPerson);
                $('label[for=rdw_manage_employer_urlAddress]').html(settings.urlFieldLabelAsPerson);
                $("[data-hide]").each(function( index ) {
                    $( this ).parent().hide();
                });
            } else {
                $('label[for=rdw_manage_employer_contactPerson]').html(settings.contactPersonFieldLabelDefault);
                $('label[for=rdw_manage_employer_contactPersonSurname]').html(settings.contactPersonSurnameFieldLabelDefault);
                $('label[for=rdw_manage_employer_companyInfo]').html(settings.infoFieldLabelDefault);
                $('label[for=rdw_manage_employer_urlAddress]').html(settings.urlFieldLabelDefault);
                $("[data-hide]").each(function( index ) {
                    $( this ).parent().show();
                });
            }
        }

        function changeIsManager()
        {
            if (form.find("[data-handler='"+settings.isManagerHandler+"']")) {

                var isManagerCheckbox = form.find("[data-handler='"+settings.isManagerHandler+"']");

                hideIsManagerFields(isManagerCheckbox.is(':checked'));

                // on change
                isManagerCheckbox.on('change', function() {
                    hideIsManagerFields(this.checked);
                });
            }
        }

        function hideIsManagerFields(isChecked)
        {
            if(isChecked) {
                $("[data-hide=\"manager\"]").each(function( index ) {
                    $( this ).parents('.form-group').hide();
                });
                $("[data-hide=\"nonManager\"]").each(function( index ) {
                    $( this ).parents('.form-group').show();
                });
            } else {
                $("[data-hide=\"nonManager\"]").each(function( index ) {
                    $( this ).parents('.form-group').hide();
                });
                $("[data-hide=\"manager\"]").each(function( index ) {
                    $( this ).parents('.form-group').show();
                });
            }
        }

        function hideEmptyManagersList() {
            if (form.find("[data-handler='"+settings.hasManagerHandler+"']")) {

                var managersList = form.find("[data-handler='"+settings.hasManagerHandler+"']");

                if( managersList.children('option').length < 2 ) {
                    var h4 = $('<h4></h4>');

                    managersList.parent().append(h4.append(settings.noAvailableManagers));
                    managersList.hide();
                }
            }
        }
    };
})(jQuery);
