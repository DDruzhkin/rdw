(function( $ ){
    $.fn.archiveFavoriteHandler = function(options) {
        $(this).on('click', function(event) {
            var $this = $(this);
            $.ajax({
                url: $this.data('url'),
                dataType: 'json',
                success: function(data) {
                    if (data.success) {
                        $('#' + $this.data('hide-element')).hide('slow');
                    }
                }
            });
        });
    };
})(jQuery);
