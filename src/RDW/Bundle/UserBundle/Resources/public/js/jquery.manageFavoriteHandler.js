(function( $ ){
    $.fn.manageFavoriteHandler = function(options) {

        var settings = $.extend({
            contentHolder: "#modal_container"
        }, options);

        $(this).on('click', function(event) {
            var $el = $(this);
            if ($el.data('clicked')){
                // Previously clicked, stop actions
                event.preventDefault();
                event.stopPropagation();
            } else {

                var target = $(this);

                $el.data('clicked', true);

                $.ajax({
                    url: $(this).data('url'),
                    dataType: 'json',
                    success: function (data) {
                        if (data.success) {
                            // show registration popup?
                            if ((typeof(data.needRegister) != 'undefined') && data.needRegister == true && typeof(data.needRegisterUrl) != 'undefined') {
                                $(document).registrationPopupHandler({url: data.needRegisterUrl});
                            } else {
                                var titleOposite = target.data('title-oposite');

                                target.data('title-oposite', target.find('span').html());
                                target.find('span').html(titleOposite);
                                target.data('url', data.url);

                                if (target.hasClass('act')) {
                                    target.removeClass('act');
                                } else {
                                    target.addClass('act');
                                }

                                $("[data-handler='manageFavorite']")
                                    .unbind()
                                    .manageFavoriteHandler();

                                if(data.success_message) {
                                    $('.alert').hide();

                                    if (target.hasClass('act')) {
                                        $('#success-message').html(data.success_message).show();
                                    } else {
                                        $('#danger-message').html(data.success_message).show();
                                    }
                                }
                            }

                            if($('.favorite-number') != 'undefined' && data.favorite_number >= 0) {
                                $('.favorite-number').html(data.favorite_number);
                            }
                        }
                    },
                    complete: function () {
                        window.setTimeout(function(){
                            $el.removeData('clicked');
                        }, 1000)

                    }
                });
            }

            return false;
        });
    };
})(jQuery);

$(document).ready(function() {
    $("[data-handler='manageFavorite']").manageFavoriteHandler();
});
