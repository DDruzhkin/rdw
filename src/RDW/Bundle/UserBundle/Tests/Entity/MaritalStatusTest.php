<?php

namespace RDW\Bundle\UserBundle\Tests\Entity;

use RDW\Bundle\UserBundle\Entity\MaritalStatus;

/**
 * Class MaritalStatusTest
 *
 * @package RDW\Bundle\UserBundle\Tests\Entity
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class MaritalStatusTest extends \PHPUnit_Framework_TestCase
{
    const CLASS_NAME = 'RDW\Bundle\UserBundle\Entity\MaritalStatus';

    /**
     * @test
     */
    public function it_should_set_title_properly()
    {
        $title = 'Title test';

        $maritalStatus = new MaritalStatus();
        $result = $maritalStatus->setTitle($title);

        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals($title, $maritalStatus->getTitle());
    }
}