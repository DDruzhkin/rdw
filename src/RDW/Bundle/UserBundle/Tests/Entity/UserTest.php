<?php

namespace RDW\Bundle\UserBundle\Tests\Entity;

use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Validator\Validator;
use Symfony\Component\Validator\ValidatorBuilder;
use Symfony\Component\Validator\ValidatorBuilderInterface;

class UserTest extends WebTestCase
{
    private $wallet;

    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $em;

    /**
     * @var ValidatorBuilderInterface
     */
    private $validatorBuilder;

    /**
     * set up
     */
    const CLASS_NAME = 'RDW\Bundle\UserBundle\Entity\RegisteredUser';

    public function setUp()
    {
        $client = static::createClient();

        $this->em = $client->getContainer()->get('doctrine')->getManager();

        $this->validatorBuilder = $client->getContainer()->get('validator.builder');

        $this->wallet = $this->getMock('RDW\Bundle\WalletBundle\Entity\Wallet');
    }

    /**
     * @test
     * @expectedException \Doctrine\DBAL\DBALException
     * @expectedExceptionMessage SQLSTATE[23000]: Integrity constraint violation: 1062 Duplicate entry 'ab@nebijokit.lt' for key 'UNIQ_1483A5E992FC23A8'
     */
    public function it_should_not_allow_save_user_with_duplicate_email()
    {
        $email = 'ab@nebijokit.lt';

        /* no users with such email should exist */
        $users = $this->em->getRepository('RDWUserBundle:RegisteredUser')->findByEmail($email);

        $this->assertCount(0, $users);

        $this->addUser($email);

        $users = $this->em->getRepository('RDWUserBundle:RegisteredUser')->findByEmail($email);
        $this->assertCount(1, $users);

        $this->addUser($email);
    }

    /**
     * @test
     */
    public function it_should_set_wallet_properly()
    {
        $user = new RegisteredUser();

        $user->setWallet($this->wallet);
        $this->assertInstanceOf('RDW\Bundle\WalletBundle\Entity\Wallet', $user->getWallet());
    }

    /**
     * @test
     */
    public function it_should_return_valid_title_for_employer()
    {
        $title = 'NebijokIT';

        $user = new RegisteredUser();
        $user->setType(RegisteredUser::USER_TYPE_EMPLOYER);
        $user->setEmployerType(RegisteredUser::EMPLOYER_TYPE_COMPANY);
        $user->setCompanyTitle($title);

        $this->assertEquals($title, $user->getTitle());
    }

    /**
     * @test
     */
    public function it_should_return_valid_title_for_employer_person()
    {
        $title = 'Personal company';

        $user = new RegisteredUser();
        $user->setType(RegisteredUser::USER_TYPE_EMPLOYER);
        $user->setEmployerType(RegisteredUser::EMPLOYER_TYPE_PERSON);
        $user->setContactPerson($title);

        $this->assertEquals($title, $user->getTitle());
    }

    /**
     * @test
     */
    public function it_should_return_valid_title_for_employee()
    {
        $title = 'Name Surname';

        $user = new RegisteredUser();
        $user->setType(RegisteredUser::USER_TYPE_EMPLOYEE);
        $user->setName(explode(' ', $title)[0]);
        $user->setSurname(explode(' ', $title)[1]);

        $this->assertEquals($title, $user->getTitle());
    }

    /**
     * @test
     */
    public function it_should_return_email_as_title_for_employee_by_default()
    {
        $email = '1@nebijokit.lt';

        $user = new RegisteredUser();
        $user
            ->setEmail($email)
            ->setEmailCanonical($email)
            ->setUsername($email)
            ->setUsernameCanonical($email)
            ->setPassword('test')
            ->setType(RegisteredUser::USER_TYPE_EMPLOYEE)
            ->setIsUnlimited(false);

        $this->em->persist($user);
        $this->em->flush();

        $this->assertEquals($email, $user->getTitle());
    }

    /**
     * @test
     */
    public function its_should_set_profile_percent_properly()
    {
        $percent = 85;

        $user = new RegisteredUser();
        $result = $user->setProfilePercent($percent);

        $this->assertInstanceOf(self::CLASS_NAME, $result);
        $this->assertEquals($percent, $user->getProfilePercent());
    }


    /**
     * creates new User object & persists it to DB
     */
    private function addUser($email)
    {
        $user = new RegisteredUser();
        $user
            ->setEmail($email)
            ->setEmailCanonical($email)
            ->setUsername($email)
            ->setUsernameCanonical($email)
            ->setPassword('test')
            ->setType(RegisteredUser::USER_TYPE_EMPLOYER)
            ->setIsUnlimited(false);

        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }

    /**
     * @test
     */
    public function it_should_set_facebook_id_properly()
    {
        $user = new RegisteredUser();
        $user->setFacebookId('21245454');
        $this->assertEquals('21245454', $user->getFacebookId());
    }

    /**
     * @test
     */
    public function it_should_set_vkontakte_id_properly()
    {
        $user = new RegisteredUser();
        $user->setVkontakteId('21245454');
        $this->assertEquals('21245454', $user->getVkontakteId());
    }

    /**
     * @test
     */
    public function it_should_set_mailru_id_properly()
    {
        $user = new RegisteredUser();
        $user->setMailruId('21245454');
        $this->assertEquals('21245454', $user->getMailruId());
    }

    /**
     * @test
     */
    public function it_should_set_odnoklassniki_id_properly()
    {
        $user = new RegisteredUser();
        $user->setOdnoklassnikiId('21245454');
        $this->assertEquals('21245454', $user->getOdnoklassnikiId());
    }

    /**
     * @test
     */
    public function it_should_set_has_manager_from_to_null_if_manager_null()
    {
        $user = new RegisteredUser();
        $manager = new RegisteredUser();

        $date = new \DateTime();

        $user->setManager($manager);
        $user->setHasManagerFrom($date);

        $this->assertEquals($date, $user->getHasManagerFrom());

        $user->setManager(null);
        $this->assertNull($user->getHasManagerFrom());
    }

    /**
     * @test
     */
    public function it_should_generate_slug()
    {
        $data = [
            ['name' => 'Aurimas', 'surname' => 'Baubkus', 'slug' => 'aurimas-baubkus'],
            ['name' => 'Василий', 'surname' => null, 'slug' => 'vasiliy'],
            ['name' => 'Сергей', 'surname' => 'Николаевич', 'slug' => 'sergey-nikolaevich'],
            ['company_title' => 'НП ОДО Энергореммонтаж', 'slug' => 'np-odo-energoremmontazh']
        ];

        foreach ($data as $row) {
            $user = new RegisteredUser();

            if (isset($row['company_title'])) {
                $user->setCompanyTitle($row['company_title']);
            } else {
                $user
                    ->setName($row['name'])
                    ->setSurname($row['surname'])
                ;
            }

            $user->generateSlug();

            $this->assertSame($row['slug'], $user->getSlug());
        }
    }

    public function unpCodes()
    {
        return [
            [999, false],
            [999999999, true],
            [999999999999, false],
            [null, false],
        ];
    }

    /**
     * @test
     * @dataProvider unpCodes
     */
    public function it_should_not_allow_code_to_be_too_long($code, $valid)
    {
        $user = new RegisteredUser();
        $user->setUnpCode($code);

        $validator = $this->validatorBuilder->addYamlMappings(
            [__DIR__.'/../../Resources/config/validation.yml']
        )->getValidator();

        $errors = $validator->validate($user, null, ['employer_edit_company']);
        $this->assertEquals($valid, $errors->count() === 0);
    }
}
