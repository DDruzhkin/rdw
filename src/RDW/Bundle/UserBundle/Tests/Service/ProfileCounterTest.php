<?php
/**
 * Created by PhpStorm.
 * User: vb
 * Date: 12/1/14
 * Time: 9:01 AM
 */

namespace RDW\Bundle\UserBundle\Tests\Service;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProfileCounterTest extends WebTestCase {
    /**
     * system under test
     */
    private $sut;

    public function setUp()
    {
        $client = static::createClient();
        $container = $client->getContainer();

        $this->sut = $container->get('rdw_user.service.profile_counter');
    }

    /**
     * @test
     */
    public function it_should_return_user_profile_percent()
    {
        $userMock = $this->getMockedUser();

        $this->sut->getUserProfilePercent($userMock);

        $this->assertGreaterThanOrEqual(0, $userMock->getProfilePercent());
        $this->assertLessThanOrEqual(100, $userMock->getProfilePercent());
    }

    /**
     * creates User mock
     */
    private function getMockedUser($id = 1)
    {
        $userMock = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser', ['getId']);
        $userMock
            ->expects($this->any())
            ->method('getId')
            ->will($this->returnValue($id))
        ;

        return $userMock;
    }
}
