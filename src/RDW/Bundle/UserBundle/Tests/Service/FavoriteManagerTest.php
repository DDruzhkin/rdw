<?php

namespace RDW\Bundle\UserBundle\Tests\Service;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

use RDW\Bundle\UserBundle\Entity\JobFavorite,
    RDW\Bundle\UserBundle\Entity\CvFavorite;

class FavoriteManagerTest extends WebTestCase
{

    /**
     * system under test
     */
    private $sut;

    public function setUp()
    {
        $client = static::createClient();
        $container = $client->getContainer();

        $this->sut = $container->get('rdw_user.service.favorite_manager');
    }

    /**
     * @test
     */
    public function it_should_allow_user_archive_favorite()
    {
        $userMock = $this->getMockedUser();

        $jobFavorite = new JobFavorite();
        $jobFavorite->setUser($userMock);

        $cvFavorite = new CvFavorite();
        $cvFavorite->setUser($userMock);

        $this->assertTrue($this->sut->isAllowedToArchive($jobFavorite, $userMock));
        $this->assertTrue($this->sut->isAllowedToArchive($cvFavorite, $userMock));
    }

    /**
     * @test
     */
    public function it_should_not_allow_archive_different_user_favorite()
    {
        $userMock = $this->getMockedUser();
        $otherUserMock = $this->getMockedUser(2);

        $jobFavorite = new JobFavorite();
        $jobFavorite->setUser($userMock);

        $this->assertFalse($this->sut->isAllowedToArchive($jobFavorite, $otherUserMock));
    }

    /**
     * @test
     */
    public function it_should_archive_favorite()
    {
        $favorites = [new CvFavorite, new JobFavorite];

        foreach ($favorites as $favorite) {
            $this->assertNull($favorite->getArchivedAt());

            $this->sut->archive($favorite);

            $this->assertInstanceOf('DateTime', $favorite->getArchivedAt());
            $this->assertNotNull($favorite->getArchivedAt());
        }
    }

    /**
     * @param int $id
     *
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function getMockedUser($id = 1)
    {
        $userMock = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser', ['getId']);
        $userMock
            ->expects($this->any())
            ->method('getId')
            ->will($this->returnValue($id));

        return $userMock;
    }
}
