<?php

namespace RDW\Bundle\UserBundle\Tests\Repository;

use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class UserRepositoryTest
 *
 * @package RDW\Bundle\UserBundle\Tests\Repository
 */
class UserRepositoryTest extends WebTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $client = static::createClient();
        $this->container = $client->getContainer();
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->repository = $this->em->getRepository('RDWUserBundle:RegisteredUser');
    }

    /**
     * @test
     */
    public function it_should_return_newest_waiting_jobs_count_for_manager_properly()
    {
        $user = $this->getUserHavingManager();
        $period = '-24hours';

        // check initial count of waiting jobs for manager
        $totalBefore = $this->repository->getNewestWaitingJobsCountByManager($user->getManager(), $period);

        $this->addWaitingJobForUser($user);
        $totalAfter = $this->repository->getNewestWaitingJobsCountByManager($user->getManager(), $period);

        $this->assertEquals($totalBefore + 1, $totalAfter);
    }

    /**
     * @return RegisteredUser
     */
    private function getUserHavingManager()
    {
        $user = $this->repository->findOneBy(['type' => RegisteredUser::USER_TYPE_EMPLOYER], ['id' => 'desc']);
        $manager = $this->repository->findOneBy(['type' => RegisteredUser::USER_TYPE_EMPLOYER], ['id' => 'asc']);
        $manager->addRole(RegisteredUser::ROLE_MANAGER);
        $user->setManager($manager);
        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }

    /**
     * @param RegisteredUser $user
     */
    private function addWaitingJobForUser(RegisteredUser $user)
    {
        $job = $this->em
            ->getRepository('RDWJobBundle:Job')
            ->findOneBy(['status' => Job::STATUS_ACTIVE]);

        if ($job instanceof Job) {
            $job->setStatus(Job::STATUS_WAITING);
            $job->setUser($user);
            $this->em->persist($job);
            $this->em->flush();
        }
    }
}
