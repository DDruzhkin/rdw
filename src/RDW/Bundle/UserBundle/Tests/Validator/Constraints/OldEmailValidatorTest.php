<?php

namespace RDW\Bundle\UserBundle\Tests\Validator\Constraints;

use RDW\Bundle\UserBundle\Validator\Constraints\OldEmailValidator;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;

class OldEmailValidatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $user;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $token;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $securityContext;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $constraint;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $context;

    /**
     * @var OldEmailValidator
     */
    private $validator;

    /**
     * set up
     */
    public function setUp()
    {
        $this->user = $this
            ->getMockBuilder('RDW\Bundle\UserBundle\Entity\RegisteredUser')
            ->disableOriginalConstructor()
            ->getMock();

        $this->token = $this
            ->getMockBuilder('Symfony\Component\Security\Core\Authentication\Token\TokenInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $this->securityContext = $this
            ->getMockBuilder('Symfony\Component\Security\Core\SecurityContext')
            ->disableOriginalConstructor()
            ->getMock();

        $this->constraint = $this
            ->getMockBuilder('Symfony\Component\Validator\Constraint')
            ->disableOriginalConstructor()
            ->getMock();

        $this->context = $this
            ->getMockBuilder('Symfony\Component\Validator\Context\ExecutionContextInterface')
            ->disableOriginalConstructor()
            ->getMock();

        $this->validator = new OldEmailValidator($this->securityContext);
        $this->validator->initialize($this->context);
    }

    /**
     * @test
     * @expectedException \Symfony\Component\Security\Core\Exception\TokenNotFoundException
     */
    public function it_should_throw_exception_when_token_not_found()
    {
        $this->securityContext->expects($this->once())->method('getToken')->willReturn(null);
        $this->validator->validate('value', $this->constraint);
    }

    /**
     * @test
     * @expectedException \Symfony\Component\Security\Core\Exception\UsernameNotFoundException
     */
    public function it_should_throw_exception_when_user_not_found()
    {
        $this->token->expects($this->once())->method('getUser')->willReturn(null);
        $this->securityContext->expects($this->once())->method('getToken')->willReturn($this->token);

        $this->validator->validate('value', $this->constraint);
    }

    /**
     * @test
     * @dataProvider violationData
     */
    public function it_should_not_validate_if_user_is_manager(
        $isManager,
        $expectedAddViolationCount,
        $expectedGetEmailCount,
        $expectedEmail,
        $givenEmail,
        $message)
    {
        $this->user->expects($this->once())->method('hasRole')->with(RegisteredUser::ROLE_MANAGER)->willReturn($isManager);
        $this->user->expects($this->exactly($expectedGetEmailCount))->method('getEmail')->willReturn($expectedEmail);
        $this->token->expects($this->once())->method('getUser')->willReturn($this->user);
        $this->securityContext->expects($this->once())->method('getToken')->willReturn($this->token);
        $this->context->expects($this->exactly($expectedAddViolationCount))->method('addViolation')->with($message);

        $this->validator->validate($givenEmail, $this->constraint);
    }

    public function violationData()
    {
        return [
            [true, 0, 0, 'baby@rdw.by', 'baby@rdw.by', 'Wrong value for your current old email'],
            [false, 0, 1, 'baby@rdw.by', 'baby@rdw.by', 'Wrong value for your current old email'],
            [false, 1, 1, 'gogo@rdw.by', 'baby@rdw.by', 'Wrong value for your current old email'],
        ];
    }
}
