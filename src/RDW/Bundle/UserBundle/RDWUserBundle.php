<?php

namespace RDW\Bundle\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class RDWUserBundle extends Bundle
{
    /**
     * {@inheritDoc}
     */
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
