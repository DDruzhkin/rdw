<?php

namespace RDW\Bundle\UserBundle\Form\Element\EventListener;

use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class UserStatusHandleSubscriber
 *
 * @package RDW\Bundle\UserBundle\Form\Element\EventListener
 */
class UserStatusHandleSubscriber implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::POST_SUBMIT => 'postSubmit'
        );
    }

    /**
     * @param FormEvent $event
     */
    public function postSubmit(FormEvent $event)
    {
        $status = $event->getData();

        /** @var RegisteredUser $user */
        $user = $event->getForm()->getParent()->getData();

        if (RegisteredUser::STATUS_BLOCKED == $status) {
            $user->setLocked(true);
            $user->setEnabled(false);
        }

        if (RegisteredUser::STATUS_INACTIVE == $status) {
            $user->setEnabled(false);
        }

        if (RegisteredUser::STATUS_ACTIVE == $status) {
            $user->setEnabled(true);
            $user->setLocked(false);
        }
    }
}
