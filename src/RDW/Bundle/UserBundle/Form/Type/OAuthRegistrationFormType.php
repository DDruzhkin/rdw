<?php

namespace RDW\Bundle\UserBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * User registration form class
 *
 * Class RegistrationFormType
 *
 * @package Registration|Login
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class OAuthRegistrationFormType extends AbstractType
{
    /**
     * Form builder
     *
     * @param FormBuilderInterface $builder builder
     * @param array                $options options array
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('socialEmail', 'email', [
                'required' => false,
                'label' => 'Email',
            ]);
    }

    /**
     * Get form name
     *
     * @return string
     */
    public function getName()
    {
        return 'rdw_user_o_auth_registration';
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'validation_groups' => ['finish'],
            'data_class' => 'RDW\Bundle\UserBundle\Entity\User',
        ]);
    }
}
