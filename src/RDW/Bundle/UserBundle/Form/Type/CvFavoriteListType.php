<?php

namespace RDW\Bundle\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class CvFavoriteListType
 *
 * @package RDW\Bundle\UserBundle\Form\Type
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class CvFavoriteListType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('favorites', 'entity', [
                /** @Ignore */
                'label' => false,
                'required' => false,
                'multiple' => true,
                'class' => 'RDW\Bundle\UserBundle\Entity\CvFavorite',
                'property' => 'id',
            ])
            ->add('submit', 'submit', [
                'label' => 'Reply to selected CVs',
                'disabled' => true,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\UserBundle\Model\FavoriteList',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_user_cv_favorite_list';
    }
}
