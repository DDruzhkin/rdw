<?php

namespace RDW\Bundle\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Edit job favorite form type class
 */
class JobFavoriteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('comment', 'text', [
                'label' => 'Note',
                'required' => false,
                'attr' => ['placeholder' => 'Note placeholder']
            ])
            ->add('submit', 'button', [
                'label' => 'Save',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\UserBundle\Entity\JobFavorite',
            'validation_groups' => ['edit']
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_user_job_favorite';
    }
}
