<?php

namespace RDW\Bundle\UserBundle\Form\Type;

use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Form\EventListener\AddRegistrationFieldsSubscriber;
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Registration form type class
 */
class RegistrationFormType extends BaseType
{
    /**
     * {@inheritDoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email', [
                'label' => 'Email',
                'required' => true,
                'attr' => ['placeholder' => 'Email placeholder']
            ])
            ->add('plainPassword', 'repeated', [
                'type' => 'password',
                'required' => true,
                'first_options' => ['label' => 'Password', 'attr' => ['placeholder' => 'Password placeholder']],
                'second_options' => ['label' => 'Password again', 'attr' => ['placeholder' => 'Password again placeholder']],
                'invalid_message' => 'Password mismatch',
            ])
            ->add('sumCaptcha', 'sum_captcha', [
                /** @Ignore */
                'label' => false,
                'required' => true,
            ])
            ->add('agree', 'checkbox', [
                'required'  => true,
                'mapped'    => false,
            ]);

        $builder->addEventSubscriber(new AddRegistrationFieldsSubscriber());

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            $form = $event->getForm();

            if (! $form->get('agree')->getData() ) {
                $form->get('agree')->addError(new FormError('You must agree with site agreements', 'validators', []));
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\UserBundle\Entity\RegisteredUser',
            'validation_groups' =>
                function(FormInterface $form) {

                    $groups = ['registration'];

                    if ($form->getData()->isTypeEmployer()) {
                        if ($form->getData()->getEmployerType() != RegisteredUser::EMPLOYER_TYPE_PERSON) {
                            $groups[] = 'registration_employer';
                        } elseif ($form->getData()->getEmployerType() == RegisteredUser::EMPLOYER_TYPE_PERSON) {
                            $groups[] = 'registration_person';
                        }
                    }

                    if ($form->getData()->isTypeEmployee()) {
                        $groups[] = 'registration_employee';
                    }

                    return $groups;
                }
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'rdw_user_registration';
    }
}
