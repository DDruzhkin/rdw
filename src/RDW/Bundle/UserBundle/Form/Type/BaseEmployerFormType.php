<?php

namespace RDW\Bundle\UserBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class BaseEmployerFormType
 *
 * @package RDW\Bundle\UserBundle\Form\Type
 */
class BaseEmployerFormType extends BaseUserFormType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('address', 'text', [
                'label' => 'Address',
                'required' => true,
                'attr' => ['placeholder' => 'Address placeholder']
            ])
            ->add('fax', 'text', [
                'label' => 'Fax',
                'required' => false,
            ])
            ->add('scope', 'entity', array(
                'class' => 'RDWUserBundle:Scope',
                'empty_value' => 'Choose option',
                'property' => 'title',
                'required' => true,
            ))
            ->add('companyInfo', 'textarea', [
                'label' => 'Information about company',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Information about company placeholder',
                    'help_text' => 'Text from 50 to 500 symbols'
                ]
            ])
            ->add('bankAccount', 'text', [
                'label' => 'Bank account',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Bank account placeholder'
                ]
            ])
            ->add('bankName', 'text', [
                'label' => 'Bank name account',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Bank name placeholder'
                ]
            ])
            ->add('bankCode', 'text', [
                'label' => 'Bank code',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Bank code placeholder',
                ],
            ])
            ->add('bankAddress', 'text', [
                'label' => 'Bank address',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Bank address placeholder',
                ],
            ]);

    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_base_employer';
    }
}
