<?php

namespace RDW\Bundle\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class EmployerNoteFormType
 * @package RDW\Bundle\UserBundle\Form\Type
 * @author Paulius Aleliūnas <paulius@eface.lt>
 */
class EmployerNoteFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('managerComment', 'textarea', [
                    'label' => 'Employer note',
                    'required' => false,
                    'attr' => ['placeholder' => 'Employer note placeholder'],
                ])
            ->add('submit', 'submit', [
                    'label' => 'Save',
                    'validation_groups' => ['employer_note'],
                ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\UserBundle\Entity\RegisteredUser',
            'validation_groups' => ['employer_note'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_user_employer_note';
    }
}
