<?php

namespace RDW\Bundle\UserBundle\Form\Type;

use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Repository\UserRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class ManageEmployerFormType
 *
 * @package RDW\Bundle\UserBundle\Form\Type
 */
class ManageEmployerFormType extends EmployerFormType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $user = $builder->getForm()->getData();

        $builder
            ->add('isManager', 'checkbox', [
                'label' => 'User is manager',
                'required' => false,
                'mapped' => false,
                'attr' => ['data-handler' => 'isManager'],
            ])
            ->add('supportingNewClients', 'checkbox', [
                'label' => 'Supporting new clients',
                'required' => false,
                'attr' => ['data-hide' => 'nonManager'],
            ])
            ->add('employerType', 'choice', [
                'choices'   => RegisteredUser::getEmployerTypeChoiceList(),
                'label' => 'Employer type',
                'empty_value' => 'Choose type',
                'data' => $user->getEmployerType(),
                'required' => false,
                'attr' => ['data-handler' => 'employerType'],
            ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {

            /** @var RegisteredUser $user */
            $user = $event->getData();

            if ($user->isEnabled()) {
                $user->setStatus(RegisteredUser::STATUS_ACTIVE);
            } else {
                $user->setStatus(RegisteredUser::STATUS_INACTIVE);
            }

            /*
            if ($user->isLocked()) {
                $user->setStatus(RegisteredUser::STATUS_BLOCKED);
            }
            */
        });

        // @todo move to subscriber
        $builder->get('isManager')
            ->addEventListener(
                FormEvents::PRE_SET_DATA,
                function (FormEvent $event) use ($user) {
                    if ($user->isManager()) {
                        $event->setData(true);
                    } else {
                        $event->setData(false);
                    }
                }
            )
            ->addEventListener(
                FormEvents::POST_SUBMIT,
                function (FormEvent $event) use ($user) {
                    $isManager = $event->getForm()->getData();

                    if ($isManager) {
                        $user->addRole(RegisteredUser::ROLE_MANAGER);
                    } else {
                        $user->removeRole(RegisteredUser::ROLE_MANAGER);
                    }
                }
            );


        $builder
            ->add('manager', 'entity', [
                'class' => 'RDW\Bundle\UserBundle\Entity\RegisteredUser',
                'label' => 'Assigned manager',
                'empty_value' => 'Choose manager',
                'required' => false,
                'property' => 'title',
                'query_builder' => function(UserRepository $userRepository) use ($user) {

                    return $userRepository->findAllManagersQueryBuilder(
                        ( $user->getId() > 0 ) ? $user : null
                    );
                },
                'attr' => [
                    'data-hide' => 'manager',
                    'data-handler' => 'hasManagers'
                ],
            ]);

        $builder->get('manager')
            ->addEventListener(
                FormEvents::POST_SUBMIT,
                function (FormEvent $event) use ($user) {
                    $manager = $event->getForm()->getData();

                    if( $manager && ! $user->getManager()) {
                        $user->setHasManagerFrom(new \DateTime());
                    }
                }
            );
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\UserBundle\Entity\RegisteredUser',
            'validation_groups' =>
                function(FormInterface $form) {
                    if ($form->getData()->getId() > 0) {
                        $groups = ['admin_edit'];

                        if ($form->getData()->getEmployerType() != RegisteredUser::EMPLOYER_TYPE_PERSON) {
                            $groups[] = 'admin_edit_company';
                            $groups[] = 'admin_edit_employer';
                        }
                    } else {
                        $groups = ['admin_create'];

                        if ($form->getData()->getEmployerType() != RegisteredUser::EMPLOYER_TYPE_PERSON) {
                            $groups[] = 'admin_create_company';
                            $groups[] = 'admin_edit_employer';
                        }
                    }

                    return $groups;
                },
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_manage_employer';
    }
}
