<?php

namespace RDW\Bundle\UserBundle\Form\Type;

use RDW\Bundle\AppBundle\Form\EventListener\BuildContactPersonPhotoUploadFormListener;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Form\EventListener\ChangeEmailFormListener;
use RDW\Bundle\UserBundle\Form\EventListener\EmployerCompanyProfileSubscriber;
use RDW\Bundle\UserBundle\Form\EventListener\EmployerPersonProfileSubscriber;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class EmployerProfileFormType
 *
 * @package RDW\Bundle\UserBundle\Form\Type
 */
class EmployerProfileFormType extends BaseEmployerFormType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $user = $builder->getForm()->getData();

        if ($user->getEmployerType() == RegisteredUser::EMPLOYER_TYPE_PERSON) {
            $builder->addEventSubscriber(new EmployerPersonProfileSubscriber());
        } else {
            $builder->addEventSubscriber(new EmployerCompanyProfileSubscriber());
        }

        $builder->addEventSubscriber(new BuildContactPersonPhotoUploadFormListener());

        $builder
            ->add('oldPassword', 'password', [
                'label' => 'Old password',
                'required' => false,
                'attr' => ['placeholder' => 'Old password placeholder']
            ])
            ->add('plainPassword', 'repeated', [
                'type' => 'password',
                'required' => false,
                'first_options' => ['label' => 'Password', 'attr' => ['placeholder' => 'Password placeholder']],
                'second_options' => ['label' => 'Password again', 'attr' => ['placeholder' => 'Password again placeholder']],
                'invalid_message' => 'Password mismatch',
            ])
            ->add('employerType', 'choice', [
                'choices'   => RegisteredUser::getEmployerTypeChoiceList(),
                'label' => 'Employer type',
                'empty_value' => 'Choose type',
                'mapped' => false,
                'data' => $user->getEmployerType(),
                'required' => true,
                'attr' => ['readonly' => 'readonly'],
            ]);

        $builder->addEventSubscriber(new ChangeEmailFormListener());
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\UserBundle\Entity\RegisteredUser',
            'validation_groups' =>
                function(FormInterface $form) {

                    $groups = ['employer_edit'];

                    if ($form->getData()->getEmployerType()) {
                        if ($form->getData()->getEmployerType() != RegisteredUser::EMPLOYER_TYPE_PERSON) {
                            $groups[] = 'employer_edit_company';
                        }
                    }

                    if ($form->getData()->getOldPassword() != '') {
                        $groups[] = 'change_password';
                    }

                    if ($form->getData()->getNewEmail() != '') {
                        $groups[] = 'changeEmail';
                    }

                    return $groups;
                }
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'rdw_employer_profile';
    }
}
