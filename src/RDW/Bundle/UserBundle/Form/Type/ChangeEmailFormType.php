<?php

namespace RDW\Bundle\UserBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\AbstractType;

/**
 * Class ChangeEmailFormType
 *
 * @package RDW\Bundle\UserBundle\Form\Type
 */
class ChangeEmailFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('oldEmail', 'email', [
                'label' => 'Old email',
                'required' => true,
                'attr' => ['placeholder' => 'Old email placeholder']
            ])
            ->add('newEmail', 'email', [
                'label' => 'New email',
                'required' => true,
                'attr' => ['placeholder' => 'New email placeholder']
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\UserBundle\Entity\RegisteredUser',
            'validation_groups' => ['changeEmail']
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_user_change_email';
    }
}
