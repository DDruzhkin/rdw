<?php

namespace RDW\Bundle\UserBundle\Form\Type;

use RDW\Bundle\AppBundle\Form\EventListener\BuildPhotoUploadFormListener;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class BaseUserFormType
 *
 * @package RDW\Bundle\UserBundle\Form\Type
 */
class BaseUserFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventSubscriber(new BuildPhotoUploadFormListener());

        $builder
            ->add('cities', 'city_collection', [
                'label' => 'Cities',
                'required' => true,
            ])
            ->add('phone', 'text', [
                'label' => 'Phone',
                'required' => true,
                'attr' => [
                    'placeholder' => '+375 29 100 000 00',
                ],
            ])
            ->add('youtubeVideo', 'text', [
                'label' => 'Video',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Video placeholder',
                    'help_text' => 'Link to youtube video',
                ],
            ])
            ->add('fbContact', 'text', [
                'label' => 'Facebook contact',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Facebook contact placeholder',
                ],
            ])
            ->add('vkContact', 'text', [
                'label' => 'VK contact',
                'required' => false,
                'attr' => [
                    'placeholder' => 'VK contact placeholder',
                ],
            ])
            ->add('okContact', 'text', [
                'label' => 'Odnoklassniki contact',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Odnoklassniki contact placeholder',
                ],
            ])
            ->add('gPlusContact', 'text', [
                'label' => 'Google plus contact',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Google pluse contact placeholder',
                ],
            ])
            ->add('twitterContact', 'text', [
                'label' => 'Twitter contact',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Twitter contact placeholder',
                ],
            ])
            ->add('linkedInContact', 'text', [
                'label' => 'LinkedId contact',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Linkedin contact placeholder',
                ],
            ])
            ->add('skypeContact', 'text', [
                'label' => 'Skype contact',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Skype contact placeholder',
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_base_user';
    }
}
