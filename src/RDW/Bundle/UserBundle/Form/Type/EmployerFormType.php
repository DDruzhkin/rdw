<?php

namespace RDW\Bundle\UserBundle\Form\Type;

use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Form\Element\EventListener\UserStatusHandleSubscriber;
use RDW\Bundle\UserBundle\Form\EventListener\AddEmailFieldListener;
use RDW\Bundle\UserBundle\Form\EventListener\EmployerCompanyProfileSubscriber;
use RDW\Bundle\UserBundle\Form\EventListener\EmployerPersonProfileSubscriber;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use RDW\Bundle\AppBundle\Form\EventListener\BuildContactPersonPhotoUploadFormListener;

/**
 * Class EmployerFormType
 *
 * @package RDW\Bundle\UserBundle\Form\Type
 */
class EmployerFormType extends BaseEmployerFormType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->addEventSubscriber(new AddEmailFieldListener());
        $builder->addEventSubscriber(new EmployerPersonProfileSubscriber());
        $builder->addEventSubscriber(new EmployerCompanyProfileSubscriber());
        $builder->addEventSubscriber(new BuildContactPersonPhotoUploadFormListener());

        $builder
            ->add('status', 'choice', [
                'choices'   => RegisteredUser::getUserStatusesChoiceListForAdmin(),
                'label' => 'User status',
                'required' => true,
            ])
            ->add('category', 'choice', [
                'choices'   => RegisteredUser::getUserCategoriesChoiceListForAdmin(),
                'label' => 'Category',
                'required' => false,
            ])
            ->add('adminComment', 'textarea', [
                'label' => 'Comment',
                'required' => false,
                'attr' => ['placeholder' => 'Comment placeholder']
            ])
            ->add('managerComment', 'textarea', [
                    'label' => 'Manager note',
                    'required' => false,
                    'attr' => ['placeholder' => 'Manager note placeholder'],
                ])
            ->add('employerType', 'choice', [
                'choices'   => RegisteredUser::getEmployerTypeChoiceList(),
                'label' => 'Employer type',
                'empty_value' => 'Choose type',
                'required' => false,
                'attr' => ['data-handler' => 'employerType'],
            ]);

        $builder->get('status')->addEventSubscriber(new UserStatusHandleSubscriber());
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\UserBundle\Entity\RegisteredUser',
            'validation_groups' =>
                function(FormInterface $form) {
                    if ($form->getData()->getId() > 0) {
                        $groups = ['admin_edit'];

                        if ($form->getData()->getEmployerType() != RegisteredUser::EMPLOYER_TYPE_PERSON) {
                            $groups[] = 'admin_edit_company';
                            $groups[] = 'admin_edit_employer';
                        }

                        if ($form->getData()->getNewEmail() != '') {
                            $groups[] = 'changeEmail';
                        }
                    } else {
                        $groups = ['admin_create'];

                        if ($form->getData()->getEmployerType() != RegisteredUser::EMPLOYER_TYPE_PERSON) {
                            $groups[] = 'admin_create_company';
                            $groups[] = 'admin_edit_employer';
                        }
                    }

                    return $groups;
                },
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_employer';
    }
}
