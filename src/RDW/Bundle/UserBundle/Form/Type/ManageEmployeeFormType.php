<?php

namespace RDW\Bundle\UserBundle\Form\Type;

use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Form\Element\EventListener\UserStatusHandleSubscriber;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class ManageEmployeeFormType
 *
 * @package RDW\Bundle\UserBundle\Form\Type
 */
class ManageEmployeeFormType extends BaseEmployeeFormType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $user = $builder->getForm()->getData();

        if ($user->getId() > 0) {
            $builder
                ->add('email', 'email', [
                    'label' => 'Email',
                    'mapped' => false,
                    'data' => $user->getEmail(),
                    'read_only' => true,
                    'required' => false,
                ]);
        } else {
            $builder
                ->add('email', 'email', [
                    'label' => 'Email',
                    'required' => false,
                ]);
        }

        $builder
            ->add('status', 'choice', [
                'choices'   => RegisteredUser::getUserStatusesChoiceListForAdmin(),
                'label' => 'User status',
                'required' => true,
            ])
            ->add('category', 'choice', [
                'choices'   => RegisteredUser::getUserCategoriesChoiceListForAdmin(),
                'label' => 'Category',
                'required' => false,
            ])
            ->add('adminComment', 'textarea', [
                'label' => 'Comment',
                'required' => false,
            ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $user = $event->getData();

            if ($user->isEnabled()) {
                $user->setStatus(RegisteredUser::STATUS_ACTIVE);
            } else {
                $user->setStatus(RegisteredUser::STATUS_INACTIVE);
            }

            //if ($user->isLocked()) {
            //    $user->setStatus(RegisteredUser::STATUS_BLOCKED);
            //}
        });

        $builder->get('status')->addEventSubscriber(new UserStatusHandleSubscriber());
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\UserBundle\Entity\RegisteredUser',
            'validation_groups' =>
                function(FormInterface $form) {
                    if ($form->getData()->getId() > 0) {
                        $groups = ['admin_edit'];

                        if ($form->getData()->isTypeEmployee()) {
                            $groups[] = 'admin_edit_employee';
                        }
                    } else {
                        $groups = ['admin_create'];

                        if ($form->getData()->isTypeEmployee()) {
                            $groups[] = 'admin_edit_employee';
                        }
                    }

                    return $groups;
                },
            'cascade_validation' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_manage_employee';
    }
}
