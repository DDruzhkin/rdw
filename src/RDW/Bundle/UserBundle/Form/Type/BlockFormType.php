<?php

namespace RDW\Bundle\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class BlockFormType
 *
 * @package RDW\Bundle\UserBundle\Form\Type
 */
class BlockFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('blockReason', 'textarea', [
                'label' => 'Block reason',
                'required' => true,
                'attr' => ['placeholder' => 'Block reason placeholder']
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\UserBundle\Entity\RegisteredUser',
            'validation_groups' => ['block']
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_user_block';
    }
}
