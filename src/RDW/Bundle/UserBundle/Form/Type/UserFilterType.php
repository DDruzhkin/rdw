<?php

namespace RDW\Bundle\UserBundle\Form\Type;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class UserFilterType
 * @package RDW\Bundle\UserBundle\Form\Type
 */
class UserFilterType extends AbstractType
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('get');

        $builder
            ->add('id', 'number', [
                'label' => 'ID',
                'required' => false,
            ])
            ->add('title', 'text', [
                    'label' => 'Title',
                    'required' => false,
                ])
            ->add('email', 'text', [
                    'label' => 'E-mail',
                    'required' => false,
                ])
            ->add('phone', 'text', [
                    'label' => 'Phone',
                    'required' => false,
                ])
            ->add('foreignId', 'text', [
                    'label' => 'CRM ID',
                    'required' => false,
                ])
            ->add('filter', 'submit')
            ->add('clear', 'submit')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\UserBundle\Model\WebUserFilter',
            'csrf_protection'   => false,
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'user_filter';
    }
}
