<?php

namespace RDW\Bundle\UserBundle\Form\Type;

use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Form\EventListener\ChangeEmailFormListener;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class EmployeeProfileFormType
 *
 * @package RDW\Bundle\UserBundle\Form\Type
 */
class EmployeeProfileFormType extends BaseEmployeeFormType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('oldPassword', 'password', [
                'label' => 'Old password',
                'required' => false,
                'attr' => ['placeholder' => 'Old password placeholder']
            ])
            ->add('plainPassword', 'repeated', [
                'type' => 'password',
                'required' => false,
                'first_options' => ['label' => 'Password', 'attr' => ['placeholder' => 'Password placeholder']],
                'second_options' => ['label' => 'Password again', 'attr' => ['placeholder' => 'Password again placeholder']],
                'invalid_message' => 'Password mismatch',
            ]);

        $builder->addEventSubscriber(new ChangeEmailFormListener());
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\UserBundle\Entity\RegisteredUser',
            'validation_groups' =>
                function(FormInterface $form) {

                    $groups = ['employee_edit'];

                    if ($form->getData()->getOldPassword() != '') {
                        $groups[] = 'change_password';
                    }

                    if ($form->getData()->getNewEmail() != '') {
                        $groups[] = 'changeEmail';
                    }

                    return $groups;
                }
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_employee_profile';
    }
}
