<?php

namespace RDW\Bundle\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class ClientSearchType
 *
 * @package RDW\Bundle\UserBundle\Form\Type
 */
class ClientSearchType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('get');
        $builder
            ->add('companyTitle', 'text', [
                /** @Ignore */
                'label' => false,
                'required' => false,
                'attr' => ['placeholder' => 'Search clients...'],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_client_search';
    }
}
