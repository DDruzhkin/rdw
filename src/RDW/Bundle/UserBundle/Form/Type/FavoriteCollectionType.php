<?php

namespace RDW\Bundle\UserBundle\Form\Type;

use RDW\Bundle\UserBundle\Repository\FavoriteRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class FavoriteCollectionType
 * @package RDW\Bundle\UserBundle\Form\Type
 */
class FavoriteCollectionType extends AbstractType
{
    /**
     * @var array
     */
    private $items;

    /**
     * @var string
     */
    private $type;

    /**
     * @param array  $items
     * @param string $type
     */
    public function __construct(array $items, $type = 'job')
    {
        if (! in_array($type, ['job', 'cv'])) {
            throw new \InvalidArgumentException();
        }

        $this->items = $items;
        $this->type = $type;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', 'entity', [
            'required' => false,
            'class' => $this->getClass($this->type),
            'property' => 'id',
            'property_path' => '[id]',
            'multiple' => true,
            'expanded' => true,
            'query_builder' => function (FavoriteRepository $repository) {
                return $repository
                    ->createQueryBuilder('f')
                    ->where('f.id IN (:ids)')
                    ->setParameter('ids', $this->items);
            },
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
            'csrf_protection' => false,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'favorite_collection';
    }

    private function getClass($type)
    {
        return ($type != 'cv') ? 'RDWUserBundle:JobFavorite' : 'RDWUserBundle:CvFavorite';
    }
}
