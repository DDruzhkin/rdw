<?php

namespace RDW\Bundle\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Personalize form type class
 */
class PersonalizeFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('personalPageTitle', 'text', [
                'label' => 'Page title',
                'required' => true,
            ])
            ->add('personalPageShortTitle', 'text', [
                'label' => 'Short title',
                'required' => true,
            ])
            ->add('slug', 'rdw_slug')
            ->add('personalPageDescription', 'ckeditor', [
                'required'  => true,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RDW\Bundle\UserBundle\Entity\RegisteredUser',
            'validation_groups' => ['personalize'],
            'cascade_validation' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_user_personalize';
    }
}
