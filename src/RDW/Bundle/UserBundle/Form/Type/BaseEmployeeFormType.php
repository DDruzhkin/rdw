<?php

namespace RDW\Bundle\UserBundle\Form\Type;

use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\Form\FormBuilderInterface;
use RDW\Bundle\AppBundle\Form\EventListener\BuildAgeFormListener;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

/**
 * Class BaseEmployeeFormType
 *
 * @package RDW\Bundle\UserBundle\Form\Type
 */
class BaseEmployeeFormType extends BaseUserFormType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('name', 'text', [
                'label' => 'Name',
                'required' => true,
                'attr' => ['placeholder' => 'Name placeholder']
            ])
            ->add('patronymic', 'text', [
                'label' => 'Patronymic',
                'required' => false,
                'attr' => ['placeholder' => 'Patronymic placeholder']
            ])
            ->add('surname', 'text', [
                'label' => 'Surname',
                'required' => true,
                'attr' => ['placeholder' => 'Surname placeholder']
            ])
            ->add('gender', 'choice', [
                'label' => 'Gender',
                'expanded' => true,
                'choices'   => RegisteredUser::getGenderChoices(),
            ])
            ->add('familyStatus', 'entity', [
                'label' => 'Family status',
                'class' => 'RDWUserBundle:MaritalStatus',
                'required' => false,
                'property' => 'title',
                'empty_value' => 'Choose option',
            ])
            ->add('children', 'integer', [
                'label' => 'Children',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Children placeholder',
                    'min' => 0
                ]
            ])
            ->add('birthday', 'birthday', [
                'label' => 'Birthday',
                'years' => range(1925, date('Y')),
            ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var RegisteredUser $entity */
            $entity = $event->getData();
            $form = $event->getForm();

            if( is_null($entity->getBirthday())) {
                $entity->setBirthday(
                    new \DateTime(implode('-', [date('Y') - 40, '01', '01']))
                );
            }

        });

         $builder->addEventSubscriber(new BuildAgeFormListener());
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_base_employee';
    }
}
