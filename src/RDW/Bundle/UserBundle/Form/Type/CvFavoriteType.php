<?php

namespace RDW\Bundle\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class CvFavoriteType
 *
 * @package RDW\Bundle\UserBundle\Form\Type
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class CvFavoriteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('comment', 'text', [
                'label' => 'Note',
                'required' => false,
                'attr' => ['placeholder' => 'Note placeholder']
            ])
            ->add('submit', 'submit', [
                'label' => 'Save',
                'validation_groups' => ['edit'],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'RDW\Bundle\UserBundle\Entity\CvFavorite',
            'validation_groups' => ['edit']
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_user_cv_favorite';
    }
}
