<?php

namespace RDW\Bundle\UserBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class AddEmailFieldListener
 *
 * @package RDW\Bundle\UserBundle\Form\EventListener
 */
class AddEmailFieldListener implements EventSubscriberInterface
{
    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'onPreSetData',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function onPreSetData(FormEvent $event)
    {
        $user = $event->getData();
        $form = $event->getForm();

        if ($user->getId() > 0) {
            $form
                ->add('oldEmail', 'email', [
                    'label' => 'Old email',
                    'required' => false,
                    'attr' => ['placeholder' => 'Old email placeholder', 'readonly' => 'readonly']
                ])
                ->add('newEmail', 'email', [
                    'label' => 'New email',
                    'required' => false,
                    'disabled' => ($user->getNewEmail()) ? true : false,
                    'attr' => [
                        'placeholder' => 'New email placeholder',
                    ]
                ]);
        } else {
            $form->add('email', 'email', [
                'label' => 'Email',
                'required' => false,
                'attr' => ['placeholder' => 'Email placeholder']
            ]);
        }
    }
}
