<?php

namespace RDW\Bundle\UserBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Employer as company subscriber
 */
class EmployerPersonProfileSubscriber implements EventSubscriberInterface
{
    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function preSetData(FormEvent $event)
    {
        $form = $event->getForm();

        $form
            ->add('urlAddress', 'url', [
                'label' => 'Url address',
                'required' => false,
                'attr' => ['placeholder' => 'Url address placeholder']
            ])
            ->add('contactPerson', 'text', [
                'label' => 'Name',
                'required' => true,
                'attr' => ['placeholder' => 'Contact person name placeholder']
            ])
            ->add('contactPersonPatronymic', 'text', [
                'label' => 'Patronymic',
                'required' => false,
                'attr' => ['placeholder' => 'Contact person patronymic placeholder']
            ])
            ->add('contactPersonSurname', 'text', [
                'label' => 'Surname',
                'required' => false,
                'attr' => ['placeholder' => 'Contact person surname placeholder']
            ])
            ->add('contactPersonPhone', 'text', [
                'label' => 'Phone',
                'required' => false,
                'attr' => ['placeholder' => '+375 29 100 000 00']
            ])
            ->add('contactPersonEmail', 'text', [
                'label' => 'Email',
                'required' => false,
                'attr' => ['placeholder' => 'Contact person email placeholder']
            ])
        ;

    }
}
