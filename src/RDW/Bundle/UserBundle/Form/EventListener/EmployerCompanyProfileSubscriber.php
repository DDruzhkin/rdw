<?php

namespace RDW\Bundle\UserBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Employer as company subscriber
 */
class EmployerCompanyProfileSubscriber implements EventSubscriberInterface
{
    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function preSetData(FormEvent $event)
    {
        $form = $event->getForm();

        $form
            ->add('companyType', 'entity', [
                'empty_value' => 'Choose company type',
                'class' => 'RDWUserBundle:CompanyType',
                'property' => 'title',
                'label' => 'Company type',
                'required' => true,
                'attr' => ['data-hide' => ''],
            ])
            ->add('companyTitle', 'text', [
                'label' => 'Company title',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Company title placeholder',
                    'data-hide' => '',
                ],
            ])
            ->add('companyTradeMark', 'text', [
              'label' => 'Company trade mark',
              'required' => false,
              'attr' => [
                'placeholder' => 'Please fill company trade mark',
                'data-hide' => '',
              ],
            ])
            ->add('unpCode', 'text', [
                'label' => 'UNP code',
                'required' => true,
                'attr' => [
                    'placeholder' => 'UNP code placeholder',
                    'data-hide' => '',
                ],
            ])
            ->add('urlAddress', 'url', [
                'label' => 'Company url address',
                'required' => false,
                'attr' => ['placeholder' => 'Company url address placeholder'],
            ])
            ->add('contactPerson', 'text', [
                'label' => 'Contact person name',
                'required' => true,
                'attr' => ['placeholder' => 'Contact person name placeholder'],
            ])
            ->add('contactPersonPatronymic', 'text', [
                'label' => 'Patronymic',
                'required' => false,
                'attr' => ['placeholder' => 'Contact person patronymic placeholder'],
            ])
            ->add('contactPersonSurname', 'text', [
                'label' => 'Surname',
                'required' => false,
                'attr' => ['placeholder' => 'Contact person surname placeholder'],
            ])
            ->add('contactPersonPhone', 'text', [
                'label' => 'Phone',
                'required' => false,
                'attr' => ['placeholder' => '+375 29 100 000 00'],
            ])
            ->add('contactPersonEmail', 'text', [
                'label' => 'Email',
                'required' => false,
                'attr' => ['placeholder' => 'Contact person email placeholder'],
            ])
        ;
    }
}
