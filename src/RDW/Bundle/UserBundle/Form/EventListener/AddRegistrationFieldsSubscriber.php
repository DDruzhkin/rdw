<?php

namespace RDW\Bundle\UserBundle\Form\EventListener;

use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * This class adds additional fields for Registration form depends from selected registration type
 * Class AddRegistrationFieldsSubscriber
 */
class AddRegistrationFieldsSubscriber implements EventSubscriberInterface
{
    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function preSetData(FormEvent $event)
    {
        $user = $event->getData();
        $form = $event->getForm();

        // check User type and render additional fields for employer type
        /** @var RegisteredUser $user */
        if ($user->isTypeEmployer()) {
            $form->add('employerType', 'choice', [
                'choices'   => RegisteredUser::getEmployerTypeChoiceList(),
                'label' => 'Employer type',
                'empty_value' => 'Choose type',
                'required' => true,
                'attr' => ['data-handler' => 'employerType'],
            ]);

            $form->add('companyType', 'entity', [
                'empty_value' => 'Choose company type',
                'label' => 'Company type',
                'required' => true,
                'property' => 'title',
                'class' => 'RDWUserBundle:CompanyType',
                'attr' => ['data-hide' => ''],
            ]);

            $form->add('companyTitle', 'text', [
                'label' => 'Company title',
                'required' => true,
                'attr' => [
                    'data-hide' => '',
                    'placeholder' => 'Company title placeholder',
                ],
            ]);

            $form->add('companyTradeMark', 'text', [
              'label' => 'Company trade mark',
              'required' => false,
              'attr' => [
                'data-hide' => '',
                'placeholder' => 'Please fill company trade mark',
              ],
            ]);

            $form->add('unpCode', 'text', [
                'label' => 'UNP code',
                'required' => true,
                'attr' => [
                    'data-hide' => '',
                    'placeholder' => 'UNP code placeholder',
                ],
            ]);

            $form->add('contactPerson', 'text', [
                /** @Ignore */
                'label' => false,
                'required' => true,
                'attr' => [
                    'placeholder' => 'Contact person placeholder',
                ],
            ]);
        }

        if ($user->isTypeEmployee()) {
            $form->add('name', 'text', [
                    'label' => 'Name',
                    'required' => true,
                    'attr' => ['placeholder' => 'Registration name placeholder'],
                ]);
        }
    }
}

