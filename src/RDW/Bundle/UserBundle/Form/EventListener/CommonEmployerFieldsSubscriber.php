<?php

namespace RDW\Bundle\UserBundle\Form\EventListener;

use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * This class adds additional fields for Registration form depends from selected registration type
 * Class AddRegistrationFieldsSubscriber
 */
class CommonEmployerFieldsSubscriber implements EventSubscriberInterface
{
    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function preSetData(FormEvent $event)
    {
        $user = $event->getData();

        $form = $event->getForm();

        // check User type and render additional fields for employer type
        if ($user->getType() == RegisteredUser::USER_TYPE_EMPLOYER) {

            $form
                ->add('cities', 'bootstrap_collection', [
                    'type' => 'entity',
                    'options' => [
                        'class' => 'RDWLocationBundle:City',
                        'property' => 'title',
                        /** @Ignore */
                        'label' => false,
                        'required' => false,
                        'empty_value' => 'Choose city',
                    ],
                    'label' => 'City',
                    'allow_add' => true,
                    'allow_delete' => true,
                    'required' => false
                ])
                ->add('address', 'text', [
                    'label' => 'Address',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'Address placeholder'
                    ]
                ])
                ->add('phone', 'text', [
                    'label' => 'Phone',
                    'required' => false,
                    'attr' => [
                        'placeholder' => '+375 29 100 000 00'
                    ]
                ])
                ->add('fax', 'text', [
                    'label' => 'Fax',
                    'required' => false,
                    'attr' => [
                        'placeholder' => '+375 29 100 000 00'
                    ]
                ])
                ->add('youtubeVideo', 'text', [
                    'label' => 'Video',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'Youtube video placeholder'
                    ]
                ])
                ->add('fbContact', 'text', [
                    'label' => 'Facebook contact',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'Facebook contact placeholder'
                    ]
                ])
                ->add('vkContact', 'text', [
                    'label' => 'VK contact',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'VK contact placeholder'
                    ]
                ])
                ->add('okContact', 'text', [
                    'label' => 'Odnoklassniki contact',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'Odnoklassniki contact placeholder'
                    ]
                ])
                ->add('gPlusContact', 'text', [
                    'label' => 'Google plus contact',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'Google pluse contact placeholder'
                    ]
                ])
                ->add('twitterContact', 'text', [
                    'label' => 'Twitter contact',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'Twitter contact placeholder'
                    ]
                ])
                ->add('linkedInContact', 'text', [
                    'label' => 'LinkedId contact',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'LinkedIn contact placeholder'
                    ]
                ])
                ->add('skypeContact', 'text', [
                    'label' => 'Skype contact',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'Skype contact placeholder'
                    ]
                ])
                ->add('scope', 'entity', array(
                    'class' => 'RDWUserBundle:Scope',
                    'empty_value' => 'Choose option',
                    'property' => 'title',
                    'required' => false,
                ))
                ->add('companyInfo', 'textarea', [
                    'label' => 'Information about company',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'Information about company placeholder',
                        'help_text' => 'Text from 200 to 500 symbols'
                    ]
                ]);
        }
    }
}
