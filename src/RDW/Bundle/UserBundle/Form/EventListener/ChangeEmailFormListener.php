<?php

namespace RDW\Bundle\UserBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ChangeEmailFormListener
 * @package RDW\Bundle\UserBundle\Form\EventListener
 */
class ChangeEmailFormListener implements EventSubscriberInterface
{
    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function preSetData(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();

        $form
            ->add('oldEmail', 'email', [
                'label' => 'Old email',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Old email placeholder',
                    'readonly' => 'readonly',
                ],
            ])
            ->add('newEmail', 'email', [
                'label' => 'New email',
                'required' => false,
                'disabled' => ($data->getNewEmail()) ? true : false,
                'attr' => [
                    'placeholder' => 'New email placeholder',
                ],
            ]);
    }
}
