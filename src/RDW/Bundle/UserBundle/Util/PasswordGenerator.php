<?php

namespace RDW\Bundle\UserBundle\Util;

/**
 * Responsible form password generator
 *
 * Class PasswordGenerator
 *
 * @author Vytautas Beliunas <vytautas@eface.lt>
 */
class PasswordGenerator extends StringGenerator
{
    /**
     * @param int $length
     *
     * @return string
     */
    public function generatePassword($length = 8)
    {
        $plainPassword = $this->generateRandomString($length);

        return $plainPassword;
    }
}
