<?php

namespace RDW\Bundle\UserBundle\Util;

/**
 * Abstract String generator class
 *
 * @author Vytautas Beliunas <vytautas@eface.lt>
 */
class StringGenerator
{
    /**
     * @param int     $length
     * @param boolean $onlyFromDigits
     *
     * @return string
     */
    public function generateRandomString($length = 8, $onlyFromDigits = false)
    {
        if ($onlyFromDigits) {
            $characters = '0123456789';
        } else {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }

        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $randomString;
    }
}
