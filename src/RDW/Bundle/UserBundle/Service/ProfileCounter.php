<?php

namespace RDW\Bundle\UserBundle\Service;

use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\PropertyAccess\PropertyAccess;
use RDW\Bundle\UserBundle\Entity\User;

/**
 *  User service object
 */
class ProfileCounter {

    /**
     * @var \Symfony\Component\PropertyAccess\PropertyAccess
     */
    protected $accessor;

    /**
     * Default constructor
     */
    public function __construct()
    {
        $this->accessor = PropertyAccess::createPropertyAccessor();
    }

    /**
     * Get percent of filled user profile
     *
     * @param RegisteredUser $user
     * @return int
     */
    public function getUserProfilePercent(RegisteredUser $user)
    {
        $fields = ( $user->isTypeEmployee() ) ? $this->getEmployeeFields() : $this->getEmployerFields();
        $missingFields = 0;

        foreach($fields as $field) {
            if( ! $this->accessor->getValue($user, $field) ) {
                $missingFields++;
            }
        }

        if( ! $user->isEmployeeHasActiveCv() && ! $user->isEmployerHasActiveJob() )
        {
            $missingFields++;
        }

        return 100 - round( $missingFields/(count($fields)+1) * 100, 0);

    }

    /**
     * Get same fields for employee and employer
     *
     * @return array
     */
    private function getMainFields()
    {
        return [
            'cities', 'phone', 'photo', 'youtubeVideo', 'fbContact', 'vkContact', 'okContact',
            'gPlusContact', 'twitterContact', 'linkedInContact', 'skypeContact'
        ];
    }

    /**
     * Get employer fields
     *
     * @return array
     */
    private function getEmployerFields()
    {
        return array_merge(
            $this->getMainFields(),
            [
                'employerType', 'companyType', 'companyTitle', 'unpCode', 'address',
                'fax', 'urlAddress', 'companyInfo', 'contactPerson', 'contactPersonSurname',
                'contactPersonPosition', 'scope'
            ]
        );
    }

    /**
     * Get employee fields
     *
     * @return array
     */
    private function getEmployeeFields()
    {
        return array_merge(
            $this->getMainFields(),
            [
                'name', 'surname', 'gender', 'familyStatus', 'children', 'birthday'
            ]
        );
    }
}
