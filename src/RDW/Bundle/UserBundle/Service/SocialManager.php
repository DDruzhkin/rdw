<?php

namespace RDW\Bundle\UserBundle\Service;

use FOS\UserBundle\Model\UserInterface;
use HWI\Bundle\OAuthBundle\Security\Core\Exception\AccountNotLinkedException;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\User;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use RDW\Bundle\UserBundle\Util\PasswordGenerator;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 *  User service object
 */
class SocialManager implements ContainerAwareInterface
{
    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var PasswordGenerator
     */
    protected $passwordGenerator;

    /**
     * @var Session
     */
    protected $session;

    /**
     * Container
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param UserManager $userManager
     * @param PasswordGenerator $passwordGenerator
     * @param Session $session
     */
    public function __construct(
        UserManager $userManager,
        PasswordGenerator $passwordGenerator,
        Session $session
    ) {
        $this->userManager = $userManager;
        $this->passwordGenerator = $passwordGenerator;
        $this->session = $session;
    }

    /**
     * @param string $email
     *
     * @return User
     */
    public function findUserByEmail($email)
    {
        return $this->userManager->findUserByEmail($email);
    }

    /**
     * @param UserResponseInterface $response
     *
     * @return UserInterface|User
     */
    public function createNewUserFromOAuthResponse(UserResponseInterface $response)
    {
        // if email in response when full user
        if (!$response->getEmail()) {
            throw new AccountNotLinkedException(sprintf("Unable create Account because it does not have an email."));
        }

        $user = new RegisteredUser();
        $user->setEmail($response->getEmail());
        $user->setUsername($response->getEmail());
        $user->setEnabled(true);

        if ($this->session->get('_user_type')) {
            $user->setType($this->session->get('_user_type'));
        } else {
            $user->setType(RegisteredUser::USER_TYPE_EMPLOYEE);
        }

        $user = $this->setUserSocialAccount($response->getResourceOwner()->getName(), $response->getUsername(), $user);
        $user->setPlainPassword($this->passwordGenerator->generatePassword());

        $this->userManager->registerNewUser($user);

        return $user;
    }

    /**
     * @param AnonymousUser $user
     *
     * @return null|RegisteredUser
     */
    public function finishSocialRegistration(AnonymousUser $user)
    {
        // find user by email
        $oldUser = $this->userManager->findUserByEmail($user->getSocialEmail());

        if ($oldUser instanceof RegisteredUser) {
            $oldUser->setEnabled(true);
            $oldUser->setConfirmationToken(null);
            $realUser = $oldUser;

            // map social accounts
            $oldUser->mapSocialAccounts($user);

            $this->userManager->updateUser($oldUser);
        } else {
            // persist new user and login again with new role
            $newUser = new RegisteredUser();
            $newUser->setEmail($user->getSocialEmail());
            $newUser->setUsername($user->getSocialEmail());
            $newUser->setEnabled(true);
            $newUser->setConfirmationToken(null);
            $newUser->setPlainPassword(sha1('rdw' . sha1(microtime())));
            $newUser->setType($user->getType());

            // map social accounts
            $newUser->mapSocialAccounts($user);

            $this->userManager->registerNewUser($newUser);

            $realUser = $newUser;
        }

        // also update anonymous user
        $user->clearDataAfterOAuthConfirmation();
        $this->userManager->updateUser($user);

        return $realUser;
    }

    /**
     * @param string $provider
     * @param string $accountId
     * @param User $user
     *
     * @return User
     */
    public function setUserSocialAccount($provider, $accountId, User $user)
    {
        switch ($provider) {

            case 'facebook':
                $user->setFacebookId($accountId);
                break;

            case 'vkontakte':
                $user->setVkontakteId($accountId);
                break;

            case 'mailru':
                $user->setMailruId($accountId);
                break;

            case 'odnoklassniki':
                $user->setOdnoklassnikiId($accountId);
                break;
        }

        return $user;
    }

    /**
     * @param string $provider
     * @param string $accountId
     *
     * @return null|User
     */
    public function findUserBySocialProvider($provider, $accountId)
    {
        switch ($provider) {

            case 'facebook':
                $user = $this->userManager->findUserBy(['facebookId' => $accountId]);
                break;

            case 'vkontakte':
                $user = $this->userManager->findUserBy(['vkontakteId' => $accountId]);
                break;

            case 'mailru':
                $user = $this->userManager->findUserBy(['mailruId' => $accountId]);
                break;

            case 'odnoklassniki':
                $user = $this->userManager->findUserBy(['odnoklassnikiId' => $accountId]);
                break;

            default:
                $user = null;
        }

        return $user;
    }

}
