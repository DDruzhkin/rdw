<?php
namespace RDW\Bundle\UserBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Knp\Component\Pager\Paginator;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\JobBundle\Entity\Job;
use RDW\Bundle\UserBundle\Entity\CvFavorite;
use RDW\Bundle\UserBundle\Entity\JobFavorite,
    RDW\Bundle\UserBundle\Entity\Favorite;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\UserInterface;
use RDW\Bundle\UserBundle\Model\FavoriteFilter;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use RDW\Bundle\AppBundle\Entity\AdInterface;

/**
 * Class FavoriteManager
 *
 * @package RDW\Bundle\UserBundle\Service
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class FavoriteManager
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $objectManager;

    /**
     * @var Paginator
     */
    private $paginator;

    /**
     * @param ObjectManager $objectManager
     * @param Paginator     $paginator
     */
    public function __construct(ObjectManager $objectManager, Paginator $paginator)
    {
        $this->objectManager = $objectManager;
        $this->paginator = $paginator;
    }

    public function getJobFavoriteById($id)
    {
        return $this->objectManager->getRepository('RDWUserBundle:JobFavorite')->find($id);
    }

    public function add(AdInterface $ad, UserInterface $user)
    {
        if (! $this->findByUser($ad, $user)) {
            $favorite = $this->create($ad, $user);

            return $this->update($favorite);
        }

        return true;
    }

    public function create(AdInterface $ad, UserInterface $user = null)
    {
        $favorite = $this->createByType($ad);
        $favorite->setAd($ad);

        if ($user) {
            $favorite->setUser($user);
        }

        return $favorite;
    }

    private function createByType(AdInterface $ad)
    {
        if ($ad instanceof Job) {
            $favorite = new JobFavorite();
        } elseif ($ad instanceof Cv) {
            $favorite = new CvFavorite();
        } else {
            throw new \RuntimeException(sprintf('Can not create favorite for object "%s"', get_class($ad)));
        }

        return $favorite;
    }

    /**
     * @param int $id
     *
     * @return CvFavorite
     */
    public function getCvFavoriteById($id)
    {
        return $this->getCvRepository()->find($id);
    }

    /**
     * @param Job           $job
     * @param UserInterface $user
     *
     * @return bool
     */
    public function removeJobFavoriteByJobAndUser(Job $job, UserInterface $user)
    {
        if ($favorite = $this->findByUser($job, $user)) {
            $this->remove($favorite);

            return true;
        }

        return false;
    }

    /**
     * @param JobFavorite $favorite
     */
    public function removeJob(JobFavorite $favorite)
    {
        $this->objectManager->remove($favorite);
        $this->objectManager->flush();
    }

    /**
     * @param AdInterface   $ad
     * @param UserInterface $user
     *
     * @return Favorite
     */
    public function findByUser(AdInterface $ad, UserInterface $user)
    {
        if ($ad instanceof Cv) {
            return $this->getCvRepository()->findOneBy(['cv' => $ad, 'user' => $user]);
        } elseif ($ad instanceof Job) {
            return $this->getJobRepository()->findOneBy(['job' => $ad, 'user' => $user]);
        } else {
            throw new UnexpectedTypeException($ad, 'RDW\Bundle\AppBundle\Entity\AdInterface');
        }
    }

    /**
     * archives Favorite
     *
     * @param Favorite $favorite
     *
     * @return void
     */
    public function archive(Favorite $favorite)
    {
        $favorite->setArchivedAt(new \DateTime());
        $this->update($favorite);
    }

    /**
     * @param Cv            $cv
     * @param UserInterface $user
     *
     * @return bool
     */
    public function removeCvFavoriteByCvAndUser(Cv $cv, UserInterface $user)
    {
        if ($favorite = $this->findByUser($cv, $user)) {
            $this->remove($favorite);

            return true;
        }

        return false;
    }

    /**
     * @param Favorite $favorite
     */
    public function remove(Favorite $favorite)
    {
        $this->objectManager->remove($favorite);
        $this->objectManager->flush();
    }

    /**
     * @param mixed $favoriteEntity
     *
     * @return bool
     */
    public function save($favoriteEntity)
    {
        if(! $favoriteEntity->getMeetTime()) {
            $favoriteEntity->setMeetTime(new \DateTime());
        }

        $this->update($favoriteEntity);

        return true;
    }

    public function merge($favorite)
    {
        return $this->objectManager->merge($favorite);
    }

    public function insert(Favorite $favorite)
    {
        if (! $this->findByUser($favorite->getAd(), $favorite->getUser())) {
            $this->update($favorite);
        } else {
            $this->objectManager->detach($favorite);
        }

        return true;
    }

    /**
     * @param mixed $favoriteEntity
     * @param bool  $andFlush
     *
     * @return bool
     */
    public function update($favorite, $andFlush = true)
    {
        $this->objectManager->persist($favorite);

        if ($andFlush) {
            $this->objectManager->flush();
        }

        return true;
    }

    /**
     * @param ArrayCollection $entities
     * @param RegisteredUser  $user
     */
    public function multipleRemoveByUser(ArrayCollection $entities, RegisteredUser $user)
    {
        foreach ($entities as $entity) {
            if ($entity->getUser()->getId() == $user->getId()) {
                $this->objectManager->remove($entity);
            }
        }

        $this->objectManager->flush();
    }

    /**
     * @param ArrayCollection $entities
     * @param RegisteredUser  $user
     */
    public function multipleMoveToArchiveByUser(ArrayCollection $entities, RegisteredUser $user)
    {
        foreach ($entities as $entity) {
            if ($entity->getUser()->getId() == $user->getId()) {
                $entity->setArchivedAt(new \DateTime());
                $this->objectManager->persist($entity);
            }
        }

        $this->objectManager->flush();
    }

    public function getRepository(Favorite $favorite)
    {
        if ($favorite instanceof JobFavorite) {
            return $this->getJobRepository();
        } elseif ($favorite instanceof CvFavorite) {
            return $this->getCvRepository();
        } else {
            throw new UnexpectedTypeException($favorite, Favorite::class);
        }
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    private function getCvRepository()
    {
        return $this->objectManager->getRepository('RDWUserBundle:CvFavorite');
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    private function getJobRepository()
    {
        return $this->objectManager->getRepository('RDWUserBundle:JobFavorite');
    }

    /**
     * @param Favorite      $favorite
     * @param UserInterface $user
     *
     * @return bool
     */
    public function isAllowedToArchive(Favorite $favorite, UserInterface $user)
    {
        return $favorite instanceof Favorite && $favorite->getUser()->getId() === $user->getId();
    }

    /**
     * @param FavoriteFilter $filter
     * @param int            $page
     * @param int            $limit
     *
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function getPaginatedList(FavoriteFilter $filter, $page, $limit)
    {
        if (FavoriteFilter::TYPE_JOB == $filter->getType()) {
            $repository = $this->getJobRepository();
            $method = 'getJobsListQuery';
        } else {
            $repository = $this->getCvRepository();
            $method = 'getCvsListQuery';
        }

        if ($filter->isArchivedVisible()) {
            $this->objectManager->getFilters()->disable('softdeleteable');
        }

        $pagination = $this->paginator->paginate($repository->{$method}($filter), $page, $limit);

        if ($filter->isArchivedVisible()) {
            $this->objectManager->getFilters()->enable('softdeleteable');
        }

        return $pagination;
    }

    /**
     * @param ArrayCollection $favorites
     *
     * @return ArrayCollection
     */
    public function getCvListFromFavorites(ArrayCollection $favorites)
    {
        $cvs = new ArrayCollection();

        foreach ($favorites as $favorite) {
            if ($favorite instanceof CvFavorite) {
                $cvs->add($favorite->getCv());
            }
        }

        return $cvs;
    }

    /**
     * @param ArrayCollection $favorites
     *
     * @return ArrayCollection
     */
    public function getJobsFromFavorites(ArrayCollection $favorites)
    {
        $jobs = new ArrayCollection();

        foreach ($favorites as $favorite) {
            if ($favorite instanceof JobFavorite) {
                $jobs->add($favorite->getJob());
            }
        }

        return $jobs;
    }
}
