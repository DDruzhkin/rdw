<?php

namespace RDW\Bundle\UserBundle\Service;

use FOS\UserBundle\Doctrine\UserManager as FosUserManager;
use FOS\UserBundle\Model\UserInterface;
use Knp\Component\Pager\Paginator;
use RDW\Bundle\LocationBundle\Service\LocationManager;
use RDW\Bundle\SlugBundle\Service\SlugService;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\User;
use FOS\UserBundle\Util\CanonicalizerInterface;
use RDW\Bundle\UserBundle\Model\WebUserFilter;
use RDW\Bundle\WalletBundle\Entity\Wallet;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Knp\Bundle\GaufretteBundle\FilesystemMap;
use FOS\UserBundle\Util\CanonicalFieldsUpdater;
use FOS\UserBundle\Util\PasswordUpdaterInterface;

/**
 * Class UserManager
 *
 * @package RDW\Bundle\UserBundle\Service
 */
class UserManager extends FosUserManager
{
    /**
     * @var FilesystemMap
     */
    protected $fileSystemMap;

    /**
     * @var string
     */
    protected $storageAdapter;

    /**
     * @var Paginator
     */
    private $paginator;

    /**
     * @var SlugService
     */
    private $slugService;

    /**
     * @var LocationManager
     */
    private $locationManager;

    /**
     * Constructor.
     *
     * @param EncoderFactoryInterface $encoderFactory
     * @param CanonicalizerInterface  $usernameCanonicalizer
     * @param CanonicalizerInterface  $emailCanonicalizer
     * @param ObjectManager           $om
     * @param string                  $class
     * @param FilesystemMap           $fileSystemMap
     * @param string                  $storageAdapter
     * @param Paginator               $paginator
     * @param SlugService             $slugService
     * @param LocationManager         $locationManager
     */
    public function __construct(
        PasswordUpdaterInterface $passwordUpdater,
        CanonicalFieldsUpdater $canonicalFieldsUpdater,
        #EncoderFactoryInterface $encoderFactory,
        #CanonicalizerInterface $usernameCanonicalizer,
        #CanonicalizerInterface $emailCanonicalizer,
        ObjectManager $om,
        $class,
        FilesystemMap $fileSystemMap,
        $storageAdapter,
        Paginator $paginator,
        SlugService $slugService,
        LocationManager $locationManager
    )
    {
        parent::__construct($passwordUpdater, $canonicalFieldsUpdater, $om, $class);
        //parent::__construct($encoderFactory, $usernameCanonicalizer, $emailCanonicalizer, $om, $class, $locationManager);

        $this->fileSystemMap = $fileSystemMap;
        $this->storageAdapter = $storageAdapter;
        $this->paginator = $paginator;
        $this->slugService = $slugService;
        $this->locationManager = $locationManager;
    }

    /**
     * @param RegisteredUser $user
     */
    public function registerNewUser(RegisteredUser $user)
    {
        $user->setUsername($user->getEmail());

        if ($user->isTypeEmployee()) {
            $user->addRole(RegisteredUser::ROLE_EMPLOYEE);
        } else {
            $user->addRole(RegisteredUser::ROLE_EMPLOYER);
        }

        if ($user->getEmployerType() == RegisteredUser::EMPLOYER_TYPE_PERSON) {
            //$user->setCompanyType(RegisteredUser::COMPANY_TYPE_OTHER);
            $user->setCompanyTitle($user->getContactPerson());
            $user->setName($user->getContactPerson());
        }

        // add new wallet
        $wallet = new Wallet();
        $wallet->setUser($user);
        $user->setWallet($wallet);

        $this->updateUser($user);
    }

    /**
     * @param RegisteredUser $user
     *
     * @return RegisteredUser
     */
    public function addDefaultLocation(RegisteredUser $user)
    {
        if ($user->getCities()->count() == 0) {
            $defaultCity = $this->locationManager->getDefaultCity();

            if ($defaultCity) {
                $user->addCity($defaultCity);
            }
        }

        return $user;
    }

    /**
     * @param RegisteredUser $user
     */
    public function updateProfile(RegisteredUser $user)
    {
        if ($user->isCompanyAsPerson()) {
            $user->setCompanyTitle($user->getContactPerson() . ' ' . $user->getContactPersonSurname());
            $user->setName($user->getContactPerson());
            $user->setPatronymic($user->getContactPersonPatronymic());
            $user->setSurname($user->getContactPersonSurname());
        }

        if ($user->isTypeEmployer()) {
            if ($user->isManager()) {
                // if user is manager, he cant have manager
                $user->setManager(null);
            } else {
                // if user not manager, he cant be assigned to new users
                $user->setSupportingNewClients(false);
            }

            // if user is manager or don't have manager, he can't have managerFrom date
            if( $user->isManager() || ! $user->getManager() ) {
                $user->setHasManagerFrom(null);
            }
        }

        $user->setUsername($user->getEmail());
        $this->updateUser($user);
    }

    /**
     * @param string $type
     *
     * @return RegisteredUser
     */
    public function createUserForAdmin($type)
    {
        /** @var $user RegisteredUser */
        $user = $this->createUser();
        $user->setType($type);

        $this->addDefaultLocation($user);

        return $user;
    }

    /**
     * @param RegisteredUser $user
     */
    public function deleteProfile(RegisteredUser $user)
    {
        if($user->isTypeEmployee()) {
            foreach($user->getCvs() as $cv) {
                $this->objectManager->remove($cv);
            }
        } elseif($user->isTypeEmployer()) {
            foreach($user->getJobs() as $job) {
                $this->objectManager->remove($job);
            }
        }

        $this->objectManager->persist($user);

        $this->deleteUser($user);
    }

    /**
     * {@inheritdoc}
     */
    public function deleteUser(UserInterface $user)
    {
        $user->setDeleted(true);

        $this->objectManager->persist($user);
        $this->objectManager->remove($user);

        $this->objectManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function findUserBy(array $criteria)
    {
        // Don't search for deleted, only do if asked so
        if (!isset($criteria['deleted'])) {
            //$criteria['deleted'] = false;
        }

        return parent::findUserBy($criteria);
    }

    /**
     * @param int $id
     *
     * @return UserInterface|object
     */
    public function findUserByIdForAdmin($id)
    {
        return $this->findUserBy(['id' => $id]);
    }

    /**
     * @param RegisteredUser $user
     */
    public function blockUser(RegisteredUser $user)
    {
        $user->setLocked(true);

        $this->updateUser($user);
    }

    /**
     * @param RegisteredUser $user
     */
    public function unblockUser(RegisteredUser $user)
    {
        $user->setLocked(false);
        $user->setEnabled(true);
        $user->setBlockReason(null);

        $this->updateUser($user);
    }

    /**
     * @param WebUserFilter $filter
     * @param int           $page
     * @param int           $limit
     *
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function getPaginatedWebUserList(WebUserFilter $filter, $page, $limit)
    {
        $query = $this->getRepository()->getWebUserListQuery($filter);

        $pagination = $this->paginator->paginate($query, $page, $limit);

        return $pagination;
    }

    /**
     * @return RegisteredUser|null
     */
    public function getManagerForSupportingNewUsers()
    {
        $manager = $this->getRepository()->findManagerForSupportingNewUsers();

        return ($manager) ? $manager[0] : null;
    }

    /**
     * @param RegisteredUser $manager
     * @param int            $limit
     *
     * @return array|\RDW\Bundle\UserBundle\Entity\RegisteredUser[]
     */
    public function getWithWaitingJobs(RegisteredUser $manager, $limit)
    {
        return $this->getRepository()->findHavingWaitingJobs($manager, $limit);
    }

    public function findUserByEmailIncludingDeleted($email)
    {
        $this->objectManager->getFilters()->disable('softdeleteable');
        $user = $this->findUserByEmail($email);
        $this->objectManager->getFilters()->enable('softdeleteable');

        return $user;
    }

    /**
     * @return \RDW\Bundle\UserBundle\Repository\UserRepository
     */
    public function getRepository()
    {
        return $this->objectManager->getRepository('RDWUserBundle:User');
    }
}
