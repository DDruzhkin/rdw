<?php
namespace RDW\Bundle\UserBundle\Validator\Constraints;

use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\TokenNotFoundException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class OldEmailValidator
 */
class OldEmailValidator extends ConstraintValidator
{
    /**
     * @var SecurityContext
     */
    protected $securityContext;

    /**
     * @param SecurityContext $securityContext
     */
    public function __construct(SecurityContext $securityContext)
    {
        $this->securityContext = $securityContext;
    }

    /**
     * @param string     $value
     * @param Constraint $constraint
     *
     * @throws TokenNotFoundException
     * @throws UsernameNotFoundException
     */
    public function validate($value, Constraint $constraint)
    {
        $token = $this->securityContext->getToken();

        if (!$token instanceof TokenInterface) {
            throw new TokenNotFoundException();
        }

        $loggedInUser = $token->getUser();

        if (!$loggedInUser instanceof RegisteredUser) {
            throw new UsernameNotFoundException();
        }

        if (! $loggedInUser->hasRole(RegisteredUser::ROLE_MANAGER) && $value != $loggedInUser->getEmail()) {
            $this->context->addViolation('Wrong value for your current old email');
        }
    }
}
