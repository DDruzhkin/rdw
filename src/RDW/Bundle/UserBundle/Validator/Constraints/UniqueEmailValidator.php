<?php
namespace RDW\Bundle\UserBundle\Validator\Constraints;

use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Service\UserManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class UniqueEmailValidator
 */
class UniqueEmailValidator extends ConstraintValidator
{
    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @param UserManager $userManager
     * @param Router $router
     * @param TranslatorInterface $translator
     */
    public function __construct(UserManager $userManager, \Symfony\Cmf\Component\Routing\ChainRouter $router, TranslatorInterface $translator)
    {
        $this->userManager = $userManager;
        $this->router = $router;
        $this->translator = $translator;
    }

    /**
     * @param string $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        $existingUser = $this->userManager->findUserByEmailIncludingDeleted($value);

        if ($existingUser instanceof RegisteredUser) {

            $resettingUrl = $this->router->generate('fos_user_resetting_request', [], true);
            $hereString = $this->translator->trans('here');

            $this->context->addViolation(
                $this->translator->trans(
                    'Email is already taken. Please enter other email address. If you already registered and forgot password please click %url%',
                    [
                        '%url%' => '<a href="' . $resettingUrl . '">' . $hereString . '</a>'
                    ]
                )
            );
        }
    }
}
