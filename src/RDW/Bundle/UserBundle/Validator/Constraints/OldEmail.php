<?php

namespace RDW\Bundle\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Old email validator
 *
 * @Annotation
 */
class OldEmail extends Constraint
{
    /**
     * {@inheritDoc}
     */
    public function validatedBy()
    {
        return 'rdw_old_email';
    }
}