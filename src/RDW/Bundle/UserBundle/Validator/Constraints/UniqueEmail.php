<?php

namespace RDW\Bundle\UserBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Unique email validator
 *
 * @Annotation
 */
class UniqueEmail extends Constraint
{
    /**
     * {@inheritDoc}
     */
    public function validatedBy()
    {
        return 'rdw_unique_email';
    }
}

