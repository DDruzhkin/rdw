<?php

namespace RDW\Bundle\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\ManageBundle\Entity\Filter;

/**
 * Class CompanyType
 *
 * @package RDW\Bundle\UserBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="company_types")
 */
class CompanyType extends Filter
{
    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\UserBundle\Entity\RegisteredUser", mappedBy="companyType", fetch="EXTRA_LAZY")
     */
    protected $users;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\JobBundle\Entity\Job", mappedBy="companyType", fetch="EXTRA_LAZY")
     */
    protected $jobs;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * constructor
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->jobs = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function isDeletable()
    {
        return ! ($this->jobs->count() > 0 || $this->users->count() > 0 || $this->isSystem());
    }
}
