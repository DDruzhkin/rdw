<?php

namespace RDW\Bundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use RDW\Bundle\JobBundle\Entity\Job,
    RDW\Bundle\UserBundle\Entity\Favorite;
use RDW\Bundle\AppBundle\Entity\AdInterface;

/**
 * JobFavorite class
 *
 * @ORM\Entity(repositoryClass="RDW\Bundle\UserBundle\Repository\FavoriteRepository")
 * @ORM\Table(name="user_job_favorites", uniqueConstraints={@ORM\UniqueConstraint(name="unique_key", columns={"job_id", "user_id"})})
 */
class JobFavorite implements Favorite
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="date", name="meet_time", nullable=true)
     * @Assert\DateTime(message="Meet time must be a valid date")
     */
    protected $meetTime;

    /**
     * @var string
     * @ORM\Column(type="text", name="comment", nullable=true)
     */
    protected $comment;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\User", inversedBy="jobFavorites")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var Job
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\JobBundle\Entity\Job")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id")
     */
    protected $job;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", name="archived_at", nullable=true)
     */
    protected $archivedAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \RDW\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param \RDW\Bundle\UserBundle\Entity\User $user
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getMeetTime()
    {
        return $this->meetTime;
    }

    /**
     * @param \DateTime $meetTime
     *
     * @return $this
     */
    public function setMeetTime($meetTime)
    {
        $this->meetTime = $meetTime;

        return $this;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     *
     * @return $this
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return \RDW\Bundle\JobBundle\Entity\Job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * @param \RDW\Bundle\JobBundle\Entity\Job $job
     *
     * @return $this
     */
    public function setJob(Job $job)
    {
        $this->job = $job;

        return $this;
    }

    public function setAd(AdInterface $ad)
    {
        $this->job = $ad;

        return $this;
    }

    public function getAd()
    {
        return $this->job;
    }

    /**
     * @return \DateTime
     */
    public function getArchivedAt()
    {
        return $this->archivedAt;
    }

    /**
     * @param \DateTime $archivedAt
     *
     * @return $this
     */
    public function setArchivedAt(\DateTime $archivedAt)
    {
        $this->archivedAt = $archivedAt;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isOwner(UserInterface $user)
    {
        return ($user->getId() == $this->getUser()->getId());
    }
}
