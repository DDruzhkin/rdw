<?php

namespace RDW\Bundle\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\HistoryBundle\Entity\History;
use RDW\Bundle\JobBundle\Entity\Job;
use Gedmo\Mapping\Annotation as Gedmo;
use RDW\Bundle\AppBundle\Entity\Transliterator;

/**
 * Main user class
 *
 * @ORM\Entity(repositoryClass="RDW\Bundle\UserBundle\Repository\UserRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class RegisteredUser extends User implements UserInterface
{
    const ROLE_EMPLOYER = 'ROLE_EMPLOYER';
    const ROLE_EMPLOYEE = 'ROLE_EMPLOYEE';
    const ROLE_MANAGER = 'ROLE_MANAGER';

    const EMPLOYER_TYPE_COMPANY = 1;
    const EMPLOYER_TYPE_AGENCY = 2;
    const EMPLOYER_TYPE_PERSON = 3;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
    const STATUS_BLOCKED = 3;

    const CATEGORY_A = 'a';
    const CATEGORY_B = 'b';
    const CATEGORY_C = 'c';

    const GENDER_MALE = 'm';
    const GENDER_FEMALE = 'f';

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $deleted;

    /**
     * @var int
     *
     * @ORM\Column(name="foreign_id", type="integer", nullable=true)
     */
    private $foreignId;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true, name="employer_type")
     */
    protected $employerType;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="company_title")
     */
    protected $companyTitle;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="company_trade_mark")
     */
    protected $companyTradeMark;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="contact_person")
     */
    protected $contactPerson;

    /**
     * @var CompanyType
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\CompanyType", inversedBy="users")
     * @ORM\JoinColumn(name="company_type_id", referencedColumnName="id")
     */
    protected $companyType;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $surname;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $patronymic;

    /**
     * @var string
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    protected $gender;

    /**
     * @var MaritalStatus
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\MaritalStatus", inversedBy="users")
     * @ORM\JoinColumn(name="family_status_id", referencedColumnName="id")
     */
    protected $familyStatus;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $children;

    /**
     * @var \DateTime
     * @ORM\Column(type="date", nullable=true)
     */
    protected $birthday;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="youtube_video")
     */
    protected $youtubeVideo;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $phone;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $category;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", name="has_manager_from", nullable=true)
     */
    protected $hasManagerFrom;

    /**
     * @var string
     * @ORM\Column(type="text", name="admin_comment", nullable=true)
     */
    protected $adminComment;

    /**
     * @var string
     * @ORM\Column(type="text", name="manager_comment", nullable=true)
     */
    protected $managerComment;

    /**
     * @var string
     * @ORM\Column(type="text", name="block_reason", nullable=true)
     */
    protected $blockReason;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="fb_contact")
     */
    protected $fbContact;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="vk_contact")
     */
    protected $vkContact;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true, name="created_at")
     *
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true, name="updated_at")
     *
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="ok_contact")
     */
    protected $okContact;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="g_plus_contact")
     */
    protected $gPlusContact;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="twitter_contact")
     */
    protected $twitterContact;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="linked_in_contact")
     */
    protected $linkedInContact;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="skype_contact")
     */
    protected $skypeContact;

    /**
     * @var string
     */
    protected $oldPassword;

    /**
     * @var string
     */
    protected $oldEmail;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="new_email")
     */
    protected $newEmail;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="photo")
     */
    protected $photo;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="unp_code")
     */
    protected $unpCode;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="bank_account")
     */
    protected $bankAccount;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="bank_name")
     */
    protected $bankName;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="bank_code")
     */
    protected $bankCode;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="bank_address")
     */
    protected $bankAddress;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $address;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $fax;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="url_address")
     */
    protected $urlAddress;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true, name="company_info")
     */
    protected $companyInfo;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="contact_person_surname")
     */
    protected $contactPersonSurname;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="contact_person_patronymic")
     */
    protected $contactPersonPatronymic;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="contact_person_position")
     */
    protected $contactPersonPosition;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="contact_person_photo")
     */
    protected $contactPersonPhoto;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="contact_person_phone")
     */
    protected $contactPersonPhone;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="contact_person_email")
     */
    protected $contactPersonEmail;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\JobBundle\Entity\Job", mappedBy="user")
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    protected $jobs;

    /**
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\Scope", inversedBy="users")
     * @ORM\JoinColumn(name="scope_id", referencedColumnName="id")
     */
    protected $scope;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\CvBundle\Entity\Cv", mappedBy="user")
     * @ORM\OrderBy({"updatedAt" = "DESC"})
     */
    protected $cvs;

    /**
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\RegisteredUser", inversedBy="assignedUsers")
     * @ORM\JoinColumn(name="manager_id", referencedColumnName="id")
     */
    protected $manager;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\UserBundle\Entity\RegisteredUser", mappedBy="manager")
     */
    protected $assignedUsers;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="personal_page_title")
     */
    protected $personalPageTitle;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="personal_page_short_title")
     */
    protected $personalPageShortTitle;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true, name="personal_page_description")
     */
    protected $personalPageDescription;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\JobBundle\Entity\JobView", mappedBy="user")
     */
    private $jobsViewed;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\JobBundle\Entity\JobShowContacts", mappedBy="user")
     */
    private $jobsShowedContacts;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\HistoryBundle\Entity\History", mappedBy="user")
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    protected $history;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\CvBundle\Entity\CvView", mappedBy="user")
     */
    protected $cvsViewed;

    /**
     * @var bool
     * @ORM\Column(name="supporting_new_clients", type="boolean", nullable=true)
     */
    private $supportingNewClients;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true, name="profile_percent")
     */
    private $profilePercent;

    /**
     * @var int
     */
    private $waitingJobsCount;

    /**
     * @var boolean
     *
     * @ORM\Column(name="has_used_first_job_promo", type="boolean", nullable=true)
     */
    private $usedFirstJobPromo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="password_reset", type="boolean", nullable=true)
     */
    private $passwordReset;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->deleted = false;
        $this->cvs = new ArrayCollection();
        $this->jobs = new ArrayCollection();
        $this->cvsViewed = new ArrayCollection();
        $this->history = new ArrayCollection();
        $this->assignedUsers = new ArrayCollection();
        $this->children = null;
    }

    /**
     * @param string $contactPersonSurname
     *
     * @return self
     */
    public function setContactPersonSurname($contactPersonSurname)
    {
        $this->contactPersonSurname = $contactPersonSurname;

        return $this;
    }

    /**
     * @param string $contactPersonPosition
     *
     * @return self
     */
    public function setContactPersonPosition($contactPersonPosition)
    {
        $this->contactPersonPosition = $contactPersonPosition;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactPersonSurname()
    {
        return $this->contactPersonSurname;
    }

    /**
     * @return string
     */
    public function getContactPersonFullName()
    {
        return sprintf('%s %s', $this->getContactPerson(), $this->getContactPersonSurname());
    }

    /**
     * @return string
     */
    public function getContactPersonPosition()
    {
        return $this->contactPersonPosition;
    }

    /**
     * @return Scope
     */
    public function getScope()
    {
        return $this->scope;
    }

    /**
     * @param Scope $scope
     *
     * @return self
     */
    public function setLocked($d) {

        return $this;
    }

    /**
     * @param Scope $scope
     *
     * @return self
     */
    public function setScope(Scope $scope)
    {
        $this->scope = $scope;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrlAddress()
    {
        return $this->urlAddress;
    }

    /**
     * @return string
     */
    public function getCompanyInfo()
    {
        return $this->companyInfo;
    }

    /**
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     *
     * @return self
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @param string $fax
     *
     * @return self
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * @param string $companyInfo
     *
     * @return self
     */
    public function setCompanyInfo($companyInfo)
    {
        $this->companyInfo = $companyInfo;

        return $this;
    }

    /**
     * @param string $urlAddress
     *
     * @return self
     */
    public function setUrlAddress($urlAddress)
    {
        $this->urlAddress = $urlAddress;

        return $this;
    }

    /**
     * @return bool
     */
    public function isTypeEmployee()
    {
        return (self::USER_TYPE_EMPLOYEE == $this->getType() ? true : false);
    }

    /**
     * @return bool
     */
    public function isTypeEmployer()
    {
        return (self::USER_TYPE_EMPLOYER == $this->getType() ? true : false);
    }

    /**
     * @return bool
     */
    public function isCompanyAsPerson()
    {
        return ($this->getEmployerType() == self::EMPLOYER_TYPE_PERSON) ? true : false;
    }

    /**
     * @return int
     */
    public function getEmployerType()
    {
        return $this->employerType;
    }

    /**
     * @param int $employerType
     *
     * @return self
     */
    public function setEmployerType($employerType)
    {
        $this->employerType = $employerType;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyTitle()
    {
        return $this->companyTitle;
    }

    /**
     * @param string $companyTitle
     *
     * @return self
     */
    public function setCompanyTitle($companyTitle)
    {
        $this->companyTitle = $companyTitle;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * @param string $contactPerson
     *
     * @return self
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * @param CompanyType $companyType
     *
     * @return self
     */
    public function setCompanyType(CompanyType $companyType = null)
    {
        $this->companyType = $companyType;

        return $this;
    }

    /**
     * @return CompanyType
     */
    public function getCompanyType()
    {
        return $this->companyType;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     *
     * @return self
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     *
     * @return self
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param \DateTime $birthday
     *
     * @return self
     */
    public function setBirthday(\DateTime $birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * @return string
     */
    public function getYoutubeVideo()
    {
        return $this->youtubeVideo;
    }

    /**
     * @param string $youtubeVideo
     *
     * @return self
     */
    public function setYoutubeVideo($youtubeVideo)
    {
        $this->youtubeVideo = $youtubeVideo;

        return $this;
    }

    /**
     * @param string $phone
     *
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getSkypeContact()
    {
        return $this->skypeContact;
    }

    /**
     * @return string
     */
    public function getFbContact()
    {
        return $this->fbContact;
    }

    /**
     * @return string
     */
    public function getVkContact()
    {
        return $this->vkContact;
    }

    /**
     * @return string
     */
    public function getOkContact()
    {
        return $this->okContact;
    }

    /**
     * @return string
     */
    public function getGPlusContact()
    {
        return $this->gPlusContact;
    }

    /**
     * @return string
     */
    public function getTwitterContact()
    {
        return $this->twitterContact;
    }

    /**
     * @return string
     */
    public function getLinkedInContact()
    {
        return $this->linkedInContact;
    }

    /**
     * @param string $skypeContact
     *
     * @return self
     */
    public function setSkypeContact($skypeContact)
    {
        $this->skypeContact = $skypeContact;

        return $this;
    }

    /**
     * @param string $fbContact
     *
     * @return self
     */
    public function setFbContact($fbContact)
    {
        $this->fbContact = $fbContact;

        return $this;
    }

    /**
     * @param string $vkContact
     *
     * @return self
     */
    public function setVkContact($vkContact)
    {
        $this->vkContact = $vkContact;

        return $this;
    }

    /**
     * @param string $okContact
     *
     * @return self
     */
    public function setOKContact($okContact)
    {
        $this->okContact = $okContact;

        return $this;
    }

    /**
     * @param string $gPlusContact
     *
     * @return self
     */
    public function setGPlusContact($gPlusContact)
    {
        $this->gPlusContact = $gPlusContact;

        return $this;
    }

    /**
     * @param string $twitterContact
     *
     * @return self
     */
    public function setTwitterContact($twitterContact)
    {
        $this->twitterContact = $twitterContact;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param string $photo
     *
     * @return self
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactPersonPhoto()
    {
        return $this->contactPersonPhoto;
    }

    /**
     * @param string $contactPersonPhoto
     *
     * @return self
     */
    public function setContactPersonPhoto($contactPersonPhoto)
    {
        $this->contactPersonPhoto = $contactPersonPhoto;

        return $this;
    }

    /**
     * @param string $linkedIdContact
     *
     * @return self
     */
    public function setLinkedInContact($linkedIdContact)
    {
        $this->linkedInContact = $linkedIdContact;

        return $this;
    }

    /**
     * @return string
     */
    public function getUnpCode()
    {
        return $this->unpCode;
    }

    /**
     * @param string $unpCode
     *
     * @return self
     */
    public function setUnpCode($unpCode)
    {
        $this->unpCode = $unpCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getOldPassword()
    {
        return $this->oldPassword;
    }

    /**
     * @param string $oldPassword
     *
     * @return self
     */
    public function setOldPassword($oldPassword)
    {
        $this->oldPassword = $oldPassword;

        return $this;
    }

    /**
     * @return array
     */
    public static function getEmployerTypeChoiceList()
    {
        return [
            self::EMPLOYER_TYPE_COMPANY => 'Employer type company',
            self::EMPLOYER_TYPE_AGENCY => 'Employer type agency',
            self::EMPLOYER_TYPE_PERSON => 'Employer type person',
        ];
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return array
     */
    public static function getUserStatusesChoiceListForAdmin()
    {
        return [
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_INACTIVE => 'Inactive',
            self::STATUS_BLOCKED => 'Blocked'
        ];
    }

    /**
     * @return string
     */
    public function getFormNameByType()
    {
        return (self::USER_TYPE_EMPLOYEE == $this->getType() ? 'rdw_employee_profile' : 'rdw_employer_profile');
    }

    /**
     * @param Cv $cv
     *
     * @return $this
     */
    public function addCv(Cv $cv)
    {
        $this->cvs->add($cv);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getCvs()
    {
        return $this->cvs;
    }

    /**
     * @return Cv
     */
    public function getLastActiveCv()
    {
        /** @var Cv $cv */
        foreach($this->cvs as $cv) {
            if($cv->isStatusActive() && ! $cv->isDeleted()) {
                return $cv;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function hasVideo()
    {
        return ($this->getYoutubeVideo() != '') ? true : false;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     *
     * @return User
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * @return string
     */
    public function getNewEmail()
    {
        return $this->newEmail;
    }

    /**
     * @param string $newEmail
     *
     * @return self
     */
    public function setNewEmail($newEmail)
    {
        $this->newEmail = $newEmail;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Is user deleted
     *
     * @return bool
     */
    public function isDeleted()
    {
        return ($this->deleted == true);
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * {@inheritdoc}
     */
    public function isAccountNonExpired()
    {
        return (!$this->isDeleted() && parent::isAccountNonExpired());
    }

    /**
     * @return string
     */
    public function getOldEmail()
    {
        return $this->oldEmail;
    }

    /**
     * @param string $oldEmail
     *
     * @return self
     */
    public function setOldEmail($oldEmail)
    {
        $this->oldEmail = $oldEmail;

        return $this;
    }

    /**
     * @return array
     */
    public static function getGenderChoices()
    {
        return ['m' => 'Male', 'f' => 'Female'];
    }

    /**
     * @param MaritalStatus $maritalStatus
     *
     * @return self
     */
    public function setFamilyStatus(MaritalStatus $maritalStatus = null)
    {
        $this->familyStatus = $maritalStatus;

        return $this;
    }

    /**
     * Get familyStatus
     *
     * @return MaritalStatus
     */
    public function getFamilyStatus()
    {
        return $this->familyStatus;
    }

    /**
     * Set children
     *
     * @param integer $children
     *
     * @return User
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * Get children
     *
     * @return integer
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return string
     */
    public function getAdminComment()
    {
        return $this->adminComment;
    }

    /**
     * @param string $adminComment
     *
     * @return $this
     */
    public function setAdminComment($adminComment)
    {
        $this->adminComment = $adminComment;

        return $this;
    }

    /**
     * @return \RDW\Bundle\UserBundle\Entity\RegisteredUser
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * @param \RDW\Bundle\UserBundle\Entity\RegisteredUser $manager
     *
     * @return $this
     */
    public function setManager(RegisteredUser $manager = null)
    {
        $this->manager = $manager;

        if (is_null($manager)) {
            $this->hasManagerFrom = null;
        }

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getHasManagerFrom()
    {
        return $this->hasManagerFrom;
    }

    /**
     * @param \DateTime $hasManagerFrom
     *
     * @return $this
     */
    public function setHasManagerFrom(\DateTime $hasManagerFrom = null)
    {
        $this->hasManagerFrom = $hasManagerFrom;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param mixed $deletedAt
     *
     * @return $this
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getBlockReason()
    {
        return $this->blockReason;
    }

    /**
     * @param string $blockReason
     *
     * @return $this
     */
    public function setBlockReason($blockReason)
    {
        $this->blockReason = $blockReason;

        return $this;
    }

    /**
     * @return string
     */
    public function getPersonalPageTitle()
    {
        return $this->personalPageTitle;
    }

    /**
     * @param mixed $personalPageTitle
     *
     * @return $this
     */
    public function setPersonalPageTitle($personalPageTitle)
    {
        $this->personalPageTitle = $personalPageTitle;

        return $this;
    }

    /**
     * @return string
     */
    public function getPersonalPageShortTitle()
    {
        return $this->personalPageShortTitle;
    }

    /**
     * @param string $personalPageShortTitle
     *
     * @return $this
     */
    public function setPersonalPageShortTitle($personalPageShortTitle)
    {
        $this->personalPageShortTitle = $personalPageShortTitle;

        return $this;
    }

    /**
     * @return string
     */
    public function getPersonalPageDescription()
    {
        return $this->personalPageDescription;
    }

    /**
     * @param string $personalPageDescription
     *
     * @return $this
     */
    public function setPersonalPageDescription($personalPageDescription)
    {
        $this->personalPageDescription = $personalPageDescription;

        return $this;
    }

    /**
     * @ORM\PrePersist
     *
     * generates slug
     *
     * @return Job
     */
   public function generateSlug()
   {
       $slugify = [];

       if (!$this->getCompanyTitle()) {
           $slugify[] = $this->getName();
           $slugify[] = $this->getSurname();
       } else {
           $slugify[] = $this->getCompanyTitle();
       }

       $this->slug = Transliterator::transliterate(implode(' ', $slugify));

       return $this;
   }

    /**
     * @return \RDW\Bundle\SlugBundle\Entity\Slug
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param \RDW\Bundle\UserBundle\Entity\User $manager
     *
     * @return bool
     */
    public function isUserManager(User $manager)
    {
        $isManager = false;

        $userManager = $this->getManager();
        if ($userManager instanceof RegisteredUser) {
            $isManager = ($userManager->getId() == $manager->getId()) ? true : false;
        }

        return $isManager;
    }

    /**
     * @return ArrayCollection
     */
    public function getJobsViewed()
    {
        return $this->jobsViewed;
    }

    /**
     * @param ArrayCollection $jobsViewed
     *
     * @return $this
     */
    public function setJobsViewed($jobsViewed)
    {
        $this->jobsViewed = $jobsViewed;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getHistory()
    {
        return $this->history;
    }

    /**
     * @param History $history
     *
     * @return $this
     */
    public function addHistory(History $history)
    {
        $this->history->add($history);

        return $this;
    }

    /**
     * @param History $history
     *
     * @return $this
     */
    public function removeHistory(History $history)
    {
        $this->history->removeElement($history);

        return $this;
    }

    /**
     * @param Job $job
     *
     * @return $this
     */
    public function addJobViewed(Job $job)
    {
        if ( ! $this->jobsViewed->contains($job)) {
            $job->addUserWhoViewed($this);
            $this->jobsViewed->add($job);
        }

        return $this;
    }

    /**
     * @param Job $job
     *
     * @return $this
     */
    public function addJobShowedContacts(Job $job)
    {
        if ( ! $this->jobsShowedContacts->contains($job)) {
            $job->addUserWhoShowContact($this);
            $this->jobsShowedContacts->add($job);
        }

        return $this;
    }


    /**
     * @param Job $job
     *
     * @return $this
     */
    public function removeJobViewed(Job $job)
    {
        $this->jobsViewed->removeElement($job);

        return $this;
    }

    /**
     * @param Job $job
     *
     * @return $this
     */
    public function removeJobShowedContacts(Job $job)
    {
        $this->jobsShowedContacts->removeElement($job);

        return $this;
    }

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $category
     *
     * @return $this
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return array
     */
    public static function getUserCategoriesChoiceListForAdmin()
    {
        return [
            self::CATEGORY_A => 'A',
            self::CATEGORY_B => 'B',
            self::CATEGORY_C => 'C',
        ];
    }

    /**
     * @return null|Cv
     */
    public function getFirstActiveCv()
    {
        $activeStatuses = [Cv::STATUS_ACTIVE];

        foreach ($this->cvs as $cv) {
            if (in_array($cv->getStatus(), $activeStatuses)) {
                return $cv;
            }
        }

        return null;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getCvsViewed()
    {
        return $this->cvsViewed;
    }

    /**
     * @return array
     */
    public function getCvsViewedIds()
    {
        $ids = [];

        /** @var \RDW\Bundle\CvBundle\Entity\CvView $view */
        foreach($this->cvsViewed as $view) {
            $ids[] = $view->getCv()->getId();
        }

        return $ids;
    }

    /**
     * @return bool
     */
    public function isManager()
    {
        return $this->hasRole(self::ROLE_MANAGER);
    }

    /**
     * @return boolean
     */
    public function isSupportingNewClients()
    {
        return $this->supportingNewClients;
    }

    /**
     * @param boolean $supportingNewClients
     *
     * @return $this
     */
    public function setSupportingNewClients($supportingNewClients)
    {
        $this->supportingNewClients = $supportingNewClients;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getAssignedUsers()
    {
        return $this->assignedUsers;
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function addAssignedUser(User $user)
    {
        if ( ! $this->assignedUsers->contains($user)) {
            $this->assignedUsers->add($user);
        }

        return $this;
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function removeAssignedUser(User $user)
    {
        $this->assignedUsers->removeElement($user);

        return $this;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->name . ' ' . $this->surname;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add jobs
     *
     * @param Job $job
     *
     * @return User
     */
    public function addJob(Job $job)
    {
        $this->jobs[] = $job;

        return $this;
    }

    /**
     * Remove jobs
     *
     * @param Job $job
     */
    public function removeJob(Job $job)
    {
        $this->jobs->removeElement($job);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * @return int
     */
    public function getProfilePercent()
    {
        return $this->profilePercent;
    }

    /**
     * @param int $profilePercent
     *
     * @return $this
     */
    public function setProfilePercent($profilePercent)
    {
        $this->profilePercent = $profilePercent;

        return $this;
    }

    /**
     * @return boolean
     */
    public function hasActiveCv()
    {
        $activeStatuses = [Cv::STATUS_ACTIVE];

        foreach ($this->cvs as $cv) {
            if (in_array($cv->getStatus(), $activeStatuses)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return boolean
     */
    public function hasActiveJob()
    {
        $activeStatuses = [Job::STATUS_ACTIVE, Job::STATUS_INACTIVE];

        foreach ($this->jobs as $job) {
            if (in_array($job->getStatus(), $activeStatuses)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isEmployeeHasActiveCv()
    {
        return ($this->isTypeEmployee() && $this->hasActiveCv()) ? true : false;
    }

    /**
     * @return bool
     */
    public function isEmployerHasActiveJob()
    {
        return ($this->isTypeEmployer() && $this->hasActiveJob()) ? true : false;
    }

    /**
     * Get main role. If employee then ROLE_EMPLOYEE, else ROLE_EMPLOYER
     * @return string
     */
    public function getMainRole()
    {
        return (self::USER_TYPE_EMPLOYEE == $this->getType()) ? self::ROLE_EMPLOYEE : self::ROLE_EMPLOYER;
    }

    /**
     * Get user title by his type
     *
     * @return string
     */
    public function getTitle()
    {
        if ($this->isTypeEmployer() && ! $this->isCompanyAsPerson()) {
            $title = $this->getCompanyTitle();
        } elseif ($this->isCompanyAsPerson()) {
            $title = $this->getContactPerson();
        } else {
            $title = trim(sprintf('%s %s', $this->getName(), $this->getSurname()));

            $title = (empty($title)) ? $this->getEmail() : $title;
        }

        return $title;
    }

    /**
     * @return int
     */
    public function getWaitingJobsCount()
    {
        return $this->waitingJobsCount;
    }

    /**
     * @param int $waitingJobsCount
     *
     * @return $this
     */
    public function setWaitingJobsCount($waitingJobsCount)
    {
        $this->waitingJobsCount = $waitingJobsCount;

        return $this;
    }

    /**
     * @return int
     */
    public function getAge()
    {
        $oDateNow = new \DateTime();
        $oDateInterval = $oDateNow->diff($this->getBirthday());

        return (int) $oDateInterval->y;
    }

    /**
     * @return string
     */
    public function getContactPersonEmail()
    {
        return $this->contactPersonEmail;
    }

    /**
     * @param string $contactPersonEmail
     *
     * @return $this
     */
    public function setContactPersonEmail($contactPersonEmail)
    {
        $this->contactPersonEmail = $contactPersonEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactPersonPatronymic()
    {
        return $this->contactPersonPatronymic;
    }

    /**
     * @param string $contactPersonPatronymic
     *
     * @return $this
     */
    public function setContactPersonPatronymic($contactPersonPatronymic)
    {
        $this->contactPersonPatronymic = $contactPersonPatronymic;

        return $this;
    }

    /**
     * @return string
     */
    public function getContactPersonPhone()
    {
        return $this->contactPersonPhone;
    }

    /**
     * @param string $contactPersonPhone
     *
     * @return $this
     */
    public function setContactPersonPhone($contactPersonPhone)
    {
        $this->contactPersonPhone = $contactPersonPhone;

        return $this;
    }

    /**
     * @return string
     */
    public function getPatronymic()
    {
        return $this->patronymic;
    }

    /**
     * @param string $patronymic
     *
     * @return $this
     */
    public function setPatronymic($patronymic)
    {
        $this->patronymic = $patronymic;

        return $this;
    }

    /**
     * @return string
     */
    public function getBankAccount()
    {
        return $this->bankAccount;
    }

    /**
     * @param string $bankAccount
     *
     * @return $this
     */
    public function setBankAccount($bankAccount)
    {
        $this->bankAccount = $bankAccount;

        return $this;
    }

    /**
     * @return string
     */
    public function getBankCode()
    {
        return $this->bankCode;
    }

    /**
     * @param string $bankCode
     *
     * @return $this
     */
    public function setBankCode($bankCode)
    {
        $this->bankCode = $bankCode;
    }

    /**
     * @return string
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param string $bankName
     *
     * @return $this
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;

        return $this;
    }

    /**
     * @return string
     */
    public function getBankAddress()
    {
        return $this->bankAddress;
    }

    /**
     * @param string $bankAddress
     *
     * @return $this
     */
    public function setBankAddress($bankAddress)
    {
        $this->bankAddress = $bankAddress;

        return $this;
    }

    /**
     * @return string
     */
    public function getManagerComment()
    {
        return $this->managerComment;
    }

    /**
     * @param string $managerComment
     *
     * @return $this
     */
    public function setManagerComment($managerComment)
    {
        $this->managerComment = $managerComment;

        return $this;
    }

    /**
     * @return boolean
     */
    public function hasUsedFirstJobPromo()
    {
        return $this->usedFirstJobPromo;
    }

    /**
     * @param boolean $usedFirstJobPromo
     *
     * @return $this
     */
    public function setUsedFirstJobPromo($usedFirstJobPromo)
    {
        $this->usedFirstJobPromo = $usedFirstJobPromo;

        return $this;
    }

    /**
     * @return int
     */
    public function getForeignId()
    {
        return $this->foreignId;
    }

    /**
     * @param int $foreignId
     *
     * @return $this
     */
    public function setForeignId($foreignId)
    {
        $this->foreignId = $foreignId;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getPasswordReset()
    {
        return $this->passwordReset;
    }

    /**
     * @param boolean $passwordReset
     *
     * @return $this
     */
    public function setPasswordReset($passwordReset)
    {
        $this->passwordReset = $passwordReset;

        return $this;
    }

    /**
     * Set companyTradeMark
     *
     * @param string $companyTradeMark
     * @return $this
     */
    public function setCompanyTradeMark($companyTradeMark)
    {
        $this->companyTradeMark = $companyTradeMark;

        return $this;
    }

    /**
     * Get companyTradeMark
     *
     * @return string 
     */
    public function getCompanyTradeMark()
    {
        return $this->companyTradeMark;
    }
}
