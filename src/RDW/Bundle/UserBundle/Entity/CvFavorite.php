<?php

namespace RDW\Bundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use RDW\Bundle\CvBundle\Entity\Cv;
use RDW\Bundle\UserBundle\Entity\Favorite;
use RDW\Bundle\AppBundle\Entity\AdInterface;

/**
 * Class CvFavorite
 *
 * @package RDW\Bundle\UserBundle\Entity
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 *
 * @ORM\Entity(repositoryClass="RDW\Bundle\UserBundle\Repository\FavoriteRepository")
 * @ORM\Table(
 *      name="user_cv_favorites",
 *      uniqueConstraints={@ORM\UniqueConstraint(name="cv_user_unique", columns={"user_id", "cv_id"})}
 * )
 */
class CvFavorite implements Favorite
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="date", name="meet_time", nullable=true)
     * @Assert\DateTime(message="Meet time must be a valid date", groups={"edit"})
     */
    protected $meetTime;

    /**
     * @var string
     * @ORM\Column(type="text", name="meet_result", nullable=true)
     */
    protected $meetResult;

    /**
     * @var string
     * @ORM\Column(type="text", name="comment", nullable=true)
     */
    protected $comment;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\UserBundle\Entity\User", inversedBy="cvFavorites")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var Cv
     *
     * @ORM\ManyToOne(targetEntity="RDW\Bundle\CvBundle\Entity\Cv")
     * @ORM\JoinColumn(name="cv_id", referencedColumnName="id")
     */
    protected $cv;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", name="archived_at", nullable=true)
     */
    protected $archivedAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \RDW\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param \RDW\Bundle\UserBundle\Entity\User $user
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getMeetTime()
    {
        return $this->meetTime;
    }

    /**
     * @param \DateTime $meetTime
     *
     * @return $this
     */
    public function setMeetTime($meetTime)
    {
        $this->meetTime = $meetTime;

        return $this;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     *
     * @return $this
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return string
     */
    public function getMeetResult()
    {
        return $this->meetResult;
    }

    /**
     * @param string $meetResult
     *
     * @return $this
     */
    public function setMeetResult($meetResult)
    {
        $this->meetResult = $meetResult;

        return $this;
    }

    /**
     * @return \RDW\Bundle\CvBundle\Entity\Cv
     */
    public function getCv()
    {
        return $this->cv;
    }

    /**
     * @param \RDW\Bundle\CvBundle\Entity\Cv $cv
     *
     * @return $this
     */
    public function setCv(Cv $cv)
    {
        $this->cv = $cv;

        return $this;
    }

    public function setAd(AdInterface $ad)
    {
        $this->cv = $ad;

        return $this;
    }

    public function getAd()
    {
        return $this->cv;
    }

    /**
     * @return \DateTime
     */
    public function getArchivedAt()
    {
        return $this->archivedAt;
    }

    /**
     * @param \DateTime $archivedAt
     *
     * @return $this
     */
    public function setArchivedAt(\DateTime $archivedAt)
    {
        $this->archivedAt = $archivedAt;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isOwner(UserInterface $user)
    {
        return ($user->getId() == $this->getUser()->getId());
    }
}
