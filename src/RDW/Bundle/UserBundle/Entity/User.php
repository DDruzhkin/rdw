<?php

namespace RDW\Bundle\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Sonata\UserBundle\Entity\BaseUser as SonataBaseUser;
use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\ConversationBundle\Entity\Conversation;
use RDW\Bundle\ConversationBundle\Entity\Message;
use RDW\Bundle\CvBundle\Entity\CvMessage;
use RDW\Bundle\JobBundle\Entity\Apply as JobApply;
use RDW\Bundle\LocationBundle\Entity\City;
use RDW\Bundle\SubscriptionBundle\Entity\Subscription;
use RDW\Bundle\WalletBundle\Entity\Wallet;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Class User
 *
 * @package RDW\Bundle\UserBundle\Entity
 *
 * @ORM\Entity(repositoryClass="RDW\Bundle\UserBundle\Repository\UserRepository")
 * @ORM\Table(
 *      name="users",
 *      indexes={
 *          @ORM\Index(name="foreign_id_idx", columns={"foreign_id"}),
 *          @ORM\Index(name="confirmation_token_idx", columns={"confirmation_token"}),
 *          @ORM\Index(name="manager_supporting_new_clients_idx", columns={"enabled", "supporting_new_clients", "object_type", "type"}),
 *      }
 * )
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 *
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="object_type", type="string")
 * @ORM\DiscriminatorMap({"registered" = "RDW\Bundle\UserBundle\Entity\RegisteredUser"})
 */
abstract class User extends BaseUser implements UserInterface
{
    const USER_TYPE_EMPLOYER = 'employer';
    const USER_TYPE_EMPLOYEE = 'employee';

    const OBJECT_TYPE_REGISTERED = 'registered';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="RDW\Bundle\LocationBundle\Entity\City", cascade={"persist"}, inversedBy="users")
     */
    protected $cities;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\ConversationBundle\Entity\Conversation", mappedBy="sender")
     */
    protected $sentConversations;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\ConversationBundle\Entity\Conversation", mappedBy="receiver")
     */
    protected $receivedConversations;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\JobBundle\Entity\Apply", mappedBy="user")
     */
    protected $jobApplies;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\CvBundle\Entity\CvMessage", mappedBy="user")
     */
    private $cvMessages;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\UserBundle\Entity\JobFavorite", mappedBy="user")
     */
    protected $jobFavorites;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\UserBundle\Entity\CvFavorite", mappedBy="user")
     */
    protected $cvFavorites;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\ConversationBundle\Entity\Message", mappedBy="sender")
     */
    protected $sentMessages;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\SubscriptionBundle\Entity\Subscription", mappedBy="user")
     */
    protected $subscriptions;

    /**
     * @ORM\OneToMany(targetEntity="RDW\Bundle\ConversationBundle\Entity\Message", mappedBy="receiver")
     */
    protected $receivedMessages;

    /**
     * @var \RDW\Bundle\WalletBundle\Entity\Wallet
     *
     * @ORM\OneToOne(targetEntity="RDW\Bundle\WalletBundle\Entity\Wallet", mappedBy="user", cascade={"persist"})
     */
    protected $wallet;

    /**
     * @var bool
     * @ORM\Column(name="is_unlimited", type="boolean", nullable=false)
     */
    private $isUnlimited;

    /**
     * @var \DateTime
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="social_email")
     */
    private $socialEmail;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="facebook_id")
     */
    protected $facebookId;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="odnoklassniki_id")
     */
    protected $odnoklassnikiId;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="vkontakte_id")
     */
    protected $vkontakteId;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, name="mailru_id")
     */
    protected $mailruId;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $type;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->cities = new ArrayCollection();
        $this->sentConversations = new ArrayCollection();
        $this->receivedConversations = new ArrayCollection();
        $this->sentMessages = new ArrayCollection();
        $this->receivedMessages = new ArrayCollection();
        $this->jobFavorites = new ArrayCollection();
        $this->subscriptions = new ArrayCollection();
        $this->cvFavorites = new ArrayCollection();
        $this->jobApplies = new ArrayCollection();
        $this->cvMessages = new ArrayCollection();
        $this->isUnlimited = false;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @param City $city
     *
     * @return $this
     */
    public function addCity(City $city)
    {
        $this->cities->add($city);

        return $this;
    }

    /**
     * @param ArrayCollection $cities
     *
     * @return $this
     */
    public function setCities(ArrayCollection $cities)
    {
        $this->cities = $cities;

        return $this;
    }

    /**
     * @param City $city
     *
     * @return $this
     */
    public function removeCity(City $city)
    {
        $this->cities->removeElement($city);

        return $this;
    }

    /**
     * @param Conversation $conversation
     *
     * @return $this
     */
    public function removeReceivedConversation(Conversation $conversation)
    {
        $this->receivedConversations->removeElement($conversation);

        return $this;
    }

    /**
     * @param Conversation $conversation
     *
     * @return $this
     */
    public function removeSentConversation(Conversation $conversation)
    {
        $this->sentConversations->removeElement($conversation);

        return $this;
    }

    /**
     * @param Conversation $conversation
     *
     * @return $this
     */
    public function addSentConversation(Conversation $conversation)
    {
        $this->sentConversations[] = $conversation;

        return $this;
    }

    /**
     * @param Conversation $conversation
     *
     * @return $this
     */
    public function addReceivedConversation(Conversation $conversation)
    {
        $this->receivedConversations[] = $conversation;

        return $this;
    }

    /**
     * @param Subscription $subscription
     *
     * @return $this
     */
    public function addSubscription(Subscription $subscription)
    {
        $this->subscriptions->add($subscription);

        return $this;
    }

    /**
     * @param Subscription $subscription
     *
     * @return $this
     */
    public function removeSubscription(Subscription $subscription)
    {
        $this->subscriptions->remove($subscription);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getReceivedConversations()
    {
        return $this->receivedConversations;
    }

    /**
     * @return ArrayCollection
     */
    public function getSentConversations()
    {
        return $this->sentConversations;
    }

    /**
     * @param ArrayCollection $sentConversations
     *
     * @return $this
     */
    public function setSentConversations(ArrayCollection $sentConversations = null)
    {
        $this->sentConversations = $sentConversations;

        return $this;
    }

    /**
     * Add favorite
     *
     * @param JobFavorite $favorite
     *
     * @return $this
     */
    public function addJobFavorite(JobFavorite $favorite)
    {
        $this->jobFavorites->add($favorite);

        return $this;
    }

    /**
     * Remove favorites
     *
     * @param JobFavorite $favorite
     */
    public function removeJobFavorite(JobFavorite $favorite)
    {
        $this->jobFavorites->removeElement($favorite);
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $favorites
     *
     * @return $this
     */
    public function setJobFavorites(ArrayCollection $favorites)
    {
        $this->jobFavorites = $favorites;

        return $this;
    }

    /**
     * Get favorites
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJobFavorites()
    {
        return $this->jobFavorites;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getCvFavorites()
    {
        return $this->cvFavorites;
    }

    /**
     * @param \Doctrine\Common\Collections\ArrayCollection $cvFavorites
     *
     * @return $this
     */
    public function setCvFavorites(ArrayCollection $cvFavorites)
    {
        $this->cvFavorites = $cvFavorites;

        return $this;
    }

    /**
     * @param CvFavorite $cvFavorite
     *
     * @return $this
     */
    public function addCvFavorite(CvFavorite $cvFavorite)
    {
        $this->cvFavorites->add($cvFavorite);

        return $this;
    }

    /**
     * @param CvFavorite $cvFavorite
     *
     * @return $this
     */
    public function removeCvFavorite(CvFavorite $cvFavorite)
    {
        $this->jobFavorites->removeElement($cvFavorite);

        return $this;
    }

    /**
     * @param Message $message
     *
     * @return $this
     */
    public function addSentMessage(Message $message)
    {
        $this->sentMessages[] = $message;

        return $this;
    }

    /**
     * @param Message $message
     *
     * @return $this
     */
    public function removeSentMessage(Message $message)
    {
        $this->sentMessages->removeElement($message);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getSentMessages()
    {
        return $this->sentMessages;
    }

    /**
     * @param ArrayCollection $sentMessages
     *
     * @return $this
     */
    public function setSentMessages(ArrayCollection $sentMessages = null)
    {
        $this->sentMessages = $sentMessages;

        return $this;
    }

    /**
     * @param Message $message
     *
     * @return $this
     */
    public function addReceivedMessage(Message $message)
    {
        $this->receivedMessages[] = $message;

        return $this;
    }

    /**
     * @param Message $message
     *
     * @return $this
     */
    public function removeReceivedMessage(Message $message)
    {
        $this->receivedMessages->removeElement($message);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getReceivedMessages()
    {
        return $this->receivedMessages;
    }

    /**
     * @return Wallet
     */
    public function getWallet()
    {
        return $this->wallet;
    }

    /**
     * @param Wallet $wallet
     *
     * @return $this
     */
    public function setWallet(Wallet $wallet)
    {
        $this->wallet = $wallet;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsUnlimited()
    {
        return $this->isUnlimited;
    }

    /**
     * @param boolean $isUnlimited
     *
     * @return $this
     */
    public function setIsUnlimited($isUnlimited)
    {
        $this->isUnlimited = $isUnlimited;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getJobApplies()
    {
        return $this->jobApplies;
    }

    /**
     * @param ArrayCollection $jobApplies
     *
     * @return $this
     */
    public function setJobApplies($jobApplies)
    {
        $this->jobApplies = $jobApplies;

        return $this;
    }

    /**
     * @param JobApply $apply
     *
     * @return $this
     */
    public function addJobApply(JobApply $apply)
    {
        $this->jobApplies->add($apply);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getSubscriptions()
    {
        return $this->subscriptions;
    }

    /**
     * @param mixed $subscriptions
     *
     * @return $this
     */
    public function setSubscriptions($subscriptions)
    {
        $this->subscriptions = $subscriptions;

        return $this;
    }

    /**
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * @param string $facebookId
     *
     * @return $this
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    /**
     * @return string
     */
    public function getOdnoklassnikiId()
    {
        return $this->odnoklassnikiId;
    }

    /**
     * @param string $odnoklassnikiId
     *
     * @return $this
     */
    public function setOdnoklassnikiId($odnoklassnikiId)
    {
        $this->odnoklassnikiId = $odnoklassnikiId;

        return $this;
    }

    /**
     * @return string
     */
    public function getVkontakteId()
    {
        return $this->vkontakteId;
    }

    /**
     * @param string $vkontakteId
     *
     * @return $this
     */
    public function setVkontakteId($vkontakteId)
    {
        $this->vkontakteId = $vkontakteId;

        return $this;
    }

    /**
     * @return string
     */
    public function getMailruId()
    {
        return $this->mailruId;
    }

    /**
     * @param string $mailruId
     *
     * @return $this
     */
    public function setMailruId($mailruId)
    {
        $this->mailruId = $mailruId;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * @return ArrayCollection
     */
    public function getCvMessages()
    {
        return $this->cvMessages;
    }

    /**
     * @param ArrayCollection $cvMessages
     */
    public function setCvMessages($cvMessages)
    {
        $this->cvMessages = $cvMessages;
    }

    /**
     * @param CvMessage $cvMessage
     *
     * @return $this
     */
    public function addCvMessage($cvMessage)
    {
        if ($cvMessage && ! $this->cvMessages->contains($cvMessage)) {
            $this->cvMessages->add($cvMessage);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getSocialEmail()
    {
        return $this->socialEmail;
    }

    /**
     * @param string $socialEmail
     */
    public function setSocialEmail($socialEmail)
    {
        $this->socialEmail = $socialEmail;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * @param string $type
     *
     * @return self
     */
    public function setType($type)
    {
        if (in_array($type, $this->getAllTypes())) {
            $this->type = $type;
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getAllTypes()
    {
        return [
            self::USER_TYPE_EMPLOYEE,
            self::USER_TYPE_EMPLOYER,
        ];
    }

    /**
     * @param string $token
     *
     * @return bool
     */
    public function isTokenCorrect($token)
    {
        return ($this->getConfirmationToken() == $token);
    }
}
