<?php

namespace RDW\Bundle\UserBundle\Entity;

use RDW\Bundle\AppBundle\Entity\AdInterface;

/**
 * interface for Favorites
 */
interface Favorite
{
    /**
     * returns User
     */
    public function getUser();

    /**
     * set Date when Favorite was archived
     *
     * @param \DateTime $date
     *
     * @return self
     */
    public function setArchivedAt(\DateTime $date);

    /**
     * @param UserInterface $user
     *
     * @return bool
     */
    public function isOwner(UserInterface $user);

    /**
     * @param AdInterface $ad
     *
     * @return self
     */
    public function setAd(AdInterface $ad);

    /**
     * @return AdInterface
     */
    public function getAd();
}
