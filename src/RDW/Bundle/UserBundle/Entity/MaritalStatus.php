<?php

namespace RDW\Bundle\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\ManageBundle\Entity\Filter;

/**
 * Class MaritalStatus
 *
 * @package RDW\Bundle\UserBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="marital_statuses")
 */
class MaritalStatus extends Filter
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\CvBundle\Entity\Cv", mappedBy="maritalStatus", fetch="EXTRA_LAZY")
     */
    protected $cvs;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\UserBundle\Entity\RegisteredUser", mappedBy="familyStatus", fetch="EXTRA_LAZY")
     */
    protected $users;

    /**
     * constructor
     */
    public function __construct()
    {
        $this->cvs = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function isDeletable()
    {
        return ! ($this->cvs->count() > 0 || $this->users->count() > 0 || $this->isSystem());
    }
}
