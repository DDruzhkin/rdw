<?php

namespace RDW\Bundle\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\ManageBundle\Entity\Filter;
use RDW\Bundle\SlugBundle\Annotations as Slugifier;

/**
 * Class Scope
 *
 * @package RDW\Bundle\UserBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="scopes")
 * @Slugifier\Slug(fields={"title"})
 */
class Scope extends Filter
{
    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\UserBundle\Entity\RegisteredUser", mappedBy="scope", fetch="EXTRA_LAZY")
     */
    protected $users;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\JobBundle\Entity\Job", mappedBy="scope", fetch="EXTRA_LAZY")
     */
    protected $jobs;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\CvBundle\Entity\Cv", mappedBy="scope", fetch="EXTRA_LAZY")
     */
    protected $cvs;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="RDW\Bundle\SubscriptionBundle\Entity\Subscription", mappedBy="scopes", fetch="EXTRA_LAZY")
     */
    protected $subscriptions;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     */
    protected $entitiesCount;

    /**
     * constructor
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->jobs = new ArrayCollection();
        $this->cvs = new ArrayCollection();
        $this->subscriptions = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function isDeletable()
    {
        return ! ($this->jobs->count() > 0 || $this->cvs->count() > 0 || $this->subscriptions->count() > 0 || $this->users->count() > 0 || $this->isSystem());
    }

    public function setEntitiesCount($entitiesCount)
    {
        $this->entitiesCount = $entitiesCount;

        return $this;
    }

    public function getEntitiesCount()
    {
        return $this->entitiesCount;
    }
}
