<?php

namespace RDW\Bundle\UserBundle\Entity;

/**
 * Interface UserInterface
 *
 * @package RDW\Bundle\UserBundle\Entity
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
interface UserInterface
{
    /**
     * @return int
     */
    public function getId();
}
