<?php

namespace RDW\Bundle\UserBundle\Model;

use RDW\Bundle\UserBundle\Entity\User;

/**
 * Class FavoriteFilter
 *
 * @package RDW\Bundle\UserBundle\Model
 */
class FavoriteFilter
{
    const TYPE_CV = 'cv';
    const TYPE_JOB = 'job';

    /**
     * @var User
     */
    protected $user;

    /**
     * @var bool
     */
    protected $archivedVisible;

    /**
     * @var string
     */
    protected $type;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->archivedVisible = false;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isArchivedVisible()
    {
        return $this->archivedVisible;
    }

    /**
     * @param boolean $archivedVisible
     *
     * @return $this
     */
    public function setArchivedVisible($archivedVisible)
    {
        $this->archivedVisible = $archivedVisible;

        return $this;
    }
}
