<?php

namespace RDW\Bundle\UserBundle\Model;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class FavoriteList
 *
 * @package RDW\Bundle\UserBundle\Model
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class FavoriteList
{
    /**
     * @var ArrayCollection
     */
    private $favorites;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->favorites = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getFavorites()
    {
        return $this->favorites;
    }

    /**
     * @param ArrayCollection $favorites
     *
     * @return $this
     */
    public function setFavorites($favorites)
    {
        $this->favorites = $favorites;

        return $this;
    }
}
