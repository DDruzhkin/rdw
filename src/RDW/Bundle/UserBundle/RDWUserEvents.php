<?php

namespace RDW\Bundle\UserBundle;

/**
 * Contains all events thrown in the RDWUserBundle
 *
 * Class RDWUserEvents
 *
 * @package User
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
final class RDWUserEvents
{
    /**
     * The ASK_QUESTION_SUCCESS event occurs when the site visitor successfully ask question in FAQ's page
     */
    const REGISTRATION_SUCCESS = 'rdw_user.event.registration_success';

    /**
     * The CHANGE_EMAIL_SUCCESS event occurs when the logged in user successfully submits change email form
     */
    const CHANGE_EMAIL_SUCCESS = 'rdw_user.event.change_email_success';

    /**
     * The CONFIRM_CHANGED_EMAIL_SUCCESS event occurs when user confirms changed email
     */
    const CONFIRM_CHANGED_EMAIL_SUCCESS = 'rdw_user.event.confirm_changed_email_success';

    /**
     * The BLOCK_USER event occurs when admin successfully submits block user form
     */
    const BLOCK_USER = 'rdw_user.event.block_user';

    /**
     * The GENERATE_NEW_PASSWORD_SUCCESS event occurs when new password is generated
     */
    const GENERATE_NEW_PASSWORD_SUCCESS = 'rdw_user.event.generate_new_password_success';

    /**
     * The GENERATE_NEW_PASSWORD event occurs when manager confirms generate new password for user
     */
    const GENERATE_NEW_PASSWORD = 'rdw_user.event.generate_new_password';

    /**
     *
     */
    const UPDATE_USER_PROFILE_PERCENT = 'rdw_user.event.update_user_profile';

    /**
     * The PRE_CREATE event occurs when user is created, but not persisted
     */
    const PRE_CREATE = 'rdw_user.event.pre_create';

    /**
     * The POST_CREATE event occurs after the user was persisted
     */
    const POST_CREATE = 'rdw_user.event.post_create';

    /**
     * The FINISH_O_AUTH_SUCCESS event occurs when the Visitor successfully submits oauth registration form
     */
    const FINISH_O_AUTH_SUCCESS = 'rdw_user.event.finish_o_auth_success';

    /**
     * The CONFIRMATION_O_AUTH_SUCCESS event occurs when the Visitor successfully confirms email account for social registration
     */
    const CONFIRMATION_O_AUTH_SUCCESS = 'rdw_user.event.confirmation_o_auth_success';

    /**
     * The CONFIRMATION_O_AUTH_FINISHED event occurs when system saves new user registered from social account
     */
    const CONFIRMATION_O_AUTH_FINISHED = 'rdw_user.event.confirmation_o_auth_finished';

    /**
     * The PRE_REMOVE event occurs when user is removed, but not persisted
     */
    const PRE_REMOVE = 'rdw_user.event.pre_remove';
}
