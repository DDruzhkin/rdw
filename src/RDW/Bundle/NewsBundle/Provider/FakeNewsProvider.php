<?php

namespace RDW\Bundle\NewsBundle\Provider;

use RDW\Bundle\NewsBundle\Model\NewsModel;
use RDW\Bundle\NewsBundle\Model\NewsProviderInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class FakeNewsProvider implements NewsProviderInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @inheritDoc
     */
    public function isActive()
    {
        return $this->container->get('kernel')->getEnvironment() !== 'prod';
    }

    /**
     * @inheritDoc
     */
    public function getNews($limit)
    {
        $limit = $limit ?: 9;

        $data = [];
        for ($i = 0; $i < $limit; $i++) {
            $data[] = (new NewsModel())
                ->setTitle(uniqid('TITLE_'))
                ->setContent(uniqid('CONTENT_'))
                ->setLink('https://rdw.by')
                ->setImageUrl('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII=');
        }

        return $data;
    }

    /**
     * @inheritDoc
     */
    public function getAlias()
    {
        return 'fake_news';
    }
}
