<?php

namespace RDW\Bundle\NewsBundle\Provider;

use Doctrine\Common\Cache\CacheProvider;
use RDW\Bundle\NewsBundle\Model\NewsModel;
use RDW\Bundle\NewsBundle\Model\NewsProviderInterface;

class TumblrRssNewsProvider implements NewsProviderInterface
{
    const CACHE_KEY = 'tumblr_rss';

    const CACHE_TTL = 3600;

    const COUNT_SYMBOLS_CONTENT = 130;

    /** @var CacheProvider */
    protected $cacheProvider;

    /** @var string */
    protected $rssUrl;

    /**
     * @inheritDoc
     */
    public function __construct($rssUrl, CacheProvider $cacheProvider)
    {
        $this->rssUrl = $rssUrl;
        $this->cacheProvider = $cacheProvider;
    }

    /**
     * @inheritDoc
     */
    public function getAlias()
    {
        return 'tumblr_rss';
    }

    /**
     * @inheritDoc
     */
    public function isActive()
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getNews($limit)
    {
        $items = $this->fetchRss();
        $data = [];
        foreach ($items as $item) {
            $data[] = (new NewsModel())
                ->setTitle($item->title)
                ->setContent($this->prepareContent($item->description))
                ->setImageUrl($this->getImageFromContent($item->description))
                ->setLink($item->link)
                ->setSource($this->getAlias());
            if (count($data) == $limit) {
                break;
            }
        }

        return $data;
    }

    /**
     * @return array
     */
    protected function fetchRss()
    {
        $cacheKey = sprintf('%s-%s', self::CACHE_KEY, 'rss_data');
        $data = $this->cacheProvider->fetch($cacheKey);
        if (false === $data) {
            $data = [];
            $client = new \GuzzleHttp\Client();
            $res = $client->request('GET', $this->rssUrl);
            if ($res->getStatusCode() === 200) {
                $rssData = json_decode(json_encode(simplexml_load_string($res->getBody()->getContents())));
                $data = $rssData->channel->item;
            }
            $this->cacheProvider->save($cacheKey, serialize($data), self::CACHE_TTL);
        } else {
            $data = unserialize($data);
        }

        return $data;
    }

    /**
     * @param $description
     *
     * @return string
     */
    public function prepareContent($description)
    {
        $description = mb_substr(strip_tags($description), 0, self::COUNT_SYMBOLS_CONTENT) . "...";

        return $description;
    }

    /**
     * @param $description
     *
     * @return string
     */
    public function getImageFromContent($description)
    {
        preg_match_all('/\< *[img][^\>]*[src] *= *[\"\']{0,1}([^\"\']*)/i', $description, $result);

        if (count($result[1])) {
            return $result[1][0];
        }

        return "";
    }
}
