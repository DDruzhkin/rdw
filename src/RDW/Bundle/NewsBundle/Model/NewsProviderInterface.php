<?php

namespace RDW\Bundle\NewsBundle\Model;

interface NewsProviderInterface
{
    /**
     * @return string
     */
    public function getAlias();

    /**
     * @return bool
     */
    public function isActive();

    /**
     * Returns array of news
     *
     * @param int $limit
     *
     * @return array|NewsModel[]
     */
    public function getNews($limit);
}
