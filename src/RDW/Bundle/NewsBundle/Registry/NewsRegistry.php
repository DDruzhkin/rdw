<?php

namespace RDW\Bundle\NewsBundle\Registry;

use Doctrine\Common\Cache\CacheProvider;
use RDW\Bundle\NewsBundle\Model\NewsModel;
use RDW\Bundle\NewsBundle\Model\NewsProviderInterface;

class NewsRegistry
{
    const CACHE_KEY = 'rdw_news';

    const CACHE_TTL = 3600;

    /** @var array|NewsProviderInterface[] */
    protected $providers = [];

    /** @var CacheProvider */
    protected $cacheProvider;

    /**
     * NewsRegistry constructor.
     *
     * @param CacheProvider $cacheProvider
     */
    public function __construct(CacheProvider $cacheProvider)
    {
        $this->cacheProvider = $cacheProvider;
    }

    /**
     * @param NewsProviderInterface $newsProvider
     * @param $alias
     */
    public function addProvider(NewsProviderInterface $newsProvider, $alias)
    {
        $this->providers[$alias] = $newsProvider;
    }

    /**
     * @param int $limit
     *
     * @return array|NewsModel[]
     */
    public function getNews($limit = 9)
    {
        $cacheKey = sprintf('%s-%s', self::CACHE_KEY, $limit);

        $news = $this->cacheProvider->fetch($cacheKey);

        if (false === $news) {
            $news = $this->obtainNews($limit);
            $this->cacheProvider->save($cacheKey, serialize($news), self::CACHE_TTL);
        }else{
            $news = unserialize($news);
        }

        return $news;
    }


    /**
     * @param int $limit
     *
     * @return array|NewsModel[]
     */
    protected function obtainNews($limit)
    {
        $data = [];

        foreach ($this->providers as $provider) {
            if ($provider->isActive()) {
                $news = $provider->getNews($limit - count($data));
                foreach ($news as $item) {
                    $item->setSource($provider->getAlias());
                }
                $data = array_merge($data, $news);

                if (null != $limit && count($data) === $limit) {
                    break;
                }
            }
        }

        return $data;
    }
}
