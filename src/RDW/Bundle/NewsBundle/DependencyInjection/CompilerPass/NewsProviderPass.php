<?php

namespace RDW\Bundle\NewsBundle\DependencyInjection\CompilerPass;

use RDW\Component\DependencyInjection\Compiler\TaggedServicesCompilerPassTrait;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class NewsProviderPass implements CompilerPassInterface
{
    use TaggedServicesCompilerPassTrait;

    const SERVICE_TAG = 'rdw_news.data_provider';
    const REGISTRY_SERVICE_NAME = 'rdw_news.registry';

    /**
     * {@inheritDoc}
     */
    public function process(ContainerBuilder $container)
    {
        $this->registerTaggedServices($container, self::REGISTRY_SERVICE_NAME, self::SERVICE_TAG, 'addProvider');
    }
}
