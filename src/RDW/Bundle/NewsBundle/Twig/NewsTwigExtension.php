<?php

namespace RDW\Bundle\NewsBundle\Twig;

use RDW\Bundle\NewsBundle\Registry\NewsRegistry;

class NewsTwigExtension extends \Twig_Extension
{
    /** @var NewsRegistry */
    protected $newsRegistry;

    /**
     * NewsTwigExtension constructor.
     *
     * @param NewsRegistry $newsRegistry
     */
    public function __construct(NewsRegistry $newsRegistry)
    {
        $this->newsRegistry = $newsRegistry;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('rdw_get_news', [$this->newsRegistry, 'getNews'])
        ];
    }
}
