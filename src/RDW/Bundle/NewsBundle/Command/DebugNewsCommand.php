<?php

namespace RDW\Bundle\NewsBundle\Command;

use RDW\Bundle\NewsBundle\Registry\NewsRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DebugNewsCommand extends Command
{
    /** @var NewsRegistry */
    protected $newsRegistry;

    /**
     * @param null|string $name
     * @param NewsRegistry $newsRegistry
     */
    public function __construct($name, NewsRegistry $newsRegistry)
    {
        parent::__construct($name);

        $this->newsRegistry = $newsRegistry;
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('rdw:debug:news')
            ->addOption('count', 'c', InputOption::VALUE_OPTIONAL, '', 9);
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $data = $this->newsRegistry->getNews($input->getOption('count'));
        $table = new Table($output);

        $table->setHeaders(['Title', 'Link', 'Source'])->setRows([]);

        foreach ($data as $news) {
            $table->addRow([
                $news->getTitle(),
                $news->getLink(),
                $news->getSource(),
            ]);
        }

        $table->render();
    }

}
