<?php

namespace RDW\Bundle\ContentBundle\Repository;

use Doctrine\ORM\EntityRepository;
use RDW\Bundle\ContentBundle\Entity\Text;

/**
 * Class TextRepository
 *
 * @package RDW\Bundle\ContentBundle\Repository
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class TextRepository extends EntityRepository
{
    /**
     * @return \Doctrine\ORM\Query
     */
    public function getListQuery()
    {
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder
            ->select('t')
            ->from('RDWContentBundle:Text', 't')
            ->andWhere('t.status != :status')
            ->setParameter('status', Text::STATUS_DELETED)
            ->orderBy('t.id', 'DESC')
        ;

        return $queryBuilder->getQuery();
    }
}
