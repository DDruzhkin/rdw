<?php

namespace RDW\Bundle\ContentBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;
use RDW\Bundle\ContentBundle\Entity\Text;
use RDW\Bundle\ContentBundle\Repository\TextRepository;

/**
 * Class TextManager
 *
 * @package RDW\Bundle\ContentBundle\Service
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class TextManager
{
    /**
     * @var ObjectManager
     */
    private $entityManager;

    /**
     * @var \Knp\Component\Pager\Paginator
     */
    private $paginator;

    /**
     * @param ObjectManager $entityManager
     * @param Paginator $paginator
     */
    public function __construct(ObjectManager $entityManager, Paginator $paginator)
    {
        $this->entityManager = $entityManager;
        $this->paginator = $paginator;
    }

    /**
     * @return Text
     */
    public function create()
    {
        return new Text();
    }

    /**
     * @param int $page
     * @param int $limit
     *
     * @return PaginationInterface
     */
    public function getPaginatedList($page, $limit)
    {
        $query = $this->getRepository()->getListQuery();

        $pagination = $this->paginator->paginate($query, $page, $limit);

        return $pagination;
    }

    /**
     * @param int $id
     *
     * @return Text|object
     */
    public function findById($id)
    {
        return $this->getRepository()->find($id);
    }

    /**
     * @param Text $text
     *
     * @return bool
     */
    public function delete(Text $text)
    {
        $text->setStatus(Text::STATUS_DELETED);
        $this->update($text);

        return true;
    }

    /**
     * @param Text $text
     * @param bool $andFlush
     *
     * @return bool
     */
    public function update(Text $text, $andFlush = true)
    {
        $this->entityManager->persist($text);

        if ($andFlush) {
            $this->entityManager->flush();
        }

        return true;
    }

    /**
     * @param array $criteria
     *
     * @return null|Text|object
     */
    public function getActiveTextBy(array $criteria)
    {
        $criteria['status'] = Text::STATUS_ACTIVE;

        return $this->getRepository()->findOneBy($criteria);
    }

    /**
     * @return TextRepository
     */
    private function getRepository()
    {
        return $this->entityManager->getRepository(Text::class);
    }
}
