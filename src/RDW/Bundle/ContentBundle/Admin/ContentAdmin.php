<?php

namespace RDW\Bundle\ContentBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use RDW\Bundle\ContentBundle\Entity\Text;

class ContentAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('user', 'text', array(
                'read_only' => true,
                'mapped' => false,
                'label' => 'Username',
            ))
            ->add('status', 'choice', array(
                'choices'   => Text::getStatuses(),
                'required' => true,
                'label' => 'Status',
            ))
            ->add('type', 'choice', array(
                'choices'   => Text::getTypesArray(),
                'required' => true,
                'label' => 'Type',
            ))
            ->add('title', 'text', array(
                'required' => true,
                'label' => 'Title',
            ))
            ->add('shortTitle', 'text', array(
                'required' => true,
                'label' => 'Short title / meta title',
            ))
            ->add('content', 'ckeditor', array(
                'required' => true,
                'label' => 'Content',
            ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        //$datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('title')
            ->add("shortTitle")
            ->add("type", 'choice', [
                'choices' => Text::getTypesArray(),
            ])
            ->add("status", 'choice', array(
                'choices' => array('inactive' => 'Не размещенa', 'active' => 'Размещена', 'blocked' => 'Заблокированные'),
            ));

        $listMapper->add('_action', 'actions', array('actions' => array(
            'edit'       => array(),
            'delete'     => array('label' => 'delete')
        )));
    }
}