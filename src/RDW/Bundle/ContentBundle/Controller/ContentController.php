<?php

namespace RDW\Bundle\ContentBundle\Controller;

use RDW\Bundle\ContentBundle\Entity\Text;
use RDW\Bundle\ContentBundle\Service\TextManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ContentController
 *
 * @package RDW\Bundle\ContentBundle\Controller
 */
class ContentController extends Controller
{
    /**
     * @param int $id
     *
     * @Route("/text/{id}", requirements={"textId"="\d+"}, name="rdw_content.content.index")
     * @Method({"GET"})
     * @Template()
     *
     * @return array
     * @throws NotFoundHttpException
     */
    public function indexAction($id)
    {
        $text = $this->getTextManager()->getActiveTextBy(['id' => $id]);

        if (! $text) {
            throw new NotFoundHttpException(sprintf('The text with id %s was not found.', $id));
        }

        return [
            'text' => $text,
        ];
    }

    /**
     * @param string $type
     *
     * @Route("/modal/{type}", name="rdw_content.content.modal")
     * @Method({"GET"})
     * @Template()
     *
     * @return array
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function modalAction($type)
    {
        $text = $this->getTextManager()->getActiveTextBy(['type' => $type]);

        if (! $text) {
            throw new NotFoundHttpException(sprintf('The text with type %s was not found.', $type));
        }

        return [
            'text' => $text,
        ];
    }

    /**
     * @param Text $text
     *
     * @Route("/modal-text/{id}", requirements={"id"="\d+"}, name="rdw_content.content.modal_text")
     * @Method({"GET"})
     * @Template("RDWContentBundle:Content:modal.html.twig")
     *
     * @return Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function modalTextAction(Text $text)
    {
        return [
            'text' => $text,
        ];
    }

    /**
     * @param string $type
     *
     * @param $template
     *
     * @return array|Response
     * @throws NotFoundHttpException
     * @throws \LogicException
     * @Route("/text/{type}/{template}", name="rdw_content.content.text")
     * @Template()
     *
     */
    public function getTextAction($type, $template)
    {
        $text = $this->getTextManager()->getActiveTextBy(['type' => $type]);

        if (!$text) {
            throw new NotFoundHttpException(sprintf('The text with type %s was not found.', $type));
        }

        return $this->render('RDWContentBundle:Content:' . $template . '', ['text' => $text]);
    }


    /**
     * @return TextManager
     */
    private function getTextManager()
    {
        return $this->container->get('rdw_content.service.text_manager');
    }
}
