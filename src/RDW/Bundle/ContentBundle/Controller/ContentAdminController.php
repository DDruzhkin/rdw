<?php

namespace RDW\Bundle\ContentBundle\Controller;

use RDW\Bundle\ContentBundle\Form\Type\TextType;
use RDW\Bundle\ContentBundle\Service\TextManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class ContentAdminController
 *
 * @package RDW\Bundle\ContentBundle\Controller
 *
 * @Route("/manage/content")
 *
 */
class ContentAdminController extends Controller
{
    /**
     * @Route("/list", name="rdw.content.manage.list")
     * @Template()
     *
     * @return array
     */
    public function listAction()
    {
        $this->isGrantedLocal();

        $pagination = $this->getTextManager()->getPaginatedList(
            $this->get('request')->query->get('page', 1),
            $this->container->getParameter('rdw_content.manage.text.paginator.per_page')
        );

        return [
            'pagination' => $pagination,
        ];
    }

    /**
     * @param Request $request
     *
     * @Route("/create", name="rdw.content.manage.create")
     * @Template()
     *
     * @return array|RedirectResponse
     */
    public function createAction(Request $request)
    {
        $this->isGrantedLocal();

        $text = $this->getTextManager()->create();
        $text->setUser($this->getUser());

        $form = $this->createForm(new TextType(), $text);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getTextManager()->update($form->getData());

            $this->get('braincrafted_bootstrap.flash')->success('Text page successfully created');

            return $this->redirect($this->get('router')->generate('rdw.content.manage.list'));
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @param Request $request
     * @param int     $textId
     *
     * @Route("/edit/{textId}", requirements={"textId" = "\d+"}, name="rdw.content.manage.edit")
     * @Template()
     *
     * @return array|RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function editAction(Request $request, $textId)
    {
        $this->isGrantedLocal();

        $text = $this->getTextManager()->findById($textId);

        if (! $text) {
            throw new NotFoundHttpException(printf("Text page with id %d was not found.", $textId));
        }

        $form = $this->createForm(new TextType(), $text);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getTextManager()->update($form->getData());

            $this->get('braincrafted_bootstrap.flash')->success('Text page successfully updated');

            return $this->redirect($this->get('router')->generate('rdw.content.manage.list'));
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @param int $textId
     *
     * @Route("/delete/{textId}", requirements={"textId" = "\d+"}, name="rdw.content.manage.delete")
     *
     * @return RedirectResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteAction($textId)
    {
        $this->isGrantedLocal();

        $text = $this->getTextManager()->findById($textId);

        if (! $text) {
            throw new NotFoundHttpException(sprintf('The text page with id "%d" does not exist.', $textId));
        }

        $this->getTextManager()->delete($text);

        return new RedirectResponse($this->get('router')->generate('rdw.content.manage.list'));
    }

    /**
     * @return TextManager
     */
    private function getTextManager()
    {
        return $this->container->get('rdw_content.service.text_manager');
    }

    /**
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    private function isGrantedLocal()
    {
        if (! $this->container->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }
    }
}
