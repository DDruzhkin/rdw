<?php

namespace RDW\Bundle\ContentBundle\Behat;

use Behat\Gherkin\Node\TableNode;
use RDW\Bundle\AppBundle\Behat\DefaultContext;
use RDW\Bundle\ContentBundle\Entity\Text;
use RDW\Bundle\ManageBundle\Entity\User;

/**
 * Class ContentContext
 *
 * @package RDW\Bundle\ContentBundle\Behat
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class ContentContext extends DefaultContext
{
    /**
     * @Given /^I am on the text list page$/
     */
    public function iAmOnTheTextListPage()
    {
        $this->getSession()->visit($this->generatePageUrl('rdw.content.manage.list'));
    }

    /**
     * @Given /^I should be on the text list page$/
     */
    public function iShouldBeOnTheTextListPage()
    {
        $this->assertSession()->addressEquals($this->generatePageUrl('rdw.content.manage.list'));
    }

    /**
     * @Given /^I am on the text create page$/
     */
    public function iAmOnTheTextCreatePage()
    {
        $this->getSession()->visit($this->generatePageUrl('rdw.content.manage.create'));
    }

    /**
     * @Given /^I should be on the text create page$/
     */
    public function iShouldBeOnTheTextEditPage()
    {
        $this->assertSession()->addressEquals($this->generatePageUrl('rdw.content.manage.create'));
    }

    /**
     * @param string $title
     *
     * @Given /^I am on the text page "([^""]*)" editing page$/
     */
    public function iAmOnTheTextEditPage($title)
    {
        $text = $this->getEntityManager()->getRepository('RDWContentBundle:Text')->findOneBy(array('title' => $title));

        $this->getSession()->visit($this->generateUrl('rdw.content.manage.edit', array('textId' => $text->getId(), true)));
    }

    /**
     * @param TableNode $table
     *
     * @Given /^there are following text pages:$/
     */
    public function thereAreFollowingTextPages(TableNode $table)
    {
        foreach ($table->getHash() as $data) {
            $this->thereIsText(
                $this->getUserManager()->findByUsername('admin@example.com'),
                isset($data['status']) ? $data['status'] : Text::STATUS_INACTIVE,
                isset($data['title']) ? $data['title'] : $this->faker->words(),
                isset($data['short_title']) ? $data['short_title'] : $this->faker->word,
                isset($data['content']) ? $data['content'] : $this->faker->text(),
                false
            );
        }

        $this->getEntityManager()->flush();
    }

    /**
     * @param User   $user
     * @param string $status
     * @param string $title
     * @param string $shortTitle
     * @param string $content
     * @param bool   $flush
     *
     * @Given /^there is text page:$/
     *
     * @return Text
     */
    public function thereIsText(User $user, $status, $title, $shortTitle, $content, $flush = true)
    {
        /** @var Text $text */
        $text = $this->getTextManager()->create();

        $text->setUser($user)
            ->setStatus($status)
            ->setTitle($title)
            ->setShortTitle($shortTitle)
            ->setContent($content);

        $this->getEntityManager()->persist($text);

        if ($flush) {
            $this->getEntityManager()->flush();
        }

        return $text;
    }

    /**
     * @return \RDW\Bundle\ManageBundle\Service\UserManager
     */
    protected function getUserManager()
    {
        return $this->getService('rdw_manage.service.user_manager');
    }

    /**
     * @return \RDW\Bundle\ContentBundle\Service\TextManager
     */
    protected function getTextManager()
    {
        return $this->getService('rdw_content.service.text_manager');
    }
}