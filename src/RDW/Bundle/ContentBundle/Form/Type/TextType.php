<?php

namespace RDW\Bundle\ContentBundle\Form\Type;

use RDW\Bundle\ContentBundle\Entity\Text;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class TextType
 *
 * @package RDW\Bundle\ContentBundle\Form\Type
 * @author  Giedrius Sabaliauskas <giedrius@eface.lt>
 */
class TextType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Text $text */
        $text = $builder->getForm()->getData();

        $builder
            ->add('author', 'text', array(
                'read_only' => true,
                'mapped' => false,
                'data' => ($text->getUser()) ? $text->getUser()->getUsername() : null,
                'label' => 'Username',
            ))
            ->add('status', 'choice', array(
                'choices'   => Text::getStatuses(),
                'required' => true,
                'label' => 'Status',
            ))
            ->add('type', 'choice', array(
                'choices'   => Text::getTypesArray(),
                'required' => true,
                'label' => 'Type',
            ))
            ->add('title', 'text', array(
                'required' => true,
                'label' => 'Title',
            ))
            ->add('shortTitle', 'text', array(
                'required' => true,
                'label' => 'Short title / meta title',
            ))
            ->add('content', 'ckeditor', array(
                'required' => true,
                'label' => 'Content',
            ))
            ->add('submit', 'submit', array(
                'label' => 'Save'
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RDW\Bundle\ContentBundle\Entity\Text',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_content_manage_text';
    }
}
