<?php

namespace RDW\Bundle\ContentBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\ContentBundle\Entity\Text;

class LoadContentData extends AbstractFixture
{
    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $text = new Text();

        $text->setType('main_page')
            ->setStatus(Text::STATUS_ACTIVE)
            ->setShortTitle('Short title')
            ->setContent('Content')
            ->setTitle('Title');

        $manager->persist($text);
        $manager->flush();
    }
}
