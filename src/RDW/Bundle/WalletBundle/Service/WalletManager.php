<?php

namespace RDW\Bundle\WalletBundle\Service;

use Doctrine\Common\Persistence\ObjectManager;
use RDW\Bundle\UserBundle\Entity\RegisteredUser;
use RDW\Bundle\UserBundle\Entity\User;
use RDW\Bundle\WalletBundle\Entity\Wallet;
use RDW\Bundle\WalletBundle\Entity\WalletOperation;
use RDW\Bundle\WalletBundle\Repository\WalletRepository;


/**
 * Class WalletManager
 *
 * @package RDW\Bundle\WalletBundle\Service
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class WalletManager
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param RegisteredUser $user
     * @param double         $amount
     * @param string         $purpose
     *
     * @return bool
     */
    public function add(RegisteredUser $user, $amount, $purpose)
    {
        $userWallet = $user->getWallet();
        $wallet = ($userWallet instanceof Wallet) ? $userWallet : $this->createWallet($user);

        $walletOperation = new WalletOperation();
        $walletOperation->setAmount($amount);
        $walletOperation->setTitle($purpose);
        $walletOperation->setWallet($wallet);

        $this->objectManager->persist($walletOperation);
        $this->objectManager->flush();

        return true;
    }

    /**
     * @param User $user
     *
     * @return Wallet
     */
    public function createWallet(User $user)
    {
        $wallet = new Wallet();
        $wallet->setUser($user);

        $this->objectManager->persist($wallet);
        $this->objectManager->flush();

        $user->setWallet($wallet);

        return $wallet;
    }

    /**
     * @param User $user
     *
     * @return int
     */
    public function getUserBalance(User $user)
    {
        /** @var $repository WalletRepository */
        $repository = $this->objectManager->getRepository('RDWWalletBundle:Wallet');

        return $repository->getUserBalance($user);
    }
}
