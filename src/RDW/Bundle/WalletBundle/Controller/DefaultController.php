<?php

namespace RDW\Bundle\WalletBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('RDWWalletBundle:Default:index.html.twig', array('name' => $name));
    }
}
