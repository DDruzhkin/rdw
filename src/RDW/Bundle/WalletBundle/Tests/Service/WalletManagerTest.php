<?php
/**
 * Created by PhpStorm.
 * User: vb
 * Date: 1/28/15
 * Time: 8:16 AM
 */

namespace RDW\Bundle\WalletBundle\Tests\Service;

use RDW\Bundle\WalletBundle\Entity\Wallet;
use RDW\Bundle\WalletBundle\Entity\WalletOperation;
use RDW\Bundle\WalletBundle\Service\WalletManager;

/**
 * Class WalletManagerTest
 *
 * @package RDW\Bundle\WalletBundle\Tests\Service
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class WalletManagerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var WalletManager
     */
    private $walletManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $em;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $user;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $repo;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $wallet;

    /**
     * Set up mocks and test object
     */
    public function setUp()
    {
        $this->em = $this
            ->getMockBuilder('Doctrine\Common\Persistence\ObjectManager')
            ->disableOriginalConstructor()
            ->getMock();

        $this->repo = $this
            ->getMockBuilder('RDW\Bundle\WalletBundle\Repository\WalletRepository')
            ->disableOriginalConstructor()
            ->getMock();

        $this->em->expects($this->any())
            ->method('getRepository')
            ->with('RDWWalletBundle:Wallet')
            ->willReturn($this->repo);

        $this->user = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');
        $this->wallet = $this->getMock('RDW\Bundle\WalletBundle\Entity\Wallet');
        $this->walletManager = new WalletManager($this->em);
    }

    /**
     * @test
     */
    public function it_should_get_user_balance()
    {
        $this->repo
            ->expects($this->once())
            ->method('getUserBalance')
            ->with($this->user)
            ->willReturn(15);

        $this->assertEquals(15, $this->walletManager->getUserBalance($this->user));
    }

    /**
     * @test
     */
    public function it_creates_new_wallet_for_user_and_persist_object()
    {
        $wallet = new Wallet();
        $wallet->setUser($this->user);

        $this->em
            ->expects($this->once())
            ->method('persist')
            ->with($wallet);

        $this->em
            ->expects($this->once())
            ->method('flush');

        $this->user
            ->expects($this->once())
            ->method('setWallet')
            ->with($wallet);

        $this->assertEquals($wallet, $this->walletManager->createWallet($this->user));
    }

    /**
     * @test
     */
    public function it_should_add_amount_to_user_wallet()
    {
        $this->user
            ->expects($this->once())
            ->method('getWallet')
            ->willReturn($this->wallet);

        $walletOperation = new WalletOperation();
        $walletOperation->setAmount(50);
        $walletOperation->setTitle('Payment for RDW services');
        $walletOperation->setWallet($this->wallet);

        $this->em
            ->expects($this->once())
            ->method('persist')
            ->with($walletOperation);

        $this->em
            ->expects($this->once())
            ->method('flush');

        $this->assertTrue($this->walletManager->add($this->user, 50, 'Payment for RDW services'));
    }

    /**
     * @test
     */
    public function it_should_add_amount_to_new_wallet()
    {
        $this->user
            ->expects($this->once())
            ->method('getWallet')
            ->willReturn(null);

        $wallet = $this->walletManager->createWallet($this->user);

        $walletOperation = new WalletOperation();
        $walletOperation->setAmount(50);
        $walletOperation->setTitle('Payment for RDW services');
        $walletOperation->setWallet($wallet);

        $this->em
            ->expects($this->exactly(2))
            ->method('persist')
            ->withConsecutive(
                array($this->equalTo($wallet)),
                array($this->equalTo($walletOperation))
            );

        $this->em
            ->expects($this->exactly(2))
            ->method('flush');

        $this->assertTrue($this->walletManager->add($this->user, 50, 'Payment for RDW services'));
    }
}
