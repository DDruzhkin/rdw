<?php
/**
 * Created by PhpStorm.
 * User: vb
 * Date: 12/14/14
 * Time: 3:38 PM
 */

namespace RDW\Bundle\WalletBundle\Tests\Entity;


use RDW\Bundle\WalletBundle\Entity\Wallet;
use RDW\Bundle\WalletBundle\Entity\WalletOperation;

class WalletTest extends \PHPUnit_Framework_TestCase {

    protected $user;

    /**
     * set up
     */
    public function setUp()
    {
        $this->user = $this->getMock('RDW\Bundle\UserBundle\Entity\User');
    }

    /**
     * @test
     */
    public function it_should_has_no_id_by_default()
    {
        $wallet = new Wallet();

        $this->assertNull($wallet->getId());
    }

    /**
     * @test
     */
    public function it_should_no_user_by_default()
    {
        $wallet = new Wallet();

        $this->assertNull($wallet->getUser());
    }

    /**
     * @test
     */
    public function it_should_set_user_properly()
    {
        $wallet = new Wallet();
        $wallet->setUser($this->user);
        $this->assertInstanceOf('RDW\Bundle\UserBundle\Entity\User', $wallet->getUser());
    }

    /**
     * @test
     */
    public function it_should_has_no_wallet_operations_by_default()
    {
        $wallet = new Wallet();

        $this->assertInstanceOf('Doctrine\Common\Collections\ArrayCollection', $wallet->getWalletOperations());
        $this->assertCount(0, $wallet->getWalletOperations());
    }

    /**
     * @test
     */
    public function it_should_add_wallet_operation_properly()
    {
        $wallet = new Wallet();
        $walletOperation = new WalletOperation();
        $this->assertCount(0, $wallet->getWalletOperations());

        $wallet->addWalletOperation($walletOperation);
        $this->assertCount(1, $wallet->getWalletOperations());
    }
}
