<?php
/**
 * Created by PhpStorm.
 * User: vb
 * Date: 12/14/14
 * Time: 3:51 PM
 */

namespace RDW\Bundle\WalletBundle\Tests\Entity;


use RDW\Bundle\WalletBundle\Entity\WalletOperation;

class WalletOperationTest extends \PHPUnit_Framework_TestCase
{
    protected $wallet;

    /**
     * set up
     */
    public function setUp()
    {
        $this->wallet = $this->getMock('RDW\Bundle\WalletBundle\Entity\Wallet');
    }

    /**
     * @test
     */
    public function it_should_has_no_id_by_default()
    {
        $walletOperation = new WalletOperation();

        $this->assertNull($walletOperation->getId());
    }

    /**
     * @test
     */
    public function it_should_no_wallet_by_default()
    {
        $walletOperation = new WalletOperation();

        $this->assertNull($walletOperation->getWallet());
    }

    /**
     * @test
     */
    public function it_should_set_wallet_properly()
    {
        $walletOperation = new WalletOperation();
        $walletOperation->setWallet($this->wallet);
        $this->assertInstanceOf('RDW\Bundle\WalletBundle\Entity\Wallet', $walletOperation->getWallet());
    }

    /**
     * @test
     */
    public function it_should_set_amount_properly_with_positive_value()
    {
        $walletOperation = new WalletOperation();
        $walletOperation->setAmount('111');

        $this->assertEquals('111', $walletOperation->getAmount());
    }

    /**
     * @test
     */
    public function it_should_set_title_properly()
    {
        $walletOperation = new WalletOperation();
        $walletOperation->setTitle('Operation title');

        $this->assertEquals('Operation title', $walletOperation->getTitle());
    }

    /**
     * @test
     */
    public function it_should_set_amount_properly_with_negative_value()
    {
        $walletOperation = new WalletOperation();
        $walletOperation->setAmount('-111');

        $this->assertEquals('-111', $walletOperation->getAmount());
    }
}
