<?php
namespace RDW\Bundle\WalletBundle\Tests\Twig;

use RDW\Bundle\WalletBundle\Twig\WalletExtension;

/**
 * Class WalletExtensionTest
 *
 * @package RDW\Bundle\WalletBundle\Tests\Twig
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class WalletExtensionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $container;

    /**
     * @var WalletExtension
     */
    protected $walletExtension;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $walletRepository;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $user;

    /**
     * set up
     */
    public function setUp()
    {
        $this->container = $this->getMockBuilder('Symfony\Component\DependencyInjection\ContainerInterface')->getMock();
        $this->user = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');
        $this->walletRepository = $this
            ->getMockBuilder('RDW\Bundle\WalletBundle\Repository\WalletRepository')
            ->disableOriginalConstructor()
            ->getMock();

        $this->walletExtension = new WalletExtension();
        $this->walletExtension->setContainer($this->container);
    }

    /**
     * @test
     */
    public function it_should_return_proper_functions()
    {
        $functions = [
            new \Twig_SimpleFunction('balance', [$this->walletExtension, 'getUserBalance'])
        ];

        $this->assertEquals($functions, $this->walletExtension->getFunctions());
    }

    /**
     * @test
     */
    public function its_should_get_proper_name()
    {
        $this->assertEquals('rdw_wallet_extension', $this->walletExtension->getName());
    }

    /**
     * @test
     */
    public function its_should_get_user_balance()
    {
        $this->container
            ->expects($this->once())
            ->method('get')
            ->with('rdw_wallet.wallet.repository')
            ->willReturn($this->walletRepository);

        $this->walletRepository
            ->expects($this->once())
            ->method('getUserBalance')
            ->with($this->user)
            ->willReturn(50);

        $this->assertEquals(50, $this->walletExtension->getUserBalance($this->user));
    }
}
