<?php
/**
 * Created by PhpStorm.
 * User: vb
 * Date: 1/28/15
 * Time: 8:43 AM
 */

namespace RDW\Bundle\WalletBundle\Tests\Repository;
use RDW\Bundle\WalletBundle\Repository\WalletRepository;

/**
 * Class WalletRepositoryTest
 *
 * @package RDW\Bundle\WalletBundle\Tests\Repository
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class WalletRepositoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $entityManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $queryBuilder;

    /**
     * @var WalletRepository
     */
    private $walletRepository;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $class;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $query;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $user;

    private $newHydrator;

    /**
     * set up
     */
    public function setUp()
    {
        $this->entityManager = $this->getMockBuilder('Doctrine\ORM\EntityManager')
            ->disableOriginalConstructor()
            ->getMock();

        $this->query = $this->getMockBuilder('\Doctrine\ORM\AbstractQuery')
            ->setMethods(array('setParameter', 'getResult'))
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $this->class = $this->getMockBuilder('\Doctrine\ORM\Mapping\ClassMetadata')
            ->disableOriginalConstructor()
            ->getMock();

        $this->queryBuilder = $this->getMockBuilder('Doctrine\ORM\QueryBuilder')
            ->disableOriginalConstructor()
            ->getMock();

        $this->queryBuilder
            ->expects($this->any())
            ->method('getQuery')
            ->will($this->returnValue($this->query));

        $this->entityManager
            ->expects($this->once())
            ->method('createQueryBuilder')
            ->will($this->returnValue($this->queryBuilder));

        $this->walletRepository = new WalletRepository($this->entityManager, $this->class);
        $this->user = $this->getMock('RDW\Bundle\UserBundle\Entity\RegisteredUser');
    }

    /**
     * @test
     */
    public function it_should_return_user_balance_from_wallet_repository()
    {
        $this->queryBuilder
            ->expects($this->once())
            ->method('select')
            ->with('SUM(o.amount) as amount')
            ->will($this->returnValue($this->queryBuilder));

        $this->queryBuilder
            ->expects($this->once())
            ->method('from')
            ->with($this->equalTo('RDWWalletBundle:Wallet'), $this->equalTo('w'))
            ->will($this->returnValue($this->queryBuilder));

        $this->queryBuilder
            ->expects($this->once())
            ->method('join')
            ->with($this->equalTo('w.walletOperations'), $this->equalTo('o'))
            ->will($this->returnValue($this->queryBuilder));

        $this->queryBuilder
            ->expects($this->once())
            ->method('where')
            ->with($this->equalTo('w.user = :user'))
            ->will($this->returnValue($this->queryBuilder));

        $this->queryBuilder
            ->expects($this->once())
            ->method('groupBy')
            ->with('o.wallet')
            ->will($this->returnValue($this->queryBuilder));

        $this->queryBuilder
            ->expects($this->once())
            ->method('setParameter')
            ->with($this->equalTo('user'), $this->equalTo($this->user))
            ->will($this->returnValue($this->queryBuilder));

        $this->query
            ->expects($this->never())
            ->method('getSingleScalarResult');

        $this->assertEquals(0, $this->walletRepository->getUserBalance($this->user));
    }
}
