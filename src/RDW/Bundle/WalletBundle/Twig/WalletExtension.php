<?php

namespace RDW\Bundle\WalletBundle\Twig;

use RDW\Bundle\OrderBundle\Entity\OrderItem;
use RDW\Bundle\UserBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class WalletExtension
 *
 * @package RDW\Bundle\WalletBundle\Twig
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 */
class WalletExtension extends \Twig_Extension implements ContainerAwareInterface
{
    /**
     * Container
     *
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('balance', [$this, 'getUserBalance'])
        ];
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function getUserBalance(User $user)
    {
        $balance = $this->container->get('rdw_wallet.wallet.repository')->getUserBalance($user);

        return $balance;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'rdw_wallet_extension';
    }
}
