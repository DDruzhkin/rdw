<?php

namespace RDW\Bundle\WalletBundle\Repository;

use Doctrine\ORM\EntityRepository;
use RDW\Bundle\UserBundle\Entity\User;

/**
 * Class WalletRepository
 *
 * @package RDW\Bundle\WalletBundle\Repository
 */
class WalletRepository extends EntityRepository
{
    /**
     * @param User $user
     *
     * @return array
     */
    public function getUserBalance(User $user)
    {
        $qb = $this->_em->createQueryBuilder();

        $qb
            ->select('SUM(o.amount) as amount')
            ->from('RDWWalletBundle:Wallet', 'w')
            ->join('w.walletOperations', 'o')
            ->where('w.user = :user')
            ->groupBy('o.wallet')
            ->setParameter('user', $user);

        if ($qb->getQuery()->getResult()) {
            return $qb->getQuery()->getSingleScalarResult();
        } else {
            return 0;
        }
    }
}
