<?php

namespace RDW\Bundle\WalletBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RDW\Bundle\UserBundle\Entity\User;

/**
 * Class Wallet
 *
 * @package RDW\Bundle\WalletBundle\Entity
 * @author  Vytautas Beliunas <vytautas@eface.lt>
 *
 * @ORM\Entity(repositoryClass="RDW\Bundle\WalletBundle\Repository\WalletRepository")
 * @ORM\Table(name="wallet")
 */
class Wallet
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="RDW\Bundle\UserBundle\Entity\User", inversedBy="wallet")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RDW\Bundle\WalletBundle\Entity\WalletOperation", mappedBy="wallet", cascade={"persist"})
     */
    protected $walletOperations;

    /**
     * default constructor
     */
    public function __construct()
    {
        $this->walletOperations = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \RDW\Bundle\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param \RDW\Bundle\UserBundle\Entity\User $user
     *
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWalletOperations()
    {
        return $this->walletOperations;
    }

    /**
     * @param WalletOperation $walletOperation
     *
     * @return $this
     */
    public function addWalletOperation(WalletOperation $walletOperation)
    {
        $this->walletOperations->add($walletOperation);

        return $this;
    }
}
