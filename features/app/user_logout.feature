@users
Feature: User logout
  In order to register again
  As a user
  I need to be able to log out

  Background:
    Given there are following users:
      | email | password | role| type | enabled |
      | bar@bar.com | foofoo | ROLE_EMPLOYER | employer | yes |

  Scenario: Successfully login in and logout
    Given I am on the login page
    Then I fill in "Email" with "bar@bar.com"
    And I fill in "Password" with "foofoo"
    And I press "Login"
    Then I should see "Logout"
    Then I should not see "Registration"
    And I follow "Logout"
    Then I should see "Registration"
    And I should not see "Logout"