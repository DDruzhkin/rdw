@users
Feature: Job and CV create access
  In order to access job or CV creation page
  As a employer or employee
  I need to be logged in

  Background:
    Given there are following users:
      | email                 | password | role          | type     | enabled |
      | behat+employer@nebijokit.lt | foo      | ROLE_EMPLOYER | employer | yes     |
      | behat+employee@nebijokit.lt | foo      | ROLE_EMPLOYEE | employee | yes     |

  Scenario: anonymous user should be able to access job creation page
    When I am on the job create page
    Then the response status code should be 200
    Then I should see "CREATE NEW JOB"

  Scenario: anonymous user should be able to access cv creation page
    When I am on "/cv/new"
    Then the response status code should be 200
    Then I should see "ADD CV"

  Scenario: Trying to access job create page when logged in as employee
    Given I am on the login page
    Then I fill in "Email" with "behat+employee@nebijokit.lt"
    And I fill in "Password" with "foo"
    And I press "Login"
    Then I should be on dashboard
    Then I am on the job create page
    Then the response status code should be 403

  Scenario: Trying to access cv create page when logged in as employer
    Given I am on the login page
    Then I fill in "Email" with "behat+employer@nebijokit.lt"
    And I fill in "Password" with "foo"
    And I press "Login"
    Then I should be on dashboard
    Then I am on the cv create page
    Then the response status code should be 403

  Scenario: Successfully access job create page when logged in as employer
    Given I am on the login page
    Then I fill in "Email" with "behat+employer@nebijokit.lt"
    And I fill in "Password" with "foo"
    And I press "Login"
    Then I should be on dashboard
    Then I am on the job create page

  Scenario: Successfully access job create page when logged in as employer
    Given I am on the login page
    Then I fill in "Email" with "behat+employee@nebijokit.lt"
    And I fill in "Password" with "foo"
    And I press "Login"
    Then I should be on dashboard
    Then I am on the cv create page
