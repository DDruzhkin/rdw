@users
Feature: Change current password
  In order to change my current password
  As a logged in user
  I need to be able to change my old password with new one

  Background:
    Given there are following cities:
      | title |
      | Minsk |
      | Vitebsk |
      | Dansk |
    Given there are following users:
      | email | password | role| type | enabled |
      | bar@bar.com | foofoo | ROLE_EMPLOYEE | employee | yes |
    Given I am on the login page
    Then I fill in "Email" with "bar@bar.com"
    And I fill in "Password" with "foofoo"
    And I press "Login"
    Then I should be on dashboard
    When I follow "Profile"
    Then I should be on profile edit page
    And I fill in the following:
      | Name                  | John |
      | Surname               | Doe |
      | Video                 | http://youtu.be/CSw9XC61Y24?list=RD-CMUsKZkdg4 |
      | Phone                 | +37066661123                                   |
      | Facebook contact      | http://facebook.com                            |
      | VK contact            | http://vk.com                                  |
      | Odnoklassniki contact | http:/odnoklassniki.ru                         |
      | Google plus contact   | http://google.com                              |
      | Twitter contact       | http://twitter.com                             |
      | LinkedId contact      | http://linkedin.com                            |
      | Skype contact         | http://skype.com                               |
      | Male                  | m                                              |

  Scenario: Successfully change old password
    When I fill in "Old password" with "foofoo"
    And  I fill in "Password" with "password"
    And  I fill in "Password again" with "password"
    And  I press "Update"
    Then I should see "The profile has been updated"
    And  I follow "Logout"
    And  I follow "Login"
    Then I fill in "Email" with "bar@bar.com"
    And  I fill in "Password" with "password"
    And  I press "Login"
    Then I should see "Logout"

  Scenario: Successfully change old password and try login with old password
    When I fill in "Old password" with "foofoo"
    And  I fill in "Password" with "password"
    And  I fill in "Password again" with "password"
    And  I press "Update"
    Then I should see "The profile has been updated"
    And  I follow "Logout"
    And  I follow "Login"
    Then I fill in "Email" with "bar@bar.com"
    And  I fill in "Password" with "foofoo"
    And  I press "Login"
    Then I should see "Bad credentials"

  Scenario: Trying to enter wrong old password
    When I fill in "Old password" with "foofoo123"
    And  I fill in "Password" with "password"
    And  I fill in "Password again" with "password"
    And  I press "Update"
    Then I should not see "The profile has been updated"
    And  I should see "Wrong value for your current password"

  Scenario: Trying to enter too short new password
    When I fill in "Old password" with "foofoo"
    And  I fill in "Password" with "pass"
    And  I fill in "Password again" with "pass"
    And  I press "Update"
    Then I should not see "The profile has been updated"
    And  I should see "Password must be at least 5 characters length."

  Scenario: Trying to enter different password for new password
    When I fill in "Old password" with "foofoo"
    And  I fill in "Password" with "pass"
    And  I fill in "Password again" with "pass123"
    And  I press "Update"
    Then I should not see "The profile has been updated"
    And  I should see "Password mismatch"
