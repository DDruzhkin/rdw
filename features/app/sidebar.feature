@users
Feature: Show different sidebar for different roles

    Scenario: Show jobs related sidebar on index page
        Given I am on homepage
        Then I should see "Subscribe jobs newsletter"

    Scenario: Show cvs related sidebar on cvs page
        Given I am on "cvs"
        Then I should see "Subscribe cvs newsletter"
