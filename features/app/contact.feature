@contacts
Feature: Contact request
  In order to send request for administrator
  As a visitor
  I need to be able to create contact request

  Scenario: Successfully send request
    Given I am on the contact form page
    And I should see "rdw_contact_message" form
    When I fill in the following:
      | Name | Tom |
      | Email | foo@bar.com |
      | Message | Short request message goes here |
    And I fill captcha validator "correctly"
    And I press "Send"
    Then I should be on the contact form page
    And I should see "The message was successfully sent"

  Scenario: Trying to send request with empty data
    Given I am on the contact form page
    And I should see "rdw_contact_message" form
    And I press "Send"
    Then I should be on the contact form page
    And I should see "The name cannot be empty"
    And I should see "The email cannot be empty"
    And I should see "The message body cannot be empty"
    And I should see "Please enter valid sum"