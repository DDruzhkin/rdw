@users
Feature: User profile update
  In order to edit my profile info
  As a logged in employee
  I need to be able to change my profile info

  Background:
    Given there are following cities:
      | title |
      | Minsk |
      | Vitebsk |
      | Dansk |
    Given there are following users:
      | email | password | role| type | enabled |
      | bar@bar.com | foofoo | ROLE_EMPLOYEE | employee | yes |
    Given I am on the login page
    Then I fill in "Email" with "bar@bar.com"
    And I fill in "Password" with "foofoo"
    And I press "Login"
    Then I should be on dashboard

  Scenario: Successfully update profile info
    When I follow "Profile"
    Then I should be on profile edit page
    And I should see "Personal Info"
    And I fill in the following:
      | Name                  | John |
      | Surname               | Doe |
      | Video                 | http://youtu.be/CSw9XC61Y24?list=RD-CMUsKZkdg4 |
      | Phone                 | +37066661123                                   |
      | Facebook contact      | http://facebook.com                            |
      | VK contact            | http://vk.com                                  |
      | Odnoklassniki contact | http:/odnoklassniki.ru                         |
      | Google plus contact   | http://google.com                              |
      | Twitter contact       | http://twitter.com                             |
      | LinkedId contact      | http://linkedin.com                            |
      | Skype contact         | http://skype.com                               |
    And I select "m" from "rdw_employee_profile[gender]"
    And I fill in birthday with "1986-8-19"
    And I press "Update"
    Then I should see "The profile has been updated"

  Scenario: Trying to submit empty form
    When I follow "Profile"
    Then I should be on profile edit page
    And I should see "PERSONAL INFO"
    And I press "Update"
    Then I should not see "The profile has been updated"
    And I should see "Name cannot be empty"
    And I should see "Surname is blank"
    And I should see "Choose option"
    And I should see "Phone cannot be empty"

  Scenario: Trying to submit invalid phone
    When I follow "Profile"
    Then I should be on profile edit page
    And I should see "PERSONAL INFO"
    And I fill in "Phone" with "54a5s4da5sd"
    And I press "Update"
    Then I should not see "The profile has been updated"
    And I should see "Field must be a valid phone number and can have only numbers, +, - and spaces"

  Scenario: Trying to submit invalid name
    When I follow "Profile"
    Then I should be on profile edit page
    And I should see "PERSONAL INFO"
    And I fill in "Name" with "54a5s4da5 sd"
    And I press "Update"
    Then I should not see "The profile has been updated"
    And I should see "Field does not meet permitted format. Please use only letters and / or space by introducing the name."

  Scenario: Trying to submit invalid surname
    When I follow "Profile"
    Then I should be on profile edit page
    And I should see "PERSONAL INFO"
    And I fill in "Surname" with "54a5s4da5 sd"
    And I press "Update"
    Then I should not see "The profile has been updated"
    And I should see "Field does not meet permitted format. Please use only letters and / or space by introducing the name."
