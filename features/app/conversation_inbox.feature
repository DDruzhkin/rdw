@conversations
Feature: View received messages
  In order to see my received messages
  As a logged in user
  I need to be able to view full list of my conversations

  Background:
    Given there are following users:
      | email | password | role| type | enabled |
      | bar@bar.com | foofoo | ROLE_EMPLOYEE | employee | yes |
      | foo@bar.com | barbar | ROLE_EMPLOYER | employer | yes |
      | foofoo@bar.com | bar1bar | ROLE_EMPLOYER | employer | yes |
    Given there are following conversations:
      | title | receiver | sender | updatedAt |
      | Title1 | foo@bar.com | foofoo@bar.com | -10M |
      | Title2 | bar@bar.com | foofoo@bar.com | -15M |
      | Title3 | bar@bar.com | foofoo@bar.com | -20M |
    Given I am on the login page
    Then I fill in "Email" with "foofoo@bar.com"
    And I fill in "Password" with "bar1bar"
    And I press "Login"
    Then I should be on dashboard

  Scenario: Successfully list my inbox
    When I follow "Messages"
    Then I should be on inbox page
    And I should see 3 messages_list in the list

  Scenario: Successfully view conversation
    When I follow "Messages"
    Then I should be on inbox page
    And I should see 3 messages_list in the list
    And I should see 3 in the "rdw_user_conversation_badge"
    When  I follow "bar@bar.com"
    Then I should be on view conversation page with title "Title3"
    And  I should see "My conversation"
    And I should see 2 in the "rdw_user_conversation_badge"
