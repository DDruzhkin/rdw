@users
Feature: User login
  In order to view my profile info
  As a visitor
  I need to be able to log in

  Background:
    Given there are following users:
      | email | password | role| type | enabled |
      | bar@bar.com | foofoo | ROLE_EMPLOYER | employer | yes |
      | foo@bar.com | secret | ROLE_EMPLOYEE | employee | no  |

  Scenario: Successfully login in
    Given I am on the login page
    Then I fill in "_username" with "bar@bar.com"
    And I fill in "Password" with "foofoo"
    And I press "Login"
    Then I should be on dashboard
    And I should see "Logout"

  Scenario: Trying to login in with wrong email
    Given I am on the login page
    Then I fill in "username" with "123bar@bar.com"
    And I fill in "Password" with "foofoo"
    And I press "Login"
    Then I should be on login page
    And I should see "Bad credentials"

  Scenario: Trying to login in with wrong email
    Given I am on the login page
    Then I fill in "username" with "123bar@bar.com"
    And I fill in "Password" with "foofoo"
    And I press "Login"
    Then I should be on login page
    Then I should see "Bad credentials"

  Scenario: Trying to login in with inactive email
    Given I am on the login page
    Then I fill in "username" with "foo@bar.com"
    And I fill in "Password" with "secret"
    And I press "Login"
    Then I should be on login page
    Then I should see "User account is disabled"

  Scenario: Trying to login in wrong password
    Given I am on the login page
    Then I fill in "username" with "bar@bar.com"
    And I fill in "Password" with "secret123"
    And I press "Login"
    Then I should be on login page
    Then I should see "Bad credentials"
