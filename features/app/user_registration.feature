@users
Feature: User registration
  In order to find a job
  As a visitor
  I need to be able to create new account

  Background:
    Given there are following users:
      | email | password | role| type |
      | bar@bar.com | foo | ROLE_EMPLOYER | employer |
    Given there are following company types:
        | title | position |
        | ODO   | 0 |
        | OHO   | 1 |
        | REA   | 2 |

  Scenario: Successfully register as employee
    Given I am on the registration page
    And I follow "Registration of employee"
    When I fill in the following:
      | Name | Foo |
      | Email | foo@bar.com |
      | Password | secret|
      | Password again | secret |
    And I fill captcha validator "correctly"
    And I check "I agree to the terms of use www.rdw.by"
    And I press "Register"
    Then I should see "Check your email foo@bar.com"

  Scenario: Successfully register as employer
    Given I am on the registration page
    And I follow "Registration of employer"
    When I fill in the following:
      | Email | foo@bar.com |
      | Password | secret|
      | Password again | secret |
      | Company title  | Test company|
      | Contact person  | Contact person |
      | UNP code        | 123456789 |
    And I select "1" from "rdw_user_registration[employerType]"
    And I select "ODO" from "Company type"
    And I fill captcha validator "correctly"
    And I check "I agree to the terms of use www.rdw.by"
    And I press "Register"
    Then I should see "Check your email foo@bar.com"

  Scenario: Successfully register as employer like person
    Given I am on the registration page
    And I follow "Registration of employer"
    When I fill in the following:
      | Email | foo@bar.com |
      | Password | secret   |
      | Password again | secret |
      | Contact person   | Person name |
    And I select "3" from "rdw_user_registration[employerType]"
    And I fill captcha validator "correctly"
    And I check "I agree to the terms of use www.rdw.by"
    And I press "Register"
    Then I should see "Check your email foo@bar.com"

  Scenario: Trying to register as employer like person with wrong data
    Given I am on the registration page
    And I follow "Registration of employer"
    And I select "Employer type person" from "Employer type"
    When I fill in the following:
      | Email | foo@bar.com |
      | Password | secret   |
      | Password again | secret |
    And I fill captcha validator "correctly"
    And I check "I agree to the terms of use www.rdw.by"
    And I press "Register"
    Then I should see "Field is required"

  Scenario: Trying to register as employer with wrong data
    Given I am on the registration page
    And I follow "Registration of employer"
    And I select "Employer type agency" from "Employer type"
    When I fill in the following:
      | Email | foo@bar.com |
      | Password | secret   |
      | Password again | secret |
      | Contact person  | Contact person |
    And I fill captcha validator "correctly"
    And I check "I agree to the terms of use www.rdw.by"
    And I press "Register"
    And I should see "Company title is blank"
    And I should see "Choose company type"

  Scenario: Trying to register with taken email
    Given I am on the registration page
    And I follow "Registration of employee"
    When I fill in the following:
      | Email | bar@bar.com |
    And I press "Register"
    Then I should see "Email is already taken. Please enter other email address. If you already registered and forgot password please click here"

  Scenario: Trying to enter to short password
    Given I am on the registration page
    And I follow "Registration of employee"
    When I fill in the following:
      | Email | foo@bar.com |
      | Password | foo |
      | Password again | foo |
    And I press "Register"
    Then I should see "Password must be at least 5 characters length."

  Scenario: Trying to submit empty form
    Given I am on the registration page
    And I follow "Registration of employee"
    And I press "Register"
    And I should see "Email is blank"
    And I should see "Password is blank"

  Scenario: Trying to enter invalid email
    Given I am on the registration page
    And I follow "Registration of employee"
    When I fill in the following:
      | Email | foo@bar |
      | Password | foobar |
      | Password again | foobarbaz |
    And I press "Register"
    Then I should see "Enter valid email address"

  @javascript
  Scenario: Trying to change employer type
    Given I am on the registration page
    And I follow "Registration of employer"
    And I select "Person" from "Employer type"
    Then I should see "Name"
    And I should not see "Company type"
    And I should not see "Company title"
    And I should not see "Contact person"
    Then I select "Agency" from "Employer type"
    And I should see "Company type"
    And I should see "Company title"
    And I should see "Contact person"
    And I should not see "Name"

  @javascript
  Scenario: Trying to submit form without agreement
    Given I am on the registration page
    And I follow "Registration of employer"
    And I press "Register"
    And I should not see "Email is blank"
