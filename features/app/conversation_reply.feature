@conversations
Feature: Reply to conversation
  In order to reply to conversation
  As a logged in user
  I need to be able write new message

  Background:
    Given there are following users:
      | email | password | role| type | enabled |
      | bar@bar.com | foofoo | ROLE_EMPLOYEE | employee | yes |
      | foo@bar.com | barbar | ROLE_EMPLOYER | employer | yes |
      | foofoo@bar.com | bar1bar | ROLE_EMPLOYER | employer | yes |
    Given there are following conversations:
      | title | receiver | sender | updatedAt |
      | Title1 | foo@bar.com | foofoo@bar.com | -10M |
      | Title2 | bar@bar.com | foofoo@bar.com | -15M |
      | Title3 | bar@bar.com | foofoo@bar.com | -20M |
    Given I am on the login page
    Then I fill in "Email" with "foofoo@bar.com"
    And I fill in "Password" with "bar1bar"
    And I press "Login"
    Then I should be on dashboard

  Scenario: Successfully reply into conversation
    When I follow "Messages"
    Then I should be on inbox page
    And  I follow "bar@bar.com"
    And  I should be on view conversation page with title "Title3"
    And  I fill in "Message" with "My new message is replied"
    And  I press "Reply"
    Then I should be on inbox page
    And  I should see "My new message is replied"

  Scenario: Trying to submit empty reply form
    When I follow "Messages"
    Then I should be on inbox page
    And  I follow "bar@bar.com"
    And  I should be on view conversation page with title "Title3"
    And  I press "Reply"
    Then I should be on reply conversation page with title "Title3"
