@users
Feature: User profile update
  In order to edit my profile info
  As a logged in employer
  I need to be able to change my profile info

  Background:
    Given there are following cities:
      | title |
      | Minsk |
      | Vitebsk |
      | Dansk |
    Given there are following scopes:
      | title |
      | IT |
      | Food |
    Given there are following users:
      | email | password | role| type | enabled | employerType |
      | bar@bar.com | foofoo | ROLE_EMPLOYER | employer | yes | 1 |
      | foo@bar.com | fooboo | ROLE_EMPLOYER | employer | yes | 3 |
    Given there are following company types:
      | title | position |
      | ODO   | 0 |
      | OHO   | 1 |
      | REA   | 2 |
    Given I am on the login page
    Then I fill in "Email" with "bar@bar.com"
    And I fill in "Password" with "foofoo"
    And I press "Login"
    Then I should be on dashboard

  Scenario: I should see different information then registered as employer with person type
    Given I am on the login page
    Then  I fill in "Email" with "foo@bar.com"
    And   I fill in "Password" with "fooboo"
    And   I press "Login"
    Then  I should be on dashboard
    And   I follow "Profile"
    And   I should see "Company data"
    And   I should see "Url address"
    And   I should not see "Company url address"
    And   I should not see "UPN code"
    And   I should not see "Company title"
    And   I should see "Information about company"
    And   I should not see "Company type"
    And   I should see "Employer type"
    And   I should not see "Contact person position"
    And   I should see "Name"
    And   I should see "Surname"

  Scenario: Successfully update profile info
    When I follow "Profile"
    Then I should be on profile edit page
    And  I should see "Company data"
    And  I fill in the following:
      | Video                   | http://youtu.be/CSw9XC61Y24?list=RD-CMUsKZkdg4 |
      | Phone                   | +37066661123                                   |
      | Fax                     | +37066661123                                   |
      | Address                 | Address                                        |
      | Company url address     | http://example.com                             |
      | UNP code                | 123                                            |
      | Facebook contact        | http://facebook.com                            |
      | VK contact              | http://vk.com                                  |
      | Odnoklassniki contact   | http:/odnoklassniki.ru                         |
      | Google plus contact     | http://google.com                              |
      | Twitter contact         | http://twitter.com                             |
      | LinkedId contact        | http://linkedin.com                            |
      | Skype contact           | http://skype.com                               |
      | Contact person name     | Contact person                                 |
      | Surname                 | Contact surname                                |
      | Company title           | Big company                                    |
      | UNP code                | 123456789                                      |
    And I select "Food" from "Scope"
    And I select "OHO" from "Company type"
    And I fill in company info with "300" symbols
    And I press "Update"
    Then I should see "The profile has been updated"

  Scenario: Trying to submit empty form
    When I follow "Profile"
    Then I should be on profile edit page
    And I should see "Company data"
    And I press "Update"
    Then I should not see "The profile has been updated"
    And I should see "Company title is blank"
    And I should see "Field is required"
    And I should see "Choose option"
    And I should see "Phone cannot be empty"

  Scenario: Trying to submit invalid phone
    When I follow "Profile"
    Then I should be on profile edit page
    And I should see "Company Data"
    And I fill in "Phone" with "54a5s4da5sd"
    And I press "Update"
    Then I should not see "The profile has been updated"
    And I should see "Field must be a valid phone number and can have only numbers, +, - and spaces"

  Scenario: Trying to submit invalid fax
    When I follow "Profile"
    Then I should be on profile edit page
    And I should see "Company Data"
    And I fill in "Fax" with "54a5s4da5sd"
    And I press "Update"
    Then I should not see "The profile has been updated"
    And I should see "Field must be a valid fax number and can have only numbers, +, - and spaces"

  Scenario: Trying to submit invalid company info
    When I follow "Profile"
    Then I should be on profile edit page
    And I fill in company info with "49" symbols
    And I press "Update"
    Then I should not see "The profile has been updated"
    And I should see "Information about employer must be at least 50 characters length"
    Then I fill in company info with "501" symbols
    And I press "Update"
    Then I should not see "The profile has been updated"
    And I should see "Information about employer cannot be longer than 500 characters length"



  @javascript
  Scenario: Trying to add city
    When I follow "Profile"
    Then I should be on profile edit page
    And  I follow "Add"
    And I select "Vitebsk" from "rdw_employer_profile_cities_1"

  @javascript
  Scenario: Trying to delete city
    When I follow "Profile"
    Then I should be on profile edit page
    And I should see "Minsk"
    And  I follow "Delete"
    And I should not see "Minsk"
