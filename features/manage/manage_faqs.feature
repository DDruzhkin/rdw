@faqs
Feature: Manage faqs list
  In order to mange faqs list
  As an administrator
  I want to able to list all faqs

  Background:
    Given there are following backend users:
      | username | password | role| type | status |
      | admin@example.com | rdwrdw | ROLE_ADMIN | admin | active |
    Given there are following faqs:
      | name | email | answer | question | status |
      | John     | john@legend.com | Good answer | Is car uses water? | active |
      | Destine  | destini@example.com | Approved answer | Is elephant eat grass? | hidden |
      | Maker  | maker@example.com | no no no | How much time left? | active |
    And I am logged in as administrator

  Scenario: Trying to list of all faqs
    Given I am on the backend home page
    When I follow "Faq list"
    Then I should be on the manage faq list page
    And I should see 3 faqs in the list
    And I should see "Good answer"
    And I should see "John"
    And I should see "john@legend.com"
    And I should see "Is car uses water?"

  @javascript
  Scenario: Trying to delete faq item
    Given I am on the backend home page
    And  I should see "Faq list"
    When I follow "Faq list"
    Then I should be on the manage faq list page
    And I should see 3 faqs in the list
    And I follow "Delete"
    And  I confirm the popup
    Then I should be on the manage faq list page
    And I should see 2 faqs in the list

  @javascript
  Scenario: Trying to cancel delete faq item
    Given I am on the backend home page
    And  I should see "Faq list"
    When I follow "Faq list"
    Then I should be on the manage faq list page
    And I should see 3 faqs in the list
    And I follow "Delete"
    And  I cancel the popup
    Then I should be on the manage faq list page
    And I should see 3 faqs in the list