@admins
Feature: Backend users management
  In order to manage backend users
  As an administrator
  I want to able to list backend users

  Background:
    Given there are following backend users:
      | username | password | role| type | status |
      | admin@example.com | rdwrdw | ROLE_ADMIN | admin | active |
      | manager@example.com | rdwrdw | ROLE_MANAGER | manager | inactive |
      | censor@example.com | rdwrdw | ROLE_BLOCKED | censor | blocked |
      | censor1@example.com | rdwrdw | ROLE_CENSOR | censor | deleted |
    And I am logged in as administrator

  Scenario: Seeing index of all backend users
    Given I am on the backend home page
    When I follow "Users"
    And I follow "User list"
    Then I should be on the user list page
    And I should see 3 users in the list

  Scenario: Seeing user create form
    Given I am on the backend home page
    When I follow "Users"
    And I follow "User list"
    Then I should be on the user list page
    And I follow "Add new user"
    And I should be on the user create page
    And I should see "rdw_manage_user" form

  Scenario: Successfully create user
    Given I am on the user create page
    When I fill in the following:
      | Username | tom@example.com |
    And I select "Admin" from "User type"
    And I select "Active" from "Status"
    And I fill in the following:
      | Name | Tom |
      | Surname | Doe |
      | Phone | +370 100-11 222 |
      | Password | rdwrdw |
      | Repeat password | rdwrdw |
    And I press "Save"
    Then I should be on the user list page
    And I should see "User successfully created"

  Scenario: Trying to create user with existing username
    Given I am on the user create page
    When I fill in the following:
      | Username | admin@example.com |
    And I press "Save"
    Then I should see "This email is already used"

  Scenario: Trying to enter invalid username
    Given I am on the user create page
    When I fill in the following:
      | Username | not_the_email |
    And I press "Save"
    Then I should see "Username must be a valid email address"

  Scenario: Successfully edit user
    Given I am on the user "manager@example.com" editing page
    When I fill in the following:
      | Username | tom.none@example.com |
      | Name | Tom |
      | Surname | Doe |
      | Phone | +370 100-11 222 |
      | Password | rdwrdw |
      | Repeat password | rdwrdw |
    And I select "Censor" from "User type"
    And I select "Inactive" from "Status"
    And I press "Save"
    Then I should be on the user list page
    And I should see "User successfully updated"
