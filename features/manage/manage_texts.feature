@texts
Feature: Content texts management
  In order to manage content texts
  As an administrator
  I want to able to list backend content texts

  Background:
    Given there are following backend users:
      | username | password | role| type | status |
      | admin@example.com | rdwrdw | ROLE_ADMIN | admin | active |
    And there are following text pages:
      | status | title | short_title | content |
      | active | More about us | About us | Long text about us |
      | inactive | CV guide for new users | Guide for new users | Long text information for new users |
      | deleted | Contact us long title | Contact us | Long text contact us |
    And I am logged in as administrator

  Scenario: Seeing index of all texts
    Given I am on the backend home page
    When I follow "Texts"
    Then I should be on the text list page
    And I should see 2 texts in the list

  Scenario: Seeing text create form
    Given I am on the backend home page
    When I follow "Texts"
    Then I should be on the text list page
    And I follow "Add new text page"
    And I should be on the text create page
    And I should see "rdw_content_manage_text" form

  Scenario: Successfully create text
    Given I am on the text create page
    When I select "Active" from "Status"
    And I fill in the following:
      | Title | Text about us |
      | Short title| Text short title |
      | Content | Main content goes here |
    And I press "Save"
    Then I should be on the text list page
    And I should see "Text page successfully created"

  Scenario: Successfully edit text page
    Given I am on the text page "More about us" editing page
    When I fill in the following:
      | Title | Text about us edited |
      | Short title| Text short title edited |
      | Content | Main content goes here, edited |
    And I select "Inactive" from "Status"
    And I press "Save"
    Then I should be on the text list page
    And I should see "Text page successfully updated"

  Scenario: Trying to submit empty form
    Given I am on the text create page
    When I select "" from "Status"
    And I press "Save"
    Then I should be on the text create page
    And I should see "Title cannot be empty"
    And I should see "Short title cannot be empty"
    And I should see "Content cannot be empty"

#  @javascript
#  Scenario: Trying to delete text page item
#    Given I am on the backend home page
#    And I should see "Texts"
#    When I follow "Texts"
#    Then I should be on the text list page
#    And I should see 2 texts in the list
#    And I follow "Delete"
#    And  I confirm the popup
#    Then I should be on the text list page
#    And I should see 1 texts in the list
#
#  @javascript
#  Scenario: Trying to cancel delete text page item
#    Given I am on the backend home page
#    And I should see "Texts"
#    When I follow "Texts"
#    Then I should be on the text list page
#    And I should see 2 texts in the list
#    And I follow "Delete"
#    And  I cancel the popup
#    Then I should be on the text list page
#    And I should see 2 texts in the list