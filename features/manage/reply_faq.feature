@faqs
Feature: Reply faq
  In order to reply to faq answer
  As an administrator
  I want to able to save reply form

  Background:
    Given there are following backend users:
      | username | password | role| type | status |
      | admin@example.com | rdwrdw | ROLE_ADMIN | admin | active |
    Given there are following faqs:
      | name | email | answer | question | status |
      | John     | john@legend.com | Good answer | Is car uses water? | active |
      | Destine  | destini@example.com | Approved answer | Is elephant eat grass? | hidden |
      | Maker  | maker@example.com | no no no | How much time left? | active |
    And I am logged in as administrator

  Scenario: Successfully reply to faq
    Given I am on the backend home page
    When I follow "Faq list"
    Then I should be on the manage faq list page
    And  I should see 3 faqs in the list
    And  I should see "Good answer"
    When I follow "Reply"
    Then I should be on reply faq page with name "Maker"
    And the "Answer" field should contain "no no no"
    And I fill in the following:
      | Question | Changed super question |
      | Answer | Updated super answer |
    And I select "Hidden" from "Status"
    And I press "Reply"
    Then I should be on the manage faq list page
    And I should see "Changed super question"
    And I should see "Updated super answer"
