@admins
Feature: Backend user login
  In order to start using backend
  As an admin
  I need to be able to log in

  Background:
    Given there are following backend users:
      | username | password | role| type | status |
      | admin@example.com | admin1 | ROLE_ADMIN | admin | active |
      | manager@example.com | manager1 | ROLE_MANAGER | manager | inactive |
      | censor@example.com | censor1 | ROLE_BLOCKED | censor | blocked |
      | censor1@example.com | censor1 | ROLE_CENSOR | censor | deleted |

  Scenario: Successfully login in
    Given I am on the manage login page
    Then I fill in "_username" with "admin@example.com"
    And I fill in "_password" with "admin1"
    And I press "Sign in"
    Then I should be on the backend home page
    And I should see "Logout"

  Scenario: Trying to login in with wrong username
    Given I am on the manage login page
    Then I fill in "_username" with "123bar@bar.com"
    And I fill in "_password" with "foofoo"
    And I press "Sign in"
    Then I should be on the manage login page
    And I should see "Bad credentials"

  Scenario: Trying to login in with inactive username
    Given I am on the manage login page
    Then I fill in "_username" with "manager@example.com"
    And I fill in "_password" with "manager1"
    And I press "Sign in"
    Then I should be on the manage login page
    Then I should see "Bad credentials"

  Scenario: Trying to login in with deleted username
    Given I am on the manage login page
    Then I fill in "_username" with "censor1@example.com"
    And I fill in "_password" with "censor1"
    And I press "Sign in"
    Then I should be on the manage login page
    Then I should see "Bad credentials"

  Scenario: Trying to login in with blocked username
    Given I am on the manage login page
    Then I fill in "_username" with "censor@example.com"
    And I fill in "_password" with "censor1"
    And I press "Sign in"
    Then I should be on the user blocked page
    And I should see "Logout"

  Scenario: Trying to login in with without password
    Given I am on the manage login page
    Then I fill in "_username" with "123bar@bar.com"
    And I fill in "_password" with ""
    And I press "Sign in"
    Then I should be on the manage login page
    And I should see "Bad credentials"