@admins
Feature: Backend user logout
  In order to leave manage area
  As a backend user
  I need to be able to log out

  Background:
    Given there are following backend users:
      | username | password | role| type | status |
      | admin@example.com | admin1 | ROLE_ADMIN | admin | active |

  Scenario: Successfully login in and logout
    Given I am on the manage login page
    Then I fill in "_username" with "admin@example.com"
    And I fill in "_password" with "admin1"
    And I press "Sign in"
    Then I should be on the backend home page
    And I should see "Logout"
    Then I should not see "Please sign in"
    And I follow "Logout"
    Then I should see "Please sign in"
    And I should not see "Logout"