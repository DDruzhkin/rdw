INSERT INTO `language_levels` (`id`, `title`, `position`)
VALUES
	(1, 'Базовый', 1),
	(2, 'Технический', 2),
	(3, 'Разговорный', 3),
	(4, 'Родной язык', 4);
