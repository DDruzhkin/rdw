# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure(2) do |config|
  config.vm.box = "scientific"
  config.vm.box_url = "http://download.frameos.org/sl6-64-chefclient-0.10.box"

  config.vm.network "forwarded_port", guest: 80, host: 8000

  config.vm.synced_folder "./", "/var/www", create: true, group: "vagrant", owner: "vagrant", :mount_options => ["dmode=777,fmode=777"]

  config.vm.provider "virtualbox" do |vb|
    vb.memory = "1024"

    # disable USB
    vb.customize ["modifyvm", :id, "--usb", "off"]
    vb.customize ["modifyvm", :id, "--usbehci", "off"]
  end

  config.ssh.forward_agent = true

  config.vm.provision "shell", inline: <<-SHELL
    # need to execute before provision
    sudo yum -y install openssh-server openssh-clients

    export DEBIAN_FRONTEND=noninteractive

    echo "Updating packages"
    sudo yum update > /dev/null

    echo "Installing unzip"
    sudo yum install unzip -y > /dev/null

    echo "Installing mysql"
    sudo yum install -y debconf-utils > /dev/null
    debconf-set-selections <<< "mysql-server mysql-server/root_password password"
    debconf-set-selections <<< "mysql-server mysql-server/root_password_again password 1234"
    sudo yum install -y mysql-server > /dev/null

    echo "Installing nginx + php"
    sudo rpm -Uvh http://mirror.webtatic.com/yum/el6/latest.rpm
    sudo yum install -y nginx php55w php55w-devel php55w-mysql php55w-curl php55w-mbstring php55w-dom > /dev/null
    # not working
    # sudo sed -i s#";date.timezone = / date.timezone = \"Europe/Minsk\""#g /etc/php.ini

    echo "Installing git"
    sudo yum install git -y > /dev/null

    echo "Installing NPM & its dependencies"
    sudo yum install -y nodejs npm > /dev/null
    sudo npm -g install bower less --loglevel error > /dev/null
    if ! [ -f /usr/local/bin/node ]; then
        sudo ln -s /usr/bin/nodejs /usr/local/bin/node
    fi
#
#     echo "Configuring Nginx"
#     if ! [ -f /etc/nginx/sites-available/nginx_vhost ]; then
#       cp /var/www/provision/nginx_vhost /etc/nginx/sites-available/nginx_vhost > /dev/null
#       ln -s /etc/nginx/sites-available/nginx_vhost /etc/nginx/sites-enabled/
#     fi
#     rm -rf /etc/nginx/sites-enabled/default
#     service nginx restart > /dev/null

    echo "Installing vendors"
    cd /var/www && php composer.phar install

    echo "PREPARING MYSQL DATABASES:";
    echo "--> Creating database";
    cd /var/www/ && sudo -u vagrant php app/console doctrine:database:drop --force
    cd /var/www/ && sudo -u vagrant php app/console doctrine:database:create

    echo "--> Running migrations";
    cd /var/www/ && sudo -u vagrant php app/console doctrine:migrations:migrate -n > /dev/null

    echo "Adding aliases"
    ALIAS=$(cat <<EOF
alias sf='php app/console'
alias composer='php /usr/bin/composer.phar'
EOF
)
    echo "${ALIAS}" > /home/vagrant/.bash_aliases
    sudo chown vagrant:vagrant /home/vagrant/.bash_aliases
  SHELL
end
